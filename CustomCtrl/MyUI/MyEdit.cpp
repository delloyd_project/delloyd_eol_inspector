﻿// MyEdit.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "MyEdit.h"

// CMyEdit
IMPLEMENT_DYNAMIC(CMyEdit, CEdit)

CMyEdit::CMyEdit()
{
	m_textRGB	= BLACK;
	m_bkRGB		= WHITE;
}

CMyEdit::~CMyEdit()
{
}

BEGIN_MESSAGE_MAP(CMyEdit, CEdit)
	ON_WM_CTLCOLOR()
	ON_CONTROL_REFLECT(EN_SETFOCUS, &CMyEdit::OnEnSetfocus)
	ON_WM_SETFOCUS()
	ON_WM_DRAWITEM()
	ON_WM_GETDLGCODE()
	ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()

// CMyEdit 메시지 처리기입니다.
HBRUSH CMyEdit::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CEdit::OnCtlColor(pDC, pWnd, nCtlColor);
	return hbr;
}

void CMyEdit::OnEnSetfocus()
{
}

void CMyEdit::OnSetFocus(CWnd* pOldWnd)
{
	return;
	CEdit::OnSetFocus(pOldWnd);
}

void CMyEdit::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CEdit::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CMyEdit::FontSizeChange(LONG weight, LONG height)
{
	LOGFONT	LogFont;

	GetFont()->GetLogFont(&LogFont);

	LogFont.lfWeight = weight;
	LogFont.lfHeight = height;

	m_font.CreateFontIndirect(&LogFont);
	SetFont(&m_font);
}

UINT CMyEdit::OnGetDlgCode()
{
	return DLGC_WANTMESSAGE;
}

HBRUSH CMyEdit::CtlColor(CDC* pDC, UINT nCtlColor)
{
	CWnd* pWnd = NULL;

	pDC->SetTextColor(m_textRGB);
	COLORREF rgb = pDC->SetBkColor(m_bkRGB);
	return HBRUSH(rgb);
}

void CMyEdit::SetMyBackColor(COLORREF rgb)
{
	m_bkRGB = rgb;
	Invalidate();
}
BOOL CMyEdit::Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID)
{
	return CEdit::Create(dwStyle, rect, pParentWnd, nID);
}
