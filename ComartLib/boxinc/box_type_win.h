


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifndef _BOX_TYPE_WIN_S6D_INC
#define _BOX_TYPE_WIN_S6D_INC

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef __cplusplus
//extern "C" {
//#endif






// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//*
//*
//*
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifndef	BOX_TYPE_WIN
#define BOX_TYPE_WIN


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#define	BOXINLINE				inline
#define BOXINLINE2
#define BOXPACKED




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
typedef __int64					ts64;
typedef int						ts32;
typedef short					ts16;
typedef char					ts08;

typedef unsigned __int64		tu64;
typedef unsigned int			tu32;
typedef unsigned short			tu16;
typedef unsigned char			tu08;




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifndef	BOX_STUINT_TYPE	
#define BOX_STUINT_TYPE

	#if defined(_WIN64)
	#pragma message("--- [box_type_win.h][64bit][stulong, stuint ]")
	typedef	unsigned long long		stulong;
	typedef			 long long		stslong;
	typedef unsigned __int64		stuint;
	typedef			 __int64		stsint;

	#else 
	#if defined(_WIN32)

	#pragma message("--- [box_type_win.h][32bit][stulong, stuint]")
	typedef unsigned long			stulong;
	typedef			 long			stslong;
	typedef unsigned int			stuint;
	typedef			 int			stsint;

	#else
	#error "COMPILER NOT SELECT"
	#endif

	#endif

#endif










// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
typedef		struct	TAG_RTSZ
{	LONG 	left, top, xz, yz;

}	RTSZ,	*PRTSZ;

#define	SET_RTSZ(a, b,c,d,e)	{	a.left = b;	 a.top = c;	 a.xz = d;		a.yz = e;	}
#define	SET_RECT(a, b,c,d,e)	{	a.left = b;	 a.top = c;	 a.right = d;  a.bottom = e;}
#define CPY_RECT(a, b)			{   CopyMemory(a, b, sizeof(RECT));	}


// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
#endif //BOX_TYPE_WIN
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-







// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef  __cplusplus
//}
//#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#endif // !defined(_BOX_TYPE_WIN_S6D_INC)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-












