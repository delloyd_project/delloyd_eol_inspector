
/* 	filename: 	hwif
 * **************************************************************************************
 *
 * 	created  :	2009/06/10
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 *
 *  version  :  1   [2009/06/10] : WIN7 64BIT / 32BIT COMPATABLE
 *				2	[2012/06/04] : new code apdn	
 *
 * **************************************************************************************
 */



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#if !defined(_AFX_FUNC_HWIF_d76_INC_)
#define _AFX_FUNC_HWIF_d76_INC_

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef __cplusplus
//extern "C" {
//#endif









// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*
//*	
//*
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
#include "ComArT3DI.H"











// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//*
//*	hwif				MACRO and TYPE and DECLARE
//*
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
#define HWI_MAX_BORAD					4
#define HWI_MAX_SDCH					64			
#define HWI_MAX_HDCH					16
#define HWI_MAX_VID_CH					80			//i>SDCH + HDCH
#define HWI_STA_SDCH					0
#define HWI_STA_HDCH					64 

#define HWI_MAX_VCAPTOVI_CH				4


#define HWI_MAX_AUD_CH					32
#define HWI_MAX_DIN_CH					16
#define HWI_MAX_DOU_CH					16

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
// use : m_stVCAPT_vidTBL[]
// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --
#define HWI_VIDTYPE_NONE				0
#define HWI_VIDTYPE_SD					1
#define HWI_VIDTYPE_SD					1
#define HWI_VIDTYPE_EFFIO				2
#define HWI_VIDTYPE_HD					3






// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * VCAPT
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#define HWI_MAX_COLOR_NAME				8
enum	
{	HWISD_BRI=0,	HWISD_CON,		HWISD_HUE,		HWISD_SAT,	
	HWIHD_BRI,		HWIHD_RED,		HWIHD_GRN,		HWIHD_BLU,
};


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * VCAPT
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#define HWI_MAX_VIDEXTTYPE				3
enum	
{	VET_NONE	=0,		
	VET_960H	=1,
	VET_EXSDI	=1,
	VET_CASDI	=2,				
};





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * VSTRM
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//
/* LATER
#define HWI_MAX_STRM_FILTR_NAME				8
enum	
{	HWISD_BRI=0,	HWISD_CON,		HWISD_HUE,		HWISD_SAT,	
	HWIHD_BRI,		HWIHD_RED,		HWIHD_GRN,		HWIHD_BLU,
};
*/




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * CA SDI
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#pragma pack(1)
typedef		struct	_t_casdi21
{
	union
	{	struct  
		{	DWORD	mem1;
			DWORD	mem2;

		}	mem32;

		struct 
		{	DWORD	size:24;		// casdi size
			

			DWORD	chip:4;			// stream chip
			DWORD	h264:1;			// h264:1, jpeg:0
			DWORD	ip:1;			// iframe:1, pframe:0
			DWORD	err:1;			// err:1, normal:0
			DWORD	fix0:1;			// fix 0


			DWORD	csum:16;		// csum no use
			DWORD	fps:8;			// fps
			DWORD	xor:8;			// head xor
		}	bit;
	}	u;


}	tcasdi2;
#pragma pack()



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
typedef stuint			(*EXT_FUNC)(stuint, stuint, stuint, stuint, stuint, stuint, stuint);


enum	DIO_TYPE		{ DIO_NO=0, DIO42, DIO44, DIO84, DIO88, DIO16, DIOFF };













// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// *	C/C++ CLASS (STRUCT) USE DEFINE
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//!		@class		
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
struct			T_DETECT
{			   ~T_DETECT()	{}	
				T_DETECT();			

	int 		FIND		(DWORD IN_nMODEL);

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
	DWORD		dwNumB;	
	
	DWORD		dwTYPE		[HWI_MAX_BORAD];
	DWORD		dwEXT		[HWI_MAX_BORAD];
	TCHAR		lpszNAME	[HWI_MAX_BORAD][128];

	
	DWORD		dtEnVLIVE	[HWI_MAX_BORAD];	
	DWORD		dtEnVCAPT	[HWI_MAX_BORAD];	
	DWORD		dtEnHCAPT	[HWI_MAX_BORAD];	
	DWORD		dtEnSD_VS	[HWI_MAX_BORAD];	
	DWORD		dtEnHD_VS	[HWI_MAX_BORAD];	

	DWORD		dtEnACAPT	[HWI_MAX_BORAD];	
	DWORD		dtEnAPLAY	[HWI_MAX_BORAD];
	DWORD		dtEnRUART	[HWI_MAX_BORAD];
	DWORD		dtEnSQUAD	[HWI_MAX_BORAD];

	DWORD		dtMustVID	[HWI_MAX_BORAD];

	tCArTBDDetailInfo		tDetail[HWI_MAX_BORAD];
};


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * T_DETECT	: Construction  / Destruction
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
inline	T_DETECT:: T_DETECT()
{
	dwNumB = 0;
	ZeroMemory(& (dwEXT      [0]), sizeof(dwEXT		));
	ZeroMemory(& (dwTYPE     [0]), sizeof(dwTYPE	));
	ZeroMemory(& (lpszNAME[0][0]), sizeof(lpszNAME	));
	
	ZeroMemory(& (dtEnVLIVE  [0]), sizeof(dtEnVLIVE	));
	ZeroMemory(& (dtEnVCAPT  [0]), sizeof(dtEnVCAPT	));
	ZeroMemory(& (dtEnHCAPT  [0]), sizeof(dtEnHCAPT	));
	ZeroMemory(& (dtEnSD_VS  [0]), sizeof(dtEnSD_VS	));
	ZeroMemory(& (dtEnHD_VS  [0]), sizeof(dtEnHD_VS	));
	
	ZeroMemory(& (dtEnACAPT  [0]), sizeof(dtEnACAPT	));
	ZeroMemory(& (dtEnAPLAY  [0]), sizeof(dtEnAPLAY	));
	ZeroMemory(& (dtEnRUART  [0]), sizeof(dtEnRUART	));
	ZeroMemory(& (dtEnSQUAD  [0]), sizeof(dtEnSQUAD	));

	ZeroMemory(& (dtMustVID  [0]), sizeof(dtMustVID	));
	ZeroMemory(& (tDetail    [0]), sizeof(tDetail	));
	
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
inline	int 	T_DETECT::FIND(DWORD IN_nMODEL)
{
	int		ni;
	
	for ( ni = 0; ni < (int)dwNumB; ni++ )
	{	
		if (dwTYPE[ni] == IN_nMODEL) 
			return ni;
	}
	return -1;
}


















// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// *	C/C++ CLASS (STRUCT) USE DEFINE
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//!		@class		
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
struct			 T_THREAD
{				 T_THREAD();
				~T_THREAD(){}			
	
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
	stuint 		stCH, sdBDINX;

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
//  audio / video only
// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
	DWORD		dwCH_XZ, dwCH_YZ, dwCH_YC;
	DWORD		dwCH_AUDSZ;

	int			nVidDMA_ACT;
	int			nAudDMA_ACT;

// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
	int			nThread_ACT;
	HANDLE		pcThread_ID;
	DWORD		dwThread_ID;		

	EXT_FUNC	vfExtFunc;
	void *		vpExtArg;

	void *		vpthis;

};


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * T_BDSTAT	: Construction  / Destruction
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
inline	T_THREAD:: T_THREAD()
{
	nVidDMA_ACT		= 0;
	nAudDMA_ACT		= 0;

	nThread_ACT		= 0;	
	vfExtFunc		= (EXT_FUNC)NULL;
	vpExtArg		= NULL;
	
	pcThread_ID		= NULL;
	dwThread_ID		= 0;
}











// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
// *	C/C++ CLASS (STRUCT) USE DEFINE
// *	
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
//!		@class		
//!					
// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
#define		HWI_FIND_ALL			900						//i:MAGIC, _DETECT 


DWORD	GetG_VCAPT_XZ		(DWORD IN_dw960H, DWORD IN_dwResConst);
DWORD	GetG_VCAPT_YZ		(DWORD IN_dwNTSC, DWORD IN_dwResConst);
DWORD	GetG_VCAPT_IP		(DWORD IN_dwResConst);
void	GetG_VCAPT_SliceTBL	(DWORD IN_dwNTSC, DWORD IN_dwUseCH, DWORD *OUT_dwCamFPSTBL);


DWORD	GetG_VSTRM_XZ		(DWORD IN_dw960H, DWORD IN_dwHDXZ, DWORD IN_dwResConst);
DWORD	GetG_VSTRM_YZ		(DWORD IN_dwNTSC, DWORD IN_dwHDYZ, DWORD IN_dwResConst);
DWORD	GetG_VSTRM_IP		(                 DWORD IN_dwHDIP, DWORD IN_dwResConst);

void	GetG_DQUAD_SCRN_SZ	(DWORD IN_dwVtyp, DWORD IN_dwMODE, RECT *io_rv);


DWORD	GetG_ACAPT_AudSiz	(DWORD IN_dwResConst);
DWORD	GetG_ACAPT_WakeTime	(DWORD IN_dwResConst);




































// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
class		cHWIF
{
public:
virtual	   ~cHWIF();
			cHWIF();

// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
public:
	int 	_DETECT (DWORD IN_dwTYPE );	
	
	DWORD	_GET_OPEN_FLAG	(DWORD IN_dwInx,   DWORD IN_dwVIDEO, DWORD IN_dwAUDIO, 
							 DWORD IN_dwAPLAY, DWORD IN_dwRUART, DWORD IN_dwSQUAD);


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	BOOL	_OPEN	(DWORD IN_dwNumB, DWORD IN_dwFLG, DWORD IN_dwNTSC, DWORD*IO_dwErrCode);
	void	_CLOSE	();


private:	
	void	BD_FEATURE(DWORD IN_dwNTSC);


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	void	_QUAD_SCRN		(DWORD in_dwstach, DWORD in_dwmode);
	void	_TVQUAD_SCRN	(DWORD in_dwstach, DWORD in_dwmode);



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	BOOL	_VSTRM_STA		(DWORD IN_dwCH, DWORD, DWORD, DWORD, DWORD, DWORD, DWORD, DWORD, DWORD);
	void	_VSTRM_STO		(DWORD IN_dwCH);



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	BOOL	_VCAPT_STA		(void *, void *, DWORD, DWORD, DWORD, DWORD, DWORD);
	void	_VCAPT_STO		(DWORD IN_dwCH);

	void	_VCAPT_VET		(DWORD IN_dwCH,  DWORD IN_dwEXT, DWORD IN_dwOVI);


	void	_VCAPT_GetFPS	(DWORD *IO_dtTBL);
	
	stuint	_VCAPT_frameChk (DWORD IN_dwCH,    DWORD IN_dtframeTBL[]);
	void	_VCAPT_frameWr	(DWORD IN_dwSTACH, DWORD IN_dwENDCH, DWORD IN_dtframeTBL[]);

	void	_VCAPT_colorWr	(DWORD IN_dwCH, DWORD IN_dwHWICNM, DWORD IN_dwValue);

private:	
	void	ADC_frameWait	(DWORD IN_dwCH);
	void	ADC_frameOn		(DWORD IN_dwCH);
	void	ADC_frameCtrl	(DWORD IN_dwCH);




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	BOOL	_ACAPT_STA	(void *IN_FUNC, void *IN_ARGS, DWORD IN_dwCH, DWORD IN_dwTRON, DWORD IN_dwSTRO);
	void	_ACAPT_STO	(DWORD IN_dwCH);
	
	void	_ACAPT_VOL	(DWORD IN_dwCH, DWORD IN_dwD);
	void	_ACAPT_AMP	(DWORD IN_dwCH, DWORD IN_dwD);
	void	_ACAPT_REF	(				DWORD IN_dwD);

	DWORD	_ACAPT_GET_BSZ   (DWORD IN_dwD);
	



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	int		_UART_RTS (DWORD  IN_dwEN);
	int 	_UART_CFG (DWORD  IN_dwBPS_inx, DWORD IN_dwPAR_inx);
	int 	_UART_TX  (BYTE * IN_bpTx,      DWORD IN_nTxSZ);

	BOOL 	_UART_RX_STA (void *IN_FUNC, void *IN_ARGS, DWORD IN_dwCH);
	void	_UART_RX_STO (DWORD IN_dwCH);



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	int		_DIO_DTECT(DIO_TYPE *IO_nMADIO, DIO_TYPE *IO_nSLDIO);
	int 	_DOUT	  (DWORD  IN_dwDO_bit);
	int 	_DIN	  (DWORD *IO_dwDI_bit);



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	int		_WDOG_EN  (DWORD IN_dwEN, DWORD IN_dwSEC);	
	int 	_WDOG_RST ();



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	int		_AUD_MUX(DWORD IN_dwEN, DWORD IN_dwCH, DWORD IN_dwVOL);
	int		_TV1_MUX(DWORD IN_dwEN, DWORD IN_dwCH, DWORD IN_dwResolCONST);
	int		_TV2_MUX(DWORD IN_dwEN, DWORD IN_dwCH);

	int		_TVX_OSD_WRT(DWORD IN_dwCH, DWORD IN_dwPOS, TCHAR *IN_szStr, DWORD IN_dwStrCnt);
	int		_TVX_OSD_CLR(DWORD IN_dwCH, DWORD IN_dwPOS, DWORD IN_dwStrCnt);
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	
	


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	BOOL	_VL_STATUS (DWORD *IO_dtVL);
	int		_HDV_STATUS(DWORD *IO_dtXZ, DWORD *IO_dtYZ, DWORD *IO_dtIP, 
						DWORD *IO_dtFPS,DWORD *IO_dtVTG);
	int		_SDI_ResolC(DWORD IN_dwCH);

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:
	
	int		_PCI_BANDWIDTH_CTRL(DWORD IN_nEN, DWORD IN_nNumB);


// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
private:	
	void	BD_ARG		 (DWORD IN_dwNTSC);
	
	

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
private:	
static 	DWORD	WINAPI	VidTHRD_FUNC (LPVOID	lpvParam);
static	DWORD	WINAPI	AudTHRD_FUNC (LPVOID	lpvParam);
static	DWORD	WINAPI	UartTHRD_FUNC(LPVOID	lpvParam);



// *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
public:
	T_DETECT	m_tDETECT;

	int 		m_nOPEN;	

	stuint		m_stSLCTInx;
	DWORD		m_dwSLCTMdl;
	stuint		m_stbd_opn_opt;						//i> bd open option flag

	DWORD		m_vidthrd_cnt;
	DWORD		m_audthrd_cnt;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 	
	//i>		feature
	DWORD		m_nEnVLIVE;
	DWORD		m_nEnVCAPT, m_nEnHCAPT, m_nEnSD_VS, m_nEnHD_VS;
	DWORD		m_nEnACAPT, m_nEnAPLAY, m_nEnRUART, m_nEnSQUAD;

	DWORD		m_npci_mdl_flg;

	DWORD		m_nahd_mdl_flg;						//i> AHD model flag
	DWORD		m_n4k_mdl_flg;						//i> EXDI 4K model flag

	stuint		m_tvosd_mdl_flg;					//i> TV OSD model flag
	stuint		m_stosd_pitch;						//i: tvosd width char 
	
	stuint		m_stvet_mdl_flg;					//i> Video Ext Type flag
	stuint		m_st960h_mdl_flg;					//i> 960H model flag
	stuint		m_exsdi_mdl_flg;					//i> 

	stuint		m_tvquad_mdl_flg;					//i> TV LIVE QUAD screen model flag
	stuint		m_tvquad_maxmode;					//i> TV LIVE QUAD screen MAX MODE

	stuint		m_dquadcap_mdl_flg;					//i> LIVE QUAD screen model flag
	stuint		m_dquadcap_chinx;					//i> LIVE QUAD screen CH
	stuint		m_dquadcap_maxch;					//i> LIVE QUAD screen CH  CNT
	stuint		m_dquadcap_maxmode;					//i> LIVE QUAD screen MAX MODE
	stuint		m_dquadcap_defmode;					//i> LIVE QUAD screen DEF MODE
	

	
	

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	stuint		m_h264ovi_mdl_flg;					//i> H264 OVER VIDEO flag
	struct		t_h264ovi_mdl_tag
	{	stuint	stch_sta;							//i> H264 OVER VIDEO CH START
		stuint	stch_cnt;							//i> H264 OVER VIDEO CH COUNT
		
		stuint	chsta_4vc[HWI_MAX_VCAPTOVI_CH];		//i> H264 OVER VIDEO NUM per CH
		stuint	chnum_4vc[HWI_MAX_VCAPTOVI_CH];		//i> H264 OVER VIDEO NUM per CH

	}	m_h264ovi;


	stuint		m_acapt_65k_flg;
	
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:			//i> video	
	
	DWORD		m_dwNTSC;	
	stuint		m_stpVL_O			[HWI_MAX_VID_CH];		//I: VIDEO LOSS

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:			//i> video capture

	stuint		m_stVCAPT_MaxSDCH;					//i>SD		CHANNEL
	stuint		m_stVCAPT_MaxHDCH;					//i>HD		CHANNEL	
	stuint		m_stVCAPT_MaxVICH;					//i>VIDEO	CHANNEL
	stuint		m_stVCAPT_MaxHDRAW;
	
	stuint		m_stVCAPT_staSDCH;
	stuint		m_stVCAPT_staHDCH;
	stuint		m_stVCAPT_staHDRAW;
	
	stuint		m_stVCAPT_MAX_GRP;					//i>Max GROUP adc(SD)
	stuint		m_stVCAPT_CH_pGRP;					//i>channel per group

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 	


	stuint		m_stBRI				[HWI_MAX_VID_CH	];
	stuint		m_stCON				[HWI_MAX_VID_CH	];
	stuint		m_stHUE				[HWI_MAX_VID_CH	];
	stuint		m_stSAT				[HWI_MAX_VID_CH	];	

	stuint		m_stRED				[HWI_MAX_VID_CH	];
	stuint		m_stGREEN			[HWI_MAX_VID_CH	];
	stuint		m_stBLUE			[HWI_MAX_VID_CH	];

	stuint		m_exsdi_chtbl[HWI_MAX_VID_CH];		//i> 960H select per CH

	stuint		m_stVCAPT_vexTBL	[HWI_MAX_VID_CH];		//i> video ext type select per CH

	stuint		m_stVCAPT_vidTBL	[HWI_MAX_VID_CH	];
	stuint		m_stVCAPT_ResTBL	[HWI_MAX_VID_CH	];
	stuint		m_stVCAPT_411TBL	[HWI_MAX_VID_CH	];	
	stuint		m_stVCAPT_FPSTBL	[HWI_MAX_VID_CH];		//i> total channel fram
	stuint		m_stVCAPT_divTBL	[HWI_MAX_VID_CH][32];



	stuint		m_stVCAPT_staTBL	[HWI_MAX_VID_CH];		//I: START STOP  	
	T_THREAD	m_tpVidTHRD			[HWI_MAX_VID_CH];		//I: THREAD  


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:			//i> video strm
	stuint		m_stVSTRM_MaxSDCH;
	stuint		m_stVSTRM_MaxHDCH;
	stuint		m_stVSTRM_MaxVICH;

	stuint		m_stVSTRM_staSDCH;
	stuint		m_stVSTRM_staHDCH;

	
	stuint		m_stVSTRM_resTBL	[HWI_MAX_VID_CH	];
	stuint		m_stVSTRM_fpsTBL	[HWI_MAX_VID_CH	];
	stuint		m_stVSTRM_prfTBL	[HWI_MAX_VID_CH	];
	stuint		m_stVSTRM_gopTBL	[HWI_MAX_VID_CH	];
	stuint		m_stVSTRM_cbrTBL	[HWI_MAX_VID_CH	];
	stuint		m_stVSTRM_qpvTBL	[HWI_MAX_VID_CH	];
	stuint		m_stVSTRM_minTBL	[HWI_MAX_VID_CH	];
	stuint		m_stVSTRM_maxTBL	[HWI_MAX_VID_CH	];

	stuint		m_stVSTRM_staTBL	[HWI_MAX_VID_CH];		//I: START   



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:			// audio	
	stuint		m_stACAPT_MaxCH;
	stuint		m_stACAPT_MaxHDCH;

	stuint		m_stMaxVolume;								//i> 
	stuint		m_stMaxPreAMP;
	stuint		m_stMaxRefer;
	stuint		m_stAudioRef;

	stuint		m_stAudResNoTBL [MAX_RESOLUTION_ACAPT_TRON];

	stuint		m_stpVolumeTBL	[HWI_MAX_AUD_CH];
	stuint		m_stpPreAmpTBL	[HWI_MAX_AUD_CH];
	stuint		m_stpAudResTBL	[HWI_MAX_AUD_CH];
	stuint		m_stpAudWakeTBL [HWI_MAX_AUD_CH];
	stuint		m_stpAudSizTBL	[HWI_MAX_AUD_CH];


	stuint		m_stpAudSTA		[HWI_MAX_AUD_CH];		//INF: THREAD
	T_THREAD	m_tpAudTHRD		[HWI_MAX_AUD_CH];		//INF: THREAD



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:			// uart
	DWORD		m_dwuart_BPS_inx;
	DWORD		m_dwuart_CFG_done;						//INF: uart CONFIG done

	stuint		m_stpUartSTA	[4];					//INF: THREAD
	T_THREAD	m_tpUartTHRD	[4];					//INF: THREAD


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:			// wdog
	stuint		m_stWDOG_En;
	stuint		m_stWDOG_Sec;


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
public:			// mux
	stuint		m_stAUDMux_En,	m_stAUDMux_CH,	m_stAUDMux_Vol;
	stuint		m_stTV1Mux_En,	m_stTV1Mux_CH;
	stuint		m_stTV2Mux_En,	m_stTV2Mux_CH;

	

};








// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef  __cplusplus
//}
//#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#endif // !defined(_AFX_FUNC_HWIF_d76_INC_)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-


