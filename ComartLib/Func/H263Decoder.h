﻿#ifndef __H263DECODER_H__
#define __H263DECODER_H__

#include <windows.h>

DECLARE_HANDLE(HBlueBirdDecoder);

class __declspec(dllexport) CBlueBirdDecoder
{
public:
	virtual VOID BlueBirdDec_Initialize(BOOL isPreviousComparison = TRUE) = 0;

	virtual VOID BlueBirdDec_Decode(BYTE *pSrc, BYTE *pDest, int nSrcSize) = 0;

	virtual VOID BlueBirdDec_Release() = 0;
};

extern "C" __declspec(dllexport) CBlueBirdDecoder *CreateBlueBirdDecoder();

extern "C" __declspec(dllexport) VOID BlueBirdDec_Initialize(HBlueBirdDecoder hDec, BOOL isPreviousComparison = TRUE);
extern "C" __declspec(dllexport) VOID BlueBirdDec_Decode(HBlueBirdDecoder hDec, BYTE *pSrc, BYTE *pDest, int nSrcSize);
extern "C" __declspec(dllexport) VOID BlueBirdDec_Release(HBlueBirdDecoder hDec);
extern "C" __declspec(dllexport) HBlueBirdDecoder BlueBirdDec_Create();

#endif