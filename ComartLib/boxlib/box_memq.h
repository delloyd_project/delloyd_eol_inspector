
/* 	filename : 	memory q
 *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
 *
 * 	created  :	2005/11/30 
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 *
 *				0	reference code [   ]
 *  version  :  1   [2005/11/30] : START CODE -> CLASS MADE / CODE WRITING..
 *                               : original code : nios II (GCC no MMU) 
 *  version  :  2   [2017/11/20] : code alignment (organization)
 *
 *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
 */



// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifndef _BOX_MEM_Q_S_
#define _BOX_MEM_Q_S_

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef __cplusplus
//extern "C" {
//#endif





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// *	
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#define BOX_MEMQ_SIZE(a)		( (a) + ((a/1000) *8) + 32)	//MEAN: * 8 => DWORD 2EA














// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *	C/C++ CLASS (STRUCT) USE DEFINE
// *	@class		
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-


class			box_memq
{
public:			box_memq(); 
			   ~box_memq();
		   
// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
public:	
	int		    t_new(int in_nid, int in_npool_sz, int in_npool_cnt, int in_npop_bufsz);
	void		t_del();
	
// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
private:	
	int			m_nid;
	HANDLE		m_hmtx;					//i: re-entry, item protect 
	
	
// - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
private:
	LPBYTE		m_bpool_ptr;			//i: q memory  pool
	int		   *m_pofst_tbl;			//i: q memory, offset table
	int		   *m_psize_tbl;			//i: q memory, size   table

	int			m_npool_sz;				//i: q memory  size
	int			m_npool_cnt;			//i: q memory  count	


	


// = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- =
public:
	int			get_que_cnt()	{	return	m_nq_cnt;		}
	int			get_que_siz()	{	return	m_nq_size;		}
	int			get_que_per()	{	return ( (m_npool_sz)? ((m_nq_size * 100) / m_npool_sz) : 0);	}

	int			get_tail_siz()	{	return	m_psize_tbl[m_nq_tail];	}
	

	void		flush();
	void		push	(BYTE * in_bpmem, int in_nbufsz);
	int 		pop		(BYTE **io_bpmemout);

	int			m_npop_buf_sz;
	LPBYTE		m_bpop_buf;	

// = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- = -- =
private:
	void		head_inc(int in_nsize);
	void		tail_inc();
	
	int			m_nq_head,	m_nq_tail;
	int			m_nq_cnt,	m_nq_size,	m_nq_rest, m_nq_ofst;	


};








// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * C INTERFACE DEFINE
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//#ifdef  __cplusplus
//}
//#endif

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// * FILE RE-INCLUDE CHECK
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#endif // !defined(_BOX_MEM_Q_S_)
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-


















