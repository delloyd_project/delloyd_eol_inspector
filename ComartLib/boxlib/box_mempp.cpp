
/* 	filename : 	memory pingpong
 *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
 *
 * 	created  :	2017/11/21 
 *	author   :	boxucom@gmail.com
 * 	file base:	windows 7 (32 / 64)
 * 
 *
 *				0	reference code [   ]
 *  version  :  1   [2005/11/30] : START CODE -> CLASS MADE / CODE WRITING..
 *                         
 *
 *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
 */

#include "StdAfx.h"
#include "..\boxinc\box_type_win.h"
#include "..\boxinc\box_log_win.h"

#include "stools.h"
#include "box_mempp.h"





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
//del>#pragma optimize("", off)

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif










// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
// *
// * class	
// *
// *
// *
// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-







// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
box_mempp::box_mempp()
{
	m_bpool_buf	= NULL;
	m_npool_sz	= 0;
	m_bpopl_buf  = NULL;

	ZeroMemory(m_bppon_ptr, sizeof(m_bppon_ptr));
	m_nppon_sz	= 0;
	
	m_hmtx		= NULL;

	m_ndbgmsg	= 0x1234;

}

box_mempp::~box_mempp() 
{	
	t_del();
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int		box_mempp::t_new
(int	in_nid,				// id>
 int	in_npool_sz,		// pool size : 
 int	in_nppon_cnt,		// ping-pong count

 int	in_nsync_use,		// sync trigger use
 int	in_nsync_sz,		// sync trigger size
 int	in_nsync_cnt,		// sync trigger cnt
 int	in_nsync_tick		// sync trigger tick
)
{	
	boxreturn((in_npool_sz  <= 256),		  -1, DBGTR, _T("too small sz: %d"), in_npool_sz);
	boxreturn((in_nppon_cnt > BOX_MEMPP_CNT), -1, DBGTR, _T("too big cnt : %d"), in_nppon_cnt);
		

	m_nid	= in_nid;
	m_hmtx	= CreateMutex(NULL, FALSE, NULL);


	m_bpool_buf		= (LPBYTE)_aligned_malloc(in_npool_sz + 256, 32);
	m_npool_sz		= in_npool_sz;

	m_nppon_cnt		= in_nppon_cnt;
	m_nppon_sz		= in_npool_sz / in_nppon_cnt;
	for (int ni = 0; ni < in_nppon_cnt; ni++)
	{	m_bppon_ptr[ni] = m_bpool_buf + (ni * m_nppon_sz);
	}

	m_bpopl_buf		= (LPBYTE)_aligned_malloc(m_nppon_sz + 128, 32);



	m_nsync_use		= in_nsync_use;
	m_nsync_sz		= in_nsync_sz;
	m_nsync_cnt		= in_nsync_cnt;
	m_nsync_tick	= in_nsync_tick;
	
	flush();	

	m_ndbgmsg	= 0;	//0x1234;

	return 1;
}

void	box_mempp::t_del()
{
	m_npool_sz	=	0;	
	if (m_bpool_buf)
	{	_aligned_free(m_bpool_buf);
		m_bpool_buf	= NULL;				
	}	

	if (m_bpopl_buf)
	{	_aligned_free(m_bpopl_buf);
		m_bpopl_buf = NULL;
	}

	m_nppon_sz  = 0;
	for (int ni = 0; ni < BOX_MEMPP_CNT; ni++)
	{	m_bppon_ptr[ni] = NULL; 
	}

	if (m_hmtx)
	{	CloseHandle(m_hmtx);
		m_hmtx = NULL;
	}

	
}





// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	box_mempp::flush()
{
	auto_mtx	mtx(m_hmtx);

	m_nq_hdinx	= 0;
	m_nq_tlinx	= 0;
	m_nq_cnt	= 0;

	
	ZeroMemory(m_bpool_buf, m_npool_sz);
	
	ZeroMemory(m_uq_trgtbl, sizeof(m_uq_trgtbl));
	ZeroMemory(m_nq_siztbl, sizeof(m_nq_siztbl));
	ZeroMemory(m_nq_cnttbl, sizeof(m_nq_cnttbl));
	ZeroMemory(m_uq_tcktbl, sizeof(m_uq_tcktbl));
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	box_mempp::head_inc(DWORD in_utrg)
{
	DWORD	oldinx = m_nq_hdinx;

	m_uq_trgtbl[m_nq_hdinx] = in_utrg;

	m_nq_hdinx++;
	m_nq_hdinx %= m_nppon_cnt;

	m_uq_trgtbl[m_nq_hdinx] = 0;
	m_nq_siztbl[m_nq_hdinx] = 0;	
	m_nq_cnttbl[m_nq_hdinx] = 0;	
	m_uq_tcktbl[m_nq_hdinx] = 0;	

	if (m_nq_cnt < m_nppon_cnt) m_nq_cnt++;
	else 
	{	boxassert(1, 500, _T("box_mempp head_inc over-run"));
		flush();
	}

	boxlog((m_ndbgmsg), DBGTR, _T("sync(o:%d_n:%d_tc:%d) trg:%d  sztbl:%d, cntbl:%d"), 
			oldinx, m_nq_hdinx, m_nq_cnt, in_utrg, m_nq_siztbl[m_nq_hdinx], m_nq_cnttbl[m_nq_hdinx]);

}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	box_mempp::tail_inc()
{
	DWORD	oldinx = m_nq_tlinx;

	m_uq_trgtbl[m_nq_tlinx] = 0;
	m_nq_siztbl[m_nq_tlinx] = 0;	
	m_nq_cnttbl[m_nq_tlinx] = 0;	
	m_uq_tcktbl[m_nq_tlinx] = 0;	

	m_nq_tlinx++;
	m_nq_tlinx %= m_nppon_cnt;

	if (m_nq_cnt) m_nq_cnt--;
	else 
	{	boxassert(1, 500, _T("box_mempp tail_inc over-run"));
		flush();
	}

	boxlog((m_ndbgmsg), DBGTR, _T("delx(o:%d_n:%d_tc:%d sztbl:%d, cntbl:%d"), 
			oldinx, m_nq_tlinx, m_nq_cnt, m_nq_siztbl[m_nq_tlinx], m_nq_cnttbl[m_nq_tlinx]);
}




// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	box_mempp::push(BYTE *in_bpmem, int in_nbufsz)
{
	auto_mtx	mtx(m_hmtx);
	
	boxreturn0((in_bpmem  == 	  NULL), DBGTR, _T("box_memq::push() input ptr is null"));
	boxreturn0((in_nbufsz <=		 0), DBGTR, _T("box_memq::push() inavlid size"));
	boxreturn0((in_nbufsz > m_nppon_sz), DBGTR, _T("box_memq::push() too big size"));
	boxreturn0((in_nbufsz > m_nppon_sz), DBGTR, _T("box_memq::push() too big size"));

	boxreturn0( ((m_nq_siztbl[m_nq_hdinx] + in_nbufsz) > m_nppon_sz), 
				DBGTR, _T("box_memq::push() code invalid.. use xxx()") );



	boxlog((m_ndbgmsg), DBGTR, _T("push(%d_t:%d) addr:%llx,sz:%7d, sztbl:%8d, cntbl:%4d"), 
					  m_nq_hdinx, m_nq_cnt, in_bpmem, in_nbufsz, 
					  m_nq_siztbl[m_nq_hdinx], m_nq_cnttbl[m_nq_hdinx]);
	
	
	LPBYTE	bpdstptr;

	bpdstptr = m_bppon_ptr[m_nq_hdinx];
	bpdstptr+= m_nq_siztbl[m_nq_hdinx];

	CopyMemory(bpdstptr, in_bpmem, in_nbufsz);

	m_nq_siztbl[m_nq_hdinx] += in_nbufsz;	
	m_nq_cnttbl[m_nq_hdinx]++;

	DWORD	ucurtick = GetTickCount();
	if ((m_uq_tcktbl[m_nq_hdinx] == 0) || (ucurtick < m_uq_tcktbl[m_nq_hdinx]))
	{	m_uq_tcktbl[m_nq_hdinx] = ucurtick;
	}



}

// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DWORD		box_mempp::chk_trg(int in_nbufsz)
{	
	auto_mtx	mtx(m_hmtx);


	if (m_nsync_use == 0) return 0;

	if ((m_nq_siztbl[m_nq_hdinx] + in_nbufsz) > m_nppon_sz)	
		return SYNC_TRG_INX;

	if (m_nsync_cnt)
	{	if (m_nq_cnttbl[m_nq_hdinx] >= m_nsync_cnt)
			return SYNC_TRG_CNT;
	}

	if (m_nsync_sz ) 
	{	if ( (m_nq_siztbl[m_nq_hdinx]+in_nbufsz) >= m_nsync_sz ) 
			return SYNC_TRG_SIZE;
	}

	if (m_nsync_tick)
	{	DWORD	ucurtick = GetTickCount();
		if ((m_uq_tcktbl[m_nq_hdinx]) && 			
			(ucurtick >=  m_uq_tcktbl[m_nq_hdinx]) && 
			(ucurtick >= (m_uq_tcktbl[m_nq_hdinx]+m_nsync_tick)) )
		{	return SYNC_TRG_TICK;
		}		
	}



	return 0;
}


// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
void	box_mempp::set_trg(DWORD in_utrg)
{	
	auto_mtx	mtx(m_hmtx);
	head_inc(in_utrg);
}








// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
int 	box_mempp::popl(BYTE **io_bpmemout)
{	
	auto_mtx	mtx(m_hmtx);

	int		nrv = 0;
	boxreturn((io_bpmemout == NULL), -1, DBGTR, _T("invalid arg ptr"));

	if ((m_nq_siztbl[m_nq_tlinx] == 0) && (m_nq_cnt == 0)) return 0;

	
	nrv			 = m_nq_siztbl[m_nq_tlinx];
	*io_bpmemout = m_bpopl_buf;
	CopyMemory(m_bpopl_buf, m_bppon_ptr[m_nq_tlinx], nrv);
	
	tail_inc();
	
	return nrv;
}






// *---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*---*-
DWORD		box_mempp::get_trg()
{
	DWORD	urv = 0;
	for (int ni = 0; ni < m_nppon_cnt; ni++)
	{	urv |= m_uq_trgtbl[ni];
	}

	return urv;
}


















