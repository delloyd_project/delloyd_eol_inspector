﻿//*****************************************************************************
// Filename	: 	CatWrapper.h
// Created	:	2016/2/11 - 15:07
// Modified	:	2016/2/11 - 15:07
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef CatWrapper_h__
#define CatWrapper_h__


#pragma once
#include "boxlib\\stools.h"

#define HDCAPT_CODE_MAXCH		8
#define SDCAPT_CODE_MAXCH		32
#define VDCAPT_CODE_MAXCH		40

#define MAX_VIDEO_TEST_CNT		4
//#define	USE_ADJUST_RGB



enum enGrabberBoardType
{
	GBT_XECAP_50B,		// 기존 win 32 구형
	GBT_XECAP_400EF,	// 기존 win 32 신형
	GBT_XECAP_400EH,	// 신규 win 64 신형
};


typedef struct tagRGB32TRI {
	UINT    rgbBlue;
	UINT    rgbGreen;
	UINT    rgbRed;
} RGB32TRI;

typedef struct _tag_VideoRGB_NTSC
{
	// 체크 플래그
	BOOL		m_bStore;

	DWORD		m_dwWidth;
	DWORD		m_dwHeight;
	DWORD		m_dwSize;

	RGBQUAD**	m_pMatRGB;

#ifdef USE_ADJUST_RGB
	RGB32TRI**	m_pMatRGBTmp;
	BYTE		m_TempCnt;
#endif

	_tag_VideoRGB_NTSC()
	{
		m_dwWidth	= 720;
		m_dwHeight	= 480;
		m_dwSize	= 0;

		m_pMatRGB	= NULL;
		m_bStore	= FALSE;

#ifdef USE_ADJUST_RGB
		m_pMatRGBTmp	= NULL;
		m_TempCnt		= 0;
#endif
	};

	~_tag_VideoRGB_NTSC()
	{
		ReleaseMem();
	};

	void AssignMem(DWORD dwSizeX, DWORD dwSizeY)
	{
		m_dwWidth = dwSizeX;
		m_dwHeight = dwSizeY;

		if (m_dwSize != (dwSizeX * dwSizeY * 4))
		{
			ReleaseMem();

			m_dwSize = dwSizeX * dwSizeY * 4;
		}

		if (NULL == m_pMatRGB)
		{
			m_pMatRGB = new RGBQUAD*[dwSizeY];
			m_pMatRGB[0] = new RGBQUAD[dwSizeX * dwSizeY];
			for (DWORD dwIdx = 1; dwIdx < dwSizeY; dwIdx++)
			{
				m_pMatRGB[dwIdx] = m_pMatRGB[dwIdx - 1] + dwSizeX;
			}
		}

#ifdef USE_ADJUST_RGB
		if (NULL == m_pMatRGBTmp)
		{
			m_pMatRGBTmp = new RGB32TRI*[dwSizeY];
			m_pMatRGBTmp[0] = new RGB32TRI[dwSizeX * dwSizeY];
			for (DWORD dwIdx = 1; dwIdx < dwSizeY; dwIdx++)
			{
				m_pMatRGBTmp[dwIdx] = m_pMatRGBTmp[dwIdx - 1] + dwSizeX;
			}
		}
#endif
	};

	void ReleaseMem()
	{
		if (NULL != m_pMatRGB)
		{
			delete[] m_pMatRGB[0];
			delete[] m_pMatRGB;

			m_pMatRGB = NULL;
		}

#ifdef USE_ADJUST_RGB
 		if (NULL != m_pMatRGBTmp)
 		{
 			delete[] m_pMatRGBTmp[0];
 			delete[] m_pMatRGBTmp;
 
 			m_pMatRGBTmp = NULL;
 		}
#endif
	};
}ST_VideoRGB_NTSC, *PST_VideoRGB_NTSC;

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
class CCatWrapper
{
public:
	CCatWrapper();
	~CCatWrapper();


#ifdef _WIN64	
	typedef	unsigned long long				stulong;
	typedef			 long long				stslong;
	typedef unsigned __int64				stuint;
	typedef			 __int64				stsint;

//#pragma message("--- [ComArT3DI.H : COMPILER 64bit : stulong, stuint] ")

#elif  _WIN32
	typedef unsigned long					stulong;
	typedef			 long					stslong;
	typedef unsigned int					stuint;
	typedef			 int					stsint;

//#pragma message("--- [ComArT3DI.H : COMPILER 32bit : stulong, stuint] ")

#else
#error "COMPILER NOT SELECT"
#endif

	// 타이머
	HANDLE			m_hTimer_Dev_Mon;
	HANDLE			m_hTimerQueue;
	DWORD			m_dwCycle_Dev_Mon;
	void			CreateTimerQueue_Mon();
	void			DeleteTimerQueue_Mon();
	void			CreateTimer_Dev_Mon();
	void			DeleteTimer_Dev_Mon();
	static VOID CALLBACK	TimerRoutine_Dev_Mon(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired);
	void			Monitor_Dev();
	
protected:

	struct _tag_VideoInform
	{
		DWORD			dwXZ, dwYZ, dw411;
		UINT64			llcapture_cnt;

		DWORD			dwFileOn;
		FPSchecker		tFPS;

		DWORD			dwcvrt_flg;
		DWORD			dwcvrt_type;
		aligmem			amem;
		aligmem		   	anv12mem;

		_tag_VideoInform()
		{
			dwXZ			= 0;
			dwYZ			= 0;
			dw411			= 0;
			llcapture_cnt	= 0;
			dwFileOn		= 0;
			dwcvrt_flg		= 0;
			dwcvrt_type		= 0;
		}
	}m_tVIDI[VDCAPT_CODE_MAXCH];

	DWORD			m_dwBDInx;		// CAT3D 보드 인덱스
	DWORD			m_dwBDID;		// CAT3D 보드 ID
	DWORD			m_dwNTSC;		// NTSC: 1 (NTSC선택)
	DWORD			m_dwHD1orSD0;	// SD: 0 / HD: 1 (화질 선택)
	DWORD			m_dwVS1orVC0;

	DWORD			m_dwBDFuncVIDEO;
	DWORD			m_dwBDFuncVCAPT;
	DWORD			m_dwBDFuncHCAPT;
	DWORD			m_dwBDFuncSD_VS;
	DWORD			m_dwBDFuncHD_VS;

	DWORD			m_dwBDFuncACAPT;
	DWORD			m_dwBDFuncAPLAY;
	DWORD			m_dwBDFuncRUART;
	DWORD			m_dwBDFuncSQUAD;
	BOOL			m_blBDOpenFlag;
	DWORD			m_dtVL_Status[VDCAPT_CODE_MAXCH]; // 카메라 영상 상태 

	UINT			m_nUseChannelCount	= 1;


	BITMAPINFO	m_biInf;
	LPBYTE		m_lpbRGB = NULL;
	DWORD		m_dwRGBSZ;

	aligmem		m_amem;
	LPBYTE pData = NULL;

	BOOL	m_bLoop = FALSE;

public:
	// 초기화 루틴
	BOOL		Search_Cat3d	();	// CAT3D 장비 검색

	void		Select_Board	(__in DWORD	IN_dwInx);

	void		Init_Board		();
	BOOL		IsOpen_Board	();

	BOOL		Open_Board		(__in DWORD IN_dwNTSC);
	void		Close_Board		();
	
	void		Init_Begin		();
	void		Final_End		();

	BOOL		Start_Capture	(__in DWORD IN_dwCH);
	void get_flag_4squad(DWORD IN_dwCH, DWORD IN_nin411);
	void		Stop_Capture(__in DWORD IN_dwCH);
	void		Start_CaptureAll();
	void		Stop_CaptureAll	();
	
	// 영상 수신 이벤트
	static UINT	CBFunc_VCEvent	(__in PVOID IN_vpArg, __in UINT IN_stCH, __in UINT IN_stAddr, __in UINT IN_stXZ, __in UINT IN_stYZ, __in UINT IN_st411);

	static UINT OnMsg_VCEvent(void *IN_vpthis, stuint IN_stCH, stuint IN_stAddr, stuint IN_stXZ, stuint IN_stYZ, stuint IN_st411);

	void Evnt_DCWndDraw(DWORD IN_dwCH, LPBYTE IN_bpAddr);
	void render(DWORD IN_dwCH, DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_lpbYC422);
	void RGBCONV(DWORD IN_dwCH, DWORD IN_dwXZ, DWORD IN_dwYZ, DWORD IN_dw4CC, LPBYTE IN_lpbYC422);
	BYTE * Evnt_cvrt_nv12(DWORD IN_dwCH, LPBYTE IN_bpAddr);
	LPBYTE		Event_Conv422(__in DWORD IN_dwCH, __in LPBYTE IN_bpAddr);
	void		Event_ProcRPS	(__in DWORD IN_dwCH);
	void		Event_DCWndDraw	(__in DWORD IN_dwCH, __in LPBYTE IN_bpAddr);

	// 수신된 영상을 RGB32로 바꾼 데이터
	ST_VideoRGB_NTSC		m_stRGB[VDCAPT_CODE_MAXCH];
	
	void		DeleteEvenVideo		(__in DWORD IN_dwCH, __inout LPBYTE lpVideo);
	BOOL		Adjust_RGB32		(__in DWORD IN_dwCH);
	void		Event_Y41PtoRGB32	(__in DWORD IN_dwCH, __in LPBYTE IN_bpAddr);
	void		Event_Y41PtoRGB32_T	(__in DWORD IN_dwCH, __in LPBYTE IN_bpAddr);

	DWORD		m_dwstaVICH;	// Start
	DWORD		m_dwendVICH;	// Stop
	DWORD		m_dwMaxVICH;
	DWORD		m_dwMaxHDCH;
	DWORD		m_dwMaxSDCH;

	DWORD		m_dt960InpTBL[VDCAPT_CODE_MAXCH];
	DWORD		m_dtFPSInpTBL[VDCAPT_CODE_MAXCH];
	DWORD		m_dtRTOInpTBL[VDCAPT_CODE_MAXCH];
	DWORD		m_dt411InpTBL[VDCAPT_CODE_MAXCH];
	DWORD		m_dtCNMInpTBL[VDCAPT_CODE_MAXCH];
	DWORD		m_dtColInpTBL[VDCAPT_CODE_MAXCH];

 	DWORD		m_dtColBriTBL[VDCAPT_CODE_MAXCH];
 	DWORD		m_dtColConTBL[VDCAPT_CODE_MAXCH];
 	DWORD		m_dtColHueTBL[VDCAPT_CODE_MAXCH];
 	DWORD		m_dtColSatTBL[VDCAPT_CODE_MAXCH];
 	DWORD		m_dtColShaTBL[VDCAPT_CODE_MAXCH];
 	DWORD		m_dtColRedTBL[VDCAPT_CODE_MAXCH];
 	DWORD		m_dtColGrnTBL[VDCAPT_CODE_MAXCH];
 	DWORD		m_dtColBluTBL[VDCAPT_CODE_MAXCH];

	DWORD		m_dtResolCONST	[VDCAPT_CODE_MAXCH];
	DWORD		m_dtmtrxResolC	[VDCAPT_CODE_MAXCH];
	DWORD		m_dtRPSOutTBL	[VDCAPT_CODE_MAXCH]; // Run FPS

	DWORD		m_dtStartTBL	[VDCAPT_CODE_MAXCH]; // Start flag
	DWORD		m_dtVLossTBL	[VDCAPT_CODE_MAXCH]; //-> 1이면 Open된 채널
	DWORD		m_dtProcSkipTBL [VDCAPT_CODE_MAXCH];

 	DWORD		m_dwDCtxWnd_flg; // DC

	HWND		m_hWndOwner;				 // 오너 윈도우 핸들
	DWORD		m_dwSelectedCh;				 // 윈도우 화면에 출력할 채널
	UINT		m_nWM_ChgStatus;			 // 카메라 영상 상태 변경시 처리 메세지
	UINT		m_nWM_RecvVideo;			 // 비데오 영상 수신 메세지
	
	enGrabberBoardType GrabberBoardType;

	void		SetGrabberBoardType(enGrabberBoardType enType)
	{
		GrabberBoardType = enType;
	};

	
	// 오너 윈도우 핸들 설정
	void		SetOwner(HWND hOwner)
	{
		m_hWndOwner = hOwner;
	};

	// 카메라 영상 상태 변경시 처리 메세지
	void		SetWM_ChgStatus(UINT nWinMsg)
	{
		m_nWM_ChgStatus = nWinMsg;
	};

	// 비데오 영상 수신 메세지
	void		SetWM_RecvVideo(UINT nWinMsg)
	{
		m_nWM_RecvVideo = nWinMsg;
	};

	// 영상 출력용 카메라 선택
	void		SelectCamera(DWORD dwCh)
	{
		m_dwSelectedCh = dwCh;
	};

	// 카메라 영상 상태 구하기
	DWORD*		GetStatus()
	{
		return m_dtVLossTBL;
	};

	BOOL		GetStatus(UINT nChIdx)
	{
		return m_dtVLossTBL[nChIdx];
	};

	// 수신된 영상 데이터의 포인터 리터 (영상 검사 용도)
	ST_VideoRGB_NTSC*	GetRecvRGBData(DWORD dwCh)
	{
		if (dwCh < VDCAPT_CODE_MAXCH)
			return &m_stRGB[dwCh];
		else
			return NULL;
	}

	BOOL		IsConnect()
	{
		UINT nCount = 0;
		for (int iCnt = 0; iCnt < VDCAPT_CODE_MAXCH; iCnt++)
		{
			if (1 == m_dtVLossTBL[iCnt])
				++nCount;
		}

		if (0 < nCount)
			return TRUE;
		else
			return FALSE;
	};

	// 사용할 보드 선택
	UINT		m_nModelID;
	void		UseBoard(UINT nModelID)
	{
		m_nModelID = nModelID;
	};


	void	SetBrightnessContrast(BYTE Bright, BYTE Constrast);
	void	SetFPS();

	void	Set_UseChannelCount(__in UINT nCount)
	{
		m_nUseChannelCount = nCount;
	}

	// *** Test
 	void	Test_Video(UINT nChIdx)
 	{
 		m_stRGB[nChIdx].AssignMem(720, 480);
 	};

};

#endif // CatWrapper_h__

