﻿#ifndef Def_IRFilter_h__
#define Def_IRFilter_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"


typedef struct _tag_IRFilter_Op
{
	double dThreshold;
	double dThr_Env;

	_tag_IRFilter_Op()
	{
		dThreshold = 0;
		dThr_Env = 0;
	};

	_tag_IRFilter_Op& operator= (_tag_IRFilter_Op& ref)
	{
		dThreshold = ref.dThreshold;
		dThr_Env = ref.dThr_Env;

		return *this;
	};

}ST_IRFilter_Opt, *PST_IRFilter_Opt;

typedef struct _tag_IRFilter_Data
{
	// 최종 결과
	UINT nResult;

	double dValue;
	double dValue_Env;


	void Reset()
	{
		nResult = 3;
		dValue = 0;
		dValue_Env = 0;
	};	

	_tag_IRFilter_Data()
	{
		Reset();
	};

	_tag_IRFilter_Data& operator= (_tag_IRFilter_Data& ref)
	{
		nResult = ref.nResult;
		dValue = ref.dValue;
		dValue_Env = ref.dValue_Env;

		return *this;
	};

}ST_IRFilter_Result, *PST_IRFilter_Result;


#endif // Def_IRFilter_h__