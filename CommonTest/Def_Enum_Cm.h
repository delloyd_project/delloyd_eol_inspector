﻿//*****************************************************************************
// Filename	: Def_Enum_Cm.h
// Created	: 2010/11/23
// Modified	: 2016/06/30
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef Def_Enum_Cm_h__
#define Def_Enum_Cm_h__

#include <afxwin.h>

#define HIGH_CLIP(x,limit) (((x)>(limit)) ? (limit): (x))
#define LOW_CLIP(x,limit) (((x)<(limit)) ? (limit): (x))
#define CLIP(x,l,h) HIGH_CLIP(LOW_CLIP(x,l),h)

// ------------------------------------------------------------
// 시스템 운용모드
//------------------------------------------------------------
// typedef enum
// {
// 	SysMode_Lot = 0,
// 	SysMode_Engineer,
// 	SysMode_Mes
// }enumSysMode;

//-------------------------------------------------------------------
// UI 관련
//-------------------------------------------------------------------


enum enLabelDataType
{
	enLabelDataType_Model,
	enLabelDataType_Product,
	enLabelDataType_HWVer,
	enLabelDataType_SWVer,
	enLabelDataType_Date,
	enLabelDataType_Barcode,
	enLabelDataType_Max,
};

static LPCTSTR g_szLabelDataType[] =
{
	_T("Model"),
	_T("Product"),
	_T("HW Version"),
	_T("SW Version"),
	_T("Date"),
	_T("Barcode"),
	NULL
};


// 로그 탭
typedef enum
{
	LOGTYPE_NORMAL = 0,	// 기본
	LOGTYPE_GUI,		// UI 관련
	LOGTYPE_COM,		// 통신 관련
	LOGTYPE_OPR,		// 
	LOGTYPE_MAX,
}enumLogType;

static LPCTSTR lpszLogType[] = { _T(""), _T("GUI"), _T("COM"), _T("OPR"), NULL };

// 통신 연결 상태
typedef enum enCommStatusType
{
	COMM_STATUS_NOTCONNECTED = 0,
	COMM_STATUS_CONNECT,
	COMM_STATUS_DISCONNECT,
	COMM_STATUS_ERROR,
	//COMM_STATUS_WARNING,
	//COMM_STATUS_EXECUTE,
	//COMM_STATUS_PAUSE,
	COMM_STATUS_SYNC_OK,
	COMM_STATUS_NO_ACK,
};

static LPCTSTR lpszCommStatus[] = 
{
	_T("미 연결"),
	_T("연결"),
	_T("연결 해제"),
	_T("Err"),
	//_T("경고"),
	//_T("실행"),
	//_T("일시정지"),
	_T("싱크 완료"),
	_T("No ACK"),
	NULL
};

static COLORREF	clrJudgeColor[] =
{
	RGB(0xEE, 0x50, 0x50),	// NG 
	RGB(0x90, 0xEE, 0x90),	// OK
	RGB(0xFF, 0xFF, 0xFF),	// Wait	
	RGB(0xFF, 0xFF, 0x00),	// Error
	RGB(0xAA, 0xAA, 0xAA),	// No Data
	RGB(0x77, 0x77, 0x77),	// Not Used
};

typedef enum enJudgment
{
	JUDGE_NG		= 0,
	JUDGE_OK		= 1,
	JUDGE_EnumMax,
};

typedef enum enModelStatus
{
	LOT_Start = 0,
	LOT_End,
	Model_Change,
	ManualMode,
	AutoMode,
	MESMode,
	MASTER_Start,
	MASTER_End,
};

typedef enum enBitStatus
{
	Bit_Clear	= 0,
	Bit_Set		= 1,
	Bit_EnumMax,
};

typedef enum enPowerOnOff
{
	Power_Off,
	Power_On,
};

typedef enum enGrabType
{
	GrabType_NTSC,
};

static LPCTSTR g_szGrabType[] =
{
	_T("Analog"),
	_T("Digital"),
	NULL
};

//---------------------------------------------------------
// Access 모드 설정값
typedef enum enPermissionMode
{
	Permission_Operator,		// 옵션 보기/수정 불가
	Permission_Manager,			// 옵션 보기 가능, 수정 불가
	Permission_Administrator,	// 옵션 보기/수정 가능
	Permission_Engineer,		// 개발자모드
	Permission_MES,		// 개발자모드
	Permission_CNC,		// 개발자모드
	Permission_EnumMax,
};

static LPCTSTR g_szPermissionMode[] =
{
	_T("Operator"),
	_T("Manager"),
	_T("Administrator"),
	_T("Engineer"),
	_T("MES"),
	_T("C&C"),
	NULL
};

static COLORREF g_clrPermissionMode[] =
{
	RGB(0, 255, 0),
	RGB(190, 190, 0),
	RGB(255, 0, 0),
	RGB(0, 0, 255),
	RGB(0, 255, 0),
};
//---------------------------------------------------------
// 설비 검사 구동 방식
typedef enum enOperateMode
{
	OpMode_Production,		// 제품 생산 모드
	OpMode_Master,			// 마스터 모드
	OpMode_StartUp_Check,	// 시업 점검 모드
	//OpMode_DryRun,			// 기구 동작 테스트 모드
	OpMode_MaxEnum,
};

static LPCTSTR g_szOperateMode[] =
{
	_T("Product Production"),
	_T("Master Mode"),
	_T("Start-up Check"),
	//_T("Dry Run"),
	NULL
};

typedef enum enHandlerMode
{
	HM_Auto,
	HM_Manual,
	HM_Step,
	HM_SelfRun,	// Test 연동없이
	HM_DryRun,	// Test 연동
};

static LPCTSTR g_szHandlerMode[] =
{
	_T("Auto"),
	_T("Manual"),
	_T("Step"),
	_T("Self Run"),
	_T("Dry Run"),
	NULL
};

//---------------------------------------------------------
// 검사기 구분용도

#define SYS_FINALTEST	0	// FFT
#define SYS_QUALITYTEST	1	// 출하

typedef enum enInsptrSysTeach
{
	enInsptrSysTeach_EOL,
	enInsptrSysTeach_SW,
	enInsptrSysTeach_Max
};

typedef enum enInsptrSysType
{
	Sys_FinalTest = SYS_FINALTEST,		// FFT
	Sys_QualityTest = SYS_QUALITYTEST,	// 출하
};

static LPCTSTR g_szInsptrSysType[] =
{
	_T("Final Test"),
	//_T("SW Axis Modify Test"),
	_T("Quality Test"),
	NULL
};

static LPCTSTR g_szProgramName[] =
{
	_T("FinalTest"),
	//_T("SW Axis Modify Test"),
	_T("QualityTest"),
	NULL
};




//---------------------------------------------------------
// 검사기 구분 테이블
typedef struct _tag_InspectorTable
{
	LONG	SysType;			// 검사기 구분
	UINT	nPCBCamCnt;			// 카메라보드 개수(Luritech 제어)
	UINT	nLightBrdCount;		// 광원 보드 개수(Luritech 제어)
	UINT	nIndigatorCnt;		// 모터 인디게이터 개수
	BOOL	bPCIIOControl;		// IO 보드 사용 유무
	BOOL	bPCIMotion;			// 모터 보드 사용 유무
}ST_InspectorTable;

static ST_InspectorTable g_InspectorTable[] =
{	//검사기		// 카메라 보드 수	// 광원 보드 수		// 인디케이터 개수	// IO 보드 사용 유무	// 모터 보드 사용 유무
	{ Sys_FinalTest, 1, 0, 0, TRUE, TRUE, },
	{ Sys_QualityTest, 1, 0, 0, FALSE, FALSE, },
	NULL
};

//---------------------------------------------------------
// Lamp 색상
typedef enum enLampColor
{
	Lamp_Red,
	Lamp_Yellow,
	Lamp_Green,
};

//---------------------------------------------------------
// Buzzer 종류
typedef enum enBuzzerType
{
	Buzzer_01,
	Buzzer_02,
	Buzzer_03,
	Buzzer_04,
	Buzzer_05,
};

typedef enum enBarcodeChk
{
	Barcode_UnknownErr,
	Barcode_Pass,	// 사용 가능
	Barcode_Overap,	// 중복
	Barcode_Length,	// 글자수 오류
	Barcode_Count,	// 입력 개수 오류
};

static LPCTSTR g_szBarcodeChk[] = 
{
	_T("Unknown Error"),
	_T("OK"),
	_T("바코드 중복"),
	_T("바코드 길이 오류"),
	_T("바코드 입력 개수 오류"),
	NULL
};

//---------------------------------------------------------

// 모델 설정 파일 확장자
#define	MODEL_FILE_EXT			_T("luri")

// 포고 설정 파일 확장자
#define	POGO_FILE_EXT			_T("ini")

// I2C 파일 확장자
#define	I2C_FILE_EXT			_T("set")

// WAVE 파일 확장자
#define	WAVE_FILE_EXT			_T("wav")

// 포고 설정 파일 확장자
#define	MOTOR_FILE_EXT			_T("mot")

// prn 파일 확장자
#define	PRN_FILE_EXT			_T("prn")

// 유지 설정 파일 확장자
#define	MAINTENANCE_FILE_EXT	_T("mai")

#define VIDEO_WIDTH				720
#define VIDEO_HEIGHT			480

//---------------------------------------------------------
#define	MAX_CHANNEL_CNT			6	// 최대 채널 개수
#define	MAX_OPERATION_THREAD	6	// 최대 개별 검사용 쓰레드 개수
#define	MAX_SITE_CNT			6	// 최대 검사 개수
#define	MAX_MODULE_CNT			25	// 최대 제품 투입 개수 (25ch다운: 25, 6ch다운: 6, 4ch보임량: 4)

typedef enum _ConstVar
{
// 	MAX_CHANNEL_CNT				= 6,	// 최대 채널 개수
// 	MAX_OPERATION_THREAD		= 6,	// 최대 개별 검사용 쓰레드 개수
// 	MAX_SITE_CNT				= 6,	// 최대 검사 개수
//	MAX_MODULE_CNT				= 6,	// 최대 제품 투입 개수 (6ch다운: 6, 4ch보임량: 4)	
	MAX_PCBCAM_CNT				= 6,	// 최대 카메라 보드 개수 (6채널 6개)
	MAX_PCB_LIGHT_CNT			= 2,	// 광원 보드 개수 (2개)
	
	MAX_DIGITAL_IO				= 64,	// 최대 AJIN IO 사용 개수
	MAX_TESTITEM				= 100,	// 최대 검사 항목
	MAX_STEP_COUNT				= 50,	// 검사 스텝 최대 개수

	MAX_INDICATOR_CNT			= 2,

};

#endif // Def_Enum_Cm_h__

