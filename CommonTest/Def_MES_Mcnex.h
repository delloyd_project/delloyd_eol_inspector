﻿//*****************************************************************************
// Filename	: 	Def_MES_Mcnex.h
// Created	:	2016/9/6 - 14:07
// Modified	:	2016/9/6 - 14:07
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_MES_Mcnex_h__
#define Def_MES_Mcnex_h__

#include <afxwin.h>

static enum enHeader_EOL_Item_MES
{
	enHeader_EOL_LOT_NO,
	enHeader_EOL_LOT_CARD_NO,
	enHeader_EOL_Channel,
	enHeader_EOL_EqpCode,
	enHeader_EOL_ItemNumber,
	enHeader_EOL_Barcode,
	enHeader_EOL_Operator,
	enHeader_EOL_TestDate,
	enHeader_EOL_TestTime,

	enHeader_EOL_TestResult,

	enHeader_EOL_Current,
	enHeader_EOL_CurrentValue,

	enHeader_EOL_OperMode,
	enHeader_EOL_OperMode_15_Reset,
	enHeader_EOL_OperMode_15_Signal,
	enHeader_EOL_OperMode_15_Check,
	enHeader_EOL_OperMode_15_Current,
	enHeader_EOL_OperMode_30_Reset,
	enHeader_EOL_OperMode_30_Signal,
	enHeader_EOL_OperMode_30_Check,
	enHeader_EOL_OperMode_30_Current,

	enHeader_EOL_CenterPoint_1,
	enHeader_EOL_CenterPoint_1_X,
	enHeader_EOL_CenterPoint_1_Y,

	enHeader_EOL_CenterPoint_2,
	enHeader_EOL_CenterPoint_2_X,
	enHeader_EOL_CenterPoint_2_Y,

	enHeader_EOL_Rotation,
	enHeader_EOL_RotDegree,

	enHeader_EOL_SFR,
	enHeader_EOL_SFR_S1,
	enHeader_EOL_SFR_S2,
	enHeader_EOL_SFR_S3,
	enHeader_EOL_SFR_S4,
	enHeader_EOL_SFR_S5,
	enHeader_EOL_SFR_S6,
	enHeader_EOL_SFR_S7,
	enHeader_EOL_SFR_S8,
	enHeader_EOL_SFR_S9,
	enHeader_EOL_SFR_S10,
	enHeader_EOL_SFR_S11,
	enHeader_EOL_SFR_S12,
	enHeader_EOL_SFR_S13,
	enHeader_EOL_SFR_S14,
	enHeader_EOL_SFR_S15,
	enHeader_EOL_SFR_S16,
	enHeader_EOL_SFR_S17,
	enHeader_EOL_SFR_S18,
	enHeader_EOL_SFR_S19,
	enHeader_EOL_SFR_S20,
	enHeader_EOL_SFR_S21,
	enHeader_EOL_SFR_S22,
	enHeader_EOL_SFR_S23,
	enHeader_EOL_SFR_S24,
	enHeader_EOL_SFR_S25,
	enHeader_EOL_SFR_S26,
	enHeader_EOL_SFR_S27,
	enHeader_EOL_SFR_S28,
	enHeader_EOL_SFR_S29,
	enHeader_EOL_SFR_S30,

	enHeader_EOL_Angle,
	enHeader_EOL_Angle_LC,
	enHeader_EOL_Angle_RC,
	enHeader_EOL_Angle_TC,
	enHeader_EOL_Angle_BC,

	enHeader_EOL_Color,
	enHeader_EOL_Color_Red_R,
	enHeader_EOL_Color_Red_G,
	enHeader_EOL_Color_Red_B,
	enHeader_EOL_Color_Green_R,
	enHeader_EOL_Color_Green_G,
	enHeader_EOL_Color_Green_B,
	enHeader_EOL_Color_Blue_R,
	enHeader_EOL_Color_Blue_G,
	enHeader_EOL_Color_Blue_B,
	enHeader_EOL_Color_Black_R,
	enHeader_EOL_Color_Black_G,
	enHeader_EOL_Color_Black_B,

	enHeader_EOL_Reverse,

	enHeader_EOL_Particle,

	enHeader_EOL_LED,
	enHeader_EOL_LED_Current,

	enHeader_EOL_Brightness,
	enHeader_EOL_Brightness_S1,
	enHeader_EOL_Brightness_S2,
	enHeader_EOL_Brightness_S3,
	enHeader_EOL_Brightness_S4,

	enHeader_EOL_PatternNoise,
	enHeader_EOL_PatternNoise_S1,
	enHeader_EOL_PatternNoise_S2,
	enHeader_EOL_PatternNoise_S3,
	enHeader_EOL_PatternNoise_S4,
	enHeader_EOL_PatternNoise_S5,
	enHeader_EOL_PatternNoise_S6,
	enHeader_EOL_PatternNoise_S7,
	enHeader_EOL_PatternNoise_S8,
	enHeader_EOL_PatternNoise_S9,

	enHeader_EOL_IRFilter,
	enHeader_EOL_IRFilter_RedGain,

	enHeader_EOL_Item_Max
};

static LPCTSTR g_lpszHeader_EOL_Item_MES[enHeader_EOL_Item_Max] =
{
	_T("LOT_NO"),
	_T("LOT_CARD_NO"),
	_T("채널번호"),
	_T("설비코드"),
	_T("지시번호"),
	_T("품번"),
	_T("작업자"),
	_T("처리일자"),
	_T("처리일시"),
	_T("최종결과"),

	// test item
	_T("전류"),
	_T("전류 값"),

	_T("동작모드"),
	_T("15fps 리셋"),
	_T("15fps 신호"),
	_T("15fps 체크"),
	_T("15fps 전류"),
	_T("30fps 리셋"),
	_T("30fps 신호"),
	_T("30fps 체크"),
	_T("30fps 전류"),

	_T("보임량 광축 1"),
	_T("광축 X"),
	_T("광축 Y"),

	_T("보임량 광축 2"),
	_T("광축 X"),
	_T("광축 Y"),

	_T("로테이션"),
	_T("각도"),

	_T("해상력 SFR"),
	_T("S1"),
	_T("S2"),
	_T("S3"),
	_T("S4"),
	_T("S5"),
	_T("S6"),
	_T("S7"),
	_T("S8"),
	_T("S9"),
	_T("S10"),
	_T("S11"),
	_T("S12"),
	_T("S13"),
	_T("S14"),
	_T("S15"),
	_T("S16"),
	_T("S17"),
	_T("S18"),
	_T("S19"),
	_T("S20"),
	_T("S21"),
	_T("S22"),
	_T("S23"),
	_T("S24"),
	_T("S25"),
	_T("S26"),
	_T("S27"),
	_T("S28"),
	_T("S29"),
	_T("S30"),

	_T("화각"),
	_T("L-C"),
	_T("R-C"),
	_T("T-C"),
	_T("B-C"),

	_T("컬러"),
	_T("RED [R]"),
	_T("RED [G]"),
	_T("RED [B]"),
	_T("GREEN [R]"),
	_T("GREEN [G]"),
	_T("GREEN [B]"),
	_T("BLUE [R]"),
	_T("BLUE [G]"),
	_T("BLUE [B]"),
	_T("BLACK [R]"),
	_T("BLACK [G]"),
	_T("BLACK [B]"),

	_T("역상"),
	
	_T("이물"),

	_T("LED"),
	_T("LED 전류"),

	_T("광량비"),
	_T("S1"),
	_T("S2"),
	_T("S3"),
	_T("S4"),

	_T("저조도"),
	_T("S1"),
	_T("S2"),
	_T("S3"),
	_T("S4"),
	_T("S5"),
	_T("S6"),
	_T("S7"),
	_T("S8"),
	_T("S9"),

	_T("IR 필터"),
	_T("RED GAIN"),

};

//-----------------------------------------------------------------------------
// Worklist 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_MES_TotalResult
{
	CString szLotNo;
	CString szLotCardNo;
	CString szChannel;
	CString szEqpCode;
	CString szItemNumber;
	CString szBarcode;
	CString szOperator;
	CString szTestDate;
	CString szTestTime;

	CStringArray	ItemHeaderz;
	CStringArray	Itemz;

	void Reset()
	{
		szLotNo		.Empty();
		szLotCardNo	.Empty();
		szChannel	.Empty();
		szEqpCode	.Empty();
		szItemNumber.Empty();
		szBarcode	.Empty();
		szOperator	.Empty();
		szTestDate	.Empty();
		szTestTime	.Empty();
	};

}ST_MES_TotalResult;


#endif // Def_MES_Mcnex_h__
