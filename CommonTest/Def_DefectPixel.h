﻿#ifndef Def_T_DefectPixel_h__
#define Def_T_DefectPixel_h__

#include <afxwin.h>

#include "Def_Test_Cm.h"

#pragma pack(push,1)

typedef enum enDefectPixel_Light
{
	Light_Defect_Black = 0,
	Light_Defect_White,
	Light_Defect_MAX,
};

typedef enum enDefectPixel_Spec
{
	//Spec_Defect_VeryHotPixel = 0,
	//Spec_Defect_HotPixel,
	//Spec_Defect_VeryBrightPixel,
	//Spec_Defect_BrightPixel,
	//Spec_Defect_VeryDeadPixel = 0,
	Spec_Defect_DeadPixel = 0,
	//Spec_Defect_RowCol,
	Spec_Defect_Cluster,
	Spec_Defect_Max,
};

typedef enum enDefectPixel_Threshold
{

	Threshold_Defect_DeadPixel = 0,
	Threshold_Defect_Max,
};

static LPCTSTR	g_szDefectPixel_Spec[] =
{
	//_T("Very HotPixel Count"),
	//_T("HotPixel Count"),
	//_T("Very BrightPixel Count"),
	//_T("BrightPixel Count"),
	//_T("Very DeadPixel Count"),
	//_T("DeadPixel Count"),
	//_T("Row Col Count"),
	//_T("Cluster Count"),
	//_T("Very Dark Count"),
	_T("Dark Count"),
	_T("Cluster Count"),
	NULL
};

typedef enum eDefectPixelType
{
	//Defect_Type_VeryHotPixel = 0,
	//Defect_Type_HotPixel,
	//Defect_Type_VeryBrightPixel,
	//Defect_Type_BrightPixel,
	//Defect_Type_VeryDeadPixel = 0,
	Defect_Type_DeadPixel=0,
	//Defect_Type_RowCol,
	Defect_Type_Cluster,
	Defect_Type_Max,
};

static LPCTSTR	g_szDefectPixel_Type[] =
{
	//_T("Very HotPixel"),
	//_T("HotPixel"),
	//_T("Very BrightPixel"),
	//_T("BrightPixel"),
	//_T("Very DeadPixel"),
	_T("DarkPixel"),
	//_T("RowCol"),
	_T("Cluster"),
	NULL
};

typedef enum enDefectPixel_Fail
{
	ROI_F_DefectPixel_FST = 0,
	ROI_F_DefectPixel_LST = 1999,
	ROI_F_DefectPixel_Max,
};

typedef struct _tag_DefectPixel_Opt
{
	int iBlockSize;
	float fThreshold[Threshold_Defect_Max];
	BOOL bEnableMin[Spec_Defect_Max];
	BOOL bEnableMax[Spec_Defect_Max];

	UINT nmagin_left;
	UINT nmagin_rightt;

	int iSpecMin[Spec_Defect_Max];
	int iSpecMax[Spec_Defect_Max];

	//ST_Comm_Spec stSpec_Min[Spec_Defect_Max];
	//ST_Comm_Spec stSpec_Max[Spec_Defect_Max];

	_tag_DefectPixel_Opt()
	{
		iBlockSize = 4;
		nmagin_left = 0;
		nmagin_rightt = 0;

		for (UINT nIdx = 0; nIdx < Threshold_Defect_Max; nIdx++)
		{
			fThreshold[nIdx] = 0;
		}
	
		for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
		{
			bEnableMin[nIdx] = FALSE;
			bEnableMax[nIdx] = FALSE;
			iSpecMin[nIdx] = 0;
			iSpecMax[nIdx] = 0;

			//stSpec_Min[nIdx].Reset();
			//stSpec_Max[nIdx].Reset();
		}
	};

	_tag_DefectPixel_Opt& operator= (_tag_DefectPixel_Opt& ref)
	{
		iBlockSize = ref.iBlockSize;
		nmagin_left = ref.nmagin_left;
		nmagin_rightt = ref.nmagin_rightt;

		for (UINT nIdx = 0; nIdx < Threshold_Defect_Max; nIdx++)
		{
			fThreshold[nIdx] = ref.fThreshold[nIdx];
		}
		
		for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
		{
			bEnableMin[nIdx] = ref.bEnableMin[nIdx];
			bEnableMax[nIdx] = ref.bEnableMax[nIdx];
			iSpecMin[nIdx] = ref.iSpecMin[nIdx];
			iSpecMax[nIdx] = ref.iSpecMax[nIdx];
			
			//stSpec_Min[nIdx] = ref.stSpec_Min[nIdx];
			//stSpec_Max[nIdx] = ref.stSpec_Max[nIdx];
		}

		return *this;
	};

}ST_DefectPixel_Opt, *PST_DefectPixel_Opt;

typedef struct _tag_DefectPixel_Result
{
	// 결과
	UINT nResult;

	UINT nEachResult[Spec_Defect_Max];
	UINT nFailCount[Spec_Defect_Max];

	UINT	nFailType[Spec_Defect_Max][ROI_F_DefectPixel_Max];
	CRect	rtFailROI[Spec_Defect_Max][ROI_F_DefectPixel_Max];

	_tag_DefectPixel_Result()
	{
		Reset();
	};

	void Reset()
	{
		nResult		= 1;

		for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
		{
			nEachResult[nIdx]	= 0;
			nFailCount[nIdx]	= 0;

			for (UINT nItm = 0; nItm < ROI_F_DefectPixel_Max; nItm++)
			{
				nFailType[nIdx][nItm] = Defect_Type_Max;
				rtFailROI[nIdx][nItm].SetRectEmpty();
			}
		}
	};
	void FailData(){
		nResult = 0;

		for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
		{
			nEachResult[nIdx] = 0;
			nFailCount[nIdx] = 0;

			for (UINT nItm = 0; nItm < ROI_F_DefectPixel_Max; nItm++)
			{
				nFailType[nIdx][nItm] = Defect_Type_Max;
				rtFailROI[nIdx][nItm].SetRectEmpty();
			}
		}
	};
	_tag_DefectPixel_Result& operator= (_tag_DefectPixel_Result& ref)
	{
		nResult		= ref.nResult;

		for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
		{
			nEachResult[nIdx]	= ref.nEachResult[nIdx];
			nFailCount[nIdx]	= ref.nFailCount[nIdx];
		
			for (UINT nItm = 0; nItm < ROI_F_DefectPixel_Max; nItm++)
			{
				nFailType[nIdx][nItm] = ref.nFailType[nIdx][nItm];
				rtFailROI[nIdx][nItm] = ref.rtFailROI[nIdx][nItm];
			}
		}

		return *this;
	};

}ST_DefectPixel_Data, *PST_DefectPixel_Data;


//typedef struct _tag_TI_DefectPixel
//{
//	ST_DefectPixel_Opt		stDefectPixelOpt;		// 검사 기준 데이터
//	ST_DefectPixel_Data		stDefectPixelData;		// 측정 데이터
//
//	_tag_TI_DefectPixel()
//	{
//	};
//
//	void Reset()
//	{
//		stDefectPixelData.Reset();
//	};
//
//	_tag_TI_DefectPixel& operator= (_tag_TI_DefectPixel& ref)
//	{
//		stDefectPixelOpt	= ref.stDefectPixelOpt;
//		stDefectPixelData	= ref.stDefectPixelData;
//
//		return *this;
//	};
//
//}ST_TI_DefectPixel, *PST_TI_DefectPixel;

#pragma pack(pop)

#endif // Def_DefectPixel_h__