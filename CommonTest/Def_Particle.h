﻿#ifndef Def_Particle_T_h__
#define Def_Particle_T_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

#pragma pack(push,1)

#define DetectCount 60

//typedef enum enParticleType
//{
	//Pr_Type_Stain = 0,
	//Pr_Type_Blemish,
	//Pr_Type_DeadPixel,
	//Pr_Type_Max,
//};

typedef enum enPaticleRegion_Part
{
	Part_ROI_Edge = 0,
	Part_ROI_Vertical,
	Part_ROI_Horizon,
	Part_ROI_Center,
	Part_ROI_Max,
};

static LPCTSTR g_szPaticleRegion[] =
{
	_T("Corner"),
	_T("Vertical Edge"),
	_T("Horizon Edge"),
	_T("Center"),
	NULL
};

// ROI
typedef struct _tag_RegionParticle
{
	CRect	rtRoi;

	//BOOL	bEllipse;

	// 멍 이물 농도
	double	dbBruiseConc;

	// 멍 이물 크기
	double	dbBruiseSize;

	_tag_RegionParticle()
	{
		//bEllipse	 = TRUE;
		dbBruiseConc = 0.0;
		dbBruiseSize = 0.0;

		rtRoi.SetRectEmpty();
	};

	void Reset()
	{
		//bEllipse	 = TRUE;
		dbBruiseConc = 0.0;
		dbBruiseSize = 0.0;

		rtRoi.SetRectEmpty();
	};

	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp = rtRoi;

		rtRoi.left	 = iPosX - (int)(rtTemp.Width() / 2);
		rtRoi.right  = rtRoi.left + rtTemp.Width();;
		rtRoi.top	 = iPosY - (int)(rtTemp.Height() / 2);
		rtRoi.bottom = rtRoi.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtRoi;

		rtRoi.left   = rtTemp.CenterPoint().x - (iWidth / 2);
		rtRoi.right  = rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtRoi.top	 = rtTemp.CenterPoint().y - (iHeight / 2);
		rtRoi.bottom = rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};

	_tag_RegionParticle& operator= (_tag_RegionParticle& ref)
	{
		rtRoi		 = ref.rtRoi;
		dbBruiseConc = ref.dbBruiseConc;
		dbBruiseSize = ref.dbBruiseSize;
		//bEllipse	 = ref.bEllipse;
		
		return *this;
	};

}ST_RegionParticle, *PST_RegionParticle;

typedef struct _tag_Particlee_Opt
{
	// 노이즈 민감도
	float		fDustDis;
	int			iEdgeW;
	int			iEdgeH;

	// 이물 검사 영역 구조체
	ST_RegionParticle	stRegionOp[Part_ROI_Max];

	void Reset()
	{
		fDustDis = 2;
		iEdgeW = 32;
		iEdgeH = 32;

		for (UINT nIdx = 0; nIdx < Part_ROI_Max; nIdx++)
		{
			stRegionOp[nIdx].Reset();
		}
	};

	_tag_Particlee_Opt()
	{
		Reset();
	};

	// 변수 교환 함수
	_tag_Particlee_Opt& operator= (_tag_Particlee_Opt& ref)
	{
		fDustDis = ref.fDustDis;

		iEdgeW = ref.iEdgeW;
		iEdgeH = ref.iEdgeH;

		for (UINT nIdx = 0; nIdx < Part_ROI_Max; nIdx++)
		{
			stRegionOp[nIdx] = ref.stRegionOp[nIdx];
		}

		return *this;
	};

}ST_Particle_Opt, *PST_Particle_Opt;

typedef struct _tag_Particle_Result
{
	UINT	nResult;

	// 검출된 이물 갯수
	UINT	nFailCount;
	CRect	rtFailRoi[DetectCount];

	UINT	nFailType[DetectCount];
	UINT	nFailTypeCount[Part_ROI_Max];
	double	dFailTypeConcentration[Part_ROI_Max];

	int		iSelectROI;

	// 농도
	float	fConcentration[DetectCount];

	void Reset()
	{
		nResult = 3;
		nFailCount = 0;

		for (UINT nIdx = 0; nIdx < DetectCount; nIdx++)
		{
			nFailType[nIdx] = 1;
			fConcentration[nIdx] = 0;
			rtFailRoi[nIdx].SetRectEmpty();
		}

		for (int t = 0; t < Part_ROI_Max; t++)
		{
			nFailTypeCount[t] = 0;
			dFailTypeConcentration[t] = 0;
		}

		iSelectROI = -1;
	};

	// 변수 초기화 함수
	_tag_Particle_Result()
	{
		Reset();
	};

	void FailData() {
		nResult = 0;
		nFailCount = 0;

		for (UINT nIdx = 0; nIdx < DetectCount; nIdx++)
		{
			nFailType[nIdx] = 1;
			rtFailRoi[nIdx].SetRectEmpty();

			fConcentration[nIdx] = 0.0;
		}

		for (int t = 0; t < Part_ROI_Max; t++)
		{
			nFailTypeCount[t] = 0;
			dFailTypeConcentration[t] = 0;
		}
	};

	// 변수 교환 함수
	_tag_Particle_Result& operator= (_tag_Particle_Result& ref)
	{
		nResult = ref.nResult;
		nFailCount = ref.nFailCount;

		for (UINT nIdx = 0; nIdx < DetectCount; nIdx++)
		{
			fConcentration[nIdx] = ref.fConcentration[nIdx];
			rtFailRoi[nIdx] = ref.rtFailRoi[nIdx];
			nFailType[nIdx] = ref.nFailType[nIdx];
		}

		for (int t = 0; t < Part_ROI_Max; t++)
		{
			nFailTypeCount[t] = ref.nFailTypeCount[t];
			dFailTypeConcentration[t] = ref.dFailTypeConcentration[t];
		}

		iSelectROI = ref.iSelectROI;

		return *this;
	};

}ST_Particle_Result, *PST_Particle_Result;

#pragma pack(pop)

#endif // Def_Particle_T_h__

