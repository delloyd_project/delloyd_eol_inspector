﻿//*****************************************************************************
// Filename	: 	Dlg_Barcode.cpp
// Created	:	2016/11/1 - 16:59
// Modified	:	2016/11/1 - 16:59
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Dlg_Barcode.cpp : implementation file
//

#include "stdafx.h"
#include "Dlg_Barcode.h"
#include "afxdialogex.h"
#include "CommonFunction.h"


#define IDC_ED_BARCODE_01		1001

#define		TABSTYLE_COUNT			MAX_SITE_CNT
static UINT g_TabOrder[TABSTYLE_COUNT] =
{
	1001, 1002, 1003, 1004, 1005, 1006
};

// CDlg_Barcode dialog

IMPLEMENT_DYNAMIC(CDlg_Barcode, CDialogEx)

CDlg_Barcode::CDlg_Barcode(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_Barcode::IDD, pParent)
{
	VERIFY(m_font_Large.CreateFont(
		42,							// nHeight
		0,							// nWidth
		0,							// nEscapement
		0,							// nOrientation
		FW_BOLD,					// nWeight
		FALSE,						// bItalic
		FALSE,						// bUnderline
		0,							// cStrikeOut
		ANSI_CHARSET,				// nCharSet
		OUT_DEFAULT_PRECIS,			// nOutPrecision
		CLIP_DEFAULT_PRECIS,		// nClipPrecision
		DEFAULT_QUALITY,			// nQuality
		VARIABLE_PITCH,				// nPitchAndFamily
		_T("Arial")));		// lpszFacename

	VERIFY(m_font_Default.CreateFont(
		24,							// nHeight
		0,							// nWidth
		0,							// nEscapement
		0,							// nOrientation
		FW_BOLD,					// nWeight
		FALSE,						// bItalic
		FALSE,						// bUnderline
		0,							// cStrikeOut
		ANSI_CHARSET,				// nCharSet
		OUT_DEFAULT_PRECIS,			// nOutPrecision
		CLIP_DEFAULT_PRECIS,		// nClipPrecision
		DEFAULT_QUALITY,			// nQuality
		VARIABLE_PITCH,				// nPitchAndFamily
		_T("Arial")));		// lpszFacename

	m_nBarcodeCnt		= 1;
	m_nBarcodeLength	= 16;

	for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
	{
		m_bUse[nIdx] = TRUE;
	}

	m_bIsModal			= FALSE;
	m_bChk_BarcodeLength= TRUE;
}

CDlg_Barcode::~CDlg_Barcode()
{
	m_font_Large.DeleteObject();
	m_font_Default.DeleteObject();
}

void CDlg_Barcode::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlg_Barcode, CDialogEx)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()


// CDlg_Barcode message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/11/4 - 11:45
// Desc.		:
//=============================================================================
int CDlg_Barcode::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	CString szText;
	for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
	{
		m_st_Site[nIdx].SetBackColor_COLORREF(RGB(0, 0, 0));
		m_st_Site[nIdx].SetTextColor(Color::White, Color::White);
		m_st_Site[nIdx].SetFont_Gdip(L"arial", 18.0F);
		m_st_Status[nIdx].SetFont_Gdip(L"arial", 10.0F);

		szText.Format(_T("Socket %d"), nIdx + 1);
		m_st_Site[nIdx].Create(szText, dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
		m_ed_Barcode[nIdx].Create(dwStyle | WS_TABSTOP | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_BARCODE_01 + nIdx);
		m_st_Status[nIdx].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_Barcode[nIdx].SetFont(&m_font_Default);
	}

	if (1 == m_nBarcodeCnt)
	{
		m_st_Site[0].SetText(_T("Barcode"));
		m_ed_Barcode[0].SetFont(&m_font_Large);

		//MoveWindow(0, 0, 600, 300);
		SetWindowPos(this, 0, 0, 600, 280, SWP_HIDEWINDOW);
		CenterWindow();
	}
	else if (m_nBarcodeCnt <= 4)
	{
		//MoveWindow(0, 0, 300 * m_nBarcodeCnt, 300);
		SetWindowPos(this, 0, 0, 300 * m_nBarcodeCnt, 280, SWP_HIDEWINDOW);
		CenterWindow();
	}
	else
	{
		//MoveWindow(0, 0, 300 * m_nBarcodeCnt, 300);
		SetWindowPos(this, 0, 0, 720, 500, SWP_HIDEWINDOW);
		CenterWindow();
	}
	
	m_bn_OK.Create(_T("OK"), dwStyle | WS_TABSTOP | BS_PUSHBUTTON, rectDummy, this, IDOK);
	m_bn_Cancel.Create(_T("Cancel"), dwStyle | WS_TABSTOP | BS_PUSHBUTTON, rectDummy, this, IDCANCEL);

	m_ed_Barcode[0].SetFocus();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/11/4 - 11:45
// Desc.		:
//=============================================================================
void CDlg_Barcode::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft		= iMagrin;
	int iTop		= iMagrin;
	int iWidth		= cx - iMagrin - iMagrin;
	int iHeight		= cy - iMagrin - iMagrin;
	int iCtrlWidth	= (iWidth - (iSpacing * (m_nBarcodeCnt - 1))) / m_nBarcodeCnt;
	int iCtrlHeight = 50;
	int iSubLeft	= iLeft;

	if (m_nBarcodeCnt <= 4)
	{
		for (UINT nIdx = 0; nIdx < m_nBarcodeCnt; nIdx++)
		{
			iTop = iMagrin;
			m_st_Site[nIdx].MoveWindow(iSubLeft, iTop, iCtrlWidth, iCtrlHeight);
			iTop += iCtrlHeight + iSpacing;
			m_ed_Barcode[nIdx].MoveWindow(iSubLeft, iTop, iCtrlWidth, iCtrlHeight);
			iTop += iCtrlHeight + iSpacing;
			m_st_Status[nIdx].MoveWindow(iSubLeft, iTop, iCtrlWidth, iCtrlHeight);

			iSubLeft += iCtrlWidth + iSpacing;
		}
	}
	else
	{		
		iCtrlWidth	= (iWidth - iSpacing) / 4;

		for (UINT nIdx = 0; nIdx < m_nBarcodeCnt; nIdx++)
		{
			iSubLeft = iMagrin;
			m_st_Site[nIdx].MoveWindow(iSubLeft, iTop, iCtrlWidth, iCtrlHeight);
			iSubLeft += iCtrlWidth;			
			m_ed_Barcode[nIdx].MoveWindow(iSubLeft, iTop, iCtrlWidth + iCtrlWidth, iCtrlHeight);
			iSubLeft += iCtrlWidth + iCtrlWidth + iSpacing;
			m_st_Status[nIdx].MoveWindow(iSubLeft, iTop, iCtrlWidth, iCtrlHeight);			

			iTop += iCtrlHeight + iSpacing;
		}
	}

	for (UINT nIdx = m_nBarcodeCnt; nIdx < MAX_SITE_CNT; nIdx++)
	{
		m_st_Site[nIdx].MoveWindow(0, 0, 0, 0);
		m_ed_Barcode[nIdx].MoveWindow(0, 0, 0, 0);
		m_st_Status[nIdx].MoveWindow(0, 0, 0, 0);
	}

	iTop += iCtrlHeight + iCateSpacing;
	if (m_nBarcodeCnt <= 4)
	{
		iCtrlWidth = 240;
	}
	else
	{
		iCtrlWidth = (iWidth - iSpacing) / 2;
	}

	iTop = cy - iMagrin - iCtrlHeight;
	m_bn_OK.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	iLeft = cx - iMagrin - iCtrlWidth;
	m_bn_Cancel.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2016/11/5 - 20:03
// Desc.		:
//=============================================================================
void CDlg_Barcode::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	if (bShow)
	{
		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			if (m_bUse[nIdx])
			{
				m_ed_Barcode[nIdx].EnableWindow(TRUE);
			}
			else
			{
				m_ed_Barcode[nIdx].EnableWindow(FALSE);
				m_st_Status[nIdx].SetText(_T("Not Use"));
				m_st_Status[nIdx].SetBackColor_COLORREF(RGB(150, 150, 150));
			}
		}
	}
}

//=============================================================================
// Method		: OnGetMinMaxInfo
// Access		: protected  
// Returns		: void
// Parameter	: MINMAXINFO * lpMMI
// Qualifier	:
// Last Update	: 2016/11/5 - 20:03
// Desc.		:
//=============================================================================
void CDlg_Barcode::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	if (m_nBarcodeCnt <= 1)
	{
		lpMMI->ptMaxTrackSize.x = 600;
		lpMMI->ptMaxTrackSize.y = 280;

		lpMMI->ptMinTrackSize.x = 600;
		lpMMI->ptMinTrackSize.y = 280;
	}
	else if (m_nBarcodeCnt <= 4)
	{
		lpMMI->ptMaxTrackSize.x = 280 * m_nBarcodeCnt;
		lpMMI->ptMaxTrackSize.y = 280;

		lpMMI->ptMinTrackSize.x = 280 * m_nBarcodeCnt;
		lpMMI->ptMinTrackSize.y = 280;
	}
	else
	{
		lpMMI->ptMaxTrackSize.x = 720;
		lpMMI->ptMaxTrackSize.y = 500;

		lpMMI->ptMinTrackSize.x = 720;
		lpMMI->ptMinTrackSize.y = 500;
	}

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/11/5 - 20:03
// Desc.		:
//=============================================================================
BOOL CDlg_Barcode::PreCreateWindow(CREATESTRUCT& cs)
{
	ModifyStyle(0, WS_SIZEBOX);

	return CDialogEx::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/11/5 - 20:03
// Desc.		:
//=============================================================================
BOOL CDlg_Barcode::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
	case WM_KEYDOWN:
		if (VK_TAB == pMsg->wParam)
		{
			int nID = GetFocus()->GetDlgCtrlID();
			for (UINT iCnt = 0; iCnt < m_nBarcodeCnt; iCnt++)
			{
				if (nID == g_TabOrder[iCnt])
				{
					if ((m_nBarcodeCnt - 1) == iCnt)
					{
						nID = IDOK;
					}
					else
					{
						nID = g_TabOrder[iCnt + 1];
					}

					break;
				}
				else if (IDOK == nID)
				{
					nID = IDCANCEL;
					break;
				}
				else if (IDCANCEL == nID)
				{
					nID = g_TabOrder[0];
					break;
				}
			}

			GetDlgItem(nID)->SetFocus();
		}
		else if (pMsg->wParam == VK_RETURN)
		{
			int nID = GetFocus()->GetDlgCtrlID();
			if ((IDOK != nID) && (IDCANCEL != nID))
			{
				for (UINT iCnt = 0; iCnt < m_nBarcodeCnt; iCnt++)
				{
					if (nID == g_TabOrder[iCnt])
					{
						if ((m_nBarcodeCnt - 1) == iCnt)
						{
							nID = g_TabOrder[0];
						}
						else
						{
							nID = g_TabOrder[iCnt + 1];
						}

						break;
					}
				}

				GetDlgItem(nID)->SetFocus();
			}
			return TRUE;
		}
		else if (pMsg->wParam == VK_ESCAPE)
		{
			// 여기에 ESC키 기능 작성       
			return TRUE;
		}
		break;

	default:
		break;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnInitDialog
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/11/5 - 20:03
// Desc.		:
//=============================================================================
BOOL CDlg_Barcode::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	if (m_bIsModal)
	{
		for (UINT nIdx = 0; nIdx < m_nBarcodeCnt; nIdx++)
		{
			m_st_Status[nIdx].ShowWindow(SW_HIDE);
		}
	}

	m_ed_Barcode[0].SetFocus();
	//GotoDlgCtrl(&m_ed_Barcode[0]);

	//return TRUE;  // return TRUE unless you set the focus to a control
	return FALSE;
	// EXCEPTION: OCX Property Pages should return FALSE
}

//=============================================================================
// Method		: OnOK
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/16 - 16:43
// Desc.		:
//=============================================================================
void CDlg_Barcode::OnOK()
{
	if (FALSE == m_bIsModal)
	{
		OnCancel();
		return;
	}

	CString szValue;

	m_szarBarcodez.RemoveAll();

	if (1 == m_nBarcodeCnt)
	{
		m_ed_Barcode[0].GetWindowText(szValue);

		m_szarBarcodez.Add(szValue);
	}
	else
	{
		for (UINT nIdx = 0; nIdx < m_nBarcodeCnt; nIdx++)
		{
			m_ed_Barcode[nIdx].GetWindowText(szValue);

			m_szarBarcodez.Add(szValue);
		}
	}
	
	//ShowWindow(SW_HIDE);
	CDialogEx::OnOK();
}

//=============================================================================
// Method		: DoModal
// Access		: virtual protected  
// Returns		: INT_PTR
// Qualifier	:
// Last Update	: 2016/11/18 - 12:25
// Desc.		:
//=============================================================================
INT_PTR CDlg_Barcode::DoModal()
{
	m_bIsModal = TRUE;

	return CDialogEx::DoModal();
}

//=============================================================================
// Method		: CheckBarcode
// Access		: protected  
// Returns		: enBarcodeChk
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2016/11/4 - 11:53
// Desc.		:
//=============================================================================
enBarcodeChk CDlg_Barcode::CheckBarcode(__in LPCTSTR szBarcode)
{
	INT_PTR iCnt = m_szarBarcodez.GetCount();

	if ((1 < m_nBarcodeCnt) && ((INT_PTR)m_nBarcodeCnt <= iCnt))
	{
		return Barcode_Count;
	}
	
// 	if (m_bChk_BarcodeLength)
// 	{
// 		if (m_nBarcodeLength != _tcslen(szBarcode))
// 		{
// 			return Barcode_Length;
// 		}
// 	}

	//iCnt = (iCnt < m_nBarcodeCnt) ? iCnt : m_nBarcodeCnt;
	//Barcode_Length

	if (0 < iCnt)
	{
		for (INT_PTR nIdx = 0; nIdx < iCnt; nIdx++)
		{
			if (0 == (m_szarBarcodez.GetAt(nIdx)).Compare(szBarcode))
			{
				return Barcode_Overap;
			}
		}
	}

	return Barcode_Pass;
}

//=============================================================================
// Method		: CheckBarcodeSocket
// Access		: protected  
// Returns		: enBarcodeChk
// Parameter	: __in UINT nSocket
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2017/5/18 - 23:47
// Desc.		:
//=============================================================================
enBarcodeChk CDlg_Barcode::CheckBarcodeSocket(__in UINT nSocket, __in LPCTSTR szBarcode)
{
	if ((1 < m_nBarcodeCnt) && (m_nBarcodeCnt <= m_nInputedBarcodeCnt))
	{
		return Barcode_Count;
	}

// 	if (m_bChk_BarcodeLength)
// 	{
// 		if (m_nBarcodeLength != _tcslen(szBarcode))
// 		{
// 			return Barcode_Length;
// 		}
// 	}

	if (0 < m_nInputedBarcodeCnt)
	{
		for (UINT nIdx = 0; nIdx < m_nInputedBarcodeCnt; nIdx++)
		{
			if (nIdx != nSocket)
			{
				if (0 == m_szBarcodeSocket[nIdx].Compare(szBarcode))
				{
					return Barcode_Overap;
				}
			}
		}
	}

	return Barcode_Pass;
}

//=============================================================================
// Method		: SetStatus
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nIndex
// Parameter	: __in enBarcodeChk nStatus
// Qualifier	:
// Last Update	: 2016/11/5 - 14:22
// Desc.		:
//=============================================================================
void CDlg_Barcode::SetStatus(__in UINT nIndex, __in enBarcodeChk nStatus)
{
	m_st_Status[nIndex].SetText(g_szBarcodeChk[nStatus]);

	if (Barcode_Pass == nStatus)
	{
		m_st_Status[nIndex].SetBackColor_COLORREF(RGB(0, 176,  80));
	}
	else
	{
		m_st_Status[nIndex].SetBackColor_COLORREF(RGB(192, 0, 0));
	}
}

//=============================================================================
// Method		: SetBarcodeCount
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2016/11/5 - 14:22
// Desc.		:
//=============================================================================
BOOL CDlg_Barcode::SetBarcodeCount(__in UINT nCount)
{
	if (nCount <= MAX_SITE_CNT)
	{	
		m_nBarcodeCnt		= nCount;
		m_bUseBarcodeCnt	= nCount;
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//=============================================================================
// Method		: SetBarcodeLength
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nLength
// Parameter	: __in BOOL bUse
// Qualifier	:
// Last Update	: 2017/2/17 - 17:00
// Desc.		:
//=============================================================================
BOOL CDlg_Barcode::SetBarcodeLength(__in UINT nLength, __in BOOL bUse /*= TRUE*/)
{
	m_nBarcodeLength = nLength;

	m_bChk_BarcodeLength = bUse;

	return TRUE;
}

//=============================================================================
// Method		: GetBarcode
// Access		: public  
// Returns		: void
// Parameter	: __in CStringArray & szarBarcodez
// Qualifier	:
// Last Update	: 2016/11/5 - 14:22
// Desc.		:
//=============================================================================
void CDlg_Barcode::GetBarcode(__in CStringArray& szarBarcodez)
{
	szarBarcodez.RemoveAll();

	if (FALSE == m_bUseSelectSocket)
	{
		szarBarcodez.Append(m_szarBarcodez);
	}
	else
	{
		for (UINT nIdx = 0; nIdx < m_nBarcodeCnt; nIdx++)
		{
			szarBarcodez.Add(m_szBarcodeSocket[nIdx]);
		}
	}
}

//=============================================================================
// Method		: GetBarcode
// Access		: public  
// Returns		: void
// Parameter	: __in CString & szBarcodez
// Qualifier	:
// Last Update	: 2016/11/5 - 14:22
// Desc.		:
//=============================================================================
void CDlg_Barcode::GetBarcode(__in CString& szBarcodez)
{
	if (0 < m_szarBarcodez.GetCount())
		szBarcodez = m_szarBarcodez.GetAt(0);
}

//=============================================================================
// Method		: InsertBarcode
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2016/11/5 - 14:22
// Desc.		:
//=============================================================================
BOOL CDlg_Barcode::InsertBarcode(__in LPCTSTR szBarcode)
{
	UINT iCnt = (UINT)m_szarBarcodez.GetCount();
	
	if (0 < m_nBarcodeCnt)
	{
		for (UINT nIdx = iCnt; nIdx < m_nBarcodeCnt; nIdx++)
		{
			if (FALSE == m_bUse[nIdx])
			{
				m_szarBarcodez.Add(STR_NO_BARCODE);
			}
			else
			{
				break;
			}
		}

		iCnt = (UINT)m_szarBarcodez.GetCount();
	}

	enBarcodeChk enError = CheckBarcode(szBarcode);
	if (iCnt == m_nBarcodeCnt)
		SetStatus(iCnt - 1, enError);
	else
		SetStatus(iCnt, enError);

	if (Barcode_Pass == enError)
	{
		if (1 == m_nBarcodeCnt)
		{
			m_szarBarcodez.RemoveAll();
			iCnt = 0;
		}
		m_szarBarcodez.Add(szBarcode);

		if (GetSafeHwnd())
		{
			m_ed_Barcode[iCnt].SetWindowText(szBarcode);
		}

		// 정해진 바코드가 모두 입력되면 2초후에 윈도우 숨김
		iCnt = (UINT)m_szarBarcodez.GetCount();
		if (iCnt == m_nBarcodeCnt)
		{
			Delay(1000);

			ShowWindow(SW_HIDE);
			return TRUE;
		}
		else
		{
			for (UINT nIdx = iCnt; nIdx < m_nBarcodeCnt; nIdx++)
			{
				if (FALSE == m_bUse[nIdx])
				{
					m_szarBarcodez.Add(STR_NO_BARCODE);
				}
				else
				{
					break;
				}
			}

			if (m_szarBarcodez.GetCount() == m_nBarcodeCnt)
			{
				Delay(1000);

				ShowWindow(SW_HIDE);
				return TRUE;
			}
		}
	}
	else if ((Barcode_Overap == enError) && (1 == m_nBarcodeCnt))
	{
		Delay(2000);

		ShowWindow(SW_HIDE);
		return TRUE;
	}
	else
	{
		//Error;
		return FALSE;
	}

	return FALSE;
}

//=============================================================================
// Method		: InsertBarcodeSocket
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nSocket
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2017/5/18 - 14:41
// Desc.		:
//=============================================================================
BOOL CDlg_Barcode::InsertBarcodeSocket(__in UINT nSocket, __in LPCTSTR szBarcode)
{
	if (FALSE == m_bUseSelectSocket)
		return FALSE;

	if (m_nBarcodeCnt <= nSocket)
		return FALSE;

	enBarcodeChk enError = CheckBarcodeSocket(nSocket, szBarcode);

	SetStatus(nSocket, enError);

	if (Barcode_Pass == enError)
	{
// 		if (1 == m_nBarcodeCnt)
// 		{
// 			//m_szarBarcodez.RemoveAll();
// 			iCnt = 0;
// 		}

		if (0 != m_szBarcodeSocket[nSocket].Compare(szBarcode))
		{
			if ((m_szBarcodeSocket[nSocket].IsEmpty()) || (0 == m_szBarcodeSocket[nSocket].Compare(STR_NO_BARCODE)))
			{
				++m_nInputedBarcodeCnt;
			}

			m_szBarcodeSocket[nSocket] = szBarcode;
		}

		if (GetSafeHwnd())
		{
			m_ed_Barcode[nSocket].SetWindowText(szBarcode);
		}

		// 정해진 바코드가 모두 입력되면 2초후에 윈도우 숨김		
		if (m_nInputedBarcodeCnt == m_bUseBarcodeCnt)
		{
			Delay(1000);

			ShowWindow(SW_HIDE);
			return TRUE;
		}
		else
		{
			if (m_nInputedBarcodeCnt == m_bUseBarcodeCnt)
			{
				Delay(1000);

				ShowWindow(SW_HIDE);
				return TRUE;
			}
		}
	}
	else if ((Barcode_Overap == enError) && (1 == m_nBarcodeCnt))
	{
		Delay(2000);

		ShowWindow(SW_HIDE);
		return TRUE;
	}
	else
	{
		//Error;
		return FALSE;
	}

	return FALSE;
}

//=============================================================================
// Method		: ResetBarcode
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/5 - 14:22
// Desc.		:
//=============================================================================
void CDlg_Barcode::ResetBarcode()
{
	if (FALSE == m_bUseSelectSocket)
	{
		m_szarBarcodez.RemoveAll();
	}
	else
	{
		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			if (TRUE == m_bUse[nIdx])
				m_szBarcodeSocket[nIdx].Empty();
			else
				m_szBarcodeSocket[nIdx] = STR_NO_BARCODE;
		}
		m_nInputedBarcodeCnt = 0;
	}

	for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
	{
		m_ed_Barcode[nIdx].SetWindowText(_T(""));
		m_st_Status[nIdx].SetText(_T(""));
		m_st_Status[nIdx].SetBackColor_COLORREF(RGB(255, 255, 255));
	}
}

//=============================================================================
// Method		: SetUsableSite
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nIdx
// Parameter	: __in BOOL bUse
// Qualifier	:
// Last Update	: 2016/11/5 - 20:04
// Desc.		:
//=============================================================================
void CDlg_Barcode::SetUsableSite(__in UINT nIdx, __in BOOL bUse)
{
	if (nIdx < MAX_SITE_CNT)
	{
		if (m_bUse[nIdx] != bUse)
		{
			m_bUse[nIdx] = bUse;

			if (bUse)
			{
				++m_bUseBarcodeCnt;
			}
			else
			{
				--m_bUseBarcodeCnt;
			}
		}

		
		if (TRUE == m_bUseSelectSocket)
		{
			if (FALSE == bUse)
			{
				if ((FALSE == m_szBarcodeSocket[nIdx].IsEmpty()) && (m_szBarcodeSocket[nIdx].Compare(STR_NO_BARCODE)))
				{
					--m_nInputedBarcodeCnt;
				}

				m_szBarcodeSocket[nIdx] = STR_NO_BARCODE;

				if (GetSafeHwnd())
				{
					m_ed_Barcode[nIdx].SetWindowText(_T(""));
				}
			}
		}
	}
}

//=============================================================================
// Method		: SetUseSelectSocket
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bUse
// Qualifier	:
// Last Update	: 2017/5/18 - 14:50
// Desc.		:
//=============================================================================
void CDlg_Barcode::SetUseSelectSocket(__in BOOL bUse)
{
	m_bUseSelectSocket = bUse;
}

//=============================================================================
// Method		: SetSelectSocket
// Access		: public  
// Returns		: void
// Parameter	: __in INT nSocket
// Qualifier	:
// Last Update	: 2017/5/18 - 14:50
// Desc.		:
//=============================================================================
void CDlg_Barcode::SetSelectSocket(__in INT nSocket)
{
	if (FALSE == m_bUseSelectSocket)
		return;

	if ((INT)m_nBarcodeCnt <= nSocket)
		return;

	if (m_iSelectedSocket != nSocket)
	{
		if (0 <= m_iSelectedSocket)
		{
			m_st_Site[m_iSelectedSocket].SetBackColor_COLORREF(RGB(0, 0, 0));
		}
	}

	if (nSocket < 0)
	{
		m_iSelectedSocket = -1;
	}
	else
	{
		m_iSelectedSocket = nSocket;
		m_st_Site[m_iSelectedSocket].SetBackColor_COLORREF(RGB(255, 200, 0));
		m_ed_Barcode[m_iSelectedSocket].SetFocus();
	}
}
