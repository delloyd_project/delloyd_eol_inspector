﻿//*****************************************************************************
// Filename	: 	Def_ErrorCode.h
// Created	:	2016/10/31 - 0:48
// Modified	:	2016/10/31 - 0:48
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_ErrorCode_h__
#define Def_ErrorCode_h__

#include <afxwin.h>

typedef enum enEquipErrorCode
{
	Err_EMO,				
	Err_MainPower,
	Err_MainPress,
	Err_AreaSensor,			
	Err_DoorSensor,
	Err_NoOrigin,
	Err_UserStop,
	
};

static LPCTSTR	g_szEquipErrorCode[] =
{
	_T("The E.M.O status. Program Restart!"),
	_T("The Main Power is off. Program Restart!"),
	_T("There's not enough Main Press."),
	_T("The Safety Sensor is detected."),
	_T("The Door Sensor is detected."),
	_T("Particle Part is located inside. turn off the machine.\n and move the camera part to the loading position by hand\n then turn on machine and restart the program"),
	NULL
};

// 오류 발생 시 조치법
static LPCTSTR g_szErrorCode_H_Desc[] = 
{
	_T("Fail"),
	_T("Pass"),
	_T("EMO ! Adjust the origin position after power on.\r\n Please Retry the product currently being inspected."),
	_T("Detect Area Sensor"),
	_T("Detect Front Door Sensor"),
	_T("Detect Left Door Sensor"),
	_T("Detect Right Door Sensor"),
	_T("Detect Rear Door Sensor"),
	_T("Detect Elericity Sensor"),
	_T("Detect Motor Index Sensor"),
	_T("연속 불량 알람"),
	_T("서버와의 통신이 끊김"),

	_T("Motor Error"),
	_T("Comm Err"),
	_T("주변 장치 오류"),
	
	NULL
};


#endif // Def_ErrorCode_h__
