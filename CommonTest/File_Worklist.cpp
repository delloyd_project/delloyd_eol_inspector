﻿//*****************************************************************************
// Filename	: 	File_Worklist.cpp
// Created	:	2017/2/28 - 17:34
// Modified	:	2017/2/28 - 17:34
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "StdAfx.h"
#include "File_Worklist.h"
#include "CommonFunction.h"

CFile_Worklist::CFile_Worklist()
{
}


CFile_Worklist::~CFile_Worklist()
{
}

//=============================================================================
// Method		: Save_FinalResult
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szMESPath
// Parameter	: __in SYSTEMTIME * pTime
// Parameter	: __in const ST_MES_FinalResult * pstWorklist
// Qualifier	:
// Last Update	: 2017/2/28 - 17:36
// Desc.		:
//=============================================================================
BOOL CFile_Worklist::Save_FinalResult(__in LPCTSTR szMESPath, __in SYSTEMTIME* pTime, __in const ST_MES_FinalResult* pstWorklist)
{
	BOOL bReturn = TRUE;
	CString szFullPath;
	CString szWorklist = Make_FinalResult(pstWorklist);
	CString szBuff;

	// ...\2016-11-17\Default_A\LOT
	CString szDatePath;
	szDatePath.Format(_T("%s%04d_%02d_%02d\\%s\\%s\\"), szMESPath, pTime->wYear, pTime->wMonth, pTime->wDay, pstWorklist->Model, pstWorklist->LOTName);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%s%s_%04d%02d%02d_%s_FinalResult.csv"), szDatePath, pstWorklist->LOTName, pTime->wYear, pTime->wMonth, pTime->wDay, pstWorklist->Equipment);

	CFile File;
	CFileException e;

	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		// #ifdef _UNICODE
		// 		WORD mode = 0xFEFF;
		// 		File.Write(&mode, sizeof(WORD));
		// #endif

		// Header 추가
		CString szHeader = Make_FinalResult_Header(&pstWorklist->ItemHeaderz) + _T("\r\n");
		szBuff = szHeader + szWorklist;
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		szBuff = szWorklist;
	}

	File.SeekToEnd();
	File.Write(szBuff.GetBuffer(), szBuff.GetLength() * sizeof(TCHAR));
	File.Flush();
	szBuff.ReleaseBuffer();

	File.Close();

	return bReturn;
}

//=============================================================================
// Method		: Save_TestItemLog
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szMESPath
// Parameter	: __in SYSTEMTIME * pTime
// Parameter	: __in const ST_MES_TestItemLog * pstWorklist
// Parameter	: __in LPCTSTR szTestName
// Parameter	: __in BOOL bAddSpaceLine
// Qualifier	:
// Last Update	: 2017/2/28 - 17:36
// Desc.		:
//=============================================================================
BOOL CFile_Worklist::Save_TestItemLog(__in LPCTSTR szMESPath, __in SYSTEMTIME* pTime, __in const ST_MES_TestItemLog* pstWorklist, __in LPCTSTR szTestName, __in BOOL bAddSpaceLine /*= FALSE*/)
{
	BOOL bReturn = TRUE;
	CString szFullPath;
	CString szWorklist = Make_TestItemLog(pstWorklist);
	CString szBuff;

	if (bAddSpaceLine)
	{
		szWorklist = _T("\r\n") + szWorklist;
	}

	// ...\2016-11-17\Default_A\LOT
	CString szDatePath;	
	szDatePath.Format(_T("%s%04d_%02d_%02d\\%s\\%s\\"), szMESPath, pTime->wYear, pTime->wMonth, pTime->wDay, pstWorklist->Model, pstWorklist->LOTName);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%s%s_%04d%02d%02d_%s_%s.csv"), szDatePath, pstWorklist->LOTName, pTime->wYear, pTime->wMonth, pTime->wDay, pstWorklist->Equipment, szTestName);

	CFile File;
	CFileException e;

	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		// #ifdef _UNICODE
		// 			WORD mode = 0xFEFF;
		// 			File.Write(&mode, sizeof(WORD));
		// #endif

		// Header 추가
		CString szHeader = Make_TestItemLog_Header(&pstWorklist->ItemHeaderz) + _T("\r\n");
		szBuff = szHeader + szWorklist;
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		szBuff = szWorklist;
	}

	File.SeekToEnd();
	File.Write(szBuff.GetBuffer(), szBuff.GetLength() * sizeof(TCHAR));
	File.Flush();
	szBuff.ReleaseBuffer();

	File.Close();

	return bReturn;
}

//=============================================================================
// Method		: Save_EEPROM_Log
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in SYSTEMTIME * pTime
// Parameter	: __in ST_MES_TestItemLog * pstWorklist
// Parameter	: __in LPCTSTR szTestName
// Parameter	: __in DWORD dwStartAddr
// Parameter	: __in DWORD dwEndAddr
// Parameter	: __in char * pDataz
// Parameter	: __in DWORD dwLength
// Qualifier	:
// Last Update	: 2017/2/28 - 17:56
// Desc.		:
//=============================================================================
BOOL CFile_Worklist::Save_EEPROM_Log(__in LPCTSTR szPath, __in SYSTEMTIME* pTime, __in ST_MES_TestItemLog* pstWorklist, __in LPCTSTR szTestName, __in DWORD dwStartAddr, __in DWORD dwEndAddr, __in char* pDataz, __in DWORD dwLength)
{
	CString szMESPath;
	szMESPath = szPath;

	// 	BOOL bReturn = TRUE;
	CString szFullPath;
	CString szWorklist;
	CString szBuff = Make_EEPROM_String(pstWorklist, szTestName, dwStartAddr, dwEndAddr, pDataz, dwLength);

	CString szDatePath;	
	szDatePath.Format(_T("%s%04d_%02d_%02d\\%s\\%s\\"), szMESPath, pTime->wYear, pTime->wMonth, pTime->wDay, pstWorklist->Model, pstWorklist->LOTName);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%s%s_%04d%02d%02d%02d_%s_%s.csv"), szDatePath, pstWorklist->LOTName, pTime->wYear, pTime->wMonth, pTime->wDay, pTime->wHour, pstWorklist->Equipment, szTestName);


	// File Open
	CFile File;
	CFileException e;

	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		// #ifdef _UNICODE
		// 		WORD mode = 0xFEFF;
		// 		File.Write(&mode, sizeof(WORD));
		// #endif

		// Header 추가
		CString szHeader = Make_TestItemLog_Header(&pstWorklist->ItemHeaderz) + _T("\r\n");
		szBuff = szHeader + szBuff;
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}
	}

	File.SeekToEnd();
	File.Write(szBuff.GetBuffer(), szBuff.GetLength() * sizeof(TCHAR));
	File.Flush();
	szBuff.ReleaseBuffer();

	File.Close();

	return TRUE;
}
