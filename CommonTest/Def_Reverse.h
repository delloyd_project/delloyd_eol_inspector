﻿#ifndef Def_T_Reverse_h__
#define Def_T_Reverse_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

#pragma pack(push,1)

typedef enum enReverse_MarkColor
{
	Rv_Mark_B = 0,
	Rv_Mark_W,
	Rv_Mark_Max,
};

static LPCTSTR	g_szReverseMarkColor[] =
{
	_T("●"),
	_T("○"),
	NULL
};

typedef enum enROI_Reverse
{
	ROI_Rv_Object_LEFT_TOP,
	ROI_Rv_Object_RIGHT_TOP,
	ROI_Rv_Object_LEFT_BOTTOM,
	ROI_Rv_Object_RIGHT_BOTTOM,
	ROI_Rv_Max,
};

static LPCTSTR g_szRoiReverse[] =
{
	_T("Left Top"),
	_T("Right Top"),
	_T("Left Bottom"),
	_T("Right Bottom"),
	NULL
};


typedef enum enROI_ReverseResult
{
	ROI_Rv_original,
	ROI_Rv_mirror,
	ROI_Rv_flip,
	ROI_Rv_rotate,
	ROI_Rv_resultMax,
};

static LPCTSTR g_szcamstate[] =
{
	_T("Original"),
	_T("Horizontal Flip"),
	_T("Vertical Flip"),
	_T("Rotation"),
	NULL
};


typedef enum enROI_ReverseRGB
{
	ReverseRGB_Red,
	ReverseRGB_Green,
	ReverseRGB_Blue,
	ReverseRGB_Black,
	ReverseRGB_Max,
};

static LPCTSTR g_szReverseRGB[] =
{
	_T("Red"),
	_T("Green"),
	_T("Blue"),
	_T("Black"),
	NULL
};

// ROI
typedef struct _tag_RegionReverse
{
	// ROI 영역 ( 카메라 [Send] )
	CRect	rtRoi;
	UINT	nMarkColor;
	UINT	nCamState;

	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp = rtRoi;

		rtRoi.left	 = iPosX - (int)(rtTemp.Width() / 2);
		rtRoi.right  = rtRoi.left + rtTemp.Width();;
		rtRoi.top	 = iPosY - (int)(rtTemp.Height() / 2);
		rtRoi.bottom = rtRoi.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtRoi;

		rtRoi.left   = rtTemp.CenterPoint().x - (iWidth / 2);
		rtRoi.right  = rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtRoi.top	 = rtTemp.CenterPoint().y - (iHeight / 2);
		rtRoi.bottom = rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};

	void Reset()
	{
		nMarkColor = Rv_Mark_B;
		nCamState = ROI_Rv_original;


		rtRoi.SetRectEmpty();
	};

	_tag_RegionReverse()
	{
		Reset();
	};

	_tag_RegionReverse& operator= (_tag_RegionReverse& ref)
	{
		rtRoi		= ref.rtRoi;
		nMarkColor	= ref.nMarkColor;
		nCamState	= ref.nCamState;

		return *this;
	};

}ST_RegionReverse, *PST_RegionReverse;

// Sub
typedef struct _tag_RegionReverseSub
{
	UINT	nRGBType[ReverseRGB_Max];

	void Reset()
	{
		for (int i = 0; i < ReverseRGB_Max; i++)
		{
			nRGBType[i] = ReverseRGB_Red;
		}

	};

	_tag_RegionReverseSub()
	{
		Reset();
	};

	_tag_RegionReverseSub& operator= (_tag_RegionReverseSub& ref)
	{
		for (int i = 0; i < ReverseRGB_Max; i++)
		{
			nRGBType[i] = ref.nRGBType[i];
		}

		return *this;
	};

}ST_RegionReverseSub, *PST_RegionReverseSub;


typedef struct _tag_Reverse_Opt
{
	ST_RegionReverse	stRegionOp[ROI_Rv_Max];
	ST_RegionReverse	stTestRegionOp[ROI_Rv_Max];
	ST_RegionReverseSub	stRegionSubOp[ROI_Rv_resultMax];

	int iSelectROI;
	UINT nStateType;

	_tag_Reverse_Opt()
	{
		for (UINT nIdx = 0; nIdx < ROI_Rv_Max; nIdx++)
		{
			stRegionOp[nIdx].Reset();
			stTestRegionOp[nIdx].Reset();
		}

		for (UINT nIdx = 0; nIdx < ROI_Rv_resultMax; nIdx++)
		{
			stRegionSubOp[nIdx].Reset();
		}

		nStateType = 0;
		iSelectROI = -1;
	};

	void Reset()
	{
		for (UINT nIdx = 0; nIdx < ROI_Rv_Max; nIdx++)
		{
			stRegionOp[nIdx].Reset();
		}

		for (UINT nIdx = 0; nIdx < ROI_Rv_resultMax; nIdx++)
		{
			stRegionSubOp[nIdx].Reset();
		}

		iSelectROI = -1;
		nStateType = 0;
	};

	_tag_Reverse_Opt& operator= (_tag_Reverse_Opt& ref)
	{
		for (UINT nIdx = 0; nIdx < ROI_Rv_Max; nIdx++)
		{
			stRegionOp[nIdx] = ref.stRegionOp[nIdx];
			stTestRegionOp[nIdx] = ref.stTestRegionOp[nIdx];
		}

		for (UINT nIdx = 0; nIdx < ROI_Rv_resultMax; nIdx++)
		{
			stRegionSubOp[nIdx] = ref.stRegionSubOp[nIdx];
		}

		iSelectROI = ref.iSelectROI;
		nStateType = ref.nStateType;

		return *this;
	};

}ST_Reverse_Opt, *PST_Reverse_Opt;


typedef struct _tag_Reverse_Result
{
	// 최종 결과
	UINT nResult;
	UINT nEachResult[ROI_Rv_Max];

	CPoint ptCenter[ROI_Rv_Max];

	BOOL RectROIdata[ROI_Rv_Max];

	UINT nCamState;


	int iSelectROI;

	_tag_Reverse_Result()
	{
		nResult = 3;
		
		for (UINT nIdx = 0; nIdx < ROI_Rv_Max; nIdx++)
		{
			nEachResult[nIdx] = 3;
			ptCenter[nIdx].x = -1;
			ptCenter[nIdx].y = -1;
			RectROIdata[nIdx] = 0;
		}

		iSelectROI = -1;
		nCamState = 0;
	};

	void Reset()
	{
		nResult = 3;

		for (UINT nIdx = 0; nIdx < ROI_Rv_Max; nIdx++)
		{
			nEachResult[nIdx] = 2;
			ptCenter[nIdx].x = -1;
			ptCenter[nIdx].y = -1;
			RectROIdata[nIdx] = 0;
		}

		iSelectROI = -1;
		nCamState = 0;
	};

	_tag_Reverse_Result& operator= (_tag_Reverse_Result& ref)
	{
		nResult	 = ref.nResult;

		for (UINT nIdx = 0; nIdx < ROI_Rv_Max; nIdx++)
		{
			nEachResult[nIdx] = ref.nEachResult[nIdx];
			ptCenter[nIdx].x = ref.ptCenter[nIdx].x;
			ptCenter[nIdx].y = ref.ptCenter[nIdx].y;
			RectROIdata[nIdx] = ref.RectROIdata[nIdx];
		}

		iSelectROI = ref.iSelectROI;
		nCamState = ref.nCamState;

		return *this;
	};

}ST_Reverse_Result, *PST_Reverse_Result;

#pragma pack(pop)

#endif // Def_Reverse_h__

