﻿#ifndef Def_FFT_h__
#define Def_FFT_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

#pragma pack(push,1)

typedef enum enFFT_Channel
{
	FFTCh1,
	FFTCh_Max,
};

static LPCTSTR g_szFFT_Channel[] =
{
	_T("Channel 1"),
	NULL
};

typedef struct _tag_FFT_Opt
{
	// 양불 기준
	double dSpecMin[FFTCh_Max];
	double dSpecMax[FFTCh_Max];

	double dbOffset[FFTCh_Max];
	UINT nTestTime[FFTCh_Max];

	CString szWaveFile;
	int iVolume;

	// 변수 초기화 함수
	_tag_FFT_Opt()
	{
		for (int i = 0; i < FFTCh_Max; i++)
		{
			dSpecMin[i] = 0;
			dSpecMax[i] = 1;
			dbOffset[i] = 1.0;
			nTestTime[i] = 1000;
		}
		
		iVolume = 50;
	};

	// 변수 교환 함수
	_tag_FFT_Opt& operator= (_tag_FFT_Opt& ref)
	{
		for (int i = 0; i < FFTCh_Max; i++)
		{
			dSpecMin[i] = ref.dSpecMin[i];
			dSpecMax[i] = ref.dSpecMax[i];
			dbOffset[i] = ref.dbOffset[i];
			nTestTime[i] = ref.nTestTime[i];
		}

		szWaveFile = ref.szWaveFile;
		iVolume = ref.iVolume;
		
		return *this;
	};

}ST_FFT_Opt, *PST_FFT_Opt;

typedef struct _tag_FFT_Result
{
	// 결과
	UINT	nResult;
	UINT	nEachResult[FFTCh_Max];

	// 전류 값
	double 	dValue[FFTCh_Max];

	void Reset()
	{
		nResult = 3;

		for (int i = 0; i < FFTCh_Max; i++)
		{
			nEachResult[i] = FALSE;
			dValue[i] = 0;
		}
	};

	// 변수 초기화 함수
	_tag_FFT_Result()
	{
		Reset();
	};

	// 변수 교환 함수
	_tag_FFT_Result& operator= (_tag_FFT_Result& ref)
	{
		nResult = ref.nResult;

		for (int i = 0; i < FFTCh_Max; i++)
		{
			nEachResult[i] = ref.nEachResult[i];
			dValue[i] = ref.dValue[i];
		}


		return *this;
	};

}ST_FFT_Result, *PST_FFT_Result;

#pragma pack(pop)

#endif // Def_FFT_h__