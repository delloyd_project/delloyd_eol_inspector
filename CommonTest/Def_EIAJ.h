﻿#ifndef Def_Resolution_h__
#define Def_Resolution_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

typedef enum enListItemNum_ReOp
{
	ReOp_CenterBlack,
	ReOp_CenterWhite,
	ReOp_CenterLeft,
	ReOp_CenterRight,
	ReOp_CenterUp,
	ReOp_CenterDown,
	ReOp_Side1st,
	ReOp_Side2st,
	ReOp_Side3st,
	ReOp_Side4st,
	ReOp_Side5st,
	ReOp_Side6st,
	ReOp_Side7st,
	ReOp_Side8st,
	ReOp_ItemNum,
};

static LPCTSTR g_szResolutionRegion[] =
{
	_T("BK"),
	_T("WH"),
	_T("C_L"),
	_T("C_R"),
	_T("C_T"),
	_T("C_B"),
	_T("S1"),
	_T("S2"),
	_T("S3"),
	_T("S4"),
	_T("S5"),
	_T("S6"),
	_T("S7"),
	_T("S8"),
	NULL
};

typedef enum enResolutionMode
{
	Resol_Mode_Bottom_Top = 0,
	Resol_Mode_Top_Bottom,
	Resol_Mode_Left_Right,
	Resol_Mode_Right_Left,
	Resol_Mode_NO
};

static LPCTSTR g_szResolutionMode[] =
{
	_T("▲ BT"),
	_T("▼ TB"),
	_T("▶ LR"),
	_T("◀ RL"),
	_T("NULL"),
	NULL
};

// 해상력 각각에 대한 영역
typedef struct _tag_RegionResolution
{
	/*영역 선택*/
	BOOL	bUse;
	/*영역*/
	CRect	RegionList;
	/*Mtf 수치*/
	UINT    i_MtfRatio;
	/*TEST 양품 범위 min*/
	UINT    i_Threshold_Min;
	/*TEST 양품 범위 max*/
	UINT    i_Threshold_Max;
	/*TEST 범위 min*/
	UINT    i_Range_Min;
	/*TEST 범위 max*/
	UINT    i_Range_Max;
	UINT	i_Mode;
	UINT    i_offset;

	/*TEST 시 사용 변수*/
	int		m_PosX, m_PosY;
	int		SFR_Width;
	int		SFR_Height;
	double	*SFR_MTF_Value;
	double  *SFR_MTF_LineFitting_Value;
	double  *SFR_BWRatio;
	int		checkLine_Buffer[30];
	int		checkLine_Buffer_cnt;

	/*Pic 시 사용 변수*/
	CPoint	 PatternStartPos, PatternEndPos;
	int		CheckLine;
	int		Result_EdgeDelta;

	/*결과 Data*/
	int		Result_data;
	/*결과*/
	BOOL	bResult;

	_tag_RegionResolution()
	{
		RegionList.left = 0;
		RegionList.top = 0;
		RegionList.right = 0;
		RegionList.bottom = 0;

		i_offset = 0;
		bUse = TRUE;

		i_MtfRatio = 10;
		i_Threshold_Min = 300;
		i_Threshold_Max = 500;
		i_Range_Min = 200;
		i_Range_Max = 500;
		i_Mode = 0;
		SFR_Width = 0;
		SFR_Height = 0;

		SFR_MTF_Value = NULL;
		SFR_BWRatio = NULL;
		SFR_MTF_LineFitting_Value = NULL;

		ZeroMemory(checkLine_Buffer, sizeof(int)* 30);
		checkLine_Buffer_cnt = 0;

		/*Pic 시 사용 변수*/
		PatternStartPos.x = 0;
		PatternStartPos.y = 0;
		PatternEndPos.x = 0;
		PatternEndPos.y = 0;

		/*결과 Data*/
		Result_data = 0;
		bResult = TR_Init;
		Result_EdgeDelta = 0;
	};

	~_tag_RegionResolution()
	{

	};

	void Reset()
	{
		RegionList.left = 0;
		RegionList.top = 0;
		RegionList.right = 0;
		RegionList.bottom = 0;
		i_offset = 0;

		bUse = TRUE;

		i_MtfRatio = 10;
		i_Threshold_Min = 300;
		i_Threshold_Max = 500;
		i_Range_Min = 200;
		i_Range_Max = 500;
		i_Mode = 0;
		SFR_Width = 0;
		SFR_Height = 0;

		if (NULL != SFR_MTF_Value)
		{
			delete[] SFR_MTF_Value;
			SFR_MTF_Value = NULL;
		}

		if (NULL != SFR_BWRatio)
		{
			delete[] SFR_BWRatio;
			SFR_BWRatio = NULL;
		}

		if (NULL != SFR_MTF_LineFitting_Value)
		{
			delete[] SFR_MTF_LineFitting_Value;
			SFR_MTF_LineFitting_Value = NULL;
		}

		ZeroMemory(checkLine_Buffer, sizeof(int)* 30);
		checkLine_Buffer_cnt = 0;

		/*Pic 시 사용 변수*/
		PatternStartPos.x = 0;
		PatternStartPos.y = 0;
		PatternEndPos.x = 0;
		PatternEndPos.y = 0;

		/*결과 Data*/
		Result_data = 0;
		bResult = FALSE;
		Result_EdgeDelta = 0;
	};

	void DataReset()
	{
		if (NULL != SFR_MTF_Value)
		{
			delete[] SFR_MTF_Value;
			SFR_MTF_Value = NULL;
		}

		if (NULL != SFR_BWRatio)
		{
			delete[] SFR_BWRatio;
			SFR_BWRatio = NULL;
		}

		if (NULL != SFR_MTF_LineFitting_Value)
		{
			delete[] SFR_MTF_LineFitting_Value;
			SFR_MTF_LineFitting_Value = NULL;
		}

		/*Pic 시 사용 변수*/
		PatternStartPos.x = 0;
		PatternStartPos.y = 0;
		PatternEndPos.x = 0;
		PatternEndPos.y = 0;

		/*결과 Data*/
		Result_data = 0;
		bResult = FALSE;
		Result_EdgeDelta = 0;
	};

	void _Rect_Position_Sum(int X, int Y, int W, int H)
	{
		RegionList.top = Y - (int)(H / 2);
		RegionList.bottom = RegionList.top + H;

		RegionList.left = X - (int)(W / 2);
		RegionList.right = RegionList.left + W;
	};

	void _Rect_Position_Sum2(int W, int H)
	{
		CRect data = RegionList;
		int posX = data.CenterPoint().x;
		int posY = data.CenterPoint().y;

		int halfX = W / 2;
		int halfY = H / 2;

		if (W % 2 == 0)
		{
			RegionList.left = posX - halfX;
			RegionList.right = posX + halfX;
		}
		else
		{
			RegionList.left = posX - halfX;
			RegionList.right = posX + halfX + 1;
		}

		if (H % 2 == 0)
		{
			RegionList.top = posY - halfY;
			RegionList.bottom = posY + halfY;
		}
		else
		{
			RegionList.top = posY - halfY;
			RegionList.bottom = posY + halfY + 1;
		}
	};

	_tag_RegionResolution& operator= (_tag_RegionResolution& ref)
	{
		bUse = ref.bUse;
		RegionList = ref.RegionList;

		m_PosX = ref.m_PosX;
		m_PosY = ref.m_PosY;

		i_MtfRatio = ref.i_MtfRatio;
		i_Threshold_Min = ref.i_Threshold_Min;
		i_Threshold_Max = ref.i_Threshold_Max;
		i_Range_Min = ref.i_Range_Min;
		i_Range_Max = ref.i_Range_Max;
		i_Mode = ref.i_Mode;

		SFR_Width = ref.SFR_Width;
		SFR_Height = ref.SFR_Height;

		i_offset = ref.i_offset;


		// ****  수정해야 함 ****
	/*	if (NULL != SFR_MTF_Value)
		{
			delete[] SFR_MTF_Value;
			SFR_MTF_Value = NULL;
		}

		if (NULL != SFR_BWRatio)
		{
			delete[] SFR_BWRatio;
			SFR_BWRatio = NULL;
		}

		if (NULL != SFR_MTF_LineFitting_Value)
		{
			delete[] SFR_MTF_LineFitting_Value;
			SFR_MTF_LineFitting_Value = NULL;
		}

		if (NULL == ref.SFR_MTF_Value)
		{
			SFR_MTF_Value = NULL;
		}
		else
		{
			memcpy(SFR_MTF_Value, ref.SFR_MTF_Value, _msize(ref.SFR_MTF_Value));
		}

		if (NULL == ref.SFR_BWRatio)
		{
			SFR_BWRatio = NULL;
		}
		else
		{
			memcpy(SFR_BWRatio, ref.SFR_BWRatio, _msize(ref.SFR_BWRatio));
		}

		if (NULL == ref.SFR_MTF_LineFitting_Value)
		{
			SFR_MTF_LineFitting_Value = NULL;
		}
		else
		{
			memcpy(SFR_MTF_LineFitting_Value, ref.SFR_MTF_LineFitting_Value, _msize(ref.SFR_MTF_LineFitting_Value));
		}*/

		for (int t = 0; t < 30; t++)
		{
			checkLine_Buffer[t] = ref.checkLine_Buffer[t];
		}
		checkLine_Buffer_cnt = ref.checkLine_Buffer_cnt;

		/*Pic 시 사용 변수*/
		PatternStartPos = ref.PatternStartPos;
		PatternEndPos = ref.PatternEndPos;
		/*결과 Data*/
		Result_data = ref.Result_data;
		bResult = ref.bResult;
		Result_EdgeDelta = ref.Result_EdgeDelta;

		return *this;
	};

}ST_RegionResolution, *PST_ZoneResolution;


typedef struct _tag_ResolRegion
{
	CRect	RegionList;

	_tag_ResolRegion()
	{
		Reset();
	};

	void Reset()
	{
		RegionList.SetRectEmpty();
	};

	_tag_ResolRegion& operator= (_tag_ResolRegion& ref)
	{
		RegionList = ref.RegionList;

		return *this;
	};

	void _Rect_Position_Sum(int X, int Y, int W, int H)
	{
		RegionList.top = Y - (int)(H / 2);
		RegionList.bottom = RegionList.top + H - 1;

		RegionList.left = X - (int)(W / 2);
		RegionList.right = RegionList.left + W - 1;
	};

	void _Rect_Position_Sum2(int W, int H)
	{
		CRect data = RegionList;
		int posX = data.CenterPoint().x;
		int posY = data.CenterPoint().y;

		int halfX = W / 2;
		int halfY = H / 2;

		if (W % 2 == 0)
		{
			RegionList.left = posX - halfX;
			RegionList.right = posX + halfX;
		}
		else
		{
			RegionList.left = posX - halfX;
			RegionList.right = posX + halfX + 1;
		}

		if (H % 2 == 0)
		{
			RegionList.top = posY - halfY;
			RegionList.bottom = posY + halfY;
		}
		else
		{
			RegionList.top = posY - halfY;
			RegionList.bottom = posY + halfY + 1;
		}
	};

}ST_ResolRegion, *PST_ResolRegion;

typedef struct _tag_Resolution_Op
{
	/*TEST 시 사용하는 영역*/
	ST_RegionResolution	StdrectData[ReOp_ItemNum];
	ST_RegionResolution	rectData[ReOp_ItemNum];

	/*TEST 시 기준 영역*/
	ST_RegionResolution	StandardrectData[ReOp_ItemNum];

	BOOL bGViewMode;

	void DataReset()
	{
		for (int i = 0; i < ReOp_ItemNum; i++)
		{
			rectData[i].DataReset();
		}
	};

	_tag_Resolution_Op()
	{
		bGViewMode = FALSE;
	};

	_tag_Resolution_Op& operator= (_tag_Resolution_Op& ref)
	{
		for (UINT nIdx = 0; nIdx < ReOp_ItemNum; nIdx++)
		{
			rectData[nIdx] = ref.rectData[nIdx];
			StandardrectData[nIdx] = ref.StandardrectData[nIdx];
		}

		bGViewMode = ref.bGViewMode;

		return *this;
	};

}ST_EIAJ_Op, *PST_Resolution_Op;

typedef struct _tag_Resolution_Data
{
	/*결과*/
	UINT    nResult;
	UINT    nEachResult[ReOp_ItemNum];
	UINT    nValueResult[ReOp_ItemNum];

	/*합격된 영역 Cnt*/
	UINT    nResult_cnt;

	/*검사 영역 Cnt*/
	UINT    nTotal_cnt;

	BOOL	bUse[ReOp_ItemNum];

	_tag_Resolution_Data()
	{

		nResult = TR_Init;
		nResult_cnt = 0;
		nTotal_cnt = 0;

		for (UINT nIdex = 0; nIdex < ReOp_ItemNum; nIdex++)
		{
			nEachResult[nIdex] = 0;
			nValueResult[nIdex] = 0;
			bUse[nIdex] = FALSE;
		}
	};

	void Reset()
	{
		nResult = TR_Init;
		nResult_cnt = 0;
		nTotal_cnt = 0;

		for (UINT nIdex = 0; nIdex < ReOp_ItemNum; nIdex++)
		{
			nEachResult[nIdex] = 0;
			nValueResult[nIdex] = 0;
			bUse[nIdex] = FALSE;
		}
	};


	_tag_Resolution_Data& operator= (_tag_Resolution_Data& ref)
	{
		nResult = ref.nResult;
		nResult_cnt = ref.nResult_cnt;
		nTotal_cnt = ref.nTotal_cnt;

		for (UINT nIdex = 0; nIdex < ReOp_ItemNum; nIdex++)
		{
			nEachResult[nIdex] = ref.nEachResult[nIdex];
			nValueResult[nIdex] = ref.nValueResult[nIdex];
			bUse[nIdex] = ref.bUse[nIdex];
		}
		return *this;
	};

}ST_EIAJ_Data, *PST_Resolution_Data;


#endif // Def_Resolution_h__