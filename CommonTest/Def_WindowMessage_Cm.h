﻿//*****************************************************************************
// Filename	: Def_WindowMessage_Cm.h
// Created	: 2012/1/16
// Modified	: 2016/08/17
//
// Author	: PiRing
//	
// Purpose	: 윈도우 메세지 정의 (0x0400 ~ 0x7FFF)
//*****************************************************************************
#ifndef Def_WindowMessage_Cm_h__
#define Def_WindowMessage_Cm_h__

//-------------------------------------------------------------------
// 프로그램 운영
//-------------------------------------------------------------------
#define		WM_ALIVE_PROCESS		(WM_USER + 108)

#define		WM_LOGMSG				WM_USER + 1		// 로그
#define		WM_OPTION				WM_USER + 2		// 옵션 창 열기
#define		WM_OPTION_CHANGED		WM_USER + 3		// 옵션이 변경됨
#define		WM_LOGMSG_DEV			WM_USER + 4		// 주변기기 로그 처리용
#define		WM_LOGMSG_PCB_CAM		WM_USER + 5		// 카메라 보드 로그 메세지
#define		WM_LOGMSG_PCB_LIGHT		WM_USER + 6		// 광원 보드 로그 메세지
#define		WM_LOGMSG_VISION_LIGHT	WM_USER + 7		// 광원 보드 로그 메세지

#define		WM_LOGMSG_HANDLER		WM_USER + 8		// 핸들러 로그 메세지
#define		WM_LOGMSG_TESTER		WM_USER + 9		// 테스터 로그 메세지

#define		WM_LOAD_COMPLETE		WM_USER + 10		// 프로그램 로딩 끝
#define		WM_TEST_FUNCTION		WM_USER + 11	// 테스트 함수 실행
#define		WM_SYSTEM_MODE			WM_USER + 12	// 프로그램 운용모드(wparam - 운용모드[관리자, LOT모드, MES모드] lParam - Type[LotStart, LotStop])

//-------------------------------------------------------------------
// 주변 기기 통신
//-------------------------------------------------------------------

#define		WM_RECV_LIGHT_BRD_ACK	WM_USER + 20	// 광원 보드로부터 데이터 수신
#define		WM_RECV_BARCODE			WM_USER + 21	// BCR로부터 데이터 수신
#define		WM_RECV_VIDEO			WM_USER + 22	// 카메라 영상 데이터가 영상보드로부터 수신됨
#define		WM_CAMERA_STATUS		WM_USER + 23	// 카메라 영상 데이터가 영상보드로부터 수신됨
											  
#define		WM_CAMERA_SELECT		WM_USER + 24	// 화면에 보여줄 Camera 선택
#define		WM_CAMERA_CHG_STATUS	WM_USER + 25	// 카메라 영상 상태가 변경됨
#define		WM_CAMERA_RECV_VIDEO	WM_USER + 26	// 카메라 영상 데이터가 영상보드로부터 수신됨
#define		WM_RECV_MES				WM_USER + 27	//
#define		WM_LOGMSG_LASER_SENSOR	WM_USER + 28	// 변위 센서
#define		WM_COMM_STATUS_VC		WM_USER + 29	// 비전 카메라 통신 상태
#define		WM_VC_RECV_VIDEO		WM_USER + 30	// 카메라 영상 데이터가 영상보드로부터 수신됨
#define		WM_RECV_DIO_BIT			WM_USER + 31	// DIO 신호 수신
#define		WM_RECV_DIO_FST_READ	WM_USER + 32	// DIO 신호 수신 : 처음 읽음

//-------------------------------------------------------------------
// 계측기 장비 제어
//-------------------------------------------------------------------
#define		WM_EXE_START			WM_USER + 40

#define		WM_CHANGE_VIEW			WM_USER + 41	// 윈도우 전환
#define		WM_PERMISSION_MODE		WM_USER + 42	// 관리자 모드 설정
#define		WM_TEST_START			WM_USER + 43	// 검사 시작
#define		WM_TEST_STOP			WM_USER + 44	// 검사 중지(검사 실패, 오류)
#define		WM_TEST_COMPLETED		WM_USER + 45	// 검사 완료, 제품 배출
#define		WM_TEST_INIT			WM_USER + 46	// 테스트 초기화

#define		WM_FILE_MODEL			WM_USER + 47	// 설정된 모델 폴더의 파일들이 변경,추가,삭제 되었음을 알림
#define		WM_CHANGED_MODEL		WM_USER + 48	// 모델 파일의 데이터가 변경 되었음을 알림
#define		WM_REFESH_MODEL			WM_USER + 49	// 모델 파일 리스트 갱신

#define		WM_MANUAL_DEV_CTRL		WM_USER + 50	// 주변장치 수동제어
#define		WM_MASTERSET_CTRL		WM_USER + 51	// 마스터셋 파라미터 저장

#define		WM_INCREASE_POGO_CNT	WM_USER + 61	// 포고 카운트 증가 (A~D)
#define		WM_UPDATE_POGO_CNT		WM_USER + 62	// 포고 카운트의 설정 데이터가 변경됨

#define		WM_CHANGED_SITENAME		WM_USER + 63	// Site 및 테스트 명칭이 변경됨
#define		WM_CHANGE_MODE			WM_USER + 64	// Lot & Model 변경
#define		WM_CHANGE_USERNAME		WM_USER + 65	// User or Operator Name 변경됨

#define		WM_EQP_INIT				WM_USER + 84	// 설비 초기화 (원점 수행)
#define		WM_EQP_OPER_MODE		WM_USER + 85	// 설비 생산 모드 변경

#define		WM_MANUAL_BARCODE		WM_USER + 66	// 수동 바코드 입력

#define		WM_UI_ERR_RESET			WM_USER + 98	// 에러 초기화

#define		WM_EXE_END				WM_USER + 99
//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
#define		WM_MOTOR_ETC_CONTROL	WM_USER + 201
#define		WM_LOGMSG_MOTOR_CAM		WM_USER + 202
#define		WM_UPDATE_MOTOR_PARAM	WM_USER + 203	// 모터 셋팅 값이 변경됨
#define		WM_MOTOR_ORIGIN			WM_USER + 204	// UI 상에서 사용되는 Alarm Clear Message [wparam - 제어 해당 축]
#define		WM_MOTOR_STOP			WM_USER + 205	// UI 상에서 사용되는 Alarm Clear Message [wparam - 제어 해당 축]
#define		WM_MOTOR_ALARM_CLEAR	WM_USER	+ 206	// UI 상에서 사용되는 Alarm Clear Message [wparam - 제어 해당 축]
#define		WM_MOTOR_MANUAL			WM_USER	+ 207	// UI 상에서 사용되는 메뉴얼 제어 Message [wparam - 제어 해당 축, lparam - 제어방법]
#define		WM_MOTOR_MOVE_POS		WM_USER	+ 208	// UI 상에서 사용되는 메뉴얼 제어 Message [wparam - 제어 해당 축, lparam - 제어 Pulse]
#define		WM_MEASURE_MANUAL		WM_USER + 209	// UI 상에서 사용되는 메뉴얼 제어 Message [wparam - 테스트(Site ID 또는 ParaID), lparam - 메뉴얼 측정 종류]
#define		WM_DRYRUN				WM_USER + 210	// DRY RUN 모드

#define		WM_MOTOR_LOADING		WM_USER + 211	// UI 상에서 사용되는 메뉴얼 제어 Message [wparam - 테스트(Site ID 또는 ParaID), lparam - 메뉴얼 측정 종류]
#define		WM_MOTOR_INSPECTION		WM_USER + 212	// UI 상에서 사용되는 메뉴얼 제어 Message [wparam - 테스트(Site ID 또는 ParaID), lparam - 메뉴얼 측정 종류]
#define		WM_MOTOR_PAR_LOAD		WM_USER + 213   // UI 상에서 사용되는 메뉴얼 제어 Message [wparam - 테스트(Site ID 또는 ParaID), lparam - 메뉴얼 측정 종류]
#define		WM_MOTOR_PAR_INSPECT	WM_USER + 214	// UI 상에서 사용되는 메뉴얼 제어 Message [wparam - 테스트(Site ID 또는 ParaID), lparam - 메뉴얼 측정 종류]
#define		WM_CYLINDER_PAR_IN		WM_USER + 215	// UI 상에서 사용되는 메뉴얼 제어 Message [wparam - 테스트(Site ID 또는 ParaID), lparam - 메뉴얼 측정 종류]
#define		WM_CYLINDER_PAR_OUT		WM_USER + 216	// UI 상에서 사용되는 메뉴얼 제어 Message [wparam - 테스트(Site ID 또는 ParaID), lparam - 메뉴얼 측정 종류]

#define		WM_CHANGE_PIC			WM_USER + 300	// PIC 상태 변환
#define		WM_ROI_SEL_PIC			WM_USER + 301	// PIC 상태 변환

#endif // Def_WindowMessage_Cm_h__
