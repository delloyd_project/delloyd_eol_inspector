﻿//*****************************************************************************
// Filename	: 	Def_MES_Cm.h
// Created	:	2016/9/6 - 14:07
// Modified	:	2016/9/6 - 14:07
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_MES_Cm_h__
#define Def_MES_Cm_h__

#include <afxwin.h>

#define	 LOT_ID_LENGTH		12

typedef enum enMES_ImgFormat
{
	EVMS_IMG_RAW,
	EVMS_IMG_BMP,
	EVMS_IMG_JPG,
};

static LPCTSTR g_szMES_ImgFormat[] =
{
	_T("raw"),
	_T("bmp"),
	_T("jpg"),
};


static LPCTSTR g_lpszHeader_TotalTestItem_EM[] =
{
	_T("Current Channel 1"),
	_T("Current Channel 2"),
	_T("Center Point X"),
	_T("Center Point Y"),
	_T("Rotation Degree"),
	_T("Color - Red"),
	_T("Color - Green"),
	_T("Color - Blue"),
	_T("Color - Black R"),
	_T("Color - Black G"),
	_T("Color - Black B"),
	_T("Angle L-C"),
	_T("Angle R-C"),
	_T("Angle T-C"),
	_T("Angle B-C"),
	_T("SFR Far S1"),
	_T("SFR Far S2"),
	_T("SFR Far S3"),
	_T("SFR Far S4"),
	_T("SFR Far S5"),
	_T("SFR Far S6"),
	_T("SFR Far S7"),
	_T("SFR Far S8"),
	_T("SFR Far S9"),
	_T("SFR Far S10"),
	_T("SFR Far S11"),
	_T("SFR Far S12"),
	_T("SFR Far S13"),
	_T("SFR Far S14"),
	_T("SFR Far S15"),
	_T("SFR Far S16"),
	_T("SFR Far S17"),
	_T("SFR Far S18"),
	_T("SFR Far S19"),
	_T("SFR Far S20"),
	_T("SFR Far S21"),
	_T("SFR Far S22"),
	_T("SFR Far S23"),
	_T("SFR Far S24"),
	_T("SFR Far S25"),
	_T("SFR Far S26"),
	_T("SFR Far S27"),
	_T("SFR Far S28"),
	_T("SFR Far S29"),
	_T("SFR Far S30"),
	_T("SFR Near S1"),
	_T("SFR Near S2"),
	_T("SFR Near S3"),
	_T("SFR Near S4"),
	_T("SFR Near S5"),
	_T("SFR Near S6"),
	_T("SFR Near S7"),
	_T("SFR Near S8"),
	_T("SFR Near S9"),
	_T("SFR Near S10"),
	_T("SFR Near S11"),
	_T("SFR Near S12"),
	_T("SFR Near S13"),
	_T("SFR Near S14"),
	_T("SFR Near S15"),
	_T("SFR Near S16"),
	_T("SFR Near S17"),
	_T("SFR Near S18"),
	_T("SFR Near S19"),
	_T("SFR Near S20"),
	_T("SFR Near S21"),
	_T("SFR Near S22"),
	_T("SFR Near S23"),
	_T("SFR Near S24"),
	_T("SFR Near S25"),
	_T("SFR Near S26"),
	_T("SFR Near S27"),
	_T("SFR Near S28"),
	_T("SFR Near S29"),
	_T("SFR Near S30"),
	_T("SFR IR S1"),
	_T("SFR IR S2"),
	_T("SFR IR S3"),
	_T("SFR IR S4"),
	_T("SFR IR S5"),
	_T("SFR IR S6"),
	_T("SFR IR S7"),
	_T("SFR IR S8"),
	_T("SFR IR S9"),
	_T("SFR IR S10"),
	_T("SFR IR S11"),
	_T("SFR IR S12"),
	_T("SFR IR S13"),
	_T("SFR IR S14"),
	_T("SFR IR S15"),
	_T("SFR IR S16"),
	_T("SFR IR S17"),
	_T("SFR IR S18"),
	_T("SFR IR S19"),
	_T("SFR IR S20"),
	_T("SFR IR S21"),
	_T("SFR IR S22"),
	_T("SFR IR S23"),
	_T("SFR IR S24"),
	_T("SFR IR S25"),
	_T("SFR IR S26"),
	_T("SFR IR S27"),
	_T("SFR IR S28"),
	_T("SFR IR S29"),
	_T("SFR IR S30"),
	_T("Particle Count"),
	_T("Brightness S1"),
	_T("Brightness S2"),
	_T("Brightness S3"),
	_T("Brightness S4"),
	_T("Sound"),
	NULL
};

//-----------------------------------------------------------------------------
// Worklist 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_MES_FinalResult
{
	CString		Time;
	CString		Equipment;
	CString		Model;
	CString		SWVersion;
	CString		LOTName;
	CString		Barcode;
	CString		Operator;
	CString		CamType;
	CString		Result;

	CStringArray	Itemz;
	CStringArray	ItemHeaderz;
}ST_MES_FinalResult;


typedef struct _tag_MES_TestItemLog
{
	CString		Time;
	CString		Equipment;
	CString		Model;
	CString		SWVersion;
	CString		LOTName;
	CString		Barcode;
	CString		Operator;
	CString		CamType;
	CString		Result;

	CStringArray	Itemz;
	CStringArray	ItemHeaderz;
}ST_MES_TestItemLog;


typedef struct _tag_EM_TotalTestItemLog
{
	CString		szIndex;
	CString		szTime;
	CString		szModel;
	CString		szLotID;
	CString		szOperator;
	CString		szBarcode;
	CString		szCamType;
	CString		szResult;

	CString		szItemz;
	CStringArray	ItemHeaderz;

}ST_EM_TotalTestItemLog;

//-----------------------------------------------------------------------------
// LOT ID 규칙
//-----------------------------------------------------------------------------
typedef struct _tag_LOTIDRule
{
	char	Location;
	char	Type;
	char	ProductCode[5];
	char	Year;
	char	Month;
	char	Day;
	char	SerialNo[2];
}ST_LOTIDRule, *PST_LOTIDRule;


#endif // Def_MES_Cm_h__
