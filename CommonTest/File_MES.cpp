﻿//*****************************************************************************
// Filename	: 	File_MES.cpp
// Created	:	2016/11/18 - 18:16
// Modified	:	2016/11/18 - 18:16
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "File_MES.h"
#include "CommonFunction.h"


CFile_MES::CFile_MES()
{
	m_szEquipmentID	= GetStationName();
}

CFile_MES::~CFile_MES()
{
}

//=============================================================================
// Method		: Conv_SYSTEMTIME2String
// Access		: protected  
// Returns		: CString
// Parameter	: __in SYSTEMTIME * pTime
// Qualifier	:
// Last Update	: 2016/11/18 - 19:55
// Desc.		:
//=============================================================================
CString CFile_MES::Conv_SYSTEMTIME2String(__in SYSTEMTIME* pTime)
{
	CString szTime;
	// 2016-09-09 15:04:19.030
	szTime.Format(_T("%04d-%02d-%02d %02d-%02d-%02d-%03d"), pTime->wYear, pTime->wMonth, pTime->wDay, pTime->wHour, pTime->wMinute, pTime->wSecond, pTime->wMilliseconds);

	return szTime;
}

//=============================================================================
// Method		: Make_MES_Result_Header
// Access		: virtual protected  
// Returns		: CString
// Parameter	: __in const CStringArray * pAddHeaderz
// Qualifier	:
// Last Update	: 2017/5/10 - 13:16
// Desc.		:
//=============================================================================
CString CFile_MES::Make_MES_Result_Header(__in const CStringArray* pAddHeaderz /*= NULL*/)
{
	CString szHeader;

	szHeader = _T("LotName,Barcode,Time,Equipment,Model,SWVersion,Operator,Result,");

	INT_PTR iCnt = pAddHeaderz->GetCount();
	for (INT iIdx = 0; iIdx < iCnt; iIdx++)
	{
		//szHeader += pAddHeaderz->GetAt(iIdx) + _T(",");

		if (iIdx == iCnt - 1)
			szHeader += pAddHeaderz->GetAt(iIdx);
		else
			szHeader += pAddHeaderz->GetAt(iIdx) + _T(",");
	}

	return szHeader;
}

CString CFile_MES::Make_MES_Result(__in const ST_MES_FinalResult* pstWorklist)
{
	CString szLine;
	CString szItem;
	CString szHeader;

	// LOTName
	szItem = pstWorklist->LOTName;
	szLine += szItem + _T(",");

	// Barcode
	szItem = pstWorklist->Barcode;
	szLine += szItem + _T(",");

	// Time
// 	if (FALSE == pstWorklist->Time.IsEmpty())
// 	{
// 		if (_T('\'') == pstWorklist->Time.GetAt(0))
// 		{
// 			szItem = pstWorklist->Time;
// 		}
// 		else
// 		{
// 			szItem = _T("'") + pstWorklist->Time;
// 		}
// 	}
// 	else
// 	{
// 		szItem.Empty();
// 	}
// 	szLine += szItem + _T(",");
	szItem = pstWorklist->Time;
	szLine += szItem + _T(",");

	// Equipment
	szItem = pstWorklist->Equipment;
	szLine += szItem + _T(",");

	// Model
	szItem = pstWorklist->Model;
	szLine += szItem + _T(",");

	// SWVersion
	szItem = pstWorklist->SWVersion;
	szLine += szItem + _T(",");

	// Operator
	szItem = pstWorklist->Operator;
	szLine += szItem + _T(",");
	
	// Result
	szItem = pstWorklist->Result;
	szLine += szItem + _T(",");

	INT_PTR iCnt = pstWorklist->Itemz.GetCount();

	for (INT_PTR nIdx = 0; nIdx < iCnt; nIdx++)
	{
		szItem = pstWorklist->Itemz[nIdx];

		if (nIdx == iCnt - 1 )
			szLine += szItem;
		else
			szLine += szItem + _T(",");
	}

	szLine += _T("\r\n");

	return szLine;
}

//=============================================================================
// Method		: Make_FinalResult_Header
// Access		: virtual protected  
// Returns		: CString
// Parameter	: __in const CStringArray * pAddHeaderz
// Qualifier	:
// Last Update	: 2016/11/23 - 14:55
// Desc.		:
//=============================================================================
CString CFile_MES::Make_FinalResult_Header(__in const CStringArray* pAddHeaderz/* = NULL*/)
{
	CString szHeader;

	szHeader = _T("Time,Equipment,Model,SWVersion,LotName,Barcode,Operator,Result,");

	INT_PTR iCnt = pAddHeaderz->GetCount();
	for (INT iIdx = 0; iIdx < iCnt; iIdx++)
	{
		szHeader += pAddHeaderz->GetAt(iIdx) + _T(",");
	}

	return szHeader;
}

//=============================================================================
// Method		: Make_FinalResult
// Access		: protected  
// Returns		: CString
// Parameter	: __in const ST_MES_FinalResult * pstWorklist
// Qualifier	:
// Last Update	: 2016/12/29 - 17:48
// Desc.		:
//=============================================================================
CString CFile_MES::Make_FinalResult(__in const ST_MES_FinalResult* pstWorklist)
{
	CString szLine;
	CString szItem;
	CString szHeader;

	// Time
// 	if (FALSE == pstWorklist->Time.IsEmpty())
// 	{
// 		if (_T('\'') == pstWorklist->Time.GetAt(0))
// 		{
// 			szItem = pstWorklist->Time;
// 		}
// 		else
// 		{
// 			szItem = _T("'") + pstWorklist->Time;
// 		}
// 	}
// 	else
// 	{
// 		szItem.Empty();
// 	}
// 	szLine += szItem + _T(",");
	szItem = pstWorklist->Time;
	szLine += szItem + _T(",");

	// Equipment
	szItem = pstWorklist->Equipment;
	szLine += szItem + _T(",");

	// Model
	szItem = pstWorklist->Model;
	szLine += szItem + _T(",");

	// SWVersion
	szItem = pstWorklist->SWVersion;
	szLine += szItem + _T(",");

	// LOTName
	szItem = pstWorklist->LOTName;
	szLine += szItem + _T(",");

	// Barcode
	szItem = pstWorklist->Barcode;
	szLine += szItem + _T(",");

	// Operator
	szItem = pstWorklist->Operator;
	szLine += szItem + _T(",");

	// Result
	szItem = pstWorklist->Result;
	szLine += szItem + _T(",");

	INT_PTR iCnt = pstWorklist->Itemz.GetCount();

	for (INT_PTR nIdx = 0; nIdx < iCnt; nIdx++)
	{
		szItem = pstWorklist->Itemz[nIdx];
		szLine += szItem + _T(",");
	}

	szLine += _T("\r\n");

	return szLine;
}

//=============================================================================
// Method		: Make_ProfileDetectLog_Header
// Access		: virtual protected  
// Returns		: CString
// Parameter	: __in const CStringArray * pAddHeaderz
// Qualifier	:
// Last Update	: 2016/11/23 - 14:55
// Desc.		:
//=============================================================================
CString CFile_MES::Make_TestItemLog_Header(__in const CStringArray* pAddHeaderz/* = NULL*/)
{
	CString szHeader;

	szHeader = _T("Time,Equipment,Model,SWVersion,LotName,Barcode,Operator,Result,");

	INT_PTR iCnt = pAddHeaderz->GetCount();
	for (INT iIdx = 0; iIdx < iCnt; iIdx++)
	{
		szHeader += pAddHeaderz->GetAt(iIdx) + _T(",");
	}

	return szHeader;
}

//=============================================================================
// Method		: Make_TestItemLog
// Access		: protected  
// Returns		: CString
// Parameter	: __in const ST_MES_TestItemLog * pstWorklist
// Qualifier	:
// Last Update	: 2016/12/29 - 17:48
// Desc.		:
//=============================================================================
CString CFile_MES::Make_TestItemLog(__in const ST_MES_TestItemLog* pstWorklist)
{
	CString szLine;
	CString szItem;
	CString szHeader;

	// Time
// 	if (FALSE == pstWorklist->Time.IsEmpty())
// 	{
// 		if (_T('\'') == pstWorklist->Time.GetAt(0))
// 		{
// 			szItem = pstWorklist->Time;
// 		}
// 		else
// 		{
// 			szItem = _T("'") + pstWorklist->Time;
// 		}
// 	}
// 	else
// 	{
// 		szItem.Empty();
// 	}
// 	szLine += szItem + _T(",");
	szItem = pstWorklist->Time;
	szLine += szItem + _T(",");

	// Equipment
	szItem = pstWorklist->Equipment;
	szLine += szItem + _T(",");

	// Model
	szItem = pstWorklist->Model;
	szLine += szItem + _T(",");

	// SWVersion
	szItem = pstWorklist->SWVersion;
	szLine += szItem + _T(",");

	// LOTName
	szItem = pstWorklist->LOTName;
	szLine += szItem + _T(",");

	// Barcode
	szItem = pstWorklist->Barcode;
	szLine += szItem + _T(",");

	// Operator
	szItem = pstWorklist->Operator;
	szLine += szItem + _T(",");

	// Result
	szItem = pstWorklist->Result;
	szLine += szItem + _T(",");

	INT_PTR iCnt = pstWorklist->Itemz.GetCount();

	for (INT_PTR nIdx = 0; nIdx < iCnt; nIdx++)
	{
		szItem = pstWorklist->Itemz[nIdx];
		szLine += szItem + _T(",");
	}

	szLine += _T("\r\n");

	return szLine;
}

CString CFile_MES::Make_EMTotalTestItemLog_Header(__in const CStringArray* pAddHeaderz /*= NULL*/)
{
	CString szHeader;

	szHeader = _T("Index,Time,Model,LotID,Operator,Barcode,Camera Type,Result,");

	INT_PTR iCnt = pAddHeaderz->GetCount();
	for (INT iIdx = 0; iIdx < iCnt; iIdx++)
	{
		szHeader += pAddHeaderz->GetAt(iIdx) + _T(",");
	}

	return szHeader;
}

CString CFile_MES::Make_EMTotalTestItemLog(__in const ST_EM_TotalTestItemLog* pstWorklist)
{
	CString szLine;
	CString szItem;
	CString szHeader;

	// Index
	szItem = pstWorklist->szIndex;
	szLine += szItem + _T(",");

	// Time
	szItem = pstWorklist->szTime;
	szLine += szItem + _T(",");

	// Model
	szItem = pstWorklist->szModel;
	szLine += szItem + _T(",");

	// LotID
	szItem = pstWorklist->szLotID;
	szLine += szItem + _T(",");

	// Operator
	szItem = pstWorklist->szOperator;
	szLine += szItem + _T(",");

	// Barcode
	szItem = pstWorklist->szBarcode;
	szLine += szItem + _T(",");

	// Camera Type
	szItem = pstWorklist->szCamType;
	szLine += szItem + _T(",");

	// Result
	szItem = pstWorklist->szResult;
	szLine += szItem + _T(",");

	// Item
	szItem = pstWorklist->szItemz;
	szLine += szItem + _T(",");

	szLine += _T("\r\n");


	return szLine;
}

BOOL CFile_MES::Save_TotalTestItem(__in LPCTSTR szPath, __in SYSTEMTIME* pTime, __in const ST_EM_TotalTestItemLog* pstWorklist)
{
	BOOL bReturn = TRUE;

	CString szFullPath;
	CString szDatePath;

	USES_CONVERSION;
	CStringA szWorklist = CT2A(Make_EMTotalTestItemLog(pstWorklist).GetBuffer(0));
	CStringA szBuff;

	szDatePath.Format(_T("%s\\%04d-%02d-%02d\\%s\\%s\\"), szPath, pTime->wYear, pTime->wMonth, pTime->wDay, pstWorklist->szModel, pstWorklist->szLotID);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%s\\TotalResult.csv"), szDatePath);

	CFile File;
	CFileException e;

	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		// Header 추가
		CString szHeader = Make_EMTotalTestItemLog_Header(&pstWorklist->ItemHeaderz) + _T("\r\n");

		USES_CONVERSION;
		CStringA szMBHeader = CT2A(szHeader.GetBuffer(0));
		szBuff = szMBHeader + szWorklist;
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		szBuff = szWorklist;
	}

	File.SeekToEnd();
	File.Write(szBuff.GetBuffer(), szBuff.GetLength());
	File.Flush();
	szBuff.ReleaseBuffer();

	File.Close();

	return bReturn;
}

//=============================================================================
// Method		: Save_MES_Result
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szMESPath
// Parameter	: __in SYSTEMTIME * pTime
// Parameter	: __in const ST_MES_FinalResult * pstWorklist
// Qualifier	:
// Last Update	: 2017/5/4 - 13:58
// Desc.		:
//=============================================================================
BOOL CFile_MES::Save_MES_Result(__in LPCTSTR szMESPath, __in SYSTEMTIME* pTime, __in const ST_MES_FinalResult* pstWorklist)
{
	BOOL bReturn = TRUE;
	CString szFullPath;
	CString szWorklist = Make_MES_Result(pstWorklist);
	CString szBuff;

	// C:\\MES\제품임시바코드.csv	
	MakeDirectory(szMESPath);

	if (FALSE == pstWorklist->Barcode.IsEmpty())
		szFullPath.Format(_T("%s%s.csv"), szMESPath, pstWorklist->Barcode);
	else
		szFullPath.Format(_T("%sNoBarcode_%s_%04d%02d%02d_%s.csv"), szMESPath, pstWorklist->LOTName, pTime->wYear, pTime->wMonth, pTime->wDay, pstWorklist->Equipment);

	CFile File;
	CFileException e;

	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

//#ifdef _UNICODE
//		WORD mode = 0xFEFF;
//		File.Write(&mode, sizeof(WORD));
//#endif

		// Header 추가
		CString szHeader = Make_MES_Result_Header(&pstWorklist->ItemHeaderz) + _T("\r\n");
		szBuff = szHeader + szWorklist;
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		szBuff = szWorklist;
	}

 	USES_CONVERSION;

	File.SeekToEnd();
	File.Write(CT2A(szBuff.GetBuffer()), szBuff.GetLength());
	//File.Write(szBuff.GetBuffer(), szBuff.GetLength() * sizeof(TCHAR));
	File.Flush();
	szBuff.ReleaseBuffer();

	File.Close();

	return bReturn;
}

BOOL CFile_MES::Save_MES_Result_List(__in LPCTSTR szPath, __in SYSTEMTIME* pTime, __in const ST_MES_FinalResult* pstWorklist)
{
	CString szPath_MESLog;

	szPath_MESLog = szPath;

	if (pstWorklist->Time.IsEmpty())
	{
		SYSTEMTIME lcTime;
		GetLocalTime(&lcTime);

		((ST_MES_FinalResult*)pstWorklist)->Time.Format(_T("%04d-%02d-%02d %02d:%02d:%02d.%03d"), lcTime.wYear, lcTime.wMonth, lcTime.wDay,
			lcTime.wHour, lcTime.wMinute, lcTime.wSecond, lcTime.wMilliseconds);
	}

	if (pstWorklist->Equipment.IsEmpty())
	{
		((ST_MES_FinalResult*)pstWorklist)->Equipment = GetStationName();
	}

	if (pstWorklist->SWVersion.IsEmpty())
	{
		((ST_MES_FinalResult*)pstWorklist)->SWVersion = GetSWVersion(pstWorklist->Model);
	}

	return Save_MES_Result(szPath_MESLog, pTime, pstWorklist);
}

BOOL CFile_MES::Save_TotalResult(__out CString& szReportFullPath, __in LPCTSTR szMESPath, __in SYSTEMTIME* pTime, __in const ST_MES_FinalResult* pstWorklist)
{
	BOOL bReturn = TRUE;
	CString szFullPath;

	USES_CONVERSION;
	CStringA szWorklist = CT2A(Make_FinalResult(pstWorklist).GetBuffer(0));

	CStringA szBuff;

	// ...\Default_A\2016-11-17\ 
	CString szDatePath;
	szDatePath.Format(_T("%s%04d-%02d-%02d\\%s\\%s\\"),
		szMESPath, pTime->wYear, pTime->wMonth, pTime->wDay, pstWorklist->Model, pstWorklist->LOTName);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%s\\TotalResult.csv"), szDatePath);
	szReportFullPath = szDatePath;

	CFile File;
	CFileException e;

	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		// #ifdef _UNICODE
		// 		WORD mode = 0xFEFF;
		// 		File.Write(&mode, sizeof(WORD));
		// #endif

		// Header 추가
		CString szHeader = Make_FinalResult_Header(&pstWorklist->ItemHeaderz) + _T("\r\n");

		//USES_CONVERSION;
		CStringA szMBHeader = CT2A(szHeader.GetBuffer(0));
		szBuff = szMBHeader + szWorklist;
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		szBuff = szWorklist;
	}

	File.SeekToEnd();
	File.Write(szBuff.GetBuffer(), szBuff.GetLength());
	File.Flush();
	szBuff.ReleaseBuffer();

	File.Close();

	return bReturn;
}

BOOL CFile_MES::Save_TotalResult_List(__out CString& szReportFullPath, __in LPCTSTR szPath, __in SYSTEMTIME* pTime, __in const ST_MES_FinalResult* pstWorklist)
{
	CString szPath_MESLog;

	szPath_MESLog = szPath;

	if (pstWorklist->Time.IsEmpty())
	{
		SYSTEMTIME lcTime;
		GetLocalTime(&lcTime);

		((ST_MES_FinalResult*)pstWorklist)->Time.Format(_T("%04d-%02d-%02d %02d-%02d-%02d-%03d"), lcTime.wYear, lcTime.wMonth, lcTime.wDay,
			lcTime.wHour, lcTime.wMinute, lcTime.wSecond, lcTime.wMilliseconds);
	}

	if (pstWorklist->Equipment.IsEmpty())
	{
		((ST_MES_FinalResult*)pstWorklist)->Equipment = GetStationName();
	}

	if (pstWorklist->SWVersion.IsEmpty())
	{
		((ST_MES_FinalResult*)pstWorklist)->SWVersion = GetSWVersion(pstWorklist->Model);
	}

	return Save_TotalResult(szReportFullPath, szPath_MESLog, pTime, pstWorklist);
}

//=============================================================================
// Method		: Save_FinalResult
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szMESPath
// Parameter	: __in SYSTEMTIME * pTime
// Parameter	: __in const ST_MES_FinalResult * pstWorklist
// Qualifier	:
// Last Update	: 2016/12/29 - 17:49
// Desc.		:
//=============================================================================
BOOL CFile_MES::Save_FinalResult(__out CString& szReportFullPath, __in LPCTSTR szMESPath, __in SYSTEMTIME* pTime, __in const ST_MES_FinalResult* pstWorklist)
{
	BOOL bReturn = TRUE;
	CString szFullPath;

	USES_CONVERSION;
	CStringA szWorklist = CT2A(Make_FinalResult(pstWorklist).GetBuffer(0));

	CStringA szBuff;

	// ...\Default_A\2016-11-17\ 
	CString szDatePath;
	szDatePath.Format(_T("%s%04d-%02d-%02d\\%s\\%s\\%s\\%s\\"),
		szMESPath, pTime->wYear, pTime->wMonth, pTime->wDay, pstWorklist->Model, pstWorklist->LOTName, pstWorklist->Result, pstWorklist->Time);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%s\\FinalResult.csv"), szDatePath);
	szReportFullPath = szDatePath;

	CFile File;
	CFileException e;

	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

// #ifdef _UNICODE
// 		WORD mode = 0xFEFF;
// 		File.Write(&mode, sizeof(WORD));
// #endif

		// Header 추가
		CString szHeader = Make_FinalResult_Header(&pstWorklist->ItemHeaderz) + _T("\r\n");
		
		//USES_CONVERSION;
		CStringA szMBHeader = CT2A(szHeader.GetBuffer(0));
		szBuff = szMBHeader + szWorklist;
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		szBuff = szWorklist;
	}

	File.SeekToEnd();
	File.Write(szBuff.GetBuffer(), szBuff.GetLength());
	File.Flush();
	szBuff.ReleaseBuffer();

	File.Close();

	return bReturn;
}

BOOL CFile_MES::Save_FinalResult_List(__out CString& szReportFullPath, __in LPCTSTR szPath, __in SYSTEMTIME* pTime, __in const ST_MES_FinalResult* pstWorklist)
{
	CString szPath_MESLog;

	szPath_MESLog = szPath;

	if (pstWorklist->Time.IsEmpty())
	{
		SYSTEMTIME lcTime;
		GetLocalTime(&lcTime);

		((ST_MES_FinalResult*)pstWorklist)->Time.Format(_T("%04d-%02d-%02d %02d-%02d-%02d-%03d"),	lcTime.wYear, lcTime.wMonth, lcTime.wDay,
																									lcTime.wHour, lcTime.wMinute, lcTime.wSecond, lcTime.wMilliseconds);
	}

	if (pstWorklist->Equipment.IsEmpty())
	{
		((ST_MES_FinalResult*)pstWorklist)->Equipment = GetStationName();
	}

	if (pstWorklist->SWVersion.IsEmpty())
	{
		((ST_MES_FinalResult*)pstWorklist)->SWVersion = GetSWVersion(pstWorklist->Model);
	}

	return Save_FinalResult(szReportFullPath, szPath_MESLog, pTime, pstWorklist);
}

//=============================================================================
// Method		: Save_TestItemLog
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szMESPath
// Parameter	: __in SYSTEMTIME * pTime
// Parameter	: __in const ST_MES_TestItemLog * pstWorklist
// Parameter	: __in LPCTSTR szTestName
// Parameter	: __in BOOL bAddSpaceLine
// Qualifier	:
// Last Update	: 2016/12/29 - 17:49
// Desc.		:
//=============================================================================
BOOL CFile_MES::Save_TestItemLog(__out CString& szReportFullPath, __in CString szTotalResult, __in LPCTSTR szMESPath, __in SYSTEMTIME* pTime, __in const ST_MES_TestItemLog* pstWorklist, __in LPCTSTR szTestName, __in BOOL bAddSpaceLine/* = FALSE*/)
{
	BOOL bReturn = TRUE;
	CString szFullPath;

	USES_CONVERSION;
	CStringA szWorklist = CT2A(Make_TestItemLog(pstWorklist).GetBuffer(0));
	CStringA szBuff;

	

	if (bAddSpaceLine)
	{
		szWorklist = _T("\r\n") + szWorklist;
	}

	// ...\Default_A\2016-11-17\ 
	CString szDatePath;
	szDatePath.Format(_T("%s%04d-%02d-%02d\\%s\\%s\\"),//%s\\%s\\"),
		szMESPath, pTime->wYear, pTime->wMonth, pTime->wDay, pstWorklist->Model, pstWorklist->LOTName);//, szTotalResult, pstWorklist->Time);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%s%s.csv"), szDatePath, szTestName);

	CString szOutPath;
	szOutPath.Format(_T("%s%s"), szDatePath, szTestName);
	szReportFullPath = szOutPath;

	CFile File;
	CFileException e;

	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

// #ifdef _UNICODE
// 			WORD mode = 0xFEFF;
// 			File.Write(&mode, sizeof(WORD));
// #endif

		// Header 추가
		CString szHeader = Make_TestItemLog_Header(&pstWorklist->ItemHeaderz) + _T("\r\n");

		USES_CONVERSION;
		CStringA szMBHeader = CT2A(szHeader.GetBuffer(0));
		szBuff = szMBHeader + szWorklist;
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		szBuff = szWorklist;
	}

	File.SeekToEnd();
	File.Write(szBuff.GetBuffer(), szBuff.GetLength()/* * sizeof(TCHAR)*/);
	File.Flush();
	szBuff.ReleaseBuffer();

	File.Close();

	return bReturn;
}

BOOL CFile_MES::Save_TestItemLog_List(__out CString& szReportFullPath, __in CString szTotalResult, __in LPCTSTR szPath, __in SYSTEMTIME* pTime, __in const ST_MES_TestItemLog* pstWorklist, __in LPCTSTR szTestName, __in BOOL bAddSpaceLine/* = FALSE*/)
{
	CString szPath_MESLog;
	szPath_MESLog = szPath;

	if (pstWorklist->Time.IsEmpty())
	{
		SYSTEMTIME lcTime;
		GetLocalTime(&lcTime);

		((ST_MES_TestItemLog*)pstWorklist)->Time.Format(_T("%04d-%02d-%02d %02d-%02d-%02d-%03d"),	lcTime.wYear, lcTime.wMonth, lcTime.wDay,
																									lcTime.wHour, lcTime.wMinute, lcTime.wSecond, lcTime.wMilliseconds);
	}

	if (pstWorklist->Equipment.IsEmpty())
	{
		((ST_MES_TestItemLog*)pstWorklist)->Equipment = GetStationName();
	}

	if (pstWorklist->SWVersion.IsEmpty())
	{
		((ST_MES_TestItemLog*)pstWorklist)->SWVersion = GetSWVersion(pstWorklist->Model);
	}

	return Save_TestItemLog(szReportFullPath, szTotalResult, szPath_MESLog, pTime, pstWorklist, szTestName, bAddSpaceLine);
}

//=============================================================================
// Method		: Make_EEPROM_String
// Access		: public  
// Returns		: CString
// Parameter	: __in ST_MES_TestItemLog * pstWorklist
// Parameter	: __in LPCTSTR szTestName
// Parameter	: __in DWORD dwStartAddr
// Parameter	: __in DWORD dwEndAddr
// Parameter	: __in char * pDataz
// Parameter	: __in DWORD dwLength
// Qualifier	:
// Last Update	: 2016/12/29 - 17:49
// Desc.		:
//=============================================================================
CString CFile_MES::Make_EEPROM_String(__in ST_MES_TestItemLog* pstWorklist, __in LPCTSTR szTestName, __in DWORD dwStartAddr, __in DWORD dwEndAddr, __in char* pDataz, __in DWORD dwLength)
{
	// 0x00 ~ 0x0F
	// address		0	1	2	3	4	5	6	7	8	9	A	B	C	D	E	F
	// 00000000h	FF	FF	FF	FF	FF	FF	FF	FF	FF	FF	FF	FF	FF	FF	FF	FF
	// 00000010h	FF	24	FA	FF	FF	FF	FF	FF	FF	FF	FF	FF	FF	FF	FF	FF

	CString szBuff;

	DWORD dwLoopCnt = dwLength / 0x10;
	UINT nRemindCnt = dwLength % 0x10;

// 	if (0 < nRemindCnt)
// 		++dwLoopCnt;

	CString szTemp;
	DWORD dwOffset = 0;

	if (pstWorklist->Time.IsEmpty())
	{
		SYSTEMTIME lcTime;
		GetLocalTime(&lcTime);

		((ST_MES_TestItemLog*)pstWorklist)->Time.Format(_T("%04d-%02d-%02d %02d:%02d:%02d.%03d"),  lcTime.wYear, lcTime.wMonth, lcTime.wDay,
																									lcTime.wHour, lcTime.wMinute, lcTime.wSecond, lcTime.wMilliseconds);
	}

	if (pstWorklist->Equipment.IsEmpty())
	{
		((ST_MES_TestItemLog*)pstWorklist)->Equipment = GetStationName();
	}

	if (pstWorklist->SWVersion.IsEmpty())
	{
		((ST_MES_TestItemLog*)pstWorklist)->SWVersion = GetSWVersion(pstWorklist->Model);
	}

	// Result -------------------------------------------------------
	pstWorklist->Itemz.RemoveAll();
	szTemp = _T("Result");
	pstWorklist->ItemHeaderz.Add(szTemp);
	szTemp = _T("1");
	pstWorklist->Itemz.Add(szTemp);
	
	szBuff += _T("\r\n") + Make_TestItemLog(pstWorklist);

	// Address ------------------------------------------------------
	pstWorklist->Itemz.RemoveAll();
	szTemp = _T("Address");
	pstWorklist->Itemz.Add(szTemp);
	for (BYTE nIdx = 0; nIdx < 0x10; nIdx++)
	{
		szTemp.Format(_T("%X"), nIdx);
		pstWorklist->Itemz.Add(szTemp);
	}

	szBuff += Make_TestItemLog(pstWorklist);

	// Data ---------------------------------------------------------
	DWORD dwAddress = dwStartAddr;
	for (UINT nIdx = 0; nIdx < dwLoopCnt; nIdx++)
	{
		pstWorklist->Itemz.RemoveAll();

		szTemp.Format(_T("%08Xh"), dwAddress);
		pstWorklist->Itemz.Add(szTemp);

		for (UINT nIdx = 0; nIdx < 0x10; nIdx++)
		{
			szTemp.Format(_T("%02X"), (BYTE)pDataz[dwOffset++]);
			pstWorklist->Itemz.Add(szTemp);
		}

		szBuff += Make_TestItemLog(pstWorklist);

		dwAddress += 0x10;
	}

	// 나머지 데이터
	if (0 < nRemindCnt)
	{
		pstWorklist->Itemz.RemoveAll();

		szTemp.Format(_T("%08Xh"), dwAddress);
		pstWorklist->Itemz.Add(szTemp);

		for (UINT nIdx = 0; nIdx < nRemindCnt; nIdx++)
		{
			szTemp.Format(_T("%02X"), (BYTE)pDataz[dwOffset++]);
			pstWorklist->Itemz.Add(szTemp);
		}

		szBuff += Make_TestItemLog(pstWorklist);
	}

	return szBuff;
}

//=============================================================================
// Method		: Save_EEPROM_Log
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in SYSTEMTIME * pTime
// Parameter	: __in ST_MES_TestItemLog * pstWorklist
// Parameter	: __in LPCTSTR szTestName
// Parameter	: __in DWORD dwStartAddr
// Parameter	: __in DWORD dwEndAddr
// Parameter	: __in char * pDataz
// Parameter	: __in DWORD dwLength
// Qualifier	:
// Last Update	: 2016/12/29 - 17:49
// Desc.		:
//=============================================================================
BOOL CFile_MES::Save_EEPROM_Log(__in LPCTSTR szPath, __in SYSTEMTIME* pTime, __in ST_MES_TestItemLog* pstWorklist, __in LPCTSTR szTestName, __in DWORD dwStartAddr, __in DWORD dwEndAddr, __in char* pDataz, __in DWORD dwLength)
{
	CString szMESPath;
	szMESPath = szPath;

	// 	BOOL bReturn = TRUE;
	CString szFullPath;
	CString szWorklist;
	CString szBuff = Make_EEPROM_String(pstWorklist, szTestName, dwStartAddr, dwEndAddr, pDataz, dwLength);

	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szMESPath, pstWorklist->Model, pTime->wYear, pTime->wMonth, pTime->wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%s%s_%04d%02d%02d%02d_%s_%s.csv"), szDatePath, pstWorklist->LOTName, pTime->wYear, pTime->wMonth, pTime->wDay, pTime->wHour, pstWorklist->Equipment, szTestName);


	// File Open
	CFile File;
	CFileException e;

	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

// #ifdef _UNICODE
// 		WORD mode = 0xFEFF;
// 		File.Write(&mode, sizeof(WORD));
// #endif

		// Header 추가
		CString szHeader = Make_TestItemLog_Header(&pstWorklist->ItemHeaderz) + _T("\r\n");
		szBuff = szHeader + szBuff;
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}
	}

	File.SeekToEnd();
	File.Write(szBuff.GetBuffer(), szBuff.GetLength() * sizeof(TCHAR));
	File.Flush();
	szBuff.ReleaseBuffer();

	File.Close();

	return TRUE;
}

//=============================================================================
// Method		: GetMESImage_FullPath
// Access		: public  
// Returns		: CString
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in LPCTSTR szModel
// Parameter	: __in LPCTSTR szLotID
// Parameter	: __in LPCTSTR szSensorID
// Parameter	: __in LPCTSTR szTestItem
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in SYSTEMTIME * pTime
// Parameter	: __in enMES_ImgFormat nFormat
// Parameter	: __in LPCTSTR szAddName
// Qualifier	:
// Last Update	: 2016/12/29 - 18:16
// Desc.		:
//=============================================================================
CString CFile_MES::GetMESImage_FullPath(__in LPCTSTR szPath, __in LPCTSTR szModel, __in LPCTSTR szLotID, __in LPCTSTR szSensorID, __in LPCTSTR szTestItem, __in LPCTSTR szBarcode, __in SYSTEMTIME* pTime, __in enMES_ImgFormat nFormat, __in LPCTSTR szAddName /*= NULL*/)
{
	CString szFilename;
	CString szTime;

	szTime.Format(_T("%04d%02d%02d%02d%02d%02d"), pTime->wYear, pTime->wMonth, pTime->wDay, pTime->wHour, pTime->wMinute, pTime->wSecond);

	if (NULL != szAddName)
	{
		CString szTemp = szFilename;
		szFilename.Format(_T("%s_%s_%s_%s_%s_%s.%s"), szLotID, szTestItem, szBarcode, szTime, GetStationName(), g_szMES_ImgFormat[nFormat], szAddName);
	}
	else
	{
		szFilename.Format(_T("%s_%s_%s_%s_%s.%s"), szLotID, szTestItem, szBarcode, szTime, GetStationName(), g_szMES_ImgFormat[nFormat]);
	}

	CString szFullPath;
	CString szSubPath;	// 모델명\날짜\

	szSubPath.Format(_T("%s\\%04d-%02d-%02d\\"), szModel, pTime->wYear, pTime->wMonth, pTime->wDay);

	szFullPath = szPath + szSubPath + szFilename;

	CString szPathTemp = szPath + szSubPath;

	MakeDirectory(szPathTemp);

	return szFullPath;
}

//=============================================================================
// Method		: GetMESLog_FullPath
// Access		: public  
// Returns		: CString
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in LPCTSTR szModel
// Parameter	: __in LPCTSTR szLotID
// Parameter	: __in SYSTEMTIME * pTime
// Parameter	: __in LPCTSTR szInFilename
// Qualifier	:
// Last Update	: 2016/12/29 - 18:16
// Desc.		:
//=============================================================================
CString CFile_MES::GetMESLog_FullPath(__in LPCTSTR szPath, __in LPCTSTR szModel, __in LPCTSTR szLotID, __in SYSTEMTIME* pTime, __in LPCTSTR szInFilename)
{
	CString szFilename;
	CString szTime;

	szTime.Format(_T("%04d%02d%02d%02d"), pTime->wYear, pTime->wMonth, pTime->wDay, pTime->wHour);
	szFilename.Format(_T("%s_%s_%s_%s.csv"), szLotID, szTime, GetStationName(), szInFilename);

	CString szFullPath;
	CString szSubPath;	// 모델명\날짜\

	szSubPath.Format(_T("%s\\%04d-%02d-%02d\\"), szModel, pTime->wYear, pTime->wMonth, pTime->wDay);

	szFullPath = szPath + szSubPath + szFilename;

	return szFullPath;
}

//=============================================================================
// Method		: VerifyLotID
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szLotID
// Qualifier	:
// Last Update	: 2016/12/29 - 18:01
// Desc.		:
//=============================================================================
BOOL CFile_MES::VerifyLotID(__in LPCTSTR szLotID)
{
	// 문자와 알파벳만 허용
	// 12자리
	USES_CONVERSION;
	CStringA szVerify = CT2A(szLotID);

	if (LOT_ID_LENGTH != szVerify.GetLength())
	{
		return FALSE;
	}

	for (int nIdx = 0; nIdx < szVerify.GetLength(); nIdx++)
	{
		if (false == isalnum(szVerify.GetAt(nIdx)))
		{
			return FALSE;
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: GetStationName
// Access		: public  
// Returns		: CString
// Qualifier	:
// Last Update	: 2016/12/29 - 18:01
// Desc.		:
//=============================================================================
CString CFile_MES::GetStationName()
{
	CString szText;
	DWORD dwSize = 255;
	::GetComputerName(szText.GetBuffer(255), &dwSize);
	szText.ReleaseBuffer();

	return szText;
}

//=============================================================================
// Method		: GetSWVersion
// Access		: public  
// Returns		: CString
// Parameter	: __in LPCTSTR szPgmName
// Qualifier	:
// Last Update	: 2016/12/29 - 18:01
// Desc.		:
//=============================================================================
CString CFile_MES::GetSWVersion(__in LPCTSTR szPgmName)
{
	CString szSWVersion;
	CString szDate = GetVersionInfo(_T("FileVersion"));
	CString szProgram = szPgmName;

	szSWVersion.Format(_T("%s_%s"), szProgram, szDate);

	return szSWVersion;
}

//=============================================================================
// Method		: Get_Report_Yield_FullPath
// Access		: public  
// Returns		: CString
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString Model
// Parameter	: __in CString LotName
// Parameter	: __in SYSTEMTIME * pTime
// Qualifier	:
// Last Update	: 2017/5/4 - 13:54
// Desc.		:
//=============================================================================
CString CFile_MES::Get_Report_Yield_FullPath(__in LPCTSTR szPath, __in CString Model, __in CString LotName, __in SYSTEMTIME *pTime)
{
	CString szDatePath;
	if (Model.IsEmpty())
	{
		Model = _T("Default");
	}
	szDatePath.Format(_T("%s\\%04d-%02d-%02d\\%s\\%s\\"), szPath, pTime->wYear, pTime->wMonth, pTime->wDay, Model, LotName);
	MakeDirectory(szDatePath);

	szDatePath += _T("Yield.ini");

	return szDatePath;
}