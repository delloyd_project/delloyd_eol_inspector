﻿//*****************************************************************************
// Filename	: 	File_MES_Mcnex.cpp
// Created	:	2016/11/18 - 18:16
// Modified	:	2016/11/18 - 18:16
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "File_MES_Mcnex.h"
#include "CommonFunction.h"


CFile_MES_Mcnex::CFile_MES_Mcnex()
{
}

CFile_MES_Mcnex::~CFile_MES_Mcnex()
{
}

//=============================================================================
// Method		: Make_MES_Result_Header
// Access		: virtual protected  
// Returns		: CString
// Parameter	: __in const CStringArray * pAddHeaderz
// Qualifier	:
// Last Update	: 2017/5/10 - 13:16
// Desc.		:
//=============================================================================
CString CFile_MES_Mcnex::Make_MES_Result_Header(__in const ST_MES_TotalResult* pstWorklist)
{
	CString szLine;
	CString szHeader;

	for (int i = 0; i < enHeader_EOL_Item_Max; i++)
	{
		szHeader = pstWorklist->ItemHeaderz[i];

		if (i == enHeader_EOL_Item_Max - 1)
			szLine += szHeader;
		else
			szLine += szHeader + _T(",");
	}

	return szLine;
}

CString CFile_MES_Mcnex::Make_MES_Result(__in const ST_MES_TotalResult* pstWorklist)
{
	CString szLine;
	CString szItem;

	for (int i = 0; i < enHeader_EOL_Item_Max; i++)
	{
		szItem = pstWorklist->Itemz[i];

		if (i == enHeader_EOL_Item_Max - 1)
			szLine += szItem;
		else
			szLine += szItem + _T(",");
	}

	return szLine;
}

BOOL CFile_MES_Mcnex::Delete_MES_TextFile(__in CString szMESPath)
{
	if (szMESPath.IsEmpty())
	{
		return FALSE;
	}

	CString szFilePath = szMESPath + _T("\\") + _T("*.txt");
	CFileFind finder;

	BOOL bWorking = finder.FindFile(szFilePath);

	while (bWorking)
	{
		bWorking = finder.FindNextFile();
		if (finder.IsArchived())
		{
			DeleteFile(finder.GetFilePath());
		}
	}

	finder.Close();

	return TRUE;
}

BOOL CFile_MES_Mcnex::Make_MES_Log_File(__in CString szMESPath, __in CString Model, __in int iIndex, __in const ST_MES_TotalResult* pstWorklist)
{
	if (szMESPath.IsEmpty())
		return FALSE;

	USES_CONVERSION;
	
	CStringA szBuf;
	CStringA szHeaderz	= CT2A(Make_MES_Result_Header(pstWorklist).GetBuffer(0));
	CStringA szItemz	= CT2A(Make_MES_Result(pstWorklist).GetBuffer(0));

	// 파일 생성
	CString szCode	= pstWorklist->szEqpCode;
	CString szModel = Model;
	CString szLotID = pstWorklist->szLotNo;
	CString szDate	= pstWorklist->szTestDate;
	szDate.Remove(_T('-'));
	CString szTime	= pstWorklist->szTestTime;
	szTime.Remove(_T(':'));
	CString szIndex;
	szIndex.Format(_T("%d"), iIndex);

	CString szFileName;
	szFileName += szCode + _T("_");
	szFileName += szModel + _T("_");
	szFileName += szLotID + _T("_");
	szFileName += szDate;
	szFileName += szTime + _T("_");
	szFileName += szIndex + _T(".csv");

	CString szFullPath;
	szFullPath += szMESPath + _T("\\") + szFileName;

	CFile File;
	CFileException e;

	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		// Header 추가
		szBuf += szHeaderz + _T("\n");
		// Test Item 추가
		szBuf += szItemz + _T("\n");
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		// Test Item 추가
		szBuf = szItemz + _T("\n");
	}

	File.SeekToEnd();
	File.Write(szBuf.GetBuffer(), szBuf.GetLength());
	File.Flush();
	szBuf.ReleaseBuffer();

	File.Close();

	return TRUE;
}
