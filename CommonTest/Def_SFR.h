﻿#ifndef Def_T_SFR_h__
#define Def_T_SFR_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

#pragma pack(push,1)

typedef enum enROI_SFR
{
	ROI_SFR_FST,
	ROI_SFR_LST = 30,
	ROI_SFR_Max = ROI_SFR_LST,
};

// SFR 영역에 대한 옵션
typedef struct _tag_RegionSFROpt
{
	// 사용 유무
	BOOL bEnable;

	// 측정 스펙
	double dbMinSpc;
	double dbMaxSpc;

	// 측정 옵셋
	double dbOffset;

	// LinePair
	double dbLinePair;

	// Font
	UINT nFont;

	// ROI 영역
	CRect rtRoi;

	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp = rtRoi;

		rtRoi.left	 = iPosX - (int)(rtTemp.Width() / 2);
		rtRoi.right  = rtRoi.left + rtTemp.Width();;
		rtRoi.top	 = iPosY - (int)(rtTemp.Height() / 2);
		rtRoi.bottom = rtRoi.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtRoi;

		rtRoi.left   = rtTemp.CenterPoint().x - (iWidth / 2);
		rtRoi.right  = rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtRoi.top	 = rtTemp.CenterPoint().y - (iHeight / 2);
		rtRoi.bottom = rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};

	void Reset()
	{
		bEnable		= FALSE;
		dbMinSpc	= 0;
		dbMaxSpc	= 0;
		dbOffset	= 0;
		dbLinePair	= 50;
		nFont = 0;
		rtRoi.SetRectEmpty();
	};

	_tag_RegionSFROpt()
	{
		Reset();
	};

	_tag_RegionSFROpt& operator= (_tag_RegionSFROpt& ref)
	{
		bEnable		= ref.bEnable;
		dbMinSpc	= ref.dbMinSpc;
		dbMaxSpc	= ref.dbMaxSpc;
		dbOffset	= ref.dbOffset;
		rtRoi		= ref.rtRoi;
		dbLinePair = ref.dbLinePair;
		nFont = ref.nFont;

		return *this;
	};

}ST_RegionSFROpt, *PST_RegionSFROpt;

// SFR 영역에 대한 옵션
typedef struct _tag_SFR_Opt
{
	ST_RegionSFROpt	stRegionOp[ROI_SFR_Max];

	double dPixelSz;
	int iSelectROI;

	BOOL bField;
	BOOL bEdge;
	BOOL bDistortion;
	UINT nResultType;

	int iMasterAxisX;
	int iMasterAxisY;

	CString szIICFile_1;
	CString szIICFile_2;
	BOOL bIICFile_1;
	BOOL bIICFile_2;

	_tag_SFR_Opt()
	{
		dPixelSz = 0;
		for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
		{
			stRegionOp[nIdx].Reset();
		}

		iSelectROI = -1;

		bField = FALSE;
		bEdge = FALSE;
		bDistortion = FALSE;
		nResultType = 0;

		iMasterAxisX = 960;
		iMasterAxisY = 540;

		bIICFile_1 = FALSE;
		bIICFile_2 = FALSE;
	};
	void Reset()
	{
		dPixelSz = 0;
		for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
		{
			stRegionOp[nIdx].Reset();
		}

		iSelectROI = -1;
		bField = FALSE;
		bEdge = FALSE;
		bDistortion = FALSE;
		nResultType = 0;

		iMasterAxisX = 960;
		iMasterAxisY = 540;

		szIICFile_1.Empty();
		szIICFile_2.Empty();

		bIICFile_1 = FALSE;
		bIICFile_2 = FALSE;

	}

	_tag_SFR_Opt& operator= (_tag_SFR_Opt& ref)
	{
		dPixelSz = ref.dPixelSz;
		for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
		{
			stRegionOp[nIdx] = ref.stRegionOp[nIdx];
		}

		iSelectROI = ref.iSelectROI;
		bField = ref.bField;
		bEdge = ref.bEdge;
		bDistortion = ref.bDistortion;
		nResultType = ref.nResultType;
		iMasterAxisX = ref.iMasterAxisX;
		iMasterAxisY = ref.iMasterAxisY;
		szIICFile_1 = ref.szIICFile_1;
		szIICFile_2 = ref.szIICFile_2;
		bIICFile_1 = ref.bIICFile_1;
		bIICFile_2 = ref.bIICFile_2;

		return *this;
	};

}ST_SFR_Opt, *PST_SFR_Opt;

//각각 영역에 대한 Data정보
typedef struct _tag_SFR_Result
{
	// 최종 결과
	UINT	nResult;
	UINT    nEachResult[ROI_SFR_Max];

	double	dbValue[ROI_SFR_Max];

	int iTotalCnt;
	int iPassCnt;

	int iSelectROI;

	void Reset()
	{
		nResult = 3;
		iTotalCnt = 0;
		iPassCnt = 0;

		for (UINT nIdex = 0; nIdex < ROI_SFR_Max; nIdex++)
		{
			nEachResult[nIdex]	 = TR_Fail;
			dbValue[nIdex]		 = 0.0;
		}
		iSelectROI = -1;
	};
	
	_tag_SFR_Result()
	{
		Reset();
	};

	_tag_SFR_Result& operator= (_tag_SFR_Result& ref)
	{
		nResult = ref.nResult;

		iTotalCnt = ref.iTotalCnt;
		iPassCnt = ref.iPassCnt;

		for (UINT nIdex = 0; nIdex < ROI_SFR_Max; nIdex++)
		{
			nEachResult[nIdex]	 = ref.nEachResult[nIdex];
			dbValue[nIdex] = ref.dbValue[nIdex];
		}
		iSelectROI = ref.iSelectROI;

		return *this;
	};

}ST_SFR_Result, *PST_SFR_Result;

#pragma pack(pop)

#endif // Def_SFR_h__