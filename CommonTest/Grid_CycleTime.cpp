﻿//*****************************************************************************
// Filename	: 	Grid_CycleTime.cpp
// Created	:	2016/11/14 - 18:00
// Modified	:	2016/11/14 - 18:00
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Grid_CycleTime.h"

static LPCTSTR lpszRowHeader[] =
{
	_T("Count"),
	_T("Cycle Time Ave"),
	_T("Cycle Time Min"),
	_T("Cycle Time Max"),
	_T("Tact Time Ave"),
	_T("Tact Time Min"),
	_T("Tact Time Max"),
	NULL
};

typedef enum
{
	IDX_Y_Header = 0,
	IDX_Y_OutputCnt = IDX_Y_Header,
	IDX_Y_CycleTime_Ave,
	IDX_Y_CycleTime_Min,
	IDX_Y_CycleTime_Max,
	IDX_Y_TactTime_Ave,
	IDX_Y_TactTime_Min,
	IDX_Y_TactTime_Max,
	IDX_ROW_MAX,
}enumRowHeaderCT;

static const COLORREF clrRowHeader[] =
{
	RGB(0, 0, 0),
	RGB(50, 50, 200),
	RGB(50, 50, 200),
	RGB(50, 50, 200),
	RGB(63, 101, 169),
	RGB(63, 101, 169),
	RGB(63, 101, 169),
};


static LPCTSTR lpszColHeader[] =
{
	_T(""),
	_T(""),
	NULL
};

typedef enum
{
	IDX_X_Header = 0,
	IDX_X_Item = IDX_X_Header,
	IDX_X_Data,
	IDX_COL_MAX,
}enumColHeaderYield;


#define		RGB_BLACK		RGB(0x00, 0x00, 0x00)
#define		RGB_WHITE		RGB(0xFF, 0xFF, 0xFF)
#define		RGB_YELLOW		RGB(0xFF, 0xFF, 0x00)
#define		RGB_ROW_HEADER	RGB(63, 101,  169)
#define		RGB_BK_ID		RGB(135, 169, 213)
#define		RGB_COL_HEADER	RGB(0xFF, 200, 100)
#define		RGB_TITLE		RGB(150, 200, 0xFF)

#define		RGB_BIT_SET		RGB(123, 255,  75) //RGB(112, 173, 71)
#define		RGB_BIT_CLEAR	RGB(100,  10,  10) //RGB(237, 125, 49)
//#define		RGB_SELECT		RGB(0xFF, 200, 100) //RGB(150, 200, 0xFF)

//=============================================================================
//
//=============================================================================
CGrid_CycleTime::CGrid_CycleTime()
{
	//m_nSocketCTCount = 0;

	SetRowColCount(IDX_ROW_MAX, IDX_COL_MAX);

	//setup the fonts
	m_font_Header.CreateFont(18, 0, 0, 0, 900, 0, 0, 0, 0, 0, 0, ANTIALIASED_QUALITY, 0, _T("Arial"));
	m_font_Data.CreateFont(18, 0, 0, 0, 900, 0, 0, 0, 0, 0, 0, ANTIALIASED_QUALITY, 0, _T("Arial"));
}

//=============================================================================
//
//=============================================================================
CGrid_CycleTime::~CGrid_CycleTime()
{
	m_font_Header.DeleteObject();
	m_font_Data.DeleteObject();
}

//=============================================================================
// Method		: OnSetup
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/14 - 14:30
// Desc.		:
//=============================================================================
void CGrid_CycleTime::OnSetup()
{
	__super::OnSetup();
}

//=============================================================================
// Method		: DrawGridOutline
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_CycleTime::DrawGridOutline()
{
	CGrid_Base::DrawGridOutline();

	SetDefFont(&m_font_Data);

	// 헤더를 설정한다.
	InitHeader();
}

//=============================================================================
// Method		: CGrid_CycleTime::CalGridOutline
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_CycleTime::CalGridOutline()
{
	//CGrid_Base::CalGridOutline();

	// 윈도우 면적에 따라서 열의 너비를 결정
	CRect rect;
	GetWindowRect(&rect);
	int nWidth = rect.Width();
	int nHeight = rect.Height();

	if ((nWidth <= 0) || (nHeight <= 0))
		return;

	// 기본 열 추가 ---------------------------------------
	int iUnitWidth = nWidth / m_nMaxCols;
	int iMisc = nWidth - (iUnitWidth * (m_nMaxCols - 1));
	SetColWidth(IDX_X_Header, iMisc);
	for (UINT iCol = IDX_X_Header + 1; iCol < m_nMaxCols; iCol++)
	{
		SetColWidth(iCol, iUnitWidth);
	}

	// 패턴 행 헤더 추가 ----------------------------------		
	UINT nUnitHeight = nHeight / m_nMaxRows;
	UINT nRemindHeight = nHeight - (nUnitHeight * m_nMaxRows);

	// Height 계산하고 남거나 모자르는 공간 계산하여 헤더 Height 추가 처리
	for (UINT iRow = 0; iRow < nRemindHeight; iRow++)
	{
		SetRowHeight(iRow, nUnitHeight + 1);
	}

	for (UINT iRow = nRemindHeight; iRow < m_nMaxRows; iRow++)
	{
		SetRowHeight(iRow, nUnitHeight);
	}
}

//=============================================================================
// Method		: CGrid_CycleTime::InitRowHeader
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_CycleTime::InitHeader()
{
	for (UINT iRow = 0; iRow < m_nMaxRows; iRow++)
	{
		QuickSetFont(IDX_X_Item, iRow, &m_font_Header);
		QuickSetBackColor_COLORREF(IDX_X_Item, iRow, clrRowHeader[iRow]);
		QuickSetTextColor(IDX_X_Item, iRow, RGB_WHITE);
		QuickSetText(IDX_X_Item, iRow, lpszRowHeader[iRow]);

		QuickSetFont(IDX_X_Data, iRow, &m_font_Data);
		QuickSetBackColor_COLORREF(IDX_X_Data, iRow, RGB_WHITE);
		QuickSetTextColor(IDX_X_Data, iRow, clrRowHeader[iRow]);
		QuickSetText(IDX_X_Data, iRow, _T("0.000 Sec"));
	}

	QuickSetText(IDX_X_Data, IDX_Y_OutputCnt, _T("0"));
}

//=============================================================================
// Method		: OnHint
// Access		: virtual protected  
// Returns		: int
// Parameter	: int col
// Parameter	: long row
// Parameter	: int section
// Parameter	: CString * string
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
int CGrid_CycleTime::OnHint(int col, long row, int section, CString *string)
{
	return FALSE;
}

//=============================================================================
// Method		: CGrid_CycleTime::OnGetCell
// Access		: virtual protected 
// Returns		: void
// Parameter	: int col
// Parameter	: long row
// Parameter	: CUGCell * cell
// Qualifier	:
// Last Update	: 2015/12/10 - 23:28
// Desc.		:
//=============================================================================
void CGrid_CycleTime::OnGetCell(int col, long row, CUGCell *cell)
{
	CGrid_Base::OnGetCell(col, row, cell);

	//   	switch (row)
	//   	{
	//   	case IDX_Y_Header:
	//   		if (IDX_X_Total == col)
	//   			cell->SetBorder(cell->GetBorder() | UG_BDR_RMEDIUM | UG_BDR_BMEDIUM);
	//   		break;
	//   	}
}

//=============================================================================
// Method		: OnDrawFocusRect
// Access		: virtual protected  
// Returns		: void
// Parameter	: CDC * dc
// Parameter	: RECT * rect
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_CycleTime::OnDrawFocusRect(CDC *dc, RECT *rect)
{

}


//=============================================================================
// Method		: SetUseSocketCTCount
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2016/11/14 - 19:53
// Desc.		:
//=============================================================================
// void CGrid_CycleTime::SetUseSocketCTCount(__in UINT nCount)
// {
// 	m_nSocketCTCount = nCount;
// 
// 	SetRowColCount(IDX_Y_SocketA + m_nSocketCTCount, IDX_COL_MAX);
// 
// 	if (GetSafeHwnd())
// 	{
// 		CalGridOutline();
// 	}
// }

//=============================================================================
// Method		: SetCycleTime
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_CycleTime * pstCycleTime
// Qualifier	:
// Last Update	: 2016/11/14 - 19:10
// Desc.		:
//=============================================================================
void CGrid_CycleTime::SetCycleTime(__in const ST_CycleTime* pstCycleTime)
{
	CString szText;

	szText.Format(_T("%d"), pstCycleTime->dwOutputCnt);
	QuickSetText(IDX_X_Data, IDX_Y_OutputCnt, szText);

	szText.Format(_T("%.3f Sec"), pstCycleTime->dAverageCT / 1000.0f);
	QuickSetText(IDX_X_Data, IDX_Y_CycleTime_Ave, szText);

	szText.Format(_T("%.3f Sec"), (DOUBLE)pstCycleTime->dwMinCT / 1000.0f);
	QuickSetText(IDX_X_Data, IDX_Y_CycleTime_Min, szText);

	szText.Format(_T("%.3f Sec"), (DOUBLE)pstCycleTime->dwMaxCT / 1000.0f);
	QuickSetText(IDX_X_Data, IDX_Y_CycleTime_Max, szText);

	szText.Format(_T("%.3f Sec"), pstCycleTime->dAveTactTime / 1000.0f);
	QuickSetText(IDX_X_Data, IDX_Y_TactTime_Ave, szText);

	szText.Format(_T("%.3f Sec"), (DOUBLE)pstCycleTime->dwMinTactTime / 1000.0f);
	QuickSetText(IDX_X_Data, IDX_Y_TactTime_Min, szText);

	szText.Format(_T("%.3f Sec"), (DOUBLE)pstCycleTime->dwMaxTactTime / 1000.0f);
	QuickSetText(IDX_X_Data, IDX_Y_TactTime_Max, szText);
	

// 	for (UINT nIdx = 0; nIdx < m_nSocketCTCount; nIdx++)
// 	{
// 		szText.Format(_T("%.3f Sec"), pstCycleTime->dAverageTT_Sock[nIdx] / 1000.0f);
// 		QuickSetText(IDX_X_Data, IDX_Y_SocketA + nIdx, szText);
// 	}

	RedrawCol(IDX_X_Data);
}

