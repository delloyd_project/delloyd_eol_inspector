#include "TI_PatternNoise.h"


CTI_PatternNoise::CTI_PatternNoise()
{
}


CTI_PatternNoise::~CTI_PatternNoise()
{
}

void CTI_PatternNoise::PatternNoise_Test(IplImage* InputImage, CRect rtROI, int NUM, int nwidth, int nheight, double& Resultavg_origin, double& ResultTotalSNR)
{
	
	int i = 0;
	BYTE R, G, B;
	double total_abs = 0;
	double avg_abs = 0;

	///////////////////////////////////////////////동적 할당//////////////////////////////////////////////
	BYTE **BW;
	BW = (BYTE **)malloc(sizeof(BYTE *)*nheight);
	for (i = 0; i<nheight; i++)BW[i] = (BYTE *)malloc(sizeof(BYTE)* nwidth);


	for (int t = 0; t<nheight; t++){
		for (int k = 0; k<nwidth; k++){
			BW[t][k] = 0;
		}
	}

	int startx = rtROI.left;
	int starty = rtROI.top;
	int endx = rtROI.right;
	int endy = rtROI.bottom;

	double **Cal_X;
	Cal_X = (double **)malloc(sizeof(double *)*nheight);                     
	for (i = 0; i<nheight; i++)Cal_X[i] = (double *)malloc(sizeof(double)* 2);

	double **Cal_Y;
	Cal_Y = (double **)malloc(sizeof(double *)*nwidth);                    
	for (i = 0; i<nwidth; i++)Cal_Y[i] = (double *)malloc(sizeof(double)* 2);

	double **testX;
	testX = (double **)malloc(sizeof(double *)* 2);                   
	for (i = 0; i<2; i++)testX[i] = (double *)malloc(sizeof(double)* nheight);

	double **testY;
	testY = (double **)malloc(sizeof(double *)* 2);                  
	for (i = 0; i<2; i++)testY[i] = (double *)malloc(sizeof(double)* nwidth);

	for (int t = 0; t<nheight; t++){
		Cal_X[t][0] = 0;
		Cal_X[t][1] = 0;
		testX[0][t] = 0;
		testX[1][t] = 0;
		for (int k = 0; k<nwidth; k++){
			BW[t][k] = 0;
			Cal_Y[k][0] = 0;
			Cal_Y[k][1] = 0;
			testY[0][k] = 0;
			testY[1][k] = 0;

		}
	}
	///////////////////////////////////////////////동적 할당//////////////////////////////////////////////

	int count = 0, CountLine[2] = { 0, };
	total_abs = 0;
	double originData = 0;
	CvScalar Tmp;
	for (int y = starty; y<endy; y++)
	{
		Cal_X[y - starty][0] = 0;
		Cal_X[y - starty][1] = 0;
		CountLine[0] = 0;

		for (int x = startx; x<endx; x++)
		{
			if ((rtROI.left <= x) && (rtROI.right >= x)){
				if ((rtROI.top <= y) && (rtROI.bottom >= y)){
					Tmp = cvGet2D(InputImage,y,x);
					
					
					B = Tmp.val[0];
					G = Tmp.val[1];
					R = Tmp.val[2];

					BW[y][x] = (255 - (BYTE)((0.29900*(R)) + (0.58700*(G)) + (0.11400*(B))));


					total_abs += BW[y][x];
					count++;
					CountLine[0]++;
					Cal_X[y - starty][0] += (double)BW[y][x];			
					Cal_X[y - starty][1] += (double)BW[y][x] * (double)BW[y][x];
					
				}
			}
		}
		if (CountLine[0]>0){
			Cal_X[y - starty][0] /= (double)CountLine[0]; 
			Cal_X[y - starty][1] /= (double)CountLine[0];
			Cal_X[y - starty][1] = Cal_X[y - starty][1] - Cal_X[y - starty][0] * Cal_X[y - starty][0];
		}
	}

	double avg_origin = 0;
	avg_origin = total_abs / (double)count;

	CountLine[1] = 0;
	for (int x = startx; x < endx; x++){
		Cal_Y[x - startx][0] = 0;		Cal_Y[x - startx][1] = 0;  CountLine[1] = 0;

		for (int y = starty; y < endy; y++){

			
			Cal_Y[x - startx][0] += (double)BW[y][x];			
			Cal_Y[x - startx][1] += (double)BW[y][x] * (double)BW[y][x];
			CountLine[1]++;
			
		}
		if (CountLine[1]>0){
			Cal_Y[x - startx][0] /= (double)CountLine[1]; Cal_Y[x - startx][1] /= (double)CountLine[1];
			Cal_Y[x - startx][1] = Cal_Y[x - startx][1] - Cal_Y[x - startx][0] * Cal_Y[x - startx][0];
		}
	}

	double Dark = 0, Noise[2];
	Noise[0] = Noise[1] = 0;

	for (long i = 0; i<rtROI.bottom - rtROI.top; i++)	Dark += Cal_X[i][0];
	Dark /= (double)rtROI.bottom - rtROI.top;

	for (long i = 0; i<rtROI.bottom - rtROI.top; i++)	Noise[0] += Cal_X[i][1];
	Noise[0] /= (double)rtROI.bottom - rtROI.top;

	for (long i = 0; i<rtROI.right - rtROI.left; i++)	Noise[1] += Cal_Y[i][1];
	Noise[1] /= (double)rtROI.right - rtROI.left;

	double SNR[4] = { 0, }, Sum[4] = { 0, };
	int Margin;
	int cnt[2] = { 0, };

	for (int i = 0; i<rtROI.right - rtROI.left; i++){

		Sum[0] += Cal_Y[i][0];		
		SNR[0] += Cal_Y[i][0] * Cal_Y[i][0];

		Sum[1] += Cal_Y[i][1];		
		SNR[1] += Cal_Y[i][1] * Cal_Y[i][1];
		cnt[0]++;
	}

	Sum[0] /= (double)cnt[0];		
	SNR[0] /= (double)cnt[0];
	
	Sum[1] /= (double)cnt[0];		
	SNR[1] /= (double)cnt[0];

	SNR[0] = SNR[0] - Sum[0] * Sum[0];
	SNR[1] = SNR[1] - Sum[1] * Sum[1];

	
	for (int i = 0; i<rtROI.bottom - rtROI.top; i++){
		Sum[2] += Cal_Y[i][2];		
		SNR[2] += Cal_Y[i][2] * Cal_Y[i][2];
		
		Sum[3] += Cal_Y[i][3];		
		SNR[3] += Cal_Y[i][3] * Cal_Y[i][3];
		cnt[1]++;
	}

	Sum[2] /= (double)cnt[1];		
	SNR[2] /= (double)cnt[1];
	
	Sum[3] /= (double)cnt[1];		
	SNR[3] /= (double)cnt[1];

	SNR[2] = SNR[2] - Sum[2] * Sum[2];
	SNR[3] = SNR[3] - Sum[3] * Sum[3];

	double TotalSNR = (Sum[0]) / sqrt(SNR[0]);
	TotalSNR = 10 * log10l(TotalSNR)*1.2;

	double Difference_Data = (Noise[0] / Noise[1]);


	for (int i = 0; i<nheight; i++){
		testX[0][i] = Cal_X[i][0]; testX[1][i] = Cal_X[i][1];

	}

	for (int i = 0; i<nwidth; i++){
		testY[0][i] = Cal_Y[i][0]; testY[1][i] = Cal_Y[i][1];
	}

	if (Dark < 0){
		Dark = 0;
	}
	if (Noise[0] <0){
		Noise[0] = 0;
	}
	if (Difference_Data <0){
		Difference_Data = 0;
	}


	ResultTotalSNR = TotalSNR;
	Resultavg_origin = avg_origin;
	

	for (i = 0; i<nheight; i++)free(BW[i]);   free(BW);
	for (i = 0; i<nheight; i++)free(Cal_X[i]);   free(Cal_X);
	for (i = 0; i<nwidth; i++)free(Cal_Y[i]);   free(Cal_Y);
	for (i = 0; i<2; i++)free(testX[i]);   free(testX);
	for (i = 0; i<2; i++)free(testY[i]);   free(testY);

	
}
