﻿//*****************************************************************************
// Filename	: 	Def_AI_Common.h
// Created	:	2018/1/09 - 11:00
// Modified	:	2018/1/09 - 11:00
//
// Author	:	KHO
//	
// Purpose	:	모든 TEST에 공통적으로 사용되는 변수
//*****************************************************************************

#ifndef Def_AI_Common_h__
#define Def_AI_Common_h__

#include <afxwin.h>

typedef enum enMarkColor
{
	AI_Mark_B = 0,
	AI_Mark_W,
	AI_Mark_Max,
};

typedef enum enParticleForm
{
	Pa_Form_Rectangle = 0,
	Pa_Form_Ellipse,
	Pa_Form_Max,
};

typedef enum enParticleArea
{
	Pa_Area_Side = 0,
	Pa_Area_Middle,
	Pa_Area_Center,
	Pa_Area_Max,
};

enum enAlSFRDataType
{
	enAlSFRDataType_MTF,
	enAlSFRDataType_CyPx,
	enAlSFRDataType_Cymm,
	enAlSFRDataType_Max,
};

typedef struct _tag_Paticle
{
	CRect			rtRoi;
	enParticleForm	enAreaForm;

	void Reset()
	{
		rtRoi.SetRectEmpty();
		enAreaForm = Pa_Form_Rectangle;
	};

	_tag_Paticle()
	{
		Reset();
	};

	_tag_Paticle& operator= (_tag_Paticle& ref)
	{
		rtRoi		= ref.rtRoi;
		enAreaForm	= ref.enAreaForm;

		return *this;
	};

}ST_Paticle, *PST_Paticle;

typedef struct _tag_AI_Common
{
	void Reset()
	{
	};

	_tag_AI_Common()
	{
	};

	_tag_AI_Common& operator= (_tag_AI_Common& ref)
	{
		return *this;
	};

}ST_AI_Common, *PST_AI_Common;

typedef struct _tag_CAN_ROI
{
	unsigned short sLeft;
	unsigned short sTop;
	unsigned short sWidth;
	unsigned short sHeight;

	_tag_CAN_ROI()
	{
		sLeft = 0;
		sTop = 0;
		sWidth = 0;
		sHeight = 0;
	};

}ST_ROI;

#endif // Def_AI_Common_h__