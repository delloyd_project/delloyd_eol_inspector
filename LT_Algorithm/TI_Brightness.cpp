﻿#include "stdafx.h"
#include "TI_Brightness.h"

CTI_Brightness::CTI_Brightness()
{
}

CTI_Brightness::~CTI_Brightness()
{
}

//=============================================================================
// Method		: Color_Test
// Access		: public  
// Returns		: void
// Parameter	: __in IplImage * pImageBuf
// Parameter	: __out BYTE ByY
// Qualifier	:
// Last Update	: 2018/1/10 - 16:08
// Desc.		:
//=============================================================================
void CTI_Brightness::Brightness_Test(__in IplImage* pImageBuf, __out BYTE& ByY)
{
	if (pImageBuf == NULL)
		return;

	IplImage* pSrcImage = cvCreateImage(cvGetSize(pImageBuf), IPL_DEPTH_8U, 1);

	cvCvtColor(pImageBuf, pSrcImage, CV_BGR2GRAY);

	BYTE byY = 0;
	double dY = 0;
	DWORD dwCount = 0;

	for (int y = 0; y < pSrcImage->height; y++)
	{
		for (int x = 0; x < pSrcImage->width; x++)
		{
			byY = pSrcImage->imageData[y * pSrcImage->widthStep + x * pSrcImage->nChannels + 0];
			dY += (double)byY;

			dwCount++;
		}
	}

	ByY = (BYTE)(dY / dwCount);
	
	cvReleaseImage(&pSrcImage);
}
