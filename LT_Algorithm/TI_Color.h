﻿#pragma once

#include <afxwin.h>

#include "cv.h"
#include "highgui.h"
#include "Def_AI_Common.h"

class CTI_Color
{
public:
	CTI_Color();
	virtual ~CTI_Color();

	void Color_Test(__in IplImage* pImageBuf, __out int& iRed, __out int& iGreen, __out int& iBlue);

protected:
};

