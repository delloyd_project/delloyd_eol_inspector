﻿#include "stdafx.h"
#include "TI_Color.h"

CTI_Color::CTI_Color()
{
}

CTI_Color::~CTI_Color()
{
}

//=============================================================================
// Method		: Color_Test
// Access		: public  
// Returns		: void
// Parameter	: __in CRect rtROI
// Parameter	: __in IplImage * pImageBuf
// Parameter	: __out BYTE byRed
// Parameter	: __out BYTE byGreen
// Parameter	: __out BYTE byBlue
// Qualifier	:
// Last Update	: 2018/1/10 - 16:08
// Desc.		:
//=============================================================================
void CTI_Color::Color_Test(__in IplImage* pImageBuf, __out int& iRed, __out int& iGreen, __out int& iBlue)
{
	if (pImageBuf == NULL)
		return;

	BYTE byR = 0, byG = 0, byB = 0;
	double dR = 0, dG = 0, dB = 0;
	double dCount = 0;

	for (int y = 0; y < pImageBuf->height; y++)
	{
		for (int x = 0; x < pImageBuf->width; x++)
		{
			byB = pImageBuf->imageData[y * pImageBuf->widthStep + x * pImageBuf->nChannels + 0];
			byG = pImageBuf->imageData[y * pImageBuf->widthStep + x * pImageBuf->nChannels + 1];
			byR = pImageBuf->imageData[y * pImageBuf->widthStep + x * pImageBuf->nChannels + 2];

			dB += (double)byB;
			dG += (double)byG;
			dR += (double)byR;

			dCount++;
		}
	}

	iRed = (int)(dR / dCount);
	iGreen = (int)(dG / dCount);
	iBlue = (int)(dB / dCount);
}
