#include "TI_Reverse.h"


CTI_Reverse::CTI_Reverse()
{
}


CTI_Reverse::~CTI_Reverse()
{
}


BOOL CTI_Reverse::Reverse_Test(IplImage* InputImage, int iWidth, int iHeight, CRect rtROI, int &iPosX, int &iPosY, int iColor)
{
	int bResult = FALSE;

	IplImage *RGBOrgImage = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 3);
	IplImage *GrayImage = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);
	IplImage *GrayImage_SharpnessCal = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);

	BYTE R, G, B;



	int iStartX = rtROI.left;
	int iStartY = rtROI.top;

	int iEndX = rtROI.right;
	int iEndY = rtROI.bottom;
	

	cvCopy(InputImage, RGBOrgImage);

	// Gray Scale
	cvCvtColor(RGBOrgImage, GrayImage, CV_BGR2GRAY);
	cvCopy(GrayImage, GrayImage_SharpnessCal);
	cvSmooth(GrayImage, GrayImage);
	cvSmooth(GrayImage, GrayImage);
	cvSmooth(GrayImage, GrayImage);
	cvThreshold(GrayImage, GrayImage, 0, 255, CV_THRESH_OTSU);

	if (iColor == 0)
	{
		cvNot(GrayImage, GrayImage);
	}

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(GrayImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;
	int iCounter = 0;

	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
	{
		iCounter++;
	}

	CvRect rect;
	double area = 0, arcCount = 0;
	CvPoint nCenterPoint;

	double dbMin_Dist = 99999;
	double dbDist = 0;
	double circularity = 0;
	int rtCenterX = 0;
	int rtCenterY = 0;
	CvPoint StartPoint;
	CvPoint EndPoint;

	// 오브젝트가 있는경우
	if (iCounter != 0)
	{
		for (; contour != 0; contour = contour->h_next)
		{
			rect = cvContourBoundingRect(contour, 1);
			area = cvContourArea(contour, CV_WHOLE_SEQ);
			arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);
			circularity = (4.0 * 3.14 * area) / (arcCount*arcCount);

			rtCenterX = rect.x + rect.width / 2;
			rtCenterY = rect.y + rect.height / 2;

			StartPoint.x = rect.x;
			StartPoint.y = rect.y;

			EndPoint.x = rect.x + rect.width;
			EndPoint.y = rect.y + rect.height;

			//cvRectangle(RGBOrgImage, StartPoint, EndPoint, CV_RGB(255, 255, 0), 1);

			if (rect.x > iStartX && rect.x + rect.width < iEndX
				&& rect.y > iStartY && rect.y + rect.height < iEndY)
			{
				dbDist = GetDistance(iStartX + rtROI.Width() / 2, iStartY + rtROI.Height() / 2, rtCenterX, rtCenterY);

				//원에 가깝고
				if (circularity > 0.7)
				{
					// 이미지 중심에서 제일 가까운 오브젝트 탐색
					if (dbMin_Dist > dbDist)
					{
						dbMin_Dist = dbDist;

						nCenterPoint.x = rtCenterX;
						nCenterPoint.y = rtCenterY;

						//됐다!
						iPosX = nCenterPoint.x;
						iPosY = nCenterPoint.y;
						bResult = TRUE;

						StartPoint.x = nCenterPoint.x - rect.width / 2;
						StartPoint.y = nCenterPoint.y - rect.height / 2;

						EndPoint.x = nCenterPoint.x + rect.width / 2;
						EndPoint.y = nCenterPoint.y + rect.height / 2;
						cvRectangle(RGBOrgImage, StartPoint, EndPoint, CV_RGB(255, 0, 0), 4);
						//cvSaveImage("D:\\RGBOrgImage.bmp", RGBOrgImage);
					}
				}
			}
		}
	}
	else
	{
		iPosX = 0;
		iPosY = 0;
		bResult = FALSE;
	}
	

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&RGBOrgImage);
	cvReleaseImage(&GrayImage_SharpnessCal);
	cvReleaseImage(&GrayImage);

	return bResult;
}

double CTI_Reverse::GetDistance(__in int iSrcX, __in int iSrcY, __in int iDstX, __in int iDstY)
{
	return sqrt((double)((iDstX - iSrcX) * (iDstX - iSrcX) + (iDstY - iSrcY) * (iDstY - iSrcY)));
}
