﻿#ifndef Test_Region_PicControl_h__
#define Test_Region_PicControl_h__

#pragma once

#define WHITE_COLOR		RGB(255,255,255)
#define BLACK_COLOR		RGB(0,0,0)
#define BLUE_COLOR		RGB(0,0,255)
#define BLUE_DK_COLOR	RGB(0,0,128)
#define RED_COLOR		RGB(255,0,0)
#define RED_DK_COLOR	RGB(128,0,0)
#define GREEN_COLOR		RGB(0,255,0)
#define GREEN_DK_COLOR	RGB(0,128,0)
#define GRAY_COLOR		RGB(192,192,192)
#define GRAY_DK_COLOR	RGB(128,128,128)
#define GRAY_LT_COLOR	RGB(230,230,230)
#define YELLOW_COLOR	RGB(255,255,0)
#define MINOR_COLOR		RGB(255,128,64)
#define PINK_COLOR		RGB(255,0,255)
#define MCYAN			RGB(0,255,255)

#include "Define_ParticleTest.h"
#include "Define_ResolutionTest.h"
#include "Define_CenterPointTest.h"
#include "Define_RotateTest.h"

// CTest_Region_PicControl

class CTest_Region_PicControl : public CWnd
{
	DECLARE_DYNAMIC(CTest_Region_PicControl)

public:
	CTest_Region_PicControl();
	virtual ~CTest_Region_PicControl();

	void	SetPtr_Particle		(ST_Particle *pstParticle);
	void	SetPtr_Resolution	(ST_Resolution *pstResolution);

	/*이물*/
	void	ParticlePic			(CDC *cdc);
	void	ParticlePic			(CDC *cdc, int NUM);
	void	ParticleErrPic		(CDC *cdc, int NUM);

	/*해상력*/
	void	ResolutionPic		(CDC *cdc);
	void	ResolutionPic		(CDC *cdc, int NUM);
	int		GetStandardData		(int NUM);
	int		GetStandardPeakData	(int NUM);
	double	GetDistance			(int x1, int y1, int x2, int y2);
	void	DrawSFRGraph		(CDC *cdc, int NUM);

	/*보임량 광축*/
	void	SetPtr_CenterPoint	(ST_CenterPoint *pstCenterPoint);
	void	CenterPointPic		(CDC *cdc);

	/*로테이트*/
	void	SetPtr_Rotate		(ST_Rotate *pstRotate);
	void	RotatePic			(CDC *cdc, int NUM);
	void	RotatePic			(CDC *cdc);

protected:
	ST_Particle*		m_pstParticle;
	ST_Resolution*		m_pstResolution;
	ST_CenterPoint*		m_pstCenterPoint;
	ST_Rotate*			m_pstRotate;

protected:
	DECLARE_MESSAGE_MAP()

};


#endif // Test_Region_PicControl_h__
