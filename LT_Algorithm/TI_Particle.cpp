﻿// *****************************************************************************
//  Filename	: 	TI_Particle.cpp
//  Created	:	2018/2/11 - 16:17
//  Modified	:	2018/2/11 - 16:17
// 
//  Author	:	Luritech
// 	
//  Purpose	:	
// ****************************************************************************
#include "stdafx.h"
#include "TI_Particle.h"


CTI_Particle::CTI_Particle()
{
	
}

CTI_Particle::~CTI_Particle()
{

}

//=============================================================================
// Method		: GetDistance
// Access		: protected  
// Returns		: double
// Parameter	: int x1
// Parameter	: int y1
// Parameter	: int x2
// Parameter	: int y2
// Qualifier	:
// Last Update	: 2018/2/11 - 16:18
// Desc.		:
//=============================================================================
double CTI_Particle::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt((double)((x2 - x1)*(x2 - x1)) + ((y2 - y1)*(y2 - y1)));

	return result;
}

//=============================================================================
// Method		: EllipseDistanceSum
// Access		: protected  
// Returns		: double
// Parameter	: int XC
// Parameter	: int YC
// Parameter	: double A
// Parameter	: double B
// Parameter	: int X1
// Parameter	: int Y1
// Qualifier	:
// Last Update	: 2018/2/11 - 16:18
// Desc.		:
//=============================================================================
double CTI_Particle::EllipseDistanceSum(int XC, int YC, double A, double B, int X1, int Y1)
{
	double DistX, DistY, x, y;

	x = (double)(X1 - XC);
	y = (double)(Y1 - YC);
	DistX = (x*x) / (double)((A*A));
	DistY = (y*y) / (double)((B*B));

	double Dist = DistX + DistY;

	return Dist;
}

//=============================================================================
// Method		: SetTestArea
// Access		: public  
// Returns		: void
// Parameter	: IN UINT dwWidth
// Parameter	: IN UINT dwHeight
// Parameter	: IN ST_ROI stROI_Side
// Parameter	: IN ST_ROI stROI_Middle
// Parameter	: IN ST_ROI stROI_Center
// Parameter	: IN bool bEllipse_Side
// Parameter	: IN bool bEllipse_Middle
// Parameter	: IN bool bEllipse_Center
// Parameter	: OUT char * * ppchOUT_Area
// Qualifier	:
// Last Update	: 2018/2/11 - 16:36
// Desc.		:
//=============================================================================
// void CTI_Particle::SetTestArea_old(IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI_Side, IN ST_ROI stROI_Middle, IN ST_ROI stROI_Center, IN bool bEllipse_Side, IN bool bEllipse_Middle, IN bool bEllipse_Center, OUT char** ppchOUT_Area)
// {
// 	IplImage *pFieldImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);
// 
// 	//최초 배경 
// 	for (UINT y = 0; y < dwHeight; y++)
// 	{
// 		for (UINT x = 0; x < dwWidth; x++)
// 		{
// 			ppchOUT_Area[y][x] = 0;
// 			pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)255;
// 			pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)255;
// 			pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)255;	
// 		}
// 	}
// 
// 	UINT Startx = stROI_Side.sLeft;
// 	UINT Endx = stROI_Side.sLeft + stROI_Side.sWidth;
// 	UINT Starty = stROI_Side.sTop;
// 	UINT Endy = stROI_Side.sTop + stROI_Side.sHeight;
// 
// 	// Side
// 	for (UINT y = Starty; y < Endy; y++)
// 	{
// 		for (UINT x = Startx; x < Endx; x++)
// 		{
// 			if (bEllipse_Side == TRUE)
// 			{
// 				if (EllipseDistanceSum(stROI_Side.sLeft + stROI_Side.sWidth / 2, stROI_Side.sTop + stROI_Side.sHeight / 2, ((double)stROI_Side.sWidth / 2.0), ((double)stROI_Side.sHeight / 2.0), x, y) <= 1 )
// 				{
// 					ppchOUT_Area[y][x] = 3;
// 					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)0;
// 					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)0;
// 					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)255;
// 				}
// 			}
// 			else
// 			{
// 				ppchOUT_Area[y][x] = 3;
// 				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)0;
// 				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)0;
// 				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)255;
// 			}
// 		}
// 	}
// 
// 	Startx = stROI_Middle.sLeft;
// 	Endx = stROI_Middle.sLeft + stROI_Middle.sWidth;
// 	Starty = stROI_Middle.sTop;
// 	Endy = stROI_Middle.sTop + stROI_Middle.sHeight;
// 
// 	// Middle
// 	for (UINT y = Starty; y < Endy; y++)
// 	{
// 		for (UINT x = Startx; x < Endx; x++)
// 		{
// 			if (bEllipse_Middle == TRUE)
// 			{
// 				if (EllipseDistanceSum(stROI_Middle.sLeft + stROI_Middle.sWidth / 2, stROI_Middle.sTop + stROI_Middle.sHeight / 2, ((double)stROI_Middle.sWidth / 2.0), ((double)stROI_Middle.sHeight / 2.0), x, y) <= 1)
// 				{
// 					ppchOUT_Area[y][x] = 2;
// 					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)0;
// 					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)255;
// 					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)0;
// 				}
// 			}
// 			else
// 			{
// 				ppchOUT_Area[y][x] = 2;
// 				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)0;
// 				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)255;
// 				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)0;
// 			}
// 		}
// 	}
// 
// 	Startx = stROI_Center.sLeft;
// 	Endx = stROI_Center.sLeft + stROI_Center.sWidth;
// 	Starty = stROI_Center.sTop;
// 	Endy = stROI_Center.sTop + stROI_Center.sHeight;
// 
// 	// Center
// 	for (UINT y = Starty; y < Endy; y++)
// 	{
// 		for (UINT x = Startx; x < Endx; x++)
// 		{
// 			if (bEllipse_Center == TRUE)
// 			{
// 				if (EllipseDistanceSum(stROI_Center.sLeft + stROI_Center.sWidth / 2, stROI_Center.sTop + stROI_Center.sHeight / 2, ((double)stROI_Center.sWidth / 2.0), ((double)stROI_Center.sHeight / 2.0), x, y) <= 1)
// 				{
// 					ppchOUT_Area[y][x] = 1;
// 					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)255;
// 					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)0;
// 					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)0;
// 				}
// 			}
// 			else
// 			{
// 				ppchOUT_Area[y][x] = 1;
// 				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)255;
// 				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)0;
// 				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)0;
// 			}
// 		}
// 	}
// 
// 
// 
// 
// 
// 
// 	cvReleaseImage(&pFieldImage);
// }

//=============================================================================
// Method		: SetTestArea
// Access		: public  
// Returns		: void
// Parameter	: IN UINT dwWidth
// Parameter	: IN UINT dwHeight
// Parameter	: IN ST_ROI stROI_Side
// Parameter	: IN ST_ROI stROI_Middle
// Parameter	: IN ST_ROI stROI_Center
// Parameter	: IN bool bEllipse_Side
// Parameter	: IN bool bEllipse_Middle
// Parameter	: IN bool bEllipse_Center
// Parameter	: OUT char * * ppchOUT_Area
// Qualifier	:
// Last Update	: 2018/2/11 - 16:36
// Desc.		:
//=============================================================================
void CTI_Particle::SetTestArea(IN UINT dwWidth, IN UINT dwHeight, IN UINT iROIWidth, IN UINT iROIheight, OUT char** ppchOUT_Area)
{
	//최초 초기화
	for (UINT y = 0; y < dwHeight; y++)
	{
		for (UINT x = 0; x < dwWidth; x++)
			ppchOUT_Area[y][x] = ROI_Pr_Max;
	}
	
	// 구역 분할 Point
	enum { LT, RT, RB, LB };

	// 4Point 지정
	CvPoint Point_Sector[4];
	Point_Sector[LT].x = iROIWidth;
	Point_Sector[LT].y = iROIheight;

	Point_Sector[RT].x = dwWidth - iROIWidth;
	Point_Sector[RT].y = iROIheight;

	Point_Sector[RB].x = dwWidth - iROIWidth;
	Point_Sector[RB].y = dwHeight - iROIheight;

	Point_Sector[LB].x = iROIWidth;
	Point_Sector[LB].y = dwHeight - iROIheight;

	for (UINT j = 0; j < dwHeight; j++)
	{
		for (UINT i = 0; i < dwWidth; i++)
		{
			// 내부 일때
			if (i >= Point_Sector[LT].x && i <= Point_Sector[RT].x && j >= Point_Sector[LT].y && j <= Point_Sector[RB].y)
				ppchOUT_Area[j][i] = ROI_Pr_Center;
			else // 외부 일때
			{
				// 위 아래쪽 외부 일때
				if (j < Point_Sector[LT].y || j > Point_Sector[RB].y)
				{
					// 양 옆 일때
					if (i < Point_Sector[LT].x || i > Point_Sector[RB].x)
						ppchOUT_Area[j][i] = ROI_Pr_Edge;
					else
						ppchOUT_Area[j][i] = ROI_Pr_VerEdge;
				}
				else // 양 옆 일때
				{
					if (i < Point_Sector[LT].x || i > Point_Sector[RB].x)
						ppchOUT_Area[j][i] = ROI_Pr_HorEdge;
				}
			}
		}
	}
}

//=============================================================================
// Method		: Lump_Detection
// Access		: public  
// Returns		: void
// Parameter	: IN LPWORD pImageBuf
// Parameter	: IN UINT dwWidth
// Parameter	: IN UINT dwHeight
// Parameter	: IN float fSensitivity
// Parameter	: IN float * fThreSize
// Parameter	: OUT int & iOUT_Counter
// Parameter	: OUT CvRect * prtOUT_Contour
// Parameter	: OUT char * * ppchOUT_Area
// Qualifier	:
// Last Update	: 2018/2/12 - 11:56
// Desc.		:
//=============================================================================
void CTI_Particle::Lump_Detection(IN LPBYTE pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN float fSensitivity, IN float* fThreSize, OUT int& iOUT_Counter, OUT char** ppchOUT_Area, OUT UINT* Particle_Region)
{
	// 	float fThreSize[ROI_Pr_Max] = {0,};
	// 	fThreSize[ROI_Pr_Side]		= fThreSize_Side;
	// 	fThreSize[ROI_Pr_Middle]	= fThreSize_Middle;
	// 	fThreSize[ROI_Pr_Center]	= fThreSize_Center;
	IplImage *srcImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

	// 8bit 컬러 이미지 복사
	for (UINT y = 0; y < dwHeight; y++)
	{
		for (UINT x = 0; x < dwWidth; x++)
		{
			srcImage->imageData[y*srcImage->widthStep + (x * 3) + 0] = pImageBuf[y*(dwWidth * 3) + (x * 3) + 0];
			srcImage->imageData[y*srcImage->widthStep + (x * 3) + 1] = pImageBuf[y*(dwWidth * 3) + (x * 3) + 1];
			srcImage->imageData[y*srcImage->widthStep + (x * 3) + 2] = pImageBuf[y*(dwWidth * 3) + (x * 3) + 2];
		}
	}

	IplImage *OriginImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

	cvCvtColor(srcImage, OriginImage, CV_RGB2GRAY);


	// 	WORD wData16Bit = 0;
	// 	BYTE byData8Bit = 0;
	// 
	// 	for (UINT y = 0; y < dwHeight; y++)
	// 	{
	// 		for (UINT x = 0; x < dwWidth; x++)
	// 		{
	// 			wData16Bit = *(pImageBuf + y * dwWidth + x);
	// 
	// 			wData16Bit = wData16Bit & 0xF000 ? 0x0FFF : wData16Bit;
	// 			byData8Bit = (wData16Bit >> 4) & 0x00FF;
	// 
	// 			OriginImage->imageData[y * OriginImage->widthStep + x] = byData8Bit;
	// 		}
	// 	}

	//cvNormalize(OriginImage, OriginImage, 0, 255,CV_MINMAX);

	cvErode(OriginImage, OriginImage);

	cvAdaptiveThreshold(OriginImage, DilateImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 71, fSensitivity);
	cvNot(DilateImage, DilateImage);

	cvRectangle(DilateImage, cvPoint(2, 2), cvPoint(DilateImage->width - 3, DilateImage->height - 3), CV_RGB(255, 0, 0), 6, 8);


	cvDilate(DilateImage, DilateImage);
	cvErode(DilateImage, DilateImage, 0, 2);

	cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;

	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
	{
		iOUT_Counter++;
	}

	//prtOUT_Contour = new CvRect[iOUT_Counter];

	double area = 0, arcCount = 0;
	iOUT_Counter = 0;
	double old_dist = 999999;

	CvRect	rect;

	for (; contour != 0; contour = contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		rect = cvContourBoundingRect(contour, 1);

		int center_x, center_y;
		center_x = rect.x + rect.width / 2;
		center_y = rect.y + rect.height / 2;
		cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(255, 0, 0), 1, 8);

		BOOL Rate_Part = FALSE;
		double  Rate = 0;

		if (rect.width > rect.height)
			Rate = (double)rect.width / rect.height;
		else
			Rate = (double)rect.height / rect.width;

		if (Rate >= 9)
			Rate_Part = TRUE;

		//Size -> 
		if ((UINT)rect.width < (dwWidth / 2) && (UINT)rect.height < (dwHeight / 2))
		{
			int Center_X = rect.x + rect.width / 2;
			int Center_Y = rect.y + rect.height / 2;

			for (int i = 0; i < ROI_Pr_Max; i++)
			{
				// [Input] 멍 크기
				if (rect.width > fThreSize[i] && rect.height > fThreSize[i] && ppchOUT_Area[Center_Y][Center_X] == i && Rate_Part == FALSE && iOUT_Counter < MAX_Particle_ResultROI_Count)
				{
					m_rtParticle[iOUT_Counter].left = rect.x;
					m_rtParticle[iOUT_Counter].top = rect.y;
					m_rtParticle[iOUT_Counter].right = rect.x + rect.width;
					m_rtParticle[iOUT_Counter].bottom = rect.y + rect.height;

					Particle_Region[iOUT_Counter] = i;

					iOUT_Counter++;

					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);
				}
			}
		}
	}



	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&srcImage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&RGBResultImage);
}

//=============================================================================
// Method		: ParticleCluster
// Access		: public  
// Returns		: void
// Parameter	: IN int nParticleCount
// Parameter	: IN ST_ROI * pROI
// Parameter	: IN float * pConcentration
// Parameter	: OUT bool * pbOUT_Judge
// Parameter	: OUT LPBYTE lpbyOUT_ParticleType
// Qualifier	:
// Last Update	: 2018/2/12 - 12:29
// Desc.		:
//=============================================================================
void CTI_Particle::ParticleCluster(IN int nParticleCount, IN ST_ROI* pROI, IN float* pConcentration, OUT bool* pbOUT_Judge, OUT LPBYTE lpbyOUT_ParticleType)
{
	int		nArea = 0;
	double dbStainThresold = 20.0;
	double dbDeadPixelThresold = 70.0;

	// 타입 별 갯수 - 전체 분류용
	int nNumOfEveryCluster[Pr_Type_Max] = { 0, };
	// 타입 종류
	int* nType = new int[nParticleCount];

	for (int i = 0; i < nParticleCount; i++)
	{
		// 초기화
		nType[i] = 0;

		// 예외처리
		if (pConcentration[i] > 100)
			pConcentration[i] = 100;
		else if (pConcentration[i] < 0)
			pConcentration[i] = 0;

		nArea = pROI[i].sWidth * pROI[i].sHeight;

		// Stain
		if (pConcentration[i] > dbStainThresold)
		{
			nType[i] = Pr_Type_Stain;
			nNumOfEveryCluster[Pr_Type_Stain]++;
		}
	}

	// 타입 별 갯수 - 갯수 제한용
	int nTypeCount[Pr_Type_Max] = { 0, };

	// 타입 별 최대 갯수 제한
	int nCount = 0;
	for (int i = 0; i < nParticleCount; i++)
	{
		if (nType[i] == Pr_Type_Stain && nTypeCount[Pr_Type_Stain]++ >= (int)m_nMax_ParticleCount)
			continue;

		pbOUT_Judge[i] = false;
		lpbyOUT_ParticleType[nCount++] = nType[i];
	}

	delete[] nType;
}

//=============================================================================
// Method		: Particle_Test
// Access		: public  
// Returns		: void
// Parameter	: IN LPWORD pImageBuf
// Parameter	: IN UINT dwWidth
// Parameter	: IN UINT dwHeight
// Parameter	: IN ST_CAN_Particle_Opt ParticleOpt
// Parameter	: IN ST_CAN_Particle_Result & ParticleResult
// Qualifier	:
// Last Update	: 2018/2/12 - 10:20
// Desc.		:
//=============================================================================
void CTI_Particle::Particle_Test(IN LPBYTE pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_CAN_Particle_Opt ParticleOpt, OUT ST_CAN_Particle_Result &ParticleResult)
{
	char** ppchArea = nullptr;

	if (nullptr == ppchArea)
	{
		ppchArea = new char*[dwHeight];
		for (DWORD dwIdx = 0; dwIdx < dwHeight; dwIdx++)
		{
			ppchArea[dwIdx] = new char[dwWidth];
		}
	}

	float** pfData			= NULL;
	CRect* pstROI			= NULL;
	float*	pfConcentration	= NULL;
	int		nGroup			= 0;
	long	nCount			= 0;
	int		nResultCount	= 0;
	int		iCount			= 0;

	UINT Particle_Resion[MAX_Particle_ResultROI_Count] = { 0, };

	// 초기화
	for (int i = 0; i < MAX_Particle_ResultROI_Count; i++)
	{
		m_rtParticle[i].left = 0;
		m_rtParticle[i].top = 0;
		m_rtParticle[i].right = 0;
		m_rtParticle[i].bottom = 0;

		nCount++;
	}

	// 검사 영역 구분
	SetTestArea(dwWidth, dwHeight, ParticleOpt.iEgdeW, ParticleOpt.iEgdeH, ppchArea);

	// 이물 검출
	//Lump_Detection(pImageBuf, dwWidth, dwHeight, ParticleOpt.fSensitivity, ParticleOpt.fThreSize, iCount, prtContour, ppchArea);
	Lump_Detection(pImageBuf, dwWidth, dwHeight, ParticleOpt.fSensitivity, ParticleOpt.fThreSize, iCount, ppchArea, Particle_Resion); // prtContour => m_rtParticle 전역 변수로 바꿈

	IplImage *srcImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);
	// 8bit 컬러 이미지 복사
	for (UINT y = 0; y < dwHeight; y++)
	{
		for (UINT x = 0; x < dwWidth; x++)
		{
			srcImage->imageData[y*srcImage->widthStep + (x * 3) + 0] = pImageBuf[y*(dwWidth * 3) + (x * 3) + 0];
			srcImage->imageData[y*srcImage->widthStep + (x * 3) + 1] = pImageBuf[y*(dwWidth * 3) + (x * 3) + 1];
			srcImage->imageData[y*srcImage->widthStep + (x * 3) + 2] = pImageBuf[y*(dwWidth * 3) + (x * 3) + 2];
		}
	}


	// 선언
	pstROI = new CRect[iCount];
	pfConcentration = new float[iCount];

	for (int i = 0; i < iCount; i++)
	{
		pstROI[i] = m_rtParticle[i];

		nCount++;
	}
	
	pfData = new float*[dwHeight];

	for (UINT i = 0; i < dwHeight; i++)
	{
		pfData[i] = new float[dwWidth];
	}

	for (UINT lopy = 0; lopy < dwHeight; lopy++)
	{
		for (UINT lopx = 0; lopx < dwWidth; lopx++)
		{
			pfData[lopy][lopx] = (0.3 * pImageBuf[lopy*(dwWidth * 3) + (lopx * 3) + 0]) + 
				(0.59 * pImageBuf[lopy*(dwWidth * 3) + (lopx * 3) + 1] +
				(0.11 * pImageBuf[lopy*(dwWidth * 3) + (lopx * 3) + 2]));
		}
	}

	///신규 농도 추가 1005
	nResultCount = nGroup = iCount;

	int Mid_x = 0;
	int Mid_y = 0;

	for (int lop = 0; lop < nGroup; lop++)
	{
		float centerSum = 0; int centerCount = 0; double center;
		float side1_Sum = 0; int side1_Count = 0; double side;
		float side2_Sum = 0; int side2_Count = 0;

// 		unsigned short size_x = pstROI[lop].right - pstROI[lop].left;
// 		unsigned short size_y = pstROI[lop].bottom - pstROI[lop].top;
		unsigned short size_x = 10;
		unsigned short size_y = 10;

		for (unsigned short y = pstROI[lop].top - size_y; y <= pstROI[lop].bottom + size_y; y++)
		{
			for (unsigned short x = pstROI[lop].left - size_x; x <= pstROI[lop].right + size_x; x++)
			{
				if (y > 0 && y < dwHeight && x > 0 && x < dwWidth)
				{
					side1_Sum += pfData[y][x];
					side1_Count++;
				}
			}
		}

		for (unsigned short y = pstROI[lop].top; y <= pstROI[lop].bottom; y++)
		{
			for (unsigned short x = pstROI[lop].left; x <= pstROI[lop].right; x++)
			{
				if (y > 0 && y < dwHeight && x > 0 && x < dwWidth)
				{
					side2_Sum += pfData[y][x];
					side2_Count++;
				}
			}
		}

		for (unsigned short y = pstROI[lop].top; y <= pstROI[lop].bottom; y++)
		{
			for (unsigned short x = pstROI[lop].left; x <= pstROI[lop].right; x++)
			{
				if (y > 0 && y < dwHeight && x > 0 && x < dwWidth)
				{
					centerSum += pfData[y][x];
					centerCount++;
				}
			}
		}

		center = (double)centerSum / (double)centerCount;
		side = (double)(side1_Sum - side2_Sum) / (double)(side1_Count - side2_Count);

		pfConcentration[lop] = (float)(100 - (center / side) * 100);

		if (pfConcentration[lop] > 100)
			pfConcentration[lop] = 100;
		else if (pfConcentration[lop] < 0)
			pfConcentration[lop] = 0;
	}

	for (UINT nSize = 0; nSize < dwHeight; nSize++)
		free(pfData[nSize]);

	free(pfData);

	int _Final_Cnt = 0;

	// [Input] 멍 농도
	// 최종 결과 
	for (int i = 0; i < nResultCount; i++)
	{
		int Center_X = (pstROI[i].left + pstROI[i].right) / 2;
		int Center_Y = (pstROI[i].top + pstROI[i].bottom) / 2;

		int StainPosition = ppchArea[Center_Y][Center_X];

		if (pfConcentration[i] >= ParticleOpt.fThreConcentration[StainPosition])
		{
			if (_Final_Cnt < 2000)
			{
				ParticleResult.byType[_Final_Cnt] = StainPosition;
				ParticleResult.stROI[_Final_Cnt] = pstROI[i];
				ParticleResult.fConcentration[_Final_Cnt] = pfConcentration[i];
				_Final_Cnt++;
				cvRectangle(srcImage, cvPoint(pstROI[i].left, pstROI[i].top), cvPoint(pstROI[i].right, pstROI[i].bottom), CV_RGB(255, 0, 0), 1, 8);
				TRACE(_T(" %d ------ Cons: %f --- Pos:(%d, %d) --- O \n"), i, pfConcentration[i], Center_X, Center_Y);

			}
			
		}
		else
		{
			TRACE(_T(" %d ------ Cons: %f --- Pos:(%d, %d) --- X \n"), i, pfConcentration[i], Center_X, Center_Y);
		}

	}
	ParticleResult.byRoiCnt = _Final_Cnt;


	delete[] pstROI;
	delete[] pfConcentration;


	if (nullptr != ppchArea)
	{
		for (UINT i = 0; i < dwHeight; i++)
			free(ppchArea[i]);
		free(ppchArea);
	}

}
