﻿//*****************************************************************************
// Filename	: 	DigitalIOCtrl.cpp
// Created	:	2017/02/05 - 15:59
// Modified	:	2017/02/05 - 15:59
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "DigitalIOCtrl.h"

#if defined(_WIN64)
#pragma	comment(lib,"..\\Lib\\64bit\\AXL_x64.lib")
#else
#pragma	comment(lib,"..\\Lib\\32bit\\AXL.lib")
#endif

CDigitalIOCtrl::CDigitalIOCtrl()
{
	m_hExternalExitEvent	= NULL;
	m_hInternalExitEvent	= NULL;
	m_hInternalExitEvent	= CreateEvent(NULL, FALSE, FALSE, NULL);
}

CDigitalIOCtrl::~CDigitalIOCtrl()
{
	SetEvent(m_hInternalExitEvent);

	if (NULL != m_hInternalExitEvent)
		CloseHandle(m_hInternalExitEvent);
}

//=============================================================================
// Method		: Thread_Monitoring
// Access		: protected static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2018/2/13 - 13:42
// Desc.		:
//=============================================================================
UINT WINAPI CDigitalIOCtrl::Thread_Monitoring(__in LPVOID lParam)
{
	ASSERT(NULL != lParam);

	CDigitalIOCtrl* pThis = (CDigitalIOCtrl*)lParam;

	HANDLE hEvent[2] = { NULL, NULL };
	hEvent[0] = pThis->m_hInternalExitEvent;

	if (NULL != pThis->m_hExternalExitEvent)
	{
		hEvent[1] = pThis->m_hExternalExitEvent;
	}

	DWORD dwEvent = 0;

	__try
	{
		pThis->m_bFlag_Mon = TRUE;
		while (pThis->m_bFlag_Mon)
		{
			// 종료 이벤트, 연결해제 이벤트 체크
			if (NULL != hEvent[1])
				dwEvent = WaitForMultipleObjects(2, hEvent, FALSE, pThis->m_dwMonCycle);
			else
				dwEvent = WaitForSingleObject(pThis->m_hInternalExitEvent, pThis->m_dwMonCycle);

			switch (dwEvent)
			{
			case WAIT_OBJECT_0:	// Exit Program
				TRACE(_T(" -- 프로그램 종료 m_hInternalExitEvent 이벤트 감지 \n"));
				pThis->m_bFlag_Mon = FALSE;
				break;

			case WAIT_OBJECT_0 + 1:// Exit Program
				TRACE(_T(" -- 프로그램 종료 m_hExternalExitEvent 이벤트 감지 \n"));
				pThis->m_bFlag_Mon = FALSE;
				break;

			case WAIT_TIMEOUT:
				pThis->Monitoring_Status();

				break;
			}
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CADLink_DASK::Thread_Monitoring()\n"));
	}

	TRACE(_T("쓰레드 종료 : CADLink_DASK::Thread_Monitoring Loop Exit\n"));
	return TRUE;
}

//=============================================================================
// Method		: Monitoring_Status
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/13 - 13:46
// Desc.		:
//=============================================================================
void CDigitalIOCtrl::Monitoring_Status()
{
	Read_DI_Status();

	Read_DO_Status();
}

//=============================================================================
// Method		: Read_DI_Status
// Access		: virtual protected  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2018/2/13 - 13:47
// Desc.		:
//=============================================================================
DWORD CDigitalIOCtrl::Read_DI_Status()
{
	DWORD	dwResult = NULL;
	DWORD	dwTemp	 = NULL;
	DWORD64 dwValue  = NULL;
	long	lPortCnt = 0;

	for (long lModule = m_lDIO_ModuleCnt - 1; 0 <= lModule; lModule--)
	{
		DWORD dwResult = AxdiReadInportDword(lModule, 0, &dwTemp);

		if (dwResult != AXT_RT_SUCCESS)
			return dwResult;
		
		dwValue  |= dwTemp;
		dwResult = AxdInfoGetInputCount(lModule, &lPortCnt);
		
		if (dwResult != AXT_RT_SUCCESS)
			return dwResult;

		if (0 != lModule)
		{
			dwValue = dwTemp << lPortCnt;
		}
	}

	if (TRUE == m_bStored_DI)
	{
		if (dwValue != m_dwDI_ReadData)
		{
			m_dwDI_ReadData = dwValue;

			// Bit 변화 감지하여 윈도우 메세지로 통보
			if ((NULL != m_hOwnerWnd) && (NULL != m_nWMBitChanged))
			{
				::SendNotifyMessage(m_hOwnerWnd, m_nWMBitChanged, (WPARAM)ReadIdx_DI, (LPARAM)m_dwDI_ReadData);
			}
		}
	}
	else
	{
		m_bStored_DI		= TRUE;
		m_dwDI_ReadData		= dwValue;

		if ((NULL != m_hOwnerWnd) && (NULL != m_nWMFirstRead))
		{
			::SendNotifyMessage(m_hOwnerWnd, m_nWMFirstRead, (WPARAM)ReadIdx_DI, (LPARAM)m_dwDI_ReadData);
		}
	}

	return dwResult;
}

//=============================================================================
// Method		: Read_DO_Status
// Access		: virtual protected  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2018/2/13 - 13:47
// Desc.		:
//=============================================================================
DWORD CDigitalIOCtrl::Read_DO_Status()
{
	DWORD	dwResult = NULL;
	DWORD	dwTemp	 = NULL;
	DWORD64 dwValue  = NULL;
	long	lPortCnt = 0;

	for (long lModule = m_lDIO_ModuleCnt - 1; 0 <= lModule; lModule--)
	{
		DWORD dwResult = AxdoReadOutportDword(lModule, 0, &dwTemp);

		if (dwResult != AXT_RT_SUCCESS)
			return dwResult;

		dwValue |= dwTemp;
		dwResult = AxdInfoGetInputCount(lModule, &lPortCnt);

		if (dwResult != AXT_RT_SUCCESS)
			return dwResult;

		if (0 != lModule)
		{
			dwValue = dwTemp << lPortCnt;
		}
	}

	if (TRUE == m_bStored_DO)
	{
		if (dwValue != m_dwDO_ReadData)
		{
			m_dwDO_ReadData = dwValue;

			// Bit 변화 감지하여 윈도우 메세지로 통보
			if ((NULL != m_hOwnerWnd) && (NULL != m_nWMBitChanged))
			{
				::SendNotifyMessage(m_hOwnerWnd, m_nWMBitChanged, (WPARAM)ReadIdx_DO, (LPARAM)m_dwDO_ReadData);
			}
		}
	}
	else
	{
		m_bStored_DO	= TRUE;
		m_dwDO_ReadData = dwValue;

		if ((NULL != m_hOwnerWnd) && (NULL != m_nWMFirstRead))
		{
			::SendNotifyMessage(m_hOwnerWnd, m_nWMFirstRead, (WPARAM)ReadIdx_DO, (LPARAM)m_dwDO_ReadData);
		}
	}

	return dwResult;
}

//=============================================================================
// Method		: AXTInit
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/5 - 15:21
// Desc.		:
//=============================================================================
BOOL CDigitalIOCtrl::AXTInit()
{
	//AxdInfoIsDIOModule	: AxdInfoIsDIOModule 함수는 실제 DIO 모듈이 존재하는가 여부를 확인하는 함수이다. DIO 관련 함수를 사용하기 전에 DIO 모듈이 있는지를 먼저 확인하기 위해 사용한다.
	//AxdInfoGetModuleCount : AxdInfoGetModuleCount 함수는 전체 시스템에 장착되어 있는 DIO 모듈의 개수를 확인하는 함수이다.
	//AxdInfoGetInputCount	: AxdInfoGetInputCount함수는 지정한 DIO 모듈에서 입력채널의 개수를 확인하는 함수 이다.
	//AxdInfoGetOutputCount : AxdInfoGetOutputCount 함수는 지정한 DIO 모듈에서 출력채널의 개수를 확인하는 함수이다.
	//AxdInfoGetModuleNo	: 초기화 되어 있는 전체 DIO 모듈 번호를 확인한다

	// Initialize library 
	// 7은 IRQ를 뜻한다. PCI에서는 자동으로 IRQ가 설정된다.
	DWORD dwResult = AxlOpenNoReset(AXL_DEFAULT_IRQNO);
	if (dwResult == AXT_RT_SUCCESS || dwResult == AXT_RT_OPEN_ALREADY)
	{
		TRACE(_T("AXL Library is initialized .\n"));

		// Inspect if DIO module exsits
		dwResult = AxdInfoIsDIOModule(&m_stAxdBoardInfo.dwStatus);
		if (m_stAxdBoardInfo.dwStatus == STATUS_EXIST)
		{
			TRACE(_T("DIO module exists.\n"));

			// DIO 모듈의 개수를 확인
			dwResult = AxdInfoGetModuleCount(&m_stAxdBoardInfo.lModuleCounts);

			if (dwResult == AXT_RT_SUCCESS)
			{
				TRACE(_T("Number of DIO module: %d\n"), m_stAxdBoardInfo.lModuleCounts);

				// IO Check
				dwResult = Get_DIO_Assign();
			}
			else
			{
				TRACE(_T("AxdInfoGetModuleCount() : ERROR code Ox%x\n"), dwResult);
			}
		}
		else
		{
			TRACE(_T("Module not exist.\n"));
			return dwResult;
		}
	}
	else
	{
		TRACE(_T("Failed initialization.\n"));
		return dwResult;
	}

	m_stAxdBoardInfo.bAxlIsOpened = TRUE;

	return TRUE;
}

//=============================================================================
// Method		: AXTState
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/5 - 15:55
// Desc.		:
//=============================================================================
BOOL CDigitalIOCtrl::AXTState()
{
	return	m_stAxdBoardInfo.bAxlIsOpened;
}

//=============================================================================
// Method		: GetDIOAssign
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/13 - 14:23
// Desc.		:
//=============================================================================
DWORD CDigitalIOCtrl::Get_DIO_Assign()
{
	DWORD	dwResult;

	// 모듈의 개수
	dwResult = AxdInfoGetModuleCount(&m_lDIO_ModuleCnt);

	if (dwResult != AXT_RT_SUCCESS)
		return dwResult;

	// 1개도 없으면 리턴
	if (m_lDIO_ModuleCnt < 1)
		return dwResult;

	m_lDI_PortCnt = 0;
	m_lDO_PortCnt = 0;

	for (long lModule = 0; lModule < m_lDIO_ModuleCnt; lModule++)
	{
		long lPortCnt = 0;

		// 해당 모듈의 Input 개수
		dwResult = AxdInfoGetInputCount(lModule, &lPortCnt);

		if (dwResult != AXT_RT_SUCCESS)
			return dwResult;
		
		m_lDI_PortCnt += lPortCnt;

		// 해당 모듈의 Output 개수
		dwResult = AxdInfoGetOutputCount(lModule, &lPortCnt);

		if (dwResult != AXT_RT_SUCCESS)
			return dwResult;

		m_lDO_PortCnt += lPortCnt;
	}

	return dwResult;
}

//=============================================================================
// Method		: GetInputPortCnt
// Access		: public  
// Returns		: long
// Qualifier	:
// Last Update	: 2017/6/28 - 11:40
// Desc.		:
//=============================================================================
long CDigitalIOCtrl::Get_DI_PortCnt()
{
	return m_lDI_PortCnt;
}

//=============================================================================
// Method		: GetOutputPortCnt
// Access		: public  
// Returns		: longa
// Qualifier	:
// Last Update	: 2017/6/28 - 11:40
// Desc.		:
//=============================================================================
long CDigitalIOCtrl::Get_DO_PortCnt()
{
	return m_lDO_PortCnt;
}

//=============================================================================
// Method		: Get_DI_Status_All
// Access		: virtual protected  
// Returns		: DWORD
// Parameter	: __out DWORD64 dwValue
// Qualifier	:
// Last Update	: 2018/2/13 - 14:47
// Desc.		:
//=============================================================================
void CDigitalIOCtrl::Get_DI_Status_All(__out DWORD64 dwValue)
{
	dwValue = m_dwDI_ReadData;
}

//=============================================================================
// Method		: Get_DI_Status
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in long lOffset
// Qualifier	:
// Last Update	: 2018/2/13 - 15:33
// Desc.		:
//=============================================================================
BOOL CDigitalIOCtrl::Get_DI_Status(__in long lOffset)
{
	DWORD	dwResult;
	long	lPortCnt	= 0;
	long	lPortAddCnt = 0;

	for (long lPort = 0; lPort < m_lDIO_ModuleCnt; lPort++)
	{
		dwResult = AxdInfoGetInputCount(lPort, &lPortCnt);

		if (dwResult != AXT_RT_SUCCESS)
			return dwResult;

		lPortAddCnt += lPortCnt;

		if (lOffset < lPortAddCnt)
		{
			return (((m_dwDI_ReadData >> lOffset) & 0x0001) == 0x0001) ? TRUE : FALSE;
		}
	}

	return FALSE;
}

//=============================================================================
// Method		: Get_DO_Status_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __out DWORD64 dwValue
// Qualifier	:
// Last Update	: 2018/2/13 - 15:17
// Desc.		:
//=============================================================================
void CDigitalIOCtrl::Get_DO_Status_All(__out DWORD64 dwValue)
{
	dwValue = m_dwDO_ReadData;
}

//=============================================================================
// Method		: Get_DO_Status
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in long lOffset
// Qualifier	:
// Last Update	: 2018/2/13 - 15:17
// Desc.		:
//=============================================================================
BOOL CDigitalIOCtrl::Get_DO_Status(__in long lOffset)
{
	DWORD	dwResult;
	long	lPortCnt	= 0;
	long	lPortAddCnt = 0;

	for (long lPort = 0; lPort < m_lDIO_ModuleCnt; lPort++)
	{
		dwResult = AxdInfoGetOutputCount(lPort, &lPortCnt);

		if (dwResult != AXT_RT_SUCCESS)
			return dwResult;

		lPortAddCnt += lPortCnt;

		if (lOffset < lPortAddCnt)
		{
			return (((m_dwDO_ReadData >> lOffset) & 0x0001) == 0x0001) ? TRUE : FALSE;
		}
	}

	return FALSE;
}

//=============================================================================
// Method		: Set_DO_Status
// Access		: virtual protected  
// Returns		: DWORD
// Parameter	: __in long lOffset
// Parameter	: __in enum_IO_SignalType SignalType
// Qualifier	:
// Last Update	: 2018/2/13 - 15:32
// Desc.		:
//=============================================================================
DWORD CDigitalIOCtrl::Set_DO_Status(__in long lOffset, __in enum_IO_SignalType SignalType /*= IO_SignalT_SetOn*/)
{
	DWORD	dwResult = NULL;
	
	long	lPortCnt	= 0;
	long	lPortAddCnt = 0;
	long 	lOffsetTemp = 0;

	lOffsetTemp = lOffset;

	for (long lModule = 0; lModule < m_lDIO_ModuleCnt; lModule++)
	{
		dwResult = AxdInfoGetOutputCount(lModule, &lPortCnt);

		if (dwResult != AXT_RT_SUCCESS)
			return dwResult;

		lPortAddCnt += lPortCnt;

		if (lOffset < lPortAddCnt)
		{

			switch (SignalType)
			{
			case IO_SignalT_SetOff: // 0으로 세팅
				dwResult = AxdoWriteOutportBit(lModule, lOffsetTemp, 0x00);
				break;

			case IO_SignalT_SetOn:	// 1로 세팅
				dwResult = AxdoWriteOutportBit(lModule, lOffsetTemp, 0x01);
				break;

			case IO_SignalT_PulseOff: // 0으로 세팅 후 1로 다시 세팅
				dwResult = AxdoOutPulseOff(lModule, lOffsetTemp, m_lOutPulseDelay);
				break;

			case IO_SignalT_PulseOn: // 1로 세팅 후 0으로 다시 세팅
				dwResult = AxdoOutPulseOn(lModule, lOffsetTemp, m_lOutPulseDelay);
				break;

			case IO_SignalT_ToggleStart: // 점멸 기능 : 1 -> 0 -> 1 -> 0 ....
				dwResult = AxdoToggleStart(lModule, lOffsetTemp, 1, m_lToggleOnTimeDelay, m_lToggleOffTimeDelay, m_lToggleCount);
				break;

			case IO_SignalT_ToggleStop:
				dwResult = AxdoToggleStop(lModule, lOffsetTemp, 0x00);
				break;

			default:
				break;
			}

			return dwResult;
		}

		lOffsetTemp -= lPortCnt;
	}

	return dwResult;
}

//=============================================================================
// Method		: SetExitEvent
// Access		: protected  
// Returns		: void
// Parameter	: __in HANDLE hExitEvent
// Qualifier	:
// Last Update	: 2018/1/4 - 19:30
// Desc.		:
//=============================================================================
void CDigitalIOCtrl::SetExitEvent(__in HANDLE hExitEvent)
{
	m_hExternalExitEvent = hExitEvent;
}

//=============================================================================
// Method		: Start_Monitoring
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/1/4 - 16:40
// Desc.		:
//=============================================================================
BOOL CDigitalIOCtrl::Start_Monitoring()
{
	if (NULL != m_hThr_Mon)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_Mon, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			//AfxMessageBox(_T("Digital I/O 모니터링 쓰레드가 동작 중 입니다."), MB_SYSTEMMODAL);
			TRACE(_T("Digital I/O 모니터링 쓰레드가 동작 중 입니다.\n"));
			return FALSE;
		}
	}

	if (NULL != m_hThr_Mon)
	{
		CloseHandle(m_hThr_Mon);
		m_hThr_Mon = NULL;
	}

	m_hThr_Mon = HANDLE(_beginthreadex(NULL, 0, Thread_Monitoring, this, 0, NULL));

	return TRUE;
}

//=============================================================================
// Method		: Stop_Monitoring
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/2/13 - 15:42
// Desc.		:
//=============================================================================
BOOL CDigitalIOCtrl::Stop_Monitoring()
{
	m_bFlag_Mon = FALSE;

	if (NULL != m_hThr_Mon)
	{
		WaitForSingleObject(m_hThr_Mon, m_dwMonCycle);

		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_Mon, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TerminateThread(m_hThr_Mon, dwExitCode);
			WaitForSingleObject(m_hThr_Mon, WAIT_ABANDONED);
			CloseHandle(m_hThr_Mon);
			m_hThr_Mon = NULL;
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: Set_MonitoringCycle
// Access		: public  
// Returns		: void
// Parameter	: __in DWORD dwMiliseconds
// Qualifier	:
// Last Update	: 2018/2/13 - 15:42
// Desc.		:
//=============================================================================
void CDigitalIOCtrl::Set_MonitoringCycle(__in DWORD dwMiliseconds)
{
	m_dwMonCycle = dwMiliseconds;
}

//=============================================================================
// Method		: SetOwnerHwnd
// Access		: public  
// Returns		: void
// Parameter	: __in HWND hOwnerWnd
// Qualifier	:
// Last Update	: 2018/2/13 - 15:42
// Desc.		:
//=============================================================================
void CDigitalIOCtrl::SetOwnerHwnd(__in HWND hOwnerWnd)
{
	m_hOwnerWnd = hOwnerWnd;
}

//=============================================================================
// Method		: Set_WM_BitChanged
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nWindowMsg
// Qualifier	:
// Last Update	: 2018/2/13 - 15:49
// Desc.		:
//=============================================================================
void CDigitalIOCtrl::Set_WM_BitChanged(__in UINT nWindowMsg)
{
	m_nWMBitChanged = nWindowMsg;
}

//=============================================================================
// Method		: Set_WM_FirstRead
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nWindowMsg
// Qualifier	:
// Last Update	: 2018/2/13 - 15:49
// Desc.		:
//=============================================================================
void CDigitalIOCtrl::Set_WM_FirstRead(__in UINT nWindowMsg)
{
	m_nWMFirstRead = nWindowMsg;
}
