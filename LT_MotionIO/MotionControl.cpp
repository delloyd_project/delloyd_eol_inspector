﻿//*****************************************************************************
// Filename	: 	MotionControl
// Created	:	2016/9/23 - 13:17
// Modified	:	2016/9/23 - 13:17
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#include "StdAfx.h"
#include "MotionControl.h"

#if defined(_WIN64)
#pragma	comment(lib,"..\\Lib\\64bit\\AXL_x64.lib")
#else
#pragma	comment(lib,"..\\Lib\\32bit\\AXL.lib")
#endif

// double m_AxisUnit[6] = { 10.0, 20.0, 10.0, 36.0, 24.0, 10.0};
// long m_AxisPulse = 10000;
// double m_AxisUnit = 10000.0;

CMotionControl::CMotionControl(void)
{
	m_pstMotorParam = NULL;
	m_hThrOrigin	= NULL;
	m_bMoveStop		= FALSE;
	m_bOriginStop	= FALSE;
}

CMotionControl::~CMotionControl(void)
{
}

//=============================================================================
// Method		: SetPtrMotorParam
// Access		: public  
// Returns		: void
// Parameter	: ST_MotionParam * pstMotorParam
// Qualifier	:
// Last Update	: 2017/3/27 - 15:27
// Desc.		:
//=============================================================================
void CMotionControl::SetPtrMotorParam(ST_MotionParam *pstMotorParam)
{
	if (pstMotorParam == NULL)
		return;

	m_pstMotorParam = pstMotorParam;
}

//=============================================================================
// Method		: MotorOpen
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/23 - 14:50
// Desc.		:
//=============================================================================
BOOL CMotionControl::SetMotorOpen()
{
	DWORD dwMode	= 0;
	DWORD dwPLevel	= 0;
	DWORD dwNLevel	= 0;
	long  lAxisCnt  = 0;

	AxmInfoGetAxisCount(&lAxisCnt);
	if (m_pstMotorParam->iAxisNum > (int)lAxisCnt || m_pstMotorParam->iAxisNum < 0)
		return FALSE;

	// 2. 모터드라이버에 정보 설정, Encoder 입력 method 설정 (13 -> -4체배)
	AxmMotSetEncInputMethod(m_pstMotorParam->iAxisNum, m_pstMotorParam->iAxisEncInputMethod);

	AxmMotSetPulseOutMethod(m_pstMotorParam->iAxisNum, m_pstMotorParam->iAxisOutputMethod);

	// 제어 단위를 설정하는 함수, 1/1 인 경우 지령값을 Pulse 단위로 사용할때 사용
	AxmMotSetMoveUnitPerPulse(m_pstMotorParam->iAxisNum, (long)m_pstMotorParam->dbUnit, (long)m_pstMotorParam->dbPulse);

	AxmMotSetMaxVel(m_pstMotorParam->iAxisNum, m_pstMotorParam->dbMaxVel);

	AxmSignalGetLimit(m_pstMotorParam->iAxisNum, &dwMode, &dwPLevel, &dwNLevel);

	AxmSignalSetLimit(m_pstMotorParam->iAxisNum, (DWORD)dwMode, (DWORD)m_pstMotorParam->dbMotorNegaLimitLevel, (DWORD)m_pstMotorParam->dbMotorPosiLimitLevel);

	// 홈센서 Active레벨을 설정하는 함수
	AxmHomeSetSignalLevel(m_pstMotorParam->iAxisNum, m_pstMotorParam->iAxisHomeSenLevel);

	// 서보팩의 위치결정완료 신호 사용여부 및 Active레벨을 설정하는 함수
	AxmSignalSetInpos(m_pstMotorParam->iAxisNum, (DWORD)m_pstMotorParam->iAxisInpositionLevel);

	AxmSignalSetServoAlarm(m_pstMotorParam->iAxisNum, (DWORD)m_pstMotorParam->iAxisAlarmLevel);
	
	AxmSignalSetServoOnLevel(m_pstMotorParam->iAxisNum, (DWORD)m_pstMotorParam->iAxisServoLevel);

	SetMotorAmpCtr(ON);

	AxmSignalSetStop(m_pstMotorParam->iAxisNum, 0, (DWORD)m_pstMotorParam->dbStopLevel);

	// 서보팩의 위치결정완료 신호 사용여부 및 Active레벨을 설정하는 함수
	AxmMotSetProfileMode(m_pstMotorParam->iAxisNum, 0);

	// 서보팩의 위치결정완료 신호 사용여부 및 Active레벨을 설정하는 함수
	AxmSignalSetZphaseLevel(m_pstMotorParam->iAxisNum, 1);

	AxmSignalSetLimit(m_pstMotorParam->iAxisNum, 0, (DWORD)m_pstMotorParam->iAxisPosLimitSenLevel, (DWORD)m_pstMotorParam->iAxisNegLimitSenLevel);

	// EMO 기능 접점 변경, PCI-N804 / 404 보드만 가능한 기능
	AxmSignalSetStop(m_pstMotorParam->iAxisNum, 0, (DWORD)m_pstMotorParam->iAxisEmergencyLevel);

	return TRUE;
}

//=============================================================================
// Method		: MotorClose
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/23 - 14:50
// Desc.		:
//=============================================================================
BOOL CMotionControl::SetMotorClose()
{
	DWORD dwStatus = 0;

	dwStatus = AxmSignalServoOn(m_pstMotorParam->iAxisNum, DISABLE);

	if (dwStatus = !AXT_RT_SUCCESS)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: MotorSetting
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/9/23 - 15:22
// Desc.		:
//=============================================================================
void CMotionControl::SetMotorSetting()
{
	// 전원 레벨
	AxmSignalSetServoOnLevel(m_pstMotorParam->iAxisNum, (DWORD)m_pstMotorParam->iAxisServoLevel);

	// 알람 사용 유무
	AxmSignalSetServoAlarm(m_pstMotorParam->iAxisNum, (DWORD)m_pstMotorParam->iAxisAlarmLevel);

	// 홈 레벨
	AxmHomeSetSignalLevel(m_pstMotorParam->iAxisNum, m_pstMotorParam->iAxisHomeSenLevel);

	// 모터 방향
	AxmMotSetPulseOutMethod(m_pstMotorParam->iAxisNum, m_pstMotorParam->iAxisOutputMethod);

	// 리미트 사용 유무
	AxmSignalSetLimit(m_pstMotorParam->iAxisNum, 0, (DWORD)m_pstMotorParam->iAxisPosLimitSenLevel, (DWORD)m_pstMotorParam->iAxisNegLimitSenLevel);

	// Inposition 레벨 Step 모터 -> 사용 x
	AxmSignalSetInpos(m_pstMotorParam->iAxisNum, (DWORD)m_pstMotorParam->iAxisInpositionLevel);

	// Enc 체베
	AxmMotSetEncInputMethod(m_pstMotorParam->iAxisNum, m_pstMotorParam->iAxisEncInputMethod);
	
	// 제한 속도도 같이 변경
	AxmMotSetMaxVel(m_pstMotorParam->iAxisNum, m_pstMotorParam->dbVel);

	// EMO 기능 접점 변경, PCI-N804 / 404 보드만 가능한 기능
	AxmSignalSetStop(m_pstMotorParam->iAxisNum, 0, (DWORD)m_pstMotorParam->iAxisEmergencyLevel);

	// 제어 단위를 설정하는 함수, 1/1 인 경우 지령값을 Pulse 단위로 사용할때 사용
	AxmMotSetMoveUnitPerPulse(m_pstMotorParam->iAxisNum, (long)m_pstMotorParam->dbUnit, (long)m_pstMotorParam->dbPulse);
}

//=============================================================================
// Method		: ThreadOrigin
// Access		: public static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2017/3/27 - 16:42
// Desc.		:
//=============================================================================
UINT WINAPI CMotionControl::ThreadOrigin(__in LPVOID lParam)
{
	CMotionControl* pCtrl = (CMotionControl*)lParam;

	pCtrl->OriginFunction();

	return 0;
}

//=============================================================================
// Method		: DoEvents
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/3 - 17:33
// Desc.		:
//=============================================================================
void CMotionControl::DoEvents()
{
	MSG msg;

	if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		if (!AfxGetApp()->PreTranslateMessage(&msg))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
	}

	::Sleep(0);
}

//=============================================================================
// Method		: DoEvents
// Access		: public  
// Returns		: void
// Parameter	: DWORD dwMiliSeconds
// Qualifier	:
// Last Update	: 2017/2/3 - 17:33
// Desc.		:
//=============================================================================
void CMotionControl::DoEvents(DWORD dwMiliSeconds)
{
	timeBeginPeriod(1);

	clock_t start_tm = clock();
	do
	{
		DoEvents();
		::Sleep(1);
	} while ((DWORD)(clock() - start_tm) < dwMiliSeconds);
}

//=============================================================================
// Method		: AxisOrigin
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/23 - 15:23
// Desc.		:
//=============================================================================
BOOL CMotionControl::OriginStartAxis()
{
	if (NULL != m_hThrOrigin)
	{
		CloseHandle(m_hThrOrigin);
		m_hThrOrigin = NULL;
	}

	m_hThrOrigin = HANDLE(_beginthreadex(NULL, 0, ThreadOrigin, this, 0, NULL));

	return TRUE;
}

//=============================================================================
// Method		: MotorOrigin
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/9/23 - 15:29
// Desc.		:
//=============================================================================
void CMotionControl::OriginFunction()
{
	// Z상 검출 유무 0 -> 미검출, 1 -> 검출
	DWORD  dwAxismZphas		= 0;

	// 원점 검색 후 Enc 값 Set하기 위한 대기 시간
	double dHomeClearTime	= 2000.0;

	DWORD dwHomeMainStepNumber;
	DWORD dwHomeStepNumber;
	DWORD dwAlramStatus;

	m_bOriginStop = FALSE;

	AxmHomeSetMethod(m_pstMotorParam->iAxisNum, m_pstMotorParam->iAxisHomeDir, m_pstMotorParam->iAxisHomeSig, dwAxismZphas, dHomeClearTime, m_pstMotorParam->dbOriginOffset);
	AxmHomeSetVel(m_pstMotorParam->iAxisNum, m_pstMotorParam->dbOriginVelFirst, m_pstMotorParam->dbOriginVelSecond, m_pstMotorParam->dbOriginVelThird, m_pstMotorParam->dbOriginVelLast, m_pstMotorParam->dbOriginAccFirst, m_pstMotorParam->dbOriginAccFirst);

	// 원점 수행 시작
	AxmHomeSetStart(m_pstMotorParam->iAxisNum);

	do
	{
		AxmHomeGetRate(m_pstMotorParam->iAxisNum, &dwHomeMainStepNumber, &dwHomeStepNumber);

		AxmSignalReadServoAlarm(m_pstMotorParam->iAxisNum, &dwAlramStatus);

		if (dwAlramStatus == TRUE)
		{
			OriginStopAxis();
			break;
		}

		if (dwHomeStepNumber == 100)
		{
			DWORD dwHomeResult;
			AxmHomeGetResult(m_pstMotorParam->iAxisNum, &dwHomeResult);

			if (dwHomeResult == FALSE)
				break;
		}

		if (m_bOriginStop)
			break;

	} while (!GetOriginStatus());
}

//=============================================================================
// Method		: OriginStopAxis
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/23 - 15:29
// Desc.		:
//=============================================================================
BOOL CMotionControl::OriginStopAxis()
{
	DWORD dwStatus = 0;

	dwStatus = AxmMoveEStop(m_pstMotorParam->iAxisNum);
	
	if (dwStatus != AXT_RT_SUCCESS)
		return FALSE;

	m_bOriginStop = TRUE;

	return TRUE;
}

//=============================================================================
// Method		: MotorEStopAxis
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/23 - 15:29
// Desc.		:
//=============================================================================
BOOL CMotionControl::MotorAxisEStop()
{
	DWORD dwStatus = 0;

	m_bMoveStop = TRUE;

	dwStatus = AxmMoveEStop(m_pstMotorParam->iAxisNum);
	
	if (dwStatus != AXT_RT_SUCCESS)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: MotorSStopAxis
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/23 - 15:29
// Desc.		:
//=============================================================================
BOOL CMotionControl::MotorAxisSStop()
{
	DWORD dwStatus = 0;

	m_bMoveStop = TRUE;

	dwStatus = AxmMoveSStop(m_pstMotorParam->iAxisNum);
	
	if (dwStatus != AXT_RT_SUCCESS)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: JogAxisMove
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nDir
// Parameter	: UINT nJogVel
// Qualifier	:
// Last Update	: 2017/3/27 - 17:35
// Desc.		:
//=============================================================================
BOOL CMotionControl::MotorAxisJogMove(UINT nDir, UINT nJogVel)
{
	double dbVel = 0.0;
	double dbAcc = 0.0;
	double dbDir = 0.0;
	DWORD dwStatus = 0;

	if (nDir == 0)
		dbDir = -1.0;
	else
		dbDir = 1.0;

	dbVel = (dbDir * m_pstMotorParam->dbVel) / 10.0 * nJogVel;
	dbAcc = m_pstMotorParam->dbAcc / 5.0;

	// 지정한 속도로 연속구동 함수
	dwStatus = AxmMoveVel(m_pstMotorParam->iAxisNum, dbVel, dbAcc, dbAcc);
	
	if (dwStatus != AXT_RT_SUCCESS)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: MotroAxisMovePos
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAbsRelMode
// Parameter	: double dbPos
// Qualifier	:
// Last Update	: 2017/3/27 - 18:00
// Desc.		:
//=============================================================================
BOOL CMotionControl::MotorAxisMove(UINT nAbsRelMode, double dbPos)
{
	DWORD	dwAbsRelMode		= 0;
	DWORD	dwGetAbsRelMode		= 0;
	DWORD	dwStatus			= 0;
	double	dbGetPos			= 0.0;
	double	dbAddPos			= 0.0;

 	m_bMoveStop = FALSE;

	// ABS, REL 모드 설정
	if (nAbsRelMode == 0)
		dwAbsRelMode = POS_ABS_MODE;
	else
		dwAbsRelMode = POS_REL_MODE;

	// 셋팅
	AxmMotSetAbsRelMode(m_pstMotorParam->iAxisNum, dwAbsRelMode);

	// 확인
	AxmMotGetAbsRelMode(m_pstMotorParam->iAxisNum, &dwGetAbsRelMode);
	
	if (dwAbsRelMode != dwGetAbsRelMode)
		return FALSE;

	// 소숫점 버림, KHO 확인 필요
	dbPos = (double)floor(dbPos);

	dwStatus = AxmMoveStartPos(m_pstMotorParam->iAxisNum, dbPos, m_pstMotorParam->dbVel, m_pstMotorParam->dbAcc, m_pstMotorParam->dbAcc);
	
	if (dwStatus != AXT_RT_SUCCESS)
	{
		AlarmMessage(dwStatus);
		return FALSE;
	}

	// 동작 완료 여부를 확인하자
	if(!GetMoveingStatus())
		return FALSE;

	// 현재 위치 인지 확인
	dbGetPos = GetCurrentPos();

	if (abs(dbPos) - abs(dbGetPos) > 10)
		return FALSE;

	if (m_pstMotorParam->iAxisAlarmLevel != 2)
	{
		dwStatus = GetAlarmStatus();

		if (dwStatus > 0)
			return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: MotroAxisDitalMove
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAbsRelMode
// Parameter	: PST_AxisMoveParam pAxisParam
// Qualifier	:
// Last Update	: 2017/3/28 - 10:22
// Desc.		:
//=============================================================================
BOOL CMotionControl::MotorAxisDitalMove(UINT nAbsRelMode, PST_AxisMoveParam pAxisParam)
{
	DWORD	dwAbsRelMode	= 0;
	DWORD	dwGetAbsRelMode = 0;
	DWORD	dwStatus		= 0;
	double	dbGetPos		= 0.0;
	double	dbAddPos		= 0.0;

	m_bMoveStop = FALSE;

	// ABS, REL 모드 설정
	if (nAbsRelMode == 0)
		dwAbsRelMode = POS_ABS_MODE;
	else
		dwAbsRelMode = POS_REL_MODE;

	// 셋팅
	AxmMotSetAbsRelMode(m_pstMotorParam->iAxisNum, dwAbsRelMode);

	// 확인
	AxmMotGetAbsRelMode(m_pstMotorParam->iAxisNum, &dwGetAbsRelMode);

	if (dwAbsRelMode != dwGetAbsRelMode)
		return FALSE;

	// 소숫점 버림, KHO 확인 필요
	pAxisParam->dbPos = (double)floor(pAxisParam->dbPos);

	dwStatus = AxmMoveStartPos(m_pstMotorParam->iAxisNum, pAxisParam->dbPos, pAxisParam->dbVel, pAxisParam->dbAcc, pAxisParam->dbAcc);

	if (dwStatus != AXT_RT_SUCCESS)
		return FALSE;

	// 동작 완료 여부를 확인하자
	if (!GetMoveingStatus())
		return FALSE;

	// 현재 위치 인지 확인
	dbGetPos = GetCurrentPos();

	if (abs(pAxisParam->dbPos) - abs(dbGetPos) > 10)
		return FALSE;

	if (m_pstMotorParam->iAxisAlarmLevel != 2)
	{
		dwStatus = GetAlarmStatus();

		if (dwStatus > 0)
			return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: GetAxisUseStatus
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/28 - 13:49
// Desc.		:
//=============================================================================
BOOL CMotionControl::GetAxisUseStatus()
{
	if (m_pstMotorParam == NULL)
		return FALSE;

	if (m_pstMotorParam->bAxisUse == TRUE)
		return	TRUE;

	return FALSE;
}

//=============================================================================
// Method		: AlarmStatus
// Access		: public  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2016/9/23 - 15:52
// Desc.		:
//=============================================================================
DWORD CMotionControl::GetAlarmStatus()
{
	DWORD dwStatus;

	AxmSignalReadServoAlarm(m_pstMotorParam->iAxisNum, &dwStatus);

	return dwStatus;
}


//=============================================================================
// Method		: SetOriginReset
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/5/29 - 14:20
// Desc.		:
//=============================================================================
BOOL CMotionControl::SetOriginReset()
{
	DWORD dwStatus = 0x02;

	AxmHomeSetResult(m_pstMotorParam->iAxisNum, dwStatus);

	return TRUE;
}

//=============================================================================
// Method		: OriginStatus
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/10/28 - 13:51
// Desc.		:
//=============================================================================
BOOL CMotionControl::GetOriginStatus()
{
	DWORD dwStatus;

	AxmHomeGetResult(m_pstMotorParam->iAxisNum, &dwStatus);

	if (dwStatus != 0x01)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: IsPulseEnd
// Access		: public  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2016/9/23 - 15:52
// Desc.		:
//=============================================================================
DWORD CMotionControl::GetMoveingStatus()
{
	// 펄스 동작중인지 상태를 확인하자(움직임여부 확인가능)
	DWORD	dwStatus = 0;

	clock_t start_tm = clock();
	do
	{
		AxmStatusReadInMotion(m_pstMotorParam->iAxisNum, &dwStatus);
		
		// 모션 구동 완료
		if (dwStatus == AXT_RT_SUCCESS)
			return TRUE;

		// STOP 버튼이 눌렸을 경우, KHO 확인 필요
		if (m_bMoveStop)
			break;

		DoEvents(500);

	} while ((DWORD)(clock() - start_tm) < 500000);

	return FALSE;
}

//=============================================================================
// Method		: IsMotorAmp
// Access		: public  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2016/9/23 - 15:52
// Desc.		:
//=============================================================================
DWORD CMotionControl::GetAmpStatus()
{
	DWORD	dwStatus;

	AxmSignalIsServoOn(m_pstMotorParam->iAxisNum, &dwStatus);

	return dwStatus;
}

//=============================================================================
// Method		: GetMotionStatus
// Access		: public  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2017/3/30 - 8:04
// Desc.		:
//=============================================================================
DWORD CMotionControl::GetMotionStatus()
{
	DWORD dwStatus = FALSE;

	AxmStatusReadInMotion(m_pstMotorParam->iAxisNum, &dwStatus);

	return dwStatus;
}

//=============================================================================
// Method		: GetPosSensorStatus
// Access		: public  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2016/9/23 - 15:53
// Desc.		:
//=============================================================================
DWORD CMotionControl::GetPosSenStatus()
{
	DWORD dwStatus = FALSE;

	AxmSignalReadLimit(m_pstMotorParam->iAxisNum, &dwStatus, NULL);

	return dwStatus;
}

//=============================================================================
// Method		: GetNegSensorStatus
// Access		: public  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2016/9/23 - 15:53
// Desc.		:
//=============================================================================
DWORD CMotionControl::GetNegSenStatus()
{
	DWORD dwStatus = FALSE;

	AxmSignalReadLimit(m_pstMotorParam->iAxisNum, NULL, &dwStatus);

	return dwStatus;
}

//=============================================================================
// Method		: GetHomeSensorStatus
// Access		: public  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2016/9/23 - 15:53
// Desc.		:
//=============================================================================
DWORD CMotionControl::GetHomeSenStatus()
{
	DWORD dwStatus = FALSE;

	AxmHomeReadSignal(m_pstMotorParam->iAxisNum, &dwStatus);

	return dwStatus;
}

//=============================================================================
// Method		: GetOriginEStopSenStatus
// Access		: public  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2017/3/28 - 15:38
// Desc.		:
//=============================================================================
DWORD CMotionControl::GetOriginEStopSenStatus()
{
	return m_bOriginStop;
}

//=============================================================================
// Method		: GetCurrentPos
// Access		: public  
// Returns		: double
// Qualifier	:
// Last Update	: 2016/9/23 - 15:54
// Desc.		:
//=============================================================================
double CMotionControl::GetCurrentPos()
{
	double dbCntPos = 0.0;

	// 현재 지령펄스와, Encoder펄스를 확인하는 함수
	AxmStatusGetCmdPos(m_pstMotorParam->iAxisNum, &dbCntPos);

// 	dbCntPos = dbCntPos * m_pstMotorParam->dbGearRatio;

	return dbCntPos;
}

//=============================================================================
// Method		: GetMotorResultMsg
// Access		: public  
// Returns		: LPCTSTR
// Qualifier	:
// Last Update	: 2017/3/30 - 9:09
// Desc.		:
//=============================================================================
LPCTSTR CMotionControl::GetMotorResultMsg()
{
	return m_strResultMeg;
}

//=============================================================================
// Method		: SetMotorAmpCtr
// Access		: public  
// Returns		: BOOL
// Parameter	: BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/3/28 - 10:28
// Desc.		:
//=============================================================================
BOOL CMotionControl::SetMotorAmpCtr(BOOL bOnOff)
{
	DWORD dwStatus = 0;

	dwStatus = AxmSignalServoOn(m_pstMotorParam->iAxisNum, bOnOff);

	Sleep(100);

	if (dwStatus != AXT_RT_SUCCESS)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: SetMotorAlarmClear
// Access		: public  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2017/3/28 - 10:29
// Desc.		:
//=============================================================================
BOOL CMotionControl::SetMotorAlarmClear()
{
	DWORD dwStatus = 0;

	dwStatus = GetAlarmStatus();

	if (FALSE == dwStatus)
		return TRUE;

	AxmSignalIsServoOn(m_pstMotorParam->iAxisNum, &dwStatus);

	dwStatus = AxmSignalServoAlarmReset(m_pstMotorParam->iAxisNum, ENABLE);

	Sleep(500);
	dwStatus = AxmSignalServoAlarmReset(m_pstMotorParam->iAxisNum, DISABLE);

	if (dwStatus != AXT_RT_SUCCESS)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: SetMotorPulseClear
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/28 - 10:29
// Desc.		:
//=============================================================================
BOOL CMotionControl::SetMotorPulseClear()
{
	DWORD dwStatus = 0;

	dwStatus = AxmStatusSetActPos(m_pstMotorParam->iAxisNum, 0);
	
	if (dwStatus == AXT_RT_SUCCESS)
		dwStatus = AxmStatusSetCmdPos(m_pstMotorParam->iAxisNum, 0);
	
	if (dwStatus != AXT_RT_SUCCESS)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: AlarmMessage
// Access		: public  
// Returns		: LPCTSTR
// Parameter	: DWORD dwAlarm
// Qualifier	:
// Last Update	: 2016/9/23 - 15:54
// Desc.		:
//=============================================================================
LPCTSTR CMotionControl::AlarmMessage(DWORD dwAlarm)
{
	CString str;

	switch (dwAlarm)
	{
	case AXT_RT_SUCCESS:
		str = _T("API 함수 수행 성공");
		break;

	case AXT_RT_OPEN_ERROR:
		str = _T("라이브러리 오픈 되지않음");
		break;

	case AXT_RT_OPEN_ALREADY:
		str = _T("라이브러리 오픈 되어있고 사용 중임");
		break;

	case AXT_RT_NOT_OPEN:
		str = _T("라이브러리 초기화 실패");
		break;        // 라이브러리 초기화 실패

	case AXT_RT_NOT_SUPPORT_VERSION:
		str = _T("지원하지않는 하드웨어");
		break;        // 지원하지않는 하드웨어

	case AXT_RT_LOCK_FILE_MISMATCH:
		str = _T(" Lock파일과 현재 Scan정보가 일치하지 않음 ");
		break;        // Lock파일과 현재 Scan정보가 일치하지 않음 

	case AXT_RT_INVALID_BOARD_NO:
		str = _T("유효하지 않는 보드 번호");
		break;       // 유효하지 않는 보드 번호

	case AXT_RT_INVALID_MODULE_POS:
		str = _T("유효하지 않는 모듈 위치");
		break;        // 유효하지 않는 모듈 위치

	case AXT_RT_INVALID_LEVEL:
		str = _T("유효하지 않는 레벨");
		break;      // 유효하지 않는 레벨

	case AXT_RT_INVALID_VARIABLE:
		str = _T("유효하지 않는 변수");
		break;        // 유효하지 않는 변수
	case AXT_RT_ERROR_VERSION_READ:
		str = _T("라이브러리 버전을 읽을수 없음");
		break;       // 라이브러리 버전을 읽을수 없음

	case AXT_RT_NETWORK_ERROR:
		str = _T("하드웨어 네트워크 에러");
		break;        // 하드웨어 네트워크 에러

	case AXT_RT_NETWORK_LOCK_MISMATCH:
		str = _T("보드 Lock정보와 현재 Scan정보가 일치하지 않음");
		break;       // 보드 Lock정보와 현재 Scan정보가 일치하지 않음

	case AXT_RT_1ST_BELOW_MIN_VALUE:
		str = _T("첫번째 인자값이 최소값보다 더 작음");
		break;        // 첫번째 인자값이 최소값보다 더 작음

	case AXT_RT_1ST_ABOVE_MAX_VALUE:
		str = _T("첫번째 인자값이 최대값보다 더 큼");
		break;        // 첫번째 인자값이 최대값보다 더 큼

	case AXT_RT_2ND_BELOW_MIN_VALUE:
		str = _T("두번째 인자값이 최소값보다 더 작음");
		break;        // 두번째 인자값이 최소값보다 더 작음
	case AXT_RT_2ND_ABOVE_MAX_VALUE:
		str = _T("두번째 인자값이 최대값보다 더 큼");
		break;       // 두번째 인자값이 최대값보다 더 큼

	case AXT_RT_3RD_BELOW_MIN_VALUE:
		str = _T("세번째 인자값이 최소값보다 더 작음");
		break;        // 세번째 인자값이 최소값보다 더 작음

	case AXT_RT_3RD_ABOVE_MAX_VALUE:
		str = _T("세번째 인자값이 최대값보다 더 큼");
		break;        // 세번째 인자값이 최대값보다 더 큼

	case AXT_RT_4TH_BELOW_MIN_VALUE:
		str = _T(" 네번째 인자값이 최소값보다 더 작음");
		break;        // 네번째 인자값이 최소값보다 더 작음

	case AXT_RT_4TH_ABOVE_MAX_VALUE:
		str = _T("네번째 인자값이 최대값보다 더 큼");
		break;        // 네번째 인자값이 최대값보다 더 큼

	case AXT_RT_5TH_BELOW_MIN_VALUE:
		str = _T(" 다섯번째 인자값이 최소값보다 더 작음");
		break;        // 다섯번째 인자값이 최소값보다 더 작음

	case AXT_RT_5TH_ABOVE_MAX_VALUE:
		str = _T("다섯번째 인자값이 최대값보다 더 큼");
		break;        // 다섯번째 인자값이 최대값보다 더 큼

	case AXT_RT_6TH_BELOW_MIN_VALUE:
		str = _T("여섯번째 인자값이 최소값보다 더 작음");
		break;       // 여섯번째 인자값이 최소값보다 더 작음

	case AXT_RT_6TH_ABOVE_MAX_VALUE:
		str = _T("여섯번째 인자값이 최대값보다 더 큼");
		break;        // 여섯번째 인자값이 최대값보다 더 큼

	case AXT_RT_7TH_BELOW_MIN_VALUE:
		str = _T("일곱번째 인자값이 최소값보다 더 작음");
		break;        // 일곱번째 인자값이 최소값보다 더 작음

	case AXT_RT_7TH_ABOVE_MAX_VALUE:
		str = _T("일곱번째 인자값이 최대값보다 더 큼");
		break;        // 일곱번째 인자값이 최대값보다 더 큼

	case AXT_RT_8TH_BELOW_MIN_VALUE:
		str = _T("여덟번째 인자값이 최소값보다 더 작음");
		break;       // 여덟번째 인자값이 최소값보다 더 작음

	case AXT_RT_8TH_ABOVE_MAX_VALUE:
		str = _T("여덟번째 인자값이 최대값보다 더 큼");
		break;        // 여덟번째 인자값이 최대값보다 더 큼

	case AXT_RT_9TH_BELOW_MIN_VALUE:
		str = _T("아홉번째 인자값이 최소값보다 더 작음");
		break;        // 아홉번째 인자값이 최소값보다 더 작음

	case AXT_RT_9TH_ABOVE_MAX_VALUE:
		str = _T("아홉번째 인자값이 최대값보다 더 큼");
		break;        // 아홉번째 인자값이 최대값보다 더 큼

	case AXT_RT_10TH_BELOW_MIN_VALUE:
		str = _T("열번째 인자값이 최소값보다 더 작음");
		break;       // 열번째 인자값이 최소값보다 더 작음

	case AXT_RT_10TH_ABOVE_MAX_VALUE:
		str = _T(" 열번째 인자값이 최대값보다 더 큼");
		break;        // 열번째 인자값이 최대값보다 더 큼

	case AXT_RT_AIO_OPEN_ERROR:
		str = _T(" AIO 모듈 오픈실패");
		break;       // AIO 모듈 오픈실패

	case AXT_RT_AIO_NOT_MODULE:
		str = _T("AIO 모듈 없음");
		break;        // AIO 모듈 없음

	case AXT_RT_AIO_NOT_EVENT:
		str = _T("AIO 이벤트 읽지 못함");
		break;        // AIO 이벤트 읽지 못함

	case AXT_RT_AIO_INVALID_MODULE_NO:
		str = _T("유효하지않은 AIO모듈");
		break;        // 유효하지않은 AIO모듈

	case AXT_RT_AIO_INVALID_CHANNEL_NO:
		str = _T("유효하지않은 AIO채널번호");
		break;        // 유효하지않은 AIO채널번호

	case AXT_RT_AIO_INVALID_USE:
		str = _T(" AIO 함수 사용못함");
		break;        // AIO 함수 사용못함

	case AXT_RT_AIO_INVALID_TRIGGER_MODE:
		str = _T("유효하지않는 트리거 모드");
		break;        // 유효하지않는 트리거 모드

	case AXT_RT_AIO_EXTERNAL_DATA_EMPTY:
		break;

	case AXT_RT_DIO_OPEN_ERROR:
		str = _T(" DIO 모듈 오픈실패");
		break;       // DIO 모듈 오픈실패

	case AXT_RT_DIO_NOT_MODULE:
		str = _T("DIO 모듈 없음");
		break;        // DIO 모듈 없음

	case AXT_RT_DIO_NOT_INTERRUPT:
		str = _T("DIO 인터럽트 설정안됨");
		break;        // DIO 인터럽트 설정안됨

	case AXT_RT_DIO_INVALID_MODULE_NO:
		str = _T("유효하지않는 DIO 모듈 번호");
		break;        // 유효하지않는 DIO 모듈 번호

	case AXT_RT_DIO_INVALID_OFFSET_NO:
		str = _T("유효하지않는 DIO OFFSET 번호");
		break;       // 유효하지않는 DIO OFFSET 번호

	case AXT_RT_DIO_INVALID_LEVEL:
		str = _T("유효하지않는 DIO 레벨");
		break;       // 유효하지않는 DIO 레벨

	case AXT_RT_DIO_INVALID_MODE:
		str = _T("유효하지않는 DIO 모드");
		break;        // 유효하지않는 DIO 모드

	case AXT_RT_DIO_INVALID_VALUE:
		str = _T("유효하지않는 값 설정");
		break;       // 유효하지않는 값 설정

	case AXT_RT_DIO_INVALID_USE:
		str = _T("DIO 함수 사용못함");
		break;        // DIO 함수 사용못함

	case AXT_RT_CNT_OPEN_ERROR:
		str = _T("CNT 모듈 오픈실패");
		break;        // CNT 모듈 오픈실패

	case AXT_RT_CNT_NOT_MODULE:
		str = _T(" CNT 모듈 없음");
		break;        // CNT 모듈 없음

	case AXT_RT_CNT_NOT_INTERRUPT:
		str = _T("CNT 인터럽트 설정안됨");
		break;        // CNT 인터럽트 설정안됨

	case AXT_RT_CNT_INVALID_MODULE_NO:
		str = _T("유효하지않는 CNT 모듈 번호");
		break;        // 유효하지않는 CNT 모듈 번호

	case AXT_RT_CNT_INVALID_CHANNEL_NO:
		str = _T("유효하지않는 채널 번호");
		break;        // 유효하지않는 채널 번호

	case AXT_RT_CNT_INVALID_OFFSET_NO:
		str = _T("유효하지않는 CNT OFFSET 번호");
		break;        // 유효하지않는 CNT OFFSET 번호

	case AXT_RT_CNT_INVALID_LEVEL:
		str = _T("유효하지않는 CNT 레벨");
		break;        // 유효하지않는 CNT 레벨

	case AXT_RT_CNT_INVALID_MODE:
		str = _T("유효하지않는 CNT 모드");
		break;        // 유효하지않는 CNT 모드

	case AXT_RT_CNT_INVALID_VALUE:
		str = _T("유효하지않는 값 설정");
		break;        // 유효하지않는 값 설정

	case AXT_RT_CNT_INVALID_USE:
		str = _T("CNT 함수 사용못함");
		break;       // CNT 함수 사용못함

	case AXT_RT_MOTION_OPEN_ERROR:
		str = _T("모션 라이브러리 Open 실패");
		break;        // 모션 라이브러리 Open 실패

	case AXT_RT_MOTION_NOT_MODULE:
		str = _T("시스템에 장착된 모션 모듈이 없음");
		break;        // 시스템에 장착된 모션 모듈이 없음

	case AXT_RT_MOTION_NOT_INTERRUPT:
		str = _T("인터럽트 결과 읽기 실패");
		break;        // 인터럽트 결과 읽기 실패

	case AXT_RT_MOTION_NOT_INITIAL_AXIS_NO:
		str = _T("해당 축 모션 초기화 실패");
		break;        // 해당 축 모션 초기화 실패

	case AXT_RT_MOTION_NOT_IN_CONT_INTERPOL:
		str = _T("연속 보간 구동 중이 아닌 상태에서 연속보간 중지 명령을 수행 하였음");
		break;        // 연속 보간 구동 중이 아닌 상태에서 연속보간 중지 명령을 수행 하였음

	case AXT_RT_MOTION_NOT_PARA_READ:
		str = _T("원점 구동 설정 파라미터 로드 실패");
		break;        // 원점 구동 설정 파라미터 로드 실패

	case AXT_RT_MOTION_INVALID_AXIS_NO:
		str = _T("해당 축이 존재하지 않음");
		break;        // 해당 축이 존재하지 않음

	case AXT_RT_MOTION_INVALID_METHOD:
		str = _T("해당 축 구동에 필요한 설정이 잘못됨");
		break;       // 해당 축 구동에 필요한 설정이 잘못됨

	case AXT_RT_MOTION_INVALID_USE:
		str = _T("'uUse' 인자값이 잘못 설정됨");
		break;        // 'uUse' 인자값이 잘못 설정됨

	case AXT_RT_MOTION_INVALID_LEVEL:
		str = _T("'uLevel' 인자값이 잘못 설정됨");
		break;        // 'uLevel' 인자값이 잘못 설정됨

	case AXT_RT_MOTION_INVALID_BIT_NO:
		str = _T("범용 입출력 해당 비트가 잘못 설정됨");
		break;       // 범용 입출력 해당 비트가 잘못 설정됨

	case AXT_RT_MOTION_INVALID_STOP_MODE:
		str = _T("모션 정지 모드 설정값이 잘못됨");
		break;       // 모션 정지 모드 설정값이 잘못됨

	case AXT_RT_MOTION_INVALID_TRIGGER_MODE:
		str = _T("트리거 설정 모드가 잘못 설정됨");
		break;        // 트리거 설정 모드가 잘못 설정됨

	case AXT_RT_MOTION_INVALID_TRIGGER_LEVEL:
		str = _T("트리거 출력 레벨 설정이 잘못됨");
		break;       // 트리거 출력 레벨 설정이 잘못됨

	case AXT_RT_MOTION_INVALID_SELECTION:
		str = _T("'uSelection' 인자가 COMMAND 또는 ACTUAL 이외의 값으로 설정되어 있음");
		break;       // 'uSelection' 인자가 COMMAND 또는 ACTUAL 이외의 값으로 설정되어 있음

	case AXT_RT_MOTION_INVALID_TIME:
		str = _T("Trigger 출력 시간값이 잘못 설정되어 있음");
		break;       // Trigger 출력 시간값이 잘못 설정되어 있음

	case AXT_RT_MOTION_INVALID_FILE_LOAD:
		str = _T("모션 설정값이 저장된 파일이 로드가 안됨");
		break;       // 모션 설정값이 저장된 파일이 로드가 안됨

	case AXT_RT_MOTION_INVALID_FILE_SAVE:
		str = _T("모션 설정값을 저장하는 파일 저장에 실패함");
		break;       // 모션 설정값을 저장하는 파일 저장에 실패함

	case AXT_RT_MOTION_INVALID_VELOCITY:
		str = _T("모션 구동 속도값이 0으로 설정되어 모션 에러 발생");
		break;       // 모션 구동 속도값이 0으로 설정되어 모션 에러 발생

	case AXT_RT_MOTION_INVALID_ACCELTIME:
		str = _T("모션 구동 가속 시간값이 0으로 설정되어 모션 에러 발생");
		break;        // 모션 구동 가속 시간값이 0으로 설정되어 모션 에러 발생

	case AXT_RT_MOTION_INVALID_PULSE_VALUE:
		str = _T("모션 단위 설정 시 입력 펄스값이 0보다 작은값으로 설정됨");
		break;      // 모션 단위 설정 시 입력 펄스값이 0보다 작은값으로 설정됨

	case AXT_RT_MOTION_INVALID_NODE_NUMBER:
		str = _T("위치나 속도 오버라이드 함수가 모션 정지 중에 실햄됨");
		break;       // 위치나 속도 오버라이드 함수가 모션 정지 중에 실햄됨

	case AXT_RT_MOTION_INVALID_TARGET:
		str = _T("다축 모션 정지 원인에 관한 플래그를 반환");
		break;       // 다축 모션 정지 원인에 관한 플래그를 반환한다.

	case AXT_RT_MOTION_ERROR_IN_NONMOTION:
		str = _T("모션 구동중이어야 되는데 모션 구동중이 아님");
		break;       // 모션 구동중이어야 되는데 모션 구동중이 아닐 때

	case AXT_RT_MOTION_ERROR_IN_MOTION:
		str = _T("모션 구동 중에 다른 모션 구동 함수를 실행함");
		break;      // 모션 구동 중에 다른 모션 구동 함수를 실행함

	case AXT_RT_MOTION_ERROR:
		str = _T("다축 구동 정지 함수 실행 중 에러 발생함");
		break;       // 다축 구동 정지 함수 실행 중 에러 발생함

	case AXT_RT_MOTION_ERROR_GANTRY_ENABLE:
		str = _T("겐트리 enable이 되어있음");
		break;      // 겐트리 enable이 되어있을 때

	case AXT_RT_MOTION_ERROR_GANTRY_AXIS:
		str = _T("겐트리 축이 마스터채널(축) 번호(0 ~ (최대축수 - 1))가 잘못 들어감");
		break;       // 겐트리 축이 마스터채널(축) 번호(0 ~ (최대축수 - 1))가 잘못 들어갔을 때

	case AXT_RT_MOTION_ERROR_MASTER_SERVOON:
		str = _T("마스터 축 서보온이 안되어있음");
		break;       // 마스터 축 서보온이 안되어있을 때

	case AXT_RT_MOTION_ERROR_SLAVE_SERVOON:
		str = _T(" 슬레이브 축 서보온이 안되어있음");
		break;       // 슬레이브 축 서보온이 안되어있을 때

	case AXT_RT_MOTION_INVALID_POSITION:
		str = _T("유효한 위치에 없음");
		break;       // 유효한 위치에 없을 때

	case AXT_RT_ERROR_NOT_SAME_MODULE:
		str = _T("똑같은 모듈내에 있지 않음");
		break;       // 똑 같은 모듈내에 있지 않을경우

	case AXT_RT_ERROR_NOT_SAME_BOARD:
		str = _T(" 똑같은 보드내에 있지않음");
		break;       // 똑 같은 보드내에 있지 아닐경우

	case AXT_RT_ERROR_NOT_SAME_PRODUCT:
		str = _T("제품이 서로 다름");
		break;      // 제품이 서로 다를경우

	case AXT_RT_NOT_CAPTURED:
		str = _T("위치가 저장되지 않음");
		break;      // 위치가 저장되지 않을 때

	case AXT_RT_ERROR_NOT_SAME_IC:
		str = _T("같은 칩내에 존재하지않음");
		break;      // 같은 칩내에 존재하지않을 때

	case AXT_RT_ERROR_NOT_GEARMODE:
		str = _T("기어모드로 변환이 안됨");
		break;       // 기어모드로 변환이 안될 때

	case AXT_ERROR_CONTI_INVALID_AXIS_NO:
		str = _T("연속보간 축맵핑 시 유효한 축이 아님");
		break;      // 연속보간 축맵핑 시 유효한 축이 아닐 때

	case AXT_ERROR_CONTI_INVALID_MAP_NO:
		str = _T("연속보간 맵핑 시 유효한 맵핑 번호가 아님");
		break;      // 연속보간 맵핑 시 유효한 맵핑 번호가 아닐 때

	case AXT_ERROR_CONTI_EMPTY_MAP_NO:
		str = _T("연속보간 맵핑 번호가 비어있음");
		break;      // 연속보간 맵핑 번호가 비워 있을 때

	case AXT_RT_MOTION_ERROR_CACULATION:
		str = _T("계산상의 오차가 발생");
		break;       // 계산상의 오차가 발생했을 때

	case AXT_RT_ERROR_MOVE_SENSOR_CHECK:
		str = _T("연속보간 구동전 에러센서가(Alarm, EMG, Limit등) 감지됨");
		break;       // 연속보간 구동전 에러센서가(Alarm, EMG, Limit등) 감지된경우

	case AXT_ERROR_HELICAL_INVALID_AXIS_NO:
		str = _T("헬리컬 축 맵핑 시 유효한 축이 아님");
		break;      // 헬리컬 축 맵핑 시 유효한 축이 아닐 때

	case AXT_ERROR_HELICAL_INVALID_MAP_NO:
		str = _T(" 헬리컬 맵핑 시 유효한 맵핑 번호가 아님");
		break;     // 헬리컬 맵핑 시 유효한 맵핑 번호가 아닐 때

	case AXT_ERROR_HELICAL_EMPTY_MAP_NO:
		str = _T("헬리컬 멥핑 번호가 비어있음");
		break;      // 헬리컬 멥핑 번호가 비워 있을 때


	case AXT_ERROR_SPLINE_INVALID_AXIS_NO:
		str = _T("스플라인 축 맵핑 시 유효한 축이 아님");
		break;        // 스플라인 축 맵핑 시 유효한 축이 아닐 때

	case AXT_ERROR_SPLINE_INVALID_MAP_NO:
		str = _T("스플라인 맵핑 시 유효한 맵핑 번호가 아님");
		break;       // 스플라인 맵핑 시 유효한 맵핑 번호가 아닐 때

	case AXT_ERROR_SPLINE_EMPTY_MAP_NO:
		str = _T("스플라인 맵핑 번호가 비어있음");
		break;       // 스플라인 맵핑 번호가 비워있을 때

	case AXT_ERROR_SPLINE_NUM_ERROR:
		str = _T("스플라인 점숫자가 부적당함");
		break;       // 스플라인 점숫자가 부적당할 때

	case AXT_RT_MOTION_INTERPOL_VALUE:
		str = _T("보간할 때 입력 값이 잘못넣어짐");
		break;      // 보간할 때 입력 값이 잘못넣어졌을 때

	case AXT_RT_ERROR_NOT_CONTIBEGIN:
		str = _T("연속보간 할 때 CONTIBEGIN함수를 호출하지 않음");
		break;       // 연속보간 할 때 CONTIBEGIN함수를 호출하지 않을 때

	case AXT_RT_ERROR_NOT_CONTIEND:
		str = _T("연속보간 할 때 CONTIEND함수를 호출하지 않음");
		break;      // 연속보간 할 때 CONTIEND함수를 호출하지 않을 때

	case AXT_RT_MOTION_HOME_SEARCHING:
		str = _T("홈을 찾고 있는 중에 다른 모션 함수들을 사용");
		break;       // 홈을 찾고 있는 중일 때 다른 모션 함수들을 사용할 때

	case AXT_RT_MOTION_HOME_ERROR_SEARCHING:
		str = _T("홈을 찾고 있는 중에 외부에서 사용자나 혹은 어떤것에 의한 강제정지");
		break;        // 홈을 찾고 있는 중일 때 외부에서 사용자나 혹은 어떤것에 의한  강제로 정지당할 때

	case AXT_RT_MOTION_HOME_ERROR_START:
		str = _T("초기화 문제로 홈시작 불가");
		break;        // 초기화 문제로 홈시작 불가할 때

	case AXT_RT_MOTION_HOME_ERROR_GANTRY:
		str = _T("홈을 찾고 있는 중일 때 겐트리 enable 불가");
		break;        // 홈을 찾고 있는 중일 때 겐트리 enable 불가할 때

	case AXT_RT_MOTION_READ_ALARM_WAITING:
		str = _T(" 서보팩으로부터 알람코드 결과를 기다리는 중임");
		break;       // 서보팩으로부터 알람코드 결과를 기다리는 중

	case AXT_RT_MOTION_READ_ALARM_NO_REQUEST:
		str = _T("서보팩에 알람코드 반환 명령이 내려지지않음");
		break;       // 서보팩에 알람코드 반환 명령이 내려지지않았을 때

	case AXT_RT_MOTION_READ_ALARM_TIMEOUT:
		str = _T("서보팩 알람읽기 시간초과(1sec이상)");
		break;      // 서보팩 알람읽기 시간초과 했을때(1sec이상)

	case AXT_RT_MOTION_READ_ALARM_FAILED:
		str = _T("서보팩 알람읽기에 실패");
		break;       // 서보팩 알람읽기에 실패 했을 때

	case AXT_RT_MOTION_READ_ALARM_UNKNOWN:
		str = _T("알람코드가 알수없는 코드임");
		break;       // 알람코드가 알수없는 코드일 때

	case AXT_RT_MOTION_READ_ALARM_FILES:
		str = _T("알람정보 파일이 정해진위치에 존재하지 않음");
		break;      // 알람정보 파일이 정해진위치에 존재하지 않을 때

	case AXT_RT_MOTION_POSITION_OUTOFBOUND:
		str = _T("설정한 위치값이 설정 최대값보다 크거나 최소값보다 작은값임");
		break;        // 설정한 위치값이 설정 최대값보다 크거나 최소값보다 작은값임

	case AXT_RT_MOTION_PROFILE_INVALID:
		str = _T("구동 속도 프로파일 설정이 잘못됨");
		break;        // 구동 속도 프로파일 설정이 잘못됨

	case AXT_RT_MOTION_VELOCITY_OUTOFBOUND:
		str = _T("구동 속도값이 최대값보다 크게 설정됨");
		break;        // 구동 속도값이 최대값보다 크게 설정됨

	case AXT_RT_MOTION_MOVE_UNIT_IS_ZERO:
		str = _T("구동 단위값이 0으로 설정됨");
		break;       // 구동 단위값이 0으로 설정됨

	case AXT_RT_MOTION_SETTING_ERROR:
		str = _T("속도, 가속도, 저크, 프로파일 설정이 잘못됨");
		break;       // 속도, 가속도, 저크, 프로파일 설정이 잘못됨

	case AXT_RT_MOTION_IN_CONT_INTERPOL:
		str = _T("연속 보간 구동 중 구동 시작 또는 재시작 함수를 실행하였음");
		break;      // 연속 보간 구동 중 구동 시작 또는 재시작 함수를 실행하였음

	case AXT_RT_MOTION_DISABLE_TRIGGER:
		str = _T("트리거 출력이 Disable 상태임");
		break;       // 트리거 출력이 Disable 상태임

	case AXT_RT_MOTION_INVALID_CONT_INDEX:
		str = _T("연속 보간 Index값 설정이 잘못됨");
		break;      // 연속 보간 Index값 설정이 잘못됨

	case AXT_RT_MOTION_CONT_QUEUE_FULL:
		str = _T("모션 칩의 연속 보간 큐가 Full 상태임");
		break;       // 모션 칩의 연속 보간 큐가 Full 상태임

	case AXT_RT_PROTECTED_DURING_SERVOON:
		str = _T("서보 온 되어 있는 상태에서 사용 못 함");
		break;      // 서보 온 되어 있는 상태에서 사용 못 함

	case AXT_RT_HW_ACCESS_ERROR:
		str = _T("메모리 Read / Write 실패");
		break;       // 메모리 Read / Write 실패

	case AXT_RT_SEQ_NOT_IN_SERVICE:
		str = _T(" 순차 구동 함수 실행 중 자원 할당 실패");
		break;        // 순차 구동 함수 실행 중 자원 할당 실패

	case AXT_ERROR_SEQ_INVALID_MAP_NO:
		str = _T("순차 구동 함수 실행 중 맵핑 번호 이상");
		break;       // 순차 구동 함수 실행 중 맵핑 번호 이상.

	case AXT_ERROR_INVALID_AXIS_NO:
		str = _T("함수 설정 인자중 축번호 이상");
		break;       // 함수 설정 인자중 축번호 이상.

	case AXT_RT_ERROR_NOT_SEQ_NODE_BEGIN:
		str = _T("순차 구동 노드 입력 시작 함수를 호출하지 않음");
		break;       // 순차 구동 노드 입력 시작 함수를 호출하지 않음.

	case AXT_RT_ERROR_NOT_SEQ_NODE_END:
		str = _T("순차 구동 노드 입력 종료 함수를 호출하지 않음");
		break;       // 순차 구동 노드 입력 종료 함수를 호출하지 않음.

	case AXT_RT_ERROR_NO_NODE:
		str = _T("순차 구동 노드 입력이 없음");
		break;       // 순차 구동 노드 입력이 없음.

	case AXT_RT_ERROR_SEQ_STOP_TIMEOUT:
		break;
	}

	m_strResultMeg = str;

	return str;
}


