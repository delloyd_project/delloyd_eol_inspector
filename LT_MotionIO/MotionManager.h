﻿//*****************************************************************************
// Filename	: 	MotionManager
// Created	:	2016/9/23 - 15:54
// Modified	:	2016/9/23 - 15:54
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#pragma once
#ifndef MotionManager_h__
#define MotionManager_h__

#include "MotionControl.h"
#include "File_Motor.h"

#include <TimeAPI.h>

class CMotionManager
{
public:
	CMotionManager();
	~CMotionManager();

	CFile_Motor				m_File_Motor;
	CMotionControl*			m_pMotionCtr;
	ST_AllMotorData			m_AllMotorData;
	CString					m_szMotorFile;

	void DoEvents				();
	void DoEvents				(DWORD dwMiliSeconds);

	// 모션 데이터 가져가기
	ST_AllMotorData*	GetPtrAllMotorData()
	{
		if (NULL == m_pMotionCtr)
			return NULL;

		return &m_AllMotorData;
	}

	// 모션 Param Path
	void SetPrtMotorPath		(CString *pszMotorpath);

	// 모션 Param Path
	void SetPrtMotorPath		(CString *pszMotorpath, LPCTSTR szRecipe);

	// 모션 데이터 불러오기
	BOOL LoadMotionInfo		();

	// 모션 데이터 저장
	BOOL SaveMotionInfo		();

	// 전체 모터 오픈
	BOOL SetAllMotorOpen		();

	// 전체 모터 셋팅 제거
	BOOL SetAllMotorClose		();

	// 전체 모터 원점 정지
	BOOL SetAllOriginStop		();

	// 전체 모터 알람 제거
	BOOL SetAllAmpCtr			(BOOL bOnOff);

	// 전체 모터 알람 제거
	BOOL SetAllAlarmClear		();

	// 모터 천천히 정지
	BOOL SetAllMotorSStop		();
	
	// 모터 급 정지
	BOOL SetAllMotorEStop		();

	// 전체 모터 셋팅
	BOOL SetAllMotionSetting	();

	// 모터 원점 정지
	BOOL SetOriginStop			(UINT nAxis);

	// 모터 알람 제거
	BOOL SetAmpCtr				(UINT nAxis, BOOL bOnOff);

	// 모터 알람 제거
	BOOL SetAlarmClear			(UINT nAxis);

	// 천천히 정지
	BOOL SetMotorSStop			(UINT nAxis);
	
	// 모터 펄스 Clear
	BOOL SetMotorPosClear		(UINT nAxis);

	// 급 정지
	BOOL SetMotorEStop			(UINT nAxis);

	// 모터 원점
	BOOL SetMotorOrigin			(UINT nAxis);

	// 전체 모터 셋팅
	void SetMotionSetting		(UINT nAxis);

	// 모션 보드 상태
	BOOL GetBoardOpen			();
	
	// 전체 원점 상태 가져오기
	BOOL GetAllOriginStatus		();

	// 원점 강제 정지 상태 가저오기
	BOOL GetOriginStopStatus	(UINT nAxis);

	// 전체 AMP 상태 가져오기
	BOOL GetAmpStatus			(UINT nAxis);

	// 원점 상태
	BOOL SetOriginReset			(UINT nAxis);

	// 원점 상태 가저오기
	BOOL GetOriginStatus		(UINT nAxis);

	// 모션 움직임 상태 가저오기
	BOOL GetMotionStatus		(UINT nAxis);

	// + 리미트 센서 상태 가져오기
	BOOL GetPosSensorStatus		(UINT nAxis);

	// - 리미트 센서 상태 가져오기
	BOOL GetNegSensorStatus		(UINT nAxis);

	// 홈 센서 상태 가져오기
	BOOL GetHomeSensorStatus	(UINT nAxis);

	// 알람 센서 상태 가져오기
	BOOL GetAlarmSenorStatus	(UINT nAxis);

	// 모터 축 넘버
	UINT GetAxisNumber			(UINT nAxis);
	
	// 모터 축 사용유무
	BOOL GetAxisUseStatus		(UINT nAxis);

	// 모터 펄스 값 가져오기
	double GetCurrentPos		(UINT nAxis);

	// + LIMIT 셋팅 가져오기
	int GetPosSensorLevel		(UINT nAxis);

	// - LIMIT 셋팅 가져오기
	int GetNegSensorLevel		(UINT nAxis);

	// Home 셋팅 가져오기
	int GetHomeSensorLevel		(UINT nAxis);

	// Alram 셋팅 가져오기
	int GetAlramSensorLevel		(UINT nAxis);

	// 에러 MSG 가져오기
	CString GetResultMessage	(UINT nAxis);

	// 모터 조그 이동, nDir 0 -> - 방향, 1 -> + 방향
	BOOL MotorJogMove			(UINT nAxis, UINT nDir, UINT nJogSpeed);

	// 멀티 속도&가감속 직접 입력된 값으로 제어,  nAbsRelMode (0->Abs, 1->Rel), lAxisSize (이동 축수), pAxisParam (이동 정보)
	BOOL MotorMultiMove			(UINT nAbsRelMode, long lAxisSize, PST_AxisMoveParam pAxisParam);

	// 모터 이동
	BOOL MotorAxisMove			(UINT nAbsRelMode, UINT nAxis, double dbPos);

	// 모터 속도&가감속 직접 입력된 값으로 제어,  nAbsRelMode (0->Abs, 1->Rel), dbPos (이동 펄스), dbVel(속도), dbAcc(가속도)
	BOOL MotorAxisDitailMove	(UINT nAbsRelMode, UINT nAxis, double dbPos, double dbVel, double dbAcc);

};

#endif // MotionManager_h__