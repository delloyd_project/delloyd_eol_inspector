﻿//*****************************************************************************
// Filename	: 	PCBCamBrd.h
// Created	:	2016/03/09
// Modified	:	2016/05/09
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef PCBCamBrd_h__
#define PCBCamBrd_h__

#pragma once
#include "SerialCom_Base.h"
#include "Define_PCBCamBrd.h"

using namespace LGIT_CamBrd;

//=============================================================================
//
//=============================================================================
class CPCBCamBrd : public CSerialCom_Base
{
public:
	CPCBCamBrd();
	virtual ~CPCBCamBrd();

	//---------------------------------------------------------
	// 재정의
	//---------------------------------------------------------
protected:
	//virtual void	OnEvent					(EEvent eEvent, EError eError);
	virtual LRESULT	OnFilterRecvData		(const char* szACK, DWORD dwAckSize);
	virtual void	OnRecvProtocol			(const char* szACK, DWORD dwAckSize);

	//-----------------------------------------------------
	// 통신 모듈
	//-----------------------------------------------------
	ST_CamBrdProtocol		m_stProtocol;
	ST_CamBrdRecvProtocol	m_stRecvProtocol;	

	void			ResetProtocol();

public:

	ST_CamBrdRecvProtocol	GetAckProtocol()
	{
		return m_stRecvProtocol;
	};

	// 보드
	LRESULT			Send_BoardCheck			(__out BYTE& byBrdNo);

	// 전압
	LRESULT			Send_Volt				(__in float* fVolt);

	LRESULT			Send_OpenShort(bool & bOutOpenShort);

	// 전류
	LRESULT			Send_GetCurrent			(__out double* iOutCurrent);

	// 사운드
	LRESULT			Send_GetSound			(__out int& iOutVoltage);

	// IR LED
	LRESULT			Send_IRLED				(__in BOOL bOnOff);

};

#endif // PCBCamBrd_h__
