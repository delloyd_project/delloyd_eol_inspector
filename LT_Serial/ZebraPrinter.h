﻿//*****************************************************************************
// Filename	: 	ZebraPrinter.h
// Created	:	2016/03/09
// Modified	:	2016/05/09
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef ZebraPrinter_h__
#define ZebraPrinter_h__

#pragma once
#include "SerialCom_Base.h"
#include "Define_ZebraPrinter.h"

using namespace ZebraPrinter;

//=============================================================================
//
//=============================================================================
class CZebraPrinter : public CSerialCom_Base
{
public:
	CZebraPrinter();
	virtual ~CZebraPrinter();

	//---------------------------------------------------------
	// 재정의
	//---------------------------------------------------------
protected:
	//virtual void	OnEvent					(EEvent eEvent, EError eError);
	virtual LRESULT	OnFilterRecvData		(const char* szACK, DWORD dwAckSize);
	//virtual void	OnRecvProtocol			(const char* szACK, DWORD dwAckSize);

	//-----------------------------------------------------
	// 통신 모듈
	//-----------------------------------------------------
	ST_ZebraPrinterFont		m_stFontData[ZebraPrinter_LineLength];
	ST_ZebraPrinterProtocol	m_stProtocol;

	void ResetProtocol();

public:

	void SettingToFontData(__in int iLineIdx, __in ST_ZebraPrinterFont stFontData)
	{
		m_stFontData[iLineIdx] = stFontData;
	};

	// 프린터 출력
	LRESULT	SendToLabelOutput(__in int iOutCount, __in CString szPrnPath);

};

#endif // ZebraPrinter_h__
