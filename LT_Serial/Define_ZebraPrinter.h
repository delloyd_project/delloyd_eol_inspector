﻿//*****************************************************************************
// Filename	: 	Define_ZebraPrinter.h
// Created	:	2016/3/8 - 13:31
// Modified	:	2016/3/8 - 13:31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Define_ZebraPrinter_h__
#define Define_ZebraPrinter_h__

#include <windows.h>

//=============================================================================
// Zebra 프린터 통신 프로토콜
//=============================================================================

namespace ZebraPrinter
{

#define ZebraPrinter_ProtoLength	4096
#define ZebraPrinter_FontLength		256
#define ZebraPrinter_LineLength		6

	typedef struct _tag_ZebraPrinterFont
	{
		CStringA szFont;

		_tag_ZebraPrinterFont()
		{

		};

		void Reset()
		{
			szFont.Empty();
		};

		void MakePrintingToFont(__in UINT nQRMode, __in UINT nCountMode, __in int iposX, __in int iposY, __in int iWidthSz, __in int iHeightSz, __in CStringA szText, __in int iLotCount = 0)
		{
			if (nQRMode)
			{
				szFont.Format("^FO%d,%d^BQN,2,%d^FDMM,A%s%04d^FS", iposX, iposY, iWidthSz, szText, iLotCount);
			} 
			else
			{
				if (nCountMode)
				{
					szFont.Format("^FO%d,%d^AON%d,%d^FD%s%04d^FS", iposX, iposY, iWidthSz, iHeightSz, szText, iLotCount);
				}
				else
				{
					szFont.Format("^FO%d,%d^AON%d,%d^FD%s^FS", iposX, iposY, iWidthSz, iHeightSz, szText);
				}
			}
		};

	}ST_ZebraPrinterFont, *PST_ZebraPrinterFont;


	typedef struct _tag_ZebraPrinterProtocol
	{
		char Data[ZebraPrinter_ProtoLength];

		_tag_ZebraPrinterProtocol()
		{
			memset(Data, 0, ZebraPrinter_ProtoLength);
		};

		CStringA PrnFileRead(__in CString szPrnPath)
		{
			CStringA szData;
			CStdioFile stdFile;

			CString szTemp;
			CString szReadData;
			if (stdFile.Open(szPrnPath, CFile::modeRead))
			{
				while (stdFile.ReadString(szTemp))
				{
					szReadData += szTemp + _T("\r\n");
				};

				stdFile.Close();
			}

			szData = (CStringA)szReadData;

			return szData;
		};

		void MakeProtocol_Printing(__in int iPrintCnt, __in CString szPrnPath, __in ST_ZebraPrinterFont* pFont)
		{
			CStringA szHead;
			CStringA szTail;
			CStringA szMakePrinting;

			// prn background to input from file path 
			szMakePrinting = PrnFileRead(szPrnPath);

			int iFind = szMakePrinting.Find("^PQ");
			if (iFind == -1)
				return;

			int iSize = szMakePrinting.GetLength();
			szHead = szMakePrinting.Left(iFind);
			szTail = szMakePrinting.Mid(iFind, iSize - iFind);

			// font
			CStringA szBuf;
			for (int i = 0; i < ZebraPrinter_LineLength; i++)
			{
				szBuf.Format("%s", pFont[i].szFont);
				szHead += szBuf;
			}

			// print Count
			szBuf.Format("^PQ%d,0,1,Y", iPrintCnt);
			szHead += szBuf;

			// Tail
			szHead += "^XZ";

// 			CFile stdFile;
// 			if (stdFile.Open(_T("D:\\writeDebug.txt"), CFile::modeCreate | CFile::modeWrite))
// 			{
// 				for (int i = 0; i < szMakePrinting.GetLength(); i++)
// 				{
// 					stdFile.Write((CString)szMakePrinting.GetBuffer(i), i);
// 				}
// 
// 				stdFile.Close();
// 			}

			memcpy(&Data[0], szHead.GetBuffer(0), ZebraPrinter_ProtoLength);
		}

	}ST_ZebraPrinterProtocol, *PST_ZebraPrinterProtocol;
};

#endif // Define_ZebraPrinter_h__
