﻿//*****************************************************************************
// Filename	: 	DAQWrapper.h
// Created	:	2016/2/11 - 15:07
// Modified	:	2016/2/11 - 15:07
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef DAQWrapper_h__
#define DAQWrapper_h__

#pragma once

#include "Def_DAQWrapper.h"
#include "DAQ_LVDS.h"

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
class CDAQWrapper
{
public:
	CDAQWrapper();
	~CDAQWrapper();
	
protected:

	int				m_nBoardCount	= 0;
	CDAQ_LVDS		m_DAQ_LVDS[MAX_DAQ_CHANNEL];

public:

	//-------------------------------------------------------------------------
	// DAQ 보드 제어용 함수들
	//-------------------------------------------------------------------------
	BOOL		Open_Device			(__out int& iBoardCount);
	BOOL		Reset_Board			(__in UINT nBoard);
	BOOL		Close_Device		();
	UINT		GetBoardCount		();

	// I2C Functions
	BOOL		I2C_Reset			(__in UINT nBoard);
	BOOL		I2C_SetClock		(__in UINT nBoard, __in int nClock);
	BOOL		I2C_Read			(__in UINT nBoard, __in BYTE bySlaveID, __in DWORD dwAddrLen, __in DWORD dwAddress, __in BYTE nByteSize, __out LPBYTE lpbyData);// Max 256 Byte
	BOOL		I2C_Write			(__in UINT nBoard, __in BYTE bySlaveID, __in DWORD dwAddrLen, __in DWORD dwAddress, __in WORD nByteSize, __in LPBYTE lpbyData);// Max 1024 Byte

	// LVDS Camera Link Functions
	BOOL		LVDS_Init			(__in UINT nBoard);
	BOOL		LVDS_Close			(__in UINT nBoard);
	BOOL		LVDS_Start			(__in UINT nBoard);
	BOOL		LVDS_Stop			(__in UINT nBoard);
	BOOL		LVDS_StartAll		();
	BOOL		LVDS_StopAll		();

	BOOL		LVDS_GetFrame		(__in UINT nBoard, __out DWORD* pdwCnt, __out unsigned char* pchBuf);
	BOOL		LVDS_GetResolution	(__in UINT nBoard, __out DWORD* pdwXRes, __out DWORD* pdwYRes);
	
	BOOL		LVDS_SetDataMode	(__in UINT nBoard, __in enDAQDataMode nMode);
	BOOL		LVDS_GetVersion		(__in UINT nBoard, __out int* pnFpgaVer, __out int* pnFirmVer);
	BOOL		LVDS_BufferFlush	(__in UINT nBoard);


	//	LVDS Set Hsync Polarity	(2014. 03. 21)
	BOOL		LVDS_HsyncPol		(__in UINT nBoard, __in enDAQHsyncPolarity bPol);
	//	LVDS Set Pclk Polarity  (2015. 01. 07 add)
	BOOL		LVDS_PclkPol		(__in UINT nBoard, __in enDAQPclkPolarity bPol);
	//	LVDS Set DVAL use		(2014. 03. 21)
	BOOL		LVDS_SetDeUse		(__in UINT nBoard, __in enDAQDvalUse bUse);
	BOOL		LVDS_VideoMode		(__in UINT nBoard, __in enDAQVideoMode nMode);
	BOOL		LVDS_GetFrameRate	(__in UINT nBoard, __out DWORD* pdwRate);
	BOOL		LVDS_SetDivide		(__in UINT nBoard, __in DWORD dwWidth, __in DWORD dwHeight, __in DWORD dwX0, __in DWORD dwX1, __in DWORD dwX2, __in DWORD dwY0, __in DWORD dwY1, __in DWORD dwY2);
	BOOL		LVDS_GetDivide		(__in UINT nBoard, __out DWORD* pdwWidth, __out DWORD* pdwHeight, __out DWORD* pdwX0, __out DWORD* pdwX1, __out DWORD* pdwX2, __out DWORD* pdwY0, __out DWORD *pdwY1, __out DWORD* pdwY2);

	// Clock Functions
	BOOL		CLK_Init			(__in UINT nBoard);
	BOOL		CLK_Close			(__in UINT nBoard);
	// nSelect == 0 : fixed clock
	// nSelect == others : Program clock
	BOOL		CLK_Select			(__in UINT nBoard, __in int nSelect);
	DWORD		CLK_Get				(__in UINT nBoard);
	//   1039Hz to 68Mhz
	BOOL		CLK_Set				(__in UINT nBoard, __in DWORD val);
	BOOL		CLK_Off				(__in UINT nBoard, __in BOOL bOff);		// bOff TRUE : clock off, FALSE : clock on


	//-------------------------------------------------------------------------
	// 기본 설정 함수들
	//-------------------------------------------------------------------------
	HWND		m_hWndOwner;				 // 오너 윈도우 핸들
	
	// 오너 윈도우 핸들 설정
	void		SetOwner (__in HWND hOwner)
	{
		for (UINT nIdx = 0; nIdx < MAX_DAQ_CHANNEL; nIdx++)
		{
			m_DAQ_LVDS[nIdx].SetOwner(hOwner);
		}
	};

	// 카메라 영상 상태 변경시 처리 메세지
	void		SetWM_ChgStatus (__in UINT nWinMsg)
	{
		for (UINT nIdx = 0; nIdx < MAX_DAQ_CHANNEL; nIdx++)
		{
			m_DAQ_LVDS[nIdx].SetWM_ChgStatus(nWinMsg);
		}
	};

	// 비데오 영상 수신 메세지
	void		SetWM_RecvVideo (__in UINT nWinMsg)
	{
		for (UINT nIdx = 0; nIdx < MAX_DAQ_CHANNEL; nIdx++)
		{
			m_DAQ_LVDS[nIdx].SetWM_RecvVideo(nWinMsg);
		}
	};
	
	// 카메라 영상 상태 구하기
	BOOL		GetSignalStatus (__in UINT nBoard)
	{
		if (nBoard < MAX_DAQ_CHANNEL)
			return m_DAQ_LVDS[nBoard].GetSignalStatus();
		else
			return FALSE;
	};

	// 수신된 영상 데이터 구조체의 포인터 구하기 (영상 검사 용도)
	ST_VideoRGB*	GetRecvVideoRGB (__in UINT nBoard)
	{
		if (nBoard < MAX_DAQ_CHANNEL)
			return m_DAQ_LVDS[nBoard].GetRecvVideoRGB();
		else
			return nullptr;
	};

	// 수신된 영상 데이터의 BYTE 포인터 구하기 (영상 검사 용도)
	LPBYTE		GetRecvRGBData (__in UINT nBoard)
	{
		if (nBoard < MAX_DAQ_CHANNEL)
			return m_DAQ_LVDS[nBoard].GetRecvRGBData();
		else
			return nullptr;
	};

	// 수신된 원본 영상 데이터의 구조체 포인터 구하기
	ST_FrameData*	GetSourceFrameData_ST	(__in UINT nBoard)
	{
		if (nBoard < MAX_DAQ_CHANNEL)
			return m_DAQ_LVDS[nBoard].GetSourceFrameData_ST();
		else
			return nullptr;
	};

	// 수신된 원본 영상 데이터의 BYTE 포인터 구하기 (영상 검사 용도)
	LPBYTE		GetSourceFrameData(__in UINT nBoard)
	{
		if (nBoard < MAX_DAQ_CHANNEL)
			return m_DAQ_LVDS[nBoard].GetSourceFrameData();
		else
			return nullptr;
	};


	// 소스 영상 포맷 설정
	void		SetColorModel(__in UINT nBoard, __in enColorModel nSrcColorModel, __in enColorModelOut nOutColorModel = CMOUT_RGBA)
	{
		if (nBoard < MAX_DAQ_CHANNEL)
			m_DAQ_LVDS[nBoard].SetColorModel(nSrcColorModel, nOutColorModel);
	};

	void		SetColorModel_All(__in enColorModel nSrcColorModel, __in enColorModelOut nOutColorModel = CMOUT_RGBA)
	{
		for (UINT nBoard = 0; nBoard < MAX_DAQ_CHANNEL; nBoard++)
		{
			m_DAQ_LVDS[nBoard].SetColorModel(nSrcColorModel, nOutColorModel);
		}
	};
	
	// 센서 종류 설정

	// 영상 캡쳐 시작
	void		Capture_Start			(__in UINT nBoard);
	void		Capture_Start_All		();
	void		Capture_Stop			(__in UINT nBoard);
	void		Capture_Stop_All		();
	BOOL		Cam_Restart				(__in UINT nBoard);

	// 프레임 레이트 설정
	void		SetFrameRate			(__in UINT nBoard, __in DOUBLE dFrameRate);
	void		SetFrameRate_All		(__in DOUBLE dFrameRate);

	// 설정
	void		SetDAQOption			(__in UINT nBoard, __in ST_DAQOption stOption);
	void		SetDAQOption_All		(__in ST_DAQOption stOption);
	BOOL		GetDAQOption			(__in UINT nBoard, __out ST_DAQOption& stOption);

	// 영상 처리 함수 사용여부
	void		SetUse_Flip				(__in UINT nBoard, __in enFlipType nFlip);
	void		SetUse_Flip_All			(__in enFlipType nFlip);
	void		SetUse_Rotate			(__in UINT nBoard, __in enRotateType nRotate);
	void		SetUse_Rotate_All		(__in enRotateType nRotate);

	// LVDS 초기화 여부
	BOOL		Is_LVDS_Init			(__in UINT nBoard);

	// LVDS 영상 캡쳐 시작 여부
	BOOL		Is_LVDS_Start			(__in UINT nBoard);

	// I2C 처리 기능
	BOOL		SetI2CFilePath			(__in UINT nBoard, __in LPCTSTR szFilePath);
	void		SetI2CFilePath_All		(__in LPCTSTR szFilePath);

	// GetFrame으로 읽은 프레임 데이터 저장
	BOOL		SaveRawImage			(__in UINT nBoard, __in LPCTSTR szFilePath);
};

#endif // DAQWrapper_h__

