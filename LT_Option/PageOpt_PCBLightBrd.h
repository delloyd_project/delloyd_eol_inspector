﻿//*****************************************************************************
// Filename	: 	PageOpt_PCBLightBrd.h
// Created	:	2015/12/16 - 15:23
// Modified	:	2015/12/16 - 15:23
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef PageOpt_PCBLightBrd_h__
#define PageOpt_PCBLightBrd_h__

#pragma once
#include "PageOption.h"
#include "Define_Option.h"

using namespace Luritech_Option;

class CPageOpt_PCBLightBrd : public CPageOption
{
	DECLARE_DYNAMIC(CPageOpt_PCBLightBrd)

public:
	CPageOpt_PCBLightBrd(LPCTSTR lpszCaption = NULL);
	CPageOpt_PCBLightBrd(UINT nIDTemplate, UINT nIDCaption = 0);
	virtual ~CPageOpt_PCBLightBrd(void);

protected:

	DECLARE_MESSAGE_MAP()

	virtual void		AdjustLayout();
	virtual void		SetPropListFont();
	virtual void		InitPropList();

	stOpt_PCB	m_stOption;

	stOpt_PCB	GetOption();
	void		SetOption(stOpt_PCB stOption);

public:

	virtual void		SaveOption();
	virtual void		LoadOption();
};

#endif // PageOpt_PCBLightBrd_h__
