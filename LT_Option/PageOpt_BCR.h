﻿//*****************************************************************************
// Filename	: PageOpt_BCR.h
// Created	: 2015/12/22
// Modified	: 2015/12/22
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef PageOpt_BCR_h__
#define PageOpt_BCR_h__

#pragma once
#include "PageOption.h"
#include "Define_Option.h"

using namespace Luritech_Option;

//=============================================================================
//
//=============================================================================
class CPageOpt_BCR : public CPageOption
{
	DECLARE_DYNAMIC(CPageOpt_BCR)

public:
	CPageOpt_BCR						(void);
	CPageOpt_BCR						(UINT nIDTemplate, UINT nIDCaption = 0);
	virtual ~CPageOpt_BCR				(void);

protected:

	DECLARE_MESSAGE_MAP()

	virtual void		AdjustLayout   	();
	virtual void		SetPropListFont	();
	virtual void		InitPropList   	();

	stOpt_BCR			m_stOption;

	stOpt_BCR			GetOption	   	();
	void				SetOption	   	(stOpt_BCR stOption);

public:

	virtual void		SaveOption	   	();
	virtual void		LoadOption	   	();
};

#endif // PageOpt_BCR_h__
