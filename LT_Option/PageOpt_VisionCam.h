//*****************************************************************************
// Filename	: PageOpt_VisionCam.h
// Created	: 2010/9/16
// Modified	: 2010/9/16 - 18:05
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef PageOpt_VisionCam_h__
#define PageOpt_VisionCam_h__

#pragma once
#include "PageOption.h"
#include "Define_Option.h"

using namespace Luritech_Option;

//=============================================================================
//
//=============================================================================
class CPageOpt_VisionCam : public CPageOption
{
	DECLARE_DYNAMIC(CPageOpt_VisionCam)

public:
	CPageOpt_VisionCam						(void);
	CPageOpt_VisionCam						(UINT nIDTemplate, UINT nIDCaption = 0);
	virtual ~CPageOpt_VisionCam				(void);

protected:

	DECLARE_MESSAGE_MAP()

	virtual void			AdjustLayout		();
	virtual void			SetPropListFont		();
	virtual void			InitPropList		();

	stOpt_VisionCam			m_stOption;

	stOpt_VisionCam			GetOption			();
	void					SetOption			(stOpt_VisionCam stOption);

public:

	virtual void			SaveOption			();
	virtual void			LoadOption			();
};

#endif // PageOpt_VisionCam_h__
