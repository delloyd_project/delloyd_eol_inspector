﻿//*****************************************************************************
// Filename	: Define_Option.h
// Created	: 2015/12/16
// Modified	: 2016/05/18
//
// Author	: PiRing
//	
// Purpose	: 옵션 관련 설정값 선언 파일
//*****************************************************************************
#ifndef Define_Option_h__
#define Define_Option_h__

#pragma pack (push,1)

#include "Define_OptionItem.h"
#include "Def_Enum_Cm.h"
#include <afxwin.h>




namespace Luritech_Option
{
	//---------------------------------
	// Option Base
	//---------------------------------
	typedef struct _stOption_Base
	{
		_stOption_Base()
		{
			Reset();
		};

		virtual void Reset()
		{
		};

		_stOption_Base& operator= (_stOption_Base& ref)
		{
			return *this;
		};
	}stOption_Base;

	//---------------------------------
	// Serial 통신
	//---------------------------------
	typedef struct _stOption_Serial : public stOption_Base
	{
		BYTE	Port;
		DWORD	BaudRate;	/* Baudrate at which running       */
		BYTE	ByteSize;	/* Number of bits/byte, 4-8        */
		BYTE	Parity;		/* 0-4=None,Odd,Even,Mark,Space    */
		BYTE	StopBits;	/* 0,1,2 = 1, 1.5, 2               */

		_stOption_Serial ()
		{
			Reset ();
		};

		void Reset ()
		{
			Port		= 1;
			BaudRate	= CBR_115200;
			ByteSize	= 8;
			Parity		= NOPARITY;
			StopBits	= ONESTOPBIT;
		};

		_stOption_Serial& operator= (_stOption_Serial& ref)
		{
			Port		= ref.Port;
			BaudRate	= ref.BaudRate;
			ByteSize	= ref.ByteSize;
			Parity		= ref.Parity;
			StopBits	= ref.StopBits;

			return *this;
		};
	}stOpt_Serial;

	//---------------------------------
	// 이더넷 통신
	//---------------------------------
	typedef struct _stOpt_Ethernet : public stOption_Base
	{
		DWORD	dwAddress;
		DWORD	dwPort;

		_stOpt_Ethernet ()
		{
			Reset ();
		};

		void Reset ()
		{
			dwAddress	= 0;
			dwPort		= 0;
		};

		_stOpt_Ethernet& operator= (_stOpt_Ethernet& ref)
		{
			dwAddress	= ref.dwAddress;
			dwPort		= ref.dwPort;

			return *this;
		};
	}stOpt_Ethernet;
	
	//---------------------------------
	// 기타
	//---------------------------------
	typedef struct _stOpt_Misc : public stOption_Base
	{
		_stOpt_Misc ()
		{
			Reset ();
		};

		void Reset ()
		{
			
		};

		_stOpt_Misc& operator= (_stOpt_Misc& ref)
		{
			return *this;
		};
	}stOpt_Misc;


	//---------------------------------
	// Barcode Reader & Motor
	//---------------------------------	
	typedef struct _stOpt_BCR : public stOption_Base
	{	
		stOpt_Serial	ComPort;
		stOpt_Serial	ComPort_Label;
		stOpt_Serial	ComPort_Motor[MAX_INDICATOR_CNT];

		_stOpt_BCR()
		{
			Reset ();
		};

		void Reset ()
		{
			ComPort.Reset();
			ComPort_Label.Reset();
			for (UINT nIdx = 0; nIdx < MAX_INDICATOR_CNT; nIdx++)
			{
				ComPort_Motor[nIdx].Reset();
			}
		};

		_stOpt_BCR& operator= (_stOpt_BCR& ref)
		{
			ComPort = ref.ComPort;
			ComPort_Label = ref.ComPort_Label;
			for (UINT nIdx = 0; nIdx < MAX_INDICATOR_CNT; nIdx++)
			{
				ComPort_Motor[nIdx] = ref.ComPort_Motor[nIdx];
			}

			return *this;
		};
	}stOpt_BCR;

	//---------------------------------
	// 광원 보드
	//---------------------------------
	typedef struct _stOpt_PCB : public stOption_Base
	{
		stOpt_Serial	ComPort_CamBrd[MAX_PCBCAM_CNT];
		stOpt_Serial	ComPort_LightBrd[MAX_PCB_LIGHT_CNT];
		

		_stOpt_PCB()
		{
			Reset();
		};

		void Reset()
		{
			for (UINT nIdx = 0; nIdx < MAX_PCBCAM_CNT; nIdx++)
				ComPort_CamBrd[nIdx].Reset();

			for (UINT nIdx = 0; nIdx < MAX_PCB_LIGHT_CNT; nIdx++)
				ComPort_LightBrd[nIdx].Reset();

			
		};

		_stOpt_PCB& operator= (_stOpt_PCB& ref)
		{
			for (UINT nIdx = 0; nIdx < MAX_PCBCAM_CNT; nIdx++)
				ComPort_CamBrd[nIdx] = ref.ComPort_CamBrd[nIdx];

			for (UINT nIdx = 0; nIdx < MAX_PCB_LIGHT_CNT; nIdx++)
				ComPort_LightBrd[nIdx] = ref.ComPort_LightBrd[nIdx];

			

			return *this;
		};
	}stOpt_PCB;


	//---------------------------------
	// 변위 센서
	//---------------------------------
	typedef struct _stOpt_SerialDevice
	{
		stOpt_Serial	LaserSensor;
		stOpt_Serial	VisionLightBrd;

		stOpt_Serial	Screw;

		_stOpt_SerialDevice()
		{
			Reset();
		};

		void Reset()
		{
			LaserSensor.Reset();
			Screw.Reset();
			VisionLightBrd.Reset();

		};

		_stOpt_SerialDevice& operator= (_stOpt_SerialDevice& ref)
		{
			LaserSensor = ref.LaserSensor;
			Screw = ref.Screw;
			VisionLightBrd = ref.VisionLightBrd;


			return *this;
		};
	}stOpt_SerialDevice;


	//---------------------------------
	// Vision Camera
	//---------------------------------	
	typedef struct _stOpt_VisionCam
	{
		stOpt_Ethernet	Address_Cam;

		CString			szPath_VImage;

		_stOpt_VisionCam()
		{
			Address_Cam.Reset();
			szPath_VImage = _T("C:\\VImage\\");
		};

		void Reset()
		{
			Address_Cam.Reset();
			szPath_VImage.Empty();
		};

		_stOpt_VisionCam& operator= (_stOpt_VisionCam& ref)
		{
			Address_Cam = ref.Address_Cam;
			szPath_VImage = ref.szPath_VImage;

			return *this;
		};
	}stOpt_VisionCam;


	//---------------------------------
	// 검사기 설정
	//---------------------------------
	typedef struct _stOption_Inspector : public stOption_Base
	{
		// 저장 경로
		CString		szPath_Log;
		CString		szPath_Report;
		CString		szPath_Model;
		CString		szPath_Motor;
		CString		szPath_Pogo;
		CString		szPath_Image;
		CString		szPath_BinFile;
		CString		szPath_I2CFile;
		CString		szPath_WavFile;
		CString		szPath_BarcodeFile;
		CString		szPath_OperatorFile;
		CString		szPath_PrnFile;

		CString		szPath_Sensor;		// 센서 롬 경로
		CString		szPath_Power;		// 파워 롬 경로		
		BOOL		bUseAlarm;			// 알람 설정
		DWORD		dwAlarmDuration;	// 알람 지속 시간
		
		// 표준 바코드 구분자 입력
		CString		szBarcodeRef;
		
		// 자동 재 실행 사용 여부
		BOOL		UseAutoRestart;
		// 모델 설정 화면 잠금 기능
		BOOL		UseDeviceInfoPane;
		// 통신 자동 연결
		BOOL		UseAutoConnection;
		// 마스터 사용 여부
		BOOL		bUseMasterCheck;
		// 바코드 사용 여부
		BOOL		bUseBarcode;

		// 불량함 사용 여부
		BOOL		bUseFailbox;
		// 프린터 사용 여부
		BOOL		bUsePrinter;
		// Logo Type
		UINT		nLogoType;
		// Door Open 사용 여부
		BOOL		bUseDoorOpen_Err;
		// Area Sensor 사용 여부
		BOOL		bUseAreaSen_Err;
		// 인터락 사용
		BOOL		bUseInterlock;	
		// ChartMonitor index
		UINT		nChartMonitorIndex;
		// Start Master Set Time
		DWORD		dwStartMasterSetTime;	// 분 단위

		UINT		nImageSaveType;

		//*** 기타 ***//
		CString		Password_Admin;
		CString		EqpCode;
		//CString		Password_ReadOnly;

		_stOption_Inspector ()
		{

			szBarcodeRef		= _T("ref:");
			szPath_Log			= _T("D:\\system\\Log");
			szPath_Report		= _T("D:\\system\\Report");
			szPath_Model		= _T("D:\\system\\Model");
			szPath_Pogo			= _T("D:\\system\\Pogo");
			szPath_Motor		= _T("D:\\system\\Motor");
			szPath_Image		= _T("D:\\system\\Image");
			szPath_BinFile		= _T("D:\\system\\Bin");
			szPath_I2CFile		= _T("D:\\system\\I2C");
			szPath_WavFile		= _T("D:\\system\\Wav");
			szPath_BarcodeFile	= _T("D:\\system\\Barcode");
			szPath_OperatorFile = _T("D:\\system\\Operator");
			szPath_PrnFile		= _T("D:\\system\\Printer");

			szPath_Sensor		= _T("D:\\system\\Sensor\\");
			szPath_Power		= _T("D:\\system\\Power\\");
			bUseAlarm			= FALSE;
			dwAlarmDuration		= 3000;

			UseDeviceInfoPane	= TRUE;
			UseAutoConnection	= FALSE;
			UseAutoRestart		= FALSE;
			bUseDoorOpen_Err	= FALSE;
			bUseAreaSen_Err		= FALSE;
			bUseMasterCheck = FALSE;
			bUseBarcode = FALSE;
			bUseFailbox = FALSE;
			bUsePrinter = FALSE;
			nLogoType = 0;
			nChartMonitorIndex = 1;

			bUseInterlock		= TRUE;
			dwStartMasterSetTime= 60;

			nImageSaveType = ImageSaveType_NG;
		};

		void Reset ()
		{
			szBarcodeRef.Empty();
			szPath_Log.Empty();
			szPath_Report.Empty();
			szPath_Model.Empty();
			szPath_Pogo.Empty();
			szPath_Motor.Empty();
			szPath_Image.Empty();
			szPath_BinFile.Empty();
			szPath_I2CFile.Empty();
			szPath_WavFile.Empty();
			szPath_BarcodeFile.Empty();
			szPath_OperatorFile.Empty();
			szPath_PrnFile.Empty();

			szPath_Sensor.Empty();
			szPath_Power.Empty();
			bUseAlarm			= FALSE;
			dwAlarmDuration		= 3000;

			UseDeviceInfoPane	= TRUE;
			UseAutoConnection	= FALSE;
			UseAutoRestart		= FALSE;
			bUseDoorOpen_Err	= FALSE;
			bUseAreaSen_Err		= FALSE;
			bUseBarcode = FALSE;
			bUseFailbox = FALSE;
			bUseMasterCheck = FALSE;
			bUsePrinter = FALSE;
			nLogoType = 0;

			bUseInterlock		= TRUE;
			dwStartMasterSetTime = 60;
			nChartMonitorIndex = 1;

			Password_Admin.Empty();
			EqpCode.Empty();
			nImageSaveType = ImageSaveType_NG;
			//Password_ReadOnly.Empty();
		};

		_stOption_Inspector& operator= (_stOption_Inspector& ref)
		{
			szBarcodeRef			= ref.szBarcodeRef;
			szPath_Log				= ref.szPath_Log;
			szPath_Report			= ref.szPath_Report;
			szPath_Model			= ref.szPath_Model;
			szPath_Pogo				= ref.szPath_Pogo;
			szPath_Motor			= ref.szPath_Motor;
			szPath_Image			= ref.szPath_Image;
			szPath_BinFile			= ref.szPath_BinFile;
			szPath_I2CFile			= ref.szPath_I2CFile;
			szPath_WavFile			= ref.szPath_WavFile;
			szPath_BarcodeFile		= ref.szPath_BarcodeFile;
			szPath_OperatorFile		= ref.szPath_OperatorFile;
			szPath_PrnFile			= ref.szPath_PrnFile;

			szPath_Sensor			= ref.szPath_Sensor;
			szPath_Power			= ref.szPath_Power;
			bUseAlarm				= ref.bUseAlarm;
			dwAlarmDuration			= ref.dwAlarmDuration;

			UseAutoRestart			= ref.UseAutoRestart;
			UseDeviceInfoPane		= ref.UseDeviceInfoPane;
			UseAutoConnection		= ref.UseAutoConnection;
			bUseDoorOpen_Err		= ref.bUseDoorOpen_Err;
			bUseAreaSen_Err			= ref.bUseAreaSen_Err;
			bUseBarcode				= ref.bUseBarcode;
			bUseFailbox = ref.bUseFailbox;
			bUsePrinter = ref.bUsePrinter;
			nLogoType				= ref.nLogoType;
			bUseMasterCheck = ref.bUseMasterCheck;

			bUseInterlock			= ref.bUseInterlock;
			dwStartMasterSetTime	= ref.dwStartMasterSetTime;
			nChartMonitorIndex		= ref.nChartMonitorIndex;

			Password_Admin			= ref.Password_Admin;
			EqpCode					= ref.EqpCode;
			nImageSaveType			= ref.nImageSaveType;
			//Password_ReadOnly		= ref.Password_ReadOnly;

			return *this;
		};
	}stOpt_Insp;

	//-----------------------------------------------------
	// 통합 검사기용
	//-----------------------------------------------------
	typedef struct _stLT_Option
	{
		stOpt_Insp			Inspector;
		stOpt_BCR			BCR;
		stOpt_PCB			PCB;
		stOpt_Misc			Misc;
		stOpt_SerialDevice	SerialDevice;
		stOpt_VisionCam		VisionCam;

		_stLT_Option ()
		{
			Reset ();
		};

		void Reset ()
		{
			Inspector.Reset();
			BCR.Reset();
			PCB.Reset();
			Misc.Reset();
			SerialDevice.Reset();
			VisionCam.Reset();
		};

		_stLT_Option& operator= (_stLT_Option& ref)
		{
			Inspector		= ref.Inspector;
			BCR				= ref.BCR;
			PCB				= ref.PCB;
			Misc			= ref.Misc;
			SerialDevice	= ref.SerialDevice;
			VisionCam		= ref.VisionCam;

			return *this;
		}
	}stLT_Option;

	
	typedef enum
	{
		OPT_INSPECTOR		= 0,
		OPT_BCR,
		OPT_PCB,
		//OPT_LIGHT,
		OPT_DISPLACE,
		OPT_VISION,
		OPT_VISIONLIGHT,
		OPT_TYPE_MAX,
	}enumOptionCategory;

	static LPCTSTR lpszOptionCategory [] = 
	{
		_T("System"),
		_T("Serial Port"),
		_T("PCB"),
		//_T("Light"),
		_T("Displacement Sensor"),
		_T("Vision Camera"),
		_T("Vision Light"),
		NULL
	};
}

#pragma pack (pop)

#endif // Define_Option_h__

