﻿//*****************************************************************************
// Filename	: 	PageOpt_PCBLightBrd.cpp
// Created	:	2015/12/16 - 15:23
// Modified	:	2015/12/16 - 15:23
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "StdAfx.h"
#include "PageOpt_PCBLightBrd.h"
#include "Define_OptionItem.h"
#include "Define_OptionDescription.h"

#include <memory>
#include "CustomProperties.h"

IMPLEMENT_DYNAMIC(CPageOpt_PCBLightBrd, CPageOption)

//=============================================================================
//
//=============================================================================
CPageOpt_PCBLightBrd::CPageOpt_PCBLightBrd(LPCTSTR lpszCaption /*= NULL*/) : CPageOption(lpszCaption)
{
}

CPageOpt_PCBLightBrd::CPageOpt_PCBLightBrd(UINT nIDTemplate, UINT nIDCaption /*= 0*/) : CPageOption(nIDTemplate, nIDCaption)
{

}

//=============================================================================
//
//=============================================================================
CPageOpt_PCBLightBrd::~CPageOpt_PCBLightBrd(void)
{
}

BEGIN_MESSAGE_MAP(CPageOpt_PCBLightBrd, CPageOption)
END_MESSAGE_MAP()

// CPageOpt_PCBLightBrd 메시지 처리기입니다.

//=============================================================================
// Method		: CPageOpt_PCBLightBrd::AdjustLayout
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_PCBLightBrd::AdjustLayout()
{
	CPageOption::AdjustLayout();
}

//=============================================================================
// Method		: CPageOpt_PCBLightBrd::SetPropListFont
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_PCBLightBrd::SetPropListFont()
{
	CPageOption::SetPropListFont();
}

//=============================================================================
// Method		: CPageOpt_PCBLightBrd::InitPropList
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_PCBLightBrd::InitPropList()
{
	CPageOption::InitPropList();

	//--------------------------------------------------------
	// 통신 포트 설정
	//--------------------------------------------------------
	CString szGroupName;
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].nLightBrdCount; nIdx++)
	{
		szGroupName.Format(_T("Light Board %d : COM Port"), nIdx + 1);
		std::auto_ptr<CMFCPropertyGridProperty> apGroup_ComPort_1(new CMFCPropertyGridProperty(szGroupName));
		InitPropList_Comport(apGroup_ComPort_1);
		m_wndPropList.AddProperty(apGroup_ComPort_1.release());
	}
}

//=============================================================================
// Method		: CPageOpt_PCBLightBrd::SaveOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_PCBLightBrd::SaveOption()
{
	CPageOption::SaveOption();

	m_stOption = GetOption();

	m_pLT_Option->SaveOption_Light(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_PCBLightBrd::LoadOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_PCBLightBrd::LoadOption()
{
	CPageOption::LoadOption();

	if (m_pLT_Option->LoadOption_Light(m_stOption))
		SetOption(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_PCBLightBrd::GetOption
// Access		: protected 
// Returns		: Luritech_Option::stOpt_100KPower
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
Luritech_Option::stOpt_PCB CPageOpt_PCBLightBrd::GetOption()
{
	UINT nGroupIndex = 0;
	UINT nSubItemIndex = 0;
	UINT nIndex = 0;

	//---------------------------------------------------------------
	// 그룹 1 
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = NULL;
	int iSubItemCount = 0;
	
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].nLightBrdCount; nIdx++)
	{
		pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
		iSubItemCount = pPropertyGroup->GetSubItemsCount();
		nSubItemIndex = 0;

		GetOption_ComPort(pPropertyGroup, m_stOption.ComPort_LightBrd[nIdx]);
	}

	return m_stOption;
}

//=============================================================================
// Method		: CPageOpt_PCBLightBrd::SetOption
// Access		: protected 
// Returns		: void
// Parameter	: stOpt_100KPower stOption
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_PCBLightBrd::SetOption(stOpt_PCB stOption)
{
 	UINT nGroupIndex = 0;
	UINT nSubItemIndex	= 0;

 	//---------------------------------------------------------------
 	// 그룹 1
 	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = NULL;
	int iSubItemCount = 0;

	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].nLightBrdCount; nIdx++)
	{
		pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
		iSubItemCount = pPropertyGroup->GetSubItemsCount();
		nSubItemIndex = 0;

		SetOption_ComPort(pPropertyGroup, m_stOption.ComPort_LightBrd[nIdx]);
	}
}