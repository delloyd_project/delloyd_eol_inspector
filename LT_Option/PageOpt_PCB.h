﻿//*****************************************************************************
// Filename	: 	PageOpt_PCB.h
// Created	:	2015/12/16 - 15:23
// Modified	:	2015/12/16 - 15:23
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef PageOpt_PCB_h__
#define PageOpt_PCB_h__

#pragma once
#include "PageOption.h"
#include "Define_Option.h"

using namespace Luritech_Option;

class CPageOpt_PCB : public CPageOption
{
	DECLARE_DYNAMIC(CPageOpt_PCB)

public:
	CPageOpt_PCB(LPCTSTR lpszCaption = NULL);
	CPageOpt_PCB(UINT nIDTemplate, UINT nIDCaption = 0);
	virtual ~CPageOpt_PCB(void);

protected:

	DECLARE_MESSAGE_MAP()

	virtual void		AdjustLayout();
	virtual void		SetPropListFont();
	virtual void		InitPropList();

	stOpt_PCB			m_stOption;

	stOpt_PCB			GetOption();
	void				SetOption(stOpt_PCB stOption);

public:

	virtual void		SaveOption();
	virtual void		LoadOption();
};

#endif // PageOpt_PCB_h__
