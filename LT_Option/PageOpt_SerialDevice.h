//*****************************************************************************
// Filename	: 	PageOpt_SerialDevice.h
// Created	:	2015/12/16 - 15:22
// Modified	:	2015/12/16 - 15:22
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef PageOpt_SerialDevice_h__
#define PageOpt_SerialDevice_h__

#pragma once
#include "PageOption.h"
#include "Define_Option.h"

using namespace Luritech_Option;

class CPageOpt_SerialDevice : public CPageOption
{
	DECLARE_DYNAMIC(CPageOpt_SerialDevice)

public:
	CPageOpt_SerialDevice(LPCTSTR lpszCaption = NULL);
	CPageOpt_SerialDevice(UINT nIDTemplate, UINT nIDCaption = 0);
	virtual ~CPageOpt_SerialDevice(void);

protected:

	DECLARE_MESSAGE_MAP()

	virtual void		AdjustLayout();
	virtual void		SetPropListFont();
	virtual void		InitPropList();

	stOpt_SerialDevice	m_stOption;

	stOpt_SerialDevice	GetOption();
	void				SetOption(stOpt_SerialDevice stOption);

public:

	virtual void		SaveOption();
	virtual void		LoadOption();
};

#endif // PageOpt_SerialDevice_h__

