﻿//*****************************************************************************
// Filename	: 	MES_SEKONIX.h
// Created	:	2016/5/10 - 15:31
// Modified	:	2016/5/10 - 15:31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef MES_SEKONIX_h__
#define MES_SEKONIX_h__

#pragma once

#include "BSocketClient.h"
#include "Define_MES_SEKONIX.h"
#include "SerQueue.h"

#define MAX_MES_SEKONIX_BUFFER	1024

class CMES_SEKONIX : public CBSocketClient
{
public:
	CMES_SEKONIX();
	virtual ~CMES_SEKONIX();

protected:
	virtual BOOL	PasingRecvAck			(const char* lpBuffer, DWORD dwReaded);

	CSerQueue			m_SerQueue;
	ST_SEKONIX_MES_Protocol	m_stProtocol;
	ST_SEKONIX_MES_Protocol	m_stRecvProtocol;

	char			m_szACKBuf[MAX_MES_SEKONIX_BUFFER];
	DWORD_PTR		m_dwACKBufSize;

public:

	void		Test(){};
	BOOL		GetLotID		(CString& szLotID, CString& szOvlCount, DWORD dwTimeOut = 5000);
	CStringA	GetRecvProtocol()
	{
		return m_stRecvProtocol.szProtocol;
	};

	void ResetAck();

};

#endif // MES_SEKONIX_h__

