﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem.cpp
// Created	:	2017/1/3 - 10:39
// Modified	:	2017/1/3 - 10:39
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Wnd_Cfg_TestItem.h"


IMPLEMENT_DYNAMIC(CWnd_Cfg_TestItem, CWnd_BaseView)

CWnd_Cfg_TestItem::CWnd_Cfg_TestItem()
{
	m_pstDevice = NULL;
	m_pstModelInfo = NULL;

	VERIFY(m_font_Data.CreateFont(
		24,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_TestItem::~CWnd_Cfg_TestItem()
{
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_TestItem, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CWnd_Cfg_TestItem message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
int CWnd_Cfg_TestItem::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	UINT nID = 1000;

	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D_SCROLLED, rectDummy, this, nID++, CMFCTabCtrl::LOCATION_TOP);

 	m_wnd_CurrentOp.SetOwner(GetOwner());
	m_wnd_CurrentOp.Create(NULL, g_szLT_TestItem_Name[TIID_Current], dwStyle, rectDummy, &m_tc_Option, nID++);

//	m_wnd_OperModeOp.SetOwner(GetOwner());
//	m_wnd_OperModeOp.Create(NULL, g_szLT_TestItem_Name[TIID_OperationMode], dwStyle, rectDummy, &m_tc_Option, nID++);

	m_wnd_CenterPointOp.SetOwner(GetOwner());
	m_wnd_CenterPointOp.Create(NULL, g_szLT_TestItem_Name[TIID_CenterPoint], dwStyle, rectDummy, &m_tc_Option, nID++);

	m_wnd_RotateOp.SetOwner(GetOwner());
	m_wnd_RotateOp.Create(NULL, g_szLT_TestItem_Name[TIID_Rotation], dwStyle, rectDummy, &m_tc_Option, nID++);

	m_Wnd_ColorOp.SetOwner(GetOwner());
	m_Wnd_ColorOp.Create(NULL, g_szLT_TestItem_Name[TIID_Color], dwStyle, rectDummy, &m_tc_Option, nID++);

	m_wnd_ReverseOp.SetOwner(GetOwner());
	m_wnd_ReverseOp.Create(NULL, g_szLT_TestItem_Name[TIID_Reverse], dwStyle, rectDummy, &m_tc_Option, nID++);

	m_wnd_EIAJOp.SetOwner(GetOwner());
	m_wnd_EIAJOp.Create(NULL, g_szLT_TestItem_Name[TIID_EIAJ], dwStyle, rectDummy, &m_tc_Option, nID++);

//	m_wnd_SFROp.SetOwner(GetOwner());
//	m_wnd_SFROp.Create(NULL, g_szLT_TestItem_Name[TIID_SFR], dwStyle, rectDummy, &m_tc_Option, nID++);

	m_wnd_AngleOp.SetOwner(GetOwner());
	m_wnd_AngleOp.Create(NULL, g_szLT_TestItem_Name[TIID_FOV], dwStyle, rectDummy, &m_tc_Option, nID++);

	m_wnd_ParticleMAOp.SetOwner(GetOwner());
	m_wnd_ParticleMAOp.Create(NULL, g_szLT_TestItem_Name[TIID_ParticleManual], dwStyle, rectDummy, &m_tc_Option, nID++);

	m_wnd_BlackSpotOp.SetOwner(GetOwner());
	m_wnd_BlackSpotOp.Create(NULL, g_szLT_TestItem_Name[TIID_BlackSpot], dwStyle, rectDummy, &m_tc_Option, nID++);

	m_wnd_DefectPixelOp.SetOwner(GetOwner());
	m_wnd_DefectPixelOp.Create(NULL, g_szLT_TestItem_Name[TIID_DefectPixel], dwStyle, rectDummy, &m_tc_Option, nID++);

//	m_wnd_LEDTestOp.SetOwner(GetOwner());
//	m_wnd_LEDTestOp.Create(NULL, g_szLT_TestItem_Name[TIID_LEDTest], dwStyle, rectDummy, &m_tc_Option, nID++);

// 	m_wnd_VideoSignalOp.SetOwner(GetOwner());
// 	m_wnd_VideoSignalOp.Create(NULL, g_szLT_TestItem_Name[TIID_VideoSignal], dwStyle, rectDummy, &m_tc_Option, nID++);
// 
// 	m_wnd_ResetOp.SetOwner(GetOwner());
// 	m_wnd_ResetOp.Create(NULL, g_szLT_TestItem_Name[TIID_Reset], dwStyle, rectDummy, &m_tc_Option, nID++);

//	m_wnd_PatternNoiseOp.SetOwner(GetOwner());
//	m_wnd_PatternNoiseOp.Create(NULL, g_szLT_TestItem_Name[TIID_PatternNoise], dwStyle, rectDummy, &m_tc_Option, nID++);

	m_wnd_BrightnessOp.SetOwner(GetOwner());
	m_wnd_BrightnessOp.Create(NULL, g_szLT_TestItem_Name[TIID_Brightness], dwStyle, rectDummy, &m_tc_Option, nID++);

	m_wnd_IRFilterOp.SetOwner(GetOwner());
	m_wnd_IRFilterOp.Create(NULL, g_szLT_TestItem_Name[TIID_IRFilter], dwStyle, rectDummy, &m_tc_Option, nID++);

// 	m_wnd_IRFilterManualOp.SetOwner(GetOwner());
// 	m_wnd_IRFilterManualOp.Create(NULL, g_szLT_TestItem_Name[TIID_IRFilterManual], dwStyle, rectDummy, &m_tc_Option, 170);

// 	m_wnd_BlackSpotOp.SetOwner(GetOwner());
// 	m_wnd_BlackSpotOp.Create(NULL, g_szLT_TestItem_Name[TIID_Particle], dwStyle, rectDummy, &m_tc_Option, 140);

//  m_wnd_SFROp.SetOwner(GetOwner());
// 	m_wnd_SFROp.Create(NULL, g_szLT_TestItem_Name[TIID_SFR], dwStyle, rectDummy, &m_tc_Option, 160);

// 	m_wnd_FFTOp.SetOwner(GetOwner());
// 	m_wnd_FFTOp.Create(NULL, g_szLT_TestItem_Name[TIID_FFT], dwStyle, rectDummy, &m_tc_Option, 180);

	UINT nItemCnt = 0;

	m_tc_Option.AddTab(&m_wnd_CurrentOp,		g_szLT_TestItem_Name[TIID_Current],			nItemCnt++, FALSE);
//	m_tc_Option.AddTab(&m_wnd_OperModeOp,		g_szLT_TestItem_Name[TIID_OperationMode],	nItemCnt++, FALSE);
// 	m_tc_Option.AddTab(&m_wnd_VideoSignalOp,	g_szLT_TestItem_Name[TIID_VideoSignal],		nItemCnt++, FALSE);
// 	m_tc_Option.AddTab(&m_wnd_ResetOp,			g_szLT_TestItem_Name[TIID_Reset],			nItemCnt++, FALSE);
  	m_tc_Option.AddTab(&m_wnd_CenterPointOp,	g_szLT_TestItem_Name[TIID_CenterPoint],		nItemCnt++, FALSE);
 	m_tc_Option.AddTab(&m_wnd_RotateOp,			g_szLT_TestItem_Name[TIID_Rotation],		nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_Wnd_ColorOp,			g_szLT_TestItem_Name[TIID_Color],			nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_wnd_ReverseOp,		g_szLT_TestItem_Name[TIID_Reverse],			nItemCnt++, FALSE);
 //	m_tc_Option.AddTab(&m_wnd_SFROp,			g_szLT_TestItem_Name[TIID_SFR],				nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_wnd_EIAJOp,			g_szLT_TestItem_Name[TIID_EIAJ],			nItemCnt++, FALSE);
 	m_tc_Option.AddTab(&m_wnd_AngleOp,			g_szLT_TestItem_Name[TIID_FOV],				nItemCnt++, FALSE);

 	m_tc_Option.AddTab(&m_wnd_ParticleMAOp,		g_szLT_TestItem_Name[TIID_ParticleManual],	nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_wnd_BlackSpotOp,		g_szLT_TestItem_Name[TIID_BlackSpot], nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_wnd_DefectPixelOp,	g_szLT_TestItem_Name[TIID_DefectPixel], nItemCnt++, FALSE);
//	m_tc_Option.AddTab(&m_wnd_LEDTestOp,		g_szLT_TestItem_Name[TIID_LEDTest],			nItemCnt++, FALSE);
 	m_tc_Option.AddTab(&m_wnd_BrightnessOp,		g_szLT_TestItem_Name[TIID_Brightness],		nItemCnt++, FALSE);
//	m_tc_Option.AddTab(&m_wnd_PatternNoiseOp,	g_szLT_TestItem_Name[TIID_PatternNoise],	nItemCnt++, FALSE);
	m_tc_Option.AddTab(&m_wnd_IRFilterOp,		g_szLT_TestItem_Name[TIID_IRFilter],		nItemCnt++, FALSE);

	//m_tc_Option.AddTab(&m_wnd_BlackSpotOp, g_szLT_TestItem_Name[TIID_Particle], nItemCnt++, FALSE);
//	m_tc_Option.AddTab(&m_wnd_EIAJOp,			g_szLT_TestItem_Name[TIID_EIAJ],		nItemCnt++, FALSE);
// 	m_tc_Option.AddTab(&m_wnd_IRFilterManualOp,	g_szLT_TestItem_Name[TIID_IRFilterManual],	nItemCnt++, FALSE);
// 	m_tc_Option.AddTab(&m_wnd_FFTOp,			g_szLT_TestItem_Name[TIID_FFT],			nItemCnt++, FALSE);

	m_tc_Option.EnableTabSwap(FALSE);
 	m_tc_Option.SetActiveTab(0);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin = 5;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	m_tc_Option.MoveWindow(iLeft, iTop, iWidth, iHeight);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_TestItem::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_TestItem::PreTranslateMessage(MSG* pMsg)
{
	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2018/1/2 - 19:10
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (bShow)
	{
		m_tc_Option.SetActiveTab(0);
		GetOwner()->SendNotifyMessage(WM_TAB_CHANGE_PIC, (WPARAM)PIC_Current, 0);
	}
	else
	{
		GetOwner()->SendNotifyMessage(WM_TAB_CHANGE_PIC, (WPARAM)PIC_Standby, 0);
	}
}

//=============================================================================
// Method		: IsTestAll
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/7/13 - 17:41
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_TestItem::IsTestAll()
{
	return FALSE;
}

//=============================================================================
// Method		: SetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: ST_ModelInfo * pstModelInfo
// Qualifier	:
// Last Update	: 2017/7/13 - 17:41
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::SetModelInfo(ST_ModelInfo* pstModelInfo)
{
	if (pstModelInfo == NULL)
		return;

	m_pstModelInfo = pstModelInfo;

 	m_wnd_CurrentOp.SetPtr_ModelInfo(m_pstModelInfo);
 	m_wnd_CurrentOp.SetUpdateData();

	//m_wnd_OperModeOp.SetPtr_ModelInfo(m_pstModelInfo);
	//m_wnd_OperModeOp.SetUpdateData();

	m_wnd_CenterPointOp.SetPtr_ModelInfo(m_pstModelInfo);
	m_wnd_CenterPointOp.SetUpdateData();

	m_wnd_RotateOp.SetPtr_ModelInfo(m_pstModelInfo);
	m_wnd_RotateOp.SetUpdateData();

 	m_wnd_EIAJOp.SetPtr_ModelInfo(m_pstModelInfo);
 	m_wnd_EIAJOp.SetUpdateData();

	//m_wnd_SFROp.SetPtr_ModelInfo(m_pstModelInfo);
	//m_wnd_SFROp.SetUpdateData();

	m_wnd_AngleOp.SetPtr_ModelInfo(m_pstModelInfo);
	m_wnd_AngleOp.SetUpdateData();

	m_Wnd_ColorOp.SetPtr_ModelInfo(m_pstModelInfo);
	m_Wnd_ColorOp.SetUpdateData();

	m_wnd_ReverseOp.SetPtr_ModelInfo(m_pstModelInfo);
	m_wnd_ReverseOp.SetUpdateData();

	//m_wnd_LEDTestOp.SetPtr_ModelInfo(m_pstModelInfo);
	//m_wnd_LEDTestOp.SetUpdateData();
 
	m_wnd_ParticleMAOp.SetPtr_ModelInfo(m_pstModelInfo);
	m_wnd_ParticleMAOp.SetUpdateData();

	//m_wnd_PatternNoiseOp.SetPtr_ModelInfo(m_pstModelInfo);
	//m_wnd_PatternNoiseOp.SetUpdateData();

	m_wnd_BrightnessOp.SetPtr_ModelInfo(m_pstModelInfo);
	m_wnd_BrightnessOp.SetUpdateData();

	m_wnd_IRFilterOp.SetPtr_ModelInfo(m_pstModelInfo);
	m_wnd_IRFilterOp.SetUpdateData();

// 	m_wnd_VideoSignalOp.SetPtr_ModelInfo(m_pstModelInfo);
// 	m_wnd_VideoSignalOp.SetUpdateData();
// 
// 	m_wnd_ResetOp.SetPtr_ModelInfo(m_pstModelInfo);
// 	m_wnd_ResetOp.SetUpdateData();

// 	m_wnd_IRFilterManualOp.SetPtr_ModelInfo(m_pstModelInfo);
// 	m_wnd_IRFilterManualOp.SetUpdateData();
// 
 	m_wnd_BlackSpotOp.SetPtr_ModelInfo(m_pstModelInfo);
 	m_wnd_BlackSpotOp.SetUpdateData();

	m_wnd_DefectPixelOp.SetPtr_ModelInfo(m_pstModelInfo);
	m_wnd_DefectPixelOp.SetUpdateData();
// 
// 	m_wnd_FFTOp.SetPtr_ModelInfo(m_pstModelInfo);
// 	m_wnd_FFTOp.SetUpdateData();

}

//=============================================================================
// Method		: GetModelInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/13 - 17:22
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::GetModelInfo()
{
	m_wnd_CurrentOp.GetUpdateData();
	//m_wnd_OperModeOp.GetUpdateData();
	m_wnd_CenterPointOp.GetUpdateData();
	m_wnd_RotateOp.GetUpdateData();
	m_wnd_EIAJOp.GetUpdateData();
	m_wnd_AngleOp.GetUpdateData();
	//m_wnd_SFROp.GetUpdateData();
	m_Wnd_ColorOp.GetUpdateData();
	m_wnd_ReverseOp.GetUpdateData();
	//m_wnd_LEDTestOp.GetUpdateData();
	m_wnd_ParticleMAOp.GetUpdateData();
// 	m_wnd_VideoSignalOp.GetUpdateData();
// 	m_wnd_ResetOp.GetUpdateData();

 	//m_wnd_PatternNoiseOp.GetUpdateData();
	m_wnd_BrightnessOp.GetUpdateData();
	m_wnd_IRFilterOp.GetUpdateData();

// 	m_wnd_IRFilterManualOp.GetUpdateData();
 	m_wnd_BlackSpotOp.GetUpdateData();
	m_wnd_DefectPixelOp.GetUpdateData();
// 	m_wnd_FFTOp.GetUpdateData();

}

//=============================================================================
// Method		: SetStatusEngineerMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2017/7/7 - 11:05
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::SetStatusEngineerMode(__in enPermissionMode InspMode)
{
	m_wnd_EIAJOp.SetStatusEngineerMode(InspMode);
}
