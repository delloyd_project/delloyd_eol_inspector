﻿#ifndef List_RotateResult_h__
#define List_RotateResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_RotateResult
{
	RotateResult_Object = 0,
	RotateResult_Value,
	RotateResult_MinSpec,
	RotateResult_MaxSpec,
	RotateResult_Result,
	RotateResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_RotateResult[] =
{
	_T(""),
	_T("Value"),
	_T("Min Spec"),
	_T("Max Spec"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_RotateResult
{
	RotateResult_ItemNum = 1,
};

static LPCTSTR	g_lpszItem_RotateResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_RotateResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_RotateResult[] =
{
	80,
	80,
	80,
	80,
	80,
};
// List_RotateInfo

class CList_RotateResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_RotateResult)

public:
	CList_RotateResult();
	virtual ~CList_RotateResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Rotate(ST_LT_TI_Rotate* pstRotate)
	{
		if (pstRotate == NULL)
			return;

		m_pstRotate = pstRotate;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_Rotate*  m_pstRotate;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_RotateInfo_h__
