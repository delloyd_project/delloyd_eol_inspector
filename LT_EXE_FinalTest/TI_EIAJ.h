﻿#pragma once
#include "Def_TestItem.h"
#include "Def_TestDevice.h"
#include "cv.h"
#include "highgui.h"

class CTI_EIAJ
{
public:
	CTI_EIAJ();
	~CTI_EIAJ();

	void	DeleteMemory				(ST_LT_TI_EIAJ *pstEIAJ);
	BOOL	SetImageSize				(DWORD dwWidth, DWORD dwHeight);

	/*해상력 측정*/
	UINT	EIAJ_Test					(ST_LT_TI_EIAJ *pstEIAJ, LPBYTE pImageBuf);

	/*측정알고리즘*/
	UINT	Get_Ref_Threshold_Value		(ST_LT_TI_EIAJ *pstEIAJ, LPBYTE IN_RGB, int iNum);
	BOOL	CheckEIAJ					(ST_LT_TI_EIAJ *pstEIAJ, LPBYTE IN_RGB, int iNum, int iloopcnt);
	BOOL	SetStartEndPos				(ST_LT_TI_EIAJ *pstEIAJ, BYTE *BWImage, int iNum, int iMode, int iref_white_value, int iref_black_value);
	int		GetResultData				(ST_LT_TI_EIAJ *pstEIAJ, int iNum);
	int		GetEdgeValue				(LPBYTE IN_RGB, int iStartX, int iStartY, int iWidth, int iHeight);

	int		GetCheckLine_LR_DU_using_Sharpness	(ST_LT_TI_EIAJ *pstEIAJ, BYTE *BWImage, int icheck_mode, int iWidth, int iHeight, int iNum, int iloopcnt);
	int		GetCheckLine_RL_UD_using_Sharpness	(ST_LT_TI_EIAJ *pstEIAJ, BYTE *BWImage, int icheck_mode, int iWidth, int iHeight, int iNum, int iloopcnt);

	/*측정알고리즘*/
	CvPoint SearchCenterPoint			(LPBYTE IN_RGB);
	double	GetDistance					(int ix1, int iy1, int ix2, int iy2);

	int	m_avg_check_line;
	unsigned int m_Black_Ref_Value;
	unsigned int m_White_Ref_Value;
	unsigned int m_Ref_Threshold;
	unsigned int m_BW_Ref_Sub_Value;

protected:

	UINT m_nWidth;
	UINT m_nHeight;
};
