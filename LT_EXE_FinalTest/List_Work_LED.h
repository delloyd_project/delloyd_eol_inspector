﻿#ifndef List_Work_LED_h__
#define List_Work_LED_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_LED_Worklist
{
	LED_W_Recode,
	LED_W_Time,
	LED_W_Equipment,
	LED_W_Model,
	LED_W_SWVersion,
	LED_W_LOTNum,
	LED_W_Barcode,
	//LED_W_Operator,
	LED_W_Result,
	LED_W_Current,
	LED_W_ResetCount,
	LED_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_LED_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Result"),
	_T("mA"),
	_T("Reset Count"),
	NULL
};

const int	iListAglin_LED_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_LED_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};

// CList_Work_LED

class CList_Work_LED : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_LED)

public:
	CList_Work_LED();
	virtual ~CList_Work_LED();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // List_Work_LED_h__
