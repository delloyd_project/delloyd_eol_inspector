﻿#ifndef List_SFRResult_h__
#define List_SFRResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_SFRResult
{
	SFRResult_Object = 0,
	SFRResult_Value,
	SFRResult_MinSpec,
	SFRResult_MaxSpec,
	SFRResult_Result,
	SFRResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_SFRResult[] =
{
	_T(""),
	_T("Value"),
	_T("Min Spec"),
	_T("Max Spec"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_SFRResult
{
	SFRResult_ItemNum = ROI_SFR_Max,
};

static LPCTSTR	g_lpszItem_SFRResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_SFRResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_SFRResult[] =
{
	80,
	80,
	80,
	80,
	80,
};
// List_SFRInfo

class CList_SFRResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_SFRResult)

public:
	CList_SFRResult();
	virtual ~CList_SFRResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_SFR(ST_LT_TI_SFR* pstSFR)
	{
		if (pstSFR == NULL)
			return;

		m_pstSFR = pstSFR;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_SFR*  m_pstSFR;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_SFRInfo_h__
