﻿#ifndef List_IRFilterManualResult_h__
#define List_IRFilterManualResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_IRFilterManualResult
{
	IRFilterManualResult_Object = 0,
	IRFilterManualResult_Result,
	IRFilterManualResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_IRFilterManualResult[] =
{
	_T(""),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_IRFilterManualResult
{
	IRFilterManualResult_ItemNum = 1,
};

static LPCTSTR	g_lpszItem_IRFilterManualResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_IRFilterManualResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_IRFilterManualResult[] =
{
	80,
	80,
};

// List_IRFilterManualInfo

class CList_IRFilterManualResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_IRFilterManualResult)

public:
	CList_IRFilterManualResult();
	virtual ~CList_IRFilterManualResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_IRFilterManual(ST_LT_TI_IRFilterMA* pstIRFilterManual)
	{
		if (pstIRFilterManual == NULL)
			return;

		m_pstIRFilterManual = pstIRFilterManual;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_IRFilterMA*  m_pstIRFilterManual;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_IRFilterManualInfo_h__
