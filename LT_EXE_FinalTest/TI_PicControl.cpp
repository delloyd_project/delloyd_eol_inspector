﻿// TI_PicControl.cpp : 구현 파일입니다.
//

#include "stdafx.h"
//#include "Project_CameraTest.h"
//#include "Project_CameraTestDlg.h"
#include "TI_PicControl.h"


// CTI_PicControl


CTI_PicControl::CTI_PicControl()
{
	m_pstModelInfo = NULL;
}

CTI_PicControl::~CTI_PicControl()
{
}

//=============================================================================
// Method		: CurrentPic
// Access		: public  
// Returns		: void
// Parameter	: CDC * cdc
// Qualifier	:
// Last Update	: 2017/9/5 - 14:46
// Desc.		:
//=============================================================================
void CTI_PicControl::CurrentPic(CDC *cdc)
{
	if (m_pstModelInfo == NULL)
		return;

	ST_LT_TI_Current stCurrent = m_pstModelInfo->stCurrent;

	CPen	my_Pan;
	::SetBkMode(cdc->m_hDC, TRANSPARENT);

	CFont font, *old_font;
	CString TxtData;
	font.CreatePointFont(150, _T("Arial"));

	old_font = cdc->SelectObject(&font);

	if (stCurrent.stCurrentData.nResult == TRUE)
		cdc->SetTextColor(BLUE_COLOR);
	else
		cdc->SetTextColor(RED_COLOR);

	cdc->SetTextAlign(TA_CENTER | TA_BASELINE);
	TxtData.Format(_T("Current: %dmA"), stCurrent.stCurrentData.nCurrent);
	cdc->TextOut(m_pstModelInfo->dwWidth / 2, m_pstModelInfo->dwHeight - 50, TxtData.GetBuffer(0), TxtData.GetLength());

	cdc->SelectObject(old_font);
	font.DeleteObject();
}

// CTI_PicControl 메시지 처리기입니다.
//=============================================================================
// Method		: CenterPointPic
// Access		: public  
// Returns		: void
// Parameter	: CDC * cdc
// Qualifier	:
// Last Update	: 2017/2/10 - 19:42
// Desc.		:
//=============================================================================
void CTI_PicControl::CenterPointPic(CDC *cdc)
{
	if (m_pstModelInfo == NULL)
		return;

	ST_LT_TI_CenterPoint stCenterPoint = m_pstModelInfo->stCenterPoint;

	CPen	my_Pan, *old_pan;
	CString TxtData = _T("");
	::SetBkMode(cdc->m_hDC, TRANSPARENT);
		
	if (stCenterPoint.stCenterPointData.nResult == TRUE){
		my_Pan.CreatePen(PS_SOLID, 5, YELLOW_COLOR);
		cdc->SetTextColor(BLUE_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
	}
	else{
		my_Pan.CreatePen(PS_SOLID, 5, RED_COLOR);
		cdc->SetTextColor(RED_COLOR);
		old_pan = cdc->SelectObject(&my_Pan);
	}

	int PosX = stCenterPoint.stCenterPointData.iResult_RealPos_X;
	int PosY = stCenterPoint.stCenterPointData.iResult_RealPos_Y;
	cdc->MoveTo(PosX - 10, PosY);
	cdc->LineTo(PosX + 10, PosY);

	cdc->MoveTo(PosX, PosY - 10);
	cdc->LineTo(PosX, PosY + 10);
	cdc->SelectObject(old_pan);

	my_Pan.DeleteObject();

	CFont font, *old_font;

	font.CreatePointFont(200, _T("Arial"));
	old_font = cdc->SelectObject(&font);
	if (stCenterPoint.stCenterPointData.nResult == TRUE)
		cdc->SetTextColor(BLUE_COLOR);
	else
		cdc->SetTextColor(RED_COLOR);

	cdc->SetTextAlign(TA_CENTER | TA_BASELINE);

	if (stCenterPoint.stCenterPointData.iResult_Offset_X < -99999 || stCenterPoint.stCenterPointData.iResult_Offset_Y < -99999)
		TxtData.Format(_T("Center Point No Detect"));
	else
		TxtData.Format(_T("OFFSET X: %d OFFSET Y: %d"), stCenterPoint.stCenterPointData.iResult_Offset_X, stCenterPoint.stCenterPointData.iResult_Offset_Y);

	cdc->TextOut(m_pstModelInfo->dwWidth / 2, m_pstModelInfo->dwHeight - 80, TxtData.GetBuffer(0), TxtData.GetLength());

	cdc->SelectObject(old_font);
	font.DeleteObject();
}

 //=============================================================================
 // Method		: ResolutionPic
 // Access		: public  
 // Returns		: void
 // Parameter	: CDC * cdc
 // Qualifier	:
 // Last Update	: 2017/2/10 - 19:46
 // Desc.		:
 //=============================================================================
 void CTI_PicControl::EIAJPic(CDC *cdc)
 {
	 if (m_pstModelInfo == NULL)
		 return;

	 for (UINT nIdx = 0; nIdx < ReOp_ItemNum; nIdx++)
		 EIAJPic(cdc, nIdx);
 }
 
//=============================================================================
// Method		: ResolutionPic
// Access		: public  
// Returns		: void
// Parameter	: CDC * cdc
// Parameter	: int NUM
// Qualifier	:
// Last Update	: 2017/2/10 - 19:46
// Desc.		:
//=============================================================================
 void CTI_PicControl::EIAJPic(CDC *cdc, int NUM)
 {
	 if (m_pstModelInfo == NULL)
		 return;

	 if (m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].bUse == FALSE)
		 return;

	 CPen	my_Pan, *old_pan;
	 CString TEXTDATA;
	 ::SetBkMode(cdc->m_hDC, TRANSPARENT);

	 CFont font, *old_font;

	 ST_LT_TI_EIAJ stEIAJ = m_pstModelInfo->stEIAJ;
	 font.CreatePointFont(150, _T("Arial"));
	 old_font = cdc->SelectObject(&font);

	 if (NUM > 1)
	 {
		 CString RectName;
		 cdc->SetTextColor(GREEN_COLOR);
		
		
		 RectName.Format(_T("%s"), g_szResolutionRegion[NUM]);
		 cdc->TextOut(stEIAJ.stEIAJOp.rectData[NUM].RegionList.left, stEIAJ.stEIAJOp.rectData[NUM].RegionList.top, RectName.GetBuffer(0), RectName.GetLength());

		 if (stEIAJ.stEIAJData.nEachResult[NUM] == TRUE)
		 {
			 my_Pan.CreatePen(PS_SOLID, 2, BLUE_COLOR);
			 cdc->SetTextColor(BLUE_COLOR);
			 old_pan = cdc->SelectObject(&my_Pan);
		 }
		 else
		 {
			 my_Pan.CreatePen(PS_SOLID, 2, RED_COLOR);
			 cdc->SetTextColor(RED_COLOR);
			 old_pan = cdc->SelectObject(&my_Pan);
		 }
	 }
	 else
	 {
		 my_Pan.CreatePen(PS_SOLID, 2, GRAY_DK_COLOR);
		 cdc->SetTextColor(GRAY_DK_COLOR);
		 old_pan = cdc->SelectObject(&my_Pan);
	 }

	 /*Roi 영역 틀*/
	 cdc->MoveTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.left, stEIAJ.stEIAJOp.rectData[NUM].RegionList.top);
	 cdc->LineTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.right, stEIAJ.stEIAJOp.rectData[NUM].RegionList.top);
	 cdc->LineTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.right, stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom);
	 cdc->LineTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.left, stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom);
	 cdc->LineTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.left, stEIAJ.stEIAJOp.rectData[NUM].RegionList.top);
	 cdc->SelectObject(old_pan);
	 my_Pan.DeleteObject();

	 if (NUM < 2){
		 cdc->SelectObject(old_font);
		 font.DeleteObject();
		 cdc->SelectObject(old_pan);
		 my_Pan.DeleteObject();
		 return;

	 }

	 /*TEST 시*/
	 int iMode = stEIAJ.stEIAJOp.rectData[NUM].i_Mode;
	 if (iMode == Resol_Mode_Bottom_Top || iMode == Resol_Mode_Top_Bottom)
	 {
		 int m_ConstValue = stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + stEIAJ.stEIAJOp.rectData[NUM].CheckLine;
		 int m_ResultValue = stEIAJ.stEIAJOp.rectData[NUM].RegionList.top;
		 my_Pan.CreatePen(PS_SOLID, 2, YELLOW_COLOR);
		 old_pan = cdc->SelectObject(&my_Pan);
		 cdc->MoveTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.left, m_ConstValue);//고정좌표
		 cdc->LineTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.right, m_ConstValue);
		 cdc->SelectObject(old_pan);
		 my_Pan.DeleteObject();

		 my_Pan.CreatePen(PS_SOLID, 2, GREEN_COLOR);
		 old_pan = cdc->SelectObject(&my_Pan);
		 int offset1, offset2;
		 offset1 = stEIAJ.stEIAJOp.rectData[NUM].RegionList.Height() - stEIAJ.stEIAJOp.rectData[NUM].PatternStartPos.y;
		 offset2 = stEIAJ.stEIAJOp.rectData[NUM].PatternEndPos.y;
		 cdc->MoveTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.left, stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom - offset1);
		 cdc->LineTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.right, stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom - offset1);
		 cdc->MoveTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.left, stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + offset2);
		 cdc->LineTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.right, stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + offset2);
		 int ThresholdPosition = GetStandardData(NUM);
		 cdc->SelectObject(old_pan);
		 my_Pan.DeleteObject();

		 int ThresholdValue, PeakThresholdValue;
		 if (iMode == Resol_Mode_Bottom_Top)
		 {
			 ThresholdValue = stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom - offset1 - ThresholdPosition;
			 PeakThresholdValue = stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom - offset1 - GetStandardPeakData(NUM);
		 }

		 if (iMode == Resol_Mode_Top_Bottom)
		 {
			 ThresholdValue = stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + stEIAJ.stEIAJOp.rectData[NUM].PatternStartPos.y + ThresholdPosition;
			 PeakThresholdValue = stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + stEIAJ.stEIAJOp.rectData[NUM].PatternStartPos.y + GetStandardPeakData(NUM);
		 }

		 my_Pan.CreatePen(PS_SOLID, 2, PINK_COLOR);
		 old_pan = cdc->SelectObject(&my_Pan);

		 if (abs(stEIAJ.stEIAJOp.rectData[NUM].RegionList.left - stEIAJ.stEIAJOp.rectData[NUM].RegionList.right) != 0)
		 {
			 cdc->MoveTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.left, ThresholdValue);//기준점
			 cdc->LineTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.right, ThresholdValue);
		 }

		 cdc->SelectObject(old_pan);
		 my_Pan.DeleteObject();

		 my_Pan.CreatePen(PS_SOLID, 2, RGB(200, 0, 200));
		 old_pan = cdc->SelectObject(&my_Pan);
		 if (abs(stEIAJ.stEIAJOp.rectData[NUM].RegionList.left - stEIAJ.stEIAJOp.rectData[NUM].RegionList.right) != 0)
		 {
			 cdc->MoveTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.left, PeakThresholdValue);//기준점
			 cdc->LineTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.right, PeakThresholdValue);
		 }

		 cdc->SelectObject(old_pan);
		 my_Pan.DeleteObject();

		 /*Data 값 출력*/
		 if (stEIAJ.stEIAJData.nEachResult[NUM] == TRUE)
		 {
			 my_Pan.CreatePen(PS_SOLID, 2, BLUE_COLOR);
			 cdc->SetTextColor(BLUE_COLOR);
		 }
		 else
		 {
			 my_Pan.CreatePen(PS_SOLID, 2, RED_COLOR);
			 cdc->SetTextColor(RED_COLOR);
		 }
		 my_Pan.DeleteObject();

	
		 TEXTDATA.Format(_T("%d"), stEIAJ.stEIAJData.nValueResult[NUM]);

		 cdc->TextOut(stEIAJ.stEIAJOp.rectData[NUM].RegionList.left - 30, m_ResultValue, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());
	


		  DrawSFRGraph(cdc, NUM);
	 }
	 else if (iMode == Resol_Mode_Right_Left || iMode == Resol_Mode_Left_Right)
	 {
		 int m_ConstValue = stEIAJ.stEIAJOp.rectData[NUM].RegionList.left + stEIAJ.stEIAJOp.rectData[NUM].CheckLine;
		 int m_ResultValue = stEIAJ.stEIAJOp.rectData[NUM].RegionList.left;

		 my_Pan.CreatePen(PS_SOLID, 2, YELLOW_COLOR);
		 old_pan = cdc->SelectObject(&my_Pan);
		 cdc->MoveTo(m_ConstValue, stEIAJ.stEIAJOp.rectData[NUM].RegionList.top);//고정좌표
		 cdc->LineTo(m_ConstValue, stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom);
		 cdc->SelectObject(old_pan);
		 my_Pan.DeleteObject();

		 my_Pan.CreatePen(PS_SOLID, 2, GREEN_COLOR);
		 old_pan = cdc->SelectObject(&my_Pan);
		 int offset1, offset2;
		 offset1 = stEIAJ.stEIAJOp.rectData[NUM].RegionList.Width() - stEIAJ.stEIAJOp.rectData[NUM].PatternEndPos.x;
		 offset2 = stEIAJ.stEIAJOp.rectData[NUM].PatternStartPos.x;
		 cdc->MoveTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.left + offset2, stEIAJ.stEIAJOp.rectData[NUM].RegionList.top);
		 cdc->LineTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.left + offset2, stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom);
		 cdc->MoveTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.right - offset1, stEIAJ.stEIAJOp.rectData[NUM].RegionList.top);
		 cdc->LineTo(stEIAJ.stEIAJOp.rectData[NUM].RegionList.right - offset1, stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom);
		 int ThresholdPosition = GetStandardData(NUM);
		 int PeakThresholdValue = GetStandardPeakData(NUM);
		 cdc->SelectObject(old_pan);
		 my_Pan.DeleteObject();

		 int ThresholdValue, ThresholdValue_Peak;
		 if (iMode == Resol_Mode_Left_Right)
		 {
			 ThresholdValue = stEIAJ.stEIAJOp.rectData[NUM].RegionList.left + (stEIAJ.stEIAJOp.rectData[NUM].PatternStartPos.x) + ThresholdPosition;
			 ThresholdValue_Peak = stEIAJ.stEIAJOp.rectData[NUM].RegionList.left + (stEIAJ.stEIAJOp.rectData[NUM].PatternStartPos.x) + PeakThresholdValue;
		 }

		 if (iMode == Resol_Mode_Right_Left)
		 {
			 ThresholdValue = stEIAJ.stEIAJOp.rectData[NUM].RegionList.right - (stEIAJ.stEIAJOp.rectData[NUM].RegionList.Width() - stEIAJ.stEIAJOp.rectData[NUM].PatternStartPos.x) - ThresholdPosition;
			 ThresholdValue_Peak = stEIAJ.stEIAJOp.rectData[NUM].RegionList.right - (stEIAJ.stEIAJOp.rectData[NUM].RegionList.Width() - stEIAJ.stEIAJOp.rectData[NUM].PatternStartPos.x) - PeakThresholdValue;
		 }

		 my_Pan.CreatePen(PS_SOLID, 2, PINK_COLOR);
		 old_pan = cdc->SelectObject(&my_Pan);

		 if (abs(stEIAJ.stEIAJOp.rectData[NUM].RegionList.top - stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom) != 0)
		 {
			 cdc->MoveTo(ThresholdValue, stEIAJ.stEIAJOp.rectData[NUM].RegionList.top);//기준점
			 cdc->LineTo(ThresholdValue, stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom);
		 }

		 cdc->SelectObject(old_pan);
		 my_Pan.DeleteObject();

		 my_Pan.CreatePen(PS_SOLID, 2, RGB(200, 0, 200));
		 old_pan = cdc->SelectObject(&my_Pan);

		 if (abs(stEIAJ.stEIAJOp.rectData[NUM].RegionList.top - stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom) != 0)
		 {
			 cdc->MoveTo(ThresholdValue_Peak, stEIAJ.stEIAJOp.rectData[NUM].RegionList.top);//기준점
			 cdc->LineTo(ThresholdValue_Peak, stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom);
		 }

		 cdc->SelectObject(old_pan);
		 my_Pan.DeleteObject();

		 /*Data 값 출력*/
		 if (stEIAJ.stEIAJData.nEachResult[NUM] == TRUE)
		 {
			 my_Pan.CreatePen(PS_SOLID, 2, BLUE_COLOR);
			 cdc->SetTextColor(BLUE_COLOR);
		 }
		 else
		 {
			 my_Pan.CreatePen(PS_SOLID, 2, RED_COLOR);
			 cdc->SetTextColor(RED_COLOR);
		 }

		 old_pan = cdc->SelectObject(&my_Pan);
		 cdc->SelectObject(old_pan);
		 my_Pan.DeleteObject();


		 TEXTDATA.Format(_T("%d"), stEIAJ.stEIAJData.nValueResult[NUM]);
		 cdc->TextOut(m_ResultValue, stEIAJ.stEIAJOp.rectData[NUM].RegionList.top - 20, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());

		 DrawSFRGraph(cdc, NUM);
	 }


	 cdc->SelectObject(old_font);
	 font.DeleteObject();


 }
  
//=============================================================================
// Method		: GetStandardData
// Access		: public  
// Returns		: int
// Parameter	: int NUM
// Qualifier	:
// Last Update	: 2017/2/10 - 19:49
// Desc.		:
//=============================================================================
int CTI_PicControl::GetStandardData(int NUM)
{
	double x = 0;
	double y = 0;

	ST_LT_TI_EIAJ stEIAJ = m_pstModelInfo->stEIAJ;

	x = (m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].i_Threshold_Min - m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].i_Range_Min);

	y = x / (m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].i_Range_Max - m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].i_Range_Min);

	double sx1, sy1, sx2, sy2;
	sx1 = m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].PatternStartPos.x;
	sy1 = m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].PatternStartPos.y;
	sx2 = m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].PatternEndPos.x;
	sy2 = m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].PatternEndPos.y;

	double dist = sqrt(((sx2 - sx1)*(sx2 - sx1)) + ((sy2 - sy1)*(sy2 - sy1)));
	int RD_OFFSET = (int)(dist * y);

	return RD_OFFSET;
}

//=============================================================================
// Method		: GetStandardPeakData
// Access		: public  
// Returns		: int
// Parameter	: int NUM
// Qualifier	:
// Last Update	: 2017/2/10 - 19:50
// Desc.		:
//=============================================================================
int CTI_PicControl::GetStandardPeakData(int NUM)
{
	double x = 0;
	double y = 0;

	ST_LT_TI_EIAJ stEIAJ = m_pstModelInfo->stEIAJ;

	x = (m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].i_Threshold_Max - m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].i_Range_Min);

	y = x / (m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].i_Range_Max - m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].i_Range_Min);

	double sx1, sy1, sx2, sy2;
	sx1 = m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].PatternStartPos.x;
	sy1 = m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].PatternStartPos.y;
	sx2 = m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].PatternEndPos.x;
	sy2 = m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].PatternEndPos.y;

	double dist = sqrt(((sx2 - sx1)*(sx2 - sx1)) + ((sy2 - sy1)*(sy2 - sy1)));
	int RD_OFFSET = (int)(dist * y);

	return RD_OFFSET;
}

//=============================================================================
// Method		: DrawSFRGraph
// Access		: public  
// Returns		: void
// Parameter	: CDC * cdc
// Parameter	: int NUM
// Qualifier	:
// Last Update	: 2017/2/10 - 20:00
// Desc.		:
//=============================================================================
void CTI_PicControl::DrawSFRGraph(CDC *cdc, int NUM)
{
	if (m_pstModelInfo->stEIAJ.stEIAJOp.bGViewMode == FALSE)
		return;

	CString TEXTDATA;
	CPen my_pen, *old_pan, bk_pen, *old_bk_pen;

	ST_LT_TI_EIAJ stEIAJ = m_pstModelInfo->stEIAJ;

	if (m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].bResult)
	{
		bk_pen.CreatePen(PS_SOLID, 2, BLUE_COLOR);
		old_bk_pen = cdc->SelectObject(&bk_pen);
		cdc->SetTextColor(BLUE_COLOR);
	}
	else
	{
		bk_pen.CreatePen(PS_SOLID, 2, RED_COLOR);
		old_bk_pen = cdc->SelectObject(&bk_pen);
		cdc->SetTextColor(RED_COLOR);
	}

	if (NUM == 2)
	{
		cdc->Rectangle(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top - 10, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.right, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top - 110);

		for (int i = 0; i < m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_Width - 1; i++)
		{
			cdc->MoveTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left + i, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_Value[i]);
			cdc->LineTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left + (i + 1), m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_Value[i + 1]);
		}
		cdc->SelectObject(old_bk_pen);
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for (int i = 0; i < m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_Width - 1; i++)
		{
			cdc->MoveTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left + i, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_LineFitting_Value[i]);
			cdc->LineTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left + (i + 1), m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_LineFitting_Value[i + 1]);
		}
		cdc->SelectObject(old_pan);
		my_pen.DeleteObject();
	}
	else if (NUM == 3)
	{
		cdc->Rectangle(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.right, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom + 10, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom + 110);

		for (int i = m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_Width - 1; i > 0; i--)
		{
			cdc->MoveTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left + i, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom + 10 + (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_Value[i]);
			cdc->LineTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left + (i - 1), m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom + 10 + (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_Value[i - 1]);
		}

		cdc->SelectObject(old_bk_pen);
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for (int i = m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_Width - 1; i > 0; i--)
		{
			cdc->MoveTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left + i, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom + 10 + (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_LineFitting_Value[i]);
			cdc->LineTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left + (i - 1), m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom + 10 + (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_LineFitting_Value[i - 1]);
		}

		cdc->SelectObject(old_pan);
		my_pen.DeleteObject();
	}
	else if (NUM == 4)
	{
		cdc->Rectangle(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.right + 10, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.right + 110, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom);

		for (int i = 0; i < m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_Height - 1; i++)
		{
			cdc->MoveTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.right + 10 + (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_Value[i], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + i);
			cdc->LineTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.right + 10 + (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_Value[i + 1], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + (i + 1));
		}

		cdc->SelectObject(old_bk_pen);
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for (int i = 0; i < m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_Height - 1; i++)
		{
			cdc->MoveTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.right + 10 + (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_LineFitting_Value[i], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + i);
			cdc->LineTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.right + 10 + (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_LineFitting_Value[i + 1], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + (i + 1));
		}

		cdc->SelectObject(old_pan);
		my_pen.DeleteObject();
	}
	else if (NUM == 5)
	{
		cdc->Rectangle(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left - 10, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left  - 110, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom);

		for (int i = 0; i < m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_Height- 1; i++)
		{
			cdc->MoveTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_Value[i], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + i);
			cdc->LineTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_Value[i + 1], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + (i + 1));
		}

		cdc->SelectObject(old_bk_pen);
		old_bk_pen->DeleteObject();
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for (int i = 0; i < m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_Height- 1; i++)
		{
			cdc->MoveTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left  - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_LineFitting_Value[i], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + i);
			cdc->LineTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left  - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_LineFitting_Value[i + 1], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + (i + 1));
		}
		cdc->SelectObject(old_pan);
		old_pan->DeleteObject();
		my_pen.DeleteObject();
	}
	else if (NUM == 7)
	{
		cdc->Rectangle(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left - 10, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left - 110, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom);

		for (int i = 0; i < m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_Height - 1; i++)
		{
			cdc->MoveTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_Value[i], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + i);
			cdc->LineTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_Value[i + 1], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + (i + 1));
		}

		cdc->SelectObject(old_bk_pen);
		old_bk_pen->DeleteObject();
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for (int i = 0; i < m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_Height - 1; i++)
		{
			cdc->MoveTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_LineFitting_Value[i], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + i);
			cdc->LineTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_LineFitting_Value[i + 1], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + (i + 1));
		}
		cdc->SelectObject(old_pan);
		old_pan->DeleteObject();
		my_pen.DeleteObject();
	}
	else if (NUM == 11)
	{
		cdc->Rectangle(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.right + 10, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.right + 110, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom);

		for (int i = 0; i < m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_Height - 1; i++)
		{
			cdc->MoveTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.right + 10 + (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_Value[i], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + i);
			cdc->LineTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.right + 10 + (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_Value[i + 1], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + (i + 1));
		}

		cdc->SelectObject(old_bk_pen);
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for (int i = 0; i < m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_Height - 1; i++)
		{
			cdc->MoveTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.right + 10 + (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_LineFitting_Value[i], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + i);
			cdc->LineTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.right + 10 + (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_LineFitting_Value[i + 1], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + (i + 1));
		}

		cdc->SelectObject(old_pan);
		my_pen.DeleteObject();
	}
	else if (NUM == 13)
	{
		cdc->Rectangle(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left  - 10, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left  - 110, m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.bottom);

		for (int i = 0; i < m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_Height- 1; i++)
		{
			cdc->MoveTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left  - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_Value[i], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + i);
			cdc->LineTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left  - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_Value[i + 1], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + (i + 1));
		}

		cdc->SelectObject(old_bk_pen);
		old_bk_pen->DeleteObject();
		bk_pen.DeleteObject();

		my_pen.CreatePen(PS_SOLID, 2, RGB(200, 127, 0));
		old_pan = cdc->SelectObject(&my_pen);

		for (int i = 0; i < m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_Height- 1; i++)
		{
			cdc->MoveTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left  - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_LineFitting_Value[i], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + i);
			cdc->LineTo(m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.left  - 10 - (int)m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].SFR_MTF_LineFitting_Value[i + 1], m_pstModelInfo->stEIAJ.stEIAJOp.rectData[NUM].RegionList.top + (i + 1));
		}
		cdc->SelectObject(old_pan);
		old_pan->DeleteObject();
		my_pen.DeleteObject();
	}
	else
	{
		cdc->SelectObject(old_bk_pen);
		old_bk_pen->DeleteObject();
		bk_pen.DeleteObject();
	}
}

//=============================================================================
// Method		: RotatePic
// Access		: public  
// Returns		: void
// Parameter	: CDC * cdc
// Qualifier	:
// Last Update	: 2017/2/10 - 20:05
// Desc.		:
//=============================================================================
void CTI_PicControl::RotatePic(CDC *cdc)
{
	if (m_pstModelInfo == NULL)
		return;

	for (UINT nIdx = 0; nIdx < RegionRotate_MaxEnum; nIdx++)
		RotatePic(cdc, nIdx);
}

//=============================================================================
// Method		: RotatePic
// Access		: public  
// Returns		: void
// Parameter	: CDC * cdc
// Parameter	: int NUM
// Qualifier	:
// Last Update	: 2017/2/10 - 19:59
// Desc.		:
//=============================================================================
void CTI_PicControl::RotatePic(CDC *cdc, int NUM)
{
	if (m_pstModelInfo == NULL)
		return;

	/*ROI 그리기*/
	CPen my_Pan, *old_pan;

	::SetBkMode(cdc->m_hDC, TRANSPARENT);

	int left   = m_pstModelInfo->stRotate.stRotateOp.rectData[NUM].RegionList.left;
	int top    = m_pstModelInfo->stRotate.stRotateOp.rectData[NUM].RegionList.top;
	int right  = m_pstModelInfo->stRotate.stRotateOp.rectData[NUM].RegionList.right;
	int bottom = m_pstModelInfo->stRotate.stRotateOp.rectData[NUM].RegionList.bottom;

	my_Pan.CreatePen(PS_SOLID, 2, RGB(255, 255, 0));
	old_pan = cdc->SelectObject(&my_Pan);

	cdc->MoveTo(left, top);
	cdc->LineTo(right, top);
	cdc->LineTo(right, bottom);
	cdc->LineTo(left, bottom);
	cdc->LineTo(left, top);


	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();

	CFont font, *old_font;
	CString TxtData;

	font.CreatePointFont(250, _T("Arial"));
	old_font = cdc->SelectObject(&font);

	if (m_pstModelInfo->stRotate.stRotateData.nResult == FALSE){
		cdc->SetTextColor(RGB(255, 0, 0));
	}
	else{
		cdc->SetTextColor(RGB(0, 0, 255));
	}

	if (m_pstModelInfo->stRotate.stRotateData.dbDegree != 90)
	{
		TxtData.Format(_T("%.2f ˚"), m_pstModelInfo->stRotate.stRotateData.dbDegree);
	}
	else
	{
		TxtData.Format(_T("각도측정실패"));

	}
	
	cdc->TextOut(m_pstModelInfo->dwWidth / 2 - 10, m_pstModelInfo->dwHeight - 40, TxtData.GetBuffer(0), TxtData.GetLength());

	cdc->SelectObject(old_font);
	font.DeleteObject();


	/*로테이트 각도 그리기*/
	my_Pan.CreatePen(PS_SOLID, 2, RGB(0, 127, 0));
	old_pan = cdc->SelectObject(&my_Pan);

	cdc->MoveTo(m_pstModelInfo->stRotate.stRotateOp.rectData[RegionRotate_LeftTop].RegionList.CenterPoint().x,
		m_pstModelInfo->stRotate.stRotateOp.rectData[RegionRotate_LeftTop].RegionList.CenterPoint().y);

	cdc->LineTo(m_pstModelInfo->stRotate.stRotateOp.rectData[RegionRotate_LeftBottom].RegionList.CenterPoint().x,
		m_pstModelInfo->stRotate.stRotateOp.rectData[RegionRotate_LeftBottom].RegionList.CenterPoint().y);

	cdc->LineTo(m_pstModelInfo->stRotate.stRotateOp.rectData[RegionRotate_RightBottom].RegionList.CenterPoint().x,
		m_pstModelInfo->stRotate.stRotateOp.rectData[RegionRotate_RightBottom].RegionList.CenterPoint().y);

	cdc->LineTo(m_pstModelInfo->stRotate.stRotateOp.rectData[RegionRotate_RightTop].RegionList.CenterPoint().x,
		m_pstModelInfo->stRotate.stRotateOp.rectData[RegionRotate_RightTop].RegionList.CenterPoint().y);

	cdc->LineTo(m_pstModelInfo->stRotate.stRotateOp.rectData[RegionRotate_LeftTop].RegionList.CenterPoint().x,
		m_pstModelInfo->stRotate.stRotateOp.rectData[RegionRotate_LeftTop].RegionList.CenterPoint().y);
	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();
}

//=============================================================================
// Method		: SFRPic
// Access		: public  
// Returns		: void
// Parameter	: CDC * cdc
// Qualifier	:
// Last Update	: 2017/8/10 - 20:48
// Desc.		:
//=============================================================================
void CTI_PicControl::SFRPic(CDC *cdc)
{
	if (m_pstModelInfo == NULL)
		return;

	for (UINT nIdx = 0; nIdx < Region_SFR_MaxEnum; nIdx++)
		SFRPic(cdc, nIdx);
}

//=============================================================================
// Method		: SFRPic
// Access		: public  
// Returns		: void
// Parameter	: CDC * pcdc
// Parameter	: int iNum
// Qualifier	:
// Last Update	: 2017/8/10 - 20:48
// Desc.		:
//=============================================================================
void CTI_PicControl::SFRPic(CDC *pcdc, int iNum)
{
	CPen Pen, *pOld_Pen;
	CFont Font, *pOld_Font;

	CString strData;
	CString strData2;
	COLORREF RoiColor = RGB(255, 0, 0);

	::SetBkMode(pcdc->m_hDC, TRANSPARENT);

	if (m_pstModelInfo->stSFR.stSFROp.stSFR_Region[iNum].bEnable == TRUE)
	{
		if (m_pstModelInfo->stSFR.stSFRData.nEachResult[iNum] == TRUE)
		{
			RoiColor = RGB(0, 0, 255);
		}
		else
		{
			RoiColor = RGB(255, 0, 0);
		}

		Pen.CreatePen(PS_SOLID, 2, RoiColor);
		pOld_Pen = pcdc->SelectObject(&Pen);

		int Width = m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].nWidth;
		int Height = m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].nHeight;
		int Left = m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].nPos_X - (Width / 2);
		int Top = m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].nPos_Y - (Height / 2);

		pcdc->MoveTo(Left, Top);
		pcdc->LineTo(Left + Width, Top);
		pcdc->LineTo(Left + Width, Top + Height);
		pcdc->LineTo(Left, Top + Height);
		pcdc->LineTo(Left, Top);

		int nFontSize = m_pstModelInfo->dwHeight / 5;

		Font.CreatePointFont(nFontSize, _T("Arial"));
		pOld_Font = pcdc->SelectObject(&Font);

		pcdc->SetTextColor(RGB(255, 255, 0));

		double db_1F_Width = (double)m_pstModelInfo->dwWidth / 2;
		double db_1F_Height = (double)m_pstModelInfo->dwHeight / 2;
		double db_1Field = sqrt(pow(db_1F_Width, 2) + pow(db_1F_Height, 2));

		double db_Field_Width = (double)m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].nPos_X - db_1F_Width;
		double db_Field_Height = (double)m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].nPos_Y - db_1F_Height;
		double db_Field = sqrt(pow(db_Field_Width, 2) + pow(db_Field_Height, 2)) / db_1Field;

		m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].dbField = db_Field;

		strData.Format(_T("S%d (%.2fF)"), iNum, m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].dbField);
		strData2.Format(_T("\r\n%.f"), m_pstModelInfo->stSFR.stSFRData.dbResultValue[iNum]);

		int nFtSz = nFontSize / 20;
		if (m_pstModelInfo->stSFR.stSFROp.stSFR_Region[iNum].nFont == 0)
		{
			pcdc->TextOut(Left - nFtSz - (nFtSz * strData.GetLength() * 1.5), m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].nPos_Y - nFtSz, strData, strData.GetLength());
			pcdc->TextOut(Left - nFtSz - (nFtSz * strData2.GetLength()), m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].nPos_Y - nFtSz + 11, strData2, strData2.GetLength());
		}
		else if (m_pstModelInfo->stSFR.stSFROp.stSFR_Region[iNum].nFont == 1)
		{
			pcdc->TextOut((Left + Width) + nFtSz, m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].nPos_Y - nFtSz, strData, strData.GetLength());
			pcdc->TextOut((Left + Width) + nFtSz, m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].nPos_Y - nFtSz + 10, strData2, strData2.GetLength());
		}
		else if (m_pstModelInfo->stSFR.stSFROp.stSFR_Region[iNum].nFont == 2)
		{
			pcdc->TextOut(m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].nPos_X - (nFtSz / 2 * strData.GetLength()), Top - nFtSz * 6, strData, strData.GetLength());
			pcdc->TextOut(m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].nPos_X - (nFtSz / 2 * strData2.GetLength()), Top - nFtSz * 6 + 11, strData2, strData2.GetLength());
		}
		else if (m_pstModelInfo->stSFR.stSFROp.stSFR_Region[iNum].nFont == 3)
		{
			pcdc->TextOut(m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].nPos_X - (nFtSz / 2 * strData.GetLength()), (Top + Height) + nFtSz, strData, strData.GetLength());
			pcdc->TextOut(m_pstModelInfo->stSFR.stSFROp.stSFR_InitRegion[iNum].nPos_X - (nFtSz / 2 * strData2.GetLength()), (Top + Height) + nFtSz + 11, strData2, strData2.GetLength());
		}

			pcdc->SelectObject(pOld_Font);
			Font.DeleteObject();

			pcdc->SelectObject(pOld_Pen);
			Pen.DeleteObject();
		}
	}

//=============================================================================
// Method		: ParticlePic
// Access		: public  
// Returns		: void
// Parameter	: CDC * cdc
// Qualifier	:
// Last Update	: 2017/8/13 - 15:02
// Desc.		:
//=============================================================================
void CTI_PicControl::ParticlePic(CDC *cdc)
{
	for (UINT nIdx = 0; nIdx < Particle_Region_MaxEnum; nIdx++)
		ParticlePic(cdc, nIdx);

	for (UINT nIdx = 0; nIdx < m_pstModelInfo->stParticle.stParticleData.iResultcnt; nIdx++)
		ParticleErrPic(cdc, nIdx);
}

//=============================================================================
// Method		: ParticlePic
// Access		: public  
// Returns		: void
// Parameter	: CDC * cdc
// Parameter	: int NUM
// Qualifier	:
// Last Update	: 2017/8/13 - 15:02
// Desc.		:
//=============================================================================
void CTI_PicControl::ParticlePic(CDC *cdc, int NUM)
{
	if (m_pstModelInfo == NULL)
		return;

	CPen	my_Pan, *old_pan;
	CString TEXTDATA;
	::SetBkMode(cdc->m_hDC, TRANSPARENT);

	switch (NUM)
	{
	case 0:
		my_Pan.CreatePen(PS_SOLID, 2, BLACK_COLOR);
		break;
	case 1:
		my_Pan.CreatePen(PS_SOLID, 2, GREEN_COLOR);
		break;
	case 2:
		my_Pan.CreatePen(PS_SOLID, 2, YELLOW_COLOR);
		break;
	default:
		break;
	}

	if (m_pstModelInfo->stParticle.stParticleOp.rectData[NUM].bEllipse == TRUE)
	{
		old_pan = cdc->SelectObject(&my_Pan);

		CBrush oldBrush;
		oldBrush.CreateStockObject(NULL_BRUSH);
		CBrush *poldBrush = cdc->SelectObject(&oldBrush);
		CRect Data = m_pstModelInfo->stParticle.stParticleOp.rectData[NUM].RegionList;
		cdc->Ellipse(Data.left, Data.top, Data.right, Data.bottom);
		cdc->SelectObject(old_pan);
		cdc->SelectObject(oldBrush);

		my_Pan.DeleteObject();
		oldBrush.DeleteObject();
	}
	else
	{
		old_pan = cdc->SelectObject(&my_Pan);
		CRect Data = m_pstModelInfo->stParticle.stParticleOp.rectData[NUM].RegionList;

		cdc->MoveTo(Data.left, Data.top);
		cdc->LineTo(Data.right, Data.top);
		cdc->LineTo(Data.right, Data.bottom);
		cdc->LineTo(Data.left, Data.bottom);
		cdc->LineTo(Data.left, Data.top);
		cdc->SelectObject(old_pan);
		my_Pan.DeleteObject();
	}
}

//=============================================================================
// Method		: ParticleErrPic
// Access		: public  
// Returns		: void
// Parameter	: CDC * cdc
// Parameter	: int NUM
// Qualifier	:
// Last Update	: 2017/8/13 - 15:03
// Desc.		:
//=============================================================================
void CTI_PicControl::ParticleErrPic(CDC *cdc, int NUM)
{
	CPen	my_Pan, *old_pan;
	CString TEXTDATA;
	::SetBkMode(cdc->m_hDC, TRANSPARENT);
	if (m_pstModelInfo->stParticle.stParticleOp.bFailCheck == TRUE)
	{
		if (m_pstModelInfo->stParticle.stParticleData.nResult == FALSE)
		{
			my_Pan.CreatePen(PS_SOLID, 2, RED_COLOR);
			old_pan = cdc->SelectObject(&my_Pan);
			cdc->SetTextColor(BLACK_COLOR);

			cdc->MoveTo(m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.left, m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.top);
			cdc->LineTo(m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.right, m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.top);
			cdc->LineTo(m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.right, m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.bottom);
			cdc->LineTo(m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.left, m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.bottom);
			cdc->LineTo(m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.left, m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.top);

			cdc->SelectObject(old_pan);
			my_Pan.DeleteObject();

			CFont font, *old_font;
			CString TxtData;

			font.CreatePointFont(130, _T("Arial"));
			old_font = cdc->SelectObject(&font);

			int temp_left = m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.left;

			TEXTDATA.Format(_T("B : %.1f"), m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].dbStainBlemish);
			if (m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.top - 40 <= 0)
				cdc->TextOut(temp_left, m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.bottom + 20, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());
			else
				cdc->TextOut(temp_left, m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.top - 40, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());
			TEXTDATA.Format(_T("S : %dx%d"), m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.right - m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.left
				, m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.bottom - m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.top);
			if (m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.top - 40 <= 0)
				cdc->TextOut(temp_left, m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.bottom + 40, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());
			else
				cdc->TextOut(temp_left, m_pstModelInfo->stParticle.stParticleData.ErrRegionList[NUM].RegionList.top - 20, TEXTDATA.GetBuffer(0), TEXTDATA.GetLength());

			cdc->SelectObject(old_font);
			font.DeleteObject();

		}
	}
}

//=============================================================================
// Method		: AlignPic
// Access		: public  
// Returns		: void
// Parameter	: CDC * cdc
// Qualifier	:
// Last Update	: 2017/9/19 - 14:00
// Desc.		:
//=============================================================================
void CTI_PicControl::AlignPic(CDC *cdc)
{
	m_pstModelInfo->dwWidth;
	m_pstModelInfo->dwHeight;

	CPen my_Pan, *old_pan;

	::SetBkMode(cdc->m_hDC, TRANSPARENT);


	/// Y축
	my_Pan.CreatePen(PS_SOLID, 2, RGB(0, 0, 255));
	old_pan = cdc->SelectObject(&my_Pan);

	cdc->MoveTo(m_pstModelInfo->dwWidth / 2, 0);
	cdc->LineTo(m_pstModelInfo->dwWidth / 2, m_pstModelInfo->dwHeight);

	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();

	/// X축 

	my_Pan.CreatePen(PS_SOLID, 2, RGB(0, 0, 255));
	old_pan = cdc->SelectObject(&my_Pan);

	cdc->MoveTo(0, m_pstModelInfo->dwHeight / 2);
	cdc->LineTo(m_pstModelInfo->dwWidth, m_pstModelInfo->dwHeight / 2);

	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();

	/// X축 LEFT
	my_Pan.CreatePen(PS_SOLID, 2, RGB(0, 0, 255));
	old_pan = cdc->SelectObject(&my_Pan);

	cdc->MoveTo(m_pstModelInfo->dwWidth / 2 - m_pstModelInfo->nAlign, 0);
	cdc->LineTo(m_pstModelInfo->dwWidth / 2 - m_pstModelInfo->nAlign, m_pstModelInfo->dwHeight);

	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();

	/// X축 RIGHT
	my_Pan.CreatePen(PS_SOLID, 2, RGB(0, 0, 255));
	old_pan = cdc->SelectObject(&my_Pan);

	cdc->MoveTo(m_pstModelInfo->dwWidth / 2 + m_pstModelInfo->nAlign, 0);
	cdc->LineTo(m_pstModelInfo->dwWidth / 2 + m_pstModelInfo->nAlign, m_pstModelInfo->dwHeight);

	cdc->SelectObject(old_pan);
	my_Pan.DeleteObject();
}
