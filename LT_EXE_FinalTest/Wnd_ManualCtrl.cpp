﻿//*****************************************************************************
// Filename	: 	Wnd_ManualCtrl.cpp
// Created	:	2017/1/4 - 13:32
// Modified	:	2017/1/4 - 13:32
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_ManualCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_ManualCtrl.h"
#include "resource.h"

typedef enum ManualCtrl_ID
{
	IDC_BTN_POWER_ON = 1001,
	IDC_BTN_MAX_NUM = IDC_BTN_POWER_ON + 100,
};
// CWnd_ManualCtrl

IMPLEMENT_DYNAMIC(CWnd_ManualCtrl, CWnd)

CWnd_ManualCtrl::CWnd_ManualCtrl()
{
	m_pDevice	= NULL;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

}

CWnd_ManualCtrl::~CWnd_ManualCtrl()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_ManualCtrl, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_BTN_POWER_ON, IDC_BTN_MAX_NUM, OnRangeCmds)
END_MESSAGE_MAP()

// CWnd_ManualCtrl message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/4 - 13:39
// Desc.		:
//=============================================================================
int CWnd_ManualCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Name.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Name.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Name.SetFont_Gdip(L"Arial", 10.0F);
	m_st_Name.Create(_T("Manual Control"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < BT_MCTRL_MAXNUM; nIdx++)
	{
		if (nIdx == BT_MCTRL_IMAGE_LOAD)
			continue;

		m_Btn_Item[nIdx].Create(g_szManual_Button[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_POWER_ON + nIdx);
		m_Btn_Item[nIdx].SetFont(&m_font);
	}
	m_Btn_Item[BT_MCTRL_IMAGE_LOAD].Create(g_szManual_Button[BT_MCTRL_IMAGE_LOAD], dwStyle | BS_PUSHLIKE | BS_AUTOCHECKBOX, rectDummy, this, IDC_BTN_POWER_ON + BT_MCTRL_IMAGE_LOAD);
	m_Btn_Item[BT_MCTRL_IMAGE_LOAD].SetFont(&m_font);
	m_Btn_Item[BT_MCTRL_IMAGE_LOAD].SetImage(IDB_UNCHECKED_16);
	m_Btn_Item[BT_MCTRL_IMAGE_LOAD].SetCheckedImage(IDB_CHECKED_16);
	m_Btn_Item[BT_MCTRL_IMAGE_LOAD].SizeToContent();
	m_Btn_Item[BT_MCTRL_IMAGE_LOAD].SetCheck(BST_UNCHECKED);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 19:39
// Desc.		:
//=============================================================================
void CWnd_ManualCtrl::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing = 0;
	int iCateSpacing = 3;
	int iLeft = iSpacing;
	int iTop = 0;
	int iWidth = cx;
	int iHeight = cy;
	int iTemp = 0;

	if ((BT_MCTRL_MAXNUM + 0) % 2 == 1)
	{
		iTemp = (BT_MCTRL_MAXNUM + 0) / 2 + 1;
	}
	else
	{
		iTemp = (BT_MCTRL_MAXNUM + 0) / 2;
	}
	
	//iTemp += 1;

	iWidth = (iWidth - iSpacing) / 2;
	iHeight = (iHeight - iCateSpacing * iTemp) / iTemp;

	m_Btn_Item[BT_MCTRL_POWER_ON].MoveWindow(iLeft, iTop, iWidth, iHeight);
	iLeft += iWidth + iSpacing;
	m_Btn_Item[BT_MCTRL_POWER_OFF].MoveWindow(iLeft, iTop, iWidth, iHeight);

// 	iTop += iHeight + iCateSpacing;
// 	iLeft = iSpacing;
// 
// 	m_Btn_Item[BT_MCTRL_IRLED_ON].MoveWindow(iLeft, iTop, iWidth, iHeight);
// 	iLeft += iWidth + iSpacing;
// 	m_Btn_Item[BT_MCTRL_IRLED_OFF].MoveWindow(iLeft, iTop, iWidth, iHeight);


	//iLeft += iWidth + iSpacing;
	//m_Btn_Item[BT_MCTRL_PIC_IMAGE_SAVE].MoveWindow(iLeft, iTop, iWidth, iHeight);
	
	//iTop += iHeight + iCateSpacing;
	//iLeft = iSpacing;
	//m_Btn_Item[BT_MCTRL_INITIALIZE].MoveWindow(iLeft, iTop, iWidth, iHeight);
	//iLeft += iWidth + iSpacing;
	//m_Btn_Item[BT_MCTRL_IMAGE_LOAD].MoveWindow(iLeft, iTop, iWidth, iHeight);

	iTop += iHeight + iCateSpacing;
	iLeft = iSpacing;
	m_Btn_Item[BT_MCTRL_LED_ON].MoveWindow(iLeft, iTop, iWidth, iHeight);
	iLeft += iWidth + iSpacing;
	m_Btn_Item[BT_MCTRL_LED_OFF].MoveWindow(iLeft, iTop, iWidth, iHeight);

	iTop += iHeight + iCateSpacing;
	iLeft = iSpacing;
	m_Btn_Item[BT_MCTRL_IR_ON].MoveWindow(iLeft, iTop, iWidth, iHeight);
	iLeft += iWidth + iSpacing;
	m_Btn_Item[BT_MCTRL_IR_OFF].MoveWindow(iLeft, iTop, iWidth, iHeight);

	iTop += iHeight + iCateSpacing;
	iLeft = iSpacing;
	//m_Btn_Item[BT_MCTRL_IMAGE_SAVE].MoveWindow(iLeft, iTop, iWidth * 2, iHeight);
	m_Btn_Item[BT_MCTRL_IMAGE_SAVE].MoveWindow(iLeft, iTop, iWidth, iHeight);
	iLeft += iWidth + iSpacing;
	m_Btn_Item[BT_MCTRL_IMAGE_LOAD].MoveWindow(iLeft, iTop, iWidth, iHeight);
}

//=============================================================================
// Method		: OnRangeCmds
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/1/14 - 17:48
// Desc.		:
//=============================================================================
void CWnd_ManualCtrl::OnRangeCmds(UINT nID)
{
	UINT nIndex = nID - IDC_BTN_POWER_ON;
	ClickOut(nIndex);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/29 - 19:39
// Desc.		:
//=============================================================================
BOOL CWnd_ManualCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: ClickOut
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/1/14 - 17:44
// Desc.		:
//=============================================================================
void CWnd_ManualCtrl::ClickOut(UINT nID)
{
	for (UINT nIdx = 0; nIdx < BT_MCTRL_MAXNUM; nIdx++)
	{
		m_Btn_Item[nIdx].EnableWindow(FALSE);
	}
	
	int nCheck = 0;

	if (nID == BT_MCTRL_IMAGE_LOAD)
	{
		nCheck = m_Btn_Item[BT_MCTRL_IMAGE_LOAD].GetCheck();
		GetOwner()->SendMessage(WM_MANUAL_CONTROL, nID, nCheck);
	}
	else
	{
		AfxGetApp()->GetMainWnd()->SendNotifyMessage(m_wm_ManualID, nID + 1, 0);
	}



	for (UINT nIdx = 0; nIdx < BT_MCTRL_MAXNUM; nIdx++)
	{
		m_Btn_Item[nIdx].EnableWindow(TRUE);
	}
}

void CWnd_ManualCtrl::ImageLoadMode(bool bMode)
{
	m_Btn_Item[BT_MCTRL_IMAGE_LOAD].SetCheck(bMode);
}