﻿#ifndef List_OperModeResult_h__
#define List_OperModeResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_OperModeResult
{
	OperModeResult_Object,
	OperModeResult_FpsCheck,
	OperModeResult_Signal,
	OperModeResult_Current,
	OperModeResult_MinSpec,
	OperModeResult_MaxSpec,
	OperModeResult_Result,
	OperModeResult_MaxCol
};

static LPCTSTR	g_lpszHeader_OperModeResult[] =
{
	_T(""),
	_T("Fps Check"),
	_T("Video Signal"),
	_T("Current"),
	_T("Min Spec"),
	_T("Max Spec"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_OperModeResultItem
{
	OperModeResult_15fps,
	OperModeResult_30fps,
	OperModeResult_ItemNum,
};


static LPCTSTR	g_lpszItem_OperModeResultItem[] =
{
	_T("15 FPS"),
	_T("30 FPS"),
	NULL
};


static LPCTSTR	g_lpszItem_OperModeResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_OperModeResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_OperModeResult[] =
{
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};
// List_OperModeInfo

class CList_OperModeResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_OperModeResult)

public:
	CList_OperModeResult();
	virtual ~CList_OperModeResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_OperMode(ST_LT_TI_OperationMode* pstOperMode)
	{
		if (pstOperMode == NULL)
			return;

		m_pstOperMode = pstOperMode;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_OperationMode*  m_pstOperMode;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_OperModeInfo_h__
