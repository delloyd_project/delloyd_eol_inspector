﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_LightCtrl.h
// Created	:	2016/10/31 - 20:17
// Modified	:	2016/10/31 - 20:17
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_LightCtrl_h__
#define Wnd_Cfg_LightCtrl_h__

#pragma once

#include "VGStatic.h"
#include "Wnd_BaseView.h"
#include "Def_DataStruct.h"
#include "Def_TestDevice.h"

enum enLight_Static{
	STI_LIT_CH1 = 0,
	STI_LIT_CH2,
	STI_LIT_CH3,
	STI_LIT_CH4,
	STI_LIT_CH5,
	STI_LIT_VOLT_CH1,
	STI_LIT_VOLT_CH2,
	STI_LIT_VOLT_CH3,
	STI_LIT_VOLT_CH4,
	STI_LIT_VOLT_CH5,
	STI_LIT_CURRENT_CH1,
	STI_LIT_CURRENT_CH2,
	STI_LIT_CURRENT_CH3,
	STI_LIT_CURRENT_CH4,
	STI_LIT_CURRENT_CH5,
	STI_LIT_MAXNUM,
};

static LPCTSTR	g_szLight_Static[] =
{
	_T("CENTER"),
	_T("LEFT"),
	_T("RIGHT"),
	_T("PARTICLE"),
	_T("CH5"),
	_T("VOLTAGE"),
	_T("VOLTAGE"),
	_T("VOLTAGE"),
	_T("VOLTAGE"),
	_T("VOLTAGE"),
	_T("STEP"),
	_T("STEP"),
	_T("STEP"),
	_T("STEP"),
	_T("STEP"),
	NULL
};

enum enLight_Edit{
	EIT_LIT_VOLT_CH1 = 0,
	EIT_LIT_VOLT_CH2,
	EIT_LIT_VOLT_CH3,
	EIT_LIT_VOLT_CH4,
	EIT_LIT_VOLT_CH5,
	EIT_LIT_STEP_CH1,
	EIT_LIT_STEP_CH2,
	EIT_LIT_STEP_CH3,
	EIT_LIT_STEP_CH4,
	EIT_LIT_STEP_CH5,
	EIT_LIT_MAXNUM,
};

static LPCTSTR	g_szLight_Button[] =
{
	_T("SET"),
	_T("SET"),
	_T("SET"),
	_T("SET"),
	_T("SET"),
	_T("APPLY"),
	_T("APPLY"),
	_T("APPLY"),
	_T("APPLY"),
	_T("APPLY"),
	NULL
};

enum enLight_Button{
	BTN_LIT_VOLT_CH1 = 0,
	BTN_LIT_VOLT_CH2,
	BTN_LIT_VOLT_CH3,
	BTN_LIT_VOLT_CH4,
	BTN_LIT_VOLT_CH5,
	BTN_LIT_STEP_CH1,
	BTN_LIT_STEP_CH2,
	BTN_LIT_STEP_CH3,
	BTN_LIT_STEP_CH4,
	BTN_LIT_STEP_CH5,
	BTN_LIT_MAXNUM,
};

enum enLight_Slider{
	SID_LIT_CH1 = 0,
	SID_LIT_CH2,
	SID_LIT_CH3,
	SID_LIT_CH4,
	SID_LIT_CH5,
	SID_LIT_MAXNUM,
};

class CWnd_Cfg_LightCtrl : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_LightCtrl)

public:
	CWnd_Cfg_LightCtrl();
	virtual ~CWnd_Cfg_LightCtrl();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnHScroll				(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void	OnRangeBtnVolt			(UINT nID);
	afx_msg void	OnRangeBtnStep			(UINT nID);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);

	CFont			m_font_Data;

	CVGStatic		m_st_Item[STI_LIT_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[EIT_LIT_MAXNUM];
	CSliderCtrl		m_sd_Item[SID_LIT_MAXNUM];
	CMFCButton		m_bn_Item[BTN_LIT_MAXNUM];

	ST_SlotVolt		m_stLightInfo;
	ST_Device*		m_pstDevice;

public:

	void SetLightInfo	(__in const ST_SlotVolt* pstSlotVolt);
	void GetLightInfo	(__out ST_SlotVolt& stSlotVolt);

	void SetPtr_Device	(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
	}
};

#endif // Wnd_Cfg_LightCtrl_h__
