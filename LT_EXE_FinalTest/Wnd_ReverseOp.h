﻿#ifndef Wnd_ReverseOp_h__
#define Wnd_ReverseOp_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"

#include "List_ReverseOp.h"
#include "List_ReverseSubOp.h"
#include "Def_DataStruct.h"

// CWnd_ReverseOp

enum enReverseButton
{
	BTN_REVERSE_TEST,
	BTN_REVERSE_RESET,
	BTN_REVERSE_MAX,
};

static LPCTSTR	g_szReverseButton[] =
{
	_T("TEST"),
	_T("RESET"),
	NULL
};
enum enReverseStatic
{
	STI_REVERSE_STATE,
	STI_REVERSE_ROI,
	STI_REVERSE_OPT,
	STI_REVERSE_MAX,
};

static LPCTSTR	g_szReverseStatic[] =
{
	_T("STATE"),
	_T("ROI LIST"),
	_T("OPTION LIST"),
	NULL
};

enum enREVERSECombo
{
	Com_REVERSE_STATE,

	Com_REVERSE_MAX,
};

static LPCTSTR g_szSatateType[] =
{
	//_T("오리지널"),
	//_T("좌우반전"),
	//_T("상하반전"),
	//_T("로테이션"),
	_T("Original"),
	_T("Horizontal"),
	_T("Vertical"),
	_T("Rotation"),
	NULL
};

enum enReverseComobox
{
	CMB_REVERSE_MAX = 1,
};

static LPCTSTR	g_szReverseCommoBox[] =
{
	NULL
};

class CWnd_ReverseOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_ReverseOp)

public:
	CWnd_ReverseOp();
	virtual ~CWnd_ReverseOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo		*m_pstModelInfo;

	CList_ReverseOp		m_List;
	CList_ReverseSubOp	m_ListSub;

	CFont				m_font;

	CButton				m_bn_Item[BTN_REVERSE_MAX];
	CComboBox			m_cb_Item[CMB_REVERSE_MAX];
	CComboBox			m_cb_State[Com_REVERSE_MAX];
	CVGStatic			m_st_Item[STI_REVERSE_MAX];
	

	// 검사 항목이 다수 인경우
	UINT				m_nTestItemCnt;

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_ReverseOp_h__
