﻿//*****************************************************************************
// Filename	: Def_DataStruct.h
// Created	: 2012/11/1
// Modified	: 2016/12/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef Def_DataStruct_h__
#define Def_DataStruct_h__

#include <afxwin.h>
#include "Def_Enum.h"
#include "Def_Test.h"
#include "Def_DataStruct_Cm.h"
#include "Def_MES_Cm.h"
#include "Def_Motion.h"
#include "Def_DigitiIO.h"
#include "Def_IndicatorSensor.h"
#include "Define_PCBLightBrd.h"
#include "Def_EVMS_Cm.h"
#include "Def_MES_Mcnex.h"

#include "List_Work_Total.h"
#include "List_Work_Angle.h"
#include "List_Work_CenterPoint.h"
#include "List_Work_Color.h"
#include "List_Work_Current.h"
#include "List_Work_Particle.h"
#include "List_Work_Rotate.h"
#include "List_Work_SFR.h"
#include "List_Work_FFT.h"
#include "List_Work_Brightness.h"
#include "List_Work_EIAJ.h"
#include "List_Work_Reverse.h"
#include "List_Work_ParticleManual.h"
#include "List_Work_PatternNoise.h"
#include "List_Work_IRFilter.h"
#include "List_Work_IRFilterManual.h"
#include "List_Work_LED.h"
#include "List_Work_Reset.h"
#include "List_Work_VideoSignal.h"
#include "List_Work_OperMode.h"
#include "List_Work_DefectPixel.h"
#pragma pack(push, 1)



//---------------------------------------------------------
// 검사 결과 저장용 구조체
//---------------------------------------------------------
typedef struct _tag_Worklist
{
	// 사용
	CList_Work_Total*			pList_Total;
	CList_Work_Current*			pList_Current[TICnt_Current];
	CList_Work_CenterPoint*		pList_CenterPt[TICnt_CenterPoint];
	CList_Work_Rotate*			pList_Rotate[TICnt_Rotation];
	CList_Work_EIAJ*			pList_EIAJ[TICnt_EIAJ];
	CList_Work_Angle*			pList_Angle[TICnt_FOV];
	CList_Work_Color*			pList_Color[TICnt_Color];
	CList_Work_Reverse*			pList_Reverse[TICnt_Reverse];
	CList_Work_ParticleManual*	pList_ParticleManual[TICnt_ParticleManual];
	//CList_Work_LED*				pList_LED[TICnt_LEDTest];
	//CList_Work_Reset*			pList_Reset[TICnt_Reset];
	//CList_Work_VideoSignal*		pList_VideoSignal[TICnt_VideoSignal];
	//CList_Work_OperMode*		pList_OperMode[TICnt_OperationMode];


	// 사용안함
	CList_Work_Particle*		pList_Particle[TICnt_BlackSpot];
	CList_Work_DefectPixel*		pList_DefectPixel[TICnt_DefectPixel];
	//CList_Work_SFR*				pList_SFR[TICnt_SFR];
	//CList_Work_FFT*				pList_FFT[TICnt_FFT];
	CList_Work_Brightness*		pList_Brightness[TICnt_Brightness];
	//CList_Work_PatternNoise*	pList_PatternNoise[TICnt_PatternNoise];
	CList_Work_IRFilter*		pList_IRFilter[TICnt_IRFilter];
	//CList_Work_IRFilterManual*	pList_IRFilterManual[TICnt_IRFilterManual];

	_tag_Worklist()
	{
		Reset();

	};

	void Reset()
	{
		pList_Total = NULL;

		for (int i = 0; i < TICnt_Current; i++)
			pList_Current[i] = NULL;
		
		for (int i = 0; i < TICnt_FOV; i++)
			pList_Angle[i] = NULL;

		for (int i = 0; i < TICnt_CenterPoint; i++)
			pList_CenterPt[i] = NULL;

		for (int i = 0; i < TICnt_Color; i++)
			pList_Color[i] = NULL;

		for (int i = 0; i < TICnt_BlackSpot; i++)
			pList_Particle[i] = NULL;

		for (int i = 0; i < TICnt_Rotation; i++)
			pList_Rotate[i] = NULL;

		//for (int i = 0; i < TICnt_SFR; i++)
		//	pList_SFR[i] = NULL;

		//for (int i = 0; i < TICnt_FFT; i++)
		//	pList_FFT[i] = NULL;

		for (int i = 0; i < TICnt_Brightness; i++)
			pList_Brightness[i] = NULL;

		for (int i = 0; i < TICnt_EIAJ; i++)
			pList_EIAJ[i] = NULL;

		for (int i = 0; i < TICnt_Reverse; i++)
			pList_Reverse[i] = NULL;

		for (int i = 0; i < TICnt_ParticleManual; i++)
			pList_ParticleManual[i] = NULL;
		
		for (int i = 0; i < TICnt_DefectPixel; i++)
			pList_DefectPixel[i] = NULL;

		//for (int i = 0; i < TICnt_PatternNoise; i++)
		//	pList_PatternNoise[i] = NULL;

		for (int i = 0; i < TICnt_IRFilter; i++)
			pList_IRFilter[i] = NULL;

		//for (int i = 0; i < TICnt_LEDTest; i++)
		//	pList_LED[i] = NULL;

		//for (int i = 0; i < TICnt_Reset; i++)
		//	pList_Reset[i] = NULL;

		//for (int i = 0; i < TICnt_VideoSignal; i++)
		//	pList_VideoSignal[i] = NULL;

		//for (int i = 0; i < TICnt_OperationMode; i++)
		//	pList_OperMode[i] = NULL;

	};

	_tag_Worklist& operator= (_tag_Worklist& ref)
	{
		pList_Total = ref.pList_Total;

		for (int i = 0; i < TICnt_Current; i++)
			pList_Current[i] = ref.pList_Current[i];

		for (int i = 0; i < TICnt_FOV; i++)
			pList_Angle[i] = ref.pList_Angle[i];

		for (int i = 0; i < TICnt_CenterPoint; i++)
			pList_CenterPt[i] = ref.pList_CenterPt[i];

		for (int i = 0; i < TICnt_Color; i++)
			pList_Color[i] = ref.pList_Color[i];

		for (int i = 0; i < TICnt_BlackSpot; i++)
			pList_Particle[i] = ref.pList_Particle[i];

		for (int i = 0; i < TICnt_Rotation; i++)
			pList_Rotate[i] = ref.pList_Rotate[i];

		//for (int i = 0; i < TICnt_SFR; i++)
		//	pList_SFR[i] = ref.pList_SFR[i];

		//for (int i = 0; i < TICnt_FFT; i++)
		//	pList_FFT[i] = ref.pList_FFT[i];

		for (int i = 0; i < TICnt_Brightness; i++)
			pList_Brightness[i] = ref.pList_Brightness[i];

		for (int i = 0; i < TICnt_EIAJ; i++)
			pList_EIAJ[i] = ref.pList_EIAJ[i];

		for (int i = 0; i < TICnt_Reverse; i++)
			pList_Reverse[i] = ref.pList_Reverse[i];

		for (int i = 0; i < TICnt_ParticleManual; i++)
			pList_ParticleManual[i] = ref.pList_ParticleManual[i];

		for (int i = 0; i < TICnt_DefectPixel; i++)
			pList_DefectPixel[i] = ref.pList_DefectPixel[i];

		//for (int i = 0; i < TICnt_PatternNoise; i++)
		//	pList_PatternNoise[i] = ref.pList_PatternNoise[i];

		for (int i = 0; i < TICnt_IRFilter; i++)
			pList_IRFilter[i] = ref.pList_IRFilter[i];

		//for (int i = 0; i < TICnt_LEDTest; i++)
		//	pList_LED[i] = ref.pList_LED[i];

		//for (int i = 0; i < TICnt_Reset; i++)
		//	pList_Reset[i] = ref.pList_Reset[i];

		//for (int i = 0; i < TICnt_VideoSignal; i++)
		//	pList_VideoSignal[i] = ref.pList_VideoSignal[i];

		//for (int i = 0; i < TICnt_OperationMode; i++)
		//	pList_OperMode[i] = ref.pList_OperMode[i];

		return *this;
	};

}ST_Worklist, *PST_Worklist;

//---------------------------------------------------------
// POGO 카운트, Beat 카운트
//---------------------------------------------------------
typedef struct _tag_PogoInfo
{
	CString		szPogoFilename;

	DWORD		dwCount_Max[MAX_SITE_CNT];
	DWORD		dwCount[MAX_SITE_CNT];

	DWORD		dwYear[MAX_SITE_CNT];
	DWORD		dwMonth[MAX_SITE_CNT];
	DWORD		dwDay[MAX_SITE_CNT];
	DWORD		dwHour[MAX_SITE_CNT];
	DWORD		dwMinute[MAX_SITE_CNT];
	DWORD		dwSecond[MAX_SITE_CNT];
	DWORD		dwMilliseconds[MAX_SITE_CNT];
	DWORD		dwDateCount[MAX_SITE_CNT];

	_tag_PogoInfo()
	{
		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			dwCount_Max[nIdx] = 50000;
			dwCount[nIdx] = 0;

			dwYear[nIdx] = 0;
			dwMonth[nIdx] = 0;
			dwDay[nIdx] = 0;
			dwHour[nIdx] = 0;
			dwMinute[nIdx] = 0;
			dwSecond[nIdx] = 0;
			dwMilliseconds[nIdx] = 0;
			dwDateCount[nIdx] = 0;
		}
	};

	_tag_PogoInfo& operator= (_tag_PogoInfo& ref)
	{
		szPogoFilename = ref.szPogoFilename;

		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			dwCount_Max[nIdx] = ref.dwCount_Max[nIdx];
			dwCount[nIdx] = ref.dwCount[nIdx];

			dwYear[nIdx] = ref.dwYear[nIdx];
			dwMonth[nIdx] = ref.dwMonth[nIdx];
			dwDay[nIdx] = ref.dwDay[nIdx];
			dwHour[nIdx] = ref.dwHour[nIdx];
			dwMinute[nIdx] = ref.dwMinute[nIdx];
			dwSecond[nIdx] = ref.dwSecond[nIdx];
			dwMilliseconds[nIdx] = ref.dwMilliseconds[nIdx];
			dwDateCount[nIdx] = ref.dwDateCount[nIdx];
		}

		return *this;
	};

	void ResetCount(UINT nPogoIdx)
	{
		if (nPogoIdx < MAX_SITE_CNT)
			dwCount[nPogoIdx] = 0;
	}

	BOOL IsMaxCount(UINT nPogoIdx)
	{
		if (nPogoIdx < MAX_SITE_CNT)
			return dwCount_Max[nPogoIdx] <= dwCount[nPogoIdx] ? TRUE : FALSE;
		else
			return FALSE;
	};

	BOOL IsDateMaxCount(UINT nPogoIdx)
	{
		SYSTEMTIME RegistrationDate;
		RegistrationDate.wYear =  (WORD) dwYear[nPogoIdx];
		RegistrationDate.wMonth = (WORD)dwMonth[nPogoIdx];
		RegistrationDate.wDay = (WORD)dwDay[nPogoIdx];
		RegistrationDate.wHour = (WORD)dwHour[nPogoIdx];
		RegistrationDate.wMinute = (WORD)dwMinute[nPogoIdx];
		RegistrationDate.wSecond = (WORD)dwSecond[nPogoIdx];
		RegistrationDate.wMilliseconds = (WORD)dwMilliseconds[nPogoIdx];

		SetLocalTime(&RegistrationDate);

		COleDateTimeSpan SpanTime(dwDateCount[nPogoIdx], 0, 0, 0);
		COleDateTime ConvTime(RegistrationDate);

		COleDateTime RegistrySpanDate = ConvTime + SpanTime;
		RegistrySpanDate.GetAsSystemTime(RegistrationDate);
		SetLocalTime(&RegistrationDate);

		SYSTEMTIME CurrentDate;
		GetLocalTime(&CurrentDate);

		if (CurrentDate.wYear >= RegistrationDate.wYear 
			&& CurrentDate.wMonth >= RegistrationDate.wMonth
			&& CurrentDate.wDay >= RegistrationDate.wDay)
		{
			return TRUE; 
		} 

		return FALSE;
	};

	void IncreasePogoCount(UINT nPogoIdx)
	{
		if (nPogoIdx < MAX_SITE_CNT)
			++dwCount[nPogoIdx];
	};

}ST_PogoInfo, PST_PogoInfo;


//---------------------------------------------------------
// Light Board, Volt, Current
//---------------------------------------------------------
typedef struct _tag_LightInfo
{
	float	fVolt[Slot_Max];
	WORD	wCode[Slot_Max];

	_tag_LightInfo()
	{
		for (UINT nIdx = 0; nIdx < Slot_Max; nIdx++)
		{
			fVolt[nIdx] = 0;
			wCode[nIdx] = 0;
		}
	};

	_tag_LightInfo& operator= (_tag_LightInfo& ref)
	{
		for (UINT nIdx = 0; nIdx < Slot_Max; nIdx++)
		{
			fVolt[nIdx] = ref.fVolt[nIdx];
			wCode[nIdx] = ref.wCode[nIdx];
		}

		return *this;
	};

}ST_LightInfo, PST_LightInfo;

//---------------------------------------------------------
// LOT 정보
//---------------------------------------------------------
typedef struct _tag_LOTInfo
{
	CString			szLotName;		// LOT 이름
	CString			szOperator;		// 작업자
	
	UINT			nLotCount;
	BOOL			bLotStatus;		// LOT 진행 상태

	SYSTEMTIME		StartTime;		// 전체 검사 시작 시간

	_tag_LOTInfo()
	{
		ZeroMemory(&StartTime, sizeof(SYSTEMTIME));
		nLotCount = 0;
		bLotStatus	= FALSE;
	};

	_tag_LOTInfo& operator= (_tag_LOTInfo& ref)
	{
		szLotName		= ref.szLotName;
		szOperator		= ref.szOperator;

		memcpy(&StartTime, &ref.StartTime, sizeof(SYSTEMTIME));

		nLotCount		= ref.nLotCount;
		bLotStatus		= ref.bLotStatus;

		return *this;
	};

	void Reset()
	{
		szLotName.Empty();
		szOperator.Empty();

		ZeroMemory(&StartTime, sizeof(SYSTEMTIME));

		nLotCount = 0;
		bLotStatus = FALSE;
	};

	virtual void SetLotInfo(__in LPCTSTR szInLOTNum, __in LPCTSTR szInOperator)
	{
		szLotName = szInLOTNum;
		szOperator = szInOperator;		
	};

}ST_LOTInfo, *PST_LOTInfo;


//---------------------------------------------------------
// 모델 설정값 구조체
//---------------------------------------------------------
typedef struct _tag_ModelInfo
{
	CString				szModelCode;							// 제품 Model 명칭
	CString				szModelFile;							// 모델 설정 파일
	CString				szMotorFile;							// 모델 설정 파일

	FLOAT				fVoltage[Def_CamBrd_Channel];			// Voltage
	UINT				nCurtainType;							// Curtain Type
	CString				szBinFile;								// Bin File
	
	CString				szI2CFile_1;								// I2C File
	CString				szI2CFile_2;								// I2C File
	BOOL				bI2CFile_1;								// I2C File
	BOOL				bI2CFile_2;								// I2C File

	CString				szWaveFile;								// Wave File
	CString				szPogoName;								// Pogo 설정 파일

	UINT				nCameraDelay;							// 카메라 안정화 시간
	UINT				nCameraDistortion;							// 카메라 안정화 시간

	BOOL				bUseOpenShort;

	DWORD				dwWidth;								// Width 
	DWORD				dwHeight;								// Height 
	UINT				nChannel;								// Channel
	
	UINT				nCameraType;							// 역상 상태
	UINT				nVideoType;								// NTSC, PAL
	UINT				nGrabType;								// 아날로그, 디지털 그래버
	double				dbPixelSize;
	double				dbTestDistance;
	double				dbTestAxisX;
	double				dbTestAxisY;
	
	UINT				nLotTryCnt;								// MES 차수
	CString				szLotID;								// MES LOT ID

	CArray <UINT, UINT&> TestItemz;								// 선택된 Test Itemz

	ST_StepInfo			stStepInfo;								// 전체 검사 스텝 정보

	ST_StepInfo			stCurrentStepInfo[TICnt_Current];		// 전류 검사 스텝 정보
	ST_StepInfo			stAngleStepInfo[TICnt_FOV];			// 화각 검사 스텝 정보
	ST_StepInfo			stCenterPtStepInfo[TICnt_CenterPoint];	// 중심점 검사 스텝 정보
	ST_StepInfo			stColorStepInfo[TICnt_Color];			// 컬러 검사 스텝 정보
	//ST_StepInfo			stFFTStepInfo[TICnt_FFT];				// 사운드 검사 스텝 정보
	ST_StepInfo			stParticleMAStepInfo[TICnt_ParticleManual];		// 이물 검사 스텝 정보
	ST_StepInfo			stBlackSpotStepInfo[TICnt_BlackSpot];		// 이물 검사 스텝 정보
	ST_StepInfo			stDefectPixelStepInfo[TICnt_DefectPixel];		// 이물 검사 스텝 정보
	ST_StepInfo			stBrightnessStepInfo[TICnt_Brightness];	// 광량비 검사 스텝 정보
	ST_StepInfo			stRotationStepInfo[TICnt_Rotation];		// 로테이션 검사 스텝 정보
	//ST_StepInfo			stSFRStepInfo[TICnt_SFR];				// SFR 검사 스텝 정보


	ST_SlotVolt			stLightInfo;	// 광원 보드 정보

	// Master Mode  [2/1/2019 Seongho.Lee]
	UINT 				nMasterSpcX;
	UINT				nMasterSpcY;
	double				dbMasterSpcR;
	
	int					iMasterInfoX;
	int					iMasterInfoY;
	double				dbMasterInfoR;

	UINT				nPicItem;

	UINT				nTestCnt;
	UINT				nTestMode;

	// 라벨 프린터
	ST_LabelPrinter	stPrinter;

	// 각 검사 항목별 설정
	ST_LT_TI_Current		stCurrent		[TICnt_Current			];
	//ST_LT_TI_LED			stLED			[TICnt_LEDTest			];
	ST_LT_TI_Angle			stAngle			[TICnt_FOV			];
	ST_LT_TI_CenterPoint	stCenterPoint	[TICnt_CenterPoint		];
	ST_LT_TI_Color			stColor			[TICnt_Color			];
	//ST_LT_TI_FFT			stFFT			[TICnt_FFT				];
	ST_LT_TI_DefectPixel	stDefectPixel	[TICnt_DefectPixel		 ];
	ST_LT_TI_BlackSpot		stBlackSpot		[TICnt_BlackSpot			];
	ST_LT_TI_Brightness		stBrightness	[TICnt_Brightness		];
	//ST_LT_TI_SFR			stSFR			[TICnt_SFR				];
	ST_LT_TI_Rotate			stRotate		[TICnt_Rotation			];
	ST_LT_TI_EIAJ			stEIAJ			[TICnt_EIAJ				];
	ST_LT_TI_Reverse		stReverse		[TICnt_Reverse			];
	ST_LT_TI_ParticleMA		stParticleMA	[TICnt_ParticleManual	];
	//ST_LT_TI_PatternNoise	stPatternNoise	[TICnt_PatternNoise		];
	ST_LT_TI_IRFilter		stIRFilter		[TICnt_IRFilter			];
	//ST_LT_TI_IRFilterMA		stIRFilterManual[TICnt_IRFilterManual	];
	//ST_LT_TI_VideoSignal	stVideoSignal	[TICnt_VideoSignal		];
	//ST_LT_TI_Reset			stReset			[TICnt_Reset			];
	//ST_LT_TI_OperationMode	stOperMode		[TICnt_OperationMode	];

	_tag_ModelInfo()
	{
		dbPixelSize		= 0.0;
		nCameraType		= CamState_Original;
		nVideoType		= VideoState_NTSC;
		nGrabType		= GrabType_NTSC;
		nCameraDistortion = 0;
		bUseOpenShort  = TRUE;

		bI2CFile_1 = FALSE;
		bI2CFile_2 = FALSE;

		for (int i = 0; i < Def_CamBrd_Channel; i++)
		{
			fVoltage[i] = 0.0f;
		}

		nCameraDelay	= 0;
		dwWidth			= 720;
		dwHeight		= 480;
		nChannel		= 3;
		nLotTryCnt		= 0 ;
		dbTestDistance = 0;
		dbTestAxisX = 0;
		dbTestAxisY = 0;

		stStepInfo.RemoveAll();
		
		for (int i = 0; i < TICnt_FOV; i++)
			stAngleStepInfo[i].RemoveAll();

		for (int i = 0; i < TICnt_CenterPoint; i++)
			stCenterPtStepInfo[i].RemoveAll();

		for (int i = 0; i < TICnt_Color; i++)
			stColorStepInfo[i].RemoveAll();

		for (int i = 0; i < TICnt_Current; i++)
			stCurrentStepInfo[i].RemoveAll();

		//for (int i = 0; i < TICnt_FFT; i++)
		//	stFFTStepInfo[i].RemoveAll();
		for (int i = 0; i < TICnt_DefectPixel ; i++)
			stDefectPixelStepInfo[i].RemoveAll();

		for (int i = 0; i < TICnt_BlackSpot; i++)
			stBlackSpotStepInfo[i].RemoveAll();

		for (int i = 0; i < TICnt_ParticleManual; i++)
			stParticleMAStepInfo[i].RemoveAll();

		for (int i = 0; i < TICnt_Brightness; i++)
			stBrightnessStepInfo[i].RemoveAll();

		for (int i = 0; i < TICnt_Rotation; i++)
			stRotationStepInfo[i].RemoveAll();

		//for (int i = 0; i < TICnt_SFR; i++)
		//	stSFRStepInfo[i].RemoveAll();

		stPrinter.Reset();
		nPicItem	= PIC_Standby;
		nTestCnt	= 0;
		nTestMode	= 0;

		nMasterSpcX = 0;
		nMasterSpcY = 0;
		dbMasterSpcR = 0.0;
		iMasterInfoX = 0;
		iMasterInfoY = 0;
		dbMasterInfoR = 0.0;

		Reset();
	};

	_tag_ModelInfo& operator= (_tag_ModelInfo& ref)
	{
		dbPixelSize		= ref.dbPixelSize;
		nCameraType		= ref.nCameraType;
		szModelCode		= ref.szModelCode;
		szModelFile		= ref.szModelFile;
		nCameraDistortion = ref.nCameraDistortion;
		bUseOpenShort = ref.bUseOpenShort;
		dbTestDistance = ref.dbTestDistance;
		dbTestAxisX = ref.dbTestAxisX;
		dbTestAxisY = ref.dbTestAxisY;

		for (int i = 0; i < Def_CamBrd_Channel; i++)
		{
			fVoltage[i] = ref.fVoltage[i];
		}

		nCameraDelay    = ref.nCameraDelay;
		szBinFile		= ref.szBinFile;
		szPogoName		= ref.szPogoName;
		szI2CFile_1		= ref.szI2CFile_1;
		szI2CFile_2		= ref.szI2CFile_2;
		bI2CFile_1		= ref.bI2CFile_1;
		bI2CFile_2		= ref.bI2CFile_2;

		szWaveFile		= ref.szWaveFile;
		dwWidth			= ref.dwWidth;
		dwHeight		= ref.dwHeight;
		nPicItem		= ref.nPicItem;
		nTestCnt		= ref.nTestCnt;
		nTestMode		= ref.nTestMode;
		nVideoType		= ref.nVideoType;
		nGrabType		= ref.nGrabType;
		nChannel		= ref.nChannel;

		nLotTryCnt		= ref.nLotTryCnt;
		szLotID			= ref.szLotID;
		szMotorFile		= ref.szMotorFile;
		stLightInfo		= ref.stLightInfo;
		stStepInfo		= ref.stStepInfo;

		nMasterSpcX		= ref.nMasterSpcX;
		nMasterSpcY		= ref.nMasterSpcY;
		dbMasterSpcR	= ref.dbMasterSpcR;
		iMasterInfoX	= ref.iMasterInfoX;	
		iMasterInfoY	= ref.iMasterInfoY;
		dbMasterInfoR	= ref.dbMasterInfoR;
		for (int i = 0; i < TICnt_FOV; i++)
			stAngleStepInfo[i] = ref.stAngleStepInfo[i];

		for (int i = 0; i < TICnt_CenterPoint; i++)
			stCenterPtStepInfo[i] = ref.stCenterPtStepInfo[i];

		for (int i = 0; i < TICnt_Color; i++)
			stColorStepInfo[i] = ref.stColorStepInfo[i];

		for (int i = 0; i < TICnt_Current; i++)
			stCurrentStepInfo[i] = ref.stCurrentStepInfo[i];

		//for (int i = 0; i < TICnt_FFT; i++)
		//	stFFTStepInfo[i] = ref.stFFTStepInfo[i];

		for (int i = 0; i < TICnt_BlackSpot; i++)
			stBlackSpotStepInfo[i] = ref.stBlackSpotStepInfo[i];

		for (int i = 0; i < TICnt_ParticleManual; i++)
			stParticleMAStepInfo[i] = ref.stParticleMAStepInfo[i];

		for (int i = 0; i < TICnt_DefectPixel; i++)
			stDefectPixelStepInfo[i] = ref.stDefectPixelStepInfo[i];

		for (int i = 0; i < TICnt_Brightness; i++)
			stBrightnessStepInfo[i] = ref.stBrightnessStepInfo[i];

		for (int i = 0; i < TICnt_Rotation; i++)
			stRotationStepInfo[i] = ref.stRotationStepInfo[i];

		//for (int i = 0; i < TICnt_SFR; i++)
		//	stSFRStepInfo[i] = ref.stSFRStepInfo[i];

		TestItemz.RemoveAll();
		TestItemz.Copy(ref.TestItemz);

		return *this;
	};

	void Reset()
	{
		nPicItem = PIC_Standby;
		nTestCnt = 0;
		nTestMode = 0;

		bI2CFile_1 = FALSE;
		bI2CFile_2 = FALSE;

		dbPixelSize = 0.0;
		nCameraType = CamState_Original;
		nVideoType = VideoState_NTSC;
		nGrabType = GrabType_NTSC;
		nCameraDistortion = 0;
		bUseOpenShort = TRUE;
		dbTestDistance = 0;
		dbTestAxisX = 0;
		dbTestAxisY = 0;

		szModelCode.Empty();
		szModelFile.Empty();

		for (int i = 0; i < Def_CamBrd_Channel; i++)
		{
			fVoltage[i] = 0.0f;
		}

		nCameraDelay = 0;
		szBinFile.Empty();
		szPogoName.Empty();
		szI2CFile_1.Empty();
		szI2CFile_2.Empty();
		szWaveFile.Empty();
		szMotorFile.Empty();

		TestItemz.RemoveAll();	
		dwWidth	= 720;
		dwHeight = 480;
		nChannel = 3;

		nLotTryCnt		= 0;
		szLotID.Empty();

		stStepInfo.RemoveAll();

		nMasterSpcX = 0;
		nMasterSpcY = 0;
		dbMasterSpcR = 0.0;

		iMasterInfoX = 0;
		iMasterInfoY = 0;
		dbMasterInfoR = 0.0;

		// reset
		for (int i = 0; i < TICnt_Current; i++)
			stCurrent[i].stCurrentResult.Reset();

		for (int i = 0; i < TICnt_CenterPoint; i++)
			stCenterPoint[i].stCenterPointResult.Reset();

		for (int i = 0; i < TICnt_Rotation; i++)
			stRotate[i].stRotateResult.Reset();

		for (int i = 0; i < TICnt_EIAJ; i++)
			stEIAJ[i].stEIAJData.Reset();

		for (int i = 0; i < TICnt_FOV; i++)
			stAngle[i].stAngleResult.Reset();

		for (int i = 0; i < TICnt_Color; i++)
			stColor[i].stColorResult.Reset();

		for (int i = 0; i < TICnt_Reverse; i++)
			stReverse[i].stReverseResult.Reset();

		for (int i = 0; i < TICnt_ParticleManual; i++)
			stParticleMA[i].stParticleResult.Reset();

		for (int i = 0; i < TICnt_BlackSpot; i++)
			stBlackSpot[i].stParticleResult.Reset();

		for (int i = 0; i < TICnt_DefectPixel; i++)
			stDefectPixel[i].stDefectPixelData.Reset();

		//for (int i = 0; i < TICnt_LEDTest; i++)
		//	stLED[i].stLEDCurrResult.Reset();

		//for (int i = 0; i < TICnt_VideoSignal; i++)
		//	stVideoSignal[i].Reset();

		//for (int i = 0; i < TICnt_Reset; i++)
		//	stReset[i].Reset();

		//for (int i = 0; i < TICnt_OperationMode; i++)
		//	stOperMode[i].Reset();

	};
}ST_ModelInfo, *PST_ModelInfo;

//---------------------------------------------------------
// 사이트별 검사 정보
//---------------------------------------------------------
typedef struct _tag_SiteInfo : public ST_SiteInfo_Base
{
	_tag_SiteInfo()
	{
		
	};

	_tag_SiteInfo& operator= (_tag_SiteInfo& ref)
	{
		__super::operator=(ref);


		return *this;
	};

	void Reset()
	{
		__super::Reset();
	};

	void SetResult(enTestResult ResultPara)
	{
		__super::SetResult(ResultPara);
		
	};
}ST_SiteInfo, *PST_SiteInfo;

//---------------------------------------------------------
// 프로그램에 사용되는 경로
//---------------------------------------------------------
typedef struct _tag_ProgramPath
{
	CString		szProgram;		// 프로그램 시작 경로
	CString		szLog;			// LOG 경로
	CString		szReport;		// 검사 결과 Report 경로
	CString		szModel;		// 모델 설정 파일 경로
	CString		szImage;		// 이미지 저장 경로
	CString		szPogo;			// 포고 설정 파일 저장 경로
	CString		szBin;			// Bin File 저장 경로
	CString		szI2C;			// I2C File 저장 경로
	CString		szWav;			// Wav File 저장 경로
	CString		szMotor;		// Motor File 저장 경로
	CString		szMes;			// Mes File 저장 경로
	CString		szMes2;			// Mes File 저장 경로
	CString		szBarcodeInfo;	// 표준 바코드 저장경로
	CString		szOperatorInfo;	// 작업자 등록 정보 저장 경로
	CString		szPrnFile;		// 라벨 프린터 경로
	CString		szMaintenance;	// Maintenance File 저장 경로

	CString		szInitIniFile;
	CString		szDefModelFile;
	CString		szDefaultEnv;

}ST_ProgramPath, *PST_ProgramPath;

//---------------------------------------------------------
// 검사 진행 시간 측정용 구조체
//---------------------------------------------------------
typedef struct _tag_TestTime : public ST_TestTime_Base
{
	void Reset()
	{
		__super::Reset();
	};
}ST_TestTime, *PST_TestTime;

//---------------------------------------------------------
// 검사 결과 저장용 구조체 (MES)
//---------------------------------------------------------
typedef struct _tag_MES_Worklist_Mcnex
{
	CString szEqpCode;	// 설비 코드 : MES 파일 읽어올 때 매칭.
	CString szMESPath;	// MES 경로 : 읽어올 MES 파일 경로.

	BOOL bStatus;
	ST_MES_TotalResult stMESResult;

	_tag_MES_Worklist_Mcnex()
	{
		bStatus = FALSE;
	};

	CString GetTextFileData(__in UINT nHeader)
	{
		CString szOutData;

		if (szMESPath.IsEmpty())
			return szOutData;

		if (szEqpCode.IsEmpty())
			return szOutData;

		CFileFind finder;
		CStdioFile file;
		CFileException e;

		CString szFileName;
		CString szFilePath = szMESPath + _T("\\") + _T("*.txt");

		BOOL bWorking = finder.FindFile(szFilePath);
		while (bWorking)
		{
			bWorking = finder.FindNextFile();

			if (finder.IsArchived())
			{
				szFileName = finder.GetFileName();
				if (szFileName == _T(".") || szFileName == _T("..") || szFileName == _T("Thumbs.db"))
					continue;

				szFileName = finder.GetFileTitle();
			}
		}

		finder.Close();

		if (szFileName.Find(szEqpCode) < 0)
			return szOutData;

		szFilePath = szMESPath + _T("\\") + szFileName + _T(".txt");

		if (file.Open(szFilePath, CFile::modeRead, &e))
		{
			CString szText;
			CString szSubText;

			if (file.ReadString(szText))
			{
				AfxExtractSubString(szSubText, szText, nHeader, _T(','));
				szOutData = szSubText;
			}

			file.Close();
		}

		return szOutData;
	};


	BOOL GetMESParameter(__in CString szEqpCode)
	{
		stMESResult.Reset();
		stMESResult.szLotNo = GetTextFileData(enHeader_EOL_LOT_NO);
		stMESResult.szLotCardNo = GetTextFileData(enHeader_EOL_LOT_CARD_NO);
		stMESResult.szChannel = GetTextFileData(enHeader_EOL_Channel);
		stMESResult.szEqpCode = GetTextFileData(enHeader_EOL_EqpCode);
		stMESResult.szItemNumber = GetTextFileData(enHeader_EOL_ItemNumber);
		stMESResult.szBarcode = GetTextFileData(enHeader_EOL_Barcode);
		stMESResult.szOperator = GetTextFileData(enHeader_EOL_Operator);
		stMESResult.szTestDate = GetTextFileData(enHeader_EOL_TestDate);
		stMESResult.szTestTime = GetTextFileData(enHeader_EOL_TestTime);

		if (szEqpCode != stMESResult.szEqpCode)
		{
			return bStatus = FALSE;
		} 

		return bStatus = TRUE;
	};

	void SetMESTestResult(__in ST_CamInfo stCamInfo)
	{
		CString szBuf;
		UINT nResult = 0;

		// 헤더 삽입
		for (int i = 0; i < enHeader_EOL_Item_Max; i++)
		{
			stMESResult.ItemHeaderz.Add(g_lpszHeader_EOL_Item_MES[i]);
		}

		// 아이템 결과 삽입
		// LOT_NO
		stMESResult.Itemz.Add(GetTextFileData(enHeader_EOL_LOT_NO));

		// LOT_CARD_NO
		stMESResult.Itemz.Add(GetTextFileData(enHeader_EOL_LOT_CARD_NO));

		// 채널번호
		stMESResult.Itemz.Add(GetTextFileData(enHeader_EOL_Channel));

		// 설비코드
		stMESResult.Itemz.Add(GetTextFileData(enHeader_EOL_EqpCode));

		// 지시번호
		stMESResult.Itemz.Add(GetTextFileData(enHeader_EOL_ItemNumber));

		// 품번
		stMESResult.Itemz.Add(GetTextFileData(enHeader_EOL_Barcode));

		// 작업자
		stMESResult.Itemz.Add(GetTextFileData(enHeader_EOL_Operator));

		// 처리일자
		stMESResult.Itemz.Add(GetTextFileData(enHeader_EOL_TestDate));

		// 처리일시
		stMESResult.Itemz.Add(GetTextFileData(enHeader_EOL_TestTime));

		// 최종결과
		nResult = stCamInfo.nJudgment;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);

		// 전류
		nResult = stCamInfo.stCurrent[0].stCurrentResult.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);

		// 전류 값
		szBuf.Format(_T("%.1f"), stCamInfo.stCurrent[0].stCurrentResult.iValue[0]);
		stMESResult.Itemz.Add(szBuf);

		//// 동작모드
		//nResult = stCamInfo.stOperMode[0].nResult;
		//if (nResult != 1) nResult = 0;
		//szBuf.Format(_T("%d"), nResult);
		//stMESResult.Itemz.Add(szBuf);
		//// 15 리셋
		//szBuf.Format(_T("%d"), stCamInfo.stOperMode[0].nResetResult[0]);
		//stMESResult.Itemz.Add(szBuf);
		//// 15 신호
		//szBuf.Format(_T("%d"), stCamInfo.stOperMode[0].nVideoResult[0]);
		//stMESResult.Itemz.Add(szBuf);
		//// 15 채크
		//szBuf.Format(_T("%d"), stCamInfo.stOperMode[0].nFPSResult[0]);
		//stMESResult.Itemz.Add(szBuf);
		//// 15 전류
		//szBuf.Format(_T("%.1f"), stCamInfo.stOperMode[0].stCurrResult[0].iValue[0]);
		//stMESResult.Itemz.Add(szBuf);
		//// 30 리셋
		//szBuf.Format(_T("%d"), stCamInfo.stOperMode[0].nResetResult[1]);
		//stMESResult.Itemz.Add(szBuf);
		//// 30 신호
		//szBuf.Format(_T("%d"), stCamInfo.stOperMode[0].nVideoResult[1]);
		//stMESResult.Itemz.Add(szBuf);
		//// 30 채크
		//szBuf.Format(_T("%d"), stCamInfo.stOperMode[0].nFPSResult[1]);
		//stMESResult.Itemz.Add(szBuf);
		//// 30 전류
		//szBuf.Format(_T("%.1f"), stCamInfo.stOperMode[0].stCurrResult[1].iValue[0]);
		//stMESResult.Itemz.Add(szBuf);

		// 중심점 검출 1
		nResult = stCamInfo.stCenterPoint[0].stCenterPointResult.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);
		// X
		szBuf.Format(_T("%d"), stCamInfo.stCenterPoint[0].stCenterPointResult.iResultOffsetX);
		stMESResult.Itemz.Add(szBuf);
		// Y
		szBuf.Format(_T("%d"), stCamInfo.stCenterPoint[0].stCenterPointResult.iResultOffsetY);
		stMESResult.Itemz.Add(szBuf);

		// 중심점 검출 2
		nResult = stCamInfo.stCenterPoint[1].stCenterPointResult.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);
		// X
		szBuf.Format(_T("%d"), stCamInfo.stCenterPoint[1].stCenterPointResult.iResultOffsetX);
		stMESResult.Itemz.Add(szBuf);
		// Y
		szBuf.Format(_T("%d"), stCamInfo.stCenterPoint[1].stCenterPointResult.iResultOffsetY);
		stMESResult.Itemz.Add(szBuf);

		// 로테이션
		nResult = stCamInfo.stRotate[0].stRotateResult.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);
		// 각도
		szBuf.Format(_T("%.2f"), stCamInfo.stRotate[0].stRotateResult.dbValue);
		stMESResult.Itemz.Add(szBuf);

		// 해상력 SFR
		//nResult = stCamInfo.stSFR[0].stSFRResult.nResult;
		//if (nResult != 1) nResult = 0;
		//szBuf.Format(_T("%d"), nResult);
		//stMESResult.Itemz.Add(szBuf);
		//// S1~S30
		//for (int i = 0; i < ROI_SFR_Max; i++)
		//{
		//	szBuf.Format(_T("%.2f"), stCamInfo.stSFR[0].stSFRResult.dbValue[i]);
		//	stMESResult.Itemz.Add(szBuf);
		//}

		// 화각
		nResult = stCamInfo.stAngle[0].stAngleResult.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);
		// 화각 결과
		for (int i = 0; i < IDX_AL_Max; i++)
		{
			szBuf.Format(_T("%.2f"), stCamInfo.stAngle[0].stAngleResult.dValue[i]);
			stMESResult.Itemz.Add(szBuf);
		}

		// 컬러
		nResult = stCamInfo.stColor[0].stColorResult.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);
		// 결과
		for (int i = 0; i < ROI_CL_Max; i++)
		{
			szBuf.Format(_T("%d"), stCamInfo.stColor[0].stColorResult.iRed[i]);
			stMESResult.Itemz.Add(szBuf);
			szBuf.Format(_T("%d"), stCamInfo.stColor[0].stColorResult.iGreen[i]);
			stMESResult.Itemz.Add(szBuf);
			szBuf.Format(_T("%d"), stCamInfo.stColor[0].stColorResult.iBlue[i]);
			stMESResult.Itemz.Add(szBuf);
		}

		// 역상
		nResult = stCamInfo.stReverse[0].stReverseResult.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);
// 		// 반전상태
// 		szBuf.Format(_T("%s"), g_szcamstate[stCamInfo.stReverse[0].stReverseResult.nCamState]);
// 		stMESResult.Itemz.Add(szBuf);

		// 이물
		nResult = stCamInfo.stParticleMA[0].stParticleResult.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);

		// 이물
		nResult = stCamInfo.stParticle[0].stParticleResult.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);

		// 이물
		nResult = stCamInfo.stDefectPixel[0].stDefectPixelData.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);

		// LED
		//nResult = stCamInfo.stLED[0].stLEDCurrResult.nResult;
		//if (nResult != 1) nResult = 0;
		//szBuf.Format(_T("%d"), nResult);
		//stMESResult.Itemz.Add(szBuf);
		//szBuf.Format(_T("%.1f"), stCamInfo.stLED[0].stLEDCurrResult.iValue[0]);
		//stMESResult.Itemz.Add(szBuf);

		// 광량비
		nResult = stCamInfo.stBrightness[0].stBrightnessResult.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);
		for (int i = ROI_BR_Side_1; i < ROI_BR_Max; i++)
		{
			szBuf.Format(_T("%.2f"), stCamInfo.stBrightness[0].stBrightnessResult.dbValue[i]);
			stMESResult.Itemz.Add(szBuf);
		}

		// 저조도
		//nResult = stCamInfo.stPatternNoise[0].stPatternNoiseResult.nResult;
		//if (nResult != 1) nResult = 0;
		//szBuf.Format(_T("%d"), nResult);
		//stMESResult.Itemz.Add(szBuf);
		//for (int i = ROI_PN_S1; i < ROI_PN_MaxEnum; i++)
		//{
		//	szBuf.Format(_T("%.2f"), stCamInfo.stPatternNoise[0].stPatternNoiseResult.dValue_dB[i]);
		//	stMESResult.Itemz.Add(szBuf);
		//}

		// IR필터
		nResult = stCamInfo.stIRFilter[0].stIRFilterResult.nResult;
		if (nResult != 1) nResult = 0;
		szBuf.Format(_T("%d"), nResult);
		stMESResult.Itemz.Add(szBuf);
		// RED GAIN
		szBuf.Format(_T("%.2f"), stCamInfo.stIRFilter[0].stIRFilterResult.dValue);
		stMESResult.Itemz.Add(szBuf);

	};



}ST_MES_Worklist_Mcnex, *PST_MES_Worklist_Mcnex;


//---------------------------------------------------------
// 유지보수 설정값 구조체
//---------------------------------------------------------
typedef struct _tag_MaintenanceInfo
{
	//enInsptrSysType		nInspectionType;

	CString				szMaintenanceFile;
	CString				szMaintenanceFullPath;
	CString				szMotorFile;
	//CString				szLightFile;

	//ST_LightInfo		stLightInfo;
	ST_TeachInfo		stTeachInfo;	// 티칭 위치 설정정보

	_tag_MaintenanceInfo()
	{
		//nInspectionType = enInsptrSysType::Sys_Focusing;
	};

	_tag_MaintenanceInfo& operator= (_tag_MaintenanceInfo& ref)
	{
		szMaintenanceFile = ref.szMaintenanceFile;
		szMaintenanceFullPath = ref.szMaintenanceFullPath;
		szMotorFile = ref.szMotorFile;
		//	szLightFile = ref.szLightFile;
		//	stLightInfo = ref.stLightInfo;
		stTeachInfo = ref.stTeachInfo;
		return *this;
	};

	void Reset()
	{
		szMaintenanceFile.Empty();
		szMaintenanceFullPath.Empty();
		szMotorFile.Empty();
		//	szLightFile.Empty();
		//	stLightInfo.Reset();
	};

	//void SetSystemType(__in enInsptrSysType nSysType)
	//{
	//nInspectionType = nSysType;
	//};

}ST_MaintenanceInfo, *PST_MaintenanceInfo;

//---------------------------------------------------------
// 전체 검사에 관련된 데이터 기록용 구조체
//---------------------------------------------------------
typedef struct _tag_InspectionInfo : public ST_InspectionInfo_Base
{
	ST_CamInfo				CamInfo;			// Camera 검사 정보
	ST_SiteInfo				SiteInfo;			// 사이트 검사 정보

	ST_ModelInfo			ModelInfo;			// 모델 정보
	ST_LOTInfo				LotInfo;			// LOT 정보
	ST_MES_Worklist_Mcnex	MESInfo;			// MES 정보
	ST_Worklist				WorklistInfo;		// 검사 결과 워크리스트
	ST_Yield				YieldInfo;			// 수율
	ST_ProgramPath			Path;				// 프로그램에 사용되는 폴더
	ST_TestTime				Time;				// 검사 진행 시간	
	ST_PogoInfo				PogoInfo;			// 포고 카운트, 비트 카운트
	ST_CycleTime			CycleTime;			// 검사 진행 시간

	ST_BarcodeRefInfo		stBarcodeInfo;		// 바코드 정보
	ST_UserConfigInfo		stUserConfigInfo;	// 작업자 등록 정보

	ST_MaintenanceInfo		MaintenanceInfo;				// 유지 정보

	CString					szRecvLotID;

	UINT					nRecvLotTryCnt;

	BOOL					bUseMasterCheck;
	BOOL					bUseBarcode;
	BOOL					bUseFailBox;
	BOOL					bUsePrinter;
	BOOL					bUseArea;
	BOOL					bUseDoor;
	UINT					nImageSaveType;
	CString					szEqpCode;			// 표준 바코드 구분자 정보
	CString					szBarcodeRef;		// 표준 바코드 구분자 정보
	CString					szBarcodeBuf;		// 바코드 수신시 저장용 버퍼 (1 소켓)

	// 시업 점검
	BOOL m_bMasterCheck;
// 	BOOL bStartUpCheckProc;
// 	BOOL bStartUpCheckPass;
// 	BOOL bStartUpCheckFail;
// 	UINT nNGTestItemMaster[TIID_MaxEnum];
// 	UINT nNGTestItemType[TIID_MaxEnum];
	
	// 영상 신호 상태
	BOOL				bVideoSignal[MAX_MODULE_CNT];

	// 이전 제품 배출 시간
	DWORD				dwPrevOutputTime;
	// 현재 Tact Time
	DWORD				dwTactTime;

	// Digital In 신호 
	BYTE				byDIO_DI[MAX_DIGITAL_IO];
	BYTE				byDIO_DO[MAX_DIGITAL_IO];
	DWORD64				dwDI;
	DWORD64				dwDO;

	_tag_InspectionInfo()
	{
		for (UINT nUnitIdx = 0; nUnitIdx < MAX_MODULE_CNT; nUnitIdx++)
		{
			bVideoSignal[nUnitIdx] = FALSE;
		}

		Judgment_All		= TR_Empty;
		dwPrevOutputTime	= 0;
		dwTactTime			= 0;
		bUseBarcode			= FALSE;
		bUseFailBox = FALSE;

		memset(byDIO_DI, 0, MAX_DIGITAL_IO);
		memset(byDIO_DO, 0, MAX_DIGITAL_IO);

		m_bMasterCheck = FALSE;

// 		bStartUpCheckPass = FALSE;
// 		bStartUpCheckFail = FALSE;
// 
// 		for (int i = 0; i < TIID_MaxEnum; i++)
//		{
//			nNGTestItemMaster[i] = 0;
// 			nNGTestItemType[i] = 0;
//		}

	};
	
	void ResetCamInfo()	// 전체 카메라 정보 초기화
	{
		CamInfo.Reset();
		SiteInfo.Reset();

		WorklistInfo.Reset();
		Judgment_All	= TR_Empty;
	};
	
	void SetInformation_Input (__in LPCTSTR szBarcode)
	{
		CamInfo.szLotID			= szLotName;
		CamInfo.szBarcode		= szBarcode;		
		CamInfo.szModelName		= szModelName;
		CamInfo.szOperatorName	= szOperatorName;
	};

	DWORD SetTactTime(__in DWORD dwOutputTime)
	{
		if (0 != dwPrevOutputTime)
		{
			if (dwPrevOutputTime < dwOutputTime)
			{
				dwTactTime = dwOutputTime - dwPrevOutputTime;
			}
			else
			{
				dwTactTime = 0xFFFFFFFF - dwPrevOutputTime + dwOutputTime;
			}

			dwPrevOutputTime = dwOutputTime;
		}
		else
		{
			dwTactTime = 0;
			dwPrevOutputTime = dwOutputTime;
		}

		return dwTactTime;
	};

	virtual void SetLotInfo(__in LPCTSTR szInLOTNum, __in LPCTSTR szInOperator)
	{
		__super::SetLotInfo(szInLOTNum, szInOperator);
		szLotName = szInLOTNum;
		szOperatorName = szInOperator;
		LotInfo.SetLotInfo(szInLOTNum, szInOperator);		
	};

	void SetLotStatus(__in BOOL bStart)
	{
		LotInfo.bLotStatus = bStart;
	};

	void SetLotName(__in LPCTSTR szInLOTNum)
	{
		szLotName = szInLOTNum;
		LotInfo.szLotName = szInLOTNum;
	};

	void ResetLotName()
	{
		szLotName.Empty();
		LotInfo.szLotName.Empty();
	};

	void SetInputTime()
	{
		GetLocalTime(&Time.tmStart_All);
		Time.dwStart_All = timeGetTime();

		CamInfo.SetInputTime(Time.tmStart_All, Time.dwStart_All);		
	};

	void SetOutputTime()
	{
		GetLocalTime(&Time.tmEnd_All);
		Time.dwDuration_All = timeGetTime();
		dwTactTime = SetTactTime(Time.dwDuration_All);

		CamInfo.SetOutputTime(Time.tmEnd_All, Time.dwDuration_All);				
		CamInfo.dwTactTime = dwTactTime;
	};

	void ResetBarcodeBuffer()
	{
		szBarcodeBuf.Empty();
		//szarBarcodeBuf.RemoveAll();
	};
}ST_InspectionInfo, *PST_InspectionInfo;

typedef struct _tag_ImageMode
{
	enImageMode eImageMode;
	BOOL bImageSaveMode;
	CString szImageCommonPath;
	CString szImagePath;

	_tag_ImageMode()
	{
		eImageMode = ImageMode_LiveCam;
	};

	_tag_ImageMode& operator= (_tag_ImageMode& ref)
	{
		bImageSaveMode = ref.bImageSaveMode;
		szImageCommonPath = ref.szImageCommonPath;
		eImageMode = ref.eImageMode;
		szImagePath = ref.szImagePath;
		return *this;
	};
}ST_ImageMode, *PST_ImageMode;

#pragma pack (pop)

#endif // Def_DataStruct_h__
