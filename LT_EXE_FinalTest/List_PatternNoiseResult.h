﻿#ifndef List_PatternNoiseResult_h__
#define List_PatternNoiseResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_PatternNoiseResult
{
	PatternNoiseResult_Object = 0,
	PatternNoiseResult_Value,
	PatternNoiseResult_dBThr,
	PatternNoiseResult_Result,
	PatternNoiseResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_PatternNoiseResult[] =
{
	_T(""),
	_T("Value"),
	_T("dB Threshold"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_PatternNoiseResult
{
	PatternNoiseResult_ItemNum = ROI_PN_MaxEnum,
};

static LPCTSTR	g_lpszItem_PatternNoiseResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_PatternNoiseResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_PatternNoiseResult[] =
{
	80,
	80,
	80,
	80,
};
// List_PatternNoiseInfo

class CList_PatternNoiseResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_PatternNoiseResult)

public:
	CList_PatternNoiseResult();
	virtual ~CList_PatternNoiseResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_PatternNoise(ST_LT_TI_PatternNoise* pstPatternNoise)
	{
		if (pstPatternNoise == NULL)
			return;

		m_pstPatternNoise = pstPatternNoise;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_PatternNoise*  m_pstPatternNoise;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_PatternNoiseInfo_h__
