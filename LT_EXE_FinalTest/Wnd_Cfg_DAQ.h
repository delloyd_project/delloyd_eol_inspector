﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_DAQ.h
// Created	:	2016/3/14 - 10:56
// Modified	:	2016/3/14 - 10:56
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_DAQ_h__
#define Wnd_Cfg_DAQ_h__

#pragma once
#include "Wnd_BaseView.h"
#include "Def_Enum.h"
#include "VGStatic.h"
#include "Def_DataStruct.h"

enum enDAQ_Static{
	STI_DAQ_Cam_BoardNum = 0,
	STI_DAQ_Cam_Data_Type,
	STI_DAQ_Cam_Signal_Type,
	STI_DAQ_Cam_MIPI_Lane,
	STI_DAQ_Cam_Sensor_Type,
	STI_DAQ_Cam_Color_Array,
	STI_DAQ_Cam_Width_Multiple,
	STI_DAQ_Cam_Height_Multiple,
	STI_DAQ_Cam_Hsync_Polarity,
	STI_DAQ_Cam_Pclk_Polarity,
	STI_DAQ_Cam_DVal_Use,
	STI_DAQ_Cam_Clock_Select,
	STI_DAQ_Cam_Clock_Hz,
	STI_DAQ_Cam_Clock_Use,
	STI_DAQ_Cam_VideoCut,
	STI_DAQ_Cam_Exposure,
	STI_DAQ_MAXNUM,
};
static LPCTSTR	g_szDAQ_Static[] =
{
	_T("Board Number"),
	_T("Data Type"),
	_T("Signal Type"),
	_T("MIPI Lane"),
	_T("Sensor Type"),
	_T("Color Array"),
	_T("Width Multiple"),
	_T("Height Multiple"),
	_T("Hsync Polarity"),
	_T("PClK Polarity"),
	_T("DVal Use"),
	_T("Clock Select"),
	_T("Clock Set (Hz)"),
	_T("Clock Use"),
	_T("Video Cut"),
	_T("Exposure"),
	NULL
};

enum enDAQ_Combobox{
	CBO_DAQ_BoardNumber = 0,
	CBO_DAQ_DataType,
	CBO_DAQ_SignalType,
	CBO_DAQ_MIPILane,
	CBO_DAQ_SensorType,
	CBO_DAQ_ColorArray,
	CBO_DAQ_HsyncPolarity,
	CBO_DAQ_PClkPolarity,
	CBO_DAQ_DValUse,
	CBO_DAQ_ClockSelect,
	CBO_DAQ_ClockUse,
	//CBO_DAQ_I2CFile,
	CBO_DAQ_VideoCut,
	CBO_DAQ_MAXNUM,
};

enum enDAQ_Edit{
	EDT_DAQ_WidthMultiple = 0,
	EDT_DAQ_HeightMultiple,
	EDT_DAQ_ClockHz,
	EDT_DAQ_Exposure,
	EDT_DAQ_MAXNUM,
};

//-----------------------------------------------------------------------------
// CWnd_Cfg_DAQ
//-----------------------------------------------------------------------------
class CWnd_Cfg_DAQ : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_DAQ)

public:
	CWnd_Cfg_DAQ();
	virtual ~CWnd_Cfg_DAQ();

protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);

	CFont			m_font_Data;

	CVGStatic		m_st_Item[STI_DAQ_MAXNUM];
	CComboBox		m_cd_Item[CBO_DAQ_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[EDT_DAQ_MAXNUM];

	void InitUI();

	void GetUIData(__out ST_LVDSInfo& stLVDSInfo);
	void SetUIData(__in const ST_LVDSInfo* pstLVDSInfo);

	void GetModelInfo(__out ST_ModelInfo& stModelInfo);
	void SetModelInfo(__in const ST_ModelInfo* pstModelInfo);

public:
	
};

#endif // Wnd_Cfg_DAQ_h__


