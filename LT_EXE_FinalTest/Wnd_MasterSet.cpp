﻿// Wnd_Wnd_MasterSet.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_MasterSet.h"
#include "resource.h"

// CWnd_MasterSet

IMPLEMENT_DYNAMIC(CWnd_MasterSet, CWnd)

typedef enum MasterSet_ID
{
	IDC_BTN_TEST = 1001,
	IDC_BTN_SAVE,
	IDC_BTN_EXIT,
	IDC_ED_MasterInfoX,
	IDC_ED_MasterInfoY,
 	IDC_ED_MasterInfoR,
	IDC_ED_MasterSpcX,
	IDC_ED_MasterSpcY,
 	IDC_ED_MasterSpcR,
	IDC_ED_MasterOffsetX,
	IDC_ED_MasterOffsetY,
 	IDC_ED_MasterOffsetR,
};

CWnd_MasterSet::CWnd_MasterSet()
{
	VERIFY(m_font_Default.CreateFont(
		25,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename}

	
	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename}
}

CWnd_MasterSet::~CWnd_MasterSet()
{
	m_font.DeleteObject();
	m_font_Default.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_MasterSet, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BTN_TEST, OnBnClickedTest)
	ON_BN_CLICKED(IDC_BTN_SAVE, OnBnClickedSave)
	ON_BN_CLICKED(IDC_BTN_EXIT, OnBnClickedExit)
END_MESSAGE_MAP()

// CWnd_MasterSet 메시지 처리기입니다.
int CWnd_MasterSet::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_bn_MasterTest.Create(_T("MASTER TEST"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_TEST);
	m_bn_MasterTest.SetFont(&m_font);
	
	m_bn_MasterSave.Create(_T("MASTER SAVE"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_SAVE);
	m_bn_MasterSave.SetFont(&m_font);
	
	m_bn_MasterExit.Create(_T("MASTER EXIT"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_EXIT);
	m_bn_MasterExit.SetFont(&m_font);
	
	m_st_UIItem.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_UIItem.SetColorStyle(CVGStatic::ColorStyle_Default);

	m_st_UIItem.SetFont_Gdip(L"Arial", 9.0F);
	m_st_UIItem.Create(_T("MASTER\n DATA") , dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < STI_MT_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);

		if (nIdx < STI_MT_MasterInfo)
			m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		else
			m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].Create(g_szMasterSet_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < EIT_MT_MAXNUM; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_MasterInfoX + nIdx);
		m_ed_Item[nIdx].SetFont(&m_font_Default);
		m_ed_Item[nIdx].SetValidChars(_T("0123456789.-"));
		m_ed_Item[nIdx].SetWindowText(_T("0"));
	}

	m_Group.SetTitle(L"MASTER INFO");

	if (!m_Group.Create(_T("MASTER INFO"), WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, 20))
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	m_bn_MasterTest.EnableWindow(FALSE);
	m_bn_MasterSave.EnableWindow(FALSE);

	for (UINT nIdx = 0; nIdx < EIT_MT_MAXNUM; nIdx++)
		m_ed_Item[nIdx].EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/2/28 - 13:25
// Desc.		:
//=============================================================================
void CWnd_MasterSet::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin = 10;
	int iSpacing = 5;

	int iLeft = iMagrin;
	int iTop = 26;

	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	int iStWidth = iWidth / 4;
	int iBtnWidth = iWidth / 7;
	int iBtnHeight = iHeight / 5.5;
	//int iBtnHeight = 20 * 1.8;

	int List_W = iMagrin;
	
	m_Group.MoveWindow(0, 0, cx, cy);

	m_st_UIItem.MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

	iLeft += iStWidth + 1;
	m_st_Item[STI_MT_X].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

	iLeft += iStWidth + 1;
	m_st_Item[STI_MT_Y].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

	iLeft += iStWidth + 1;
	m_st_Item[STI_MT_R].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

	for (UINT nIdx = 0; nIdx < 3; nIdx++)
	{
		iTop += iBtnHeight + 1;
		iLeft = iMagrin;
		m_st_Item[STI_MT_MasterInfo + nIdx].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

		iLeft += iStWidth + 1;
		m_ed_Item[EIT_MT_MasterInfoX + nIdx * 3].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

		iLeft += iStWidth + 1;
		m_ed_Item[EIT_MT_MasterInfoY + nIdx * 3].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

		iLeft += iStWidth + 1;
		m_ed_Item[EIT_MT_MasterInfoR + nIdx * 3].MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);
	}
	
	iStWidth = iWidth / 3;

	iTop += iBtnHeight + 1;
	iLeft = iMagrin;
	m_bn_MasterTest.MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);
	
	iLeft += iStWidth + 2;
	m_bn_MasterSave.MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);

	iLeft += iStWidth + 2;
	m_bn_MasterExit.MoveWindow(iLeft, iTop, iStWidth, iBtnHeight);
}

//=============================================================================
// Method		: OnBnClickedTest
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/11 - 16:33
// Desc.		:
//=============================================================================
void CWnd_MasterSet::OnBnClickedTest()
{
	CString strValue;

	m_ed_Item[EIT_MT_MasterInfoX].GetWindowText(strValue);
	m_pstModelInfo->iMasterInfoX = _ttoi(strValue);

	m_ed_Item[EIT_MT_MasterInfoY].GetWindowText(strValue);
	m_pstModelInfo->iMasterInfoY = _ttoi(strValue);

	m_ed_Item[EIT_MT_MasterInfoR].GetWindowText(strValue);
	m_pstModelInfo->dbMasterInfoR = _ttof(strValue);

	m_bn_MasterExit.EnableWindow(FALSE);
	GetOwner()->SendNotifyMessage(WM_MASTER_SET, 0, 0);
	m_bn_MasterExit.EnableWindow(TRUE);
}

//=============================================================================
// Method		: OnBnClickedSave
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/11 - 16:33
// Desc.		:
//=============================================================================
void CWnd_MasterSet::OnBnClickedSave()
{
	GetUIData();
	GetOwner()->SendNotifyMessage(WM_MODEL_SAVE, 0, 0);
}

//=============================================================================
// Method		: OnBnClickedExit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/11 - 16:33
// Desc.		:
//=============================================================================
void CWnd_MasterSet::OnBnClickedExit()
{
	GetOwner()->SendNotifyMessage(WM_CHANGE_MODE, MASTER_End, 0);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_MasterSet::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_MasterSet::PreTranslateMessage(MSG* pMsg)
{
	return CWnd::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: GetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/28 - 19:39
// Desc.		:
//=============================================================================
void CWnd_MasterSet::GetUIData()
{
	CString strValue;
	int iOffsetValue;
	double dbOffsetValue;
	
	m_ed_Item[EIT_MT_MasterSpcX].GetWindowText(strValue);
	m_pstModelInfo->nMasterSpcX = _ttoi(strValue);
	
	m_ed_Item[EIT_MT_MasterSpcY].GetWindowText(strValue);
	m_pstModelInfo->nMasterSpcY = _ttoi(strValue);

	m_ed_Item[EIT_MT_MasterSpcR].GetWindowText(strValue);
	m_pstModelInfo->dbMasterSpcR = _ttof(strValue);

	m_ed_Item[EIT_MT_MasterOffsetX].GetWindowText(strValue);
	iOffsetValue = _ttoi(strValue);
	m_pstModelInfo->stCenterPoint->stCenterPointOpt.nRefAxisX -= iOffsetValue;

	m_ed_Item[EIT_MT_MasterOffsetY].GetWindowText(strValue);
	iOffsetValue = _ttoi(strValue);
	m_pstModelInfo->stCenterPoint->stCenterPointOpt.nRefAxisY -= iOffsetValue;

	m_ed_Item[EIT_MT_MasterOffsetR].GetWindowText(strValue);
	dbOffsetValue = _ttof(strValue);
	m_pstModelInfo->stRotate->stRotateOpt.dbMasterDegree -= dbOffsetValue;
}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/11 - 16:51
// Desc.		:
//=============================================================================
void CWnd_MasterSet::SetUIData()
{
	CString strValue;

	strValue.Format(_T("%d"), m_pstModelInfo->iMasterInfoX);
	m_ed_Item[EIT_MT_MasterInfoX].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstModelInfo->iMasterInfoY);
	m_ed_Item[EIT_MT_MasterInfoY].SetWindowText(strValue);

	strValue.Format(_T("%.1f"), m_pstModelInfo->dbMasterInfoR);
	m_ed_Item[EIT_MT_MasterInfoR].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstModelInfo->nMasterSpcX);
	m_ed_Item[EIT_MT_MasterSpcX].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstModelInfo->nMasterSpcY);
	m_ed_Item[EIT_MT_MasterSpcY].SetWindowText(strValue);

	strValue.Format(_T("%.2f"), m_pstModelInfo->dbMasterSpcR);
	m_ed_Item[EIT_MT_MasterSpcR].SetWindowText(strValue);
}

//=============================================================================
// Method		: SetMasterData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/11 - 17:20
// Desc.		:
//=============================================================================
void CWnd_MasterSet::SetMasterData(__in int iOffsetX, __in int iOffsetY, __in double dbDegree)
{
	CString strValue;

	strValue.Format(_T("%d"), iOffsetX);
	m_ed_Item[EIT_MT_MasterOffsetX].SetWindowText(strValue);

	strValue.Format(_T("%d"), iOffsetY);
	m_ed_Item[EIT_MT_MasterOffsetY].SetWindowText(strValue);

	strValue.Format(_T("%.2f"), dbDegree);
	m_ed_Item[EIT_MT_MasterOffsetR].SetWindowText(strValue);
}

//=============================================================================
// Method		: PermissionMode
// Access		: public  
// Returns		: void
// Parameter	: enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2018/3/5 - 16:58
// Desc.		:
//=============================================================================
void CWnd_MasterSet::PermissionMode(enPermissionMode InspMode)
{
	switch (InspMode)
	{
	case Permission_Operator:
		m_bn_MasterTest.EnableWindow(FALSE);
		m_bn_MasterSave.EnableWindow(FALSE);

		for (UINT nIdx = 0; nIdx < EIT_MT_MAXNUM; nIdx++)
			m_ed_Item[nIdx].EnableWindow(FALSE);
		break;

	case Permission_Manager:
	case Permission_Administrator:
	case Permission_Engineer:
	case Permission_CNC:
		m_bn_MasterTest.EnableWindow(TRUE);
		m_bn_MasterSave.EnableWindow(TRUE);

		for (UINT nIdx = 0; nIdx < EIT_MT_MAXNUM; nIdx++)
			m_ed_Item[nIdx].EnableWindow(TRUE);
		break;

	default:
		break;
	}
}
