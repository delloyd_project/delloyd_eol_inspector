﻿// List_OperModeOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_OperModeOp.h"

#define IOperOp_ED_CELLEDIT		5001

// CList_OperModeOp

IMPLEMENT_DYNAMIC(CList_OperModeOp, CListCtrl)

CList_OperModeOp::CList_OperModeOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
	m_pstOperMode = NULL;
}

CList_OperModeOp::~CList_OperModeOp()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_OperModeOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_OperModeOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_OperModeOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IOperOp_ED_CELLEDIT, &CList_OperModeOp::OnEnKillFocusEdit)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_OperModeOp 메시지 처리기입니다.
int CList_OperModeOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER, CRect(0, 0, 0, 0), this, IOperOp_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:36
// Desc.		:
//=============================================================================
void CList_OperModeOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[OperOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = OperOp_Min; nCol < OperOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() - iHeaderWidth_OperOp[OperOp_FPS]) / (OperOp_MaxCol - OperOp_Min);
		SetColumnWidth(nCol, iUnitWidth);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:55
// Desc.		:
//=============================================================================
BOOL CList_OperModeOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:56
// Desc.		:
//=============================================================================
void CList_OperModeOp::InitHeader()
{
	for (UINT nCol = 0; nCol < OperOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_OperOp[nCol], iListAglin_OperOp[nCol], iHeaderWidth_OperOp[nCol]);
	}

	for (UINT nCol = 0; nCol < OperOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_OperOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:56
// Desc.		:
//=============================================================================
void CList_OperModeOp::InsertFullData()
{
	if (m_pstOperMode == NULL)
		return;

	DeleteAllItems();

	for (UINT nIndx = 0; nIndx < OperOp_ItemNum; nIndx++)
	{
		InsertItem(nIndx, _T(""));
		SetRectRow(nIndx);
	}

}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_OperModeOp::SetRectRow(UINT nRow)
{
	CString strText;

	if (m_pstOperMode->stCurrOpt[nRow].nSpecMin[0] >= m_pstOperMode->stCurrOpt[nRow].nSpecMax[0])
	{
		m_pstOperMode->stCurrOpt[nRow].nSpecMax[0] = m_pstOperMode->stCurrOpt[nRow].nSpecMin[0] + 1;
	}

	strText.Format(_T("%s"), g_lpszItem_OperOp[nRow]);
	SetItemText(nRow, OperOp_FPS, strText);

	strText.Format(_T("%.1f"), m_pstOperMode->stCurrOpt[nRow].nSpecMin[0]);
	SetItemText(nRow, OperOp_Min, strText);

	strText.Format(_T("%.1f"), m_pstOperMode->stCurrOpt[nRow].nSpecMax[0]);
	SetItemText(nRow, OperOp_Max, strText);

	strText.Format(_T("%.2f"), m_pstOperMode->stCurrOpt[nRow].dbOffset[0]);
	SetItemText(nRow, OperOp_Offset, strText);
 }

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_OperModeOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_OperModeOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem <5 && pNMItemActivate->iSubItem >0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
void CList_OperModeOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (m_nEditCol == OperOp_Offset || m_nEditCol == OperOp_Min || m_nEditCol == OperOp_Max)
	{
		UpdateCellData_double(m_nEditRow, m_nEditCol, _ttof(strText));
	}
	else
	{
		UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));
	}


	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/1/12 - 17:57
// Desc.		:
//=============================================================================
BOOL CList_OperModeOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	switch (nCol)
	{
	case OperOp_Min:
		m_pstOperMode->stCurrOpt[nRow].nSpecMin[0] = iValue;
		break;

	case OperOp_Max:
		m_pstOperMode->stCurrOpt[nRow].nSpecMax[0] = iValue;
		break;

	default:
		break;
	}
 
	CString str;
	str.Format(_T("%d"), iValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);
 
	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dValue
// Qualifier	:
// Last Update	: 2017/1/12 - 17:58
// Desc.		:
//=============================================================================
BOOL CList_OperModeOp::UpdateCellData_double(UINT nRow, UINT nCol, double dValue)
{
	switch (nCol)
	{
	case OperOp_Offset:
		m_pstOperMode->stCurrOpt[nRow].dbOffset[0] = dValue;
		break;
	case OperOp_Min:
		m_pstOperMode->stCurrOpt[nRow].nSpecMin[0] = dValue;
		break;
	case OperOp_Max:
		m_pstOperMode->stCurrOpt[nRow].nSpecMax[0] = dValue;
		break;
	default:
		break;
	}

	CString str;
	str.Format(_T("%.2f"), dValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Parameter	: ST_LT_TI_OperMode & stOperMode
// Qualifier	:
// Last Update	: 2017/1/12 - 17:58
// Desc.		:
//=============================================================================
void CList_OperModeOp::GetCellData()
{
	if (m_pstOperMode == NULL)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/1/12 - 17:58
// Desc.		:
//=============================================================================
BOOL CList_OperModeOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue	  = _ttoi(strText);
		double dbValue = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + zDelta / 120;
			dbValue = dbValue + zDelta / 120 * 0.1;
		}
		else
		{
			if (0 < iValue)
			{
				iValue = iValue + zDelta / 120;
			}

			if (0 < dbValue)
			{
				dbValue = dbValue + zDelta / 120 * 0.1;
			}
		}

		if (m_nEditCol == OperOp_Offset || m_nEditCol == OperOp_Min || m_nEditCol == OperOp_Max)
		{
			UpdateCellData_double(m_nEditRow, m_nEditCol, dbValue);
		}
		else
		{
			UpdateCellData(m_nEditRow, m_nEditCol, iValue);
		}
	}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}
