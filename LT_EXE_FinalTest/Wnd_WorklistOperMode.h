﻿//*****************************************************************************
// Filename	: Wnd_WorklistOperMode.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistOperMode_h__
#define Wnd_WorklistOperMode_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_OperMode.h"

//=============================================================================
// Wnd_WorklistOperMode
//=============================================================================
class CWnd_WorklistOperMode : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistOperMode)

public:
	CWnd_WorklistOperMode();
	virtual ~CWnd_WorklistOperMode();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

//	CList_Work_OperMode			m_list_OperMode[TICnt_OperationMode];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistOperMode_h__


