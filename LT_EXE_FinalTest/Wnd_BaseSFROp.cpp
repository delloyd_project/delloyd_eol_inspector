﻿// Wnd_BaseSFROp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_BaseSFROp.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_BaseSFROp

IMPLEMENT_DYNAMIC(CWnd_BaseSFROp, CWnd)

CWnd_BaseSFROp::CWnd_BaseSFROp()
{
	m_pstModelInfo	= NULL;
}

CWnd_BaseSFROp::~CWnd_BaseSFROp()
{

}

BEGIN_MESSAGE_MAP(CWnd_BaseSFROp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

// CWnd_BaseSFROp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_BaseSFROp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	//if (CWnd::OnCreate(lpCreateStruct) == -1)
	//	return -1;

	//DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	//CRect rectDummy;
	//rectDummy.SetRectEmpty();

	//m_tcTestItem.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, 1, CMFCTabCtrl::LOCATION_BOTTOM);

	//for (UINT nIdex = 0; nIdex < TICnt_SFR; nIdex++)
	//{
	//	m_wndSFROp[nIdex].SetOwner(GetOwner());
	//	m_wndSFROp[nIdex].SetTestItemCount(nIdex);
	//	m_wndSFROp[nIdex].Create(NULL, g_szLT_TestItemSFR[nIdex], dwStyle, rectDummy, &m_tcTestItem, 100 + nIdex);
	//	m_tcTestItem.AddTab(&m_wndSFROp[nIdex], g_szLT_TestItemSFR[nIdex], nIdex, FALSE);
	//}

	//m_tcTestItem.EnableTabSwap(FALSE);
	//m_tcTestItem.SetActiveTab(0);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_BaseSFROp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	m_tcTestItem.MoveWindow(iLeft, iTop, iWidth, iHeight);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_BaseSFROp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_BaseSFROp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	//CWnd::OnShowWindow(bShow, nStatus);

	//if (NULL == m_pstModelInfo)
	//	return;

	//if (TRUE == bShow)
	//{
	//	m_pstModelInfo->nTestMode = TIID_SFR;
	//	m_pstModelInfo->nTestCnt = m_tcTestItem.GetActiveTab();
	//	m_pstModelInfo->nPicItem = PIC_SFR;
	//}
}


//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_BaseSFROp::SetUpdateData()
{
	//if (m_pstModelInfo == NULL)
	//	return;

	//for (UINT nIdex = 0; nIdex < TICnt_SFR; nIdex++)
	//{
	//	m_wndSFROp[nIdex].SetPtr_ModelInfo(m_pstModelInfo);
	//	m_wndSFROp[nIdex].SetUpdateData();
	//}
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/14 - 18:01
// Desc.		:
//=============================================================================
void CWnd_BaseSFROp::GetUpdateData()
{
	//if (m_pstModelInfo == NULL)
	//	return;

	//for (UINT nIdex = 0; nIdex < TICnt_SFR; nIdex++)
	//{
	//	m_wndSFROp[nIdex].SetPtr_ModelInfo(m_pstModelInfo);
	//	m_wndSFROp[nIdex].GetUpdateData();
	//}
}