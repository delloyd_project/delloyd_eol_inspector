﻿#ifndef Wnd_BaseIRFilterManualOp_h__
#define Wnd_BaseIRFilterManualOp_h__

#pragma once

#include "Wnd_IRFilterManualOp.h"

// CWnd_BaseIRFilterManualOp

class CWnd_BaseIRFilterManualOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseIRFilterManualOp)

public:
	CWnd_BaseIRFilterManualOp();
	virtual ~CWnd_BaseIRFilterManualOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
//	CWnd_IRFilterManualOp m_wndIRFilterManualOp[TICnt_IRFilterManual];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseIRFilterManualOp_h__
