﻿//*****************************************************************************
// Filename	: 	Wnd_AccessMode.h
// Created	:	2016/3/11 - 14:52
// Modified	:	2016/3/11 - 14:52
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_AcessMode_h__
#define Wnd_AcessMode_h__

#pragma once

#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Reg_Management.h"
#include "resource.h"

//-----------------------------------------------------------------------------
// CWnd_AccessMode
//-----------------------------------------------------------------------------
class CWnd_AccessMode : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_AccessMode)

public:
	CWnd_AccessMode();
	virtual ~CWnd_AccessMode();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnGetMinMaxInfo			(MINMAXINFO* lpMMI);

	afx_msg void	OnBnClickedRbOperMode	();
	afx_msg void	OnBnClickedRbManagerMode();
	afx_msg void	OnBnClickedRbMesMode	();
	afx_msg void	OnBnClickedOK			();
	afx_msg void	OnBnClickedCancel		();

	CFont				m_font_Default;
	CFont				m_Font;

	CVGStatic			m_st_Title;
		
	CMFCButton			m_rb_OperMode;
	CMFCButton			m_rb_AdminMode;

	CVGStatic			m_st_Password;
	CEdit				m_ed_Password;

	CButton				m_bn_OK;
	CButton				m_bn_Cancel;

	enPermissionMode	m_AcessMode;
	CReg_Management		m_regManagement;

	void		LoadAcessMode	();
	void		SaveAcessMode	();
	BOOL		CheckPassword	();

public:	

 	void		SetAcessMode	(__in enPermissionMode nAcessMode);
	void		SetButtonEnable	(BOOL bMode);
	
	enPermissionMode	GetAcessMode()
	{
		return m_AcessMode;
	};
};

#endif // Wnd_AcessMode_h__


