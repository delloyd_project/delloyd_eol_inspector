//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestStep.cpp
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_TestStep.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_TestStep.h"
#include "resource.h"

// CWnd_Cfg_TestStep
#define IDC_LC_STEPLIST			1000
#define IDC_CB_TESTITEM			1001
#define IDC_CB_TESTITEMCNT		1008

#define IDC_CB_CANCTRL			1002
#define IDC_CB_RETRY_CNT		1003
#define IDC_CB_AXIS				1004
#define IDC_CB_IOPORT			1005
#define IDC_CB_IOSIGNAL			1006
#define IDC_CB_UTILITY			1007

#define IDC_BN_STEPCTRL_S		1100
#define IDC_BN_STEPCTRL_E		IDC_BN_STEPCTRL_S + SC_MaxEnum - 1
#define IDC_CHK_STEPITEM_S		1200
#define IDC_CHK_STEPITEM_E		IDC_CHK_STEPITEM_S + SI_MaxEnum - 1
#define IDC_ED_STEPITEM_S		1300
#define IDC_ED_STEPITEM_E		IDC_ED_STEPITEM_S + SI_MaxEnum - 1
#define IDC_BN_SEQCHECK			1400


static LPCTSTR g_szTestStep[] =
{
	_T("Test Item"),		// SI_Test			
	//_T("Test Sub Item"),	// SI_TestCnt			
	//_T("Retry Cnt"),		// SI_Retry		
	_T("Delay"),			// SI_Delay		
	//_T("Move Axis"),		// SI_MoveAxis
	//_T("Move Pos"),		// SI_MovePos		
	//_T("IO Port"),			// SI_IOport
	//_T("IO Signal"),		// SI_IOsignal	
	NULL
};

static LPCTSTR g_szTestCtrl[] =
{
	_T("Add"),		// SC_Add
	_T("Insert"),	// SC_Insert
	_T("Remove"),	// SC_Remove
	_T("Up"),		// SC_Order_Up
	_T("Down"),		// SC_Order_Down
	NULL
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_TestStep, CWnd_BaseView)

CWnd_Cfg_TestStep::CWnd_Cfg_TestStep()
{
	VERIFY(m_font.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_TestStep::~CWnd_Cfg_TestStep()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_TestStep, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_CHK_STEPITEM_S,	IDC_CHK_STEPITEM_E,		OnBnClickedBnStepItem)
	ON_COMMAND_RANGE(IDC_BN_STEPCTRL_S,		IDC_BN_STEPCTRL_E,		OnBnClickedBnStepCtrl)
	ON_BN_CLICKED(IDC_BN_SEQCHECK, OnBnClickedSeqCheck)
	ON_CBN_SELCHANGE(IDC_CB_TESTITEM, OnSelChangeCbTestItem)
END_MESSAGE_MAP()


// CWnd_Cfg_TestStep message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/24 - 16:23
// Desc.		:
//=============================================================================
int CWnd_Cfg_TestStep::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// 스텝 리스트
	m_lc_StepList.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LC_STEPLIST);
	
	for (UINT nIdx = 0; nIdx < SC_MaxEnum; nIdx++)
	{
		m_bn_StepCtrl[nIdx].m_bTransparent = TRUE;
		m_bn_StepCtrl[nIdx].Create(g_szTestCtrl[nIdx], dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_STEPCTRL_S + nIdx);
		m_bn_StepCtrl[nIdx].SetMouseCursorHand();
	}

	m_bn_SeqCheck.m_bTransparent = TRUE;
	m_bn_SeqCheck.Create(_T("Sequence Check"), dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_SEQCHECK);
	m_bn_SeqCheck.SetMouseCursorHand();

	// 검사 항목
	m_cb_TestItem.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_TESTITEM);
	m_cb_TestItem.EnableWindow(FALSE);
	m_cb_TestItem.SetFont(&m_font);

	for (UINT nIdx = 0; nIdx < TIID_MaxEnum; nIdx++)
	{
		m_cb_TestItem.AddString(g_szLT_TestItem_Name[nIdx]);
	}
	m_cb_TestItem.SetCurSel(0);

	// 검사 세부 항목
	//m_cb_TestItemCnt.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_TESTITEMCNT);
	//m_cb_TestItemCnt.EnableWindow(FALSE);
	//m_cb_TestItemCnt.SetFont(&m_font);

	// 로딩 항목
	//for (UINT nIdx = 0; nIdx < TICnt_Initial; nIdx++)
	//{
	//	m_cb_TestItemCnt.AddString(g_szLT_TestItemInitial[nIdx]);
	//}
	//m_cb_TestItemCnt.SetCurSel(0);

	// Retry Count
	//m_cb_RetryCnt.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_RETRY_CNT);
	//m_cb_RetryCnt.EnableWindow(FALSE);
	//m_cb_RetryCnt.SetFont(&m_font);

	//CString szText;
	//for (UINT nIdx = 0; nIdx <= 5; nIdx++)
	//{
	//	szText.Format(_T("%d"), nIdx);
	//	m_cb_RetryCnt.AddString(szText);
	//}
	//m_cb_RetryCnt.SetCurSel(0);

	// 모터 항목
// 	m_cb_AxisItem.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_AXIS);
// 	m_cb_AxisItem.EnableWindow(FALSE);
// 	m_cb_AxisItem.SetFont(&m_font);

// 	for (UINT nIdx = 0; nIdx < AX_Stage_Max; nIdx++)
// 	{
// 		m_cb_AxisItem.AddString(g_lpszMotor_Axis[nIdx]);
// 	}
// 	m_cb_AxisItem.SetCurSel(0);
	
	// Step 항목 조건
	for (UINT nIdx = 0; nIdx < SI_MaxEnum; nIdx++)
	{
		m_chk_StepItem[nIdx].Create(g_szTestStep[nIdx], dwStyle | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_STEPITEM_S + nIdx);
		m_ed_StepItem[nIdx].Create(dwStyle | ES_CENTER | WS_BORDER, rectDummy, this, IDC_ED_STEPITEM_S + nIdx);

		m_chk_StepItem[nIdx].SetMouseCursorHand();
		m_chk_StepItem[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_StepItem[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_StepItem[nIdx].SizeToContent();

		m_ed_StepItem[nIdx].EnableWindow(FALSE);
		m_ed_StepItem[nIdx].SetFont(&m_font);

		m_ed_StepItem[nIdx].SetWindowText(_T("0"));
	}
 	
	m_ed_StepItem[SI_Delay].SetValidChars(_T("0123456789"));
	//m_ed_StepItem[SI_MovePos].SetValidChars(_T("0123456789-."));
	
	m_st_Name.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Name.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Name.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Name.Create(_T("TEST ITEM ADD"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_TestName.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_TestName.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_TestName.SetFont_Gdip(L"Arial", 9.0F);
	m_st_TestName.Create(_T("TEST ITEM INFO"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	return 1;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/24 - 16:23
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing	= 5;
	int iCateSpacing= 10;
	int iMagin		= 10;
	int iLeft		= iMagin;
	int iTop		= iMagin;
	int iWidth		= cx - (iMagin * 2);
	int iHeight		= cy - (iMagin * 2);
	int iHalfWidth	= (iWidth - iCateSpacing) / 3;
	int iHalfHeight	= (iHeight - iCateSpacing)  / 2;
	int iCapWidth	= (iWidth - iSpacing * 4 ) / 5;
	int iValWidth	= 0;
	int iCtrlHeight	= 25;
	int iLeftSub	= 0;

	// 스텝 리스트
	m_lc_StepList.MoveWindow(iLeft, iTop, iWidth, iHalfHeight);

	// 스텝 추가/삭제/이동
	iLeft = iMagin;
	iLeftSub = iLeft + iCapWidth + iSpacing;
	iTop += iHalfHeight + iCateSpacing;

	m_st_Name.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);
	
	iTop += iCtrlHeight + iSpacing;

	for (UINT nIdx = 0; nIdx < SC_MaxEnum; nIdx++)
	{
		m_bn_StepCtrl[nIdx].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);

		iLeft += iCapWidth + iSpacing;
	}
	
	iTop += iCtrlHeight + iCateSpacing;
	iLeft = iMagin;
	m_st_TestName.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);

	iHalfWidth = (iWidth - iCateSpacing) / 2;
	iLeft = iMagin;

	iCapWidth = iHalfWidth - iLeft;
	iLeftSub = iLeft + iCapWidth + iSpacing;

	iTop += iCtrlHeight + iSpacing;
	iValWidth = iWidth - iLeftSub + iMagin;

	m_chk_StepItem[SI_Test].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	m_cb_TestItem.MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);

	//iTop += iCtrlHeight + iSpacing;
	//m_chk_StepItem[SI_TestCnt].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	//m_cb_TestItemCnt.MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);

	//iTop += iCtrlHeight + iSpacing;
	//m_chk_StepItem[SI_Retry].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	//m_cb_RetryCnt.MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);

	iTop += iCtrlHeight + iSpacing;
	m_chk_StepItem[SI_Delay].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	m_ed_StepItem[SI_Delay].MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);

// 	iTop += iCtrlHeight + iSpacing;
// 	m_chk_StepItem[SI_MoveAxis].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
// 	m_cb_AxisItem.MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);
// 
// 	iTop += iCtrlHeight + iSpacing;
// 	m_chk_StepItem[SI_MovePos].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
// 	m_ed_StepItem[SI_MovePos].MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);
// 
// 	iTop += iCtrlHeight + iSpacing;
// 	m_bn_SeqCheck.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);


}

//=============================================================================
// Method		: OnBnClickedBnStepItem
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/9/25 - 9:51
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::OnBnClickedBnStepItem(UINT nID)
{
	enStepItem nIDIdx = (enStepItem)(nID - IDC_CHK_STEPITEM_S);
	
	BOOL bEnable = TRUE;

	if (BST_CHECKED == m_chk_StepItem[nIDIdx].GetCheck())
	{
		bEnable = TRUE;
	}
	else
	{
		bEnable = FALSE;
	}
	
	m_ed_StepItem[nIDIdx].EnableWindow(bEnable);
	
	switch (nIDIdx)
	{
	case CWnd_Cfg_TestStep::SI_Test:
	//case CWnd_Cfg_TestStep::SI_TestCnt:
	//case CWnd_Cfg_TestStep::SI_Retry:
	case CWnd_Cfg_TestStep::SI_Delay:

		if (TRUE == bEnable)
		{
			for (int i = 0; i < SI_MaxEnum; i++)
			{
				m_chk_StepItem[i].SetCheck(0);
			}

			m_chk_StepItem[SI_Test].SetCheck(1);
		//	m_chk_StepItem[SI_TestCnt].SetCheck(1);
		//	m_chk_StepItem[SI_Retry].SetCheck(1);
			m_chk_StepItem[SI_Delay].SetCheck(1);

			Item_Enable(nIDIdx);
		}
		else
		{
			m_chk_StepItem[SI_Test].SetCheck(0);
		//	m_chk_StepItem[SI_TestCnt].SetCheck(0);
		//	m_chk_StepItem[SI_Retry].SetCheck(0);
			m_chk_StepItem[SI_Delay].SetCheck(0);

			m_cb_TestItem.EnableWindow(FALSE);
		//	m_cb_TestItemCnt.EnableWindow(FALSE);
		//	m_cb_RetryCnt.EnableWindow(FALSE);
			m_ed_StepItem[SI_Delay].EnableWindow(FALSE);
		}
	
		break;


// 	case CWnd_Cfg_TestStep::SI_MoveAxis:
// 		if (TRUE == bEnable)
// 		{
// 			for (int i = 0; i < SI_MaxEnum; i++)
// 			{
// 				//if (i != SI_MoveAxis || i != SI_MovePos)
// 					m_chk_StepItem[i].SetCheck(0);
// 			}
// 
// 			m_chk_StepItem[SI_MoveAxis].SetCheck(1);
// 			m_chk_StepItem[SI_MovePos].SetCheck(1);
// 
// 			m_cb_AxisItem.SetCurSel(0);
// 			m_ed_StepItem[SI_MovePos].SetWindowText(_T("0"));
// 
// 			Item_Enable(nIDIdx);
// 		}
// 		else
// 		{
// 			m_chk_StepItem[SI_MoveAxis].SetCheck(0);
// 			m_chk_StepItem[SI_MovePos].SetCheck(0);
// 
// 			m_cb_AxisItem.EnableWindow(FALSE);
// 			m_ed_StepItem[SI_MovePos].EnableWindow(FALSE);
// 		}
// 		
// 
// 		break;

// 	case CWnd_Cfg_TestStep::SI_MovePos:
// 
// 		if (TRUE == bEnable)
// 		{
// 			for (int i = 0; i < SI_MaxEnum; i++)
// 			{
// 				//if (i != SI_MoveAxis || i != SI_MovePos)
// 				m_chk_StepItem[i].SetCheck(0);
// 			}
// 
// 			m_chk_StepItem[SI_MoveAxis].SetCheck(1);
// 			m_chk_StepItem[SI_MovePos].SetCheck(1);
// 
// 			m_cb_AxisItem.SetCurSel(0);
// 			m_ed_StepItem[SI_MovePos].SetWindowText(_T("0"));
// 
// 			Item_Enable(nIDIdx);
// 		}
// 		else
// 		{
// 			m_chk_StepItem[SI_MoveAxis].SetCheck(0);
// 			m_chk_StepItem[SI_MovePos].SetCheck(0);
// 
// 			m_cb_AxisItem.EnableWindow(FALSE);
// 			m_ed_StepItem[SI_MovePos].EnableWindow(FALSE);
// 		}
// 
// 		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: OnBnClickedBnStepCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/9/25 - 9:51
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::OnBnClickedBnStepCtrl(UINT nID)
{
	enStepCtrl nIDIdx = (enStepCtrl)(nID - IDC_BN_STEPCTRL_S);

	switch (nIDIdx)
	{
	case CWnd_Cfg_TestStep::SC_Add:
		Item_Add();
		break;

	case CWnd_Cfg_TestStep::SC_Insert:
		Item_Insert();
		break;

	case CWnd_Cfg_TestStep::SC_Remove:
		Item_Remove();
		break;

	case CWnd_Cfg_TestStep::SC_Order_Up:
		Item_Up();
		break;

	case CWnd_Cfg_TestStep::SC_Order_Down:
		Item_Down();
		break;

	default:
		break;
	}
}


void CWnd_Cfg_TestStep::OnBnClickedSeqCheck()
{
	POSITION nPos = m_lc_StepList. GetFirstSelectedItemPosition();

	if (nPos != NULL)
	{
		UINT nIdx = (UINT)nPos - 1;

		ST_StepInfo stOutStepInfo;
		Get_StepInfo(stOutStepInfo);

		if (TRUE == stOutStepInfo.StepList[nIdx].bTestUse)
		{
			// recipe view test -> Message Send
			UINT nTestitem = stOutStepInfo.StepList[nIdx].nTestItem;
			UINT nTestitemCnt = stOutStepInfo.StepList[nIdx].nTestItemCnt;
			GetOwner()->SendMessage(WM_MANUAL_TEST, nTestitemCnt, nTestitem);
		}
// 		else if (TRUE == stOutStepInfo.StepList[nIdx].bAxisUse)
// 		{
// 			// device struct pointer
// 			UINT nAxis = stOutStepInfo.StepList[nIdx].nAxisItem;
// 			double dbPos = stOutStepInfo.StepList[nIdx].dbMovePos;
// 
// 			m_pstDevice->MotionManager.MotorAxisMove(POS_ABS_MODE, nAxis, dbPos);
// 		}
		else if (TRUE == stOutStepInfo.StepList[nIdx].bIOUse)
		{
			// device struct pointer
			UINT nIO = stOutStepInfo.StepList[nIdx].nIOItem;
			UINT nSignal = stOutStepInfo.StepList[nIdx].nIOSignal;
			
		}
	}
}


void CWnd_Cfg_TestStep::Item_TestAddString(enLT_TestItem_ID nTestItem)
{
	//m_cb_TestItemCnt.ResetContent();

	CString szTest;

	for (UINT nTestCnt = 0; nTestCnt < g_nLT_TestItemCount[nTestItem]; nTestCnt++)
	{
		switch (nTestItem)
		{
		case TIID_TestInitialize:
			szTest.Format(_T("%s"), g_szLT_TestItemInitial[nTestCnt]);
			break;
		case TIID_TestFinalize:
			szTest.Format(_T("%s"), g_szLT_TestItemFinal[nTestCnt]);
			break;
		case TIID_Current:
			szTest.Format(_T("%s"), g_szLT_TestItemCurrent[nTestCnt]);
			break;
		//case TIID_OperationMode:
		//	szTest.Format(_T("%s"), g_szLT_TestItemOperationMode[nTestCnt]);
		//	break;

// 		case TIID_VideoSignal:
// 			szTest.Format(_T("%s"), g_szLT_TestItemVideoSignal[nTestCnt]);
// 			break;
		case TIID_CenterPoint:
			szTest.Format(_T("%s"), g_szLT_TestItemCenterPt[nTestCnt]);
			break;
		case TIID_Rotation:
			szTest.Format(_T("%s"), g_szLT_TestItemRotate[nTestCnt]);
			break;
 		case TIID_EIAJ:
 			szTest.Format(_T("%s"), g_szLT_TestItemEIAJ[nTestCnt]);
 			break;
		//case TIID_SFR:
		//	szTest.Format(_T("%s"), g_szLT_TestItemSFR[nTestCnt]);
		//	break;
		case TIID_FOV:
			szTest.Format(_T("%s"), g_szLT_TestItemAngle[nTestCnt]);
			break;
		case TIID_Color:
			szTest.Format(_T("%s"), g_szLT_TestItemColor[nTestCnt]);
			break;
		case TIID_Reverse:
			szTest.Format(_T("%s"), g_szLT_TestItemReverse[nTestCnt]);
			break;
		case TIID_ParticleManual:
			szTest.Format(_T("%s"), g_szLT_TestItemParticleManual[nTestCnt]);
			break;
		case TIID_BlackSpot:
			szTest.Format(_T("%s"), g_szLT_TestItemBlackSpot[nTestCnt]);
			break;
		case TIID_DefectPixel:
			szTest.Format(_T("%s"), g_szLT_TestItemDefectPixel[nTestCnt]);
			break;
		//case TIID_LEDTest:
		//	szTest.Format(_T("%s"), g_szLT_TestItemLED[nTestCnt]);
		//	break;
// 		case TIID_Reset:
// 			szTest.Format(_T("%s"), g_szLT_TestItemReset[nTestCnt]);
// 			break;
		case TIID_Brightness:
			szTest.Format(_T("%s"), g_szLT_TestItemBrightness[nTestCnt]);
			break;
		//case TIID_PatternNoise:
		//	szTest.Format(_T("%s"), g_szLT_TestItemPatternNoise[nTestCnt]);
		//	break;
		case TIID_IRFilter:
			szTest.Format(_T("%s"), g_szLT_TestItemIRFilter[nTestCnt]);
			break;
		default:
			break;
		}

		//m_cb_TestItemCnt.AddString(szTest);

	}

	//m_cb_TestItemCnt.SetCurSel(0);
}

void CWnd_Cfg_TestStep::OnSelChangeCbTestItem()
{
	UINT nTestItem = m_cb_TestItem.GetCurSel();
	Item_TestAddString((enLT_TestItem_ID)nTestItem);
}

//=============================================================================
// Method		: GetTestStepData
// Access		: protected  
// Returns		: BOOL
// Parameter	: __out ST_StepUnit & stStepUnit
// Qualifier	:
// Last Update	: 2017/9/25 - 17:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_TestStep::GetTestStepData(__out ST_StepUnit& stStepUnit)
{
	CString		szText;

	// Retry Count
	/*if (BST_CHECKED == m_chk_StepItem[SI_Retry].GetCheck())
	{
		int iSel = m_cb_RetryCnt.GetCurSel();
		if (0 <= iSel)
		{
			stStepUnit.nRetryCnt = iSel;
		}
	}
	else
	{
		stStepUnit.nRetryCnt = 0;
	}
*/
	// Test Item
	if (BST_CHECKED == m_chk_StepItem[SI_Test].GetCheck())
	{
		stStepUnit.bTestUse = TRUE;

		int iSel = m_cb_TestItem.GetCurSel();
		if (0 <= iSel)
		{
			stStepUnit.nTestItem = iSel;
		}
	}
	else
	{
		stStepUnit.bTestUse = FALSE;
		stStepUnit.nTestItem = 0;
	}

	// Test Item Count
	//if (BST_CHECKED == m_chk_StepItem[SI_TestCnt].GetCheck())
	//{
	//	stStepUnit.bTestUse = TRUE;

	//	int iSel = m_cb_TestItemCnt.GetCurSel();
	//	if (0 <= iSel)
	//	{
	//		stStepUnit.nTestItemCnt = iSel;
	//	}
	//}
	//else
	//{
	//	stStepUnit.bTestUse = FALSE;
	//	stStepUnit.nTestItemCnt = 0;
	//}

	// Delay
	if (BST_CHECKED == m_chk_StepItem[SI_Delay].GetCheck())
	{
		m_ed_StepItem[SI_Delay].GetWindowText(szText);

		stStepUnit.dwDelay = (DWORD)_ttoi(szText.GetBuffer(0));
	}
	else
	{
		stStepUnit.dwDelay = 0;
	}

// 	// Axis
// 	if (BST_CHECKED == m_chk_StepItem[SI_MoveAxis].GetCheck())
// 	{
// 		stStepUnit.bAxisUse = TRUE;
// 
// 		int iSel = m_cb_AxisItem.GetCurSel();
// 		if (0 <= iSel)
// 		{
// 			stStepUnit.nAxisItem = iSel;
// 		}
// 	}
// 	else
// 	{
// 		stStepUnit.bAxisUse = FALSE;
// 		stStepUnit.nAxisItem = 0;
// 	}
// 
// 	// Move axis
// 	if (BST_CHECKED == m_chk_StepItem[SI_MovePos].GetCheck())
// 	{
// 		m_ed_StepItem[SI_MovePos].GetWindowText(szText);
// 
// 		stStepUnit.dbMovePos = _ttof(szText.GetBuffer(0));
// 	}
// 	else
// 	{
// 		stStepUnit.dbMovePos = 0.0;
// 	}

	return TRUE;
}

//=============================================================================
// Method		: Item_Add
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 10:25
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Item_Add()
{
	ST_StepUnit stStepUnit;
	if (GetTestStepData(stStepUnit))
	{
		m_lc_StepList.Item_Add(stStepUnit);
	}
}

//=============================================================================
// Method		: Item_Insert
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Item_Insert()
{
	ST_StepUnit stStepUnit;
	if (GetTestStepData(stStepUnit))
	{
		m_lc_StepList.Item_Insert(stStepUnit);
	}
}

//=============================================================================
// Method		: Item_Remove
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Item_Remove()
{
	m_lc_StepList.Item_Remove();	
}

//=============================================================================
// Method		: Item_Up
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Item_Up()
{
	m_lc_StepList.Item_Up();
}

//=============================================================================
// Method		: Item_Down
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Item_Down()
{
	m_lc_StepList.Item_Down();
}


void CWnd_Cfg_TestStep::Item_Enable(enStepItem enStep)
{
	switch (enStep)
	{
	case CWnd_Cfg_TestStep::SI_Test:
	//case CWnd_Cfg_TestStep::SI_TestCnt:
	//case CWnd_Cfg_TestStep::SI_Retry:
	case CWnd_Cfg_TestStep::SI_Delay:
		m_cb_TestItem.EnableWindow(TRUE);
	//	m_cb_TestItem.EnableWindow(TRUE);
	//	m_cb_TestItemCnt.EnableWindow(TRUE);
		m_ed_StepItem[SI_Delay].EnableWindow(TRUE);

		//m_cb_AxisItem.EnableWindow(FALSE);
		//m_ed_StepItem[SI_MovePos].EnableWindow(FALSE);
		break;


// 	case CWnd_Cfg_TestStep::SI_MoveAxis:
// 	case CWnd_Cfg_TestStep::SI_MovePos:
// 		m_cb_AxisItem.EnableWindow(TRUE);
// 		m_ed_StepItem[SI_MovePos].EnableWindow(TRUE);
// 
// 		m_cb_TestItem.EnableWindow(FALSE);
// 		m_cb_TestItemCnt.EnableWindow(FALSE);
// 		m_cb_RetryCnt.EnableWindow(FALSE);
// 		m_cb_IOItem.EnableWindow(FALSE);
// 		m_cb_IOSignal.EnableWindow(FALSE);
// 		m_ed_StepItem[SI_Delay].EnableWindow(FALSE);
// 		break;

	default:
		break;
	}
}


//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 14:05
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::SetSystemType(__in enInsptrSysType nSysType)
{
}

//=============================================================================
// Method		: Set_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_StepInfo * pstInStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 20:27
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Set_StepInfo(__in const ST_StepInfo* pstInStepInfo)
{
	m_lc_StepList.Set_StepInfo(pstInStepInfo);
}

//=============================================================================
// Method		: Get_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_StepInfo & stOutStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 20:38
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Get_StepInfo(__out ST_StepInfo& stOutStepInfo)
{
	m_lc_StepList.Get_StepInfo(stOutStepInfo);
}

//=============================================================================
// Method		: Init_DefaultSet
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 20:56
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Init_DefaultSet()
{
	// 리스트 초기화
	m_lc_StepList.DeleteAllItems();

	// 체크 버튼 초기화
	for (UINT nIdx = 0; nIdx < SI_MaxEnum; nIdx++)
	{
		m_chk_StepItem[nIdx].SetCheck(BST_UNCHECKED);
		m_ed_StepItem[nIdx].EnableWindow(FALSE);
	}

	m_cb_TestItem.SetCurSel(0);
	m_cb_TestItem.EnableWindow(FALSE);

	//m_cb_TestItemCnt.SetCurSel(0);
	//m_cb_TestItemCnt.EnableWindow(FALSE);

}
