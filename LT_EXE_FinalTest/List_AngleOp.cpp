﻿// List_AngleOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_AngleOp.h"

#define IRtOp_ED_CELLEDIT			5001
#define IRtOp_CB_CELLCOMBO_TYPE		5002

// CList_AngleOp

IMPLEMENT_DYNAMIC(CList_AngleOp, CListCtrl)

CList_AngleOp::CList_AngleOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
	
	m_nWidth	= 640;
	m_nHeight	= 480;

	m_pstAngle	= NULL;
}

CList_AngleOp::~CList_AngleOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_AngleOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_AngleOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_AngleOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IRtOp_ED_CELLEDIT, &CList_AngleOp::OnEnKillFocusEdit)
	ON_CBN_KILLFOCUS(IRtOp_CB_CELLCOMBO_TYPE, &CList_AngleOp::OnEnKillFocusCombo)
	ON_CBN_SELCHANGE(IRtOp_CB_CELLCOMBO_TYPE, &CList_AngleOp::OnEnSelectFocusCombo)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_AngleOp 메시지 처리기입니다.
int CList_AngleOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IRtOp_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);
	

	m_cb_Type.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IRtOp_CB_CELLCOMBO_TYPE);
	m_cb_Type.ResetContent();

	for (UINT nIdex = 0; NULL != g_szAngleMarkColor[nIdex]; nIdex++)
	{
		m_cb_Type.InsertString(nIdex, g_szAngleMarkColor[nIdex]);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_AngleOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[AngOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = AngOp_PosX; nCol < AngOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() - iHeaderWidth_AngleOp[AngOp_Object]) / (AngOp_MaxCol - AngOp_PosX);
		SetColumnWidth(nCol, iUnitWidth);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_AngleOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_AngleOp::InitHeader()
{
	for (int nCol = 0; nCol < AngOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_AngleOp[nCol], iListAglin_AngleOp[nCol], iHeaderWidth_AngleOp[nCol]);
	}

	for (int nCol = 0; nCol < AngOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_AngleOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_AngleOp::InsertFullData()
{
	if (m_pstAngle == NULL)
		return;

 	DeleteAllItems();
 

	for (UINT nIdx = 0; nIdx < AngOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_AngleOp::SetRectRow(UINT nRow)
{
	if (m_pstAngle == NULL)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_szRoiAngle[nRow]);
	SetItemText(nRow, AngOp_Object, strValue);

	strValue.Format(_T("%d"), m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.CenterPoint().x);
	SetItemText(nRow, AngOp_PosX, strValue);

	strValue.Format(_T("%d"),  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.CenterPoint().y);
	SetItemText(nRow, AngOp_PosY, strValue);

	strValue.Format(_T("%d"),  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.Width());
	SetItemText(nRow, AngOp_Width, strValue);

	strValue.Format(_T("%d"),  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.Height());
	SetItemText(nRow, AngOp_Height, strValue);

	strValue.Format(_T("%s"), g_szAngleMarkColor[ m_pstAngle->stAngleOpt.stRegionOp[nRow].nMarkColor]);
	SetItemText(nRow, AngOp_Mark, strValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_AngleOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	m_pstAngle->bTestMode = FALSE;
	m_pstAngle->stAngleOpt.iSelectROI = pNMItemActivate->iItem;
	m_pstAngle->stAngleResult.iSelectROI = -1;

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_AngleOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < AngOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;
			m_pstAngle->bTestMode = FALSE;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			if (pNMItemActivate->iSubItem == AngOp_Mark)
			{
				m_cb_Type.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Type.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Type.SetFocus();
				m_cb_Type.SetCurSel( m_pstAngle->stAngleOpt.stRegionOp[pNMItemActivate->iItem].nMarkColor);
			}
			else
			{
				m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_ed_CellEdit.SetFocus();
			}
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_AngleOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_AngleOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (m_pstAngle == NULL)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	CRect rtTemp =  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi;

	switch (nCol)
	{
	case AngOp_PosX:
		 m_pstAngle->stAngleOpt.stRegionOp[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
		break;
	case AngOp_PosY:
		 m_pstAngle->stAngleOpt.stRegionOp[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
		break;
	case AngOp_Width:
		 m_pstAngle->stAngleOpt.stRegionOp[nRow].RectPosWH(iValue, rtTemp.Height());
		break;
	case AngOp_Height:
		 m_pstAngle->stAngleOpt.stRegionOp[nRow].RectPosWH(rtTemp.Width(), iValue);
		break;
	default:
		break;
	}

	if ( m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.left < 0)
	{
		CRect rtTemp =  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi;

		 m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.left = 0;
		 m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.right =  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.left + rtTemp.Width();
	}

	if ( m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.right > m_nWidth)
	{
		CRect rtTemp =  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi;

		 m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.right = m_nWidth;
		 m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.left =  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.right - rtTemp.Width();
	}

	if ( m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.top < 0)
	{
		CRect rtTemp =  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi;

		 m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.top = 0;
		 m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.bottom = rtTemp.Height() +  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.top;
	}

	if ( m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.bottom > m_nHeight)
	{
		CRect rtTemp =  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi;

		 m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.bottom = m_nHeight;
		 m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.top =  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.bottom - rtTemp.Height();
	}

	if ( m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.Height() <= 0)
		 m_pstAngle->stAngleOpt.stRegionOp[nRow].RectPosWH( m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.Width(), 1);

	if ( m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.Height() >= m_nHeight)
		 m_pstAngle->stAngleOpt.stRegionOp[nRow].RectPosWH( m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.Width(), m_nHeight);

	if ( m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.Width() <= 0)
		 m_pstAngle->stAngleOpt.stRegionOp[nRow].RectPosWH(1,  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.Height());

	if ( m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.Width() >= m_nWidth)
		 m_pstAngle->stAngleOpt.stRegionOp[nRow].RectPosWH(m_nWidth,  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.Height());

	CString strValue;

	switch (nCol)
	{
	case AngOp_PosX:
		strValue.Format(_T("%d"),  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.CenterPoint().x);
		break;
	case AngOp_PosY:
		strValue.Format(_T("%d"),  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.CenterPoint().y);
		break;
	case AngOp_Width:
		strValue.Format(_T("%d"),  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.Width());
		break;
	case AngOp_Height:
		strValue.Format(_T("%d"),  m_pstAngle->stAngleOpt.stRegionOp[nRow].rtRoi.Height());
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCelldbData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/10/14 - 17:46
// Desc.		:
//=============================================================================
BOOL CList_AngleOp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	CString str;
	str.Format(_T("%.1f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_AngleOp::GetCellData()
{
	if (m_pstAngle == NULL)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_AngleOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue  = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120)*0.1);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dbValue)
				dbValue = dbValue + ((zDelta / 120)*0.1);
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		if (dbValue > 2.0)
			dbValue = 2.0;
		
		UpdateCellData(m_nEditRow, m_nEditCol, iValue);

	}
	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: OnEnKillFocusCombo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/11/3 - 0:12
// Desc.		:
//=============================================================================
void CList_AngleOp::OnEnKillFocusCombo()
{
	SetItemText(m_nEditRow, AngOp_Mark, g_szRotateMarkColor[m_pstAngle->stAngleOpt.stRegionOp[m_nEditRow].nMarkColor]);
	m_cb_Type.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectFocusCombo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/2 - 20:36
// Desc.		:
//=============================================================================
void CList_AngleOp::OnEnSelectFocusCombo()
{
	m_pstAngle->stAngleOpt.stRegionOp[m_nEditRow].nMarkColor = m_cb_Type.GetCurSel();
}
