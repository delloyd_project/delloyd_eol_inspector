//*****************************************************************************
// Filename	: 	List_TestStep.cpp
// Created	:	2017/9/24 - 16:35
// Modified	:	2017/9/24 - 16:35
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// List_TestStep.cpp : implementation file
//

#include "stdafx.h"
#include "List_TestStep.h"

// CList_TestStep
typedef enum enTestStepHeader
{
	TSH_Master,
	TSH_No,
	TSH_TestItem,
	//TSH_TestItemCnt,
	//TSH_Retry,
	TSH_Delay,
	//TSH_MoveAxis,
	//TSH_MovePos,
	//TSH_IOPort,
	//TSH_IOSignal,
	TSH_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader[] =
{
	_T("Master"),			// TSH_Master
	_T("No"),				// TSH_No
	_T("Test Item"),		// TSH_TestItem
	//_T("Sub"),				// TSH_TestItemCnt
	//_T("Retry"),			// TSH_Retry
	_T("Delay"),			// TSH_Delay
	//_T("Axis"),			// TSH_MoveAxis
	//_T("Move"),			// TSH_MovePos
	//_T("Port"),			// TSH_IOPort
	//_T("Bit"),			// TSH_IOSignal
	NULL					   
};

const int	iListAglin[] =
{
	LVCFMT_CENTER,	 // TSH_Master
	LVCFMT_CENTER,	 // TSH_No
	LVCFMT_CENTER,	 // TSH_TestItem
	LVCFMT_CENTER,	 // TSH_TestItem
	LVCFMT_CENTER,	 // TSH_Retry
	LVCFMT_CENTER,	 // TSH_Delay
	LVCFMT_CENTER,	 // TSH_MoveAxis
	LVCFMT_CENTER,	 // TSH_MovePos
	LVCFMT_CENTER,	 // TSH_MovePos
	LVCFMT_CENTER,	 // TSH_MovePos
};

// 540 기준
const int	iHeaderWidth[] =
{
	52, 	// TSH_Master
	40, 	// TSH_No
	300,	// TSH_TestItem
	//50,		// TSH_TestItem
	75,		// TSH_Retry
	75,		// TSH_Delay
	//65,	// TSH_MoveAxis
	//65,	// TSH_MovePos
	//100,		// TSH_IOPort
	//50,		// TSH_IOPort
};


IMPLEMENT_DYNAMIC(CList_TestStep, CListCtrl)

CList_TestStep::CList_TestStep()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_pHeadWidth = iHeaderWidth;
}

CList_TestStep::~CList_TestStep()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_TestStep, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK,	&CList_TestStep::OnNMClick)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_TestStep message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
int CList_TestStep::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	//SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);
	
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER | LVS_EX_CHECKBOXES);
	InitHeader();

	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
void CList_TestStep::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
BOOL CList_TestStep::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | LVS_SINGLESEL | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
void CList_TestStep::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	//NM_LISTVIEW* pNMView = (NM_LISTVIEW*)pNMHDR;
	//int index = pNMView->iItem;

 	LVHITTESTINFO HitInfo;
 	HitInfo.pt = pNMItemActivate->ptAction;
 
 	HitTest(&HitInfo);
 
 	// Check Ctrl Event
	if (HitInfo.flags == LVHT_ONITEMSTATEICON)
	{
		UINT nBuffer;

		nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

		if (nBuffer == 0x2000)
			m_stStepInfo.Step_MasterCheck(pNMItemActivate->iItem, FALSE);

		if (nBuffer == 0x1000)
			m_stStepInfo.Step_MasterCheck(pNMItemActivate->iItem, TRUE);

	}

	*pResult = 0;
}

//=============================================================================
// Method		: InitHeader
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/21 - 16:11
// Desc.		:
//=============================================================================
void CList_TestStep::InitHeader()
{
	if (FALSE == m_bIntiHeader)
	{
		m_bIntiHeader = TRUE;

		int iMaxCol = TSH_MaxCol;

		for (int nCol = 0; nCol < iMaxCol; nCol++)
		{
			InsertColumn(nCol, g_lpszHeader[nCol], iListAglin[nCol], iHeaderWidth[nCol]);
		}
	}

	for (int nCol = 0; nCol < TSH_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, m_pHeadWidth[nCol]);
	}
}

//=============================================================================
// Method		: ResetOrderingNumbers
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 16:05
// Desc.		:
//=============================================================================
void CList_TestStep::ResetOrderingNumbers()
{
	CString szText;

	for (int nIdx = 0; nIdx < GetItemCount(); nIdx++)
	{
		// TSH_No
		szText.Format(_T("%d"), nIdx + 1);
		SetItemText(nIdx, TSH_No, szText);
	}
}

//=============================================================================
// Method		: SetSelectItem
// Access		: public  
// Returns		: void
// Parameter	: __in int nItem
// Qualifier	:
// Last Update	: 2017/9/25 - 18:28
// Desc.		:
//=============================================================================
void CList_TestStep::SetSelectItem(__in int nItem)
{
	if (nItem < GetItemCount())
	{
		SetHotItem(nItem);
		SetItemState(nItem, LVIS_FOCUSED, LVIS_FOCUSED);
		SetItemState(nItem, LVIS_SELECTED, LVIS_SELECTED);
		SetFocus();
	}
}

//=============================================================================
// Method		: InsertTestStep
// Access		: public  
// Returns		: void
// Parameter	: __in int nItem
// Parameter	: __in const ST_StepUnit * pTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 19:57
// Desc.		:
//=============================================================================
void CList_TestStep::InsertTestStep(__in int nItem, __in const ST_StepUnit* pTestStep)
{
	ASSERT(GetSafeHwnd());
	if (NULL == pTestStep)
		return;

	if (GetItemCount() <= nItem)
		return;

	int iNewCount = nItem;

	InsertItem(iNewCount, _T(""));

	CString szText;

	// TSH_Master
	if (pTestStep->bMasterUse == TRUE)
		SetItemState(iNewCount, 0x2000, LVIS_STATEIMAGEMASK);
	else
		SetItemState(iNewCount, 0x1000, LVIS_STATEIMAGEMASK);

	// TSH_No
	szText.Format(_T("%d"), iNewCount);
	SetItemText(iNewCount, TSH_No, szText);

 	// TSH_TestItem
	if (pTestStep->bTestUse)
	{
		SetItemText(iNewCount, TSH_TestItem, g_szLT_TestItem_Name[pTestStep->nTestItem]);
	}
	else
	{
		SetItemText(iNewCount, TSH_TestItem, _T("-"));
	}



	// TSH_TestItemCnt
	if (pTestStep->bTestUse)
	{
		CString szTest;
		switch (pTestStep->nTestItem)
		{
		case TIID_TestInitialize:
			szTest.Format(_T("%s"), g_szLT_TestItemInitial[pTestStep->nTestItemCnt]);
			break;
		case TIID_TestFinalize:
			szTest.Format(_T("%s"), g_szLT_TestItemFinal[pTestStep->nTestItemCnt]);
			break;
		case TIID_Current:
			szTest.Format(_T("%s"), g_szLT_TestItemCurrent[pTestStep->nTestItemCnt]);
			break;
		//case TIID_OperationMode:
		//	szTest.Format(_T("%s"), g_szLT_TestItemOperationMode[pTestStep->nTestItemCnt]);
		//	break;
		case TIID_CenterPoint:
			szTest.Format(_T("%s"), g_szLT_TestItemCenterPt[pTestStep->nTestItemCnt]);
			break;
		case TIID_Rotation:
			szTest.Format(_T("%s"), g_szLT_TestItemRotate[pTestStep->nTestItemCnt]);
			break;
 		case TIID_EIAJ:
 			szTest.Format(_T("%s"), g_szLT_TestItemEIAJ[pTestStep->nTestItemCnt]);
 			break;
	//	case TIID_SFR:
			//szTest.Format(_T("%s"), g_szLT_TestItemSFR[pTestStep->nTestItemCnt]);
			//break;
		case TIID_FOV:
			szTest.Format(_T("%s"), g_szLT_TestItemAngle[pTestStep->nTestItemCnt]);
			break;
		case TIID_Color:
			szTest.Format(_T("%s"), g_szLT_TestItemColor[pTestStep->nTestItemCnt]);
			break;
		case TIID_Reverse:
			szTest.Format(_T("%s"), g_szLT_TestItemReverse[pTestStep->nTestItemCnt]);
			break;
		//case TIID_LEDTest:
		//	szTest.Format(_T("%s"), g_szLT_TestItemLED[pTestStep->nTestItemCnt]);
		//	break;
		case TIID_ParticleManual:
			szTest.Format(_T("%s"), g_szLT_TestItemParticleManual[pTestStep->nTestItemCnt]);
			break;

		case TIID_BlackSpot:
			szTest.Format(_T("%s"), g_szLT_TestItemBlackSpot[pTestStep->nTestItemCnt]);
			break;
		case TIID_DefectPixel:
			szTest.Format(_T("%s"), g_szLT_TestItemDefectPixel[pTestStep->nTestItemCnt]);
			break;
// 		case TIID_Reset:
// 			szTest.Format(_T("%s"), g_szLT_TestItemReset[pTestStep->nTestItemCnt]);
// 			break;
// 		case TIID_VideoSignal:
// 			szTest.Format(_T("%s"), g_szLT_TestItemVideoSignal[pTestStep->nTestItemCnt]);
// 			break;

		case TIID_Brightness:
			szTest.Format(_T("%s"), g_szLT_TestItemBrightness[pTestStep->nTestItemCnt]);
			break;

		case TIID_IRFilter:
			szTest.Format(_T("%s"), g_szLT_TestItemIRFilter[pTestStep->nTestItemCnt]);
			break;

		//case TIID_PatternNoise:
		//	szTest.Format(_T("%s"), g_szLT_TestItemPatternNoise[pTestStep->nTestItemCnt]);
		//	break;

		default:
			break;
		}
		
	//	SetItemText(iNewCount, TSH_TestItemCnt, szTest);
	}
	else
	{
	//	SetItemText(iNewCount, TSH_TestItemCnt, _T("-"));
	}

	// TSH_Retry
	//szText.Format(_T("%d"), pTestStep->nRetryCnt);
	//SetItemText(iNewCount, TSH_Retry, szText);

	// TSH_Delay
	if (0 < pTestStep->dwDelay)
	{
		szText.Format(_T("%d"), pTestStep->dwDelay);
	}
	else
	{
		szText = _T("0");
	}
	SetItemText(iNewCount, TSH_Delay, szText);

// 	// TSH_MoveAxis
// 	if (pTestStep->bAxisUse)
// 	{
// 		szText.Format(_T("%s"), g_lpszMotor_Axis[pTestStep->nAxisItem]);
// 	}
// 	else
// 	{
// 		szText = _T("-");
// 	}
// 	SetItemText(iNewCount, TSH_MoveAxis, szText);
// 
// 	// TSH_MoveX
// 	if (pTestStep->bAxisUse)
// 	{
// 		szText.Format(_T("%.1f"), pTestStep->dbMovePos);
// 	}
// 	else
// 	{
// 		szText = _T("-");
// 	}
// 	SetItemText(iNewCount, TSH_MovePos, szText);

	// 번호 재부여
	ResetOrderingNumbers();

	// 화면에 보이게 하기
	EnsureVisible(iNewCount, TRUE);
	ListView_SetItemState(GetSafeHwnd(), iNewCount, LVIS_FOCUSED | LVIS_SELECTED, 0x000F);
}

//=============================================================================
// Method		: InsertTestStep
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_StepUnit * pTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 14:22
// Desc.		:
//=============================================================================
void CList_TestStep::AddTestStep(const __in ST_StepUnit* pTestStep)
{
	ASSERT(GetSafeHwnd());
	if (NULL == pTestStep)
		return;

	int iNewCount = GetItemCount();

	InsertItem(iNewCount, _T(""));

	CString szText;

	// TSH_Master
	if (pTestStep->bMasterUse == TRUE)
		SetItemState(iNewCount, 0x2000, LVIS_STATEIMAGEMASK);
	else
		SetItemState(iNewCount, 0x1000, LVIS_STATEIMAGEMASK);

	// TSH_No
	szText.Format(_T("%d"), iNewCount);
	SetItemText(iNewCount, TSH_No, szText);

	// TSH_TestItem
	if (pTestStep->bTestUse)
	{
		SetItemText(iNewCount, TSH_TestItem, g_szLT_TestItem_Name[pTestStep->nTestItem]);
	}
	else
	{
		SetItemText(iNewCount, TSH_TestItem, _T("-"));
	}

	// TSH_TestItemCnt
	if (pTestStep->bTestUse)
	{
		CString szTest;
		switch (pTestStep->nTestItem)
		{
		case TIID_TestInitialize:
			szTest.Format(_T("%s"), g_szLT_TestItemInitial[pTestStep->nTestItemCnt]);
			break;
		case TIID_TestFinalize:
			szTest.Format(_T("%s"), g_szLT_TestItemFinal[pTestStep->nTestItemCnt]);
			break;
		case TIID_Current:
			szTest.Format(_T("%s"), g_szLT_TestItemCurrent[pTestStep->nTestItemCnt]);
			break;
		//case TIID_OperationMode:
		//	szTest.Format(_T("%s"), g_szLT_TestItemOperationMode[pTestStep->nTestItemCnt]);
		//	break;
		case TIID_CenterPoint:
			szTest.Format(_T("%s"), g_szLT_TestItemCenterPt[pTestStep->nTestItemCnt]);
			break;
		case TIID_Rotation:
			szTest.Format(_T("%s"), g_szLT_TestItemRotate[pTestStep->nTestItemCnt]);
			break;
 		case TIID_EIAJ:
 			szTest.Format(_T("%s"), g_szLT_TestItemEIAJ[pTestStep->nTestItemCnt]);
 			break;
		//case TIID_SFR:
		//	szTest.Format(_T("%s"), g_szLT_TestItemSFR[pTestStep->nTestItemCnt]);
		//	break;
		case TIID_FOV:
			szTest.Format(_T("%s"), g_szLT_TestItemAngle[pTestStep->nTestItemCnt]);
			break;
		case TIID_Color:
			szTest.Format(_T("%s"), g_szLT_TestItemColor[pTestStep->nTestItemCnt]);
			break;
		case TIID_Reverse:
			szTest.Format(_T("%s"), g_szLT_TestItemReverse[pTestStep->nTestItemCnt]);
			break;
		//case TIID_LEDTest:
		//	szTest.Format(_T("%s"), g_szLT_TestItemLED[pTestStep->nTestItemCnt]);
		//	break;
		case TIID_ParticleManual:
			szTest.Format(_T("%s"), g_szLT_TestItemParticleManual[pTestStep->nTestItemCnt]);
			break;

		case TIID_BlackSpot:
			szTest.Format(_T("%s"), g_szLT_TestItemBlackSpot[pTestStep->nTestItemCnt]);
			break;
		case TIID_DefectPixel:
			szTest.Format(_T("%s"), g_szLT_TestItemDefectPixel[pTestStep->nTestItemCnt]);
			break;
// 		case TIID_Reset:
// 			szTest.Format(_T("%s"), g_szLT_TestItemReset[pTestStep->nTestItemCnt]);
// 			break;
// 		case TIID_VideoSignal:
// 			szTest.Format(_T("%s"), g_szLT_TestItemVideoSignal[pTestStep->nTestItemCnt]);
// 			break;

		case TIID_Brightness:
			szTest.Format(_T("%s"), g_szLT_TestItemBrightness[pTestStep->nTestItemCnt]);
			break;

		case TIID_IRFilter:
			szTest.Format(_T("%s"), g_szLT_TestItemIRFilter[pTestStep->nTestItemCnt]);
			break;

		//case TIID_PatternNoise:
		//	szTest.Format(_T("%s"), g_szLT_TestItemPatternNoise[pTestStep->nTestItemCnt]);
		//	break;

		default:
			break;
		}

		//SetItemText(iNewCount, TSH_TestItemCnt, szTest);
	}
	else
	{
		//SetItemText(iNewCount, TSH_TestItemCnt, _T("-"));
	}

	// TSH_Retry
//	szText.Format(_T("%d"), pTestStep->nRetryCnt);
//	SetItemText(iNewCount, TSH_Retry, szText);

	// TSH_Delay
	if (0 < pTestStep->dwDelay)
	{
		szText.Format(_T("%d"), pTestStep->dwDelay);
	}
	else
	{
		szText = _T("0");
	}
	SetItemText(iNewCount, TSH_Delay, szText);

// 	// TSH_MoveAxis
// 	if (pTestStep->bAxisUse)
// 	{
// 		szText.Format(_T("%s"), g_lpszMotor_Axis[pTestStep->nAxisItem]);
// 	}
// 	else
// 	{
// 		szText = _T("-");
// 	}
// 	SetItemText(iNewCount, TSH_MoveAxis, szText);
// 
// 	// TSH_MoveX
// 	if (pTestStep->bAxisUse)
// 	{
// 		szText.Format(_T("%.1f"), pTestStep->dbMovePos);
// 	}
// 	else
// 	{
// 		szText = _T("-");
// 	}
// 	SetItemText(iNewCount, TSH_MovePos, szText);


	// 화면에 보이게 하기
	EnsureVisible(iNewCount, TRUE);
	ListView_SetItemState(GetSafeHwnd(), iNewCount, LVIS_FOCUSED | LVIS_SELECTED, 0x000F);
}

//=============================================================================
// Method		: Set_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_StepInfo * pstInStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 20:35
// Desc.		:
//=============================================================================
void CList_TestStep::Set_StepInfo(__in const ST_StepInfo* pstInStepInfo)
{
	m_stStepInfo.StepList.RemoveAll();
	DeleteAllItems();

	if (NULL != pstInStepInfo)
	{
		m_stStepInfo.StepList.Copy(pstInStepInfo->StepList);

		for (UINT nIdx = 0; nIdx < pstInStepInfo->StepList.GetCount(); nIdx++)
		{
			AddTestStep(&(pstInStepInfo->StepList[nIdx]));
		}
	}
}

//=============================================================================
// Method		: Get_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_StepInfo & stOutStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 23:27
// Desc.		:
//=============================================================================
void CList_TestStep::Get_StepInfo(__out ST_StepInfo& stOutStepInfo)
{
	stOutStepInfo.StepList.RemoveAll();
	stOutStepInfo.StepList.Copy(m_stStepInfo.StepList);
}

//=============================================================================
// Method		: Item_Add
// Access		: public  
// Returns		: void
// Parameter	: __in ST_StepUnit & stTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 23:22
// Desc.		:
//=============================================================================
void CList_TestStep::Item_Add(__in ST_StepUnit& stTestStep)
{
	POSITION posSel = GetFirstSelectedItemPosition();

	// 데이터 정상인가 확인
	if (m_stStepInfo.StepList.GetCount() != GetItemCount())
	{
		// 에러
		AfxMessageBox(_T("CList_TestStep::Item_Add() Data Count Error"));
		return;
	}

	if (MAX_STEP_COUNT < m_stStepInfo.StepList.GetCount())
	{
		// 에러
		AfxMessageBox(_T("Limit Max Step"));
		return;
	}

	m_stStepInfo.StepList.Add(stTestStep);
	AddTestStep(&stTestStep);
}

//=============================================================================
// Method		: Item_Insert
// Access		: public  
// Returns		: void
// Parameter	: __in ST_StepUnit & stTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 23:22
// Desc.		:
//=============================================================================
void CList_TestStep::Item_Insert(__in ST_StepUnit& stTestStep)
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		m_stStepInfo.StepList.InsertAt(iIndex, stTestStep);

		InsertTestStep(iIndex, &stTestStep);
	}
	else
	{
		// 항목을 선택 하세요.
	}
}

//=============================================================================
// Method		: Item_Remove
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CList_TestStep::Item_Remove()
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		DeleteItem(iIndex);
		ResetOrderingNumbers();

		m_stStepInfo.StepList.RemoveAt(iIndex);

		// 아이템 선택 활성화
		if (iIndex < GetItemCount())
		{
			SetSelectItem(iIndex);
		}
		else
		{
			SetSelectItem(iIndex - 1);
		}
	}
	else
	{
		// 항목을 선택 하세요.
	}
}

//=============================================================================
// Method		: Item_Up
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CList_TestStep::Item_Up()
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		// 0번 인덱스는 위로 이동 불가
		if ((0 < iIndex) && (1 < GetItemCount()))
		{
			ST_StepUnit stStep = m_stStepInfo.StepList.GetAt(iIndex);

			DeleteItem(iIndex);
			InsertTestStep(iIndex - 1, &stStep);

			m_stStepInfo.StepList.RemoveAt(iIndex);
			m_stStepInfo.StepList.InsertAt(iIndex - 1, stStep);

			// 아이템 선택 활성화
			SetSelectItem(iIndex - 1);
		}
	}
	else
	{
		// 항목을 선택 하세요.
	}
}

//=============================================================================
// Method		: Item_Down
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CList_TestStep::Item_Down()
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		// 마지막 인덱스는 아래로 이동 불가
		if ((iIndex < (GetItemCount() - 1)) && (1 < GetItemCount()))
		{
			ST_StepUnit stStep = m_stStepInfo.StepList.GetAt(iIndex);

			DeleteItem(iIndex);
			m_stStepInfo.StepList.RemoveAt(iIndex);

			// 변경되는 위치가 최하단이면, Insert 대신 Add 사용
			if ((iIndex + 1) < (GetItemCount()))
			{
				InsertTestStep(iIndex + 1, &stStep);
				m_stStepInfo.StepList.InsertAt(iIndex + 1, stStep);
			}
			else
			{
				AddTestStep(&stStep);
				m_stStepInfo.StepList.Add(stStep);
			}

			// 아이템 선택 활성화
			SetSelectItem(iIndex + 1);
		}
	}
	else
	{
		// 항목을 선택 하세요.
	}
}