﻿#ifndef List_ReverseSubOp_h__
#define List_ReverseSubOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_ReverseSubOp
{
	ReverseSubOp_Object = 0,
	ReverseSubOp_LeftTop,
	ReverseSubOp_RightTop,
	ReverseSubOp_LeftBottom,
	ReverseSubOp_RightBottom,
	ReverseSubOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_ReverseSubOp[] =
{
	_T(""),
	_T("Left Top"),
	_T("Right Top"),
	_T("Left Bottom"),
	_T("Right Bottom"),
	NULL
};

typedef enum enListItemNum_ReverseSubOp
{
	ReverseSubOp_ItemNum = ROI_Rv_resultMax,
};

static LPCTSTR	g_lpszItem_ReverseSubOp[] =
{
	NULL
};

const int	iListAglin_ReverseSubOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_ReverseSubOp[] =
{
	110,
	100,
	100,
	100,
	100,
	100,
	100,
};
// List_ReverseSubInfo

class CList_ReverseSubOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_ReverseSubOp)

public:
	CList_ReverseSubOp();
	virtual ~CList_ReverseSubOp();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_ReverseSub(ST_LT_TI_Reverse* pstReverse)
	{
		if (pstReverse == NULL)
			return;

		m_pstReverse = pstReverse;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_Reverse*  m_pstReverse;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;
	CComboBox	m_cb_CamType;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel	(UINT nFlags, short zDelta, CPoint pt);
		
	afx_msg void	OnEnKillFocusEdit		();
	afx_msg void	OnEnSelectFocusCombo();
	afx_msg void	OnEnKillFocusCombo();
};

#endif // List_ReverseSubInfo_h__
