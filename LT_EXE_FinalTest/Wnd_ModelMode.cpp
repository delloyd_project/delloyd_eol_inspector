﻿//*****************************************************************************
// Filename	: 	Wnd_ModelMode.cpp
// Created	:	2016/3/11 - 14:59
// Modified	:	2016/3/11 - 14:59
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
// Wnd_ModelMode.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_ModelMode.h"
#include "resource.h"

#define IDC_CB_MODEL			1004

//=============================================================================
// CWnd_ModelMode
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_ModelMode, CWnd_BaseView)

CWnd_ModelMode::CWnd_ModelMode()
{
	VERIFY(m_font_Large.CreateFont(
		36,							// nHeight
		0,							// nWidth
		0,							// nEscapement
		0,							// nOrientation
		FW_BOLD,					// nWeight
		FALSE,						// bItalic
		FALSE,						// bUnderline
		0,							// cStrikeOut
		ANSI_CHARSET,				// nCharSet
		OUT_DEFAULT_PRECIS,			// nOutPrecision
		CLIP_DEFAULT_PRECIS,		// nClipPrecision
		ANTIALIASED_QUALITY,		// nQuality
		VARIABLE_PITCH,				// nPitchAndFamily
		_T("Arial")));		// lpszFacename

	VERIFY(m_font_Default.CreateFont(
		20,							// nHeight
		0,							// nWidth
		0,							// nEscapement
		0,							// nOrientation
		FW_BOLD,					// nWeight
		FALSE,						// bItalic
		FALSE,						// bUnderline
		0,							// cStrikeOut
		ANSI_CHARSET,				// nCharSet
		OUT_DEFAULT_PRECIS,			// nOutPrecision
		CLIP_DEFAULT_PRECIS,		// nClipPrecision
		ANTIALIASED_QUALITY,		// nQuality
		VARIABLE_PITCH,				// nPitchAndFamily
		_T("Arial")));		// lpszFacename

	m_szModelPath = _T("C:\\Model\\");
	m_szFileExt = _T("luri");

	m_szAppName = _T("Common");
	m_szKeyName = _T("ModelCode");

	m_bFindModelFile = FALSE;
}

CWnd_ModelMode::~CWnd_ModelMode()
{
	m_font_Default.DeleteObject();
	m_font_Large.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_ModelMode, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_WM_GETMINMAXINFO()
	ON_CBN_SELENDOK	(IDC_CB_MODEL,	OnCbnSelEndCbModel	)
	ON_BN_CLICKED	(IDOK,			OnBnClickedOK		)
	ON_BN_CLICKED	(IDCANCEL,		OnBnClickedCancel	)
END_MESSAGE_MAP()

//=============================================================================
// CWnd_ModelMode message handlers
//=============================================================================
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/11 - 15:00
// Desc.		:
//=============================================================================
int CWnd_ModelMode::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Model.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Model.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Model.SetFont_Gdip(L"arial", 16.0F);
	m_st_Model.Create(_T("Selected Model File"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Model_CB.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Model_CB.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Model_CB.SetFont_Gdip(L"arial", 9.0F);
	m_st_Model_CB.Create(_T("Model File"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_ModelFile.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_ModelFile.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_ModelFile.SetFont_Gdip(L"arial", 9.0F);
	m_st_ModelFile.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_cb_Model.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_MODEL);

	m_bn_OK.Create(_T("OK"), dwStyle | WS_TABSTOP, rectDummy, this, IDOK);
	m_bn_Cancel.Create(_T("Cancel"), dwStyle | WS_TABSTOP, rectDummy, this, IDCANCEL);
	
	m_cb_Model.SetFont(&m_font_Default);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/11 - 15:00
// Desc.		:
//=============================================================================
void CWnd_ModelMode::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iMagrin;
	int iTop = iMagrin;
	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;
	int iCtrlWidth = 120;
	int iCtrlHeight = 35;
	int iCtrlHeight_MSG = 50;
	int iTempWidth = iWidth - iSpacing - iCtrlWidth;
	int iSubLeft = iLeft;

	m_st_Model.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);
	iTop += iCtrlHeight + iMagrin;

	m_st_Model_CB.MoveWindow(iLeft, iTop, iWidth - iTempWidth, 28);
	iLeft += iWidth - iTempWidth + iSpacing;
	m_cb_Model.MoveWindow(iLeft, iTop, iTempWidth - iSpacing, 20);

	iTop += 28 + iMagrin;
	iLeft = iMagrin;
	m_st_ModelFile.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);

	iTop += iCtrlHeight + iMagrin;
	iWidth = (iWidth - iMagrin) / 2;

	m_bn_OK.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);

	iLeft += iWidth + iMagrin;
	m_bn_Cancel.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);
}

//=============================================================================
// Method		: OnGetMinMaxInfo
// Access		: protected  
// Returns		: void
// Parameter	: MINMAXINFO * lpMMI
// Qualifier	:
// Last Update	: 2017/1/11 - 11:51
// Desc.		:
//=============================================================================
void CWnd_ModelMode::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMaxTrackSize.x = 420;
	lpMMI->ptMaxTrackSize.y = 200;

	lpMMI->ptMinTrackSize.x = 420;
	lpMMI->ptMinTrackSize.y = 200;

	CWnd::OnGetMinMaxInfo(lpMMI);
}

//=============================================================================
// Method		: OnCbnSelEndCbModel
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/2 - 17:01
// Desc.		:
//=============================================================================
void CWnd_ModelMode::OnCbnSelEndCbModel()
{
	int iSel = m_cb_Model.GetCurSel();

	if (0 <= iSel)
	{
		m_cb_Model.GetLBText(iSel, m_szFindModelFile);

		CString szValue;
		szValue.Format(_T("%s.%s"), m_szFindModelFile, m_szFileExt);
		m_st_ModelFile.SetText(szValue);

		m_bFindModelFile = TRUE;
	}
}

//=============================================================================
// Method		: OnBnClickedOK
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/22 - 10:28
// Desc.		:
//=============================================================================
void CWnd_ModelMode::OnBnClickedOK()
{
	if (m_szFindModelFile.IsEmpty() == FALSE)
		GetOwner()->SendMessage(WM_CHANGED_MODEL, (WPARAM)m_szFindModelFile.GetBuffer(), TRUE);

	m_szFindModelFile.ReleaseBuffer();

	AfxGetApp()->GetMainWnd()->EnableWindow(TRUE);
	this->ShowWindow(SW_HIDE);
}

//=============================================================================
// Method		: OnBnClickedCancel
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/22 - 10:28
// Desc.		:
//=============================================================================
void CWnd_ModelMode::OnBnClickedCancel()
{
	if (m_szFindModelFile.IsEmpty() == FALSE)
		GetOwner()->SendMessage(WM_CHANGED_MODEL, (WPARAM)m_szFindModelFile.GetBuffer(), TRUE);

	m_szFindModelFile.ReleaseBuffer();
	AfxGetApp()->GetMainWnd()->EnableWindow(TRUE);
	this->ShowWindow(SW_HIDE);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/6/22 - 11:01
// Desc.		:
//=============================================================================
void CWnd_ModelMode::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (bShow)
	{

		m_fileWatch.SetWatchOption(m_szModelPath, m_szFileExt);
		m_fileWatch.RefreshList();
		CStringList* strModelList = m_fileWatch.GetFileList();

		POSITION pos;
		CString szValue;

		m_cb_Model.ResetContent();
		for (pos = strModelList->GetHeadPosition(); pos != NULL;)
		{
			m_cb_Model.AddString(strModelList->GetNext(pos));
		}

		if (!m_szFindModelFile.IsEmpty())
		{
			int iSel = m_cb_Model.FindStringExact(0, m_szFindModelFile);

			if (0 <= iSel)
			{
				m_cb_Model.SetCurSel(iSel);
				m_cb_Model.GetLBText(iSel, m_szFindModelFile);

				szValue.Format(_T("%s.%s"), m_szFindModelFile, m_szFileExt);
				m_st_ModelFile.SetText(szValue);
			}
		}

		//m_szFindModelFile.Empty();
	}
}

//=============================================================================
// Method		: ReadModelCode
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out CString & szModelCode
// Qualifier	:
// Last Update	: 2017/1/10 - 10:13
// Desc.		:
//=============================================================================
BOOL CWnd_ModelMode::ReadModelCode(__in LPCTSTR szPath, __out CString& szModelCode)
{
	szModelCode;
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	// 모델명
	if (0 < GetPrivateProfileString(m_szAppName, m_szKeyName, _T(""), inBuff, 255, szPath))
	{
		szModelCode = inBuff;
		return TRUE;
	}
	else
	{
		return FALSE;
	}


	return TRUE;
}

//=============================================================================
// Method		: FindModelByModelCode
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szCode
// Qualifier	:
// Last Update	: 2017/1/10 - 10:13
// Desc.		:
//=============================================================================
BOOL CWnd_ModelMode::FindModelByModelCode(__in LPCTSTR szCode)
{
	CString		strFindModel;
	CString		szReadedCode;
	CFileFind	finder;
	CString		szInputCode = szCode;

	szInputCode.MakeUpper();
	m_bFindModelFile = FALSE;

	CString strWildcard;
	strWildcard.Format(_T("%s*.%s"), m_szModelPath, m_szFileExt);

	// start working for files
	BOOL bWorking = finder.FindFile(strWildcard);

	while (bWorking)
	{
		bWorking = finder.FindNextFile();

		if (finder.IsDots())
			continue;

		if (finder.IsDirectory())
			continue;

		if (finder.IsArchived())
		{
			if (ReadModelCode(finder.GetFilePath(), szReadedCode))
			{
				szReadedCode.MakeUpper();
				if (0 == szReadedCode.Compare(szInputCode))
				{
					strFindModel = finder.GetFileTitle();
					m_szFindModelFile = strFindModel;
					m_bFindModelFile = TRUE;
					break;
				}
			}
			else
			{
				if (0 == szInputCode.GetLength())
				{
					m_bFindModelFile = TRUE;
					break;
				}
			}
		}
	}

	finder.Close();

	if (m_bFindModelFile)
	{

		strFindModel.Format(_T("%s.%s"), m_szFindModelFile, m_szFileExt);
		m_st_ModelFile.SetWindowText(strFindModel);
		OnBnClickedOK();
		return TRUE;
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: SetCurrentModel
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szModel
// Qualifier	:
// Last Update	: 2017/3/2 - 17:25
// Desc.		:
//=============================================================================
void CWnd_ModelMode::SetCurrentModel(__in LPCTSTR szModel)
{
	m_szFindModelFile = szModel;
}

//=============================================================================
// Method		: GetModelFile
// Access		: public  
// Returns		: CString
// Qualifier	:
// Last Update	: 2017/1/10 - 11:53
// Desc.		:
//=============================================================================
CString CWnd_ModelMode::GetModelFile()
{
	return m_szFindModelFile;
}

