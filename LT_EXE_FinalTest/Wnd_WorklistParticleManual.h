﻿//*****************************************************************************
// Filename	: Wnd_WorklistParticleManual.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistParticleManual_h__
#define Wnd_WorklistParticleManual_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_ParticleManual.h"

//=============================================================================
// Wnd_WorklistParticleManual
//=============================================================================
class CWnd_WorklistParticleManual : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistParticleManual)

public:
	CWnd_WorklistParticleManual();
	virtual ~CWnd_WorklistParticleManual();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

	CList_Work_ParticleManual				m_list_ParticleManual[TICnt_ParticleManual];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistParticleManual_h__


