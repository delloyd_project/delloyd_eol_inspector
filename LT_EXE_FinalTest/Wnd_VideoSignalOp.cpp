﻿// Wnd_VideoSignalOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"

#include "Wnd_VideoSignalOp.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_VideoSignalOp
typedef enum VideoSignalOptID
{
	IDC_BTN_ITEM   = 1001,
	IDC_CMB_ITEM   = 2001,
	IDC_EDT_ITEM   = 3001,
	IDC_LIST_ITEM  = 4001,
	IDC_BTN_INDEX  = 5001,
};

IMPLEMENT_DYNAMIC(CWnd_VideoSignalOp, CWnd)

CWnd_VideoSignalOp::CWnd_VideoSignalOp()
{
	m_pstModelInfo	= NULL;
	m_nTestItemCnt = 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_VideoSignalOp::~CWnd_VideoSignalOp()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_VideoSignalOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_VideoSignalOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_VideoSignalOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < BTN_VIDEOSIGNAL_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szVideoSignalButton[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_VideoSignalOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

//	int iIdxCnt = PtOp_ItemNum;
	int iMargin = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	int iHeaderH = 40;
	int iCtrl_W = iWidth / 5;
	int iCtrl_H = 25;

	int iSTWidth = iWidth / 5;
	int iSTHeight = 25;

// 	int iList_H = iHeaderH + iIdxCnt * 15;
// 
// 	if (iIdxCnt <= 0 || iList_H > iHeight)
// 	{
// 		iList_H = iHeight;
// 	}

	iLeft = cx - iMargin - iSTWidth;
	m_bn_Item[BTN_VIDEOSIGNAL_TEST].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_VideoSignalOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_VideoSignalOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (NULL == m_pstModelInfo)
		return;

	if (TRUE == bShow)
	{
		//m_pstModelInfo->nTestMode = TIID_VideoSignal;
		m_pstModelInfo->nTestCnt = m_nTestItemCnt;
		m_pstModelInfo->nPicItem = PIC_VideoSignal;
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_VideoSignalOp::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;

	switch (nIdex)
	{
	case BTN_VIDEOSIGNAL_TEST:
		//GetOwner()->SendMessage(WM_MANUAL_TEST, m_nTestItemCnt, TIID_VideoSignal);
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_VideoSignalOp::SetUpdateData()
{
	if (m_pstModelInfo == NULL)
		return;
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Parameter	: __out ST_TestItemOpt & stTestItemOpt
// Qualifier	:
// Last Update	: 2017/10/14 - 18:07
// Desc.		:
//=============================================================================
void CWnd_VideoSignalOp::GetUpdateData()
{
	if (m_pstModelInfo == NULL)
		return;
}
