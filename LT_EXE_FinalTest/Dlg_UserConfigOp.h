#pragma once
#include "resource.h"
#include "Wnd_Cfg_UserConfig.h"

// CDlg_UserConfigOp 대화 상자입니다.

class CDlg_UserConfigOp : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_UserConfigOp)

public:
	CDlg_UserConfigOp(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_UserConfigOp();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_USER_CONFIG };

	CMFCButton m_bn_Save;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg void	OnBnClickedBnSave();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
	CWnd_Cfg_UserConfig m_wndUserConfig;
	CString m_szUserConfigPath;

	ST_UserConfigInfo* m_pstUserConfigInfo;

public:

	void SetPtr_UserConfigInfo(__in ST_UserConfigInfo*  pstUserConfigInfo)
	{
		if (pstUserConfigInfo == NULL)
			return;

		m_pstUserConfigInfo = pstUserConfigInfo;
	};

	void Set_UserConfigInfo(__in const ST_UserConfigInfo* pstUserConfigInfo);
	void Get_UserConfigInfo(__out ST_UserConfigInfo& stUserConfigInfo);
	void SetPath(__in LPCTSTR szUserConfigPath);

};
