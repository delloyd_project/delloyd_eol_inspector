﻿#ifndef Wnd_IRFilterOp_h__
#define Wnd_IRFilterOp_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "Def_DataStruct.h"

// CWnd_IRFilterOp
enum eniRStatic
{
	STI_IR_dBThr,
	STI_IR_EnvThr,
	STI_IR_MAX,
};

static LPCTSTR	g_szIRStatic[] =
{
	_T("Test Thr"),
	_T("Environment Thr"),
	NULL
};

enum enIRFilterButton
{
	BTN_IR_TEST = 0,
	BTN_IR_MAX,
};

static LPCTSTR	g_szIRFilterButton[] =
{
	_T("TEST"),
	NULL
};

enum enIREdit
{
	EDT_IR_dBThr,
	EDT_IR_EnvThr,
	EDT_IR_MAX,
};

class CWnd_IRFilterOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_IRFilterOp)

public:
	CWnd_IRFilterOp();
	virtual ~CWnd_IRFilterOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo			*m_pstModelInfo;

	CFont				m_font;
	CVGStatic			m_st_Item[STI_IR_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_IR_MAX];
	CButton				m_bn_Item[BTN_IR_MAX];

	// 검사 항목이 다수 인경우
	UINT				m_nTestItemCnt;

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_IRFilterOp_h__
