﻿// Wnd_Cfg_LabelPrinter.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Wnd_Cfg_LabelPrinter.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_Cfg_LabelPrinter
typedef enum LabelPrinterID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_LabelPrinter, CWnd)

CWnd_Cfg_LabelPrinter::CWnd_Cfg_LabelPrinter()
{
	m_pstModelInfo	= NULL;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font2.CreateFont(
		18,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_LabelPrinter::~CWnd_Cfg_LabelPrinter()
{
	m_font.DeleteObject();
	m_font2.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_LabelPrinter, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_Cfg_LabelPrinter 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Cfg_LabelPrinter::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_LABEL_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szLabelStatic[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < BTN_LABEL_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szLabelButton[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_LABEL_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_LABEL_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
		m_cb_Item[nIdex].SetFont(&m_font2);
	}
	
	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);

	if (!m_szPrnPath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_szPrnPath, PRN_FILE_EXT);
		m_IniWatch.RefreshList();

		RefreshFileList(CMB_LABEL_PRNFiLE, m_IniWatch.GetFileList());
		m_cb_Item[CMB_LABEL_PRNFiLE].SetCurSel(0);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_LabelPrinter::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iIdxCnt = LabelPrt_ItemNum;
	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	int iHeaderH = 65;
	int iList_H = iHeaderH + iIdxCnt * 16;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	int iCtrl_W = iWidth / 5;
	int iCtrl_H = 25;

	iLeft = iMargin;
	m_st_Item[STI_LABEL_PRNFILE].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iLeft += iCtrl_W + 1;
	m_cb_Item[CMB_LABEL_PRNFiLE].MoveWindow(iLeft, iTop, iCtrl_W * 1.5, 100);

	iLeft = cx - iMargin - iCtrl_W;
	m_bn_Item[BTN_LABEL_SAVE].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);
	
	iTop += iCtrl_H + iSpacing;
	iLeft = iMargin;
	m_st_Item[STI_LABEL_COUNT].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iLeft += iCtrl_W + 1;
	m_ed_Item[EDT_LABEL_COUNT].MoveWindow(iLeft, iTop, iCtrl_W * 1.5, iCtrl_H);

	iLeft = cx - iMargin - iCtrl_W;
	m_bn_Item[BTN_LABEL_TEST].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iTop += iCtrl_H + iSpacing;
	iLeft = iMargin;
	m_st_Item[STI_LABEL_SERIAL_NUMBER].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iLeft += iCtrl_W + 1;
	m_ed_Item[EDT_LABEL_SERIAL_NUMBER].MoveWindow(iLeft, iTop, iCtrl_W * 1.5, iCtrl_H);

	

	iLeft = iMargin;
	iTop += iCtrl_H + iSpacing;
	m_List.MoveWindow(iLeft, iTop, iWidth, cy - iTop - iMargin);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_LabelPrinter::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}


void CWnd_Cfg_LabelPrinter::SetPath(__in LPCTSTR szPrnFilePath)
{
	m_szPrnPath = szPrnFilePath;
}

void CWnd_Cfg_LabelPrinter::RefreshFileList(__in UINT nComboID, __in const CStringList* pFileList)
{
	m_cb_Item[nComboID].ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();

	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_cb_Item[nComboID].AddString(pFileList->GetNext(pos));
	}

	// 초기화
	if (0 < pFileList->GetCount())
	{
		m_cb_Item[nComboID].SetCurSel(0);
	}
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Cfg_LabelPrinter::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (NULL == m_pstModelInfo)
		return;

	if (TRUE == bShow)
	{
		//m_pstModelInfo->nTestMode = TIID_SFR;
		//m_pstModelInfo->nPicItem = PIC_SFR;
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_LabelPrinter::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
	CString szBuf;
	CString szBuf2;

	switch (nIdex)
	{
	case BTN_LABEL_TEST:
		m_ed_Item[EDT_LABEL_COUNT].GetWindowTextW(szBuf);
		m_ed_Item[EDT_LABEL_SERIAL_NUMBER].GetWindowTextW(szBuf2);
		GetOwner()->SendMessage(WM_PRINTER_TEST, _ttoi(szBuf), _ttoi(szBuf2));
		break;

	case BTN_LABEL_SAVE:
		GetUpdateData();
		GetOwner()->SendMessage(WM_MODEL_SAVE, 0, 0);
		break;

	default:
		break;
	}
}


//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_LabelPrinter::SetUpdateData()
{
	if (m_pstModelInfo == NULL)
		return;

	m_List.SetPtr_LabelPrinter(&m_pstModelInfo->stPrinter);
	m_List.InsertFullData();

	CString strValue;

	strValue = m_szPrnPath + m_pstModelInfo->stPrinter.szPrnFileName;

// 	if (!m_szPrnPath.IsEmpty())
// 	{
// 		m_IniWatch.SetWatchOption(m_szPrnPath, I2C_FILE_EXT);
// 		m_IniWatch.RefreshList();
// 
// 		RefreshFileList(CMB_LABEL_PRNFiLE, m_IniWatch.GetFileList());
// 
// 		int iSel = m_cb_Item[CMB_LABEL_PRNFiLE].FindStringExact(0, m_pstModelInfo->stPrinter.szPrnFileName);
// 
// 		if (0 <= iSel)
// 			m_cb_Item[CMB_LABEL_PRNFiLE].SetCurSel(iSel);
// 		else 
// 			m_cb_Item[CMB_LABEL_PRNFiLE].SetCurSel(0);
// 	}
// 	else
// 	{
// 		m_cb_Item[CMB_LABEL_PRNFiLE].SetCurSel(0);
// 	}

	int iSel = m_cb_Item[CMB_LABEL_PRNFiLE].FindStringExact(0, m_pstModelInfo->stPrinter.szPrnFileName);

	if (0 <= iSel)
		m_cb_Item[CMB_LABEL_PRNFiLE].SetCurSel(iSel);
	else
		m_cb_Item[CMB_LABEL_PRNFiLE].SetCurSel(0);
	
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/14 - 18:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_LabelPrinter::GetUpdateData()
{
	if (m_pstModelInfo == NULL)
		return;

	CString strValue;
	m_cb_Item[CMB_LABEL_PRNFiLE].GetWindowText(strValue);
	m_pstModelInfo->stPrinter.szPrnFileName = strValue;

}
