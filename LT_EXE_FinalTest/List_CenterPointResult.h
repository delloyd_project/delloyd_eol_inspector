﻿#ifndef List_CenterPointResult_h__
#define List_CenterPointResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_CenterPointResult
{
	CPResult_Object = 0,
	CPResult_AxisValue,
	CPResult_Reference,
	CPResult_Offset,
	CPResult_Result,
	CPResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_CenterPointResult[] =
{
	_T(""),
	_T("Axis Value"),
	_T("Reference"),
	_T("Offset"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_CenterPointResult
{
	CPResult_ItemNum = 2,
};

static LPCTSTR	g_lpszItem_CenterPointResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_CenterPointResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_CenterPointResult[] =
{
	80,
	80,
	80,
	80,
	80,
};
// List_CenterPointInfo

class CList_CenterPointResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_CenterPointResult)

public:
	CList_CenterPointResult();
	virtual ~CList_CenterPointResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_CenterPoint(ST_LT_TI_CenterPoint* pstCenterPoint)
	{
		if (pstCenterPoint == NULL)
			return;

		m_pstCenterPoint = pstCenterPoint;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_CenterPoint*  m_pstCenterPoint;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_CenterPointInfo_h__
