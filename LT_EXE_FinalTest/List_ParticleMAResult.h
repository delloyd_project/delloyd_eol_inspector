﻿#ifndef List_ParticleMAResult_h__
#define List_ParticleMAResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_ParticleMAResult
{
	ParticleMAResult_Object = 0,
	ParticleMAResult_Result,
	ParticleMAResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_ParticleMAResult[] =
{
	_T(""),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_ParticleMAResult
{
	ParticleMAResult_ItemNum = 1,
};

static LPCTSTR	g_lpszItem_ParticleMAResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_ParticleMAResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_ParticleMAResult[] =
{
	80,
	80,
};

// List_ParticleMAInfo

class CList_ParticleMAResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_ParticleMAResult)

public:
	CList_ParticleMAResult();
	virtual ~CList_ParticleMAResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_ParticleMA(ST_LT_TI_ParticleMA* pstParticleMA)
	{
		if (pstParticleMA == NULL)
			return;

		m_pstParticleMA = pstParticleMA;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_ParticleMA*  m_pstParticleMA;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_ParticleMAInfo_h__
