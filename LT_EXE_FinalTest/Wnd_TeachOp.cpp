﻿// Wnd_TeachOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_TeachOp.h"

// CWnd_TeachOp

IMPLEMENT_DYNAMIC(CWnd_TeachOp, CWnd)

typedef enum TeachOp_ID
{
	IDC_TC_BN_SAVE = 1100,
	//IDC_TC_BN_Loading,
	//IDC_TC_BN_Inspection,
	//IDC_TC_BN_Par_Loading,
	//IDC_TC_BN_Par_Cylinder,
	//IDC_TC_BN_Par_Inspection,
	IDC_TC_ED_ITEM1	= 1200,
	IDC_TC_ED_ITEM2,
	IDC_TC_ED_ITEM3,
	IDC_TC_ED_ITEM4,
	IDC_TC_ED_ITEM5,
	IDC_TC_ED_ITEM6,
	IDC_TC_ED_ITEM7,
	IDC_TC_ED_ITEM8,
	IDC_TC_ED_ITEM9,
	IDC_TC_ED_ITEM10,
	IDC_TC_ED_ITEM11,
	IDC_TC_ED_ITEM12,
	IDC_TC_ED_ITEM13,
	IDC_TC_ED_ITEM14,
	IDC_TC_MAXNUM,
};

CWnd_TeachOp::CWnd_TeachOp()
{
	m_nItemCnt = 0;

	m_pstMaintenanceInfo = NULL;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_TeachOp::~CWnd_TeachOp()
{
	m_font.DeleteObject		();
}

BEGIN_MESSAGE_MAP(CWnd_TeachOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_TC_BN_SAVE, OnBnClickedBnSave)
	/*ON_BN_CLICKED(IDC_TC_BN_Inspection, OnBnClickedBn_Inspection)
	ON_BN_CLICKED(IDC_TC_BN_Loading, OnBnClickedBn_Loading)
	ON_BN_CLICKED(IDC_TC_BN_Par_Inspection, OnBnClickedBn_Par_Inspection)
	ON_BN_CLICKED(IDC_TC_BN_Par_Loading, OnBnClickedBn_Par_Loading)
	ON_BN_CLICKED(IDC_TC_BN_Par_Cylinder, OnBnClickedBn_Par_Cylinder)*/
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/28 - 17:02
// Desc.		:
//=============================================================================
int CWnd_TeachOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwEtStyle = WS_VISIBLE | WS_BORDER | ES_CENTER;
	DWORD dwStyle	= WS_VISIBLE | WS_CHILD;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Name.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Name.SetBackColor_COLORREF(RGB(33, 73, 125));
	m_st_Name.SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
	m_st_Name.SetFont_Gdip(L"Arial", 12.0F);
	m_st_Name.Create(_T("MOTOR TEACH PARAMETER SETTING"), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < TC_ALL_MAX; nIdx++)						// TC_ALL_MAX = m_nItemCnt  임시변경[12/21/2018 Seongho.Lee]
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].SetTextAlignment(StringAlignmentNear);
		m_st_Item[nIdx].Create(g_szTeachItem_Image[nIdx], dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < BN_TC_MAXNUM; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szTeachOpButton[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_TC_BN_SAVE + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; nIdx < TC_ALL_MAX; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwEtStyle, rectDummy, this, IDC_TC_ED_ITEM1 + nIdx);
		m_ed_Item[nIdx].SetWindowText(_T("0"));
		m_ed_Item[nIdx].SetValidChars(_T("0123456789.-"));
		m_ed_Item[nIdx].SetFont(&m_font);
	}

	m_st_Comm.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Comm.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Comm.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Comm.Create(_T("COMM"), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
void CWnd_TeachOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMargin  = 5;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth	 = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	int iEdWidth = (iWidth - iSpacing * 9) / 8;

	int iEditHeight = (iHeight - iSpacing * 11) / 10; 
	int iBtnWidth = 0;
	int iBtnHeight = (iHeight - iSpacing * 11) / 7 ;
	int iTitleH = 35;
	
	int iWidthTemp = (iWidth - iSpacing * 3) / 2.5;
	int iWidthItem = (iWidthTemp - iSpacing) / 2;
	m_st_Name.MoveWindow(0, iTop, cx, iTitleH);

	iLeft = iMargin;
	iTop += iTitleH + iSpacing;

	m_st_Comm.MoveWindow(iLeft, iTop, cx, iEditHeight);
	//////////////////////////////////Loading

	iLeft = iSpacing;
	iTop += iEditHeight + iSpacing;
	m_st_Item[TC_ImageTest_Loading_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft += iWidthItem + iSpacing;
	m_ed_Item[TC_ImageTest_Loading_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft = iSpacing;
	iTop += iEditHeight + iSpacing;
	m_st_Item[TC_ImageTest_Loading_Y].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft += iWidthItem + iSpacing;
	m_ed_Item[TC_ImageTest_Loading_Y].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft =  iSpacing;
	iTop += iEditHeight + iSpacing;
	m_st_Item[TC_ImageTest_Loading_Dist].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft += iWidthItem + iSpacing;
	m_ed_Item[TC_ImageTest_Loading_Dist].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft =  iSpacing;
	iTop += iEditHeight + iSpacing;
	m_st_Item[TC_ImageTest_Crash_Start].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft += iWidthItem + iSpacing;
	m_ed_Item[TC_ImageTest_Crash_Start].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft = iSpacing;
	iTop += iEditHeight + iSpacing;
	m_st_Item[TC_ImageTest_Crash_End].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft += iWidthItem + iSpacing;
	m_ed_Item[TC_ImageTest_Crash_End].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);


	//////////////////////////////////Inspection
	/*iLeft = (iWidthTemp + iSpacing) + iSpacing;
	iTop = iTitleH + iSpacing + iMargin + iEditHeight + iSpacing;
	m_st_Item[TC_ImageTest_Inspection_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft += iWidthItem + iSpacing;
	m_ed_Item[TC_ImageTest_Inspection_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft = (iWidthTemp + iSpacing) + iSpacing;
	iTop += iEditHeight + iSpacing;
	m_st_Item[TC_ImageTest_Inspection_Y].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft += iWidthItem + iSpacing;
	m_ed_Item[TC_ImageTest_Inspection_Y].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft = (iWidthTemp + iSpacing) + iSpacing;
	iTop += iEditHeight + iSpacing;
	m_st_Item[TC_ImageTest_Inspection_Dist].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft += iWidthItem + iSpacing;
	m_ed_Item[TC_ImageTest_Inspection_Dist].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);
*/
	//iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
	//iTop += iEditHeight + iSpacing;
	//m_st_Item[TC_ImageTest_Par_Loading_Dist].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	//iLeft += iWidthItem + iSpacing;
	//m_ed_Item[TC_ImageTest_Par_Loading_Dist].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);


	//////////////////////////////////Particle
	//iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
	iLeft = (iWidthTemp + iSpacing) + iSpacing;
	iTop = iTitleH + iSpacing + iMargin + iEditHeight + iSpacing;
	m_st_Item[TC_ImageTest_Par_Inspection_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft += iWidthItem + iSpacing;
	m_ed_Item[TC_ImageTest_Par_Inspection_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	//iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
	iLeft = (iWidthTemp + iSpacing) + iSpacing;
	iTop += iEditHeight + iSpacing;
	m_st_Item[TC_ImageTest_Par_Inspection_Y].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft += iWidthItem + iSpacing;
	m_ed_Item[TC_ImageTest_Par_Inspection_Y].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	//iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
	iLeft = (iWidthTemp + iSpacing) + iSpacing;
	iTop += iEditHeight + iSpacing;
	m_st_Item[TC_ImageTest_Par_Inspectoin_Dist].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft += iWidthItem + iSpacing;
	m_ed_Item[TC_ImageTest_Par_Inspectoin_Dist].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	//iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
	iLeft = (iWidthTemp + iSpacing) + iSpacing;
	iTop += iEditHeight + iSpacing;
	m_st_Item[TC_ImageTest_Par_Loading_Dist].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

	iLeft += iWidthItem + iSpacing;
	m_ed_Item[TC_ImageTest_Par_Loading_Dist].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);


	//////////////////////////////////button

	/*iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
	iTop = iTitleH + iSpacing + iMargin;
	m_bn_Item[BN_TC_Loading].MoveWindow(iLeft, iTop, iWidthTemp, iBtnHeight);

	iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
	iTop += iBtnHeight + iSpacing;
	m_bn_Item[BN_TC_Inspection].MoveWindow(iLeft, iTop, iWidthTemp, iBtnHeight);

	iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
	iTop += iBtnHeight + iSpacing;
	m_bn_Item[BN_TC_Par_Loading].MoveWindow(iLeft, iTop, iWidthTemp, iBtnHeight);

	iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
	iTop += iBtnHeight + iSpacing;
	m_bn_Item[BN_TC_Par_Cylinder].MoveWindow(iLeft, iTop, iWidthTemp, iBtnHeight);

	iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
	iTop += iBtnHeight + iSpacing;
	m_bn_Item[BN_TC_Par_Inspection].MoveWindow(iLeft, iTop, iWidthTemp, iBtnHeight);*/

	//iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
	//iTop += iBtnHeight + iSpacing; //= cy - iEditHeight - iSpacing;
	iTop = iTitleH + iSpacing + iMargin + iEditHeight + iSpacing;
	iLeft += iWidthItem + iSpacing;
	iWidthTemp = cx - iLeft - iMargin;
	m_bn_Item[BN_TC_SAVE].MoveWindow(iLeft, iTop, iWidthTemp, iBtnHeight);

}

//=============================================================================
// Method		: OnBnClickedBnSave
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 9:56
// Desc.		:
//=============================================================================
void CWnd_TeachOp::OnBnClickedBnSave()
{
	// UI 데이터 수집
	GetUpdateData();

	// 수집된 데이터 저장
	m_pstDevice->SaveMotionInfo();

	CString szMaintenanceFullPath;

	m_fileMaintenance.SaveTeachFile(m_pstMaintenanceInfo->szMaintenanceFullPath, &m_pstMaintenanceInfo->stTeachInfo);
	SetUpdateData();
}

////=============================================================================
//// Method		: OnBnClickedBnSave
//// Access		: public  
//// Returns		: void
//// Qualifier	:
//// Last Update	: 2017/3/30 - 9:56
//// Desc.		:
////=============================================================================
//void CWnd_TeachOp::OnBnClickedBn_Loading()
//{
//	GetOwner()->SendNotifyMessageW(WM_MOTOR_LOADING, 0, 0);
//}
////=============================================================================
//// Method		: OnBnClickedBnSave
//// Access		: public  
//// Returns		: void
//// Qualifier	:
//// Last Update	: 2017/3/30 - 9:56
//// Desc.		:
////=============================================================================
//void CWnd_TeachOp::OnBnClickedBn_Inspection()
//{
//	GetOwner()->SendNotifyMessageW(WM_MOTOR_INSPECTION, 0, 0);
//}
////=============================================================================
//// Method		: OnBnClickedBnSave
//// Access		: public  
//// Returns		: void
//// Qualifier	:
//// Last Update	: 2017/3/30 - 9:56
//// Desc.		:
////=============================================================================
//void CWnd_TeachOp::OnBnClickedBn_Par_Loading()
//{
//	GetOwner()->SendNotifyMessageW(WM_MOTOR_PAR_LOAD, 0, 0);
//}
////=============================================================================
//// Method		: OnBnClickedBnSave
//// Access		: public  
//// Returns		: void
//// Qualifier	:
//// Last Update	: 2017/3/30 - 9:56
//// Desc.		:
////=============================================================================
//void CWnd_TeachOp::OnBnClickedBn_Par_Inspection()
//{
//	GetOwner()->SendNotifyMessageW(WM_MOTOR_PAR_INSPECT, 0, 0);
//}
//
////=============================================================================
//// Method		: OnBnClickedBnSave
//// Access		: public  
//// Returns		: void
//// Qualifier	:
//// Last Update	: 2017/3/30 - 9:56
//// Desc.		:
////=============================================================================
//void CWnd_TeachOp::OnBnClickedBn_Par_Cylinder()
//{
//	GetOwner()->SendNotifyMessageW(WM_CYLINDER_PAR_IN, 0, 0);
//}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
BOOL CWnd_TeachOp::PreTranslateMessage(MSG* pMsg)
{
	return CWnd::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/5 - 18:30
// Desc.		:
//=============================================================================
BOOL CWnd_TeachOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetUpdateItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/5 - 19:02
// Desc.		:
//=============================================================================
void CWnd_TeachOp::SetUpdateItem()
{
	m_nItemCnt = TC_ALL_MAX;
	for (UINT nIdx = 0; nIdx < m_nItemCnt; nIdx++)
	{
		m_szTeacOpName[nIdx] = g_szTeachItem_Image[nIdx];
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/7 - 16:38
// Desc.		:
//=============================================================================
void CWnd_TeachOp::SetUpdateData()
{

	if (m_pstMaintenanceInfo == NULL)
		return;

	CString szData;

	for (int _a = 0; _a < TC_ALL_MAX; _a++)
	{
		szData.Format(_T("%.1f"), m_pstMaintenanceInfo->stTeachInfo.dbTeachData[_a]);
		m_ed_Item[_a].SetWindowText(szData);
	}
}

//=============================================================================
// Method		: GetUpdateData
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/7 - 16:38
// Desc.		:
//=============================================================================
void CWnd_TeachOp::GetUpdateData()
{
	if (m_pstMaintenanceInfo == NULL)
		return;

	CString szData;

	for (int _a = 0; _a < TC_ALL_MAX; _a++)
	{
		m_ed_Item[_a].GetWindowText(szData);
		m_pstMaintenanceInfo->stTeachInfo.dbTeachData[_a] = _ttof(szData);
	}
}
