﻿//*****************************************************************************
// Filename	: 	Wnd_SiteInfo.h
// Created	:	2016/5/13 - 11:39
// Modified	:	2016/5/13 - 11:39
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_SiteInfo_h__
#define Wnd_SiteInfo_h__

#pragma once

#include "Def_ErrorCode.h"
#include "Def_Test.h"
#include "Def_DataStruct.h"
#include "VGStatic.h"
#include "Wnd_VideoView.h"
#include "VGGroupWnd.h"
#include "Wnd_BaseView.h"

//-----------------------------------------------------------------------------
// CWnd_SiteInfo
//-----------------------------------------------------------------------------
class CWnd_SiteInfo : public CVGGroupWnd
{
	DECLARE_DYNAMIC(CWnd_SiteInfo)

public:
	CWnd_SiteInfo();
	virtual ~CWnd_SiteInfo();

protected:
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	virtual void	OnRedrawControl			();

	DECLARE_MESSAGE_MAP()

	CFont			m_Font;	
	CVGStatic		m_st_TestTimeT;
	CVGStatic		m_st_TestTimeV;

	CVGStatic		m_st_BarcodeT;
	CVGStatic		m_st_BarcodeV;


	// Item 명칭
	CVGStatic		m_stItem_T[MAX_STEP_COUNT];
	// Item 데이터
	CVGStatic		m_stItem_V[MAX_STEP_COUNT];
	
	// 검사 결과
	CVGStatic		m_stJudgment;
	CVGStatic		m_st_OperateMode;

	CVGStatic		m_st_BackColor;

	// 데이터
	ST_ModelInfo*	m_pstModelInfo;

	UINT			m_nUseTestCount;
	BOOL			m_bUseTestItem[MAX_STEP_COUNT];

	void			RedrawWindow();

	void			SetTestItem	();

	CString			GetErrorCodeText(__in INT ErrorCode);
	//CVGGroupWnd		m_Group;

public:
	// 영상 화면 출력용
	CWnd_VideoView	m_wnd_VideoView;
	
	void	SetModelInfo (__in ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = pstModelInfo;

		SetTestItem();
		m_wnd_VideoView.SetModelInfo(m_pstModelInfo);
	}

	void	SetMasueControl (__in BOOL bMasueMode)
	{
		m_wnd_VideoView.SetMasueControl(bMasueMode);
	};

	// 설비 구동 모드
	void		SetOperateMode(__in enOperateMode nOperMode);

	//-------------------------------------------------------------------
	// 검사 결과 정보 UI 업데이트
	//-------------------------------------------------------------------
 	void	SetElapsedTime		(__in DWORD dwTime);			// 진행시간
 	void	SetTestResult		(__in enTestEachResult EachResult, __in UINT nStepIdx, __in CString strText);	
 	void	SetTestJudgment		(__in enTestEachResult EachResult);	
	void	SetTestJudgment		(__in enTestEachResult EachResult, __in CString szValue);
	void	SetResetResult		();
	void	SetBarcode			(__in LPCTSTR szBarcode);		// BARCODE
};

#endif // Wnd_SiteInfo_h__


