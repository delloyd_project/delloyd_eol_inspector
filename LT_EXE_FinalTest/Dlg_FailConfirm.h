#pragma once
#include "resource.h"
#include "Def_TestDevice.h"

// CDlg_FailConfirm 대화 상자입니다.

class CDlg_FailConfirm : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_FailConfirm)

public:
	CDlg_FailConfirm(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_FailConfirm();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_FAILBOX_CONFIRM };

	BOOL m_bFlag_Butten[DI_NotUseBit_Max];

	ST_Device* m_pDevice;
	CString m_szPassword;

	void SetPtr_Device(__in ST_Device* pDevice)
	{
		if (pDevice == NULL)
			return;

		m_pDevice = pDevice;
	};

	void Set_Password(__in CString szPassword)
	{
		m_szPassword = szPassword;
	};

	void SetConfirmToBtnEnable(__in BOOL bEnable);
	void OnConfirmOK();
	

protected:
	CFont		m_font;
	CVGStatic	m_stText;
	CMFCButton	m_bn_OK;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg void OnBnClickedBnOK();

	// 시리얼 통신 데이터
	afx_msg LRESULT	OnRecvMainBrd(WPARAM wParam, LPARAM lParam);


	DECLARE_MESSAGE_MAP()

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnClose();
};
