﻿#ifndef Wnd_BaseOperModeOp_h__
#define Wnd_BaseOperModeOp_h__

#pragma once

#include "Wnd_OperModeOp.h"

// CWnd_BaseOperModeOp

class CWnd_BaseOperModeOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseOperModeOp)

public:
	CWnd_BaseOperModeOp();
	virtual ~CWnd_BaseOperModeOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
	//CWnd_OperModeOp m_wndOperModeOp[TICnt_OperationMode];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseOperModeOp_h__
