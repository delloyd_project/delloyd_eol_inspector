﻿//*****************************************************************************
// Filename	: 	Wnd_ModelMode.h
// Created	:	2016/3/11 - 14:52
// Modified	:	2016/3/11 - 14:52
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_ModelMode_h__
#define Wnd_ModelMode_h__

#pragma once

#include "resource.h"
#include "File_WatchList.h"
#include "VGStatic.h"
#include "Wnd_BaseView.h"

//-----------------------------------------------------------------------------
// CWnd_ModelMode
//-----------------------------------------------------------------------------
class CWnd_ModelMode : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_ModelMode)

public:
	CWnd_ModelMode();
	virtual ~CWnd_ModelMode();

protected:
	
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnGetMinMaxInfo			(MINMAXINFO* lpMMI);
	afx_msg void	OnCbnSelEndCbModel		();
	afx_msg void	OnBnClickedOK			();
	afx_msg void	OnBnClickedCancel		();
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);

	DECLARE_MESSAGE_MAP()

	CFont			m_font_Large;
	CFont			m_font_Default;

	CVGStatic		m_st_Model;

	CVGStatic		m_st_ModelFile;
	CVGStatic		m_st_Model_CB;

	CComboBox		m_cb_Model;

	CButton			m_bn_OK;
	CButton			m_bn_Cancel;

	CString			m_szModelPath;
	CString			m_szFileExt;

	CString			m_szAppName;
	CString			m_szKeyName;

	CString			m_szFindModelFile;
	BOOL			m_bFindModelFile;

	CFile_WatchList		m_fileWatch;

	BOOL		ReadModelCode(__in LPCTSTR szPath, __out CString& szModelCode);
	BOOL		FindModelByModelCode(__in LPCTSTR szCode);

public:	

	void		SetModleInfo(__in LPCTSTR szPath, __in LPCTSTR szFileExt)
	{
		m_szModelPath = szPath;
		m_szFileExt = szFileExt;
	};

	// AppName, KeyName
	void		SetIniFileInfo(__in LPCTSTR szAppName, __in LPCTSTR szKeyName)
	{
		m_szAppName = szAppName;
		m_szKeyName = szKeyName;
	};

	void		SetCurrentModel(__in LPCTSTR szModel);

	CString		GetModelFile();

};

#endif // Wnd_ModelMode_h__


