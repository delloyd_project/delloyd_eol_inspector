﻿//*****************************************************************************
// Filename	: 	TestManager_EQP.h
// Created	:	2016/5/9 - 13:31
// Modified	:	2016/07/22
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef TestManager_EQP_h__
#define TestManager_EQP_h__

#pragma once

#include "TestManager_Device.h"

#include "Reg_InspInfo.h"
#include "File_Report.h"
#include "File_MES.h"

#include "Dlg_ErrView.h"

#include "TI_CenterPoint.h"
#include "TI_SFR.h"
#include "TI_Color.h"
#include "TI_Particle.h"
#include "TI_Brightness.h"

#include "TestProcess_Image.h"

//-----------------------------------------------------------------------------
// CTestManager_EQP
//-----------------------------------------------------------------------------
class CTestManager_EQP : public CTestManager_Device
{
public:
	CTestManager_EQP();
	virtual ~CTestManager_EQP();
	
protected:

	//-----------------------------------------------------
	// 옵션
	//-----------------------------------------------------
	// 환경설정 데이터 불러오기
	virtual BOOL	OnLoadOption					();	
	virtual void	InitDevicez						(__in HWND hWndOwner = NULL);

	//-----------------------------------------------------
	// 쓰레드 작업 처리
	//-----------------------------------------------------
	typedef struct
	{
		LPVOID		pOwner;
		UINT		nIndex;		// Socket Index
		UINT		nArg_1;		// Test Item
		UINT		nArg_2;		// 
	}stThreadParam;

	// Start 신호에 의해 구동되는 쓰레드
	HANDLE			m_hThrTest_Unit[MAX_OPERATION_THREAD];
	HANDLE			m_hThrTest_All;

	// 자동화 처리용 함수	
	BOOL			StartOperation_Auto				(__in UINT nUnitIdx, __in UINT nTestItemID);
	BOOL			StartOperation_AutoAll			();	

	// 수동 모드일때 자동화 처리용 함수
	void			StartOperation_Manual			(__in UINT nUnitIdx);

	// 검사 쓰레드
	static UINT WINAPI	ThreadTest_Unit				(__in LPVOID lParam);
	static UINT WINAPI	ThreadZone_All				(__in LPVOID lParam);

	// 쓰레드 내에서 처리될 자동 작업 함수
	BOOL			AutomaticProcess				(__in UINT nUnitIdx, __in UINT nTestItemID);
	void			AutomaticProcess_All			();

	// 자동으로 처리되고 있는 쓰레드 강제 종료
	void			StopProcess						(__in UINT nUnitIdx);
	void			StopProcess_All					();	

	// 전체 테스트 초기 세팅
	virtual void	OnInitialTest_All				();
	// 전체 테스트 종료 세팅
	virtual void	OnFinallyTest_All				(__in enTestProcess TestProc);
	// 개별 테스트 초기 세팅
	virtual void	OnInitialTest_Unit				(__in UINT nUnitIdx);
	// 개별 테스트 종료 세팅
	virtual void	OnFinallyTest_Unit				(__in UINT nUnitIdx, __in BOOL bResult);

	// 전체 검사 작업 시작
	virtual LRESULT	OnStartTest_All					();
	virtual LRESULT	OnStartTest_All_TestItem		(__in UINT nUnitIdx, __in UINT nTestItemID);
	// 개별 검사 작업 시작
	virtual LRESULT	OnStartTest_Unit				(__in UINT nUnitIdx, __in UINT nTestItemID);
	virtual LRESULT	OnStartTest_Unit_TestItem		(__in UINT nTestItemID);
	// 검사 결과에 따른 메세지 팝업
	virtual void	OnPopupMessageTest_All			(__in enResultCode_All nResultCode);

	//-----------------------------------------------------
	// 테스트
	//-----------------------------------------------------
	virtual BOOL	OnCheckDeviceComm				(){return TRUE;};
	// 기구물 안전 체크
	virtual BOOL	OnCheckSaftyJIG					();	
	// 전체 채널의 카메라 정보 업데이트
	virtual void	OnUpdateCamInfo_All				(){};
	// 제품 결과 판정 및 리포트 처리
	virtual void	OnJugdement_And_Report			();
	// Cycle Time Check
	virtual void	OnSetCycleTime					();
	// 카메라 데이터 초기화
	virtual void	OnResetCamInfo					();
	//-----------------------------------------------------	

	// 모델 설정 데이터가 정상인가 판단
	BOOL			CheckModelInfo					();	
	BOOL			CheckLOTInfo					();
	BOOL			CheckBarCodeInfo				();
	BOOL			CheckEASCount					();
	BOOL			CheckJigFix						();

	// 타이머 ---------------------------------------------
	virtual void	CreateTimer_UpdateUI			();
	virtual void	OnMonitor_TimeCheck				();
	virtual void	OnMonitor_UpdateUI				();
	DWORD			m_dwTimeCheck;	// 시간 체크용 임시 변수
	
	virtual	BOOL	EVMSPathSearch					(__out CString &szEVNPath){return TRUE;};

	//-----------------------------------------------------
	// Digital I/O
	//-----------------------------------------------------
	// I/O 신호 감지 및 신호별 기능 처리
	virtual void	OnDetectDigitalInSignal			(__in BYTE byBitOffset, __in BOOL bOnOff);
	// Digital I/O 신호 UI 처리
	virtual void	OnDIO_UpdateDInSignal			(__in BYTE byBitOffset, __in BOOL bOnOff){};
	virtual void	OnDIO_UpdateDOutSignal			(__in BYTE byBitOffset, __in BOOL bOnOff){};
	// I/O 신호 초기화
	virtual void	OnInitDigitalIOSignal			();
	virtual void	OnFunc_IO_EMO					();
	virtual void	OnFunc_IO_ErrorReset			();
	virtual void	OnFunc_IO_Init					();
	virtual void	OnFunc_IO_Door					();
	virtual void	OnFunc_IO_Safety				();
	virtual void	OnFunc_IO_AirCheck				();
	virtual void	OnFunc_IO_MainPower				();
	virtual void	OnFunc_IO_Stop					();
	virtual void	OnFunc_Motion_Initial			();

	virtual void	OnAddErrorInfo					(__in enEquipErrorCode lErrorCode);
	virtual void	OnSetInsertBarcode				(__in LPCTSTR szBarcode){};

	//-----------------------------------------------------
	// UI 검사항목
	//-----------------------------------------------------
	virtual LRESULT	StartTest_TestItem				(__in UINT nTestItemID, __in UINT nTestIdx, __in UINT nStepIdx, __in int iRetryCount);
	virtual LRESULT	StartTest_TestItem_Motion		(__in UINT nTestItemID);

	virtual	LRESULT	OnTest_TestInitialize			(__in UINT nStepIdx);
	virtual	LRESULT	OnTest_TestFinalize				(__in UINT nStepIdx);
	virtual	LRESULT	OnTest_BarCode					(__in UINT nStepIdx);

	// 사용 안함
	virtual	LRESULT	OnTest_Particle(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_Defectpixel(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_Brightness				(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_SFR						(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_PatternNoise				(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_IRFilter					(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_IRFilterManual			(__in UINT nStepIdx, __in UINT nTestIdx = 0);

	// 사용
	virtual	LRESULT	OnTest_Current					(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_CenterPoint				(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_CenterPoint_Manual		(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_CenterPoint_Adj			(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_Rotation					(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_EIAJ						(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_Angle					(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_Color					(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_Reverse					(__in int iRetryCount, __in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_ParticleManual			(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_LEDTest					(__in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_VideoSignal				(__in int iRetryCount,__in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_Reset					(__in int iRetryCount,__in UINT nStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnTest_OperationMode			(__in UINT nStepIdx, __in UINT nTestIdx = 0);

	virtual	LRESULT	OnEachTest_Current				(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnEachTest_OperationMode		(__in UINT nTestStepIdx, __in UINT nTestIdx = 0);
 	virtual	LRESULT	OnEachTest_CenterPoint			(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
 	virtual	LRESULT	OnEachTest_CenterPoint_Manual	(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
 	virtual	LRESULT	OnEachTest_CenterPoint_Adj		(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
 	virtual	LRESULT	OnEachTest_Rotation				(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnEachTest_EIAJ					(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
 	virtual	LRESULT	OnEachTest_SFR					(__in int iRetryCount,__in UINT nTestStepIdx, __in UINT nTestIdx = 0);
 	virtual	LRESULT	OnEachTest_Angle				(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
 	virtual	LRESULT	OnEachTest_Color				(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
 	virtual	LRESULT	OnEachTest_Reverse				(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnEachTest_LEDTest				(__in UINT nTestStepIdx, __in UINT nTestIdx = 0);
 	virtual LRESULT OnEachTest_ParticleManual (__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
	virtual LRESULT OnEachTest_BlackSpot (__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
	virtual LRESULT OnEachTest_DefectPixel (__in int iRetryCount, UINT nTestStepIdx, UINT nTestIdx = 0);
 	virtual	LRESULT	OnEachTest_VideoSignal			(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
 	virtual	LRESULT	OnEachTest_Reset				(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
 	virtual	LRESULT	OnEachTest_PatternNoise			(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
	virtual	LRESULT	OnEachTest_IRFilter				(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);
 	virtual	LRESULT	OnEachTest_Brightness			(__in int iRetryCount, __in UINT nTestStepIdx, __in UINT nTestIdx = 0);


// 	virtual	LRESULT	OnEachTest_IRFilterManual		(__in UINT nTestStepIdx, __in UINT nTestIdx = 0);

// 	virtual	LRESULT	OnEachTest_Particle				(__in UINT nTestStepIdx, __in UINT nTestIdx = 0);

	//-----------------------------------------------------
	// UI 업데이트
	//-----------------------------------------------------
	virtual void	OnUpdateElapsedTime_All			(__in DWORD dwTime){};
	virtual void	OnUpdateTestResult				(__in enTestEachResult EachResult, __in UINT nStepIdx, __in CString strText){};
	virtual void	OnUpdateTestJudgment			(__in enTestEachResult EachResult){};
	virtual	void	OnUpdateTResetResult			(){};

	virtual void	OnTestProgress					(__in enTestProcess nProcess);
	virtual void	OnTestFailResultCode_Unit		(__in UINT nUnitIdx, __in enResultCode_TestItem nFailCode);
	virtual void	OnTestResult					(__in enTestResult nResult);
	virtual void	OnTestErrCode					(__in LPCTSTR szErrorCode){};
	virtual void	OnSetInputTime					();
	virtual void	OnSetResetSiteInfo				(){};
	virtual void	OnSetTestInitialize				(__in enTestEachResult EachResult, __in CString strText	){};	// 검사 초기화
	virtual void	OnSetTestFinalize				(__in enTestEachResult EachResult, __in CString strText	){};	// 검사 마무리
	
	virtual void	OnSetLotInfo					(){};

	virtual BOOL	OnGetStartBtnStatus				(){ return TRUE; };
	virtual BOOL	OnGetStopBtnStatus				(){ return TRUE; };
	virtual void	OnSetLotStartBtnStatus			(__in BOOL bStatus) {};
	virtual void	OnSetLotStopBtnStatus			(__in BOOL bStatus) {};
	virtual void	OnSetStartBtnStatus				(__in BOOL bStatus) {};
	virtual void	OnSetStopBtnStatus				(__in BOOL bStatus) {};
	virtual void	OnTestMasterStartUpEvent		(__in BOOL bStatus) {};
	//-----------------------------------------------------
	// Motion
	//-----------------------------------------------------
	virtual LRESULT	StartTest_MotionItem			(__in UINT nAxisItemID,	__in double	dbPos);
	virtual LRESULT	StartTest_IOItem				(__in UINT nIO,			__in UINT	nSignal);

	//-----------------------------------------------------
	// UI Utility
	//-----------------------------------------------------
	virtual LRESULT	StartTest_Utility				(__in CString szMsg);


	//Indicator 데이터 
	virtual void	OnSetIndicatorData				(__in float fAxisX,	__in float fAxisY){};

	// 팝업된 다이얼로그 닫기
	virtual void	OnHidePopupUI					(){};

	// Worklist 처리
	virtual void	OnInsertWorklist				(){};
	virtual void	OnSaveWorklist					(){};
	virtual void	OnLoadWorklist					(){};

	virtual BOOL	MessageView						(__in CString szText, __in BOOL bMode = FALSE){return TRUE; };

	// 수율 
	virtual void	OnUpdateYield					(){};

	// 포고 카운트 처리
	BOOL			IncreasePogoCount				();
	BOOL			SavePogoCount					();
	BOOL			LoadPogoCount					();
	BOOL			CheckPogoCount					();
	virtual void	OnSetStatus_PogoCount			(){};

	// 표준 바코드
	BOOL			LoadRefBarcode();
	BOOL			LoadOperatorInfo();

	virtual BOOL	OnChangeLotInfo()								{return TRUE;};
	virtual BOOL	OnChangeLotInfo(__in enModelStatus LotStatus)	{return TRUE;};
	virtual void	OnSetMasterOffSetData			(__in int iOffsetX, __in int iOffsetY, __in double dbDegree){};
	virtual void	OnSetResult						(__in enTestEachResult Result, __in CString strText){};		//
	
	ST_ImageMode m_stImageMode;
	// 중심점 검출 알고리즘 
	CTI_CenterPoint TI_CenterPoint;

	// SFR 알고리즘 
	CTI_SFR			TI_SFR;

	// Color 알고리즘
	CTI_Color		TI_Color;

	// Particle 알고리즘
	CTI_Particle	TI_Particle;

	// Brightness 알고리즘
	CTI_Brightness	TI_Brightness;

	// 검사에 관련된 모든 정보가 들어있는 구조체 
	ST_InspectionInfo	m_stInspInfo;

	// 레지스트리 데이터 처리용 클래스
	CReg_InspInfo		m_regInspInfo;

	// 결과 데이터 처리용 클래스
	CFile_Report		m_FileReport;

	ST_Worklist			m_WorklistPtr;

	CFile_MES			m_MES;

	// 검사 시작 조건이 될때까지 검사 시작 방지
	BOOL				m_bFlag_ReadyTest;

	BOOL				m_bFlag_Indicator[Indicator_Max];

	BOOL				m_bFlag_UseStop;
	BOOL				m_bFlag_TestItem[TIID_MaxEnum];

	BOOL				m_bFlag_Butten[DI_NotUseBit_Max];

	float				m_fValue[Indicator_Max];

	BOOL				m_bFlag_TestProcess;
	BOOL				m_bMovingMotor;
	// 마스터 SET 확인
	BOOL				m_bFlag_MasterSet;
public:
	BOOL				m_bFlag_loopTest;
	
	// 생성자 처리용 코드
	virtual void		OnInitialize				();
	// 소멸자 처리용 코드
	virtual	void		OnFinalize					();

	virtual void		DisplayVideo_NoSignal		(__in UINT nChIdx){};

	//-----------------------------------------------------
	// 검사 상태 확인용 함수
	//-----------------------------------------------------
	// 개별, 전체 검사가 수행 중인지 확인
	BOOL				IsTesting					();
	// 전체 검사가 수행 중인지 확인
	BOOL				IsTesting_All				();
	// 개별 검사가 수행 중인지 확인
	BOOL				IsTesting_Unit				(__in UINT nUnitIdx);
	// 입력된 Unit을 제외한 검사가 수행 중인지 확인
	BOOL				IsTesting_Exc				(__in UINT nExcUnitIdx);

	BOOL				m_bTeststop;

	// 관리자 모드 확인 (구버전)
	BOOL				IsManagerMode				();
	// 제어 권한 상태 구하기
	enPermissionMode	GetPermissionMode			();
	// 제어 권한 상태 설정
	virtual void		SetPermissionMode			(__in enPermissionMode nAcessMode);
	// 설비 구동 모드
	virtual void		SetOperateMode				(__in enOperateMode nOperMode);

	virtual void		ManualAutoMode				(__in BOOL bMode){};
	
	virtual void		ManualBtnEnable				(__in BOOL bMode){};
	
	// Master Mode Test
	BOOL				SetAutoMasterMode();

	// 팝업 컨트롤
	virtual void		OnPopupParticleManualResult	(__out UINT& nTestResult){};
	virtual void		OnPopupLEDTestResult		(__in UINT nTestIdx,__out UINT& nTestResult){};
	virtual void		OnPopupCenterPtTunningResult(){};
	virtual void		OnPopupFailBoxConfirm		(){};

	// 팝업 생성 여부
	virtual BOOL		OnPopupParticleManualVisible	() { return TRUE; };
	virtual BOOL		OnPopupLEDTestVisible			() { return TRUE; };
	virtual BOOL		OnPopupCenterPtTunningVisible	() { return TRUE; };
	virtual BOOL		OnPopupFailBoxConfirmVisible	() { return TRUE; };

	virtual void		PermissionStatsView			();

	void				OnChangWorklistData			();
	CString				MakeTestItem				(__in ST_CamInfo* pstCamInfo);

	BOOL				SaveCurrentMesSystem		(__in UINT nTestItemCnt);
	BOOL				SaveOperationModeMesSystem	(__in UINT nTestItemCnt);
 	BOOL				SaveCenterPointMesSystem	(__in UINT nTestItemCnt);
	BOOL				SaveFinalMesSystem();
	BOOL				SaveRotateMesSystem			(__in UINT nTestItemCnt);
	BOOL				SaveEIAJMesSystem			(__in UINT nTestItemCnt);
	BOOL				SaveAngleMesSystem			(__in UINT nTestItemCnt);
	BOOL				SaveColorMesSystem			(__in UINT nTestItemCnt);
	BOOL				SaveReverseMesSystem		(__in UINT nTestItemCnt);
	BOOL				SaveParticleManualMesSystem	(__in UINT nTestItemCnt);
	BOOL				SaveParticleMesSystem		(UINT nTestItemCnt);
	BOOL				SaveDefectPixelMesSystem		(UINT nTestItemCnt);
	BOOL				SaveLEDMesSystem			(__in UINT nTestItemCnt);
	BOOL				SaveVideoSignalMesSystem	(__in UINT nTestItemCnt);
	BOOL				SaveResetMesSystem			(__in UINT nTestItemCnt);
 	BOOL				SaveSFRMesSystem			(__in UINT nTestItemCnt);

	BOOL				SaveBrightnessMesSystem		(__in UINT nTestItemCnt);
	BOOL				SavePatternNoiseMesSystem	(__in UINT nTestItemCnt);
	BOOL				SaveIRFilterMesSystem		(__in UINT nTestItemCnt);

// 	BOOL				SaveIRFilterManualMesSystem(__in UINT nTestItemCnt);
// 	BOOL				SaveParticleMesSystem		(__in UINT nTestItemCnt);
// 	BOOL				SaveFFTMesSystem			(__in UINT nTestItemCnt);


	// 이미지 테스트
	CTestProcess_Image	m_ImageTest;

	// 영상 버퍼 가져오기
	LPBYTE				GetImageBuffer				(__in UINT nViewCh, __in UINT nGrabType, __out DWORD& dwWidth, __out DWORD& dwHeight, __out UINT& nChannel);
	
	BOOL				GetImageBuffer();
	void RunLedChange(UINT nColor, BOOL bMode = TRUE);
	// 영상 상태 확인
	BOOL				IsCameraConnect				(__in UINT nCh, __in UINT nGrabType);
	
	// 영상 On/Off
	BOOL				CameraOnOff					(__in UINT nCh, __in UINT nGrabType, __in UINT nSWVer, __in UINT nFpsMode, __in UINT nDistortion,__in UINT nEdgeOnOff, __in BOOL bOnOff);

	// 전류 측정
	UINT				TestProcessCurrent			(__in ST_Current_Opt* pstOpt, __out ST_Current_Result &stResult);
	
	// 영상 신호 검사 (I2C)
	UINT				TestProcessVideoSignal		(__in UINT nCh, __out UINT& nResult);

	// Reset 검사 (I2C)
	UINT				TestProcessReset			(__in UINT nCh, __in UINT nResetDelay, __out UINT& nResult);

	// Frame 검사 (I2C)
	UINT				TestProcessFrameCheck		(__in UINT nCh, __in UINT nfpsMode, __out UINT& nResult);

	// 광축 조정
	UINT				OpticalCenter_Adj			(__in UINT nCh, __in int iCenterX, __in int iCenterY, __in int iTargerX, __in int iTargerY);

	// 광축 초기 위치 이동
	UINT				OpticalCenter_Adj_initial	(__in UINT nCh);

	//
	UINT				OpticalCenter_Adj			(__in UINT nAxis, __in UINT nCh, __in int iOffset);


	UINT				OpticalCenter_Adj_Horizon	(__in UINT nCh, __in int iOffset);
	UINT				OpticalCenter_Adj_Vertical	(__in UINT nCh, __in int iOffset);
	UINT				OpticalCenter_Adj_AllWrite	(__in UINT nCh);

	UINT				DistortionCorrection		(__in BOOL bOnOff);
	UINT				EdgeOnOff					(__in BOOL bOnOff);
	UINT				OperationModeChange			(__in UINT nfpsMode);

	UINT				CameraRegToFunctionChange	(__in UINT nSWVer, __in UINT nfpsMode, __in UINT nDistortion, __in UINT nEdgeOnOff);

	UINT				Motion_StageY_CL_Distance	(__in double dDistance);
	UINT				Motion_StageY_BlemishZone_In(__in UINT nTestItemID);
	UINT				Motion_StageY_BlemishZone_Out();
	UINT				Motion_StageY_Initial		();

	// 라벨 프린터
	UINT				OnPrinterOut				(__in int iLotCount);

	IplImage *m_TestImage = NULL;

	// 레포트 이미지 저장하기
	virtual void ReportImageSave(__in enLT_TestItem_ID nTestItem, __in UINT nTestCount, __in SYSTEMTIME* pTime, __in UINT nEachResult) {};
};

#endif // TestManager_EQP_h__

