﻿#ifndef List_Work_SFR_h__
#define List_Work_SFR_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_SFR_Worklist
{
	SFR_W_Recode,
	SFR_W_Time,
	SFR_W_Equipment,
	SFR_W_Model,
	SFR_W_SWVersion,
	SFR_W_CameraType,
	SFR_W_LOTNum,
	SFR_W_Barcode,
	SFR_W_Operator,
	SFR_W_Result,
	SFR_W_Data ,
	SFR_W_Data_F = SFR_W_Result + ROI_SFR_Max,
	SFR_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_SFR_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("Camera Type"),
	_T("LOTNum"),
	_T("Barcode"),
	_T("Operator"),
	_T("Result"),
	_T("S1"),
	_T("S2"),
	_T("S3"),
	_T("S4"),
	_T("S5"),
	_T("S6"),
	_T("S7"),
	_T("S8"),
	_T("S9"),
	_T("S10"),
	_T("S11"),
	_T("S12"),
	_T("S13"),
	_T("S14"),
	_T("S15"),
	_T("S16"),
	_T("S17"),
	_T("S18"),
	_T("S19"),
	_T("S20"),
	_T("S21"),
	_T("S22"),
	_T("S23"),
	_T("S24"),
	_T("S25"),
	_T("S26"),
	_T("S27"),
	_T("S28"),
	_T("S29"),
	_T("S30"),
	NULL,
};

const int	iListAglin_SFR_Worklist[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_SFR_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,
	70,

};
// CList_Work_SFR

class CList_Work_SFR : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_SFR)

public:
	CList_Work_SFR();
	virtual ~CList_Work_SFR();
	CFont		m_Font;

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	UINT Header_MaxNum();

	void InitHeader();
	void InsertFullData(__in const ST_CamInfo* pstCamInfo);
	void SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo);

	void GetData(UINT nRow, UINT &DataNum, CString *Data);


	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

};


#endif // List_Work_SFR_h__
