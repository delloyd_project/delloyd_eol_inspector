﻿#ifndef List_LEDCurrentOp_h__
#define List_LEDCurrentOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_LEDCurrentOp
{
	LEDCurOp_Channel,
	LEDCurOp_Min,
	LEDCurOp_Max,
	LEDCurOp_Offset,
	LEDCurOp_MaxCol,
};

// 헤더
static const LPCTSTR g_lpszHeader_LEDCurrOp[] =
{
	_T(""),
	_T("Min Spec"),
	_T("Max Spec"),
	_T("Offset"),
	NULL
};

typedef enum enListItemNum_LEDCurr
{
	LEDCurOp_Site1,
	LEDCurOp_ItemNum,
};

static const LPCTSTR g_lpszItem_LEDCurrOp[] =
{
	_T("LED"),
	NULL
};

const int	iListAglin_LEDCurOp[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_LEDCurrOp[] =
{
	100,
	130,
	130,
	130,
};
// List_LEDCurrentInfo

class CList_LEDCurrentOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_LEDCurrentOp)

public:
	CList_LEDCurrentOp();
	virtual ~CList_LEDCurrentOp();

	void InitHeader();
	void InsertFullData();
	void SetRectRow(UINT nRow);
	void GetCellData();

	void SetPtr_LEDCurrent(ST_LT_TI_LED *pstLEDCurrent)
	{
		if (pstLEDCurrent == NULL)
			return;

		m_pstLEDCurrent = pstLEDCurrent;
	};

protected:

	ST_LT_TI_LED*	m_pstLEDCurrent;

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	UINT		m_nEditCol;
	UINT		m_nEditRow;

	BOOL		UpdateCellData			(UINT nRow, UINT nCol, int  iValue);
	BOOL		UpdateCellData_double	(UINT nRow, UINT nCol, double dValue);

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick			(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk			(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnEnKillFocusEdit	();

	afx_msg BOOL	OnMouseWheel		(UINT nFlags, short zDelta, CPoint pt);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()
};

#endif // List_LEDCurrentInfo_h__
