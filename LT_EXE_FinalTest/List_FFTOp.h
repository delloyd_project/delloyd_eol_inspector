﻿#ifndef List_FFTOp_h__
#define List_FFTOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_FFTOp
{
	FFTOp_Channel,
	FFTOp_Min,
	FFTOp_Max,
	FFTOp_Offset,
	FFTOp_TestTime,
	FFTOp_MaxCol,
};

// 헤더
static const LPCTSTR g_lpszHeader_FFTOp[] =
{
	_T(""),
	_T("Min Spec"),
	_T("Max Spec"),
	_T("Offset"),
	_T("Test Time"),
	NULL
};

typedef enum enListItemNum_FFTOp
{
	FFTOp_Site1,
	FFTOp_ItemNum,
};

static const LPCTSTR g_lpszItem_FFTOp[] =
{
	_T("Channel 1"),
	NULL
};

const int	iListAglin_FFTOp[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_FFTOp[] =
{
	100,
	100,
	100,
	100,
	100,
};
// List_CurrentInfo

class CList_FFTOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_FFTOp)

public:
	CList_FFTOp();
	virtual ~CList_FFTOp();

	void InitHeader();
	void InsertFullData();
	void SetRectRow(UINT nRow);
	void GetCellData();

	void SetPtr_FFT(ST_LT_TI_FFT *pstFFT)
	{
		if (pstFFT == NULL)
			return;

		m_pstFFT = pstFFT;
	};

protected:

	ST_LT_TI_FFT*	m_pstFFT;

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	UINT		m_nEditCol;
	UINT		m_nEditRow;

	BOOL		UpdateCellData			(UINT nRow, UINT nCol, int  iValue);
	BOOL		UpdateCellData_double	(UINT nRow, UINT nCol, double dValue);

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick			(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk			(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnEnKillFocusEdit	();

	afx_msg BOOL	OnMouseWheel		(UINT nFlags, short zDelta, CPoint pt);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()
};

#endif // List_CurrentInfo_h__
