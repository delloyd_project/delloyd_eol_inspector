﻿#ifndef Wnd_FFTOp_h__
#define Wnd_FFTOp_h__

#pragma once

#include "VGStatic.h"
#include "List_FFTOp.h"
#include "Def_DataStruct.h"
#include "File_WatchList.h"

// CWnd_FFTOp

enum enFFTStatic
{
	STI_FFT_WAVE_SEL,
	STI_FFT_WAVE_VOLUME,
	STI_FFT_MAX,
};

static LPCTSTR	g_szFFTStatic[] =
{
	_T("WAVE FILE"),
	_T("VOLUME (1~100)"),
	NULL
};

enum enFFTButton
{
	BTN_FFT_TEST,
	BTN_FFT_MAX,
};

static LPCTSTR	g_szFFTButton[] =
{
	_T("TEST"),
	NULL
};

enum enFFTComobox
{
	CMB_FFT_WAVE_SEL,
	CMB_FFT_MAX,
};

enum enFFTEdit
{
	EDT_FFT_VOLUME,
	EDT_FFT_MAX,
};

class CWnd_FFTOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_FFTOp)

public:
	CWnd_FFTOp();
	virtual ~CWnd_FFTOp();

	void SetUpdateData	();
	void GetUpdateData	();
	void RefreshFileList(__in const CStringList* pFileList);

	void SetPtr_ModelInfo(ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = pstModelInfo;
	};

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

	// 파일이 있는 경로 설정
	void SetPath(__in LPCTSTR szWavePath)
	{
		m_szWavePath = szWavePath;
	};

protected:
	CFile_WatchList	m_IniWatch;
	ST_ModelInfo	*m_pstModelInfo;
	CList_FFTOp		m_List;

	CFont			m_font;

	CVGStatic			m_st_Item[STI_FFT_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_FFT_MAX];
	CButton				m_bn_Item[BTN_FFT_MAX];
	CComboBox			m_cb_Item[CMB_FFT_MAX];
	
	// 검사 항목이 다수 인경우
	UINT			m_nTestItemCnt;
	CString			m_szWavePath;

	DECLARE_MESSAGE_MAP()

	afx_msg void OnSize				(UINT nType, int cx, int cy);
	afx_msg int	 OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnShowWindow		(BOOL bShow, UINT nStatus);
	afx_msg void OnRangeBtnCtrl		(UINT nID);
	virtual BOOL PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // Wnd_FFTOp_h__
