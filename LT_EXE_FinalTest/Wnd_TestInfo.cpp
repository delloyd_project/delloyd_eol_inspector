﻿//*****************************************************************************
// Filename	: 	Wnd_TestInfo.cpp
// Created	:	2016/7/5 - 16:18
// Modified	:	2016/7/5 - 16:18
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_TestInfo.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_TestInfo.h"
#include "CommonFunction.h"
#include "resource.h"

// CWnd_TestInfo

typedef enum TestInfoBtn_ID
{
	IDC_BTN_START = 1001,
	IDC_BTN_STOP,
	IDC_BTN_LOTSTART,
	IDC_BTN_LOTSTOP,
	IDC_BTN_MODEL,
	IDC_BTN_MASTER_TEST,
	IDC_BTN_STARTUP_MODE,
	IDC_BTN_MASTER_MODE,
	IDC_BTN_MAX_NUM,
};

typedef enum TestInfoEdit_ID
{
	IDC_ED_DEGREE = 2000,
};

IMPLEMENT_DYNAMIC(CWnd_TestInfo, CWnd)

CWnd_TestInfo::CWnd_TestInfo()
{
	VERIFY(m_Font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		FIXED_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_TestInfo::~CWnd_TestInfo()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_TestInfo, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_BTN_START, IDC_BTN_MAX_NUM, OnRangeCmds)
END_MESSAGE_MAP()

// CWnd_TestInfo message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
int CWnd_TestInfo::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwEtStyle = WS_VISIBLE | WS_BORDER | ES_CENTER;
	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < TB_MaxEnum; nIdx++)
	{
		m_bn_Test[nIdx].Create(g_szTestButton[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_START + nIdx);
		m_bn_Test[nIdx].SetFont(&m_Font);
	}

	PermissionMode(m_enInspMode);
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
void CWnd_TestInfo::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing = 10;
	int iMagrin  = 3;

	int iLeft	= 0;
	int iTop	= 0;
	int iWidth	= cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin;


	if (m_enInspMode == Permission_Administrator)
	{
		int iHeightCnt = TB_MaxEnum / 2;

		int ibnWidth = (iWidth - (iMagrin * 1)) / 2;
		int ibnHeight = (iHeight - iMagrin * (iHeightCnt - 1)) / iHeightCnt;

		m_bn_Test[TB_Start].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

		iLeft += ibnWidth + iMagrin;
		m_bn_Test[TB_Stop].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

		iLeft = 0;
		iTop += ibnHeight + iMagrin;
		m_bn_Test[TB_LotStart].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

		iLeft += ibnWidth + iMagrin;
		m_bn_Test[TB_LotStop].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

		iLeft = 0;
		iTop += ibnHeight + iMagrin;
		m_bn_Test[TB_ModelChange].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

// 		iLeft += ibnWidth + iMagrin;
// 		m_bn_Test[TB_MasterTest].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

// 		iLeft = 0;
// 		iTop += ibnHeight + iMagrin;
// 		m_bn_Test[TB_StartUpMode].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

		iLeft += ibnWidth + iMagrin;
		m_bn_Test[TB_MasterSet].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);
	}
	
	else
	{
		int iHeightCnt = TB_MaxEnum / 2;

		int ibnWidth = (iWidth - (iMagrin * 1)) / 2;
		int ibnHeight = (iHeight - iMagrin * (iHeightCnt - 1)) / iHeightCnt;

		m_bn_Test[TB_Start].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

		iLeft += ibnWidth + iMagrin;
		m_bn_Test[TB_Stop].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

		iLeft = 0;
		iTop += ibnHeight + iMagrin;
		m_bn_Test[TB_LotStart].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

		iLeft += ibnWidth + iMagrin;
		m_bn_Test[TB_LotStop].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

		iLeft = 0;
		iTop += ibnHeight + iMagrin;
		m_bn_Test[TB_ModelChange].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

		iLeft += ibnWidth + iMagrin;
		m_bn_Test[TB_MasterSet].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);

// 		iLeft += ibnWidth + iMagrin;
// 		m_bn_Test[TB_MasterTest].MoveWindow(iLeft, iTop, ibnWidth, ibnHeight);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/16 - 19:04
// Desc.		:
//=============================================================================
BOOL CWnd_TestInfo::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: ButtonEnable
// Access		: public  
// Returns		: void
// Parameter	: enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2017/6/21 - 17:25
// Desc.		:
//=============================================================================
void CWnd_TestInfo::PermissionMode(enPermissionMode InspMode)
{
	m_enInspMode = InspMode;

	switch (InspMode)
	{
	case Permission_Operator:
		m_bn_Test[TB_Start].EnableWindow(FALSE);
		m_bn_Test[TB_Stop].EnableWindow(FALSE);
		m_bn_Test[TB_LotStart].EnableWindow(TRUE);
		m_bn_Test[TB_LotStop].EnableWindow(TRUE);
		m_bn_Test[TB_ModelChange].EnableWindow(TRUE);
		break;

	case Permission_Manager:
	case Permission_Engineer:
	case Permission_Administrator:
	case Permission_CNC:
	case Permission_MES:
		m_bn_Test[TB_Start].EnableWindow(TRUE);
		m_bn_Test[TB_Stop].EnableWindow(TRUE);
		m_bn_Test[TB_LotStart].EnableWindow(FALSE);
		m_bn_Test[TB_LotStop].EnableWindow(FALSE);
		m_bn_Test[TB_ModelChange].EnableWindow(TRUE);
//		m_bn_Test[TB_StartUpMode].EnableWindow(TRUE);
		m_bn_Test[TB_MasterSet].EnableWindow(TRUE);
		break;

	default:
		break;
	}

	CRect rt;
	GetClientRect(rt);
	OnSize(SIZE_RESTORED, rt.Width(), rt.Height());
}

//=============================================================================
// Method		: ButtonEnable
// Access		: public  
// Returns		: void
// Parameter	: BOOL bMode
// Qualifier	:
// Last Update	: 2017/7/11 - 10:07
// Desc.		:
//=============================================================================
void CWnd_TestInfo::ButtonEnable(BOOL bMode)
{
	m_bn_Test[TB_LotStart].EnableWindow(bMode);
	m_bn_Test[TB_Start].EnableWindow(!bMode);
	m_bn_Test[TB_Stop].EnableWindow(!bMode);
}


void CWnd_TestInfo::LotStartBtnEnable(BOOL bEnable)
{
	m_bn_Test[TB_LotStart].EnableWindow(bEnable);
}

void CWnd_TestInfo::LotStopBtnEnable(BOOL bEnable)
{
	m_bn_Test[TB_LotStop].EnableWindow(bEnable);
}

void CWnd_TestInfo::StartBtnEnable(BOOL bEnable)
{
	m_bn_Test[TB_Start].EnableWindow(bEnable);
}


void CWnd_TestInfo::StopBtnEnable(BOOL bEnable)
{
	m_bn_Test[TB_Stop].EnableWindow(bEnable);
}


// void CWnd_TestInfo::MasterTestBtnEnable(BOOL bEnable)
// {
// 	m_bn_Test[TB_MasterTest].EnableWindow(bEnable);
// }

void CWnd_TestInfo::MasterModeBtnEnable(BOOL bEnable)
{
	m_bn_Test[TB_MasterSet].EnableWindow(bEnable);
}

BOOL CWnd_TestInfo::GetStartBtnEnable()
{
	return m_bn_Test[TB_Start].IsWindowEnabled();
}


BOOL CWnd_TestInfo::GetStopBtnEnable()
{
	return m_bn_Test[TB_Stop].IsWindowEnabled();
}


//=============================================================================
// Method		: OnRangeCmds
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/6/21 - 14:33
// Desc.		:
//=============================================================================
void CWnd_TestInfo::OnRangeCmds(UINT nID)
{
	UINT nIndex = nID - IDC_BTN_START;
	CString strData;
	double dDegreePuls = 0;

	switch (nIndex)
	{
	case TB_Start:
		GetOwner()->SendNotifyMessage(WM_TEST_START, 0, 0);
		break;
	case TB_Stop:
		m_bn_Test[TB_Start].EnableWindow(FALSE);
		m_bn_Test[TB_Stop].EnableWindow(FALSE);
		
		GetOwner()->SendMessage(WM_TEST_STOP, 0, 0);
		Sleep(50);

		m_bn_Test[TB_Start].EnableWindow(TRUE);
		m_bn_Test[TB_Stop].EnableWindow(TRUE);
		break;
	case TB_LotStart:
		GetOwner()->SendNotifyMessage(WM_CHANGE_MODE, LOT_Start, 0);
		break;
	case TB_LotStop:
		GetOwner()->SendNotifyMessage(WM_CHANGE_MODE, LOT_End, 0);
		break;
	case TB_ModelChange:
		GetOwner()->SendNotifyMessage(WM_CHANGE_MODE, Model_Change, 0);
		break; 

// 	case TB_MasterTest:
// 		GetOwner()->SendNotifyMessage(WM_MASTER_TEST, 0, 0);
// 		break;

	//case TB_MasterMode:
	//	GetOwner()->SendNotifyMessage(WM_MASTER_MODE, 0, 0);
	//	break;

	case TB_MasterSet:
		GetOwner()->SendNotifyMessage(WM_CHANGE_MODE, MASTER_Start, 0);
		break;

	default:
		break;
	}
}
