#include "stdafx.h"
#include "TestProcess_Image.h"
#include "CommonFunction.h"

#define _USE_MATH_DEFINES
#include <math.h>

using namespace cv;

CTestProcess_Image::CTestProcess_Image()
{
}


CTestProcess_Image::~CTestProcess_Image()
{
}

void CTestProcess_Image::OnTestProcessAngle(__in IplImage* pFrameImage, __in ST_Angle_Opt* pstOpt, __out ST_Angle_Result &stResult)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
	cvCopy(pFrameImage, pInputImage);
	for (UINT nROI = ROI_AL_Center; nROI < ROI_AL_Max; nROI++)
	{
		pstOpt->stTestRegionOp[nROI].rtRoi.left = pstOpt->stRegionOp[nROI].rtRoi.left;
		pstOpt->stTestRegionOp[nROI].rtRoi.right = pstOpt->stRegionOp[nROI].rtRoi.right;
		pstOpt->stTestRegionOp[nROI].rtRoi.top = pstOpt->stRegionOp[nROI].rtRoi.top;
		pstOpt->stTestRegionOp[nROI].rtRoi.bottom = pstOpt->stRegionOp[nROI].rtRoi.bottom;

	}



	OnTestProcessFovROI_Auto(pInputImage, pstOpt);

	// ROI 보정 알고리즘
	//for (UINT nROI = ROI_AL_Center; nROI < ROI_AL_Max; nROI++)
	//{
	//	CPoint ptCenter = TI_CenterPoint.CenterPoint_Test(pstOpt->stRegionOp[nROI].rtRoi, pstOpt->stRegionOp[nROI].nMarkColor, pInputImage);

	//	// ROI 보정
	//	if (ptCenter.x != -1 && ptCenter.x != -1)
	//	{
	//		pstOpt->stRegionOp[nROI].rtRoi.left = ptCenter.x - (pstOpt->stRegionOp[nROI].rtRoi.Width() / 2);
	//		pstOpt->stRegionOp[nROI].rtRoi.right = ptCenter.x + (pstOpt->stRegionOp[nROI].rtRoi.Width() / 2);
	//		pstOpt->stRegionOp[nROI].rtRoi.top = ptCenter.y - (pstOpt->stRegionOp[nROI].rtRoi.Height() / 2);
	//		pstOpt->stRegionOp[nROI].rtRoi.bottom = ptCenter.y + (pstOpt->stRegionOp[nROI].rtRoi.Height() / 2);
	//	}

	//}

	for (UINT nROI = ROI_AL_Center; nROI < ROI_AL_Max; nROI++)
	{
		CPoint ptCenter = TI_CenterPoint.CenterPoint_Test(pstOpt->stTestRegionOp[nROI].rtRoi, pstOpt->stRegionOp[nROI].nMarkColor, pInputImage);
		stResult.ptCenter[nROI] = ptCenter;
	}

	// 데이터 산출
	for (UINT nROI = ROI_AL_Left; nROI < ROI_AL_Max; nROI++)
	{
		double dTriAngleA = stResult.ptCenter[ROI_AL_Center].x - stResult.ptCenter[nROI].x;
		double dTriAngleB = stResult.ptCenter[ROI_AL_Center].y - stResult.ptCenter[nROI].y;

		double dTriAngleC = pow(dTriAngleA, 2) + pow(dTriAngleB, 2);

		stResult.dValue[nROI - 1] = sqrt(dTriAngleC);
	}

	// 결과
	stResult.nResult = TER_Pass;

	for (UINT nIdx = 0; nIdx < IDX_AL_Max; nIdx++)
	{
		if (pstOpt->nMinSpc[nIdx] <= stResult.dValue[nIdx] && pstOpt->nMaxSpc[nIdx] >= stResult.dValue[nIdx])
		{
			stResult.nEachResult[nIdx] = TER_Pass;
		}
		else
		{
			stResult.nEachResult[nIdx] = TER_Fail;
			stResult.nResult = TER_Fail;
		}
	}

	cvReleaseImage(&pInputImage);
}

void CTestProcess_Image::OnTestProcessCenterPoint(__in IplImage* pFrameImage, __in ST_CenterPoint_Opt* pstOpt, __out ST_CenterPoint_Result &stResult)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
	cvCopy(pFrameImage, pInputImage);

	// 알고리즘
	CPoint ptCenter = TI_CenterPoint.CenterPoint_Test(pstOpt->rtRoi, pstOpt->nMarkColor, pInputImage);
// 	CPoint ptCenter;
// 	TI_CenterPoint.FiducialMark_Test(pInputImage, pInputImage->width, pInputImage->height, pstOpt->rtRoi, ptCenter);

	// ROI 보정
// 	if (ptCenter.x != -1 && ptCenter.x != -1)
// 	{
// 		pstOpt->rtRoi.left = ptCenter.x - (pstOpt->rtRoi.Width() / 2);
// 		pstOpt->rtRoi.right = ptCenter.x + (pstOpt->rtRoi.Width() / 2);
// 		pstOpt->rtRoi.top = ptCenter.y - (pstOpt->rtRoi.Height() / 2);
// 		pstOpt->rtRoi.bottom = ptCenter.y + (pstOpt->rtRoi.Height() / 2);
// 	}

	// 결과
	stResult.iValueX = ptCenter.x;
	stResult.iValueY = ptCenter.y;
	
	stResult.iPicValueX = ptCenter.x;
	stResult.iPicValueY = ptCenter.y;

	//카메라 상태 별로 값 변경.
	CamType_CenterPoint(pFrameImage->width, pFrameImage->height, stResult.iValueX, stResult.iValueY);

	// 옵셋
	stResult.iValueX += pstOpt->iOffsetX;
	stResult.iValueY += pstOpt->iOffsetY;

	stResult.iResultOffsetX = stResult.iValueX - pstOpt->nRefAxisX;
	stResult.iResultOffsetY = stResult.iValueY - pstOpt->nRefAxisY;

	// 개별 결과 X축
	if (abs(stResult.iResultOffsetX) <= abs(pstOpt->iDevOffsetX))
	{
		stResult.nResultX = TER_Pass;
	}
	else
	{
		stResult.nResultX = TER_Fail;
	}

	// 5. 개별 결과 Y축
	if (abs(stResult.iResultOffsetY) <= abs(pstOpt->iDevOffsetY))
	{
		stResult.nResultY = TER_Pass;
	}
	else
	{
		stResult.nResultY = TER_Fail;
	}

	// 6. 전체 결과	 
	if (stResult.nResultX == TER_Pass && stResult.nResultY == TER_Pass)
	{
		stResult.nResult = TER_Pass;
	}
	else
	{
		stResult.nResult = TER_Fail;
	}

	cvReleaseImage(&pInputImage);
}

void CTestProcess_Image::OnTestProcessColor(__in IplImage* pFrameImage, __in ST_CenterPoint_Opt* pstCenterOpt, __in ST_Color_Opt* pstOpt, __out ST_Color_Result &stResult)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;
	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
	cvCopy(pFrameImage, pInputImage);


	BOOL bResult = TRUE;

	//광축을 먼저 잡고 roi 위치를 옮긴다.


	//// ROI 보정 알고리즘

	//CRect rtCenter;
	//rtCenter.left = CAM_IMAGE_WIDTH_HALF - 200;
	//rtCenter.right = CAM_IMAGE_WIDTH_HALF + 200;

	//rtCenter.top = CAM_IMAGE_HEIGHT_HALF - 200;
	//rtCenter.bottom = CAM_IMAGE_HEIGHT_HALF + 200;

	//CPoint ptCenter = TI_CenterPoint.CenterPoint_Test(rtCenter, AI_Mark_B, pInputImage);

	//int iGapX = ptCenter.x - CAM_IMAGE_WIDTH_HALF;
	//int iGapY = CAM_IMAGE_HEIGHT_HALF - ptCenter.y;
	//for (UINT nROI = ROI_CL_R; nROI < ROI_CL_Max; nROI++)
	//{

	//	// ROI 보정
	//	if (ptCenter.x != -1 && ptCenter.x != -1)
	//	{
	//		pstOpt->stTestRegionOp[nROI].rtRoi.left = pstOpt->stRegionOp[nROI].rtRoi.left - iGapX;
	//		pstOpt->stTestRegionOp[nROI].rtRoi.right = pstOpt->stRegionOp[nROI].rtRoi.right - iGapX;
	//		pstOpt->stTestRegionOp[nROI].rtRoi.top = pstOpt->stRegionOp[nROI].rtRoi.top - iGapY;
	//		pstOpt->stTestRegionOp[nROI].rtRoi.bottom = pstOpt->stRegionOp[nROI].rtRoi.bottom - iGapY;
	//	}
	//}
	for (UINT nROI = ROI_CL_R; nROI < ROI_CL_Max; nROI++)
	{

			pstOpt->stTestRegionOp[nROI].rtRoi.left = pstOpt->stRegionOp[nROI].rtRoi.left ;
			pstOpt->stTestRegionOp[nROI].rtRoi.right = pstOpt->stRegionOp[nROI].rtRoi.right ;
			pstOpt->stTestRegionOp[nROI].rtRoi.top = pstOpt->stRegionOp[nROI].rtRoi.top ;
			pstOpt->stTestRegionOp[nROI].rtRoi.bottom = pstOpt->stRegionOp[nROI].rtRoi.bottom ;
	}
	OnTestProcessColorROI_Auto(pInputImage, pstOpt);


	// 3. 알고리즘
	for (UINT nROI = 0; nROI < ROI_CL_Max; nROI++)
	{
		CvRect rcSz;
		rcSz.x = pstOpt->stTestRegionOp[nROI].rtRoi.left;
		rcSz.y = pstOpt->stTestRegionOp[nROI].rtRoi.top;
		rcSz.width = pstOpt->stTestRegionOp[nROI].rtRoi.Width();
		rcSz.height = pstOpt->stTestRegionOp[nROI].rtRoi.Height();

		cvSetImageROI(pInputImage, rcSz);

		IplImage* pROI = cvCreateImage(cvGetSize(pInputImage), IPL_DEPTH_8U, pInputImage->nChannels);
		cvCopy(pInputImage, pROI);

		TI_Color.Color_Test(pROI, stResult.iRed[nROI], stResult.iGreen[nROI], stResult.iBlue[nROI]);

		if (pstOpt->stRegionOp[nROI].nMaxSpcR >= stResult.iRed[nROI]
			&& pstOpt->stRegionOp[nROI].nMinSpcR <= stResult.iRed[nROI]
			&& pstOpt->stRegionOp[nROI].nMaxSpcG >= stResult.iGreen[nROI]
			&& pstOpt->stRegionOp[nROI].nMinSpcG <= stResult.iGreen[nROI]
			&& pstOpt->stRegionOp[nROI].nMaxSpcB >= stResult.iBlue[nROI]
			&& pstOpt->stRegionOp[nROI].nMinSpcB <= stResult.iBlue[nROI])
		{
			bResult &= TRUE;
			stResult.nEachResult[nROI] = TER_Pass;
		}
		else
		{
			bResult &= FALSE;
			stResult.nEachResult[nROI] = TER_Fail;
		}

		cvReleaseImage(&pROI);
		cvResetImageROI(pInputImage);
	}

	// 4. 결과
	if (bResult == TRUE)
	{
		stResult.nResult = TER_Pass;
	}
	else
	{
		stResult.nResult = TER_Fail;
	}

	stResult.iSelectROI = 1;
	cvReleaseImage(&pInputImage);
}

void CTestProcess_Image::OnTestProcessParticle(__in IplImage* pFrameImage, __in ST_Particle_Opt* pstOpt, __out ST_Particle_Result &stResult)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	stResult.Reset();

	ST_CAN_Particle_Opt		stOption;
	ST_CAN_Particle_Result	stResult_Can;
	
	//!SH _181218: 추가 필요
	stOption.fSensitivity = pstOpt->fDustDis;
	stOption.iEgdeW = pstOpt->iEdgeW;
	stOption.iEgdeH = pstOpt->iEdgeH;
	//stOption.iEgdeW = 32;
	//stOption.iEgdeH = 32;
	
	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
	cvCopy(pFrameImage, pInputImage);

	int iParticleCount = 0;

	// 알고리즘
	for (UINT nROI = 0; nROI < Part_ROI_Max; nROI++)
	{

		//stOption.bEllipse[nROI] = false;//pstOpt->stRegionOp[nROI].bEllipse ? true : false;
		stOption.fThreConcentration[nROI] = (float)pstOpt->stRegionOp[nROI].dbBruiseConc;
		stOption.fThreSize[nROI] = (float)pstOpt->stRegionOp[nROI].dbBruiseSize;

		stOption.stROI[nROI].sLeft = (unsigned short)pstOpt->stRegionOp[nROI].rtRoi.left;
		stOption.stROI[nROI].sTop = (unsigned short)pstOpt->stRegionOp[nROI].rtRoi.top;
		stOption.stROI[nROI].sWidth = (unsigned short)pstOpt->stRegionOp[nROI].rtRoi.Width();
		stOption.stROI[nROI].sHeight = (unsigned short)pstOpt->stRegionOp[nROI].rtRoi.Height();

		//CvRect rcROISz;
		//rcROISz.x = pstOpt->stRegionOp[nROI].rtRoi.left;
		//rcROISz.y = pstOpt->stRegionOp[nROI].rtRoi.top;
		//rcROISz.width = pstOpt->stRegionOp[nROI].rtRoi.Width();
		//rcROISz.height = pstOpt->stRegionOp[nROI].rtRoi.Height();

		//cvSetImageROI(pInputImage, rcROISz);
		//IplImage* pROI = cvCreateImage(cvGetSize(pInputImage), IPL_DEPTH_8U, pInputImage->nChannels);
		//cvCopy(pInputImage, pROI);


		//iParticleCount =
		//	TI_Particle.ParticleCountDetection(
		//	(double)pstOpt->fDustDis,
		//	pstOpt->stRegionOp[nROI].dbBruiseConc,
		//	pstOpt->stRegionOp[nROI].dbBruiseSize,
		//	pInputImage,
		//	stResult.fConcentration,
		//	stResult.rtFailRoi
		//	);

		//cvReleaseImage(&pROI);
		//cvResetImageROI(pInputImage);
	}

	TI_Particle.Particle_Test((LPBYTE)pInputImage->imageData, pInputImage->width, pInputImage->height, stOption, stResult_Can);

	int Region_Stain = 0;
	// 영역별 카운트
	for (int i = 0; i < stResult_Can.byRoiCnt; i++)
	{
		Region_Stain = stResult_Can.byType[i];
		stResult.nFailTypeCount[Region_Stain]++;
	}

	if (stResult_Can.byRoiCnt > 0)
	{
		double Max_ConcValue = 0.0;
		// 이물 파트별 Max 값만 저장
		for (int i = 0; i < Part_ROI_Max; i++)
		{
			Max_ConcValue = 0.0;
			for (int j = 0; j < stResult_Can.byRoiCnt; j++)
			{
				if (j < 2000)
				{
					// 이물의 위치는??
					Region_Stain = stResult_Can.byType[j];
					if (Region_Stain == i)
					{
						if (stResult_Can.fConcentration[j] > Max_ConcValue)
							Max_ConcValue = stResult_Can.fConcentration[j];
					}
				}
				
			}
			// 영역에서 가장 어두운 곳의 값
			stResult.dFailTypeConcentration[i] = Max_ConcValue;
		}
	}
	// 검출된 이물 갯수
	stResult.nFailCount = 0;

	for (UINT nIdx = 0; nIdx < stResult_Can.byRoiCnt; nIdx++)
	{
		if (DetectCount > nIdx)
		{
			stResult.nFailCount++;
			stResult.nFailType[nIdx] = (UINT)stResult_Can.byType[nIdx];
			stResult.fConcentration[nIdx] = stResult_Can.fConcentration[nIdx];
			stResult.rtFailRoi[nIdx] = stResult_Can.stROI[nIdx];
		}
		
	}

	// 결과
	//if (pstOpt.stSpec_Min.dbValue <= stResult.nFailCount && pstOpt.stSpec_Max.dbValue >= stResult.nFailCount)
	if (stResult.nFailCount > 0)
	{
		stResult.nResult = TER_Fail;
	}
	else
	{
		stResult.nResult = TER_Pass;
	}

	cvReleaseImage(&pInputImage);
}

void CTestProcess_Image::OnTestProcessDefectPixel(__in IplImage* pFrameImage, __in ST_DefectPixel_Opt* pstOpt, __out ST_DefectPixel_Data &stResult)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;
	
	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, 1);
	cvCvtColor(pFrameImage, pInputImage, CV_RGB2GRAY);

	BOOL bResult = TRUE;

	BYTE byIndexCount = 0;
	short sPointX[MAX_Particle_ResultROI_Count] = { 0, };
	short sPointY[MAX_Particle_ResultROI_Count] = { 0, };
	float fConcentration[MAX_Particle_ResultROI_Count] = { 0, }; 
	BYTE byFailType[MAX_Particle_ResultROI_Count] = { 0, };

	TI_DefectPixel.DefectPixel_Test((LPBYTE)pInputImage->imageData, 1, pstOpt->iBlockSize, 0, 0, 99, 99
		, 0, pstOpt->fThreshold[Spec_Defect_DeadPixel], byIndexCount, 
		sPointX, sPointY, fConcentration, byFailType, pFrameImage->width, pFrameImage->height, MAX_Particle_ResultROI_Count,pstOpt->nmagin_left,pstOpt->nmagin_rightt);
	

	UINT nCnt[Spec_Defect_Max] = { 0, };

	for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
	{
		stResult.nFailCount[nIdx]= 0;
	}


	for (int nIdx = 0; nIdx < byIndexCount; nIdx++)
	{
		// 유형별 카운팅

		if (nIdx > ROI_F_DefectPixel_Max)
		{
			break;
		}

		UINT nItem = byFailType[nIdx];		

		stResult.nFailType[nItem][nCnt[nItem]] = nItem;
		stResult.rtFailROI[nItem][nCnt[nItem]].left = sPointX[nIdx];
		stResult.rtFailROI[nItem][nCnt[nItem]].top = sPointY[nIdx];
		stResult.rtFailROI[nItem][nCnt[nItem]].right = sPointX[nIdx]+1;
		stResult.rtFailROI[nItem][nCnt[nItem]].bottom = sPointY[nIdx]+1;

		stResult.nFailCount[nItem]++;
		nCnt[nItem]++;
	}

	for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
	{
		BOOL	bMinUse = pstOpt->bEnableMin[nIdx];
		BOOL	bMaxUse = pstOpt->bEnableMax[nIdx];

		int iMinDev = pstOpt->iSpecMin[nIdx];
		int iMaxDev = pstOpt->iSpecMax[nIdx];

		// 판정
		stResult.nEachResult[nIdx] = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nFailCount[nIdx]);

		stResult.nResult &= stResult.nEachResult[nIdx];
	}

	//// * 측정값 입력 (카메라 초기화)
	//COleVariant varResult;
	//varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_DefectPixel].vt);
	//varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	//m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	// 4. 결과
	if (stResult.nResult == TRUE)
	{
		stResult.nResult = TER_Pass;
	}
	else
	{
		stResult.nResult = TER_Fail;
	}

	cvReleaseImage(&pInputImage);
}

void CTestProcess_Image::OnTestProcessBrightness(__in IplImage* pFrameImage, __in ST_Brightness_Opt* pstOpt, __out ST_Brightness_Result &stResult)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	BYTE byCenterY = 0x0;
	BYTE bySideY[ROI_BR_Max] = { 0x0, };

	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
	cvCopy(pFrameImage, pInputImage);

	// 기준 Y값 구하기
	CvRect rcCenterSz;
	rcCenterSz.x = pstOpt->stRegionOp[ROI_BR_Center].rtRoi.left;
	rcCenterSz.y = pstOpt->stRegionOp[ROI_BR_Center].rtRoi.top;
	rcCenterSz.width = pstOpt->stRegionOp[ROI_BR_Center].rtRoi.Width();
	rcCenterSz.height = pstOpt->stRegionOp[ROI_BR_Center].rtRoi.Height();

	cvSetImageROI(pInputImage, rcCenterSz);
	IplImage* pROI = cvCreateImage(cvGetSize(pInputImage), IPL_DEPTH_8U, pInputImage->nChannels);
	cvCopy(pInputImage, pROI);

	TI_Brightness.Brightness_Test(pInputImage, byCenterY);
	stResult.dbValue[ROI_BR_Center] = 100;
	stResult.nEachResult[ROI_BR_Center] = TER_Pass;

	cvReleaseImage(&pROI);
	cvResetImageROI(pInputImage);

	// 영역 별 Y 값 구하기
	BOOL bResult = TRUE;
	for (UINT nROI = ROI_BR_Side_1; nROI < ROI_BR_Max; nROI++)
	{
		CvRect rcSz;
		rcSz.x = pstOpt->stRegionOp[nROI].rtRoi.left;
		rcSz.y = pstOpt->stRegionOp[nROI].rtRoi.top;
		rcSz.width = pstOpt->stRegionOp[nROI].rtRoi.Width();
		rcSz.height = pstOpt->stRegionOp[nROI].rtRoi.Height();

		cvSetImageROI(pInputImage, rcSz);
		IplImage* pROI = cvCreateImage(cvGetSize(pInputImage), IPL_DEPTH_8U, pInputImage->nChannels);
		cvCopy(pInputImage, pROI);

		TI_Brightness.Brightness_Test(pInputImage, bySideY[nROI]);

		cvReleaseImage(&pROI);
		cvResetImageROI(pInputImage);

		// 주변 광량비 구하기
		double dYRate = ((double)bySideY[nROI] / (double)byCenterY) * 100.0;
		stResult.dbValue[nROI] = dYRate;

		if (stResult.dbValue[nROI] >= pstOpt->stRegionOp[nROI].dMinSpec
			&& stResult.dbValue[nROI] <= pstOpt->stRegionOp[nROI].dMaxSpec)
		{
			bResult &= TRUE;
			stResult.nEachResult[nROI] = TER_Pass;
		}
		else
		{
			bResult &= FALSE;
			stResult.nEachResult[nROI] = TER_Fail;
		}
	}

	if (bResult == TRUE)
	{
		stResult.nResult = TER_Pass;
	}
	else
	{
		stResult.nResult = TER_Fail;
	}

	cvReleaseImage(&pInputImage);
}

void CTestProcess_Image::OnTestProcessRotate(__in IplImage* pFrameImage, __in ST_CenterPoint_Opt* pstCenterOpt, __in ST_Rotate_Opt* pstOpt, __out ST_Rotate_Result &stResult)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
	cvCopy(pFrameImage, pInputImage);

// 	ROI 보정 알고리즘
// 		for (UINT nROI = 0; nROI < ROI_RT_Max; nROI++)
// 		{
// 			CPoint ptCenter = TI_CenterPoint.CenterPoint_Test(pstCenterOpt->rtRoi, pstCenterOpt->nMarkColor, pInputImage);
// 	
// 			// ROI 보정
// 	 		if (ptCenter.x != -1 && ptCenter.x != -1)
// 	 		{
// 	 			pstOpt->stRegionOp[nROI].rtRoi.left = ptCenter.x - (pstOpt->stRegionOp[nROI].rtRoi.Width() / 2);
// 	 			pstOpt->stRegionOp[nROI].rtRoi.right = ptCenter.x + (pstOpt->stRegionOp[nROI].rtRoi.Width() / 2);
// 	 			pstOpt->stRegionOp[nROI].rtRoi.top = ptCenter.y - (pstOpt->stRegionOp[nROI].rtRoi.Height() / 2);
// 	 			pstOpt->stRegionOp[nROI].rtRoi.bottom = ptCenter.y + (pstOpt->stRegionOp[nROI].rtRoi.Height() / 2);
// 	 		}
// 	
// 		}


	for (UINT nROI = 0; nROI < ROI_RT_Max; nROI++)
	{
		pstOpt->stTestRegionOp[nROI].rtRoi.left = pstOpt->stRegionOp[nROI].rtRoi.left;
		pstOpt->stTestRegionOp[nROI].rtRoi.right = pstOpt->stRegionOp[nROI].rtRoi.right;
		pstOpt->stTestRegionOp[nROI].rtRoi.top = pstOpt->stRegionOp[nROI].rtRoi.top;
		pstOpt->stTestRegionOp[nROI].rtRoi.bottom = pstOpt->stRegionOp[nROI].rtRoi.bottom;

	}

	OnTestProcessRotateROI_Auto(pInputImage, pstOpt);

	for (UINT nROI = 0; nROI < ROI_RT_Max; nROI++)
	{
		CPoint ptCenter = TI_CenterPoint.CenterPoint_Test(pstOpt->stTestRegionOp[nROI].rtRoi, pstOpt->stTestRegionOp[nROI].nMarkColor, pInputImage);
		stResult.ptCenter[nROI] = ptCenter;
	}

	// 4. 수식
	//============================= Horizontal
	double dbHorzontalDegree = atan2((stResult.ptCenter[ROI_RT_RT].y + stResult.ptCenter[ROI_RT_RB].y) / 2.0
		- (stResult.ptCenter[ROI_RT_LT].y + stResult.ptCenter[ROI_RT_LB].y) / 2.0,
		(stResult.ptCenter[ROI_RT_RT].x + stResult.ptCenter[ROI_RT_RB].x) / 2.0
		- (stResult.ptCenter[ROI_RT_LT].x + stResult.ptCenter[ROI_RT_LB].x) / 2.0);

	dbHorzontalDegree = dbHorzontalDegree * 180.0 / CV_PI;

	//============================= Vertical
	double dbVerticalDegree = atan2((stResult.ptCenter[ROI_RT_LB].y + stResult.ptCenter[ROI_RT_RB].y) / 2.0
		- (stResult.ptCenter[ROI_RT_LT].y + stResult.ptCenter[ROI_RT_RT].y) / 2.0,
		(stResult.ptCenter[ROI_RT_LB].x + stResult.ptCenter[ROI_RT_RB].x) / 2.0
		- (stResult.ptCenter[ROI_RT_LT].x + stResult.ptCenter[ROI_RT_RT].x) / 2.0);

	dbVerticalDegree = dbVerticalDegree * 180.0 / CV_PI - 90.0;

	// 5. 평균값
	stResult.dbValue = (dbHorzontalDegree + dbVerticalDegree) / 2.0;

	// 6. 옵셋
	stResult.dbValue += pstOpt->dbOffset;

	// 7. 판정
	if (pstOpt->dbMinSpc <= stResult.dbValue && pstOpt->dbMaxSpc >= stResult.dbValue)
	{
		stResult.nResult = TER_Pass;
	}
	else
	{
		stResult.nResult = TER_Fail;
	}


	cvReleaseImage(&pInputImage);
}

void CTestProcess_Image::OnTestProcessSFR(__in IplImage* pFrameImage, __in ST_SFR_Opt* pstOpt, __out ST_SFR_Result &stResult)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
	cvCopy(pFrameImage, pInputImage);

	// 중심 대비 이동
	OnTestProcessSFRROI_Auto_Center(TRUE, pInputImage, pstOpt);

	// 측정
	for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
	{
		if (TRUE == pstOpt->stRegionOp[nROI].bEnable)
		{
			IplImage *OriginImage = cvCreateImage(cvSize(pstOpt->stRegionOp[nROI].rtRoi.Width(), pstOpt->stRegionOp[nROI].rtRoi.Height()), IPL_DEPTH_8U, 1);
			IplImage *FlipOriginImage = NULL;
			IplImage *FlipBufferImage = NULL;

			cv::Mat MatData_Flip;
			cv::Mat MatData;

			cvSetZero(OriginImage);

			// ROI Image Cut
			cvSetImageROI(pInputImage, 
				cvRect(pstOpt->stRegionOp[nROI].rtRoi.left, pstOpt->stRegionOp[nROI].rtRoi.top, pstOpt->stRegionOp[nROI].rtRoi.Width(), pstOpt->stRegionOp[nROI].rtRoi.Height()));

			// 3채널 -> 1채널로 변환
			if (pInputImage->nChannels == 3)
			{
				cvCvtColor(pInputImage, OriginImage, CV_RGB2GRAY);
			}
			else
			{
				cvCopy(pInputImage, OriginImage);
			}

			// 원본 이미지 Reset
			cvResetImageROI(pInputImage);

			// 가로가 세로보다 길면, 세로 형태의 이미지로 플립시킨다.
			if (pstOpt->stRegionOp[nROI].rtRoi.Width() >  pstOpt->stRegionOp[nROI].rtRoi.Height())
			{
				FlipOriginImage = cvCreateImage(cvSize(pstOpt->stRegionOp[nROI].rtRoi.Height(), pstOpt->stRegionOp[nROI].rtRoi.Width()), IPL_DEPTH_8U, 1);

				MatData = cvarrToMat(OriginImage);

				cv::flip(MatData.t(), MatData_Flip, 0);
				FlipBufferImage = &IplImage(MatData_Flip);

				cvCopy(FlipBufferImage, FlipOriginImage);
			}
			else
			{
				FlipOriginImage = cvCreateImage(cvSize(pstOpt->stRegionOp[nROI].rtRoi.Width(), pstOpt->stRegionOp[nROI].rtRoi.Height()), IPL_DEPTH_8U, 1);
				cvCopy(OriginImage, FlipOriginImage);
			}

			stResult.dbValue[nROI] = TI_SFR.SFR_Test(pstOpt->dPixelSz, pstOpt->stRegionOp[nROI].dbLinePair, FlipOriginImage, pstOpt->nResultType);

			cvReleaseImage(&FlipOriginImage);
			cvReleaseImage(&OriginImage);
		}
	}

	// 4. 판정
	stResult.nResult = TER_Pass;
	
	int iTotalCnt = 0;
	int iPassCnt = 0;

	for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
	{
		if (TRUE == pstOpt->stRegionOp[nROI].bEnable)
		{
			CString szData;

			// 옵셋
			double dbValue = stResult.dbValue[nROI] += pstOpt->stRegionOp[nROI].dbOffset;
			szData.Format(_T("%.2f"), dbValue);

			stResult.dbValue[nROI] = _ttof(szData);

			if (pstOpt->stRegionOp[nROI].dbMinSpc <= stResult.dbValue[nROI] && pstOpt->stRegionOp[nROI].dbMaxSpc >= stResult.dbValue[nROI])
			{
				stResult.nEachResult[nROI] = TER_Pass;
				iPassCnt++;
			}
			else
			{
				stResult.nEachResult[nROI] = TER_Fail;
				stResult.nResult = TER_Fail;
			}

			iTotalCnt++;
		}
	}

	stResult.iTotalCnt = iTotalCnt;
	stResult.iPassCnt = iPassCnt;

	//for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
	//	pstOpt->stRegionOp[nROI].rtRoi = InitROI[nROI];

	cvReleaseImage(&pInputImage);
}


void CTestProcess_Image::OnTestProcessEIAJ(__in IplImage* pFrameImage, ST_LT_TI_EIAJ *pstEIAJ)
{
	if (pFrameImage == NULL)
		return;

	if (pstEIAJ == NULL)
		return;

	if (TI_EIAJ.SetImageSize(pFrameImage->width, pFrameImage->height) == FALSE)
		return;


	(enTestEachResult)TI_EIAJ.EIAJ_Test(pstEIAJ, (LPBYTE)pFrameImage->imageData);


// 	int iRef_BlackValue = 0;
// 	int iRef_WhiteValue = 0;
// 	stResult.Reset();
// 
// 	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
// 	cvCopy(pFrameImage, pInputImage);
// 
// 	CRect InitROI[ReOp_ItemNum];
// 
// 	for (int i = EiajOp_Black; i < ReOp_ItemNum; i++)
// 		InitROI[i] = pstOpt->stRegionOp[i].rtRoi;
// 
// 
// 
// 	CRect rcSearchROI;
// 	rcSearchROI.left = pFrameImage->width / 3;
// 	rcSearchROI.right = pFrameImage->width * 2 / 3;
// 	rcSearchROI.top = pFrameImage->height / 3;
// 	rcSearchROI.bottom = pFrameImage->height * 2 / 3;
// 
// 	CPoint SearchPoint = TI_CenterPoint.CenterPoint_Test(rcSearchROI, 0, pInputImage);
// 	//CvPoint SearchPoint = TI_EIAJ.SearchCenterPoint(pInputImage);
// 
// 	if (SearchPoint.x == (-1) * pInputImage->width && SearchPoint.y == (-1) * pInputImage->height)
// 		return;
// 
// 	for (int i = EiajOp_Black; i < ReOp_ItemNum; i++)
// 	{
// 		if (pstOpt->stRegionOp[i].bEnable == TRUE)
// 		{
// 			int posX = InitROI[i].CenterPoint().x;
// 			int posY = InitROI[i].CenterPoint().y;
// 			int OffsetX = InitROI[EiajOp_Black].CenterPoint().x;
// 			int OffsetY = InitROI[EiajOp_Black].CenterPoint().y;
// 
// 			pstOpt->stRegionOp[i].RectPosXY(posX - OffsetX + SearchPoint.x, posY - OffsetY + SearchPoint.y);
// 		}
// 	}
// 
// 	//CString str;
// 	///----------------------------------------------------
// 
// 	for (int i = EiajOp_Black; i < EiajOp_ROI_FST; i++)
// 	{
// 		CvRect rcSz;
// 		rcSz.x = pstOpt->stRegionOp[i].rtRoi.left;
// 		rcSz.y = pstOpt->stRegionOp[i].rtRoi.top;
// 		rcSz.width = pstOpt->stRegionOp[i].rtRoi.Width();
// 		rcSz.height = pstOpt->stRegionOp[i].rtRoi.Height();
// 
// 		cvSetImageROI(pInputImage, rcSz);
// 
// 		IplImage* pROI = cvCreateImage(cvGetSize(pInputImage), IPL_DEPTH_8U, pInputImage->nChannels);
// 		cvCopy(pInputImage, pROI);
// 		
// 
// 		if (i == EiajOp_Black)
// 			iRef_BlackValue = TI_EIAJ.Get_Ref_Threshold_Value(pROI);
// 		else if (i == EiajOp_White)
// 			iRef_WhiteValue = TI_EIAJ.Get_Ref_Threshold_Value(pROI);
// 
// 		cvResetImageROI(pInputImage);
// 		cvReleaseImage(&pROI);
// 	}
// 
// 	for (int i = EiajOp_ROI_FST; i < ReOp_ItemNum; i++)
// 	{
// 		if (pstOpt->stRegionOp[i].bEnable == TRUE)
// 		{
// 			BOOL bCenterSide = FALSE;
// 			
// 			if (i > 5)
// 				bCenterSide = TRUE;
// 
// 			CvRect rcSz;
// 			rcSz.x = pstOpt->stRegionOp[i].rtRoi.left;
// 			rcSz.y = pstOpt->stRegionOp[i].rtRoi.top;
// 			rcSz.width = pstOpt->stRegionOp[i].rtRoi.Width();
// 			rcSz.height = pstOpt->stRegionOp[i].rtRoi.Height();
// 
// 			cvSetImageROI(pInputImage, rcSz);
// 
// 			IplImage* pROI = cvCreateImage(cvSize(rcSz.width, rcSz.height), IPL_DEPTH_8U, pInputImage->nChannels);
// 			cvCopy(pInputImage, pROI);
// 
// 			CString str;
// 			str.Format(_T("D:\\pROI_%d.bmp"), i);
// 			//cvSaveImage((CStringA)str, pROI);
// 
// 			double* dSFR_MTF_Value = new double[rcSz.width * rcSz.height];
// 			double* dSFR_MTF_LineFitting_Value = new double[rcSz.width * rcSz.height];
// 			double* dSFR_BWRatio = new double[rcSz.width * rcSz.height];
// 
// 			memset(dSFR_MTF_Value, 0, rcSz.width * rcSz.height * sizeof(double));
// 			memset(dSFR_MTF_LineFitting_Value, 0, rcSz.width * rcSz.height * sizeof(double));
// 			memset(dSFR_BWRatio, 0, rcSz.width * rcSz.height * sizeof(double));
// 
// 			int* iChecklineBuffer = new int[30];
// 
// 			for (int j = 0; j < 30;j++)
// 			{
// 				iChecklineBuffer[j] = 0;
// 			}
// 
// 			int iChecklineBufferCnt = 0;
// 
// 			int iSFRWidth = 0;
// 			int iSFRHeight = 0;
// 
// 			int iValue =
// 				TI_EIAJ.EIAJ_Test(
// 				bCenterSide,
// 				pROI,
// 				pstOpt->stRegionOp[i].nTestMode,
// 				pstOpt->stRegionOp[i].dbMTF +20,
// 				iRef_WhiteValue,
// 				iRef_BlackValue,
// 				dSFR_MTF_Value,
// 				dSFR_MTF_LineFitting_Value,
// 				dSFR_BWRatio,
// 				iSFRWidth,
// 				iSFRHeight,
// 				pstOpt->stRegionOp[i].dbMinRange,
// 				pstOpt->stRegionOp[i].dbMaxRange,
// 				pstOpt->stRegionOp[i].PatternStartPos,
// 				pstOpt->stRegionOp[i].PatternEndPos,
// 				iChecklineBuffer[iChecklineBufferCnt],
// 				iChecklineBufferCnt, i);
// 
// 			
// 			stResult.nValueResult[i - EiajOp_ROI_FST] = (int)(iValue + pstOpt->stRegionOp[i].dbOffset);
// 			if (stResult.nValueResult[i - EiajOp_ROI_FST] >= pstOpt->stRegionOp[i].dbMinSpc && stResult.nValueResult[i - EiajOp_ROI_FST] <= pstOpt->stRegionOp[i].dbMaxSpc)
// 			{
// 				stResult.nEachResult[i - EiajOp_ROI_FST] = TER_Pass;
// 			}
// 			else
// 			{
// 				stResult.nEachResult[i - EiajOp_ROI_FST] = TER_Fail;
// 			}
// 				
// 			delete[] dSFR_MTF_Value;
// 			delete[] dSFR_MTF_LineFitting_Value;
// 			delete[] dSFR_BWRatio;
// 			delete[] iChecklineBuffer;
// 
// 			cvResetImageROI(pInputImage);
// 			cvReleaseImage(&pROI);
// 		}
// 	}
// 
// 	cvReleaseImage(&pInputImage);
}

void CTestProcess_Image::OnTestProcessReverse(__in IplImage* pFrameImage, __in ST_Reverse_Opt* pstOpt, __out ST_Reverse_Result &stResult)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
	cvCopy(pFrameImage, pInputImage);
	
	int nwidth = pInputImage->width;
	int nheight = pInputImage->height;


	int iPosX = 0;
	int iPosY = 0;
	int iColor = 0;
	
	BOOL bResult = FALSE;
	
	stResult.Reset();

	//테스트
	for (int NUM = 0; NUM < ROI_Rv_Max;NUM++)
	{
		iPosX = stResult.ptCenter[NUM].x;
		iPosY = stResult.ptCenter[NUM].x;
		iColor = pstOpt->stRegionOp[NUM].nMarkColor;

		bResult = TI_Reverse.Reverse_Test(pInputImage, nwidth, nheight, pstOpt->stRegionOp[NUM].rtRoi, iPosX, iPosY, iColor);

		if (bResult == TRUE) //잡았을경우
		{
			stResult.nCamState = pstOpt->stRegionOp[NUM].nCamState;
			break;
		}
	}

	if (bResult == TRUE)
	{
		if (pstOpt->nStateType == stResult.nCamState)
		{
			stResult.nResult = TER_Pass;
			
		}
		else
		{
			stResult.nResult = TER_Fail;
		}
	}
	else
	{
		stResult.nResult = TER_Fail;
	}

	cvReleaseImage(&pInputImage);
}

void CTestProcess_Image::OnTestProcessReverseColor(__in IplImage* pFrameImage, __in ST_Reverse_Opt* pstOpt, __out ST_Reverse_Result &stResult)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
	cvCopy(pFrameImage, pInputImage);

	for (UINT nROI = 0; nROI < ROI_Rv_Max; nROI++)
	{
		pstOpt->stTestRegionOp[nROI].rtRoi.left = pstOpt->stRegionOp[nROI].rtRoi.left;
		pstOpt->stTestRegionOp[nROI].rtRoi.right = pstOpt->stRegionOp[nROI].rtRoi.right;
		pstOpt->stTestRegionOp[nROI].rtRoi.top = pstOpt->stRegionOp[nROI].rtRoi.top;
		pstOpt->stTestRegionOp[nROI].rtRoi.bottom = pstOpt->stRegionOp[nROI].rtRoi.bottom;

	}




	OnTestProcessReverseROI_Auto(pInputImage, pstOpt);

	BOOL bResult = TRUE;

	int iRed[ROI_Rv_resultMax] = { 0, };
	int iGreen[ROI_Rv_resultMax] = { 0, };
	int iBlue[ROI_Rv_resultMax] = { 0, };

	int iRGBResult[ReverseRGB_Max] = { 0, };

	// 3. 알고리즘
	for (UINT nROI = 0; nROI < ROI_Rv_Max; nROI++)
	{
		CvRect rcSz;
		rcSz.x = pstOpt->stTestRegionOp[nROI].rtRoi.left;
		rcSz.y = pstOpt->stTestRegionOp[nROI].rtRoi.top;
		rcSz.width = pstOpt->stTestRegionOp[nROI].rtRoi.Width();
		rcSz.height = pstOpt->stTestRegionOp[nROI].rtRoi.Height();

		cvSetImageROI(pInputImage, rcSz);

		IplImage* pROI = cvCreateImage(cvGetSize(pInputImage), IPL_DEPTH_8U, pInputImage->nChannels);
		cvCopy(pInputImage, pROI);

		TI_Color.Color_Test(pROI, iRed[nROI], iGreen[nROI], iBlue[nROI]);

		double iColor[3] = { iRed[nROI], iGreen[nROI], iBlue[nROI] };
		int iRGBMax = GetMax(iColor, 3);



#if 1
		if (abs(iRed[nROI] - iGreen[nROI]) < 20 && abs(iRed[nROI] - iBlue[nROI]) < 20)
		{
			iRGBResult[nROI] = ReverseRGB_Black;

		}
		else{
			//red
			if (iRed[nROI] > 80 && abs(iRed[nROI] - iGreen[nROI]) > 30 && abs(iRed[nROI] - iBlue[nROI]) > 30)
			{
				iRGBResult[nROI] = ReverseRGB_Red;

			}
			else if (iBlue[nROI] > 80 && abs(iRed[nROI] - iBlue[nROI]) > 30 && abs(iGreen[nROI] - iBlue[nROI]) > 30)
			{
				iRGBResult[nROI] = ReverseRGB_Blue;

			}
			else// if (iBlue[nROI] > 80 && abs(iRed[nROI] - iBlue[nROI]) > 30 && abs(iGreen[nROI] - iBlue[nROI]) > 30)
			{
				if (iRGBMax == iGreen[nROI])	// Green
				{
					iRGBResult[nROI] = ReverseRGB_Green;
				}

			}

		
		
		
		
		
		
		
		
		
		}
#else
		// Black
		if (iRed[nROI] <= 35 && iGreen[nROI] <= 35 && iBlue[nROI] <= 35)
		{
			iRGBResult[nROI] = ReverseRGB_Black;
		} 
		else
		{
			if (iRGBMax == iRed[nROI])			// Red
			{
				iRGBResult[nROI] = ReverseRGB_Red;
			}
			else if (iRGBMax == iGreen[nROI])	// Green
			{
				iRGBResult[nROI] = ReverseRGB_Green;
			}
			else if (iRGBMax == iBlue[nROI])	// Blue
			{
				iRGBResult[nROI] = ReverseRGB_Blue;
			}
		}
#endif

		cvReleaseImage(&pROI);
		cvResetImageROI(pInputImage);
	}

	// 역상 상태 구하기
	int iChkCount = 0;
	int iReverseState = 4;
	for (int i = 0; i < ROI_Rv_resultMax; i++)
	{
		iChkCount = 0;
		for (int j = 0; j < ReverseRGB_Max; j++)
		{
			if (pstOpt->stRegionSubOp[i].nRGBType[j] == iRGBResult[j])
			{
				iChkCount++;
			}
		}

		if (iChkCount == ReverseRGB_Max)
		{
			iReverseState = i;
			break;
		}
	}

	stResult.nCamState = iReverseState;

	if (iReverseState == pstOpt->nStateType)
	{
		bResult = TRUE;
	}
	else
	{
		bResult = FALSE;
	}

	// 4. 결과
	if (bResult == TRUE)
	{
		stResult.nResult = TER_Pass;
	}
	else
	{
		stResult.nResult = TER_Fail;
	}

	cvReleaseImage(&pInputImage);
}

void CTestProcess_Image::OnTestProcessPatternNoise(__in IplImage* pFrameImage, __in ST_PatternNoise_Opt* pstOpt, __out ST_PatternNoise_Result &stResult)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	stResult.iTotalCnt = 0;
	stResult.iPassCnt = 0;
	stResult.nResult = TER_Fail;

	for (int i = 0; i < ROI_PN_MaxEnum;i++)
		stResult.nEachResult[i] = 2;

	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
	cvCopy(pFrameImage, pInputImage);

	//BYTE	*m_RGBScanbuf = NULL;
	//m_RGBScanbuf = new BYTE[pInputImage->width * pInputImage->height * 4];
	//memset(m_RGBScanbuf, 0, pInputImage->width * pInputImage->height * 4);

	int nwidth = pInputImage->width;
	int height = pInputImage->height;
	
	BOOL Result = FALSE;

	for (int NUM = 0; NUM < ROI_PN_MaxEnum; NUM++)
	{
		stResult.dValue_dB[NUM] = 0;
		stResult.dValue_Env[NUM] = 0;

		TI_PatternNoise.PatternNoise_Test(pInputImage, pstOpt->stRegionOp[NUM].rtRoi, NUM, nwidth, height, stResult.dValue_Env[NUM], stResult.dValue_dB[NUM]);

		// 만약 기준 신호레벨은 0이라면, SNR은 1이다.(전체평균 = 평균 noise 편차)
		if (stResult.dValue_Env[NUM] > pstOpt->dThr_Env) //Test 환경이 아닐때
		{
			Result = FALSE;
			stResult.nEachResult[NUM] = TER_Fail;
			stResult.dValue_dB[NUM] = 0;
		}
		else
		{
			if (stResult.dValue_dB[NUM] >= pstOpt->dThr_dB)//임지은 이상인지 이하인지 확인
			{
				Result = TRUE;
				stResult.nEachResult[NUM] = TER_Pass;

			}
			else
			{
				Result = FALSE;
				stResult.nEachResult[NUM] = TER_Fail;
			}
		}
		if (Result == TRUE) //TEST 합격이면
		{
			stResult.iPassCnt++; //Passcount를 올린다
		}

		stResult.iTotalCnt++; //전체 count를 온린다.

	}

	if (stResult.iTotalCnt == stResult.iPassCnt) //합격 카운터와 전체 카운터가 같으면
	{
		stResult.nResult = TER_Pass;
	}
	else
	{
		stResult.nResult = TER_Fail;

	}



	//delete m_RGBScanbuf;
	//m_RGBScanbuf = NULL;
	cvReleaseImage(&pInputImage);
}

void CTestProcess_Image::OnTestProcessIRFilter(__in IplImage* pFrameImage, __in ST_IRFilter_Opt* pstOpt, __out ST_IRFilter_Result &stResult)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	stResult.Reset();

	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
	IplImage* pGrayImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, 1);

	cvCopy(pFrameImage, pInputImage);
	cvCvtColor(pFrameImage, pGrayImage, CV_BGR2GRAY);

	int iRed = 0;
	int iGreen = 0;
	int iBlue = 0;
	int iGray = 0;

	for (int y = 0; y < pInputImage->height; y++)
	{
		for (int x = 0; x < pInputImage->width; x++)
		{
			BYTE Blue = pInputImage->imageData[y * pInputImage->widthStep + x * pInputImage->nChannels + 0];
			BYTE Green = pInputImage->imageData[y * pInputImage->widthStep + x * pInputImage->nChannels + 1];
			BYTE Red = pInputImage->imageData[y * pInputImage->widthStep + x * pInputImage->nChannels + 2];

			BYTE Gray = pGrayImage->imageData[y * pGrayImage->widthStep + x * pGrayImage->nChannels];

			iBlue += Blue;
			iGreen += Green;
			iRed += Red;
			iGray += Gray;
		}
	}

	int iAvgRed = iRed / (pInputImage->width * pInputImage->height);	// 레드
	int iAvgGreen = iGreen / (pInputImage->width * pInputImage->height);
	int iAvgBlue = iBlue / (pInputImage->width * pInputImage->height);

	int iAvgGray = iGray / (pInputImage->width * pInputImage->height); // 환경변수

	stResult.dValue_Env = (double)iAvgGray;
	stResult.dValue = (double)iAvgRed;

	// 결과
	if (stResult.dValue_Env > pstOpt->dThr_Env) //Test 환경이 아닐때
	{
		stResult.nResult = TER_Fail;
	}
	else // 환경일 때
	{
		if (stResult.dValue < pstOpt->dThreshold)
		{
			stResult.nResult = TER_Pass;
		} 
		else
		{
			stResult.nResult = TER_Fail;
		}
	}

	cvReleaseImage(&pInputImage);
	cvReleaseImage(&pGrayImage);
}

void CTestProcess_Image::OnTestProcessSFRROI_Auto(__in IplImage* pFrameImage, __inout ST_SFR_Opt* pstOpt)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	CvMemStorage* pContourStorage = cvCreateMemStorage(0);
	CvSeq *pContour = 0;
	IplImage* pSrcImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, 1);
	IplImage* pBinaryImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, 1);

	IplImage* pContoresImage = NULL;
	IplImage* eig_image = NULL;
	IplImage* temp_image = NULL;

	cvCvtColor(pFrameImage, pSrcImage, CV_RGB2GRAY);
	//cvThreshold(pSrcImage, pBinaryImage, 30, 255, CV_THRESH_BINARY);

	cvAdaptiveThreshold(pSrcImage, pBinaryImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 71, 5);
	cvNot(pBinaryImage, pBinaryImage);
	cvRectangle(pBinaryImage, cvPoint(0, 0), cvPoint(pBinaryImage->width - 3, pBinaryImage->height - 3), CV_RGB(0, 0, 0), 6);
	cvFindContours(pBinaryImage, pContourStorage, &pContour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	CvRect rcContour;
	CRect rtROI[ROI_SFR_Max];

	for (int i = 0; i < ROI_SFR_Max; i++)
	{
		CvPoint Corr_Center;

		if (pstOpt->stRegionOp[i].bEnable)
		{
			int _W = pstOpt->stRegionOp[i].rtRoi.right - pstOpt->stRegionOp[i].rtRoi.left;
			int _H = pstOpt->stRegionOp[i].rtRoi.bottom - pstOpt->stRegionOp[i].rtRoi.top;

			CvSeq *pContour_Tmp = pContour;

			rtROI[i] = pstOpt->stRegionOp[i].rtRoi;
			CvPoint ROICenterPoint;

			ROICenterPoint.x = (rtROI[i].left + rtROI[i].right) / 2;
			ROICenterPoint.y = (rtROI[i].top + rtROI[i].bottom) / 2;

			// ROI에 근접한 Contours 구하기
			double Old_Dist = 99999;
			double Max_Size = 0;

			CvRect Near_ContP;
			BOOL TRUE_Flag = FALSE;
			for (; pContour_Tmp != 0; pContour_Tmp = pContour_Tmp->h_next)
			{
				rcContour = cvBoundingRect(pContour_Tmp, 1);
				double Size_Rect = rcContour.width * rcContour.height;
				double Dist_Contour2ROI = TI_CenterPoint.GetDistance(rcContour.x + (rcContour.width / 2), rcContour.y + (rcContour.height / 2), ROICenterPoint.x, ROICenterPoint.y);
				if (rcContour.height > _H && rcContour.width > _W)
				{
					if (Old_Dist > Dist_Contour2ROI && rcContour.width < pFrameImage->width / 2 && rcContour.height < pFrameImage->height / 2)
					{

						Old_Dist = Dist_Contour2ROI;
						Near_ContP.x = rcContour.x - 5; // 여유 +-5 pixel
						Near_ContP.y = rcContour.y - 5; // 여유 +-5 pixel
						Near_ContP.width = rcContour.width + 10;
						Near_ContP.height = rcContour.height + 10;
						TRUE_Flag = TRUE;
					}
				}

			}

			// 근접한 Contours 이미지 따기
			pContoresImage = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_8U, 1);
			eig_image = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_32F, 1);
			temp_image = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_32F, 1);

			if (Near_ContP.x < 0)
				Near_ContP.x = 0;

			if (Near_ContP.y < 0)
				Near_ContP.y = 0;

			if (Near_ContP.x + Near_ContP.width > pFrameImage->width - 1)
				Near_ContP.x = pFrameImage->width - Near_ContP.width - 1;

			if (Near_ContP.y + Near_ContP.height > pFrameImage->height - 1)
				Near_ContP.y = pFrameImage->height - Near_ContP.height - 1;

			cvSetImageROI(pSrcImage, Near_ContP);
			cvCopy(pSrcImage, pContoresImage);
			cvResetImageROI(pSrcImage);

			// 스무딩 처리
			cvDilate(pContoresImage, pContoresImage, 0, 3);
			cvErode(pContoresImage, pContoresImage, 0, 3);
			//cvSmooth(pContoresImage, pContoresImage);

			cvNormalize(pContoresImage, pContoresImage,0,255,NORM_MINMAX);

			cvDilate(pContoresImage, pContoresImage, 0, 3);
			cvErode(pContoresImage, pContoresImage, 0, 3);

			int MAX_CORNERS = 2000;
			CvPoint2D32f* corners = new CvPoint2D32f[MAX_CORNERS];
			CvPoint2D32f* corners_Final = new CvPoint2D32f[MAX_CORNERS];

			// 코너 찾기
			cvGoodFeaturesToTrack(pContoresImage, eig_image, temp_image, corners, &MAX_CORNERS, 0.12, 30.0, 0, 7, 0, 0.04);

			int Corners_Cnt = 0;
		
			for (int j = 0; j < MAX_CORNERS; j++)
			{ 
				if (!(corners[j].x > pContoresImage->width / 3  && corners[j].x < pContoresImage->width *2.0/ 3  &&
					corners[j].y > pContoresImage->height / 3  && corners[j].y < pContoresImage->height *2.0/ 3 ))
				{
					corners_Final[Corners_Cnt] = corners[j];
					cvCircle(pContoresImage, cvPoint(corners[j].x, corners[j].y), 4, CV_RGB(255, 0, 0), 1);
					Corners_Cnt++;
				}

			}

			// 월드좌표 변환
			for (int j = 0; j < Corners_Cnt; j++)
			{
				corners_Final[j].x += Near_ContP.x;
				corners_Final[j].y += Near_ContP.y;
			}

			double Dist_1st = 0.0;
			CvPoint Dist_1stPoint;

			double Dist_2rd = 0.0;
			CvPoint Dist_2rdPoint;


			// W가 큰 ROI 일때 (가로SFR) => X축, Y축에 대한 거리 추출 가중치가 달라짐

			if (rtROI[i].Width() > rtROI[i].Height())
			{
				// 1st
				double Max_Tmp = 99999;

				for (int k = 0; k < Corners_Cnt; k++)
				{
					int Pointy_Mul = (corners_Final[k].y - ROICenterPoint.y) * 20;

					Dist_1st = TI_CenterPoint.GetDistance(ROICenterPoint.x, ROICenterPoint.y, corners_Final[k].x, corners_Final[k].y - Pointy_Mul);
					if (Max_Tmp > Dist_1st)
					{
						Max_Tmp = Dist_1st;
						Dist_1stPoint.x = corners_Final[k].x;
						Dist_1stPoint.y = corners_Final[k].y;
					}
				}

				// 2rd
				Max_Tmp = 99999;
				for (int k = 0; k < Corners_Cnt; k++)
				{
					// 1st Point 를 제외한 가장 가까운 Point = 2rd 
					if (corners_Final[k].x != Dist_1stPoint.x && corners_Final[k].y != Dist_1stPoint.y && (corners_Final[k].y < Dist_1stPoint.y + _H  && corners_Final[k].y > Dist_1stPoint.y - _H ))
					{
						// 가중치를 적용한 거리 계산
						int Pointy_Mul = (corners_Final[k].y - ROICenterPoint.y) * 20;
						Dist_2rd = TI_CenterPoint.GetDistance(ROICenterPoint.x, ROICenterPoint.y, corners_Final[k].x, corners_Final[k].y - Pointy_Mul);

						if (Max_Tmp > Dist_2rd)
						{
							Max_Tmp = Dist_2rd;
							Dist_2rdPoint.x = corners_Final[k].x;
							Dist_2rdPoint.y = corners_Final[k].y;
						}
					}
				}
			}

			// H가 큰 ROI 일때 (세로SFR)
			else if (rtROI[i].Width() < rtROI[i].Height())
			{
				// 1st
				double Max_Tmp = 99999;

				for (int k = 0; k < Corners_Cnt; k++)
				{
					int Pointy_Mul = (corners_Final[k].x - ROICenterPoint.x) * 20;

					Dist_1st = TI_CenterPoint.GetDistance(ROICenterPoint.x, ROICenterPoint.y, corners_Final[k].x + Pointy_Mul, corners_Final[k].y);
					if (Max_Tmp > Dist_1st)
					{
						Max_Tmp = Dist_1st;
						Dist_1stPoint.x = corners_Final[k].x;
						Dist_1stPoint.y = corners_Final[k].y;
					}
				}

				// 2rd
				Max_Tmp = 99999;
				for (int k = 0; k < Corners_Cnt; k++)
				{

					// 1st Point 를 제외한 가장 가까운 Point = 2rd 
					if (corners_Final[k].x != Dist_1stPoint.x && corners_Final[k].y != Dist_1stPoint.y && (corners_Final[k].x <= Dist_1stPoint.x + _W  && corners_Final[k].x >= Dist_1stPoint.x - _W ))
					{
						// 가중치를 적용한 거리 계산
						int Pointy_Mul = (corners_Final[k].x - ROICenterPoint.x) * 20;
						Dist_2rd = TI_CenterPoint.GetDistance(ROICenterPoint.x, ROICenterPoint.y, corners_Final[k].x + Pointy_Mul, corners_Final[k].y);

						if (Max_Tmp > Dist_2rd)
						{
							Max_Tmp = Dist_2rd;
							Dist_2rdPoint.x = corners_Final[k].x;
							Dist_2rdPoint.y = corners_Final[k].y;
						}
					}
				}
			}

			Corr_Center.x = (int)((Dist_1stPoint.x + Dist_2rdPoint.x) / 2.0);
			Corr_Center.y = (int)((Dist_1stPoint.y + Dist_2rdPoint.y) / 2.0);

			//cvCircle(pContoresImage, Corr_Center, 4, CV_RGB(255, 255, 255), 1);


			// 보정값 넣기
			pstOpt->stRegionOp[i].rtRoi.left = Corr_Center.x - (_W / 2);
			pstOpt->stRegionOp[i].rtRoi.top = Corr_Center.y - (_H / 2);
			pstOpt->stRegionOp[i].rtRoi.right = pstOpt->stRegionOp[i].rtRoi.left + _W;
			pstOpt->stRegionOp[i].rtRoi.bottom = pstOpt->stRegionOp[i].rtRoi.top + _H;

			delete[] corners;
			cvReleaseImage(&eig_image);
			cvReleaseImage(&temp_image);
			cvReleaseImage(&pContoresImage);

		}

	}

	cvReleaseMemStorage(&pContourStorage);
	cvReleaseImage(&pSrcImage);
	cvReleaseImage(&pBinaryImage);


}


void CTestProcess_Image::OnTestProcessColorROI_Auto(__in IplImage* pFrameImage, __inout ST_Color_Opt* pstOpt)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	CPoint ptCenter;

	// 초기값
	ptCenter.x = -1;
	ptCenter.y = -1;

	IplImage *OriginImage = cvCreateImage(cvSize(pFrameImage->width, pFrameImage->height), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(pFrameImage->width, pFrameImage->height), IPL_DEPTH_8U, 1);

	cvSetZero(OriginImage);
	cvSetZero(DilateImage);

	// 3채널 -> 1채널로 변환
	cvCvtColor(pFrameImage, OriginImage, CV_RGB2GRAY);

	// 2진화
	cvThreshold(OriginImage, DilateImage, 100, 255, CV_THRESH_BINARY);

	cvNot(DilateImage, DilateImage);
	cvErode(DilateImage, DilateImage);

	

	CvSeq *Contour = 0;

	CvMemStorage* CvStorage = cvCreateMemStorage(0);
	cvFindContours(DilateImage, CvStorage, &Contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double dbOldDistance = 999999;

	int iCenterX = 0;
	int iCenterY = 0;
	int iCntCountour = 0;

	CvRect rtTemp;

	for (int i = 0; i<ROI_CL_Max; i++)
	{
		// CvSeq *pContourBuf = Contour;
		{
			int W = pstOpt->stTestRegionOp[i].rtRoi.right - pstOpt->stTestRegionOp[i].rtRoi.left;
			int H = pstOpt->stTestRegionOp[i].rtRoi.bottom - pstOpt->stTestRegionOp[i].rtRoi.top;

			ptCenter.x = -1;
			ptCenter.y = -1;
			dbOldDistance = 999999;

			CvSeq *pContourBuf = Contour;

			for (; pContourBuf != 0; pContourBuf = pContourBuf->h_next)
			{
				rtTemp = cvContourBoundingRect(pContourBuf, 1);

				double dbCircularity = (4.0 * M_PI * cvContourArea(pContourBuf, CV_WHOLE_SEQ)) / (cvArcLength(pContourBuf, CV_WHOLE_SEQ, -1) * cvArcLength(pContourBuf, CV_WHOLE_SEQ, -1));

				// 값이 작을 수록 찌그러진 것도 잡는다.
				//if (dbCircularity > 0.75)
				{
					CvPoint pt1, pt2;

					pt1.x = rtTemp.x;
					pt1.y = rtTemp.y;

					pt2.x = rtTemp.x + rtTemp.width;
					pt2.y = rtTemp.y + rtTemp.height;

					//cvRectangle(DilateImage, pt1, pt2, CV_RGB(255, 255, 255));

					int iDevCenterX1 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().x - (pFrameImage->width / 14);
					int iDevCenterX2 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().x + (pFrameImage->width / 14);

					int iDevCenterY1 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().y - (pFrameImage->height / 10);
					int iDevCenterY2 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().y + (pFrameImage->height / 10);

					if (rtTemp.x + (rtTemp.width / 2) > iDevCenterX1
						&& rtTemp.x + (rtTemp.width / 2) < iDevCenterX2
						&& rtTemp.y + (rtTemp.height / 2) > iDevCenterY1
						&& rtTemp.y + (rtTemp.height / 2) < iDevCenterY2)
					{
						iCenterX = rtTemp.x + rtTemp.width / 2;
						iCenterY = rtTemp.y + rtTemp.height / 2;

						if (rtTemp.width < pFrameImage->width / 2 && rtTemp.height < pFrameImage->height / 2 && rtTemp.width > 10 && rtTemp.height > 10)
						{
							double dbDistance = TI_CenterPoint.GetDistance(
								pstOpt->stTestRegionOp[i].rtRoi.left,
								pstOpt->stTestRegionOp[i].rtRoi.right,
								iCenterX, iCenterY);

							if (dbDistance < dbOldDistance)
							{
								ptCenter.x = iCenterX;
								ptCenter.y = iCenterY;
								dbOldDistance = dbDistance;
							}
						}
					}
				}

				iCntCountour++;
			}

			if (ptCenter.x > -1 && ptCenter.y > -1)
			{
				pstOpt->stTestRegionOp[i].rtRoi.left	= ptCenter.x - (W / 2);
				pstOpt->stTestRegionOp[i].rtRoi.top		= ptCenter.y - (H / 2);
				pstOpt->stTestRegionOp[i].rtRoi.right	= pstOpt->stTestRegionOp[i].rtRoi.left + W;
				pstOpt->stTestRegionOp[i].rtRoi.bottom	= pstOpt->stTestRegionOp[i].rtRoi.top + H;
			}
		}
	}

	//cvSaveImage("D:\\ImageDebug.bmp", DilateImage);

	cvReleaseImage(&OriginImage);
	cvReleaseImage(&DilateImage);
	cvReleaseMemStorage(&CvStorage);

}


void CTestProcess_Image::OnTestProcessRotateROI_Auto(__in IplImage* pFrameImage, __inout ST_Rotate_Opt* pstOpt)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	CPoint ptCenter;

	// 초기값
	ptCenter.x = -1;
	ptCenter.y = -1;

	IplImage *OriginImage = cvCreateImage(cvSize(pFrameImage->width, pFrameImage->height), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(pFrameImage->width, pFrameImage->height), IPL_DEPTH_8U, 1);

	cvSetZero(OriginImage);
	cvSetZero(DilateImage);

	// 3채널 -> 1채널로 변환
	cvCvtColor(pFrameImage, OriginImage, CV_RGB2GRAY);

	// 2진화
	cvThreshold(OriginImage, DilateImage, 100, 255, CV_THRESH_BINARY);

	cvNot(DilateImage, DilateImage);
	cvErode(DilateImage, DilateImage);



	CvSeq *Contour = 0;

	CvMemStorage* CvStorage = cvCreateMemStorage(0);
	cvFindContours(DilateImage, CvStorage, &Contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double dbOldDistance = 999999;

	int iCenterX = 0;
	int iCenterY = 0;
	int iCntCountour = 0;

	CvRect rtTemp;

	for (int i = 0; i < ROI_CL_Max; i++)
	{
		// CvSeq *pContourBuf = Contour;
		{
			int W = pstOpt->stTestRegionOp[i].rtRoi.right - pstOpt->stTestRegionOp[i].rtRoi.left;
			int H = pstOpt->stTestRegionOp[i].rtRoi.bottom - pstOpt->stTestRegionOp[i].rtRoi.top;

			ptCenter.x = -1;
			ptCenter.y = -1;
			dbOldDistance = 999999;

			CvSeq *pContourBuf = Contour;

			for (; pContourBuf != 0; pContourBuf = pContourBuf->h_next)
			{
				rtTemp = cvContourBoundingRect(pContourBuf, 1);

				double dbCircularity = (4.0 * M_PI * cvContourArea(pContourBuf, CV_WHOLE_SEQ)) / (cvArcLength(pContourBuf, CV_WHOLE_SEQ, -1) * cvArcLength(pContourBuf, CV_WHOLE_SEQ, -1));

				// 값이 작을 수록 찌그러진 것도 잡는다.
				//if (dbCircularity > 0.75)
				{
					CvPoint pt1, pt2;

					pt1.x = rtTemp.x;
					pt1.y = rtTemp.y;

					pt2.x = rtTemp.x + rtTemp.width;
					pt2.y = rtTemp.y + rtTemp.height;

					//cvRectangle(DilateImage, pt1, pt2, CV_RGB(255, 255, 255));

					int iDevCenterX1 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().x - (pFrameImage->width / 14);
					int iDevCenterX2 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().x + (pFrameImage->width / 14);

					int iDevCenterY1 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().y - (pFrameImage->height / 10);
					int iDevCenterY2 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().y + (pFrameImage->height / 10);

					if (rtTemp.x + (rtTemp.width / 2) > iDevCenterX1
						&& rtTemp.x + (rtTemp.width / 2) < iDevCenterX2
						&& rtTemp.y + (rtTemp.height / 2) > iDevCenterY1
						&& rtTemp.y + (rtTemp.height / 2) < iDevCenterY2)
					{
						iCenterX = rtTemp.x + rtTemp.width / 2;
						iCenterY = rtTemp.y + rtTemp.height / 2;

						if (rtTemp.width < pFrameImage->width / 2 && rtTemp.height < pFrameImage->height / 2 && rtTemp.width > 10 && rtTemp.height > 10)
						{
							double dbDistance = TI_CenterPoint.GetDistance(
								pstOpt->stTestRegionOp[i].rtRoi.left,
								pstOpt->stTestRegionOp[i].rtRoi.right,
								iCenterX, iCenterY);

							if (dbDistance < dbOldDistance)
							{
								ptCenter.x = iCenterX;
								ptCenter.y = iCenterY;
								dbOldDistance = dbDistance;
							}
						}
					}
				}

				iCntCountour++;
			}

			if (ptCenter.x > -1 && ptCenter.y > -1)
			{
				pstOpt->stTestRegionOp[i].rtRoi.left = ptCenter.x - (W / 2);
				pstOpt->stTestRegionOp[i].rtRoi.top = ptCenter.y - (H / 2);
				pstOpt->stTestRegionOp[i].rtRoi.right = pstOpt->stTestRegionOp[i].rtRoi.left + W;
				pstOpt->stTestRegionOp[i].rtRoi.bottom = pstOpt->stTestRegionOp[i].rtRoi.top + H;
			}
		}
	}

	//cvSaveImage("D:\\ImageDebug.bmp", DilateImage);

	cvReleaseImage(&OriginImage);
	cvReleaseImage(&DilateImage);
	cvReleaseMemStorage(&CvStorage);

}

void CTestProcess_Image::OnTestProcessReverseROI_Auto(__in IplImage* pFrameImage, __inout ST_Reverse_Opt* pstOpt)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	CPoint ptCenter;

	// 초기값
	ptCenter.x = -1;
	ptCenter.y = -1;

	IplImage *OriginImage = cvCreateImage(cvSize(pFrameImage->width, pFrameImage->height), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(pFrameImage->width, pFrameImage->height), IPL_DEPTH_8U, 1);

	cvSetZero(OriginImage);
	cvSetZero(DilateImage);

	// 3채널 -> 1채널로 변환
	cvCvtColor(pFrameImage, OriginImage, CV_RGB2GRAY);

	// 2진화
	cvThreshold(OriginImage, DilateImage, 100, 255, CV_THRESH_BINARY);

	cvNot(DilateImage, DilateImage);
	cvErode(DilateImage, DilateImage);



	CvSeq *Contour = 0;

	CvMemStorage* CvStorage = cvCreateMemStorage(0);
	cvFindContours(DilateImage, CvStorage, &Contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double dbOldDistance = 999999;

	int iCenterX = 0;
	int iCenterY = 0;
	int iCntCountour = 0;

	CvRect rtTemp;

	for (int i = 0; i < ROI_Rv_Max; i++)
	{
		// CvSeq *pContourBuf = Contour;
		{
			int W = pstOpt->stTestRegionOp[i].rtRoi.right - pstOpt->stTestRegionOp[i].rtRoi.left;
			int H = pstOpt->stTestRegionOp[i].rtRoi.bottom - pstOpt->stTestRegionOp[i].rtRoi.top;

			ptCenter.x = -1;
			ptCenter.y = -1;
			dbOldDistance = 999999;

			CvSeq *pContourBuf = Contour;

			for (; pContourBuf != 0; pContourBuf = pContourBuf->h_next)
			{
				rtTemp = cvContourBoundingRect(pContourBuf, 1);

				double dbCircularity = (4.0 * M_PI * cvContourArea(pContourBuf, CV_WHOLE_SEQ)) / (cvArcLength(pContourBuf, CV_WHOLE_SEQ, -1) * cvArcLength(pContourBuf, CV_WHOLE_SEQ, -1));

				// 값이 작을 수록 찌그러진 것도 잡는다.
				//if (dbCircularity > 0.75)
				{
					CvPoint pt1, pt2;

					pt1.x = rtTemp.x;
					pt1.y = rtTemp.y;

					pt2.x = rtTemp.x + rtTemp.width;
					pt2.y = rtTemp.y + rtTemp.height;

					//cvRectangle(DilateImage, pt1, pt2, CV_RGB(255, 255, 255));

					int iDevCenterX1 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().x - (pFrameImage->width / 14);
					int iDevCenterX2 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().x + (pFrameImage->width / 14);

					int iDevCenterY1 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().y - (pFrameImage->height / 10);
					int iDevCenterY2 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().y + (pFrameImage->height / 10);

					if (rtTemp.x + (rtTemp.width / 2) > iDevCenterX1
						&& rtTemp.x + (rtTemp.width / 2) < iDevCenterX2
						&& rtTemp.y + (rtTemp.height / 2) > iDevCenterY1
						&& rtTemp.y + (rtTemp.height / 2) < iDevCenterY2)
					{
						iCenterX = rtTemp.x + rtTemp.width / 2;
						iCenterY = rtTemp.y + rtTemp.height / 2;

						if (rtTemp.width < pFrameImage->width / 2 && rtTemp.height < pFrameImage->height / 2 && rtTemp.width > 10 && rtTemp.height > 10)
						{
							double dbDistance = TI_CenterPoint.GetDistance(
								pstOpt->stTestRegionOp[i].rtRoi.left,
								pstOpt->stTestRegionOp[i].rtRoi.right,
								iCenterX, iCenterY);

							if (dbDistance < dbOldDistance)
							{
								ptCenter.x = iCenterX;
								ptCenter.y = iCenterY;
								dbOldDistance = dbDistance;
							}
						}
					}
				}

				iCntCountour++;
			}

			if (ptCenter.x > -1 && ptCenter.y > -1)
			{
				pstOpt->stTestRegionOp[i].rtRoi.left = ptCenter.x - (W / 2);
				pstOpt->stTestRegionOp[i].rtRoi.top = ptCenter.y - (H / 2);
				pstOpt->stTestRegionOp[i].rtRoi.right = pstOpt->stTestRegionOp[i].rtRoi.left + W;
				pstOpt->stTestRegionOp[i].rtRoi.bottom = pstOpt->stTestRegionOp[i].rtRoi.top + H;
			}
		}
	}

	//cvSaveImage("D:\\ImageDebug.bmp", DilateImage);

	cvReleaseImage(&OriginImage);
	cvReleaseImage(&DilateImage);
	cvReleaseMemStorage(&CvStorage);
}

void CTestProcess_Image::OnTestProcessFovROI_Auto(__in IplImage* pFrameImage, __inout ST_Angle_Opt* pstOpt)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	CPoint ptCenter;

	// 초기값
	ptCenter.x = -1;
	ptCenter.y = -1;

	IplImage *OriginImage = cvCreateImage(cvSize(pFrameImage->width, pFrameImage->height), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(pFrameImage->width, pFrameImage->height), IPL_DEPTH_8U, 1);

	cvSetZero(OriginImage);
	cvSetZero(DilateImage);

	// 3채널 -> 1채널로 변환
	cvCvtColor(pFrameImage, OriginImage, CV_RGB2GRAY);

	// 2진화
	cvThreshold(OriginImage, DilateImage, 100, 255, CV_THRESH_BINARY);

	cvNot(DilateImage, DilateImage);
	cvErode(DilateImage, DilateImage);



	CvSeq *Contour = 0;

	CvMemStorage* CvStorage = cvCreateMemStorage(0);
	cvFindContours(DilateImage, CvStorage, &Contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double dbOldDistance = 999999;

	int iCenterX = 0;
	int iCenterY = 0;
	int iCntCountour = 0;

	CvRect rtTemp;

	for (int i = 0; i < ROI_AL_Max; i++)
	{
		// CvSeq *pContourBuf = Contour;
		{
			int W = pstOpt->stTestRegionOp[i].rtRoi.right - pstOpt->stTestRegionOp[i].rtRoi.left;
			int H = pstOpt->stTestRegionOp[i].rtRoi.bottom - pstOpt->stTestRegionOp[i].rtRoi.top;

			ptCenter.x = -1;
			ptCenter.y = -1;
			dbOldDistance = 999999;

			CvSeq *pContourBuf = Contour;

			for (; pContourBuf != 0; pContourBuf = pContourBuf->h_next)
			{
				rtTemp = cvContourBoundingRect(pContourBuf, 1);

				double dbCircularity = (4.0 * M_PI * cvContourArea(pContourBuf, CV_WHOLE_SEQ)) / (cvArcLength(pContourBuf, CV_WHOLE_SEQ, -1) * cvArcLength(pContourBuf, CV_WHOLE_SEQ, -1));

				// 값이 작을 수록 찌그러진 것도 잡는다.
				if (dbCircularity > 0.6)
				{
					CvPoint pt1, pt2;

					pt1.x = rtTemp.x;
					pt1.y = rtTemp.y;

					pt2.x = rtTemp.x + rtTemp.width;
					pt2.y = rtTemp.y + rtTemp.height;

					//cvRectangle(DilateImage, pt1, pt2, CV_RGB(255, 255, 255));

					int iDevCenterX1 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().x - (pFrameImage->width / 14);
					int iDevCenterX2 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().x + (pFrameImage->width / 14);

					int iDevCenterY1 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().y - (pFrameImage->height / 10);
					int iDevCenterY2 = pstOpt->stTestRegionOp[i].rtRoi.CenterPoint().y + (pFrameImage->height / 10);

					if (rtTemp.x + (rtTemp.width / 2) > iDevCenterX1
						&& rtTemp.x + (rtTemp.width / 2) < iDevCenterX2
						&& rtTemp.y + (rtTemp.height / 2) > iDevCenterY1
						&& rtTemp.y + (rtTemp.height / 2) < iDevCenterY2)
					{
						iCenterX = rtTemp.x + rtTemp.width / 2;
						iCenterY = rtTemp.y + rtTemp.height / 2;

						if (rtTemp.width < pFrameImage->width / 2 && rtTemp.height < pFrameImage->height / 2 && rtTemp.width > 10 && rtTemp.height > 10)
						{
							double dbDistance = TI_CenterPoint.GetDistance(
								pstOpt->stTestRegionOp[i].rtRoi.left,
								pstOpt->stTestRegionOp[i].rtRoi.right,
								iCenterX, iCenterY);

							if (dbDistance < dbOldDistance)
							{
								ptCenter.x = iCenterX;
								ptCenter.y = iCenterY;
								dbOldDistance = dbDistance;
							}
						}
					}
				}

				iCntCountour++;
			}

			if (ptCenter.x > -1 && ptCenter.y > -1)
			{
				pstOpt->stTestRegionOp[i].rtRoi.left = ptCenter.x - (W / 2);
				pstOpt->stTestRegionOp[i].rtRoi.top = ptCenter.y - (H / 2);
				pstOpt->stTestRegionOp[i].rtRoi.right = pstOpt->stTestRegionOp[i].rtRoi.left + W;
				pstOpt->stTestRegionOp[i].rtRoi.bottom = pstOpt->stTestRegionOp[i].rtRoi.top + H;
			}
		}
	}
	
	//cvSaveImage("D:\\ImageDebug.bmp", DilateImage);

	cvReleaseImage(&OriginImage);
	cvReleaseImage(&DilateImage);
	cvReleaseMemStorage(&CvStorage);
}
void CTestProcess_Image::OnAutoSettingProcessAngle(__in IplImage* pFrameImage, __inout ST_Angle_Opt* pstOpt, __out ST_Angle_Result &stResult)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
	cvCopy(pFrameImage, pInputImage);
	for (UINT nROI = ROI_AL_Center; nROI < ROI_AL_Max; nROI++)
	{
		pstOpt->stTestRegionOp[nROI].rtRoi.left = pstOpt->stRegionOp[nROI].rtRoi.left;
		pstOpt->stTestRegionOp[nROI].rtRoi.right = pstOpt->stRegionOp[nROI].rtRoi.right;
		pstOpt->stTestRegionOp[nROI].rtRoi.top = pstOpt->stRegionOp[nROI].rtRoi.top;
		pstOpt->stTestRegionOp[nROI].rtRoi.bottom = pstOpt->stRegionOp[nROI].rtRoi.bottom;

	}



	OnTestProcessFovROI_Auto(pInputImage, pstOpt);
// 
// 	// ROI 보정 알고리즘
// 	for (UINT nROI = ROI_AL_Center; nROI < ROI_AL_Max; nROI++)
// 	{
// 		CPoint ptCenter = TI_CenterPoint.CenterPoint_Test(pstOpt->stRegionOp[nROI].rtRoi, pstOpt->stRegionOp[nROI].nMarkColor, pInputImage);
// 
// 		// ROI 보정
// 		if (ptCenter.x != -1 && ptCenter.x != -1)
// 		{
// 			pstOpt->stRegionOp[nROI].rtRoi.left = ptCenter.x - (pstOpt->stRegionOp[nROI].rtRoi.Width() / 2);
// 			pstOpt->stRegionOp[nROI].rtRoi.right = ptCenter.x + (pstOpt->stRegionOp[nROI].rtRoi.Width() / 2);
// 			pstOpt->stRegionOp[nROI].rtRoi.top = ptCenter.y - (pstOpt->stRegionOp[nROI].rtRoi.Height() / 2);
// 			pstOpt->stRegionOp[nROI].rtRoi.bottom = ptCenter.y + (pstOpt->stRegionOp[nROI].rtRoi.Height() / 2);
// 		}
// 
// 	}

	for (UINT nROI = ROI_AL_Center; nROI < ROI_AL_Max; nROI++)
	{
		CPoint ptCenter = TI_CenterPoint.CenterPoint_Test(pstOpt->stTestRegionOp[nROI].rtRoi, pstOpt->stRegionOp[nROI].nMarkColor, pInputImage);
		stResult.ptCenter[nROI] = ptCenter;
	}

	// 데이터 산출
	for (UINT nROI = ROI_AL_Left; nROI < ROI_AL_Max; nROI++)
	{
		double dTriAngleA = stResult.ptCenter[ROI_AL_Center].x - stResult.ptCenter[nROI].x;
		double dTriAngleB = stResult.ptCenter[ROI_AL_Center].y - stResult.ptCenter[nROI].y;

		double dTriAngleC = pow(dTriAngleA, 2) + pow(dTriAngleB, 2);

		stResult.dValue[nROI - 1] = sqrt(dTriAngleC);
	}

	// Auto Spec Setting
	for (UINT nIdx = 0; nIdx < IDX_AL_Max; nIdx++)
	{
		pstOpt->nMinSpc[nIdx] = stResult.dValue[nIdx] - pstOpt->nSpecDev;
		pstOpt->nMaxSpc[nIdx] = stResult.dValue[nIdx] + pstOpt->nSpecDev;
	}

	// 결과
	stResult.nResult = TER_Pass;

	for (UINT nIdx = 0; nIdx < IDX_AL_Max; nIdx++)
	{
		if (pstOpt->nMinSpc[nIdx] <= stResult.dValue[nIdx] && pstOpt->nMaxSpc[nIdx] >= stResult.dValue[nIdx])
		{
			stResult.nEachResult[nIdx] = TER_Pass;
		}
		else
		{
			stResult.nEachResult[nIdx] = TER_Fail;
			stResult.nResult = TER_Fail;
		}
	}

	for (UINT nROI = ROI_AL_Center; nROI < ROI_AL_Max; nROI++)
	{
		pstOpt->stRegionOp[nROI].rtRoi=pstOpt->stTestRegionOp[nROI].rtRoi;
	}
	cvReleaseImage(&pInputImage);

	

}

void CTestProcess_Image::OnAutoSettingProcessColor(__in IplImage* pFrameImage, __in ST_CenterPoint_Opt* pstCenterOpt, __inout ST_Color_Opt* pstOpt, __out ST_Color_Result &stResult)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
	cvCopy(pFrameImage, pInputImage);
	for (UINT nROI = ROI_CL_R; nROI < ROI_CL_Max; nROI++)
	{

		pstOpt->stTestRegionOp[nROI].rtRoi.left = pstOpt->stRegionOp[nROI].rtRoi.left;
		pstOpt->stTestRegionOp[nROI].rtRoi.right = pstOpt->stRegionOp[nROI].rtRoi.right;
		pstOpt->stTestRegionOp[nROI].rtRoi.top = pstOpt->stRegionOp[nROI].rtRoi.top;
		pstOpt->stTestRegionOp[nROI].rtRoi.bottom = pstOpt->stRegionOp[nROI].rtRoi.bottom;
	}

	OnTestProcessColorROI_Auto(pInputImage, pstOpt);

	BOOL bResult = TRUE;

	// 3. 알고리즘
	for (UINT nROI = 0; nROI < ROI_CL_Max; nROI++)
	{
		CvRect rcSz;
		rcSz.x = pstOpt->stTestRegionOp[nROI].rtRoi.left;
		rcSz.y = pstOpt->stTestRegionOp[nROI].rtRoi.top;
		rcSz.width = pstOpt->stTestRegionOp[nROI].rtRoi.Width();
		rcSz.height = pstOpt->stTestRegionOp[nROI].rtRoi.Height();

		cvSetImageROI(pInputImage, rcSz);

		IplImage* pROI = cvCreateImage(cvGetSize(pInputImage), IPL_DEPTH_8U, pInputImage->nChannels);
		cvCopy(pInputImage, pROI);

		TI_Color.Color_Test(pROI, stResult.iRed[nROI], stResult.iGreen[nROI], stResult.iBlue[nROI]);

		if (pstOpt->stRegionOp[nROI].nMaxSpcR >= stResult.iRed[nROI]
			&& pstOpt->stRegionOp[nROI].nMinSpcR <= stResult.iRed[nROI]
			&& pstOpt->stRegionOp[nROI].nMaxSpcG >= stResult.iGreen[nROI]
			&& pstOpt->stRegionOp[nROI].nMinSpcG <= stResult.iGreen[nROI]
			&& pstOpt->stRegionOp[nROI].nMaxSpcB >= stResult.iBlue[nROI]
			&& pstOpt->stRegionOp[nROI].nMinSpcB <= stResult.iBlue[nROI])
		{
			bResult &= TRUE;
			stResult.nEachResult[nROI] = TER_Pass;
		}
		else
		{
			bResult &= FALSE;
			stResult.nEachResult[nROI] = TER_Fail;
		}

		cvReleaseImage(&pROI);
		cvResetImageROI(pInputImage);
	}

	// Auto Spec Setting
	for (UINT nIdx = 0; nIdx < ROI_CL_Max; nIdx++)
	{
		pstOpt->stRegionOp[nIdx].nMinSpcR = stResult.iRed[nIdx] - pstOpt->nSpecDev;
		pstOpt->stRegionOp[nIdx].nMaxSpcR = stResult.iRed[nIdx] + pstOpt->nSpecDev;

		pstOpt->stRegionOp[nIdx].nMinSpcG = stResult.iGreen[nIdx] - pstOpt->nSpecDev;
		pstOpt->stRegionOp[nIdx].nMaxSpcG = stResult.iGreen[nIdx] + pstOpt->nSpecDev;

		pstOpt->stRegionOp[nIdx].nMinSpcB = stResult.iBlue[nIdx] - pstOpt->nSpecDev;
		pstOpt->stRegionOp[nIdx].nMaxSpcB = stResult.iBlue[nIdx] + pstOpt->nSpecDev;

		if (pstOpt->stRegionOp[nIdx].nMinSpcR < 0)
			pstOpt->stRegionOp[nIdx].nMinSpcR = 0;

		if (pstOpt->stRegionOp[nIdx].nMinSpcG < 0)
			pstOpt->stRegionOp[nIdx].nMinSpcG = 0;

		if (pstOpt->stRegionOp[nIdx].nMinSpcB < 0)
			pstOpt->stRegionOp[nIdx].nMinSpcB = 0;
	}

	// 4. 결과
	if (bResult == TRUE)
	{
		stResult.nResult = TER_Pass;
	}
	else
	{
		stResult.nResult = TER_Fail;
	}


	for (UINT nROI = ROI_CL_R; nROI < ROI_CL_Max; nROI++)
	{

		pstOpt->stRegionOp[nROI].rtRoi = pstOpt->stTestRegionOp[nROI].rtRoi;
	}
	cvReleaseImage(&pInputImage);

	
}

void CTestProcess_Image::OnTestProcessSFRROI_Auto_Center(__in BOOL bMode, __in IplImage* pFrameImage, __inout ST_SFR_Opt* pstOpt)
{
	if (pFrameImage == NULL)
		return;

	if (pstOpt == NULL)
		return;

	// ROI 보정 알고리즘
	IplImage* pInputImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, pFrameImage->nChannels);
	cvCopy(pFrameImage, pInputImage);

	CRect rcSearchROI;
	rcSearchROI.left = pFrameImage->width / 3;
	rcSearchROI.right = pFrameImage->width * 2 / 3;
	rcSearchROI.top = pFrameImage->height / 3;
	rcSearchROI.bottom = pFrameImage->height * 2 / 3;

	CPoint ptCenter = TI_CenterPoint.CenterPoint_Test(rcSearchROI, 0, pInputImage);

	if (ptCenter.x < 0 || ptCenter.y < 0)
	{
		ptCenter.x = pstOpt->iMasterAxisX;
		ptCenter.y = pstOpt->iMasterAxisY;
	}

	if (bMode == TRUE) // 보정
	{
		//// ROI 보정
		int iOffsetX = ptCenter.x - pstOpt->iMasterAxisX;
		int iOffsetY = ptCenter.y - pstOpt->iMasterAxisY;

		pstOpt->iMasterAxisX += iOffsetX;
		pstOpt->iMasterAxisY += iOffsetY;

		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		{
			if (ptCenter.x != -1 && ptCenter.x != -1)
			{
				pstOpt->stRegionOp[nROI].rtRoi.left += iOffsetX;
				pstOpt->stRegionOp[nROI].rtRoi.right += iOffsetX;
				pstOpt->stRegionOp[nROI].rtRoi.top += iOffsetY;
				pstOpt->stRegionOp[nROI].rtRoi.bottom += iOffsetY;
			}
		}
	}
 	else
 	{
 		//// Offset Save
 		pstOpt->iMasterAxisX = ptCenter.x;
 		pstOpt->iMasterAxisY = ptCenter.y;
 	}

	cvReleaseImage(&pInputImage);


}

void CTestProcess_Image::DeleteMemory(ST_LT_TI_EIAJ *pstEIAJ)
{
	TI_EIAJ.DeleteMemory(pstEIAJ);
}

//=============================================================================
// Method		: CamType_CenterPoint
// Access		: public  
// Returns		: void
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __out int & nPosX
// Parameter	: __out int & nPosY
// Qualifier	:
// Last Update	: 2018/4/19 - 16:39
// Desc.		:
//=============================================================================
void CTestProcess_Image::CamType_CenterPoint(__in int iImgW, __in int iImgH, __out int &nPosX, __out int &nPosY)
{
	//카메라 상태에 따라 값 변경.
	int iChangePosX = 0;
	int iChangePosY = 0;

	switch (m_nCameraType)
	{
		// 좌우반전
	case CamState_Mirror:
		iChangePosX = nPosX;
		iChangePosY = nPosY;
		break;

		// 상하반전
	case CamState_Flip:
		iChangePosX = iImgW - nPosX;
		iChangePosY = iImgH - nPosY;
		break;

		// 오리지널
	case CamState_Original:
		iChangePosX = iImgW - nPosX;
		iChangePosY = nPosY;
		break;

		// 로테이트
	case CamStateRotate:
		iChangePosX = nPosX;
		iChangePosY = iImgH - nPosY;
		break;

	default:
		break;
	}

	nPosX = iChangePosX;
	nPosY = iChangePosY;
}


BOOL CTestProcess_Image::Get_MeasurmentData(__in BOOL bMinUse, __in BOOL bMaxUse, __in int iMinSpec, __in int iMaxSpec, __in int iValue)
{
	BOOL bMinResult = TRUE;
	BOOL bMaxResult = TRUE;

	if (TRUE == bMinUse)
	{
		bMinResult = (iMinSpec <= iValue) ? TRUE : FALSE;
	}

	if (TRUE == bMaxUse)
	{
		bMaxResult = (iMaxSpec >= iValue) ? TRUE : FALSE;
	}

	return bMinResult & bMaxResult;
}
