﻿//*****************************************************************************
// Filename	: 	Wnd_ModelInfo.h
// Created	:	2016/5/13 - 11:39
// Modified	:	2016/5/13 - 11:39
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_ModelInfo_h__
#define Wnd_ModelInfo_h__

#pragma once

#include "VGGroupWnd.h"
#include "Def_DataStruct.h"
#include "VGStatic.h"
#include "Grid_ModelInfo.h"

//-----------------------------------------------------------------------------
// CWnd_ModelInfo
//-----------------------------------------------------------------------------
class CWnd_ModelInfo : public CWnd
{
	DECLARE_DYNAMIC(CWnd_ModelInfo)

public:
	CWnd_ModelInfo();
	virtual ~CWnd_ModelInfo();

protected:
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	virtual void	OnRedrawControl			();

	CGrid_ModelInfo		m_grid_ModelInfo;
	ST_ModelInfo*		m_stModelInfo;
	ST_PogoInfo*		m_stPogoInfo;

	DECLARE_MESSAGE_MAP()

public:

	void	SetModelInfo(__in ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_stModelInfo = pstModelInfo;

		m_grid_ModelInfo.SetModelInfo(m_stModelInfo);
	}

	void	UpdatePogoCnt(__in ST_PogoInfo* pstPogoInfo)
	{
		if (pstPogoInfo == NULL)
			return;

		m_stPogoInfo = pstPogoInfo;

		m_grid_ModelInfo.UpdatePogoCnt(m_stPogoInfo);
	};

};

#endif // Wnd_ModelInfo_h__


