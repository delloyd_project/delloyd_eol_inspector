﻿#include "stdafx.h"
#include "Grid_MESInfo.h"

static LPCTSTR lpszRowHeader[] =
{
	_T("LOT_NO"),
	_T("LOT_CARD_NO"),
	_T("Channel Num"),
	_T("Eqp Code"),
	_T("지시 번호"),
	_T("품번"),
	_T("Operator"),
//	_T("처리 일자"),
//	_T("처리 일시"),
	
	NULL
};

typedef enum
{
	IDX_Y_Header = 0,
	IDX_Y_Lot_No = IDX_Y_Header,
	IDX_Y_Lot_Card_No,
	IDX_Y_Channel,
	IDX_Y_EqpCode,
	IDX_Y_ItemNumber,
	IDX_Y_Barcode,
	IDX_Y_Operator,
//	IDX_Y_TestDate,
//	IDX_Y_TestTime,

	IDX_ROW_MAX,

}enumRowHeaderModelInfo;

static LPCTSTR lpszColHeader[] =
{
	_T(""),
	_T(""),
	NULL
};

typedef enum
{
	IDX_X_Header = 0,
	IDX_X_Item = IDX_X_Header,
	IDX_X_Data,
	IDX_COL_MAX,
}enumColHeaderModelInfo;


#define		RGB_BLACK		RGB(0x00, 0x00, 0x00)
#define		RGB_WHITE		RGB(0xFF, 0xFF, 0xFF)
#define		RGB_YELLOW		RGB(0xFF, 0xFF, 0x00)
#define		RGB_ROW_HEADER	RGB(86, 86,  86)
#define		RGB_BK_ID		RGB(135, 169, 213)
#define		RGB_COL_HEADER	RGB(0xFF, 200, 100)
#define		RGB_TITLE		RGB(150, 200, 0xFF)

#define		RGB_BIT_SET		RGB(123, 255,  75) //RGB(112, 173, 71)
#define		RGB_BIT_CLEAR	RGB(100,  10,  10) //RGB(237, 125, 49)
//#define		RGB_SELECT		RGB(0xFF, 200, 100) //RGB(150, 200, 0xFF)

//=============================================================================
//
//=============================================================================
CGrid_MESInfo::CGrid_MESInfo()
{
	SetRowColCount(IDX_ROW_MAX, IDX_COL_MAX);

	VERIFY(m_font_Header.CreateFont(
		14,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_NORMAL,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		FIXED_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Data.CreateFont(
		14,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_NORMAL,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		FIXED_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

//=============================================================================
//
//=============================================================================
CGrid_MESInfo::~CGrid_MESInfo()
{
	m_font_Header.DeleteObject();
	m_font_Data.DeleteObject();
}

//=============================================================================
// Method		: OnSetup
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/14 - 14:30
// Desc.		:
//=============================================================================
void CGrid_MESInfo::OnSetup()
{
	__super::OnSetup();
}

//=============================================================================
// Method		: DrawGridOutline
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_MESInfo::DrawGridOutline()
{
	CGrid_Base::DrawGridOutline();

	SetDefFont(&m_font_Data);

	// 헤더를 설정한다.
	InitHeader();
}

//=============================================================================
// Method		: CGrid_MESInfo::CalGridOutline
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_MESInfo::CalGridOutline()
{
	//CGrid_Base::CalGridOutline();

	// 윈도우 면적에 따라서 열의 너비를 결정
	CRect rect;
	GetWindowRect(&rect);
	int nWidth = rect.Width();
	int nHeight = rect.Height();

	if ((nWidth <= 0) || (nHeight <= 0))
		return;

	// 기본 열 추가 ---------------------------------------
	// 열 너비용 비율을 만듦 ()
	int		nColWidthRate[IDX_COL_MAX] = { 40, 60 };

	int		iMisc = 0;
	int		iUnitWidth = 0;
	// 기본 열 추가 ---------------------------------------
	for (UINT iCol = 0; iCol < m_nMaxCols; iCol++)
	{
		iUnitWidth = (nWidth * nColWidthRate[iCol]) / 100;
		iMisc += iUnitWidth;
		SetColWidth(iCol, iUnitWidth);
	}
	// Width 계산하고 남거나 모자르는 공간 계산하여 Name 영역에 추가	
	SetColWidth(IDX_X_Header, ((nWidth * nColWidthRate[IDX_X_Header]) / 100) + nWidth - iMisc);
	// 	int iUnitWidth = nWidth / m_nMaxCols;
	// 	int iMisc = nWidth - (iUnitWidth * (m_nMaxCols - 1));
	// 	SetColWidth(IDX_X_Header, iMisc);
	// 	for (UINT iCol = IDX_X_Header + 1; iCol < m_nMaxCols; iCol++)
	// 	{
	// 		SetColWidth(iCol, iUnitWidth);
	// 	}

	// 패턴 행 헤더 추가 ----------------------------------		
	UINT nUnitHeight = nHeight / m_nMaxRows;
	UINT nRemindHeight = nHeight - (nUnitHeight * m_nMaxRows);

	// Height 계산하고 남거나 모자르는 공간 계산하여 헤더 Height 추가 처리
	for (UINT iRow = 0; iRow < nRemindHeight; iRow++)
	{
		SetRowHeight(iRow, nUnitHeight + 1);
	}

	for (UINT iRow = nRemindHeight; iRow < m_nMaxRows; iRow++)
	{
		SetRowHeight(iRow, nUnitHeight);
	}
}

//=============================================================================
// Method		: CGrid_MESInfo::InitRowHeader
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_MESInfo::InitHeader()
{
	for (UINT iRow = 0; iRow < m_nMaxRows; iRow++)
	{
		QuickSetFont(IDX_X_Item, iRow, &m_font_Header);
		QuickSetBackColor_COLORREF(IDX_X_Item, iRow, RGB_ROW_HEADER);
		QuickSetTextColor(IDX_X_Item, iRow, RGB_WHITE);
		QuickSetText(IDX_X_Item, iRow, lpszRowHeader[iRow]);

		QuickSetFont(IDX_X_Data, iRow, &m_font_Data);
		QuickSetBackColor_COLORREF(IDX_X_Data, iRow, RGB_WHITE);
		QuickSetTextColor(IDX_X_Data, iRow, RGB_BLACK);
	}

	//QuickSetText(IDX_X_Data, IDX_Y_LotCount, _T("0"));
}

//=============================================================================
// Method		: OnHint
// Access		: virtual protected  
// Returns		: int
// Parameter	: int col
// Parameter	: long row
// Parameter	: int section
// Parameter	: CString * string
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
int CGrid_MESInfo::OnHint(int col, long row, int section, CString *string)
{
	return FALSE;
}

//=============================================================================
// Method		: CGrid_MESInfo::OnGetCell
// Access		: virtual protected 
// Returns		: void
// Parameter	: int col
// Parameter	: long row
// Parameter	: CUGCell * cell
// Qualifier	:
// Last Update	: 2015/12/10 - 23:28
// Desc.		:
//=============================================================================
void CGrid_MESInfo::OnGetCell(int col, long row, CUGCell *cell)
{
	CGrid_Base::OnGetCell(col, row, cell);
}

//=============================================================================
// Method		: OnDrawFocusRect
// Access		: virtual protected  
// Returns		: void
// Parameter	: CDC * dc
// Parameter	: RECT * rect
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_MESInfo::OnDrawFocusRect(CDC *dc, RECT *rect)
{
}

//=============================================================================
// Method		: SetLotInifo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_LOTInfo* pstLotInfo
// Qualifier	:
// Last Update	: 2016/3/31 - 18:42
// Desc.		:
//=============================================================================
void CGrid_MESInfo::SetMESInifo(__in const ST_MES_TotalResult* pstMESInfo)
{
	CString strText;

	strText = pstMESInfo->szLotNo;
	QuickSetText(IDX_X_Data, IDX_Y_Lot_No, strText);

	strText = pstMESInfo->szLotCardNo;
	QuickSetText(IDX_X_Data, IDX_Y_Lot_Card_No, strText);

	strText = pstMESInfo->szChannel;
	QuickSetText(IDX_X_Data, IDX_Y_Channel, strText);

	strText = pstMESInfo->szEqpCode;
	QuickSetText(IDX_X_Data, IDX_Y_EqpCode, strText);

	strText = pstMESInfo->szItemNumber;
	QuickSetText(IDX_X_Data, IDX_Y_ItemNumber, strText);

	strText = pstMESInfo->szBarcode;
	QuickSetText(IDX_X_Data, IDX_Y_Barcode, strText);

	strText = pstMESInfo->szOperator;
	QuickSetText(IDX_X_Data, IDX_Y_Operator, strText);

// 	strText = pstMESInfo->szTestDate;
// 	QuickSetText(IDX_X_Data, IDX_Y_TestDate, strText);
// 
// 	strText = pstMESInfo->szTestTime;
// 	QuickSetText(IDX_X_Data, IDX_Y_TestTime, strText);

	RedrawCol(IDX_X_Data);
}

