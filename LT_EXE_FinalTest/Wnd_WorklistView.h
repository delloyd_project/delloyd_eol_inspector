﻿//*****************************************************************************
// Filename	: Wnd_WorklistView.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistView_h__
#define Wnd_WorklistView_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_Total.h"

#include "Wnd_WorklistAngle.h"
#include "Wnd_WorklistCenterPt.h"
#include "Wnd_WorklistColor.h"
#include "Wnd_WorklistCurrent.h"
#include "Wnd_WorklistParticle.h"
#include "Wnd_WorklistRotate.h"
#include "Wnd_WorklistSFR.h"
#include "Wnd_WorklistFFT.h"
#include "Wnd_WorklistBrightness.h"
#include "Wnd_WorklistIRFilter.h"

#include "Wnd_WorklistEIAJ.h"
#include "Wnd_WorklistReverse.h"
#include "Wnd_WorklistParticleManual.h"
#include "Wnd_WorklistPatternNoise.h"
#include "Wnd_WorklistIRFilterManual.h"
#include "Wnd_WorklistLED.h"
#include "Wnd_WorklistReset.h"
#include "Wnd_WorklistVideoSignal.h"
#include "Wnd_WorklistOperMode.h"
#include "Wnd_WorklistDefectPixel.h"

//=============================================================================
// Wnd_WorklistView
//=============================================================================
class CWnd_WorklistView : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistView)

public:
	CWnd_WorklistView();
	virtual ~CWnd_WorklistView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

 	CList_Work_Total			m_list_Total;

	CWnd_WorklistAngle			m_list_Angle;
	CWnd_WorklistCenterPt		m_list_CenterPoint;
	CWnd_WorklistColor			m_list_Color;
	CWnd_WorklistCurrent		m_list_Current;
	CWnd_WorklistParticle		m_list_BlackSpot;
	CWnd_WorklistRotate			m_list_Rotate;
	CWnd_WorklistDefectPixel	m_list_DefectPixel;
	//CWnd_WorklistSFR			m_list_SFR;
	//CWnd_WorklistFFT			m_list_FFT;
	CWnd_WorklistBrightness		m_list_Brightness;
	CWnd_WorklistIRFilter		m_list_IRFilter;

	CWnd_WorklistEIAJ			m_list_EIAJ;
	CWnd_WorklistReverse		m_list_Reverse;
	CWnd_WorklistParticleManual	m_list_ParticleManual;
	//CWnd_WorklistPatternNoise	m_list_PatternNoise;
	//CWnd_WorklistIRFilterManual	m_list_IRFilterManual;
	//CWnd_WorklistLED			m_list_LED;
	//CWnd_WorklistReset			m_list_Reset;
	//CWnd_WorklistVideoSignal	m_list_VideoSignal;
	//CWnd_WorklistOperMode		m_list_OperMode;

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistView_h__


