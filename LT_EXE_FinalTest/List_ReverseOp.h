﻿#ifndef List_ReverseOp_h__
#define List_ReverseOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_ReverseOp
{
	ReverseOp_Object = 0,
	ReverseOp_PosX,
	ReverseOp_PosY,
	ReverseOp_Width,
	ReverseOp_Height,
	ReverseOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_ReverseOp[] =
{
	_T(""),
	_T("Pos X"),
	_T("Pos Y"),
	_T("Width"),
	_T("Height"),
	NULL
};

typedef enum enListItemNum_ReverseOp
{
	ReverseOp_ItemNum = ROI_Rv_Max,
};

static LPCTSTR	g_lpszItem_ReverseOp[] =
{
	NULL
};

const int	iListAglin_ReverseOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_ReverseOp[] =
{
	110,
	100,
	100,
	100,
	100,
	100,
	100,
};
// List_ReverseInfo

class CList_ReverseOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_ReverseOp)

public:
	CList_ReverseOp();
	virtual ~CList_ReverseOp();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Reverse(ST_LT_TI_Reverse* pstReverse)
	{
		if (pstReverse == NULL)
			return;

		m_pstReverse = pstReverse;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_Reverse*  m_pstReverse;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;
	CComboBox	m_cb_CamType;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel	(UINT nFlags, short zDelta, CPoint pt);
		
	afx_msg void	OnEnKillFocusEdit		();
	afx_msg void	OnEnSelectFocusCamTypeCombo	();
};

#endif // List_ReverseInfo_h__
