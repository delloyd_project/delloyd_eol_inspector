﻿// List_CurrentResult.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_CurrentResult.h"

// CList_CurrentResult



IMPLEMENT_DYNAMIC(CList_CurrentResult, CListCtrl)

CList_CurrentResult::CList_CurrentResult()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
	
	m_nWidth	= 640;
	m_nHeight	= 480;

	m_pstCurrent	= NULL;
}

CList_CurrentResult::~CList_CurrentResult()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_CurrentResult, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_CurrentResult::OnNMClick)
	ON_WM_MOUSEWHEEL()
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &CList_CurrentResult::OnNMCustomdraw)
END_MESSAGE_MAP()

// CList_CurrentResult 메시지 처리기입니다.
int CList_CurrentResult::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_CurrentResult::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol < CurrentResult_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, rectClient.Width() / CurrentResult_MaxCol);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_CurrentResult::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_CurrentResult::InitHeader()
{
	for (int nCol = 0; nCol < CurrentResult_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_CurrentResult[nCol], iListAglin_CurrentResult[nCol], iHeaderWidth_CurrentResult[nCol]);
	}

	for (int nCol = 0; nCol < CurrentResult_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_CurrentResult[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_CurrentResult::InsertFullData()
{
	if (m_pstCurrent == NULL)
		return;

 	DeleteAllItems();
 

	for (UINT nIdx = 0; nIdx < CurrentResult_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_CurrentResult::SetRectRow(UINT nRow)
{
	if (m_pstCurrent == NULL)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_szCurrent_Channel[nRow]);
	SetItemText(nRow, CurrentResult_Object, strValue);

	strValue.Format(_T("%.1f"), m_pstCurrent->stCurrentResult.iValue[nRow]);
	SetItemText(nRow, CurrentResult_Value, strValue);

	strValue.Format(_T("%.1f"),  m_pstCurrent->stCurrentOpt.nSpecMin[nRow]);
	SetItemText(nRow, CurrentResult_MinSpec, strValue);

	strValue.Format(_T("%.1f"), m_pstCurrent->stCurrentOpt.nSpecMax[nRow]);
	SetItemText(nRow, CurrentResult_MaxSpec, strValue);

	strValue.Format(_T("%s"), g_lpszItem_CurrentResult[m_pstCurrent->stCurrentResult.nEachResult[nRow]]);
	SetItemText(nRow, CurrentResult_Result, strValue);

}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_CurrentResult::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	*pResult = 0;
}


void CList_CurrentResult::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVCUSTOMDRAW  lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	int nRow = 0, nSub = 0;
	switch (lplvcd->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;          // 아이템외에 일반적으로 처리하는 부분
		lplvcd->clrTextBk = RGB(0, 0, 255);
		break;
	case CDDS_ITEMPREPAINT:                          // 행 아이템에 대한 처리를 할 경우
		*pResult = CDRF_NOTIFYSUBITEMDRAW;
		break;
	case CDDS_ITEMPREPAINT | CDDS_SUBITEM:  // 행과 열 아이템에 대한 처리를 할 경우
		nRow = (int)lplvcd->nmcd.dwItemSpec;         // 행 인덱스를 가져옴
		nSub = (int)lplvcd->iSubItem;                       // 열 인덱스를 가져옴

		if (nSub >= CurrentResult_Value)
		{
			lplvcd->clrTextBk = RGB(250, 236, 197);           // 해당 행, 열 아이템의 배경색을 지정한다.
			lplvcd->clrText = RGB(0, 0, 0);                      // 해당 행, 열 아이템의 글자색을 지정한다.
		}

		if (nSub > CurrentResult_Value)
		{
			lplvcd->clrTextBk = RGB(255, 255, 255);           // 해당 행, 열 아이템의 배경색을 지정한다.
			lplvcd->clrText = RGB(0, 0, 0);                      // 해당 행, 열 아이템의 글자색을 지정한다.
		}

		if (nSub == CurrentResult_Result)
		{
			if (m_pstCurrent->stCurrentResult.nEachResult[nRow] == TRUE)
			{
				lplvcd->clrTextBk = RGB(0, 0, 255);           // 해당 행, 열 아이템의 배경색을 지정한다.
				lplvcd->clrText = RGB(255, 255, 255);                      // 해당 행, 열 아이템의 글자색을 지정한다.
			}
			else
			{
				lplvcd->clrTextBk = RGB(255, 0, 0);           // 해당 행, 열 아이템의 배경색을 지정한다.
				lplvcd->clrText = RGB(255, 255, 255);                      // 해당 행, 열 아이템의 글자색을 지정한다.
			}
			
		}

		break;
	default:
		*pResult = CDRF_DODEFAULT;

		break;
	}
}
