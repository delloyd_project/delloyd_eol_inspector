﻿//*****************************************************************************
// Filename	: 	Pane_CommStatus.cpp
// Created	:	2014/7/5 - 10:24
// Modified	:	2015/12/12 - 0:07
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "resource.h"
#include "Pane_CommStatus.h"
#include "CommonFunction.h"
#include "BSocketClient.h"
#include "BSocketServer.h"
#include "Dlg_AcessMode.h"
#include "Dlg_OperMode.h"

#define		IDC_BN_TEST_S		1001
#define		IDC_BN_TEST_E		1004

#define		IDC_PRG_TESTING		1020

#define		IDC_BN_DEVICE_S		1100
#define		IDC_BN_DEVICE_E		IDC_BN_DEVICE_S + DEV_BN_MaxCount

static LPCTSTR m_szDeviceName[] =
{
	_T(""),						//DevSI_PermissionMode
	_T(""),						//DevSI_PermissionMode
	_T("MES"),					//MES
	_T("Motion Board"),			//DevSI_PCIMotorBrd
	_T("IO Board"),				//DevSI_PCIIoBrd
	_T("Analog Board"),			//DevSI_GrabberBrd_ComArt
	//_T("Digital Board"),			//DevSI_GrabberBrd_DAQ
	_T("Barcode Reader"),		//DevSI_BCR
	_T("Label Printer"),				//DevSI_Printer
	_T("Interface Board"),		//DevSI_CanMain
	_T("Interface Board 2"),		//DevSI_CanMain
	_T("Interface Board 3"),		//DevSI_CanMain
	_T("Interface Board 4"),		//DevSI_CanMain
	_T("Light Board "),			//DevSI_Light
	_T("Light Board 2"),		//DevSI_Light
	_T("Light Board 3"),		//DevSI_Light
	_T("Light Board 4"),		//DevSI_Light
	_T("Indicator X"),			//DevSI_IndicatorX
	_T("Indicator Y"),			//DevSI_IndicatorY
	NULL
};

static LPCTSTR m_szBnDeviceName[] =
{
	_T("Access Mode"),			//DEV_BN_PermissionChange
	_T("Operate Mode	"),		// DEV_BN_OperateModeChange
	_T("Keyboard"),
	_T("Manual Barcode"),
	_T("Equipment Init"),		// DEV_BN_EquipmentInit
	NULL
};

static LPCTSTR m_szBnTestName[] =
{
	_T("Test 01"),
	_T("Test 02"),
	_T("Test 03"),
	_T("Test 04"),
	NULL
};

//=============================================================================
// CPane_CommStatus
//=============================================================================
IMPLEMENT_DYNAMIC(CPane_CommStatus, CMFCTasksPane)

//=============================================================================
//
//=============================================================================
CPane_CommStatus::CPane_CommStatus()
{
	VERIFY(m_Font.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		FIXED_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

//=============================================================================
//
//=============================================================================
CPane_CommStatus::~CPane_CommStatus()
{
	TRACE(_T("<<< Start ~CPane_CommStatus >>> \n"));

	m_Font.DeleteObject();

	TRACE(_T("<<< End ~CPane_CommStatus >>> \n"));
}


BEGIN_MESSAGE_MAP(CPane_CommStatus, CMFCTasksPane)
	ON_WM_CREATE()	
	ON_COMMAND_RANGE			(IDC_BN_TEST_S, IDC_BN_TEST_E, OnBnClickedTest)
	ON_UPDATE_COMMAND_UI_RANGE	(IDC_BN_TEST_S, IDC_BN_TEST_E, OnUpdateCmdUI_Test)
	ON_COMMAND_RANGE			(IDC_BN_DEVICE_S, IDC_BN_DEVICE_E, OnBnClicked_Dev)
	ON_UPDATE_COMMAND_UI_RANGE	(IDC_BN_DEVICE_S, IDC_BN_DEVICE_E, OnUpdateCmdUI_Dev)
END_MESSAGE_MAP()

//=============================================================================
// CPane_CommStatus 메시지 처리기입니다.
//=============================================================================

//=============================================================================
// Method		: CPane_CommStatus::OnCreate
// Access		: public 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2015/12/12 - 0:07
// Desc.		:
//=============================================================================
int CPane_CommStatus::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMFCTasksPane::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < DevSI_MaxCount; nIdx++)
	{
		m_st_Device[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
		m_st_Device[nIdx].SetFont_Gdip(L"Arial", 8.0F);
		m_st_Device[nIdx].Create(m_szDeviceName[nIdx], dwStyle, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < DEV_BN_MaxCount; nIdx++)
	{
		m_bn_Device[nIdx].Create(m_szBnDeviceName[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_DEVICE_S + nIdx);
		m_bn_Device[nIdx].SetFont(&m_Font);
	}
	
	for (UINT nIdx = 0; nIdx < DEV_ST_IndicatorMax; nIdx++)
	{
		m_st_Indecator[nIdx].Create(_T("0.00"), dwStyle, rectDummy, this, IDC_STATIC);
		m_st_Indecator[nIdx].SetFont_Gdip(L"Arial", 20.0F);
	}


#ifdef USE_LOG_WND
	m_st_WarningEvent.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_st_WarningEvent.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_WarningEvent.SetFont_Gdip(L"Arial", 8.0F);

	m_st_WarningEvent.Create (_T("Connection"), WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, CRect(0,0,0,0), this, IDC_STATIC);

	if (!m_ed_WarningLog.Create(WS_CHILD | WS_VISIBLE, CRect(0,0,0,0), this, 121) )
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}
	m_ed_WarningLog.SetFont(&m_Font);
	m_ed_WarningLog.HideCaret();
	m_ed_WarningLog.SetBackColor_COLORREF(RGB(0xFF, 0xFF, 0xFF));
#endif
	
	for (int iCnt = 0; iCnt < 4; iCnt++)
	{		
		m_bn_Test[iCnt].Create(m_szBnTestName[iCnt], WS_VISIBLE | WS_CHILD, CRect (0, 0, 0, 0), this, IDC_BN_TEST_S + iCnt);
	}

	//HINSTANCE hInstance = AfxFindResourceHandle (MAKEINTRESOURCE (nID), RT_GROUP_ICON);
	//HICON hIcon = (HICON)::LoadImage (hInstance, MAKEINTRESOURCE (nID), IMAGE_ICON, 0, 0, LR_DEFAULTCOLOR);
	
	// Pane 옵션 설정
	EnableGroupCollapse (FALSE);
	EnableWrapLabels (TRUE);
	EnableOffsetCustomControls (FALSE);

	// 그룹 추가
	AddPermissionMode();

	//Add_OperateMode();

	// 바코드
	AddBarcode();

	// 프린터
	//AddLabelPrinter();

	Add_EquipmentCtrl();

#ifdef USE_EVMS_MODE
	AddTCPIP();
#endif

	AddGrabber();
	AddPCISlot();
	AddSerialComm();

	if (g_InspectorTable[SET_INSPECTOR].nIndigatorCnt > 0)
		AddDisplay();

#ifdef _DEBUG
	AddTestCtrl();
#ifdef USE_LOG_WND
	AddWarningStatus();
#endif
#endif

	AddUtilities();
	AddSystemInfo ();	

	return 0;
}

//=============================================================================
// Method		: CPane_CommStatus::CalcFixedLayout
// Access		: public 
// Returns		: CSize
// Parameter	: BOOL
// Parameter	: BOOL
// Qualifier	:
// Last Update	: 2010/11/25 - 15:49
// Desc.		: Pane의 가로 크기 변경
//=============================================================================
CSize CPane_CommStatus::CalcFixedLayout (BOOL, BOOL)
{
	ASSERT_VALID(this);

	return CSize (140, 32767);	
}

//=============================================================================
// Method		: CPane_CommStatus::OnUpdateCmdUI_Test
// Access		: virtual protected 
// Returns		: void
// Parameter	: CCmdUI * pCmdUI
// Qualifier	:
// Last Update	: 2013/6/11 - 19:53
// Desc.		:
//=============================================================================
void CPane_CommStatus::OnUpdateCmdUI_Test( CCmdUI* pCmdUI )
{
	if ((IDC_BN_TEST_S <= pCmdUI->m_nID) && (pCmdUI->m_nID <= IDC_BN_TEST_E))
	{
		UINT nBnIndex = pCmdUI->m_nID - IDC_BN_TEST_S;

		m_bn_Test[nBnIndex].EnableWindow(TRUE);
	}
}

//=============================================================================
// Method		: CPane_CommStatus::OnBnClickedTest
// Access		: protected 
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2013/6/11 - 19:42
// Desc.		:
//=============================================================================
void CPane_CommStatus::OnBnClickedTest(UINT nID)
{
	if ((IDC_BN_TEST_S <= nID) && (nID <= IDC_BN_TEST_E))
	{
		UINT nTestNo = nID - IDC_BN_TEST_S;

		(AfxGetApp()->GetMainWnd())->SendMessage(WM_TEST_FUNCTION, (WPARAM)nTestNo, 0);
	}
}

//=============================================================================
// Method		: OnUpdateCmdUI_Dev
// Access		: protected  
// Returns		: void
// Parameter	: CCmdUI * pCmdUI
// Qualifier	:
// Last Update	: 2016/9/21 - 18:10
// Desc.		:
//=============================================================================
void CPane_CommStatus::OnUpdateCmdUI_Dev(CCmdUI* pCmdUI)
{
	if ((IDC_BN_DEVICE_S <= pCmdUI->m_nID) && (pCmdUI->m_nID <= IDC_BN_DEVICE_E))
	{
		UINT nBnIndex = pCmdUI->m_nID - IDC_BN_DEVICE_S;

		m_bn_Device[nBnIndex].EnableWindow(TRUE);
	}
}

//=============================================================================
// Method		: OnBnClicked_Dev
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2016/9/21 - 18:10
// Desc.		:
//=============================================================================
void CPane_CommStatus::OnBnClicked_Dev(UINT nID)
{
	if ((IDC_BN_DEVICE_S <= nID) && (nID <= IDC_BN_DEVICE_E))
	{
		UINT nTestNo = nID - IDC_BN_DEVICE_S;

		switch (nTestNo)
		{
		case DEV_BN_PermissionChange:
			(AfxGetApp()->GetMainWnd())->SendMessage(WM_PERMISSION_MODE, 0, 1);
			break;

		case DEV_BN_OperateModeChange:
		{
			//(AfxGetApp()->GetMainWnd())->SendMessage(WM_PERMISSION_MODE, 0, 0);
			CDlg_OperMode dlgMode;
			dlgMode.SetSystemType();
			if (IDCANCEL != dlgMode.DoModal())
			{
				//SetStatus_OperateMode(dlgMode.Get_OperateMode());
			}
			break;
		}

		case DEV_BN_Keyboard:
			RunTouchKeyboard();
			break;
		case DEV_BN_Barcode:
		{
			//(AfxGetApp()->GetMainWnd())->SendMessage(WM_MANUAL_BARCODE, 0, 0);
			 GetParent()->SendMessage(WM_MANUAL_BARCODE, 0, 0);
		}
			break;

		case DEV_BN_EquipmentInit:
		{
			//(AfxGetApp()->GetMainWnd())->SendMessage(WM_EQP_INIT, 0, 0);
			GetParent()->SendMessage(WM_EQP_INIT, 0, 0);
		}
		break;
		}
	}
}

//=============================================================================
// Method		: CPane_CommStatus::AddSystemInfo
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/11/25 - 15:49
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddSystemInfo()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle (), MAKEINTRESOURCE (IDI_ICON_Luritech), IMAGE_ICON, 16, 16, LR_SHARED); 

	int nGroupSysInfo = AddGroup (_T("Program Info"), TRUE, FALSE, hIcon);
	AddLabel (nGroupSysInfo, GetVersionInfo(_T("CompanyName")), -1, TRUE);
	
	CString strText;
	strText.Format(_T("%s (%s)"), GetVersionInfo(_T("ProductVersion")),  GetVersionInfo(_T("FileVersion")));
	AddLabel (nGroupSysInfo, _T("Program Version "));
	AddLabel (nGroupSysInfo, strText, -1, TRUE);	

	COleDateTime now = COleDateTime::GetCurrentTime ();	
	CString strDate = now.Format (_T("Exe :%Y/%m/%d %H:%M:%S"));
	AddLabel (nGroupSysInfo, _T("Execution Time"));
	AddLabel (nGroupSysInfo, strDate, -1, TRUE);	
}

//=============================================================================
// Method		: AddPermissionMode
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/9/21 - 17:12
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddPermissionMode()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupCommStatus = AddGroup(_T("Permission"), FALSE, TRUE, hIcon);

	AddWindow(nGroupCommStatus, m_st_Device[DevSI_PermissionMode].GetSafeHwnd(), 20);
	AddWindow(nGroupCommStatus, m_bn_Device[DEV_BN_PermissionChange].GetSafeHwnd(), 25);

	SetStatus_PermissionMode(enPermissionMode::Permission_Operator);
}

//************************************
// Method:    AddBarcode
// FullName:  CPane_CommStatus::AddBarcode
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void CPane_CommStatus::AddBarcode()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupCommStatus = AddGroup(_T("Barcode"), FALSE, TRUE, hIcon);

	// 	AddWindow(nGroupCommStatus, m_st_Device[DevSI_GrabberBrd_ComArt].GetSafeHwnd(), 20);
	AddWindow(nGroupCommStatus, m_st_Device[DevSI_BCR].GetSafeHwnd(), 20);
}

void CPane_CommStatus::AddLabelPrinter()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupCommStatus = AddGroup(_T("Printer"), FALSE, TRUE, hIcon);

	// 	AddWindow(nGroupCommStatus, m_st_Device[DevSI_GrabberBrd_ComArt].GetSafeHwnd(), 20);
	AddWindow(nGroupCommStatus, m_st_Device[DevSI_LabelPrinter].GetSafeHwnd(), 20);
}

//=============================================================================
// Method		: AddPCISlot
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/25 - 10:36
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddPCISlot()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupCommStatus = 0;

	if (g_InspectorTable[SET_INSPECTOR].bPCIMotion == TRUE || g_InspectorTable[SET_INSPECTOR].bPCIIOControl == TRUE)
		nGroupCommStatus = AddGroup(_T("PCI"), FALSE, TRUE, hIcon);

	if (g_InspectorTable[SET_INSPECTOR].bPCIMotion == TRUE)
		AddWindow(nGroupCommStatus, m_st_Device[DevSI_PCIMotorBrd].GetSafeHwnd(), 20);

	if (g_InspectorTable[SET_INSPECTOR].bPCIIOControl == TRUE)
		AddWindow(nGroupCommStatus, m_st_Device[DevSI_PCIIoBrd].GetSafeHwnd(), 20);

}

//=============================================================================
// Method		: AddSerialComm
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/17 - 13:33
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddSerialComm()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupCommStatus = AddGroup(_T("Devices"), FALSE, TRUE, hIcon);

	for (UINT nIdx = 0; nIdx < g_InspectorTable[SET_INSPECTOR].nPCBCamCnt; nIdx++)
		AddWindow(nGroupCommStatus, m_st_Device[DevSI_CanMain + nIdx].GetSafeHwnd(), 20);

	for (UINT nIdx = 0; nIdx < g_InspectorTable[SET_INSPECTOR].nLightBrdCount; nIdx++)
		AddWindow(nGroupCommStatus, m_st_Device[DevSI_Light + nIdx].GetSafeHwnd(), 20);
}

//=============================================================================
// Method		: AddTCPIP
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/13 - 17:05
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddTCPIP()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupCommStatus = AddGroup(_T("TCP/IP"), FALSE, TRUE, hIcon);

	AddWindow(nGroupCommStatus, m_st_Device[Dev_MES].GetSafeHwnd(), 20);
}

//=============================================================================
// Method		: AddGrabber
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/28 - 10:19
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddGrabber()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupCommStatus = AddGroup(_T("Grabber"), FALSE, TRUE, hIcon);

 	AddWindow(nGroupCommStatus, m_st_Device[DevSI_GrabberBrd_ComArt].GetSafeHwnd(), 20);
}

//=============================================================================
// Method		: AddDisplay
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/15 - 14:47
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddDisplay()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupCommStatus = AddGroup(_T("Display"), FALSE, TRUE, hIcon);

	AddLabel(nGroupCommStatus, _T("Indicator X"), -1, TRUE);
 	AddWindow(nGroupCommStatus, m_st_Indecator[DEV_ST_IndicatorX].GetSafeHwnd(), 40);

	AddLabel(nGroupCommStatus, _T("Indicator Y"), -1, TRUE);
	AddWindow(nGroupCommStatus, m_st_Indecator[DEV_ST_IndicatorY].GetSafeHwnd(), 40);
}

//=============================================================================
// Method		: AddUtilities
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/27 - 10:08
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddUtilities()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);
	int nGroupCommStatus = AddGroup(_T("Utilities"), TRUE, FALSE, hIcon);
	AddWindow(nGroupCommStatus, m_bn_Device[DEV_BN_Barcode].GetSafeHwnd(), 30);
	AddWindow(nGroupCommStatus, m_bn_Device[DEV_BN_Keyboard].GetSafeHwnd(), 30);
}

//=============================================================================
// Method		: CPane_CommStatus::AddMarkingStatus
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2013/2/4 - 11:14
// Desc.		:
//=============================================================================
#ifdef USE_LOG_WND
void CPane_CommStatus::AddWarningStatus()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle (), MAKEINTRESOURCE (IDI_ICON_CONNECT), IMAGE_ICON, 16, 16, LR_SHARED);

	int nLogMessage = AddGroup (_T("Log 메세지"), FALSE, TRUE, hIcon);
	
	//AddWindow (nGroupPLCStatus, m_st_WarningEvent.GetSafeHwnd (), 20);
	AddWindow (nLogMessage, m_ed_WarningLog.GetSafeHwnd (), 100/*250*/);
}
#endif

//=============================================================================
// Method		: CPane_CommStatus::AddTestCtrl
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2013/6/11 - 19:43
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddTestCtrl()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle (), MAKEINTRESOURCE (IDI_ICON_CONNECT), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupTestCtrl = AddGroup (_T("TEST"), FALSE, TRUE, hIcon);

	for (UINT iCnt = 0; iCnt < 4; iCnt++)
		AddWindow (nGroupTestCtrl, m_bn_Test[iCnt].GetSafeHwnd (), 25);
}

void CPane_CommStatus::Add_EquipmentCtrl()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);
	int nGroupCommStatus = AddGroup(_T("Equipment Ctrl"), TRUE, FALSE, hIcon);

	//AddLabel(nGroupCommStatus, _T("Utilities"), -1, TRUE);	
	AddWindow(nGroupCommStatus, m_bn_Device[DEV_BN_EquipmentInit].GetSafeHwnd(), 30);
}
//=============================================================================
// Method		: Add_OperateMode
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/12 - 20:06
// Desc.		:
//=============================================================================
void CPane_CommStatus::Add_OperateMode()
{
// MasterMode & Start-up Mode 추가 [1/30/2019 Seongho.Lee]
	{
		HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

		int nGroupCommStatus = AddGroup(_T("Operate Mode"), FALSE, TRUE, hIcon);

		//AddLabel(nGroupCommStatus, _T(""), -1, TRUE);
		AddWindow(nGroupCommStatus, m_st_Device[DevSI_OperateMode].GetSafeHwnd(), 20);
		AddWindow(nGroupCommStatus, m_bn_Device[DEV_BN_OperateModeChange].GetSafeHwnd(), 30);

		SetStatus_OperateMode(enOperateMode::OpMode_Production);
	}
}

//=============================================================================
// Method		: SetStatus_PermissionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2016/9/21 - 20:56
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_PermissionMode(__in enPermissionMode InspMode)
{
	m_st_Device[DevSI_PermissionMode].SetText(g_szPermissionMode[InspMode]);

	switch (InspMode)
	{
	case Permission_Operator:
		m_st_Device[DevSI_PermissionMode].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case Permission_Manager:
		m_st_Device[DevSI_PermissionMode].SetColorStyle(CVGStatic::ColorStyle_Yellow);
		break;

	case Permission_Administrator:
		m_st_Device[DevSI_PermissionMode].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	case Permission_Engineer:
		m_st_Device[DevSI_PermissionMode].SetColorStyle(CVGStatic::ColorStyle_Blue);
		break;
	case Permission_CNC:
		m_st_Device[DevSI_PermissionMode].SetColorStyle(CVGStatic::ColorStyle_Blue);
		break;
	default:
		break;
	}
}
//=============================================================================
// Method		: SetStatus_OperateMode
// Access		: public  
// Returns		: void
// Parameter	: __in enOperateMode nOperMode
// Qualifier	:
// Last Update	: 2018/3/12 - 20:05
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_OperateMode(__in enOperateMode nOperMode)
{
	m_st_Device[DevSI_OperateMode].SetText(g_szOperateMode[nOperMode]);

	switch (nOperMode)
	{
	case OpMode_Production:
		m_st_Device[DevSI_OperateMode].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case OpMode_Master:
		m_st_Device[DevSI_OperateMode].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	case OpMode_StartUp_Check:
		m_st_Device[DevSI_OperateMode].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

// 	case OpMode_DryRun:
// 		m_st_Device[DevSI_OperateMode].SetColorStyle(CVGStatic::ColorStyle_Red);
// 		break;

	default:
		break;
	}
}
//=============================================================================
// Method		: SetStatus_MES
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nCommStatus
// Qualifier	:
// Last Update	: 2016/5/31 - 17:27
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_MES(__in UINT nCommStatus)
{
	switch (nCommStatus)
	{
	case enTCPIPConnectStatus::COMM_CONNECTED:
		m_st_Device[Dev_MES].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case enTCPIPConnectStatus::COMM_DISCONNECT:
		m_st_Device[Dev_MES].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		break;

	case enTCPIPConnectStatus::COMM_CONNECT_DROP:
		m_st_Device[Dev_MES].SetColorStyle(CVGStatic::ColorStyle_Yellow);
		break;

	case enTCPIPConnectStatus::COMM_CONNECT_ERROR:
		m_st_Device[Dev_MES].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;
	}
}

//=============================================================================
// Method		: SetStatus_Motor
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nConStatus
// Qualifier	:
// Last Update	: 2017/6/12 - 17:45
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_Motor(__in UINT nConStatus)
{
	enCommStatusType nStatus = (enCommStatusType)nConStatus;

	switch (nStatus)
	{
	case COMM_STATUS_CONNECT:
		m_st_Device[DevSI_PCIMotorBrd].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case COMM_STATUS_NOTCONNECTED:
	case COMM_STATUS_DISCONNECT:
		m_st_Device[DevSI_PCIMotorBrd].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetStatus_IO
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nConStatus
// Qualifier	:
// Last Update	: 2017/6/12 - 17:47
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_IO(__in UINT nConStatus)
{
	enCommStatusType nStatus = (enCommStatusType)nConStatus;

	switch (nStatus)
	{
	case COMM_STATUS_CONNECT:
		m_st_Device[DevSI_PCIIoBrd].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case COMM_STATUS_NOTCONNECTED:
	case COMM_STATUS_DISCONNECT:
		m_st_Device[DevSI_PCIIoBrd].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	case COMM_STATUS_SYNC_OK:
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetStatus_GrabberBrd
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nConStatus
// Qualifier	:
// Last Update	: 2016/10/6 - 20:50
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_GrabberBrd_ComArt(__in UINT nConStatus)
{
 	if (nConStatus)
 		m_st_Device[DevSI_GrabberBrd_ComArt].SetColorStyle(CVGStatic::ColorStyle_Green);
 	else
 		m_st_Device[DevSI_GrabberBrd_ComArt].SetColorStyle(CVGStatic::ColorStyle_Red);
}

//=============================================================================
// Method		: SetStatus_BCR
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bConStatus
// Qualifier	:
// Last Update	: 2016/10/6 - 20:48
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_BCR(__in BOOL bConStatus)
{
	if (bConStatus)
		m_st_Device[DevSI_BCR].SetColorStyle(CVGStatic::ColorStyle_Green);
	else
		m_st_Device[DevSI_BCR].SetColorStyle(CVGStatic::ColorStyle_Red);
}

void CPane_CommStatus::SetStatus_LabelPrinter(__in BOOL bConStatus)
{
	if (bConStatus)
		m_st_Device[DevSI_LabelPrinter].SetColorStyle(CVGStatic::ColorStyle_Green);
	else
		m_st_Device[DevSI_LabelPrinter].SetColorStyle(CVGStatic::ColorStyle_Red);
}

//=============================================================================
// Method		: SetStatus_CameraBoard
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nBrdIdx
// Parameter	: __in UINT nConStatus
// Qualifier	:
// Last Update	: 2016/10/6 - 20:48
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_CameraBoard(__in UINT nBrdIdx, __in UINT nConStatus)
{
	enCommStatusType nStatus = (enCommStatusType)nConStatus;

	switch (nStatus)
	{
	case COMM_STATUS_CONNECT:
		m_st_Device[DevSI_CanMain + nBrdIdx].SetColorStyle(CVGStatic::ColorStyle_Yellow);
		break;

	case COMM_STATUS_NOTCONNECTED:
	case COMM_STATUS_DISCONNECT:
		m_st_Device[DevSI_CanMain + nBrdIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	case COMM_STATUS_SYNC_OK:
		m_st_Device[DevSI_CanMain + nBrdIdx].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetStatus_LightBoard
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nBrdIdx
// Parameter	: __in UINT nConStatus
// Qualifier	:
// Last Update	: 2017/6/28 - 16:24
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_LightBoard(__in UINT nBrdIdx, __in UINT nConStatus)
{
	enCommStatusType nStatus = (enCommStatusType)nConStatus;

	switch (nStatus)
	{
	case COMM_STATUS_CONNECT:
		m_st_Device[DevSI_Light + nBrdIdx].SetColorStyle(CVGStatic::ColorStyle_Yellow);
		break;

	case COMM_STATUS_NOTCONNECTED:
	case COMM_STATUS_DISCONNECT:
		m_st_Device[DevSI_Light + nBrdIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	case COMM_STATUS_SYNC_OK:
		m_st_Device[DevSI_Light + nBrdIdx].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetStatus_Indicator
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nConStatus
// Parameter	: __in UINT nChIdx
// Qualifier	:
// Last Update	: 2017/1/2 - 16:56
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_Indicator(__in UINT nConStatus, __in UINT nChIdx)
{
	enCommStatusType nStatus = (enCommStatusType)nConStatus;

	switch (nStatus)
	{
	case COMM_STATUS_CONNECT:
		m_st_Device[DevSI_IndicatorX + nChIdx].SetColorStyle(CVGStatic::ColorStyle_Yellow);
		break;

	case COMM_STATUS_NOTCONNECTED:
	case COMM_STATUS_DISCONNECT:
		m_st_Device[DevSI_IndicatorX + nChIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	case COMM_STATUS_SYNC_OK:
		m_st_Device[DevSI_IndicatorX + nChIdx].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	default:
		break; 
	}
}

//=============================================================================
// Method		: Set_Barcode
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2016/10/20 - 21:21
// Desc.		:
//=============================================================================
void CPane_CommStatus::Set_Barcode(__in LPCTSTR szBarcode)
{
#ifdef USE_LOG_WND

	m_ed_WarningLog.AddString(szBarcode);
	m_ed_WarningLog.AddString(_T("\n"));

#endif
}

//=============================================================================
// Method		: SetIndicatorDisplay
// Access		: public  
// Returns		: void
// Parameter	: __in float fAxisX
// Parameter	: __in float fAxisY
// Qualifier	:
// Last Update	: 2017/6/15 - 19:42
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetIndicatorDisplay(__in float fAxisX, __in float fAxisY)
{
	CString szText;

	if (fAxisX >= 0)
		szText.Format(_T("+ %.2f"), fAxisX);
	else
		szText.Format(_T("%.2f"), fAxisX);

	m_st_Indecator[DEV_ST_IndicatorX].SetText(szText);

	if (fAxisY >= 0)
		szText.Format(_T("+ %.2f"), fAxisY);
	else
		szText.Format(_T("%.2f"), fAxisY);

	m_st_Indecator[DEV_ST_IndicatorY].SetText(szText);
}
