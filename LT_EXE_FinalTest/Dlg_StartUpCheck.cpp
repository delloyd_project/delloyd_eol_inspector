// Dlg_StartUpCheck.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Dlg_StartUpCheck.h"
#include "afxdialogex.h"

#include "Dlg_ChkPassword.h"

#define		IDC_BN_OK				1000
#define		IDC_BN_CANCEL			1001
#define		IDC_BN_TESTITEM			1002

// CDlg_StartUpCheck 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_StartUpCheck, CDialogEx)

CDlg_StartUpCheck::CDlg_StartUpCheck(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_StartUpCheck::IDD, pParent)
{

	VERIFY(m_font.CreateFont(
		30,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font2.CreateFont(
		22,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

}

CDlg_StartUpCheck::~CDlg_StartUpCheck()
{
	m_font.DeleteObject();
	m_font2.DeleteObject();
}




void CDlg_StartUpCheck::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}



BEGIN_MESSAGE_MAP(CDlg_StartUpCheck, CDialogEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BN_OK, OnBnClickedBnOK)
	ON_BN_CLICKED(IDC_BN_CANCEL, OnBnClickedBnCancel)
	//ON_CBN_SELCHANGE(IDC_CB_TESTITEM, OnCbnSelChangeTestItem)

END_MESSAGE_MAP()


// CDlg_StartUpCheck 메시지 처리기입니다.


int CDlg_StartUpCheck::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// 검사 항목
// 	m_cb_TestItem.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_TESTITEM);
// 	m_cb_TestItem.SetFont(&m_font2);
// 
// 	for (UINT nIdx = 0; nIdx < TIID_MaxEnum; nIdx++)
// 	{
// 		m_cb_TestItem.AddString(g_szLT_TestItem_Name[nIdx]);
// 	}
// 	m_cb_TestItem.SetCurSel(0);
	for (int i = TIID_Current; i < TIID_MaxEnum; i++)
	{
		m_bn_TestItem[i].Create(g_szLT_TestItem_Name[i], dwStyle | BS_PUSHLIKE | BS_AUTOCHECKBOX, rectDummy, this, IDC_BN_TESTITEM + i);
		m_bn_TestItem[i].SetImage(IDB_UNCHECKED_16);
		m_bn_TestItem[i].SetCheckedImage(IDB_CHECKED_16);
		m_bn_TestItem[i].SizeToContent();
		m_bn_TestItem[i].SetCheck(BST_UNCHECKED);
	}

	m_stNGItem.SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_stNGItem.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_stNGItem.SetFont_Gdip(L"Arial", 10.0F);
	m_stNGItem.Create(_T("Defect Type"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_stText.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_stText.SetColorStyle(CVGStatic::ColorStyle_Orange);
	m_stText.SetFont_Gdip(L"Arial", 20.0F);
	m_stText.Create(_T("Start-Up Check"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_bn_OK.Create(_T("OK"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_OK);
	m_bn_OK.SetMouseCursorHand();
	m_bn_OK.SetFont(&m_font);

	m_bn_Cancel.Create(_T("CANCEL"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_CANCEL);
	m_bn_Cancel.SetMouseCursorHand();
	m_bn_Cancel.SetFont(&m_font);

	return 0;
}


void CDlg_StartUpCheck::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing = 5;
	int iCateSpacing = 10;
	int iMagin = 10;
	int iLeft = iMagin;
	int iTop = iMagin;
	int iWidth = cx - (iMagin * 2);
	int iHeight = cy - (iMagin * 2);
	int iItemHeight = iHeight * 5 / 6;

	m_stText.MoveWindow(iLeft, iTop, iWidth, iHeight / 5);
	
	iTop += (iHeight / 5) + iSpacing;
	//m_stNGItem.MoveWindow(iLeft, iTop, iWidth / 5 - iSpacing, 30);
	
	for (int i = TIID_Current; i < TIID_MaxEnum; i++)
	{
		m_bn_TestItem[i].MoveWindow(iLeft, iTop, iWidth, (iHeight * 3 / 5) / (TIID_MaxEnum - TIID_Current));
		iTop += (iHeight * 3 / 5) / (TIID_MaxEnum - TIID_Current) + iSpacing;
	}

	m_bn_OK.MoveWindow(iLeft, iTop, iWidth / 2, iHeight - iTop + iMagin);
	m_bn_Cancel.MoveWindow(iLeft + iWidth / 2, iTop, iWidth / 2, iHeight - iTop + iMagin);

	//iLeft += iWidth / 2;
	//m_bn_NG.MoveWindow(iLeft, iTop, iWidth / 2, iHeight / 6);
}


void CDlg_StartUpCheck::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	lpMMI->ptMaxTrackSize.x = 1000;
	lpMMI->ptMaxTrackSize.y = 800;

	lpMMI->ptMinTrackSize.x = 1000;
	lpMMI->ptMinTrackSize.y = 800;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}


BOOL CDlg_StartUpCheck::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreCreateWindow(cs);
}


BOOL CDlg_StartUpCheck::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_RETURN)
	{
		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_ESCAPE)
	{
		// 여기에 ESC키 기능 작성       
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CDlg_StartUpCheck::OnBnClickedBnOK()
{
// 	for (int i = TIID_Current; i < TIID_MaxEnum; i++)
// 	{
// 		m_pstInspInfo->nNGTestItemMaster[i] = m_bn_TestItem[i].GetCheck();
// 	}

	OnOK();
}

void CDlg_StartUpCheck::OnBnClickedBnCancel()
{
	CDlg_ChkPassword dlgPassword(this);

	if (IDOK == dlgPassword.DoModal())
	{
		OnCancel();
	}
}


