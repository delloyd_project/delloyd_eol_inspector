﻿#ifndef List_Work_DefectPixel_h__
#define List_Work_DefectPixel_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_Defect_Worklist
{
	Defect_W_Recode,
	Defect_W_Time,
	Defect_W_Equipment,
	Defect_W_Model,
	Defect_W_SWVersion,
	Defect_W_LOTNum,
	Defect_W_Barcode,
	Defect_W_Operator,
	Defect_W_Result,
	Defect_W_Count,
	Defect_W_Count2,
	Defect_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_Defect_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Operator"),
	_T("Result"),
	_T("Dark"),
	_T("Cluster"),
	NULL,							
};

const int	iListAglin_Defect_Worklist[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_Defect_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};
// CList_Work_Particle

class CList_Work_DefectPixel : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_DefectPixel)

public:
	CList_Work_DefectPixel();
	virtual ~CList_Work_DefectPixel();
	CFont		m_Font;

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize				(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow	(CREATESTRUCT& cs);

	UINT Header_MaxNum();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);
	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);

	void GetData		(UINT nRow, UINT &DataNum, CString *Data);


	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

};


#endif // List_Work_Particle_h__
