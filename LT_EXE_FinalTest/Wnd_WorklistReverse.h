﻿//*****************************************************************************
// Filename	: Wnd_WorklistReverse.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistReverse_h__
#define Wnd_WorklistReverse_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_Reverse.h"

//=============================================================================
// Wnd_WorklistReverse
//=============================================================================
class CWnd_WorklistReverse : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistReverse)

public:
	CWnd_WorklistReverse();
	virtual ~CWnd_WorklistReverse();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

	CList_Work_Reverse				m_list_Reverse[TICnt_Reverse];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistReverse_h__


