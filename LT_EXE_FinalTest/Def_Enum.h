﻿//*****************************************************************************
// Filename	: Def_Enum.h
// Created	: 2010/11/23
// Modified	: 2016/06/30
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef Def_Enum_h__
#define Def_Enum_h__

#include "Def_Enum_Cm.h"

//-------------------------------------------------------------------
// UI 관련
//-------------------------------------------------------------------

typedef struct _tag_TestItemMode
{
	BOOL	bCurrent;
	BOOL	bCenterPoint;
	BOOL	bEIAJ;
	BOOL	bSFR;
	BOOL	bRotate;
	BOOL	bPatternNoise;
	BOOL	bSteeringLinkage;
	BOOL	bWarningPhrase;
	BOOL	bParticle;
	BOOL	bIR_Filter;
	BOOL	bVideoSignal;
	BOOL	bFOV;
	BOOL	bImageReversal;
	BOOL	bBrightnessRatio;
	BOOL	bColor;

	_tag_TestItemMode()
	{
		bCurrent			= TRUE;
		bCenterPoint		= TRUE;
		bEIAJ				= TRUE;
		bSFR				= FALSE;
		bRotate				= FALSE;
		bPatternNoise		= FALSE;
		bSteeringLinkage	= FALSE;
		bWarningPhrase		= FALSE;
		bParticle			= FALSE;
		bIR_Filter			= FALSE;
		bVideoSignal		= FALSE;
		bFOV				= FALSE;
		bImageReversal		= FALSE;
		bBrightnessRatio	= FALSE;
		bColor				= FALSE;
	};

}ST_TestItemMode;

// 통신 Device
typedef enum 
{
	COMM_DEV_BASE		= 100,
	COMM_DEV_FTDI		= 200,
	COMM_DEV_MCTRL		= 300,
	COMM_DEV_LPRNT		= 400,
	COMM_DEV_LAST		= COMM_DEV_LPRNT,// 마지막으로 설정할것.
	COMM_DEV_MAX,
}enumCommStatus_Device;

typedef enum enMasterMode
{
	Master_Set,
	Master_Test,
	Master_MaxEnum,
};

typedef enum enLightCtrl
{
	LightChat_Center,
	LightChat_Left,
	LightChat_Right,
	LightChat_Particle,
	LightChat_MaxEnum,
};

typedef enum enChangeView
{
	VideoView_Ch_1,
	VideoView_MaxEnum,
};

typedef enum enSocketIndex
{
	Socket_1,
	Socket_MaxEnum,
};

typedef enum enPic_TestItem
{
	PIC_Standby,
	PIC_Angle,
	PIC_CenterPoint,
	PIC_Color,
	PIC_Current,
//	PIC_FFT,
	PIC_BlackSpot,
	PIC_DefectPixel,
	PIC_Brightness,
	PIC_Rotate,
//	PIC_SFR,
	PIC_EIAJ,
	PIC_Reverse,
	PIC_ParticleManual,
//	PIC_PatternNoise,
	PIC_IRFilter,
//	PIC_IRFilterManual,
//	PIC_LEDTest,
	PIC_VideoSignal,
	PIC_Reset,
//	PIC_OperMode,
	PIC_MaxEnum,
};

enum enManual_Commend
{
	//MUL_InitialPos = 0,
	MUL_VoltOn,
	MUL_VoltOff,
	//MUL_IRLEDOn,
	//MUL_IRLEDOff,
	MUL_OriginalImage,
//	MUL_PicImage,
//	MUL_Initialize,
	MUL_ImageLoad,
	MUL_LED_On,
	MUL_LED_Off,
	MUL_IR_On,
	MUL_IR_Off,
	MUL_MaxEnum,
};

enum enManual_Motion_Com
{
	//MUL_MOT_Initial,
	MUL_MOT_Load = 0,
	MUL_MOT_Insp,
	MUL_MOT_Par_Load,
	MUL_MOT_Par_Insp,
	MUL_MOT_Cyl_In,
	MUL_MOT_Cyl_Out,
	MUL_MOT_MaxEnum,
};

enum enManual_TESTCommend
{
	MT_Cmd_Current = 0,
	MT_Cmd_CenterPoint,
	MT_Cmd_Rotate,
	MT_Cmd_EIAJ,
	MT_Cmd_SFR,
	MT_Cmd_Particle,
	MT_Cmd_Brightness,
	MT_Cmd_Total,
};


typedef enum enSFR_FontMode
{
	SFR_Mode_F_Left = 0,
	SFR_Mode_F_Right,
	SFR_Mode_F_Up,
	SFR_Mode_F_Down,
	SFR_Mode_F_Max,
};

static LPCTSTR g_szSFR_FontMode[SFR_Mode_F_Max] =
{
	_T("◁"),
	_T("▷"),
	_T("△"),
	_T("▽"),
};

enum enSFRDataType
{
	enSFRDataType_MTF,
	enSFRDataType_CyPx,
	enSFRDataType_Cymm,
	enSFRDataType_Max,
};

typedef enum enImageMode
{
	ImageMode_LiveCam,
	ImageMode_StillShotImage,
	ImageMode_MaxNum,
};

static LPCTSTR g_szSFRDataType[] =
{
	_T("/ MTF"),
	_T("/ Px"),
	_T("/ mm"),
	NULL
};


#ifdef MAX_CHANNEL_CNT
#undef MAX_CHANNEL_CNT
#define 	MAX_CHANNEL_CNT			6	// 장비에서 실제 사용하는 채널 수 (소켓, 사이트 등)
#endif

#ifdef MAX_OPERATION_THREAD
#undef MAX_OPERATION_THREAD
#define 	MAX_OPERATION_THREAD	6	// 최대 개별 검사용 쓰레드 개수
#endif

#ifdef MAX_MODULE_CNT
#undef MAX_MODULE_CNT
#define 	MAX_MODULE_CNT			6	// 최대 제품 투입 개수 (25ch다운: 25, 6ch다운: 6, 4ch보임량: 4)
#endif

typedef enum _ConstVar_Eqp
{
	USE_CHANNEL_CNT = 1,	// 장비에서 실제 사용하는 채널 수 (소켓, 사이트 등)
	USE_TEST_ITEM_CNT = 3,
};

#endif // Def_Enum_h__

