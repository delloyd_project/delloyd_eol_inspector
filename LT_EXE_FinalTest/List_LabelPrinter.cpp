﻿// List_LabelPrinter.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_LabelPrinter.h"
#include "Def_WindowMessage_Cm.h"

#define OPT_ED_CELLEDIT		5001
#define OPT_CB_FONT			5002

// CList_LabelPrinter


IMPLEMENT_DYNAMIC(CList_LabelPrinter, CListCtrl)

CList_LabelPrinter::CList_LabelPrinter()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);

	m_nEditCol	= 0;
	m_nEditRow	= 0;

	m_pstLabel	= NULL;
}

CList_LabelPrinter::~CList_LabelPrinter()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_LabelPrinter, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_LabelPrinter::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_LabelPrinter::OnNMDblclk)
	ON_EN_KILLFOCUS(OPT_ED_CELLEDIT, &CList_LabelPrinter::OnEnKillFocusEdit)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_LabelPrinter 메시지 처리기입니다.
int CList_LabelPrinter::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER );

	InitHeader(); 
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER, CRect(0, 0, 0, 0), this, OPT_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_LabelPrinter::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

// 	int iColWidth[LabelPrt_MaxCol] = { 0, };
// 	int iColDivide = 0;
// 	int iUnitWidth = 0;
// 	int iMisc = 0;
// 
// 	CRect rectClient;
// 	GetClientRect(rectClient);
// 
// 	for (int nCol = LabelPrt_PosX; nCol < LabelPrt_MaxCol; nCol++)
// 	{
// 		iUnitWidth = (rectClient.Width() - iHeaderWidth_LabelPrinter[LabelPrt_Object]) / (LabelPrt_MaxCol - LabelPrt_Text);
// 		SetColumnWidth(nCol, iUnitWidth);
// 	}

}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_LabelPrinter::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_LabelPrinter::InitHeader()
{
	for (int nCol = 0; nCol < LabelPrt_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_LabelPrinter[nCol], iListAglin_LabelPrinter[nCol], iHeaderWidth_LabelPrinter[nCol]);
	}

	for (int nCol = 0; nCol < LabelPrt_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_LabelPrinter[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_LabelPrinter::InsertFullData()
{
	if (m_pstLabel == NULL)
		return;

 	DeleteAllItems();
 
	for (UINT nIdx = 0; nIdx < LabelPrt_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_LabelPrinter::SetRectRow(UINT nRow)
{
	if (m_pstLabel == NULL)
		return;

	CString strValue;

	SetItemText(nRow, LabelPrt_Object, g_szLabelDataType[nRow]);

	strValue.Format(_T("%s"), m_pstLabel->szText[nRow]);
	SetItemText(nRow, LabelPrt_Text, strValue);

	strValue.Format(_T("%d"), m_pstLabel->iPosX[nRow]);
	SetItemText(nRow, LabelPrt_PosX, strValue);

	strValue.Format(_T("%d"), m_pstLabel->iPosY[nRow]);
	SetItemText(nRow, LabelPrt_PosY, strValue);

	strValue.Format(_T("%d"), m_pstLabel->iWidthSz[nRow]);
	SetItemText(nRow, LabelPrt_Width, strValue);

	strValue.Format(_T("%d"), m_pstLabel->iHeightSz[nRow]);
	SetItemText(nRow, LabelPrt_Height, strValue);

}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_LabelPrinter::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LVHITTESTINFO HitInfo;
	HitInfo.pt = pNMItemActivate->ptAction;
	
	HitTest(&HitInfo);

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_LabelPrinter::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem > LabelPrt_Object && pNMItemActivate->iSubItem < LabelPrt_MaxCol)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			//...
			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusECpOpellEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_LabelPrinter::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	UpdateCellData(m_nEditRow, m_nEditCol, strText);
	

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}


//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_LabelPrinter::UpdateCellData(UINT nRow, UINT nCol, CString szValue)
{
	if (m_pstLabel == NULL)
		return FALSE;

	int iValue = 0;
	CString strValue;

	if (iValue < 0)
		iValue = 0;

	switch (nCol)
	{
	case LabelPrt_Text:
		m_pstLabel->szText[nRow] = szValue;
		break;
	case LabelPrt_PosX:
		iValue = _ttoi(szValue);
		m_pstLabel->iPosX[nRow] = iValue;
		break;
	case LabelPrt_PosY:
		iValue = _ttoi(szValue);
		m_pstLabel->iPosY[nRow] = iValue;
		break;
	case LabelPrt_Width:
		iValue = _ttoi(szValue);
		m_pstLabel->iWidthSz[nRow] = iValue;
		break;
	case LabelPrt_Height:
		iValue = _ttoi(szValue);
		m_pstLabel->iHeightSz[nRow] = iValue;
		break;
	
	default:
		break;
	}

	switch (nCol)
	{
	case LabelPrt_Text:
		strValue = szValue;
		break;
	case LabelPrt_PosX:
		strValue.Format(_T("%d"), m_pstLabel->iPosX[nRow]);
		break;
	case LabelPrt_PosY:
		strValue.Format(_T("%d"), m_pstLabel->iPosY[nRow]);
		break;
	case LabelPrt_Width:
		strValue.Format(_T("%d"), m_pstLabel->iWidthSz[nRow]);
		break;
	case LabelPrt_Height:
		strValue.Format(_T("%d"), m_pstLabel->iHeightSz[nRow]);
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_LabelPrinter::GetCellData()
{
	if (m_pstLabel == NULL)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_LabelPrinter::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue	= _ttof(strText);

		if (0 < zDelta)
		{
			iValue	= iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120) * 0.01);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dbValue)
				dbValue = dbValue + ((zDelta / 120) * 0.01);
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;
		
		strText.Format(_T("%d"), iValue);
		UpdateCellData(m_nEditRow, m_nEditCol, strText);
	}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}
