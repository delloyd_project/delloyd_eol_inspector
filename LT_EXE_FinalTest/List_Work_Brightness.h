﻿#ifndef List_Work_Brightness_h__
#define List_Work_Brightness_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_Br_Worklist
{
	Br_W_Recode,
	Br_W_Time,
	Br_W_Equipment,
	Br_W_Model,
	Br_W_SWVersion,
	//Br_W_CameraType,
	Br_W_LOTNum,
	Br_W_Barcode,
	//Br_W_Operator,
	Br_W_Result,
	Br_W_Side1,
	Br_W_Side2,
	Br_W_Side3,
	Br_W_Side4,
	Br_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_Br_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Result"),
	_T("Side 1"),
	_T("Side 2"),
	_T("Side 3"),
	_T("Side 4"),
	NULL
};

const int	iListAglin_Br_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_Br_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};

// CList_Work_Brightness

class CList_Work_Brightness : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_Brightness)

public:
	CList_Work_Brightness();
	virtual ~CList_Work_Brightness();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;

	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	};

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // List_Work_Brightness_h__
