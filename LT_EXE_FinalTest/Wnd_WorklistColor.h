﻿//*****************************************************************************
// Filename	: Wnd_WorklistColor.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistColor_h__
#define Wnd_WorklistColor_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_Color.h"

//=============================================================================
// Wnd_WorklistColor
//=============================================================================
class CWnd_WorklistColor : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistColor)

public:
	CWnd_WorklistColor();
	virtual ~CWnd_WorklistColor();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

	CList_Work_Color			m_list_Color[TICnt_Color];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistColor_h__


