﻿// List_Work_Total.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_Work_Total.h"

// CList_Work_Total

IMPLEMENT_DYNAMIC(CList_Work_Total, CListCtrl)

CList_Work_Total::CList_Work_Total()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Work_Total::~CList_Work_Total()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_Work_Total, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CList_Work_Total 메시지 처리기입니다.

int CList_Work_Total::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);
	InitHeader();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/2/22 - 13:37
// Desc.		:
//=============================================================================
void CList_Work_Total::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	//if ((cx == 0) && (cy == 0))
	//	return;

 //	CRect rc;
 //	GetClientRect(&rc);
 //
 //	for (int nCol = Total_W_Recode; nCol < Total_W_MaxCol; nCol++)
 //	{
 //		SetColumnWidth(nCol, (rc.Width() - iHeaderWidth_Total_Worklist[Total_W_Recode]) / (Total_W_MaxCol - Total_W_Time));
 //	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/22 - 13:37
// Desc.		:
//=============================================================================
BOOL CList_Work_Total::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: Header_MaxNum
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/2/22 - 13:37
// Desc.		:
//=============================================================================
UINT CList_Work_Total::Header_MaxNum()
{
	return (UINT)Total_W_MaxCol;
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/22 - 13:37
// Desc.		:
//=============================================================================
void CList_Work_Total::InitHeader()
{
	for (int nCol = 0; nCol < Total_W_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_Total_Worklist[nCol], iListAglin_Total_Worklist[nCol], iHeaderWidth_Total_Worklist[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 13:37
// Desc.		:
//=============================================================================
void CList_Work_Total::InsertFullData(__in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	int iNewCount = GetItemCount();

	InsertItem(iNewCount, _T(""));

	SetRectRow(iNewCount, pstCamInfo);

}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: __in const ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 13:37
// Desc.		:
//=============================================================================
void CList_Work_Total::SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	CString strText;

	strText.Format(_T("%s"), pstCamInfo->szIndex);
	SetItemText(nRow, Total_W_Recode, strText);

	strText.Format(_T("%s"), pstCamInfo->szTime);
	SetItemText(nRow, Total_W_Time, strText);

	strText.Format(_T("%s"), pstCamInfo->szEquipment);
	SetItemText(nRow, Total_W_Equipment, strText);

	strText.Format(_T("%s"), pstCamInfo->szModelName);
	SetItemText(nRow, Total_W_Model, strText);

	strText.Format(_T("%s"), pstCamInfo->szSWVersion);
	SetItemText(nRow, Total_W_SWVersion, strText);

	strText.Format(_T("%s"), pstCamInfo->szLotID);
	SetItemText(nRow, Total_W_LOTNum, strText);

	strText.Format(_T("%s"), pstCamInfo->szBarcode);
	SetItemText(nRow, Total_W_Barcode, strText);

	//strText.Format(_T("%s"), pstCamInfo->szOperatorName);
	//SetItemText(nRow, Total_W_Operator, strText);

	strText.Format(_T("%s"), g_TestResult[pstCamInfo->nJudgment].szText);
	SetItemText(nRow, Total_W_Result, strText);


	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->nJudgmentInitial].szText);
	if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	{
		strText = _T("X");
	}
	SetItemText(nRow, Total_W_Initalize, strText);


	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stCurrent[TICurrentCnt_1].stCurrentResult.nResult].szText);
	if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	{
		strText = _T("X");
	}
	SetItemText(nRow, Total_W_Current, strText);

	//strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stOperMode[TIOperationModeCnt_1].nResult].szText);
	//if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	//{
	//	strText = _T("X");
	//}
	//SetItemText(nRow, Total_W_OperMode, strText);


	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stCenterPoint[TICenterPtCnt_1].stCenterPointResult.nResult].szText);
	if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	{
		strText = _T("X");
	}
	SetItemText(nRow, Total_W_CenterPoint, strText);


	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stRotate[TIRotateCnt_1].stRotateResult.nResult].szText);
	if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	{
		strText = _T("X");
	}
	SetItemText(nRow, Total_W_Rotate, strText);


	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stEIAJ[TIEIAJCnt_1].stEIAJData.nResult].szText);
	if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	{
		strText = _T("X");
	}
	SetItemText(nRow, Total_W_EIAJ, strText);


	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stAngle[TIAngleCnt_1].stAngleResult.nResult].szText);
	if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	{
		strText = _T("X");
	}
	SetItemText(nRow, Total_W_Angle, strText);


	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stColor[TIColorCnt_1].stColorResult.nResult].szText);
	if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	{
		strText = _T("X");
	}
	SetItemText(nRow, Total_W_Color, strText);


	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stReverse[TIReverseCnt_1].stReverseResult.nResult].szText);
	if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	{
		strText = _T("X");
	}
	SetItemText(nRow, Total_W_Reverse, strText);


	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stParticle[TIBlackSpotCnt_1].stParticleResult.nResult].szText);
	if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	{
		strText = _T("X");
	}
	SetItemText(nRow, Total_W_BlackSpot, strText);


	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stParticleMA[TIParticleManualCnt_1].stParticleResult.nResult].szText);
	if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	{
		strText = _T("X");
	}
	SetItemText(nRow, Total_W_ParticleMA, strText);

	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stParticle[TIDefectPixelCnt_1].stParticleResult.nResult].szText);
	if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	{
		strText = _T("X");
	}
	SetItemText(nRow, Total_W_DefectPixel, strText);


	//strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stLED[TILEDCnt_1].stLEDCurrResult.nResult].szText);
	//if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	//{
	//	strText = _T("X");
	//}
	//SetItemText(nRow, Total_W_LED, strText);

	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stBrightness[TIBrightnessCnt_1].stBrightnessResult.nResult].szText);
	if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	{
		strText = _T("X");
	}
	SetItemText(nRow, Total_W_Brightness, strText);

	//strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stPatternNoise[TIPatternNoiseCnt_1].stPatternNoiseResult.nResult].szText);
	//if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	//{
	//	strText = _T("X");
	//}
	//SetItemText(nRow, Total_W_PatternNoise, strText);

	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stIRFilter[TIIRFilterCnt_1].stIRFilterResult.nResult].szText);
	if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	{
		strText = _T("X");
	}
	SetItemText(nRow, Total_W_IRFilter, strText);


	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->nJudgmentFinalize].szText);

	if (strText == _T("STAND BY") || strText == _T("EMPTY"))
	{
		strText = _T("X");
	}
	SetItemText(nRow, Total_W_Finalize, strText);

}

//=============================================================================
// Method		: GetData
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: UINT & DataNum
// Parameter	: CString * Data
// Qualifier	:
// Last Update	: 2017/2/22 - 13:40
// Desc.		:
//=============================================================================
void CList_Work_Total::GetData(UINT nRow, UINT &DataNum, CString *Data)
{
	DataNum = Total_W_MaxCol;
	CString temp[Total_W_MaxCol];
	for (int t = 0; t < Total_W_MaxCol; t++)
	{
		temp[t] = GetItemText(nRow, t);
		Data[t] = GetItemText(nRow, t);
	}
}
