﻿#ifndef Wnd_BaseSFROp_h__
#define Wnd_BaseSFROp_h__

#pragma once

#include "Wnd_SFROp.h"

// CWnd_BaseSFROp

class CWnd_BaseSFROp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseSFROp)

public:
	CWnd_BaseSFROp();
	virtual ~CWnd_BaseSFROp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
//	CWnd_SFROp m_wndSFROp[TICnt_SFR];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetPath(__in LPCTSTR szI2CPath)
	{
		//for (int i = 0; i < TICnt_SFR; i++)
		//	m_wndSFROp[i].SetPath(szI2CPath);
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseSFROp_h__
