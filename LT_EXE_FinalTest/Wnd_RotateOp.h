﻿#ifndef Wnd_RotateOp_h__
#define Wnd_RotateOp_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "List_RotateOp.h"
#include "Def_DataStruct_Cm.h"

// CWnd_RotateOp

enum enRotateStatic
{
	STI_ROT_MinSpc,
	STI_ROT_MaxSpc,
	STI_ROT_Offset,
	STI_ROT_ROI,
	STI_ROT_MAX,
};

static LPCTSTR	g_szRotateStatic[] =
{
	_T("MIN DEGREE"),
	_T("MAX DEGREE"),
	_T("OFFSET"),
	_T("ROI LIST"),
	NULL
};

enum enRotateButton
{
	BTN_ROT_TEST = 0,
	BTN_ROT_MAX,
};

static LPCTSTR	g_szRotateButton[] =
{
	_T("TEST"),
	NULL
};

enum enRotateComobox
{
	CMB_ROT_MAX = 1,
};

static LPCTSTR	g_szRotateCommoBox[] =
{
	NULL
};

enum enRotateEdit
{
	EDT_ROT_MinSpc,
	EDT_ROT_MaxSpc,
	EDT_ROT_Offset,
	EDT_ROT_MAX,
};

class CWnd_RotateOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_RotateOp)

public:
	CWnd_RotateOp();
	virtual ~CWnd_RotateOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo		*m_pstModelInfo;
	CList_RotateOp		m_List;

	CFont				m_font;

	CVGStatic			m_st_Item[STI_ROT_MAX];
	CButton				m_bn_Item[BTN_ROT_MAX];
	CComboBox			m_cb_Item[CMB_ROT_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_ROT_MAX];

	// 검사 항목이 다수 인경우
	UINT				m_nTestItemCnt;

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_RotateOp_h__
