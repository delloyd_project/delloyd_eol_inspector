﻿//*****************************************************************************
// Filename	: 	Reg_InspInfo.h
// Created	:	2016/3/31 - 16:33
// Modified	:	2016/3/31 - 16:33
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Reg_InspInfo_h__
#define Reg_InspInfo_h__

#pragma once
#include "Registry.h"
#include "Def_DataStruct.h"

class CReg_InspInfo
{
public:
	CReg_InspInfo();
	CReg_InspInfo(__in LPCTSTR lpszRegPath);
	~CReg_InspInfo();

protected:
	CRegistry	m_Reg;

	CString		m_strRegPath;

public:
	void		SetRegitryPath(__in LPCTSTR lpszRegPath)
	{
		m_strRegPath = lpszRegPath;
	}

	BOOL	SaveLotInfo			(__in const ST_LOTInfo* pstLotInfo);
	BOOL	LoadLotInfo			(__out ST_LOTInfo& stLotInfo);

 	BOOL	SaveSelectedModel	(__in CString szModelFile, __in CString szModel);
 	BOOL	LoadSelectedModel	(__out CString& szModelFile, __out CString& szModel);

	BOOL	SaveYield			(__in const ST_Yield* pstYield);
	BOOL	LoadYield			(__out ST_Yield& stYield);

	// Password 2차
	BOOL	SavePassword		(__in UINT nIndex, __in CString szPassword);
	CString LoadPassword		(__in UINT nIndex);

	BOOL	SavePogoInfo		(__in const ST_PogoInfo* pstPogoInfo);
	BOOL	LoadPogoInfo		(__out ST_PogoInfo& stPogoInfo);

	BOOL	SaveSelectedMotor	(__in const CString szMotorFile);
	BOOL	LoadSelectedMotor	(__out CString& szMotorFile);

	// 유지 보수
	BOOL	SaveSelectMaintenance(__in CString szMaintenanceFile);
	BOOL	LoadSelectMaintenance(__out CString& szMaintenanceFile);
};

#endif // Reg_InspInfo_h__
