﻿//*****************************************************************************
// Filename	: Wnd_WorklistParticle.cpp
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#include "stdafx.h"
#include "Wnd_WorklistParticle.h"

#define	IDC_LIST_ARRAY			1201
#define	IDC_LIST_CHANNEL		1202

enum enWorklist
{
	IDC_LIST_TOTAL	= 1000,
	IDC_LIST_PARTICLE,
};

//=============================================================================
// CWnd_WorklistParticle
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_WorklistParticle, CWnd)

//=============================================================================
//
//=============================================================================
CWnd_WorklistParticle::CWnd_WorklistParticle()
{
}

CWnd_WorklistParticle::~CWnd_WorklistParticle()
{
}

BEGIN_MESSAGE_MAP(CWnd_WorklistParticle, CWnd)
	ON_WM_CREATE	()
	ON_WM_SIZE		()
	ON_NOTIFY		(NM_CLICK, IDC_LIST_ARRAY, OnNMClickListArray)
END_MESSAGE_MAP()


//=============================================================================
// CWnd_WorklistParticle 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_WorklistParticle::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
int CWnd_WorklistParticle::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_list_Array.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ARRAY);
	m_tc_Worklist.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, 1, CMFCTabCtrl::LOCATION_BOTTOM);

	for (UINT nCnt = 0; nCnt < TICnt_BlackSpot; nCnt++)
	{
		m_list_BlackSpot[nCnt].SetTestIndex(nCnt);
		m_list_BlackSpot[nCnt].Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Worklist, IDC_LIST_PARTICLE + nCnt);
		m_tc_Worklist.AddTab(&m_list_BlackSpot[nCnt], g_szLT_TestItemBlackSpot[nCnt], nCnt, FALSE);
	}
	
	m_tc_Worklist.EnableTabSwap(FALSE);
	m_tc_Worklist.SetActiveTab(0);

	return 0;
}

//=============================================================================
// Method		: CWnd_WorklistParticle::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
void CWnd_WorklistParticle::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin  = 5;
	int iSpacing = 5;

	int iLeft	= iMagrin;
	int iTop	= iMagrin;
	int iWidth	= cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	m_tc_Worklist.MoveWindow(iLeft, iTop, iWidth, iHeight);
}

//=============================================================================
// Method		: CWnd_WorklistParticle::PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
BOOL CWnd_WorklistParticle::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnNMClickListArray
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * result
// Qualifier	:
// Last Update	: 2016/8/12 - 16:05
// Desc.		:
//=============================================================================
void CWnd_WorklistParticle::OnNMClickListArray(NMHDR * pNMHDR, LRESULT * result)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	NM_LISTVIEW* pNMView = (NM_LISTVIEW*)pNMHDR;

	int index = pNMView->iItem;

	if (0 <= index)
	{
		ST_Worklist stWorklist;
	}
}

//=============================================================================
// Method		: InsertWorklist
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_Worklist * pstWorklist
// Qualifier	:
// Last Update	: 2016/8/12 - 16:56
// Desc.		:
//=============================================================================
void CWnd_WorklistParticle::InsertWorklist(__in const ST_Worklist* pstWorklist)
{
	m_list_Array.InsertWorklist(pstWorklist);

	int iCount = m_list_Array.GetItemCount();
	
	if (0 < iCount)
	{
		m_list_Array.SetSelectionMark(iCount - 1);
		m_list_Array.SetItemState(iCount - 1, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		m_list_Array.SetFocus();

		//m_list_Channel.SetWorklistChInfo(pstWorklist);
	}
}

//=============================================================================
// Method		: GetPtr_Worklist
// Access		: public  
// Returns		: void
// Parameter	: ST_Worklist & pWorklist
// Qualifier	:
// Last Update	: 2017/2/22 - 14:05
// Desc.		:
//=============================================================================
void CWnd_WorklistParticle::GetPtr_Worklist(ST_Worklist& pWorklist)
{
	for (int i = 0; i < TICnt_BlackSpot; i++)
		pWorklist.pList_Particle[i] = &m_list_BlackSpot[i];
}