﻿#ifndef List_Work_Current_h__
#define List_Work_Current_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_Curr_Worklist
{
	Curr_W_Recode,
	Curr_W_Time,
	Curr_W_Equipment,
	Curr_W_Model,
	Curr_W_SWVersion,
	Curr_W_LOTNum,
	Curr_W_Barcode,
	//Curr_W_Operator,
	Curr_W_Result,
	Curr_W_Current_1,
	Curr_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_Curr_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Result"),
	_T("mA"),
	NULL
};

const int	iListAglin_Curr_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_Curr_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};

// CList_Work_Current

class CList_Work_Current : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_Current)

public:
	CList_Work_Current();
	virtual ~CList_Work_Current();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // List_Work_Current_h__
