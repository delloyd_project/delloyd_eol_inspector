﻿//*****************************************************************************
// Filename	: 	Wnd_ManualCtrl.cpp
// Created	:	2017/1/4 - 13:32
// Modified	:	2017/1/4 - 13:32
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_ManualCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_ManualMotion.h"
#include "resource.h"

typedef enum ManualCtrl_ID
{
	IDC_BTN_ITEM = 1001,
};
// CWnd_Manual_Motion

IMPLEMENT_DYNAMIC(CWnd_Manual_Motion, CWnd)

CWnd_Manual_Motion::CWnd_Manual_Motion()
{
	m_pDevice	= NULL;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

}

CWnd_Manual_Motion::~CWnd_Manual_Motion()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Manual_Motion, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeCmds)
END_MESSAGE_MAP()

// CWnd_Manual_Motion message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/4 - 13:39
// Desc.		:
//=============================================================================
int CWnd_Manual_Motion::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Name.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Name.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Name.SetFont_Gdip(L"Arial", 10.0F);
	m_st_Name.Create(_T("Motion Control"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < BT_MMotion_MAXNUM; nIdx++)
	{
		m_Btn_Item[nIdx].Create(g_szManual_Motion_Button[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdx);
		m_Btn_Item[nIdx].SetFont(&m_font);
	}
	//m_Btn_Item[BT_MCTRL_IMAGE_LOAD].Create(g_szManual_Button[BT_MCTRL_IMAGE_LOAD], dwStyle | BS_PUSHLIKE | BS_AUTOCHECKBOX, rectDummy, this, IDC_BTN_MOTION_LOAD + BT_MCTRL_IMAGE_LOAD);
	//m_Btn_Item[BT_MCTRL_IMAGE_LOAD].SetFont(&m_font);
	//m_Btn_Item[BT_MCTRL_IMAGE_LOAD].SetImage(IDB_UNCHECKED_16);
	//m_Btn_Item[BT_MCTRL_IMAGE_LOAD].SetCheckedImage(IDB_CHECKED_16);
	//m_Btn_Item[BT_MCTRL_IMAGE_LOAD].SizeToContent();
	//m_Btn_Item[BT_MCTRL_IMAGE_LOAD].SetCheck(BST_UNCHECKED);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 19:39
// Desc.		:
//=============================================================================
void CWnd_Manual_Motion::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing = 0;
	int iCateSpacing = 3;
	int iLeft = iSpacing;
	int iTop = 0;
	int iWidth = cx;
	int iHeight = cy;
	int iTemp = 0;

	if ((BT_MMotion_MAXNUM + 0) % 2 == 1)
	{
		iTemp = (BT_MMotion_MAXNUM + 0) / 2 + 1;
	}
	else
	{
		iTemp = (BT_MMotion_MAXNUM + 0) / 2;
	}
	
	//iTemp += 1;

	iWidth = (iWidth - iSpacing) / 2;
	iHeight = (iHeight - iCateSpacing * iTemp) / iTemp;

	//m_Btn_Item[BT_MMotion_Load].MoveWindow(iLeft, iTop, iWidth, iHeight);
	//iLeft += iWidth + iSpacing;
	//m_Btn_Item[BT_MMotion_Inspect].MoveWindow(iLeft, iTop, iWidth, iHeight);

// 	iTop += iHeight + iCateSpacing;
// 	iLeft = iSpacing;
// 
// 	m_Btn_Item[BT_MCTRL_IRLED_ON].MoveWindow(iLeft, iTop, iWidth, iHeight);
// 	iLeft += iWidth + iSpacing;
// 	m_Btn_Item[BT_MCTRL_IRLED_OFF].MoveWindow(iLeft, iTop, iWidth, iHeight);

	//m_Btn_Item[BT_MMotion_Initialize].MoveWindow(iLeft, iTop, iWidth * 2, iHeight);

	//iTop += iHeight + iCateSpacing;
	iLeft = iSpacing;

	m_Btn_Item[BT_MMotion_Load].MoveWindow(iLeft, iTop, iWidth, iHeight);
	iLeft += iWidth + iSpacing;
	m_Btn_Item[BT_MMotion_Inspect].MoveWindow(iLeft, iTop, iWidth, iHeight);

	iTop += iHeight + iCateSpacing;
	iLeft = iSpacing;

	m_Btn_Item[BT_MMotion_Part_Load].MoveWindow(iLeft, iTop, iWidth, iHeight);
	iLeft += iWidth + iSpacing;
	m_Btn_Item[BT_MMotion_Part_Insp].MoveWindow(iLeft, iTop, iWidth, iHeight);
	
	iTop += iHeight + iCateSpacing;
	iLeft = iSpacing;
	m_Btn_Item[BT_MMotion_Cylinder_In].MoveWindow(iLeft, iTop, iWidth, iHeight);
	iLeft += iWidth + iSpacing;
	m_Btn_Item[BT_MMotion_Cylinder_Out].MoveWindow(iLeft, iTop, iWidth, iHeight);

	//iTop += iHeight + iCateSpacing;
	//iLeft = iSpacing;
	//m_Btn_Item[BT_MMotion_LED_On].MoveWindow(iLeft, iTop, iWidth, iHeight);
	//iLeft += iWidth + iSpacing;
	//m_Btn_Item[BT_MMotion_LED_Off].MoveWindow(iLeft, iTop, iWidth, iHeight);

	//iTop += iHeight + iCateSpacing;
	//iLeft = iSpacing;
	//m_Btn_Item[BT_MMotion_IR_On].MoveWindow(iLeft, iTop, iWidth, iHeight);
	//iLeft += iWidth + iSpacing;
	//m_Btn_Item[BT_MMotion_IR_Off].MoveWindow(iLeft, iTop, iWidth, iHeight);

	SetBtnState();
}

//=============================================================================
// Method		: OnRangeCmds
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/1/14 - 17:48
// Desc.		:
//=============================================================================
void CWnd_Manual_Motion::OnRangeCmds(UINT nID)
{
	UINT nIndex = nID - IDC_BTN_ITEM;
	ClickOut(nIndex);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/29 - 19:39
// Desc.		:
//=============================================================================
BOOL CWnd_Manual_Motion::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: ClickOut
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/1/14 - 17:44
// Desc.		:
//=============================================================================
void CWnd_Manual_Motion::ClickOut(UINT nID)
{
	for (UINT nIdx = 0; nIdx < BT_MMotion_MAXNUM; nIdx++)
	{
		m_Btn_Item[nIdx].EnableWindow(FALSE);
	}
	int nCheck = 0;
	//if (nID == BT_MCTRL_IMAGE_LOAD)
	//{
	//	nCheck = m_Btn_Item[BT_MCTRL_IMAGE_LOAD].GetCheck();
	//}
	//GetOwner()->SendMessage(WM_MANUAL_MOTION, nCheck , nID);

	AfxGetApp()->GetMainWnd()->SendNotifyMessage(m_wm_ManualID ,0 , nID + 1);

	//for (UINT nIdx = 0; nIdx < BT_MMotion_MAXNUM; nIdx++)
	//{
	//	m_Btn_Item[nIdx].EnableWindow(TRUE);
	//}

	//if (BT_MMotion_Cylinder_In == nID)
	//{
	//	m_Btn_Item[BT_MMotion_Cylinder_In].EnableWindow(FALSE);
	//}
	//else if (BT_MMotion_Cylinder_Out == nID)
	//{
	//	m_Btn_Item[BT_MMotion_Cylinder_Out].EnableWindow(FALSE);
	//}
	
	SetBtnState();
}

void CWnd_Manual_Motion::SetBtnState()
{
	if (m_bParticle_In == TRUE)
	{
		for (UINT nIdx = 0; nIdx < BT_MMotion_MAXNUM; nIdx++)
		{
			m_Btn_Item[nIdx].EnableWindow(TRUE);
		}

		m_Btn_Item[BT_MMotion_Cylinder_In].EnableWindow(FALSE);
		m_Btn_Item[BT_MMotion_Load].EnableWindow(FALSE);
		m_Btn_Item[BT_MMotion_Inspect].EnableWindow(FALSE);

		//!SH _190110: 이상태에서 모터 충돌 위치면 
		if (m_pDevice->MotionManager.GetCurrentPos(AX_StageDistance) > m_pDevice->MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End]
			&& m_pDevice->MotionManager.GetCurrentPos(AX_StageDistance) < m_pDevice->MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_Start])
		{
			m_Btn_Item[BT_MMotion_Cylinder_Out].EnableWindow(FALSE);
		}

		if (m_pDevice->MotionManager.GetCurrentPos(AX_StageDistance) < m_pDevice->MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End])
		{
			m_Btn_Item[BT_MMotion_Part_Load].EnableWindow(FALSE);
			m_Btn_Item[BT_MMotion_Part_Insp].EnableWindow(FALSE);
		}
		else
		{
			m_Btn_Item[BT_MMotion_Part_Load].EnableWindow(TRUE);
			m_Btn_Item[BT_MMotion_Part_Insp].EnableWindow(TRUE);

		}

	}
	else if (m_bParticle_Out == TRUE)
	{
		for (UINT nIdx = 0; nIdx < BT_MMotion_MAXNUM; nIdx++)
		{
			m_Btn_Item[nIdx].EnableWindow(TRUE);
		}

		m_Btn_Item[BT_MMotion_Cylinder_Out].EnableWindow(FALSE);
		m_Btn_Item[BT_MMotion_Part_Insp].EnableWindow(FALSE);

		//!SH _190110: 이상태에서 모터 충돌 위치면 
		if (m_pDevice->MotionManager.GetCurrentPos(AX_StageDistance) > m_pDevice->MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End]
			&& m_pDevice->MotionManager.GetCurrentPos(AX_StageDistance) < m_pDevice->MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_Start])
		{
			m_Btn_Item[BT_MMotion_Cylinder_In].EnableWindow(FALSE);
		}
	}
}


void CWnd_Manual_Motion::ImageLoadMode(bool bMode)
{
	//m_Btn_Item[BT_MCTRL_IMAGE_LOAD].SetCheck(bMode);
}