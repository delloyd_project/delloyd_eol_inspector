﻿#ifndef List_ParticleResult_h__
#define List_ParticleResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_ParticleResult
{
	ParticleResult_Object = 0,
	ParticleResult_Value,
	ParticleResult_Size,
	ParticleResult_ConcSpec,
	ParticleResult_SizeSpec,
	ParticleResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_ParticleResult[] =
{
	_T(""),
	_T("Concentration"),
	_T("Size"),
	_T("Concentration Spec"),
	_T("Size Spec"),
	NULL
};

typedef enum enListItemNum_ParticleResult
{
	ParticleResult_ItemNum = 1,
};

static LPCTSTR	g_lpszItem_ParticleResult[] =
{
	_T("PASS"),
	_T("FAIL"),
	NULL
};

const int	iListAglin_ParticleResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_ParticleResult[] =
{
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};
// List_ParticleInfo

class CList_ParticleResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_ParticleResult)

public:
	CList_ParticleResult();
	virtual ~CList_ParticleResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Particle(ST_LT_TI_BlackSpot* pstParticle)
	{
		if (pstParticle == NULL)
			return;

		m_pstParticle = pstParticle;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_BlackSpot*  m_pstParticle;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_ParticleInfo_h__
