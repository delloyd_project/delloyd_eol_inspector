﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_Model.h
// Created	:	2016/3/14 - 10:56
// Modified	:	2016/3/14 - 10:56
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_Model_h__
#define Wnd_Cfg_Model_h__

#pragma once
#include "Wnd_BaseView.h"
#include "Def_Enum.h"
#include "VGStatic.h"
#include "Def_DataStruct.h"
#include "File_WatchList.h"

enum enModel_Static
{
	STI_ML_ModelName = 0,
	STI_ML_Cam_Volt,
	//STI_ML_Cam_Volt2,
	//STI_ML_Cam_CoverUse,
	//STI_ML_Grab_Type,
	STI_ML_Cam_Type,
	//STI_ML_Video_Type,
	//STI_ML_I2C_File_1,
	//STI_ML_I2C_File_2,
	STI_ML_Cam_Delay,
	STI_ML_Cam_Width,
	STI_ML_Cam_Height,
	//STI_ML_Cam_Distortion,
	STI_ML_TestX,
	STI_ML_TestY,
	STI_ML_Test_Distance,
	STI_ML_UseOpenShort,
	STI_ML_MAXNUM,
};

static LPCTSTR	g_szModel_Static[] =
{
	_T("MODEL"),
	_T("Voltage Camera"),
	//_T("Voltage LED"),
	//_T("Cover Use"),
	//_T("Grabber Type"),
	_T("Camera Type"),
	//_T("Camera State"),
	//_T("NTSC / PAL"),
	//_T("I2C Setting (Cam On)"),
	//_T("I2C Setting (Cam Off)"),
	_T("Camera Delay"),
	_T("Camera Width"),
	_T("Camera Height"),
	//_T("Distortion"),
	_T("Test Axis X [mm]"),
	_T("Test Axis Y [mm]"),
	_T("Test Distance [mm]"),
	_T("Use OpenShort Test"),
	NULL
};

enum enModel_Combobox
{
	//COB_ML_GrabType = 0,
	COB_ML_CameraType = 0,
	COB_ML_UseOpenShort,
	//COB_ML_VideoType,
	//COB_ML_I2CFile_1,
	//COB_ML_I2CFile_2,
	//COB_ML_CurtainType,
	COB_ML_MAXNUM,
};

enum enModel_Edit
{
	EDT_ML_ModelName = 0,
	EDT_ML_Voltage,
	//EDT_ML_Voltage2,
	EDT_ML_CameraDelay,
	EDT_ML_CameraWidth,
	EDT_ML_CameraHeight,
	EDT_ML_TestX,
	EDT_ML_TestY,
	EDT_ML_TestDistance,
	EDT_ML_MAXNUM,
};


enum enModel_Button
{
	//BTN_ML_Distortion,
	//BTN_ML_IIC_Check1,
	//BTN_ML_IIC_Check2,
	BTN_ML_MAXNUM = 1,
};

//-----------------------------------------------------------------------------
// CWnd_Cfg_Model
//-----------------------------------------------------------------------------
class CWnd_Cfg_Model : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_Model)

public:
	CWnd_Cfg_Model();
	virtual ~CWnd_Cfg_Model();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	afx_msg void	OnBnClickedBnDistortionSet();

	CFont			m_font;
	CString			m_szI2CPath;

	CVGStatic		m_st_Item[STI_ML_MAXNUM];
	CComboBox		m_cb_Item[COB_ML_MAXNUM];
	CMFCButton		m_bn_Item[BTN_ML_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[EDT_ML_MAXNUM];

	CFile_WatchList	m_IniWatch;


	void		GetUIData			(__out ST_ModelInfo& stModelInfo);
	void		SetUIData			(__in const ST_ModelInfo* pModelInfo);
	void		RefreshFileList		(__in UINT nComboID, __in const CStringList* pFileList);

public:
	
	// 파일이 있는 경로 설정
	void	SetPath	(__in LPCTSTR szI2CPath)
	{
		m_szI2CPath = szI2CPath;
	};

	// 모델 데이터를 UI에 표시
	void		SetModelInfo		(__in const ST_ModelInfo* pModelInfo);
	// UI에 표시된 데이터의 구조체 반환	
	void		GetModelInfo		(__out ST_ModelInfo& stModelInfo);
};

#endif // Wnd_Cfg_Model_h__


