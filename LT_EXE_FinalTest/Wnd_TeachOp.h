﻿//*****************************************************************************
// Filename	: 	Wnd_TeachOp.h
// Created	:	2017/03/28 - 13:47
// Modified	:	2017/03/28 - 13:47
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************

#pragma once
#include "VGStatic.h"
#include "MotionManager.h"
#include "Def_Motion.h"

#include "File_Maintenance.h"

enum enTeachnOpButton
{
	BN_TC_SAVE = 0,
	//BN_TC_Loading,
	//BN_TC_Inspection,
	//BN_TC_Par_Loading,
	//BN_TC_Par_Cylinder,
	//BN_TC_Par_Inspection,
	BN_TC_MAXNUM,
};

static LPCTSTR g_szTeachOpButton[] =
{
	_T("SAVE"),
	//_T("MOVE Load"),
	//_T("MOVE Inspect"),
	//_T("Move Particle Load"),
	//_T("Move Particle Cylinder"),
	//_T("Move Particle Inspect"),
	NULL
};

// CWnd_TeachOp
class CWnd_TeachOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_TeachOp)

public:
	CWnd_TeachOp();
	virtual ~CWnd_TeachOp();

	CMotionManager*	m_pstDevice;
	ST_MaintenanceInfo*		m_pstMaintenanceInfo;

	void SetPtr_MaintenanceInfo(__in ST_MaintenanceInfo* pstMaintInfo)
	{
		if (pstMaintInfo == NULL)
			return;

		m_pstMaintenanceInfo = pstMaintInfo;
	}

	void GetPtr_MaintenanceInfo(__out ST_MaintenanceInfo& stMaintInfo)
	{
		stMaintInfo = *m_pstMaintenanceInfo;
	}

	void SetPtr_Device(__in CMotionManager* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
	};

	enInsptrSysType m_nSysType;
	void SetSystemType(__in enInsptrSysType nSysType)
	{
		m_nSysType = nSysType;
	
		SetUpdateItem();
	};

	void	SetUpdateData	();

protected:

	CVGStatic				m_st_Comm;

	UINT					m_nItemCnt;
	CVGStatic				m_st_Name;
	CFont					m_font;
	CVGStatic				m_st_Item[TC_ALL_MAX];
	CMFCMaskedEdit			m_ed_Item[TC_ALL_MAX];
	CMFCButton				m_bn_Item[BN_TC_MAXNUM];
	CString					m_szTeacOpName[TC_ALL_MAX];
	ST_MotorInfo*			m_pstMotorInfo;
	CFile_Maintenance		m_fileMaintenance;

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedBnSave	();
	/*afx_msg void    OnBnClickedBn_Loading();
	afx_msg void    OnBnClickedBn_Inspection();
	afx_msg void    OnBnClickedBn_Par_Loading();
	afx_msg void    OnBnClickedBn_Par_Inspection();
	afx_msg void    OnBnClickedBn_Par_Cylinder();*/
	virtual BOOL	PreTranslateMessage	(MSG* pMsg);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	
	void	SetUpdateItem	();
	void	GetUpdateData	();
	
	DECLARE_MESSAGE_MAP()
};