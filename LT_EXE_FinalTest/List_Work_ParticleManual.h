﻿#ifndef List_Work_ParticleManual_h__
#define List_Work_ParticleManual_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_ParticleManual_Worklist
{
	ParticleManual_W_Recode,
	ParticleManual_W_Time,
	ParticleManual_W_Equipment,
	ParticleManual_W_Model,
	ParticleManual_W_SWVersion,
	ParticleManual_W_LOTNum,
	ParticleManual_W_Barcode,
	//ParticleManual_W_Operator,
	ParticleManual_W_Result,
	ParticleManual_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_ParticleManual_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Result"),
	NULL
};

const int	iListAglin_ParticleManual_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_ParticleManual_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};

// CList_Work_ParticleManual

class CList_Work_ParticleManual : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_ParticleManual)

public:
	CList_Work_ParticleManual();
	virtual ~CList_Work_ParticleManual();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // List_Work_ParticleManual_h__
