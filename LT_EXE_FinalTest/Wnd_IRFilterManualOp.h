﻿#ifndef Wnd_IRFilterManualOp_h__
#define Wnd_IRFilterManualOp_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "Def_DataStruct.h"

// CWnd_IRFilterManualOp

enum enIRFilterManualButton
{
	BTN_IRMANUAL_TEST = 0,
	BTN_IRMANUAL_MAX,
};

static LPCTSTR	g_szIRFilterManualButton[] =
{
	_T("TEST"),
	NULL
};

enum enIRFilterManualComobox
{
	CMB_IRMANUAL_MAX = 1,
};


class CWnd_IRFilterManualOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_IRFilterManualOp)

public:
	CWnd_IRFilterManualOp();
	virtual ~CWnd_IRFilterManualOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo		*m_pstModelInfo;

	CFont				m_font;
	
	CButton				m_bn_Item[BTN_IRMANUAL_MAX];

	// 검사 항목이 다수 인경우
	UINT				m_nTestItemCnt;

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};

#endif // Wnd_IRFilterManualOp_h__
