﻿#ifndef List_Work_IRFilter_h__
#define List_Work_IRFilter_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_IRFilter_Worklist
{
	IRFilter_W_Recode,
	IRFilter_W_Time,
	IRFilter_W_Equipment,
	IRFilter_W_Model,
	IRFilter_W_SWVersion,
	IRFilter_W_LOTNum,
	IRFilter_W_Barcode,
	//IRFilter_W_Operator,
	IRFilter_W_Result,
	IRFilter_W_Value,
	IRFilter_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_IRFilter_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	//_T("Operator"),
	_T("Result"),
	_T("Value"),

	NULL
};

const int	iListAglin_IRFilter_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_IRFilter_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};

// CList_Work_IRFilter

class CList_Work_IRFilter : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_IRFilter)

public:
	CList_Work_IRFilter();
	virtual ~CList_Work_IRFilter();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;

	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	};

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
};

#endif // List_Work_IRFilter_h__
