﻿#ifndef Wnd_BaseFFTOp_h__
#define Wnd_BaseFFTOp_h__

#pragma once

#include "Wnd_FFTOp.h"

// CWnd_BaseFFTOp

class CWnd_BaseFFTOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseFFTOp)

public:
	CWnd_BaseFFTOp();
	virtual ~CWnd_BaseFFTOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
	//CWnd_FFTOp m_wndFFTOp[TICnt_FFT];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetPath(__in LPCTSTR szWavPath)
	{
		//for (int i = 0; i < TICnt_FFT; i++)
		//{
		//	m_wndFFTOp[i].SetPath(szWavPath);
		//}
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseFFTOp_h__
