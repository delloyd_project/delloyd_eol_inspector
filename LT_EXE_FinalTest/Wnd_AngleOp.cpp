﻿// Wnd_AngleOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_AngleOp.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_AngleOp
typedef enum AngleOptID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_AngleOp, CWnd)

CWnd_AngleOp::CWnd_AngleOp()
{
	m_pstModelInfo	= NULL;
	m_nTestItemCnt = 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_AngleOp::~CWnd_AngleOp()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_AngleOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_AngleOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_AngleOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_ANG_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szAngleStatic[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < BTN_ANG_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szAngleButton[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_ANG_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_ANG_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
		//m_cb_Item[nIdex].InsertString(nIdex, _T(""));
	}
		
	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);
	m_ListSub.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 1);
	

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_AngleOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iIdxCnt = AngOp_ItemNum;
	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	int iHeaderH = 40;
	int iList_H  = iHeaderH + iIdxCnt * 15;

	int iCtrl_W = iWidth / 4;
	int iSTHeight = 25;

	m_st_Item[STI_AUTO_SET].MoveWindow(iLeft, iTop, iCtrl_W, iSTHeight);

	iLeft += iCtrl_W + 1;
	m_ed_Item[EDT_AUTO_SET].MoveWindow(iLeft, iTop, iCtrl_W, iSTHeight);

	iLeft = cx - iMargin - iCtrl_W;
	m_bn_Item[BTN_ANG_TEST].MoveWindow(iLeft, iTop, iCtrl_W, iSTHeight);

	//
	iLeft = cx - iMargin - iCtrl_W;
	iTop += iSpacing + iSTHeight;
	m_bn_Item[BTN_AUTO_SET].MoveWindow(iLeft, iTop, iCtrl_W, iSTHeight);

	iLeft = iMargin;
	iTop += iSpacing + iSTHeight;
	m_st_Item[STI_ANG_ROI].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	iLeft = iMargin;
	iTop += 2 + iSTHeight;
	m_List.MoveWindow(iLeft, iTop, iWidth, iList_H);

	iTop	+= iSpacing + iList_H;
	iIdxCnt = AngSOp_ItemNum;
	iList_H = iHeaderH + iIdxCnt * 15;

	m_st_Item[STI_ANG_OPT].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	if (iIdxCnt <= 0 || iList_H > iHeight - iTop)
	{
		iList_H = iHeight;
	}

	iTop += 2 + iSTHeight;
	m_ListSub.MoveWindow(iLeft, iTop, iWidth, iList_H);
	iTop += iList_H + iSpacing;

}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_AngleOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_AngleOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (NULL == m_pstModelInfo)
		return;

	if (TRUE == bShow)
	{
		m_pstModelInfo->nTestMode = TIID_FOV;
		m_pstModelInfo->nTestCnt = m_nTestItemCnt;
		m_pstModelInfo->nPicItem = PIC_Angle;
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_AngleOp::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
	m_pstModelInfo->stAngle->bTestMode = TRUE;
	switch (nIdex)
	{
	case BTN_ANG_TEST:
		GetOwner()->SendMessage(WM_MANUAL_TEST, m_nTestItemCnt, TIID_FOV);
		break;

	case BTN_AUTO_SET:
		GetUpdateData();
		GetOwner()->SendMessage(WM_AUTO_SETTING, m_nTestItemCnt, TIID_FOV);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_AngleOp::SetUpdateData()
{
	if (m_pstModelInfo == NULL)
		return;

	m_List.SetPtr_Angle(&m_pstModelInfo->stAngle[m_nTestItemCnt]);
	m_List.SetImageSize(m_pstModelInfo->dwWidth, m_pstModelInfo->dwHeight);
	m_List.InsertFullData();

	m_ListSub.SetPtr_Angle(&m_pstModelInfo->stAngle[m_nTestItemCnt]);
	m_ListSub.InsertFullData();

	CString strValue;
	strValue.Format(_T("%d"), m_pstModelInfo->stAngle[m_nTestItemCnt].stAngleOpt.nSpecDev);
	m_ed_Item[EDT_AUTO_SET].SetWindowText(strValue);
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/14 - 18:01
// Desc.		:
//=============================================================================
void CWnd_AngleOp::GetUpdateData()
{
	if (m_pstModelInfo == NULL)
		return;

	CString strValue;
	m_ed_Item[EDT_AUTO_SET].GetWindowText(strValue);
	m_pstModelInfo->stAngle[m_nTestItemCnt].stAngleOpt.nSpecDev = _ttoi(strValue);
}