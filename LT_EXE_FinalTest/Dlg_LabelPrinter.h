#pragma once
#include "resource.h"
#include "Def_TestDevice.h"
#include "Wnd_Cfg_LabelPrinter.h"

// CDlg_LabelPrinter 대화 상자입니다.

class CDlg_LabelPrinter : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_LabelPrinter)

public:
	CDlg_LabelPrinter(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_LabelPrinter();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_LABEL_PRINTER };

	void SetPtr_Device(__in ST_Device* pDevice)
	{
		if (pDevice == NULL)
			return;

		m_pDevice = pDevice;
	};

	// 모델 데이터를 UI에 표시
	void SetPath(__in LPCTSTR szPrnFilePath);
	void SetModelInfo(ST_ModelInfo* pstModelInfo);
	void GetModelInfo();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()

	ST_Device* m_pDevice;
	ST_ModelInfo* m_pstModelInfo;
	CString m_szPrnPath;

	CWnd_Cfg_LabelPrinter m_wndLabelPrinter;

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnClose();
};
