﻿// List_Work_Color.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_Work_Color.h"

// CList_Work_Color
IMPLEMENT_DYNAMIC(CList_Work_Color, CListCtrl)

CList_Work_Color::CList_Work_Color()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Work_Color::~CList_Work_Color()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_Work_Color, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CList_Work_Color 메시지 처리기입니다.
int CList_Work_Color::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	InitHeader();
	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/2/22 - 12:44
// Desc.		:
//=============================================================================
void CList_Work_Color::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
BOOL CList_Work_Color::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_Color::InitHeader()
{
	for (int nCol = 0; nCol < Col_W_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_Col_Worklist[nCol], iListAglin_Col_Worklist[nCol], iHeaderWidth_Col_Worklist[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const  ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_Color::InsertFullData(__in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	int iNewCount = GetItemCount();

	InsertItem(iNewCount, _T(""));
	SetRectRow(iNewCount, pstCamInfo);
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: __in const  ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_Color::SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	CString strText;

	strText.Format(_T("%s"), pstCamInfo->szIndex);
	SetItemText(nRow, Col_W_Recode, strText);

	strText.Format(_T("%s"), pstCamInfo->szTime);
	SetItemText(nRow, Col_W_Time, strText);

	strText.Format(_T("%s"), pstCamInfo->szEquipment);
	SetItemText(nRow, Col_W_Equipment, strText);

	strText.Format(_T("%s"), pstCamInfo->szModelName);
	SetItemText(nRow, Col_W_Model, strText);

	strText.Format(_T("%s"), pstCamInfo->szSWVersion);
	SetItemText(nRow, Col_W_SWVersion, strText);

	strText.Format(_T("%s"), pstCamInfo->szLotID);
	SetItemText(nRow, Col_W_LOTNum, strText);

	strText.Format(_T("%s"), pstCamInfo->szBarcode);
	SetItemText(nRow, Col_W_Barcode, strText);

	//strText.Format(_T("%s"), pstCamInfo->szOperatorName);
	//SetItemText(nRow, Col_W_Operator, strText);
 
// 	strText.Format(_T("%s"), pstCamInfo->szCamType);
// 	SetItemText(nRow, Col_W_CameraType, strText);

	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stColor[m_nTestIndex].stColorResult.nResult].szText);
	SetItemText(nRow, Col_W_Result, strText);

	if (pstCamInfo->stColor[m_nTestIndex].stColorResult.nResult == TER_Init)
	{
		for (UINT i = 0; i < ROI_CL_Max; i++)
		{
			strText.Format(_T("X"));
			SetItemText(nRow, Col_W_RedR + (3 * i), strText);

			strText.Format(_T("X"));
			SetItemText(nRow, Col_W_RedG + (3 * i), strText);

			strText.Format(_T("X"));
			SetItemText(nRow, Col_W_RedB + (3 * i), strText);
		}
	}
	else
	{
		for (UINT i = 0; i < ROI_CL_Max; i++)
		{
			strText.Format(_T("%d"), pstCamInfo->stColor[m_nTestIndex].stColorResult.iRed[i]);
			SetItemText(nRow, Col_W_RedR + (3 * i), strText);

			strText.Format(_T("%d"), pstCamInfo->stColor[m_nTestIndex].stColorResult.iGreen[i]);
			SetItemText(nRow, Col_W_RedG + (3 * i), strText);

			strText.Format(_T("%d"), pstCamInfo->stColor[m_nTestIndex].stColorResult.iBlue[i]);
			SetItemText(nRow, Col_W_RedB + (3 * i), strText);
		}
	}
}

//=============================================================================
// Method		: GetData
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: UINT & DataNum
// Parameter	: CString * Data
// Qualifier	:
// Last Update	: 2017/2/22 - 12:47
// Desc.		:
//=============================================================================
void CList_Work_Color::GetData(UINT nRow, UINT &DataNum, CString *Data)
{
	DataNum = Col_W_MaxCol;
	CString temp[Col_W_MaxCol];
	for (int t = 0; t < Col_W_MaxCol; t++)
	{
		temp[t] = GetItemText(nRow, t);
		Data[t] = GetItemText(nRow, t);
	}
}
