﻿//*****************************************************************************
// Filename	: Wnd_MainView.h
// Created	: 2016/03/11
// Modified	: 2016/03/11
//
// Author	: PiRing
//	
// Purpose	: 기본 화면용 윈도우
//*****************************************************************************
#ifndef Wnd_MainView_h__
#define Wnd_MainView_h__

#pragma once

#include "VGStatic.h"
#include "Grid_ModelInfo.h"
#include "Wnd_BaseView.h"
#include "Wnd_SiteInfo.h"
#include "Wnd_TestInfo.h"
#include "Wnd_ModelInfo.h"
#include "Wnd_LotInfo.h"
#include "Def_TestDevice.h"
//#include "Dlg_MasterMode.h"
#include "Wnd_MasterSet.h"

//=============================================================================
// CWnd_MainView
//=============================================================================
class CWnd_MainView : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_MainView)

public:
	CWnd_MainView();
	virtual ~CWnd_MainView();

protected:
	DECLARE_MESSAGE_MAP()

	virtual void	MoveWindow_Status(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);

	// 검사 결과 판정
	CVGStatic			m_st_Judgment;

	// 데이터
	enPermissionMode	m_InspMode;
	ST_InspectionInfo*	m_pstInspInfo;
	ST_Device*			m_pDevice;

	CWnd_TestInfo		m_wnd_TestInfo;
	//CDlg_MasterMode		m_wnd_MasterSet;
	CWnd_ModelInfo		m_wnd_ModelInfo;
	CWnd_LotInfo		m_wnd_LotInfo;
	CWnd_MasterSet		m_wnd_MasterSet;

	CVGStatic			m_stAlramMsg;

public:
	
	CWnd_SiteInfo		m_wnd_SiteInfo;
	
	void	SetPtrInspectionInfo		(__inout ST_InspectionInfo* pstInspInfo)
	{
		if (pstInspInfo == NULL)
			return;

		m_pstInspInfo = pstInspInfo;
	};

	void	SetPtr_Device				(__in ST_Device* pDevice)
	{
		if (pDevice == NULL)
			return;

		m_pDevice = pDevice;
	};

	// 검사 모드 설정
	void	SetInspectionMode			(enPermissionMode InspMode);
	void	SetMasterMode				(__in BOOL bMode);

	// 설비 구동 모드
	void		SetOperateMode(__in enOperateMode nOperMode);

	// UI 갱신
	void	UpdateModelInfo				();
	void	UpdateLotInfo				();
	void	UpdateMESInfo				();
	void	UpdateYield					();
	void	UpdatePogoCount				();
	void	UpdatePogoCnt				();
	void	UpdateMasterInfo			(__in int iOffsetX, __in int iOffsetY, __in double dbDegree);
	void	UpdateSetResult				(__in enTestEachResult Result, __in CString strText);
	// ----------------------------------------------------------------------
	// 검사별 상세 정보 
	// ----------------------------------------------------------------------

	// 검사 시간
	void	UpdateElapsedTime			(__in DWORD	dwTime);			

	// 검사 항목 결과
	void	UpdateTestResult			(__in enTestEachResult EachResult, __in UINT nStepIdx, __in CString strText);	

	// 검사 최종 판단
	void	UpdateTestJudgment			(__in enTestEachResult EachResult);	

	// 검사 UI 초기화
	void	UpdateResetResult			();

	// 알람 메세지
	void	UpdateAlarmMessage			(CString szText);

	void	OnSetBtnLotStartEnable		(__in BOOL bEnable);
	void	OnSetBtnLotStopEnable		(__in BOOL bEnable);
	void	OnSetBtnStartEnable			(__in BOOL bEnable);
	void	OnSetBtnStopEnable			(__in BOOL bEnable);
	//void	OnSetBtnMasterTestEnable	(__in BOOL bEnable);
	void	OnSetBtnMasterModeEnable(__in BOOL bEnable);

	BOOL	OnGetBtnStartEnable			();
	BOOL	OnGetBtnStopEnable			();

	void	InsertBarcode				(__in LPCTSTR szBarcode);
};

#endif // Wnd_MainView_h__
