﻿// Wnd_ResetOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"

#include "Wnd_ResetOp.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_ResetOp
typedef enum ResetOptID
{
	IDC_BTN_ITEM   = 1001,
	IDC_CMB_ITEM   = 2001,
	IDC_EDT_ITEM   = 3001,
	IDC_LIST_ITEM  = 4001,
	IDC_BTN_INDEX  = 5001,
};

IMPLEMENT_DYNAMIC(CWnd_ResetOp, CWnd)

CWnd_ResetOp::CWnd_ResetOp()
{
	m_pstModelInfo	= NULL;
	m_nTestItemCnt = 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_ResetOp::~CWnd_ResetOp()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_ResetOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_ResetOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_ResetOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_RESET_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szResetStatic[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < BTN_Reset_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szResetButton[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_RESET_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_ResetOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

//	int iIdxCnt = PtOp_ItemNum;
	int iMargin = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	int iHeaderH = 40;
	int iCtrl_W = iWidth / 5;
	int iCtrl_H = 25;

	int iSTWidth = iWidth / 5;
	int iSTHeight = 25;

	for (UINT nIdx = 0; nIdx < STI_RESET_MAX; nIdx++)
	{
		iLeft = iMargin;
		m_st_Item[STI_RESET_DELAY + nIdx].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

		iTop += iCtrl_H + iSpacing;
	}

	iTop = iMargin;
	for (UINT nIdx = 0; nIdx < EDT_RESET_MAX; nIdx++)
	{
		iLeft = iSTWidth + iMargin + 1;
		m_ed_Item[EDT_RESET_DELAY + nIdx].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

		iTop += iCtrl_H + iSpacing;
	}

	iTop = iMargin;
	iLeft = cx - iMargin - iSTWidth;
	m_bn_Item[BTN_Reset_TEST].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_ResetOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_ResetOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (NULL == m_pstModelInfo)
		return;

	if (TRUE == bShow)
	{
		//m_pstModelInfo->nTestMode = TIID_Reset;
		m_pstModelInfo->nTestCnt = m_nTestItemCnt;
		m_pstModelInfo->nPicItem = PIC_Reset;
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_ResetOp::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;

	switch (nIdex)
	{
	case BTN_Reset_TEST:
		//GetOwner()->SendMessage(WM_MANUAL_TEST, m_nTestItemCnt, TIID_Reset);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_ResetOp::SetUpdateData()
{
	//if (m_pstModelInfo == NULL)
	//	return;

	//CString strValue;
	//strValue.Format(_T("%d"), m_pstModelInfo->stReset[m_nTestItemCnt].nDelay);
	//m_ed_Item[EDT_RESET_DELAY].SetWindowText(strValue);
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Parameter	: __out ST_TestItemOpt & stTestItemOpt
// Qualifier	:
// Last Update	: 2017/10/14 - 18:07
// Desc.		:
//=============================================================================
void CWnd_ResetOp::GetUpdateData()
{
	//if (m_pstModelInfo == NULL)
	//	return;

	//CString strValue;
	//m_ed_Item[EDT_RESET_DELAY].GetWindowText(strValue);
	//m_pstModelInfo->stReset[m_nTestItemCnt].nDelay = _ttoi(strValue);
}
