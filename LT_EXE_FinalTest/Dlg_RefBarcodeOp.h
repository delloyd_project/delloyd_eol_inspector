#pragma once
#include "resource.h"
#include "Wnd_Cfg_BarcodeConfig.h"

// CDlg_RefBarcodeOp 대화 상자입니다.

class CDlg_RefBarcodeOp : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_RefBarcodeOp)

public:
	CDlg_RefBarcodeOp(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_RefBarcodeOp();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_REFBARCODE_OP };

	CMFCButton m_bn_Save;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg void	OnBnClickedBnSave();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
	CWnd_Cfg_BarcodeConfig m_wndBarcodeConfig;
	CString m_szBarcodeOpPath;
	CString m_szRefBarcode;

	ST_BarcodeRefInfo* m_pstBarcodeInfo;

public:

	void SetPtr_BarcodeInfo(__in ST_BarcodeRefInfo*  pstBarcodeInfo)
	{
		if (pstBarcodeInfo == NULL)
			return;

		m_pstBarcodeInfo = pstBarcodeInfo;
	};

	void SetReferenceSeperator(__in CString szRefBarcode)
	{
		m_szRefBarcode = szRefBarcode;
	};

	void Set_BarcodeInfo(__in const ST_BarcodeRefInfo* pstBarcodeInfo);
	void Get_BarcodeInfo(__out ST_BarcodeRefInfo& stBarcodeInfo);
	void SetPath(__in LPCTSTR szModelPath, __in LPCTSTR szBarcodePath);

};
