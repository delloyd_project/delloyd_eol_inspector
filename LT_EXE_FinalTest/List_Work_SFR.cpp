﻿// List_Work_SFR.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_Work_SFR.h"


// CList_Work_SFR

IMPLEMENT_DYNAMIC(CList_Work_SFR, CListCtrl)

CList_Work_SFR::CList_Work_SFR()
{

	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Work_SFR::~CList_Work_SFR()
{
	m_Font.DeleteObject();
}


BEGIN_MESSAGE_MAP(CList_Work_SFR, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CList_Work_SFR 메시지 처리기입니다.
int CList_Work_SFR::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	InitHeader();
	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

void CList_Work_SFR::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);
}

BOOL CList_Work_SFR::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

UINT CList_Work_SFR::Header_MaxNum()
{
	return (UINT)SFR_W_MaxCol;
}

void CList_Work_SFR::InitHeader()
{
	for (int nCol = 0; nCol < SFR_W_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_SFR_Worklist[nCol], iListAglin_SFR_Worklist[nCol], iHeaderWidth_SFR_Worklist[nCol]);
	}
}

void CList_Work_SFR::InsertFullData(__in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	int iNewCount = this->GetItemCount();

	InsertItem(iNewCount, _T(""));
	SetRectRow(iNewCount, pstCamInfo);
}

void CList_Work_SFR::SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	CString strText;

	strText.Format(_T("%s"), pstCamInfo->szIndex);
	SetItemText(nRow, SFR_W_Recode, strText);

	strText.Format(_T("%s"), pstCamInfo->szTime);
	SetItemText(nRow, SFR_W_Time, strText);

	strText.Format(_T("%s"), pstCamInfo->szEquipment);
	SetItemText(nRow, SFR_W_Equipment, strText);

	strText.Format(_T("%s"), pstCamInfo->szModelName);
	SetItemText(nRow, SFR_W_Model, strText);

	strText.Format(_T("%s"), pstCamInfo->szSWVersion);
	SetItemText(nRow, SFR_W_SWVersion, strText);

	strText.Format(_T("%s"), pstCamInfo->szLotID);
	SetItemText(nRow, SFR_W_LOTNum, strText);

	strText.Format(_T("%s"), pstCamInfo->szBarcode);
	SetItemText(nRow, SFR_W_Barcode, strText);

	strText.Format(_T("%s"), pstCamInfo->szOperatorName);
	SetItemText(nRow, SFR_W_Operator, strText);

	strText.Format(_T("%s"), pstCamInfo->szCamType);
	SetItemText(nRow, SFR_W_CameraType, strText);

	//strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stSFR[m_nTestIndex].stSFRResult.nResult].szText);
	//SetItemText(nRow, SFR_W_Result, strText);

	//if (pstCamInfo->stSFR[m_nTestIndex].stSFRResult.nResult == TER_Init)
	//{
	//	for (UINT i = 0; i < ROI_SFR_Max; i++)
	//	{
	//		strText.Format(_T("X"));
	//		SetItemText(nRow, SFR_W_Data + i, strText);
	//	}
	//}
	//else
	//{
	//	for (UINT i = 0; i < ROI_SFR_Max; i++)
	//	{
	//		strText.Format(_T("%0.2f"), pstCamInfo->stSFR[m_nTestIndex].stSFRResult.dbValue[i]);
	//		SetItemText(nRow, SFR_W_Data + i, strText);
	//	}
	//}
}

void CList_Work_SFR::GetData(UINT nRow, UINT &DataNum, CString *Data)
{
	DataNum = SFR_W_MaxCol;
	CString temp[SFR_W_MaxCol];
	for (int t = 0; t < SFR_W_MaxCol; t++)
	{
		temp[t] = GetItemText(nRow, t);
		Data[t] = GetItemText(nRow, t);
	}
}
