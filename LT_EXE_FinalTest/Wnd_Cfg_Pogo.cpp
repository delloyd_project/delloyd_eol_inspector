﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_Pogo.cpp
// Created	:	2016/10/31 - 21:02
// Modified	:	2016/10/31 - 21:02
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************

#include "stdafx.h"
#include "Wnd_Cfg_Pogo.h"
#include "Reg_InspInfo.h"

#define		IDC_CB_FILE				1001
#define		IDC_BN_SAVE				1002
#define		IDC_ED_CNT_MAX			1100
#define		IDC_ED_COUNT			1200
#define		IDC_BN_RESET_CNT		1300
#define		IDC_BN_RESET_CNT_E		1300 + USE_CHANNEL_CNT

#define		IDC_ED_DAY_REG			1400
#define		IDC_ED_DAY_CNT			1500
#define		IDC_BN_DAY_REG			1600
#define		IDC_BN_DAY_REG_E		1700 + USE_CHANNEL_CNT

#define		IDC_BN_APPLY			1804
#define		IDC_BN_CANCEL			1805

#define		WM_REFRESH_POGO			WM_USER + 201

//-----------------------------------------------------------------------------
// CWnd_Cfg_Pogo
//-----------------------------------------------------------------------------
IMPLEMENT_DYNAMIC(CWnd_Cfg_Pogo, CWnd_BaseView)

CWnd_Cfg_Pogo::CWnd_Cfg_Pogo()
{
	VERIFY(m_font_Default.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_pstPogoCnt = NULL;
}

CWnd_Cfg_Pogo::~CWnd_Cfg_Pogo()
{
	m_font_Default.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_Pogo, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_CBN_SELENDOK	(IDC_CB_FILE,	OnCbnSelendokFile)
	ON_BN_CLICKED	(IDC_BN_SAVE,	OnBnClickedBnSave)
	ON_COMMAND_RANGE(IDC_BN_RESET_CNT, IDC_BN_RESET_CNT_E, OnBnClickedReset)
	ON_COMMAND_RANGE(IDC_BN_DAY_REG, IDC_BN_DAY_REG_E, OnBnClickedRegisterationDate)
	ON_MESSAGE		(WM_REFRESH_POGO,	OnRefreshFileList)
END_MESSAGE_MAP()


// CWnd_Cfg_Pogo message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
int CWnd_Cfg_Pogo::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_File.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_File.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_File.SetFont_Gdip(L"Arial", 9.0F);

	m_st_File.Create(_T("Pogo File"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_cb_File.Create(dwStyle | CBS_DROPDOWNLIST | CBS_SORT, rectDummy, this, IDC_CB_FILE);
	m_bn_NewFile.Create(_T("Add New File"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SAVE);


	CString szText;
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_stCount_Max[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_stCount_Max[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_stCount_Max[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_stCount[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_stCount[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_stCount[nIdx].SetFont_Gdip(L"Arial", 9.0F);

		m_st_DayConfig[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_DayConfig[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_DayConfig[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_DayCount[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_DayCount[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_DayCount[nIdx].SetFont_Gdip(L"Arial", 9.0F);

		szText.Format(_T(" Socket %d: Max Count"), nIdx + 1);
		m_stCount_Max[nIdx].Create(szText, dwStyle, rectDummy, this, IDC_STATIC);
		szText.Format(_T(" Socket %d: Count"), nIdx + 1);
		m_stCount[nIdx].Create(szText, dwStyle, rectDummy, this, IDC_STATIC);

		szText.Format(_T(" Registration Date"), nIdx + 1);
		m_st_DayConfig[nIdx].Create(szText, dwStyle, rectDummy, this, IDC_STATIC);
		szText.Format(_T(" Date Count"), nIdx + 1);
		m_st_DayCount[nIdx].Create(szText, dwStyle, rectDummy, this, IDC_STATIC);

		m_ed_Count_Max[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER | ES_NUMBER, rectDummy, this, IDC_ED_CNT_MAX + nIdx);
		m_ed_Count[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER | ES_NUMBER, rectDummy, this, IDC_ED_COUNT + nIdx);

		m_ed_DayConfig[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER | ES_READONLY, rectDummy, this, IDC_ED_DAY_REG + nIdx);
		m_ed_DayCount[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER | ES_NUMBER, rectDummy, this, IDC_ED_DAY_CNT + nIdx);

		m_bn_ResetCount[nIdx].Create(_T("Reset"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_RESET_CNT + nIdx);
		m_bn_ConfigDay[nIdx].Create(_T("Registration"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_DAY_REG + nIdx);

		m_ed_Count_Max[nIdx].SetFont(&m_font_Default);
		m_ed_Count[nIdx].SetFont(&m_font_Default);

		m_ed_DayConfig[nIdx].SetFont(&m_font_Default);
		m_ed_DayCount[nIdx].SetFont(&m_font_Default);
	}

	m_bn_Cancel.Create(_T("Cancel"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_CANCEL);
	m_cb_File.SetFont(&m_font_Default);


	// 파일 감시 쓰레드 설정
	//	m_IniWatch.SetOwner(GetSafeHwnd(), WM_REFRESH_POGO);
	if (!m_strPogoPath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_strPogoPath, POGO_FILE_EXT);
		//m_IniWatch.BeginWatchThrFunc();

		m_IniWatch.RefreshList();

		RefreshFileList(m_IniWatch.GetFileList());

		if (m_szPogoFile.IsEmpty())
		{
			m_cb_File.SetCurSel(0);
		}
	}

	// 기본 데이터
	CString strFile;
	m_cb_File.GetWindowText(strFile);
	SetPogoFile(strFile);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_Pogo::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iMagrin;
	int iTop = iMagrin;
	int iSubTop = 0;
	int iSub2Top = 0;
	int iSub3Top = 0;
	int iSubLeft = 0;
	
	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin;

	int iCtrlHeight = 26;
	int iCapWidth	= 140;
	int Combo_W = (int)(iCapWidth * 1.5);

	int iTempWidth	= 0;

	m_st_File.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	
	iLeft += iCapWidth + iSpacing;
	m_cb_File.MoveWindow(iLeft, iTop, Combo_W, iCtrlHeight);

	iLeft += Combo_W + iSpacing;
	m_bn_NewFile.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
 
 	iLeft = iMagrin;
 	iTop += iCtrlHeight + iSpacing;
 
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{	
		iSubTop = iTop + iCtrlHeight + iSpacing;
		iSub2Top = iSubTop + iCtrlHeight + iSpacing;
		iSub3Top = iSub2Top + iCtrlHeight + iSpacing;

		m_stCount_Max[nIdx].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_stCount[nIdx].MoveWindow(iLeft, iSubTop, iCapWidth, iCtrlHeight);
		//m_st_DayConfig[nIdx].MoveWindow(iLeft, iSub2Top, iCapWidth, iCtrlHeight);
		//m_st_DayCount[nIdx].MoveWindow(iLeft, iSub3Top, iCapWidth, iCtrlHeight);

		iSubLeft = iLeft + iCapWidth + iSpacing;
		m_ed_Count_Max[nIdx].MoveWindow(iSubLeft, iTop, Combo_W, iCtrlHeight);
		m_ed_Count[nIdx].MoveWindow(iSubLeft, iSubTop, Combo_W, iCtrlHeight);
		//m_ed_DayConfig[nIdx].MoveWindow(iSubLeft, iSub2Top, Combo_W, iCtrlHeight);
		//m_ed_DayCount[nIdx].MoveWindow(iSubLeft, iSub3Top, Combo_W, iCtrlHeight);

		iSubLeft += Combo_W + iSpacing;
		m_bn_ResetCount[nIdx].MoveWindow(iSubLeft, iTop, iCapWidth, iCtrlHeight * 2 + iSpacing);
		//m_bn_ConfigDay[nIdx].MoveWindow(iSubLeft, iSub2Top, iCapWidth, iCtrlHeight * 2 + iSpacing);

		iTop += iCtrlHeight + iCateSpacing;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Pogo::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Pogo::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class

	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2016/10/31 - 22:38
// Desc.		:
//=============================================================================
void CWnd_Cfg_Pogo::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_BaseView::OnShowWindow(bShow, nStatus);

	if (bShow)
	{
		LoadIniFile(m_szPogoFile);
	}
}

//=============================================================================
// Method		: OnCbnSelendokFile
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/6 - 11:01
// Desc.		:
//=============================================================================
void CWnd_Cfg_Pogo::OnCbnSelendokFile()
{
	// 선택된 파일 로드
	CString szFile;

	int iSel = m_cb_File.GetCurSel();
	if (0 <= iSel)
	{
		m_cb_File.GetWindowText(szFile);

		SelectEndPogoFile(szFile);
	}
}

//=============================================================================
// Method		: OnBnClickedBnSave
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/6 - 11:01
// Desc.		:
//=============================================================================
void CWnd_Cfg_Pogo::OnBnClickedBnSave()
{
	// UI에서 데이터 구함
	CString szText;
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_ed_Count_Max[nIdx].GetWindowText(szText);
		m_pstPogoCnt->dwCount_Max[nIdx] = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();

		m_ed_Count[nIdx].GetWindowText(szText);
		m_pstPogoCnt->dwCount[nIdx] = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();
	}

	// 파일 다른이름으로 저장
	CString strFullPath;
	CString strFullTitle;
	CString strFileExt;
	strFileExt.Format(_T("Ini File (*.%s)| *.%s||"), POGO_FILE_EXT, POGO_FILE_EXT);

	CFileDialog fileDlg(FALSE, POGO_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = m_strPogoPath;

	if (fileDlg.DoModal() == IDOK)
	{
		strFullPath = fileDlg.GetPathName();
		strFullTitle = fileDlg.GetFileTitle();

		if (m_fileModel.SavePogoIniFile(strFullPath, m_pstPogoCnt))
		{
			// 리스트 모델 갱신
			m_szPogoFile = strFullTitle;
			m_IniWatch.RefreshList();
			RefreshFileList(m_IniWatch.GetFileList());
		}
	}
}

//=============================================================================
// Method		: OnBnClickedReset
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2016/10/31 - 21:45
// Desc.		:
//=============================================================================
void CWnd_Cfg_Pogo::OnBnClickedReset(UINT nID)
{
	UINT nIndex = nID - IDC_BN_RESET_CNT;
	
	m_ed_Count[nIndex].SetWindowText(_T("0"));
}

void CWnd_Cfg_Pogo::OnBnClickedRegisterationDate(UINT nID)
{
	UINT nIndex = nID - IDC_BN_DAY_REG;

	// 현재 날짜 등록 함수

	SYSTEMTIME RegistrationTime;
	GetLocalTime(&RegistrationTime);

	CString szText;

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_pstPogoCnt->dwYear[nIdx] = RegistrationTime.wYear;
		m_pstPogoCnt->dwMonth[nIdx] = RegistrationTime.wMonth;
		m_pstPogoCnt->dwDay[nIdx] = RegistrationTime.wDay;
		m_pstPogoCnt->dwHour[nIdx] = RegistrationTime.wHour;
		m_pstPogoCnt->dwMinute[nIdx] = RegistrationTime.wMinute;
		m_pstPogoCnt->dwSecond[nIdx] = RegistrationTime.wSecond;
		m_pstPogoCnt->dwMilliseconds[nIdx] = RegistrationTime.wMilliseconds;

		szText.Format(_T("%04d-%02d-%02d %02d:%02d:%02d:%02d"),
			m_pstPogoCnt->dwYear[nIdx],
			m_pstPogoCnt->dwMonth[nIdx],
			m_pstPogoCnt->dwDay[nIdx],
			m_pstPogoCnt->dwHour[nIdx],
			m_pstPogoCnt->dwMinute[nIdx],
			m_pstPogoCnt->dwSecond[nIdx],
			m_pstPogoCnt->dwMilliseconds[nIdx]);

		m_ed_DayConfig[nIdx].SetWindowText(szText);

		m_pstPogoCnt->dwDateCount[nIdx] = 30;
		szText.Format(_T("%d"), m_pstPogoCnt->dwDateCount[nIdx]);
		m_ed_DayCount[nIdx].SetWindowText(szText);
	}

	SavePogoSetting();
}

//=============================================================================
// Method		: OnRefreshFileList
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/1/6 - 11:01
// Desc.		:
//=============================================================================
LRESULT CWnd_Cfg_Pogo::OnRefreshFileList(WPARAM wParam, LPARAM lParam)
{
	RefreshFileList(m_IniWatch.GetFileList());

	return 0;
}

//=============================================================================
// Method		: LoadIniFile
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFile
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Pogo::LoadIniFile(__in LPCTSTR szFile)
{
	CString strFilePath;

	strFilePath.Format(_T("%s%s.%s"), m_strPogoPath, szFile, POGO_FILE_EXT);

	if (m_fileModel.LoadPogoIniFile(strFilePath, *m_pstPogoCnt))
	{
		// 화면에 표시
		CString szText;
		for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
		{
			szText.Format(_T("%d"), m_pstPogoCnt->dwCount_Max[nIdx]);
			m_ed_Count_Max[nIdx].SetWindowText(szText);

			szText.Format(_T("%d"), m_pstPogoCnt->dwCount[nIdx]);
			m_ed_Count[nIdx].SetWindowText(szText);

			szText.Format(_T("%d"), m_pstPogoCnt->dwDateCount[nIdx]);
			m_ed_DayCount[nIdx].SetWindowText(szText);

			szText.Format(_T("%04d-%02d-%02d %02d:%02d:%02d:%02d"), 
				m_pstPogoCnt->dwYear[nIdx], 
				m_pstPogoCnt->dwMonth[nIdx], 
				m_pstPogoCnt->dwDay[nIdx],
				m_pstPogoCnt->dwHour[nIdx],
				m_pstPogoCnt->dwMinute[nIdx],
				m_pstPogoCnt->dwSecond[nIdx],
				m_pstPogoCnt->dwMilliseconds[nIdx]);

			m_ed_DayConfig[nIdx].SetWindowText(szText);

		}
		GetOwner()->SendNotifyMessage(WM_UPDATE_POGO_CNT, 0, 0);

		return TRUE;
	}
	return FALSE;
}

//=============================================================================
// Method		: SaveIniFile
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFile
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Pogo::SaveIniFile(__in LPCTSTR szFile)
{
	CString strFilePath;

	strFilePath.Format(_T("%s%s.%s"), m_strPogoPath, szFile, POGO_FILE_EXT);

	// UI에서 데이터 구함
	CString szText;
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_ed_Count_Max[nIdx].GetWindowText(szText);
		m_pstPogoCnt->dwCount_Max[nIdx] = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();

		m_ed_Count[nIdx].GetWindowText(szText);
		m_pstPogoCnt->dwCount[nIdx] = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();

		m_ed_DayCount[nIdx].GetWindowText(szText);
		m_pstPogoCnt->dwDateCount[nIdx] = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();
	}

	GetOwner()->SendNotifyMessage(WM_UPDATE_POGO_CNT, 0, 0);
	return m_fileModel.SavePogoIniFile(strFilePath, m_pstPogoCnt);
}

//=============================================================================
// Method		: SelectEndPogoFile
// Access		: protected  
// Returns		: void
// Parameter	: __in LPCTSTR szFile
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
void CWnd_Cfg_Pogo::SelectEndPogoFile(__in LPCTSTR szFile)
{
	// 파일 로드
	if (LoadIniFile(szFile))
	{
		m_szPogoFile = szFile;
	}
}

//=============================================================================
// Method		: RefreshFileList
// Access		: public  
// Returns		: void
// Parameter	: __in const CStringList * pFileList
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
void CWnd_Cfg_Pogo::RefreshFileList(__in const CStringList* pFileList)
{
	m_cb_File.ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();

	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_cb_File.AddString(pFileList->GetNext(pos));
	}

	// 이전에 선택되어있는 파일 다시 선택
	if (!m_szPogoFile.IsEmpty())
	{
		int iSel = m_cb_File.FindStringExact(0, m_szPogoFile);

		if (0 <= iSel)
		{
			m_cb_File.SetCurSel(iSel);
		}
	}
}

//=============================================================================
// Method		: SetPogoFile
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szFile
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
void CWnd_Cfg_Pogo::SetPogoFile(__in LPCTSTR szFile)
{
	int iSel = m_cb_File.FindStringExact(0, szFile);

	if (0 <= iSel)
	{
		m_cb_File.SetCurSel(iSel);

		SelectEndPogoFile(szFile);
	}
}

//=============================================================================
// Method		: GetPogoInfo
// Access		: public  
// Returns		: ST_PogoInfo
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
ST_PogoInfo CWnd_Cfg_Pogo::GetPogoInfo()
{
	CString szText;
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_ed_Count_Max[nIdx].GetWindowText(szText);
		m_pstPogoCnt->dwCount_Max[nIdx] = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();

		m_ed_Count[nIdx].GetWindowText(szText);
		m_pstPogoCnt->dwCount[nIdx] = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();

		m_ed_DayCount[nIdx].GetWindowText(szText);
		m_pstPogoCnt->dwDateCount[nIdx] = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();
	}

	m_pstPogoCnt->szPogoFilename = m_szPogoFile;

	return *m_pstPogoCnt;
}

//=============================================================================
// Method		: GetPogoName
// Access		: public  
// Returns		: CString
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
CString CWnd_Cfg_Pogo::GetPogoName()
{
	return m_szPogoFile;
}

//=============================================================================
// Method		: SavePogoSetting
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Pogo::SavePogoSetting()
{
	CString szFile;
	m_cb_File.GetWindowText(szFile);

	return SaveIniFile(szFile);
}
