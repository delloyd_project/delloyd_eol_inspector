﻿// List_ReverseSubOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_ReverseSubOp.h"

#define IRtOp_ED_CELLEDIT			5001
#define IRtOp_CB_CELLCOMBO_TYPE		5002

// CList_ReverseSubOp

IMPLEMENT_DYNAMIC(CList_ReverseSubOp, CListCtrl)

CList_ReverseSubOp::CList_ReverseSubOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
	
	m_nWidth	= 720;
	m_nHeight	= 480;

	m_pstReverse = NULL;
}

CList_ReverseSubOp::~CList_ReverseSubOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_ReverseSubOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_ReverseSubOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_ReverseSubOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IRtOp_ED_CELLEDIT, &CList_ReverseSubOp::OnEnKillFocusEdit)
	ON_CBN_KILLFOCUS(IRtOp_CB_CELLCOMBO_TYPE, &CList_ReverseSubOp::OnEnKillFocusCombo)
	ON_CBN_SELCHANGE(IRtOp_CB_CELLCOMBO_TYPE, &CList_ReverseSubOp::OnEnSelectFocusCombo)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_ReverseSubOp 메시지 처리기입니다.
int CList_ReverseSubOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IRtOp_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);
	

	m_cb_Type.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IRtOp_CB_CELLCOMBO_TYPE);
	m_cb_Type.ResetContent();

	for (UINT nIdex = 0; NULL != g_szReverseRGB[nIdex]; nIdex++)
	{
		m_cb_Type.InsertString(nIdex, g_szReverseRGB[nIdex]);

	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ReverseSubOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[ReverseSubOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = ReverseSubOp_LeftTop; nCol < ReverseSubOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() - iHeaderWidth_ReverseSubOp[ReverseSubOp_Object]) / (ReverseSubOp_MaxCol - ReverseSubOp_LeftTop);
		SetColumnWidth(nCol, iUnitWidth);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ReverseSubOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ReverseSubOp::InitHeader()
{
	for (int nCol = 0; nCol < ReverseSubOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_ReverseSubOp[nCol], iListAglin_ReverseSubOp[nCol], iHeaderWidth_ReverseSubOp[nCol]);
	}

	for (int nCol = 0; nCol < ReverseSubOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_ReverseSubOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_ReverseSubOp::InsertFullData()
{
	if (m_pstReverse == NULL)
		return;

 	DeleteAllItems();
 

	for (UINT nIdx = 0; nIdx < ReverseSubOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_ReverseSubOp::SetRectRow(UINT nRow)
{
	if (m_pstReverse == NULL)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_szcamstate[nRow]);
	SetItemText(nRow, ReverseSubOp_Object, strValue);

	for (int i = 0; i < ReverseRGB_Max; i++)
	{
		strValue.Format(_T("%s"), g_szReverseRGB[m_pstReverse->stReverseOpt.stRegionSubOp[nRow].nRGBType[i]]);
		SetItemText(nRow, ReverseSubOp_LeftTop + i, strValue);
	}

}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_ReverseSubOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_ReverseSubOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < ReverseSubOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_cb_Type.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_cb_Type.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_cb_Type.SetFocus();
			m_cb_Type.SetCurSel(m_pstReverse->stReverseOpt.stRegionSubOp[m_nEditRow].nRGBType[m_nEditCol - 1]);
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_ReverseSubOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_ReverseSubOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (m_pstReverse == NULL)
		return FALSE;


	return TRUE;
}

//=============================================================================
// Method		: UpdateCelldbData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/10/14 - 17:46
// Desc.		:
//=============================================================================
BOOL CList_ReverseSubOp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	CString str;
	str.Format(_T("%.1f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ReverseSubOp::GetCellData()
{
	if (m_pstReverse == NULL)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ReverseSubOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue  = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120)*0.1);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dbValue)
				dbValue = dbValue + ((zDelta / 120)*0.1);
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		if (dbValue > 2.0)
			dbValue = 2.0;
		
		UpdateCellData(m_nEditRow, m_nEditCol, iValue);

	}
	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}


void CList_ReverseSubOp::OnEnSelectFocusCombo()
{
	m_pstReverse->stReverseOpt.stRegionSubOp[m_nEditRow].nRGBType[m_nEditCol - 1] = m_cb_Type.GetCurSel();
}


void CList_ReverseSubOp::OnEnKillFocusCombo()
{

	SetItemText(m_nEditRow, m_nEditCol, g_szReverseRGB[m_pstReverse->stReverseOpt.stRegionSubOp[m_nEditRow].nRGBType[m_nEditCol - 1]]);
	m_cb_Type.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

