﻿// List_ReverseResult.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_ReverseResult.h"

// CList_ReverseResult



IMPLEMENT_DYNAMIC(CList_ReverseResult, CListCtrl)

CList_ReverseResult::CList_ReverseResult()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
	
	m_nWidth	= 720;
	m_nHeight	= 480;

	m_pstReverse	= NULL;
}

CList_ReverseResult::~CList_ReverseResult()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_ReverseResult, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_ReverseResult::OnNMClick)
	ON_WM_MOUSEWHEEL()
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &CList_ReverseResult::OnNMCustomdraw)
END_MESSAGE_MAP()

// CList_ReverseResult 메시지 처리기입니다.
int CList_ReverseResult::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ReverseResult::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol < ReverseResult_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, rectClient.Width() / ReverseResult_MaxCol);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ReverseResult::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ReverseResult::InitHeader()
{
	for (int nCol = 0; nCol < ReverseResult_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_ReverseResult[nCol], iListAglin_ReverseResult[nCol], iHeaderWidth_ReverseResult[nCol]);
	}

	for (int nCol = 0; nCol < ReverseResult_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_ReverseResult[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_ReverseResult::InsertFullData()
{
	if (m_pstReverse == NULL)
		return;

 	DeleteAllItems();
 

	for (UINT nIdx = 0; nIdx < ReverseResult_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_ReverseResult::SetRectRow(UINT nRow)
{
	if (m_pstReverse == NULL)
		return;

	CString strValue;

// 	strValue.Format(_T("%d"), nRow);
// 	SetItemText(nRow, ReverseResult_Object, strValue);
	strValue.Format(_T("%d"), nRow + 1);
	SetItemText(nRow, ReverseResult_Object, strValue);

	if (m_pstReverse->stReverseResult.nCamState == 4)
		strValue = _T("Unknown");
	else
	strValue.Format(_T("%s"), g_szcamstate[m_pstReverse->stReverseResult.nCamState]);
	//strValue.Format(_T("%s"), g_szcamstate[m_pstReverse->stReverseResult.nCamState]);
	SetItemText(nRow, ReverseResult_CamState, strValue);

	strValue.Format(_T("%s"), g_szcamstate[m_pstReverse->stReverseOpt.nStateType]);
	SetItemText(nRow, ReverseResult_MasterCamState, strValue);

	strValue.Format(_T("%s"), g_lpszItem_ReverseResult[m_pstReverse->stReverseResult.nResult]);
	SetItemText(nRow, ReverseResult_Result, strValue);

}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_ReverseResult::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	m_pstReverse->stReverseResult.iSelectROI = pNMItemActivate->iItem;
	m_pstReverse->stReverseOpt.iSelectROI = -1;

	*pResult = 0;
}


void CList_ReverseResult::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVCUSTOMDRAW  lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	int nRow = 0, nSub = 0;
	switch (lplvcd->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;          // 아이템외에 일반적으로 처리하는 부분
		lplvcd->clrTextBk = RGB(0, 0, 255);
		break;
	case CDDS_ITEMPREPAINT:                          // 행 아이템에 대한 처리를 할 경우
		*pResult = CDRF_NOTIFYSUBITEMDRAW;
		break;
	case CDDS_ITEMPREPAINT | CDDS_SUBITEM:  // 행과 열 아이템에 대한 처리를 할 경우
		nRow = (int)lplvcd->nmcd.dwItemSpec;         // 행 인덱스를 가져옴
		nSub = (int)lplvcd->iSubItem;                       // 열 인덱스를 가져옴

		if (nSub >= ReverseResult_CamState)
		{
			lplvcd->clrTextBk = RGB(250, 236, 197);           // 해당 행, 열 아이템의 배경색을 지정한다.
			lplvcd->clrText = RGB(0, 0, 0);                      // 해당 행, 열 아이템의 글자색을 지정한다.
		}

		if (nSub > ReverseResult_CamState)
		{
			lplvcd->clrTextBk = RGB(255, 255, 255);           // 해당 행, 열 아이템의 배경색을 지정한다.
			lplvcd->clrText = RGB(0, 0, 0);                      // 해당 행, 열 아이템의 글자색을 지정한다.
		}


		if (nSub == ReverseResult_Result)
		{

			if (m_pstReverse->stReverseResult.nResult == TRUE)
			{
				lplvcd->clrTextBk = RGB(0, 0, 255);           // 해당 행, 열 아이템의 배경색을 지정한다.
				lplvcd->clrText = RGB(255, 255, 255);                      // 해당 행, 열 아이템의 글자색을 지정한다.
			}
			else
			{
				lplvcd->clrTextBk = RGB(255, 0, 0);           // 해당 행, 열 아이템의 배경색을 지정한다.
				lplvcd->clrText = RGB(255, 255, 255);                      // 해당 행, 열 아이템의 글자색을 지정한다.
			}
		}

		break;
	default:
		*pResult = CDRF_DODEFAULT;

		break;
	}
}
