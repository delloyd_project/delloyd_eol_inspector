﻿#ifndef List_Work_Particle_h__
#define List_Work_Particle_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_Par_Worklist
{
	Par_W_Recode,
	Par_W_Time,
	Par_W_Equipment,
	Par_W_Model,
	Par_W_SWVersion,
	Par_W_LOTNum,
	Par_W_Barcode,
	Par_W_Operator,
	Par_W_Result,
	Par_W_Count,
	Par_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_Par_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("LOT ID"),
	_T("Barcode"),
	_T("Operator"),
	_T("Result"),
	_T("Count"),
	NULL,							
};

const int	iListAglin_Par_Worklist[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_Par_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};
// CList_Work_Particle

class CList_Work_Particle : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_Particle)

public:
	CList_Work_Particle();
	virtual ~CList_Work_Particle();
	CFont		m_Font;

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize				(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow	(CREATESTRUCT& cs);

	UINT Header_MaxNum();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);
	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);

	void GetData		(UINT nRow, UINT &DataNum, CString *Data);


	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

};


#endif // List_Work_Particle_h__
