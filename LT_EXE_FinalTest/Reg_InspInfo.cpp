﻿//*****************************************************************************
// Filename	: 	Reg_InspInfo.cpp
// Created	:	2016/3/31 - 16:33
// Modified	:	2016/3/31 - 16:33
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Reg_InspInfo.h"

#define  DEF_INI_FILE _T("initmodel.ini")

CReg_InspInfo::CReg_InspInfo()
{
	m_strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);
}

CReg_InspInfo::CReg_InspInfo(__in LPCTSTR lpszRegPath)
{
	m_strRegPath = lpszRegPath;
}

CReg_InspInfo::~CReg_InspInfo()
{
}

//=============================================================================
// Method		: SaveLotInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in const ST_LOTInfo * pstLotInfo
// Qualifier	:
// Last Update	: 2016/7/22 - 11:24
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::SaveLotInfo(__in const ST_LOTInfo* pstLotInfo)
{
	CString		strValue;
	CString		strRegPath;
	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (!m_Reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		m_Reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		strValue = pstLotInfo->szLotName;
		m_Reg.WriteString(_T("LotName"), strValue.GetBuffer());
		strValue.ReleaseBuffer();

		strValue = pstLotInfo->szOperator;
		m_Reg.WriteString(_T("Operator"), strValue.GetBuffer());
		strValue.ReleaseBuffer();
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

//=============================================================================
// Method		: LoadLotInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __out ST_LOTInfo & stLotInfo
// Qualifier	:
// Last Update	: 2016/7/22 - 11:24
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::LoadLotInfo(__out ST_LOTInfo& stLotInfo)
{
	DWORD		dwValue = 0;
	CString		strValue;
	CString		strRegPath;
	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		if (m_Reg.ReadString(_T("LotName"), strValue))
			stLotInfo.szLotName = strValue;

		if (m_Reg.ReadString(_T("Operator"), strValue))
			stLotInfo.szOperator = strValue;
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

//=============================================================================
// Method		: SaveSelectedModel
// Access		: public  
// Returns		: BOOL
// Parameter	: __in CString szModelFile
// Parameter	: __in CString szModel
// Qualifier	:
// Last Update	: 2016/11/24 - 15:39
// Desc.		:
//=============================================================================
 BOOL CReg_InspInfo::SaveSelectedModel(__in CString szModelFile, __in CString szModel)
 {
 	CString		strValue;
 	CString		strRegPath;
 	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);
 
 	if (!m_Reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
 	{
 		m_Reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
 	}
 
 	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
 	{
 		strValue = szModelFile;
 		m_Reg.WriteString(_T("SelectedModelFile"), strValue.GetBuffer());
 
 		strValue = szModel;
 		m_Reg.WriteString(_T("Set_Model"), strValue.GetBuffer());
 	}
 	else
 	{
 		return FALSE;
 	}
 
 	m_Reg.Close();
 
 	
 
 	return TRUE;
 }

//=============================================================================
// Method		: LoadSelectedModel
// Access		: public  
// Returns		: BOOL
// Parameter	: __out CString & szModelFile
// Parameter	: __out CString & szModel
// Qualifier	:
// Last Update	: 2016/11/24 - 15:38
// Desc.		:
//=============================================================================
 BOOL CReg_InspInfo::LoadSelectedModel(__out CString& szModelFile, __out CString& szModel)
 {
 	DWORD		dwValue = 0;
 	CString		strValue;
 	CString		strRegPath;
 	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);
 
 	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
 	{
 		if (m_Reg.ReadString(_T("SelectedModelFile"), strValue))
 			szModelFile = strValue;
 		else
 			szModelFile = _T("Default");
 
 		if (m_Reg.ReadString(_T("Set_Model"), strValue))
 			szModel = strValue;
 		else
 			szModel = _T("Default");//Default
 	}
 	else
 	{
 		return FALSE;
 	}
 
 	m_Reg.Close();
 
 	return TRUE;
 }

//=============================================================================
// Method		: SaveYield
// Access		: public  
// Returns		: BOOL
// Parameter	: __in const ST_Yield * pstYield
// Qualifier	:
// Last Update	: 2016/6/1 - 21:08
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::SaveYield(__in const ST_Yield* pstYield)
{
	CString		strValue;
	DWORD		dwValue;
	CString		strRegPath;
	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (!m_Reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		m_Reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		dwValue = pstYield->dwPass;
		m_Reg.WriteDWORD(_T("Pass"), dwValue);


		dwValue = pstYield->dwFail;
		m_Reg.WriteDWORD(_T("Fail"), dwValue);
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

BOOL CReg_InspInfo::LoadYield(__out ST_Yield& stYield)
{
	DWORD		dwValue = 0;
	CString		strValue;
	CString		strRegPath;
	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		if (m_Reg.ReadDWORD(_T("Pass"), dwValue))
			stYield.dwPass = dwValue;
		else
			stYield.dwPass = 0;
		
		if (m_Reg.ReadDWORD(_T("Fail"), dwValue))
			stYield.dwFail = dwValue;
		else
			stYield.dwFail = 0;
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

//=============================================================================
// Method		: SavePassword
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nIndex
// Qualifier	:
// Last Update	: 2016/6/25 - 15:58
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::SavePassword(__in UINT nIndex, __in CString szPassword)
{
	CRegistry	reg;
	if (reg.Open(HKEY_CURRENT_USER, REG_PATH_OPTION_BASE))
	{
		//Password
		if (0 == nIndex)
			reg.WriteString(_T("Password"), szPassword);
		else
			reg.WriteString(_T("Password_2"), szPassword);

		reg.Close();

		return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: LoadPassword
// Access		: public  
// Returns		: CString
// Parameter	: __in UINT nIndex
// Qualifier	:
// Last Update	: 2016/6/25 - 16:03
// Desc.		:
//=============================================================================
CString CReg_InspInfo::LoadPassword(__in UINT nIndex)
{
	CRegistry	reg;
	CString		strValue;
	if (reg.Open(HKEY_CURRENT_USER, REG_PATH_OPTION_BASE))
	{
		if (0 == nIndex)
			reg.ReadString(_T("Password"), strValue);
		else
			reg.ReadString(_T("Password_2"), strValue);

		reg.Close();
	}

	return strValue;
}

//=============================================================================
// Method		: SavePogoInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in const ST_PogoInfo * pstPogoInfo
// Qualifier	:
// Last Update	: 2016/10/31 - 22:44
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::SavePogoInfo(__in const ST_PogoInfo* pstPogoInfo)
{
	CString		strValue;
	DWORD		dwValue;
	CString		strRegPath;
	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (!m_Reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		m_Reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	CString szKey;
	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			szKey.Format(_T("MaxCount_%d"), nIdx + 1);
			dwValue = pstPogoInfo->dwCount_Max[nIdx];
			m_Reg.WriteDWORD(szKey, dwValue);

			szKey.Format(_T("Count_%d"), nIdx + 1);
			dwValue = pstPogoInfo->dwCount[nIdx];
			m_Reg.WriteDWORD(szKey, dwValue);
		}
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

//=============================================================================
// Method		: LoadPogoInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __out ST_PogoInfo & stPogoInfo
// Qualifier	:
// Last Update	: 2016/10/31 - 22:44
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::LoadPogoInfo(__out ST_PogoInfo& stPogoInfo)
{
	DWORD		dwValue = 0;
	CString		strValue;
	CString		strRegPath;
	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	CString szKey;
	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			szKey.Format(_T("MaxCount_%d"), nIdx + 1);
			if (m_Reg.ReadDWORD(szKey, dwValue))
				stPogoInfo.dwCount_Max[nIdx] = dwValue;

			szKey.Format(_T("Count_%d"), nIdx + 1);
			if (m_Reg.ReadDWORD(szKey, dwValue))
				stPogoInfo.dwCount[nIdx] = dwValue;
		}
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

//=============================================================================
// Method		: SaveSelectedMotor
// Access		: public  
// Returns		: BOOL
// Parameter	: __in const CString szMotorFile
// Qualifier	:
// Last Update	: 2017/8/12 - 18:24
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::SaveSelectedMotor(__in const CString szMotorFile)
{
	CString		strValue;
	CString		strRegPath;
	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (!m_Reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		m_Reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		strValue = szMotorFile;
		m_Reg.WriteString(_T("SelectedMotorFile"), strValue.GetBuffer());
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();
	return TRUE;
}

//=============================================================================
// Method		: LoadSelectedMotor
// Access		: public  
// Returns		: BOOL
// Parameter	: __out CString & szMotorFile
// Qualifier	:
// Last Update	: 2017/8/12 - 18:25
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::LoadSelectedMotor(__out CString& szMotorFile)
{
	CString		strValue;
	CString		strRegPath;

	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		if (m_Reg.ReadString(_T("SelectedMotorFile"), strValue))
			szMotorFile = strValue;
		else
			szMotorFile = _T("Default");
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

BOOL CReg_InspInfo::SaveSelectMaintenance(CString szMaintenanceFile)
{
	CString		strValue;
	CString		strRegPath;

	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (!m_Reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		m_Reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		strValue = szMaintenanceFile;
		m_Reg.WriteString(_T("SelectedMaintenanceFile"), strValue.GetBuffer());
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

BOOL CReg_InspInfo::LoadSelectMaintenance(CString & szMaintenanceFile)
{
	CString		strValue;
	CString		strRegPath;

	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		if (m_Reg.ReadString(_T("SelectedMaintenanceFile"), strValue))
			szMaintenanceFile = strValue;
		else
			szMaintenanceFile = _T("Default");
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}
