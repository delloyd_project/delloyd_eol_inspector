﻿//*****************************************************************************
// Filename	: 	Def_Test.h
// Created	:	2016/06/30
// Modified	:	2016/08/08
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_Test_h__
#define Def_Test_h__

#include "Def_Enum.h"
#include "Def_Test_Cm.h"
#include "Def_TestItem.h"
#include "VGStatic.h"

//msec 측정 라이브러리 추가
#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

#define CAM_MAX_NUM			1




// 결과 코드 전체 검사
enum enResultCode_AF
{
	RCAF_UnknownError = 0,
	RCAF_OK,
	RCAF_Exception,
	RCAF_TestRunning,
	RCAF_Model_Err,
	RCAF_EMO,
	RCAF_MaxPogoCount,
	RCAF_AreaSensor,
	RCAF_Model_Empty_Err,
	RCAF_LotID_Empty_Err,
	RCAF_Operator_Empty_Err,
	RCAF_Barcode_Empty_Err,
	RCAF_ForcedStop,
	RCAF_PowerOn_Err,
	RCAF_PowerOff_Err,
	RCAF_Current_Err,
	RCAF_CameraBd_Err,
	RCAF_TimeOut_Err,
	RCAF_NoImage_Err,
	RCAF_UserStop,

	RCAF_VisionErr,
	RCAF_DisplaceErr,
	RCAF_AFPositionErr,
	RCAF_FocusErr,

	RCAF_Max,
};

static LPCTSTR g_szResultCode_AF[] =
{
	_T("[ERR] Unknown Err."),												
	_T("[ERR] OK"),																	
	_T("[ERR] Exception Err."),											
	_T("[ERR] Test Running Err."),								
	_T("[ERR] Model Setting Err."),													
	_T("[ERR] E.M.O Button is Active.."),																
	_T("[ERR] Max Pogo Count."),						
	_T("[ERR] Area Sensor Detected."),								
	_T("[ERR] Model File Empty Err."),														
	_T("[ERR] Lot ID empty Err."),								
	_T("[ERR] Operator Empty Err."),
	_T("[ERR] BarCode Empty Err."),
	_T("[ERR] Test Forced Stop."),													
	_T("[ERR] Cam Power On Err."),							
	_T("[ERR] Cam Power Off Err."),
	_T("[ERR] Current Err."), 													
	_T("[ERR] Camera Board Err."),														
	_T("[ERR] TimeOut Err."),														
	_T("[ERR] Camera Connect Err."),											
	_T("[ERR] User Stop."),

	_T("[ERR] Vision Err."),
	_T("[ERR] Displace Err"),
	_T("[ERR] Focus Position Err"),
	_T("[ERR] Focus Err"),
	NULL
};

typedef enum enIndicatorNum
{
	IndicatorX,
	IndicatorY,
	Indicator_Max,
};

typedef enum enTestEachResult
{
	TER_Fail,
	TER_Pass,
	TER_Run,
	TER_Empty,
	TER_SKIP,
	TER_Init,
	TER_UserStop,
	TER_MachineCheck,
	TER_MachineException,
	TER_Timeout,
	TER_NoImage,
	TER_NoBarcode,
	TER_TransferErr,
	TER_PogoCountOver,
	TER_EquipAlarm,
	TER_SensorDetect,
	TER_MasterCheck,
	TER_MasterMode,
};


static ST_StaticInf g_TestEachResult[] =
{
 	{ _T("FAIL"),				RGB(0, 0, 0), RGB(192, 0, 0),		RGB(174, 90, 33) },
 	{ _T("PASS"),				RGB(0, 0, 0), RGB(0, 122, 204),	RGB(20, 86, 150) },
 	{ _T("RUNNING"),			RGB(0, 0, 0),	RGB(237, 125,  49), RGB( 0,  0,  0)	 },
 	{ _T("EMPTY"),				RGB(0, 0, 0),	RGB(237, 125,  49), RGB( 0,  0,  0) },
 	{ _T("TEST SKIP"),			RGB(0, 0, 0),	RGB(237, 125,  49), RGB( 0,  0,  0) },
 	{ _T("STAND BY"),			RGB(0, 0, 0), RGB(125, 183, 91),	RGB(140, 255,165)},
 	{ _T("USER STOP"),			RGB(0, 0, 0), RGB(192, 0, 0),		RGB(174, 90, 33) },
 	{ _T("MACHINE ERROR"),		RGB(0, 0, 0), RGB(192, 0, 0),		RGB(174, 90, 33) },
 	{ _T("MACHINE EXCEPTION"),	RGB(0, 0, 0), RGB(192, 0, 0),		RGB(174, 90, 33) },
 	{ _T("TIMEOUT"),			RGB(0, 0, 0), RGB(192, 0, 0),		RGB(174, 90, 33) },
 	{ _T("CAMERA DISCONNECT"),	RGB(0, 0, 0), RGB(192, 0, 0),		RGB(174, 90, 33) },
 	{ _T("NO BARCODE"),			RGB(0, 0, 0), RGB(192, 0, 0),		RGB(174, 90, 33) },
 	{ _T("DEVICES ERROR"),		RGB(0, 0, 0), RGB(192, 0, 0),		RGB(174, 90, 33) },
 	{ _T("POGO COUNT OVER"),	RGB(0, 0, 0), RGB(192, 0, 0),		RGB(174, 90, 33) },
 	{ _T("EQUIPMENT ALARM"),	RGB(0, 0, 0), RGB(192, 0, 0),		RGB(174, 90, 33) },
	{ _T("SENSOR NOT RECOGNIZED"), RGB(0, 0, 0), RGB(192, 0, 0), RGB(174, 90, 33) },
	{ _T("NOT CONFIRM TO MASTER"), RGB(0, 0, 0), RGB(192, 0, 0), RGB(174, 90, 33) },
	{ _T("OPEN SHORT ERROR"), RGB(0, 0, 0), RGB(192, 0, 0), RGB(174, 90, 33) },



	NULL
};


typedef struct _tag_StaticResult
{
	LPCTSTR	szText;
	UINT nColorStyle;

}ST_StaticResult, *PST_StaticResult;

static ST_StaticResult g_TestJudgment[] =
{
	{ _T("FAIL"),					CVGStatic::ColorStyle_Red		},
	{ _T("PASS"),					CVGStatic::ColorStyle_Blue		},
	{ _T("RUNNING"),				CVGStatic::ColorStyle_Yellow	},
	{ _T("EMPTY"),					CVGStatic::ColorStyle_Yellow	},
	{ _T("TEST SKIP"),				CVGStatic::ColorStyle_Yellow	},
	{ _T("STAND BY"),				CVGStatic::ColorStyle_Green		},
	{ _T("USER STOP"),				CVGStatic::ColorStyle_Red		},
	{ _T("MACHINE ERROR"),			CVGStatic::ColorStyle_Red		},
	{ _T("MACHINE EXCEPTION"),		CVGStatic::ColorStyle_Red		},
	{ _T("TIMEOUT"),				CVGStatic::ColorStyle_Red		},
	{ _T("CAMERA DISCONNECT"),		CVGStatic::ColorStyle_Red		},
	{ _T("NO BARCODE"),				CVGStatic::ColorStyle_Red		},
	{ _T("DEVICES ERROR"),			CVGStatic::ColorStyle_Red		},
	{ _T("POGO COUNT OVER"),		CVGStatic::ColorStyle_Red		},
	{ _T("EQUIPMENT ALARM"),		CVGStatic::ColorStyle_Red		},
	{ _T("SENSOR NOT RECOGNIZED"),	CVGStatic::ColorStyle_Red		},
	{ _T("NOT CONFIRM TO MASTER"),	CVGStatic::ColorStyle_Red		},
	{ _T("MASTER MODE"),			CVGStatic::ColorStyle_Blue		},
	NULL
};

//-----------------------------------------------------------------------------
// 카메라 검사 정보 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_CamInfo
{
	CString				szReportFilePath;
	CString				szIndex;
	CString				szLotID;			// Lot ID	
	CString				szModelName;		// 모델 이름
	CString				szOperatorName;		// 작업자 이름
	CString				szBarcode;			// Barcode
	CString				szEquipment;		
	CString				szSWVersion;
	CString				szCamType;			// 역상 상태

	SYSTEMTIME			tmInputTime;		// 제품 투입시간
	DWORD				dwInputTime;		// CycleTime 계산용 제품 투입시간
	SYSTEMTIME			tmOutputTime;		// 제품 배출시간
	DWORD				dwOutputTime;		// CycleTime 계산용 제품 배출시간
	DWORD				dwCycleTime;		// 제품 투입에서 배출까지 시간
	DWORD				dwTactTime;			// Tact Time
	//DWORD				dwTestTime;			// 각 Site별 테스트 시간 누적
	CString				szDay;				// ex)20161008
	CString				szTime;				// ex)180000

	UINT				nSocketIndex;		// 테이블 위치 (Socket)
	enTestProcess		nProgressStatus;	// 각 카메라의 검사 진행 상태

	enTestResult		nJudgment;			// 최종 결과, 제품 유무
	enTestResult		nJudgmentProc;			// 최종 결과, 제품 유무
	LRESULT				ResultCode;			// 결과 코드 (오류 코드)	
	
	enTestEachResult	nJudgment_TestItem[USE_TEST_ITEM_CNT];	// 검사 항목별 결과
	INT					FailTestItem;		// 불량이 발생한 검사 항목

	
	enTestEachResult	nJudgmentInitial;
	enTestEachResult	nJudgmentFinalize;

	enTestEachResult	nJudgmentVision;
	enTestEachResult	nJudgmentDisplace;
	enTestEachResult	nJudgmentAFPosition;

	double				dVColletDegree;		// 비전 콜렛 측정 데이터
	double				dDisplaceAvg;		// 변위 평균 측정 데이터 
	double				dInitPosY;
	double				dInitPosR;
	BOOL				bTestMode;

	UINT				CurrentTestItem;	// 현재 검사 중인 TestItem	
	DWORD				dwTestItemST[USE_TEST_ITEM_CNT];	// TestItem 검사 시간

	// 각 검사 항목별 설정
	ST_LT_TI_Current		stCurrent[TICnt_Current];
//	ST_LT_TI_OperationMode	stOperMode[TICnt_OperationMode];
	ST_LT_TI_CenterPoint	stCenterPoint[TICnt_CenterPoint];
	ST_LT_TI_Rotate			stRotate[TICnt_Rotation];
	ST_LT_TI_EIAJ			stEIAJ[TICnt_EIAJ];
	ST_LT_TI_Angle			stAngle[TICnt_FOV];
	ST_LT_TI_Color			stColor[TICnt_Color];
	ST_LT_TI_Reverse		stReverse[TICnt_Reverse];
	ST_LT_TI_ParticleMA		stParticleMA[TICnt_ParticleManual];
	ST_LT_TI_BlackSpot		stParticle[TICnt_BlackSpot];
	ST_LT_TI_DefectPixel	stDefectPixel[TICnt_DefectPixel];
//	ST_LT_TI_LED			stLED[TICnt_LEDTest];

	//ST_LT_TI_PatternNoise	stPatternNoise[TICnt_PatternNoise];
	//ST_LT_TI_IRFilterMA		stIRFilterMA[TICnt_IRFilterManual];
	//ST_LT_TI_VideoSignal	stVideoSignal[TICnt_VideoSignal];
	//ST_LT_TI_Reset			stReset[TICnt_Reset];


	ST_LT_TI_IRFilter		stIRFilter	[TICnt_IRFilter];
 	ST_LT_TI_Brightness		stBrightness [TICnt_Brightness	];
 //	ST_LT_TI_SFR			stSFR		 [TICnt_SFR			];
// 	ST_LT_TI_BlackSpot		stBlackSpot	 [TICnt_BlackSpot	];
// 	ST_LT_TI_FFT			stFFT		 [TICnt_FFT			];

	_tag_CamInfo()
	{

		ZeroMemory(&tmInputTime, sizeof(SYSTEMTIME));
		ZeroMemory(&tmOutputTime, sizeof(SYSTEMTIME));
		dwInputTime		= 0;
		dwOutputTime	= 0;
		dwCycleTime		= 0;
		dwTactTime		= 0;
		//dwTestTime		= 0;

		nSocketIndex	= 0;
		nProgressStatus = enTestProcess::TP_Idle;
		nJudgment		= enTestResult::TR_Empty;
		nJudgmentProc		= enTestResult::TR_Empty;

		ResultCode		= RCC_OK;
		nJudgmentInitial = enTestEachResult::TER_Init;
		nJudgmentFinalize = enTestEachResult::TER_Init;
		nJudgmentVision = enTestEachResult::TER_Init;
		nJudgmentDisplace = enTestEachResult::TER_Init;
		nJudgmentAFPosition = enTestEachResult::TER_Init;
		FailTestItem = -1;

		dVColletDegree	= 0;
		dDisplaceAvg	= 0;

		dInitPosY = 0;
		dInitPosR = 0;

		bTestMode = FALSE;
	};

	void Reset()
	{
		szReportFilePath.Empty();
		szIndex.Empty();
		szLotID.Empty();
		szModelName.Empty();
		szOperatorName.Empty();
		szBarcode.Empty();
		szEquipment.Empty();
		szSWVersion.Empty();
		szCamType.Empty();

		ZeroMemory(&tmInputTime, sizeof(SYSTEMTIME));
		ZeroMemory(&tmOutputTime, sizeof(SYSTEMTIME));
		dwInputTime		= 0;
		dwOutputTime	= 0;
		dwCycleTime		= 0;
		dwTactTime		= 0;
		szDay.Empty();
		szTime.Empty();
		nProgressStatus = enTestProcess::TP_Idle;
		nJudgment		= enTestResult::TR_Init;
		nJudgmentProc = enTestResult::TR_Init;
		nJudgmentInitial = enTestEachResult::TER_Init;
		nJudgmentFinalize = enTestEachResult::TER_Init;
		nJudgmentVision = enTestEachResult::TER_Init;
		nJudgmentDisplace = enTestEachResult::TER_Init;
		nJudgmentAFPosition = enTestEachResult::TER_Init;
		ResultCode		= RCC_OK;
		FailTestItem	= -1;
	
		dVColletDegree	= 0;
		dDisplaceAvg	= 0;

		dInitPosY = 0;
		dInitPosR = 0;

		bTestMode = FALSE;

		// reset
		for (int i = 0; i < TICnt_Current; i++)
			stCurrent[i].Reset();

		//for (int i = 0; i < TICnt_OperationMode; i++)
		//	stOperMode[i].Reset();

		for (int i = 0; i < TICnt_CenterPoint; i++)
			stCenterPoint[i].Reset();

		for (int i = 0; i < TICnt_Rotation; i++)
			stRotate[i].Reset();

		for (int i = 0; i < TICnt_EIAJ; i++)
			stEIAJ[i].Reset();

		//for (int i = 0; i < TICnt_SFR; i++)
		//	stSFR[i].Reset();

		for (int i = 0; i < TICnt_FOV; i++)
			stAngle[i].Reset();

		for (int i = 0; i < TICnt_Color; i++)
			stColor[i].Reset();

		for (int i = 0; i < TICnt_Reverse; i++)
			stReverse[i].Reset();

		for (int i = 0; i < TICnt_ParticleManual; i++)
			stParticleMA[i].Reset();

		for (int i = 0; i < TICnt_BlackSpot; i++)
			stParticle[i].Reset();

		for (int i = 0; i < TICnt_DefectPixel; i++)
			stDefectPixel[i].Reset();

		//for (int i = 0; i < TICnt_LEDTest; i++)
		//	stLED[i].Reset();

		//for (int i = 0; i < TICnt_VideoSignal; i++)
		//	stVideoSignal[i].Reset();

		//for (int i = 0; i < TICnt_Reset; i++)
		//	stReset[i].Reset();

		for (int i = 0; i < TICnt_IRFilter; i++)
			stIRFilter[i].Reset();

		for (int i = 0; i < TICnt_Brightness; i++)
			stBrightness[i].Reset();

		//for (int i = 0; i < TICnt_PatternNoise; i++)
		//	stPatternNoise[i].Reset();
	};

	_tag_CamInfo& operator= (_tag_CamInfo& ref)
	{
		szReportFilePath = ref.szReportFilePath;
		szIndex			= ref.szIndex;
		szLotID			= ref.szLotID;		
		szModelName		= ref.szModelName;
		szOperatorName	= ref.szOperatorName;
		szBarcode = ref.szBarcode;
		szEquipment = ref.szEquipment;
		szSWVersion = ref.szSWVersion;
		szCamType = ref.szCamType;

		memcpy(&tmInputTime, &ref.tmInputTime, sizeof(SYSTEMTIME));
		memcpy(&tmOutputTime, &ref.tmOutputTime, sizeof(SYSTEMTIME));
		dwInputTime		= ref.dwInputTime;
		dwOutputTime	= ref.dwOutputTime;
		dwCycleTime		= ref.dwCycleTime;
		dwTactTime		= ref.dwTactTime;
		//dwTestTime		= ref.dwTestTime;
		szDay			= ref.szDay;
		szTime			= ref.szTime;
		nSocketIndex	= ref.nSocketIndex;
		nProgressStatus = ref.nProgressStatus;
		nJudgment = ref.nJudgment;
		
		nJudgmentProc = ref.nJudgmentProc;
		nJudgmentInitial = ref.nJudgmentInitial;
		nJudgmentFinalize = ref.nJudgmentFinalize;
		nJudgmentVision = ref.nJudgmentVision;
		nJudgmentDisplace = ref.nJudgmentDisplace;
		nJudgmentAFPosition = ref.nJudgmentAFPosition;


		ResultCode		= ref.ResultCode;
		FailTestItem	= ref.FailTestItem;

		dVColletDegree	= ref.dVColletDegree;
		dDisplaceAvg	= ref.dDisplaceAvg;
		dInitPosY		= ref.dInitPosY;
		dInitPosR		= ref.dInitPosR;
		bTestMode		= ref.bTestMode;


		// worklist
		for (int i = 0; i < TICnt_Current; i++)
			stCurrent[i] = ref.stCurrent[i];

		for (int i = 0; i < TICnt_CenterPoint; i++)
			stCenterPoint[i] = ref.stCenterPoint[i];

		//for (int i = 0; i < TICnt_OperationMode; i++)
		//	stOperMode[i] = ref.stOperMode[i];

		for (int i = 0; i < TICnt_Rotation; i++)
			stRotate[i] = ref.stRotate[i];

		for (int i = 0; i < TICnt_EIAJ; i++)
			stEIAJ[i] = ref.stEIAJ[i];

		for (int i = 0; i < TICnt_FOV; i++)
			stAngle[i] = ref.stAngle[i];

		for (int i = 0; i < TICnt_Color; i++)
			stColor[i] = ref.stColor[i];

		for (int i = 0; i < TICnt_Reverse; i++)
			stReverse[i] = ref.stReverse[i];

		for (int i = 0; i < TICnt_ParticleManual; i++)
			stParticleMA[i] = ref.stParticleMA[i];

		for (int i = 0; i < TICnt_BlackSpot; i++)
			stParticle[i] = ref.stParticle[i];
		
		for (int i = 0; i < TICnt_DefectPixel; i++)
			stDefectPixel[i] = ref.stDefectPixel[i];

		//for (int i = 0; i < TICnt_LEDTest; i++)
		//	stLED[i] = ref.stLED[i];

		//for (int i = 0; i < TICnt_VideoSignal; i++)
		//	stVideoSignal[i] = ref.stVideoSignal[i];

		//for (int i = 0; i < TICnt_Reset; i++)
		//	stReset[i] = ref.stReset[i];

		//for (int i = 0; i < TICnt_PatternNoise; i++)
		//	stPatternNoise[i] = ref.stPatternNoise[i];

		for (int i = 0; i < TICnt_IRFilter; i++)
			stIRFilter[i] = ref.stIRFilter[i];
// 
// 		for (int i = 0; i < TICnt_BlackSpot; i++)
// 			stBlackSpot[i] = ref.stBlackSpot[i];
// 
		for (int i = 0; i < TICnt_Brightness; i++)
			stBrightness[i] = ref.stBrightness[i];

		//for (int i = 0; i < TICnt_SFR; i++)
		//	stSFR[i] = ref.stSFR[i];
// 
// 
// 		for (int i = 0; i < TICnt_FFT; i++)
// 			stFFT[i] = ref.stFFT[i];


		return *this;
	};

	void SetTestProgress(__in enTestProcess nProcess)
	{
		nProgressStatus = nProcess;
	};

	void SetInformation(__in LPCTSTR szIn_LotName, __in LPCTSTR szIn_Barcode, __in LPCTSTR szIn_ModelName, __in LPCTSTR szIn_OperatorName)
	{
		szLotID			= szIn_LotName;
		szBarcode		= szIn_Barcode;
		szModelName		= szIn_ModelName;		// 모델 이름
		szOperatorName	= szIn_OperatorName;
	};

	void SetInputTime()
	{		
		GetLocalTime(&tmInputTime);
		dwInputTime = timeGetTime();
		szDay.Format(_T("%04d-%02d-%02d"), tmInputTime.wYear, tmInputTime.wMonth, tmInputTime.wDay);
		szTime.Format(_T("%02d:%02d:%02d"), tmInputTime.wHour, tmInputTime.wMinute, tmInputTime.wSecond);
	};

	void SetInputTime(__in SYSTEMTIME tmIn, __in DWORD dwIn)
	{
		memcpy(&tmInputTime, &tmIn, sizeof(SYSTEMTIME));
		dwInputTime = dwIn;
		szDay.Format(_T("%04d-%02d-%02d"), tmInputTime.wYear, tmInputTime.wMonth, tmInputTime.wDay);
		szTime.Format(_T("%02d:%02d:%02d"), tmInputTime.wHour, tmInputTime.wMinute, tmInputTime.wSecond);
	};

 	void SetOutputTime()
 	{
 		GetLocalTime(&tmOutputTime);
		dwOutputTime = timeGetTime();

		if (dwOutputTime < dwInputTime)
		{
			dwCycleTime = 0xFFFFFFFF - dwInputTime + dwOutputTime;
		}
		else
		{
			dwCycleTime = dwOutputTime - dwInputTime;
		}
 	};

	void SetOutputTime(__in SYSTEMTIME tmOut, __in DWORD dwOut)
	{
		memcpy(&tmOutputTime, &tmOut, sizeof(SYSTEMTIME));
		dwOutputTime = dwOut;

		if (dwOutputTime < dwInputTime)
		{
			dwCycleTime = 0xFFFFFFFF - dwInputTime + dwOutputTime;
		}
		else
		{
			dwCycleTime = dwOutputTime - dwInputTime;
		}
	};

	void SetSocketIndex(__in UINT nIdx)
	{
		nSocketIndex = nIdx;
	};

}ST_CamInfo;

#endif // Def_Test_h__
