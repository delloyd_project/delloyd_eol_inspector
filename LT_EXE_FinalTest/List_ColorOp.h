﻿#ifndef List_ColorOp_h__
#define List_ColorOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_ColorOp
{
	CoOp_Object = 0,
	CoOp_PosX,
	CoOp_PosY,
	CoOp_Width,
	CoOp_Height,
	CoOp_MinR,
	CoOp_MaxR,
	CoOp_MinG,
	CoOp_MaxG,
	CoOp_MinB,
	CoOp_MaxB,
	CoOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_ColorOp[] =
{
	_T(""),
	_T("X"),
	_T("Y"),
	_T("W"),
	_T("H"),
	_T("MinR"),
	_T("MaxR"),
	_T("MinG"),
	_T("MaxG"),
	_T("MinB"),
	_T("MaxB"),
	NULL  
};

typedef enum enListItemNum_ColorOp
{
	CoOp_ItemNum = ROI_CL_Max,
};

static LPCTSTR	g_lpszItem_ColorOp[] =
{
	NULL
};

const int	iListAglin_ColorOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER, 
};

const int	iHeaderWidth_ColorOp[] =
{
	50,
	45,
	45,
	45,
	45,
	45,
	45,
	45,
	45,
	45,
	45,
	45,
};
// List_ColorInfo

class CList_ColorOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_ColorOp)

public:
	CList_ColorOp();
	virtual ~CList_ColorOp();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Color(ST_LT_TI_Color* pstColor)
	{
		if (pstColor == NULL)
			return;

		m_pstColor = pstColor;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth  = nWidth;
		m_nHeight = nHeight;
	};

protected:

	ST_LT_TI_Color*  m_pstColor;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT		m_nEditCol;
	UINT		m_nEditRow;

	UINT		m_nWidth;
	UINT		m_nHeight;

	BOOL		UpdateCellData			(UINT nRow, UINT nCol, int  iValue);
	BOOL		UpdateCelldbData		(UINT nRow, UINT nCol, double dValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel	(UINT nFlags, short zDelta, CPoint pt);
	
	afx_msg void	OnEnKillFocusEdit		();
};

#endif // List_ColorInfo_h__
