﻿//*****************************************************************************
// Filename	: 	Pane_CommStatus.h
// Created	:	2014/7/5 - 10:23
// Modified	:	2016/09/21
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Pane_CommStatus_h__
#define Pane_CommStatus_h__

#pragma once

#include "Def_Enum.h"
#include "VGStatic.h"

//#define		USE_LOG_WND // 테스트 용도 출력 창

#ifdef USE_LOG_WND
#include "LogOutput.h"
#endif

//=============================================================================
// CPane_CommStatus
//=============================================================================
class CPane_CommStatus : public CMFCTasksPane
{
	DECLARE_DYNAMIC(CPane_CommStatus)

public:
	CPane_CommStatus();
	virtual ~CPane_CommStatus();

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);	
	afx_msg void	OnUpdateCmdUI_Test	(CCmdUI* pCmdUI);
	afx_msg void	OnBnClickedTest		(UINT nID);

	afx_msg void	OnUpdateCmdUI_Dev	(CCmdUI* pCmdUI);
	afx_msg void	OnBnClicked_Dev		(UINT nID);

	CFont			m_Font;

#ifdef USE_LOG_WND
	CVGStatic		m_st_WarningEvent;
	CLogOutput		m_ed_WarningLog;
#endif
	
	CButton			m_bn_Test[4];
	
	enum enDeviceStaticIndex
	{
		DevSI_BaseIndex			= 0,
		DevSI_PermissionMode	= 0,
		DevSI_OperateMode,
		Dev_MES,
		DevSI_PCIMotorBrd,
		DevSI_PCIIoBrd,
 		DevSI_GrabberBrd_ComArt,
		//DevSI_GrabberBrd_DAQ,
		DevSI_BCR,
		DevSI_LabelPrinter,
		DevSI_CanMain,
		DevSI_CanMain2,
		DevSI_CanMain3,
		DevSI_CanMain4,
		DevSI_Light,
		DevSI_Light2,
		DevSI_Light3,
		DevSI_Light4,
		DevSI_IndicatorX,
		DevSI_IndicatorY,
		DevSI_MaxCount,
	};

	enum enDeviceButtonIndex
	{
		DEV_BN_PermissionChange,
		DEV_BN_OperateModeChange,
		DEV_BN_Keyboard,
		DEV_BN_Barcode,
		DEV_BN_EquipmentInit,
		DEV_BN_MaxCount,
	};

	enum enDeviceIndicator
	{
		DEV_ST_IndicatorX,
		DEV_ST_IndicatorY,
		DEV_ST_IndicatorMax,
	};


	// -- 제어 권한 모드 --
	// 작업자, 상급자, 관리자
	// 접속	

	// -- 주변 장치 --
	// 프레임 그래버 보드 (최대 6개, PC당 2보드 총 4채널) 
	// 바코드 리더기

	// -- Digital In --
	// I/O 상태 (최대 16)


	CVGStatic		m_st_Device[DevSI_MaxCount];
	CButton			m_bn_Device[DEV_BN_MaxCount];
	CVGStatic		m_st_Indecator[DEV_ST_IndicatorMax];
	
	void			AddSystemInfo			();

	void			AddPermissionMode		();	

	void			AddBarcode				();
	void			AddLabelPrinter			();
	void			AddPCISlot				();
	void			AddSerialComm			();
	void			AddTCPIP				();
	void			AddGrabber				();
	void			AddDisplay				();
	void			AddUtilities			();
	
#ifdef USE_LOG_WND
	void			AddWarningStatus		();
#endif
	void			AddTestCtrl				();	
	// 장비제어
	virtual void	Add_EquipmentCtrl();
	// 설비 구동 모드 표시
	virtual void	Add_OperateMode();
public:

	virtual CSize	CalcFixedLayout			(BOOL, BOOL);
	
	void			SetStatus_PermissionMode	(__in enPermissionMode InspMode);
	void			SetStatus_MES				(__in UINT nCommStatus);
	void			SetStatus_Motor				(__in UINT nConStatus);
	void			SetStatus_IO				(__in UINT nConStatus);
	void			SetStatus_GrabberBrd_ComArt	(__in UINT nConStatus);
	void			SetStatus_BCR				(__in BOOL bConStatus);
	void			SetStatus_LabelPrinter		(__in BOOL bConStatus);
	void			SetStatus_CameraBoard		(__in UINT nBrdIdx, __in UINT nConStatus);
	void			SetStatus_LightBoard		(__in UINT nBrdIdx, __in UINT nConStatus);
	void			SetStatus_Indicator			(__in UINT nConStatus, __in UINT nChIdx);
//	void			SetStatus_LaserSensor		(__in BOOL bConnect, __in UINT nSensorIdx = 0);
//	void			SetStatus_VisionCam			(__in BOOL bConnect, __in UINT nCamIdx = 0);
//	void			SetStatus_VisionLight		(__in BOOL bConnect, __in UINT nChIdx = 0);
	void			Set_Barcode					(__in LPCTSTR szBarcode);
	void			SetIndicatorDisplay			(__in float fAxisX, __in float fAxisY);

	// 설비 구동 모드
	void			SetStatus_OperateMode(__in enOperateMode nOperMode);
};

#endif // Pane_CommStatus_h__
