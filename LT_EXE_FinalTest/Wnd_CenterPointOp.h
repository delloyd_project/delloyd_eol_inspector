﻿#ifndef Wnd_CenterPointOp_h__
#define Wnd_CenterPointOp_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "List_CenterPointOp.h"
#include "Def_DataStruct_Cm.h"

// CWnd_CenterPointOp

enum enCenterPointStatic
{
	STI_CTP_REFAXIS_X = 0,
	STI_CTP_REFAXIS_Y,
	STI_CTP_DEVOFFSET_X,
	STI_CTP_DEVOFFSET_Y,
	STI_CTP_OFFSET_X,
	STI_CTP_OFFSET_Y,
	//STI_CTP_TESTMODE,
	STI_CTP_MAX,
};

static LPCTSTR	g_szCenterPointStatic[] =
{
	_T("Reference Axis X"),
	_T("Reference Axis Y"),
	_T("Spec X"),
	_T("Spec Y"),
	_T("Offset X"),
	_T("Offset Y"),
	//_T("Test Mode"),
	NULL
};

enum enCenterPointButton
{
	BTN_CTP_TEST = 0,
	BTN_CTP_MAX,
};

static LPCTSTR	g_szCenterPointButton[] =
{
	_T("TEST"),
	NULL
};

enum enCenterPointComobox
{
	//CMB_CTP_TESTMODE,
	CMB_CTP_MAX
};

enum enCenterPointEdit
{
	EDT_CTP_REFAXIS_X = 0,
	EDT_CTP_REFAXIS_Y,
	EDT_CTP_DEVOFFSET_X,
	EDT_CTP_DEVOFFSET_Y,
	EDT_CTP_OFFSET_X,
	EDT_CTP_OFFSET_Y,
	EDT_CTP_MAX,
};



class CWnd_CenterPointOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_CenterPointOp)

public:
	CWnd_CenterPointOp();
	virtual ~CWnd_CenterPointOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo		*m_pstModelInfo;
	CList_CenterPointOp  m_List;

	CFont				m_font;

	CVGStatic			m_st_Item[STI_CTP_MAX];
	CButton				m_bn_Item[BTN_CTP_MAX];
	//CComboBox			m_cb_Item[CMB_CTP_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_CTP_MAX];

	// 검사 항목이 다수 인경우
	UINT				m_nTestItemCnt;

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};

#endif // Wnd_CenterPointOp_h__
