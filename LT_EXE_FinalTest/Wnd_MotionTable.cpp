﻿// Wnd_IOTable.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_MotionTable.h"

// CWnd_MotionTable

typedef enum MotionTable_ID
{
	IDC_ST_AXIS_NAME	= 1000,
	IDC_ST_SEN_AMP		= 1100,
	IDC_ST_SEN_POS		= 1200,
	IDC_ST_SEN_ORI		= 1300,
	IDC_ST_SEN_MOTION	= 1400,
	IDC_ST_SEN_LLIT		= 1500,
	IDC_ST_SEN_HOME		= 1600,
	IDC_ST_SEN_HLIT		= 1700,
	IDC_ST_SEN_ALRAM	= 1800,
	IDC_ST_SEN_MAXNUM   = IDC_ST_SEN_ALRAM + 100,
};

IMPLEMENT_DYNAMIC(CWnd_MotionTable, CWnd)

CWnd_MotionTable::CWnd_MotionTable()
{
	m_pstDevice				= NULL;
	m_hTimerQueue			= NULL;
	m_hTimer_SensorCheck	= NULL;
	m_nAxisNum				= 0;
	m_nAxisMax				= 0;

	m_hExitEvent	= CreateEvent( NULL, FALSE, FALSE, NULL);

// 	CreateTimerQueue_Mon	();
// 	CreateTimerSensorCheck	();
}

CWnd_MotionTable::~CWnd_MotionTable()
{
//	DeleteTimerQueue_Mon	();
}

BEGIN_MESSAGE_MAP(CWnd_MotionTable, CWnd)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_ST_AXIS_NAME,	IDC_ST_SEN_AMP	 - 1, OnAxisSelect)
	ON_COMMAND_RANGE(IDC_ST_SEN_AMP,  	IDC_ST_SEN_POS	 - 1, OnAxisAmpCtr)
	ON_COMMAND_RANGE(IDC_ST_SEN_ALRAM,  IDC_ST_SEN_MAXNUM,	OnAxisAlramCtr)
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/28 - 17:02
// Desc.		:
//=============================================================================
int CWnd_MotionTable::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_gb_Item.Create(NULL, dwStyle | BS_GROUPBOX, rectDummy, this, IDC_STATIC);

	m_st_Name.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Name.SetBackColor_COLORREF(RGB(33, 73, 125));
	m_st_Name.SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
	m_st_Name.SetFont_Gdip(L"Arial", 12.0F);
	m_st_Name.Create(_T("MOTOR INFO MONITERING"), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < ST_MT_MAXNUM; nIdx++)
	{
		m_st_Axis[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_Axis[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Axis[nIdx].SetFont_Gdip(L"Arial", 10.0F);
		m_st_Axis[nIdx].Create(g_szMotionStatusName[nIdx], dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < ST_MT_MAXNUM; nIdx++)
	{
		m_nAxisMax = m_pstDevice->m_AllMotorData.MotorInfo.lMaxAxisCnt;

		if (m_pstDevice->m_AllMotorData.MotorInfo.lMaxAxisCnt == 0)
		{
			m_nAxisMax = 4;
		}

		for (UINT nAxis = 0; nAxis < m_nAxisMax; nAxis++)
		{
			m_st_Sen[nAxis][ST_MT_POS_NAME].SetFont_Gdip(L"Arial", 12.0F);
			m_st_Sen[nAxis][nIdx].SetStaticStyle(CVGStatic::StaticStyle_GroupHeader);

			if (nIdx == ST_MT_AXIS_NAME)
			{
				CString strName;

				if (m_pstDevice->m_AllMotorData.pMotionParam == NULL)
					strName.Empty();
				else
					strName = m_pstDevice->m_AllMotorData.pMotionParam[nAxis].szAxisName;

				m_st_Sen[nAxis][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
				m_st_Sen[nAxis][nIdx].Create(strName, dwStyle | SS_NOTIFY | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_ST_AXIS_NAME + nAxis + nIdx * MAX_AIXS);
			}
			else
			{
				m_st_Sen[nAxis][nIdx].SetColorStyle(CVGStatic::ColorStyle_Black);
				m_st_Sen[nAxis][nIdx].Create(_T(""), dwStyle | SS_NOTIFY | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_ST_AXIS_NAME + nAxis + nIdx * MAX_AIXS);
			}
		}
	}

	m_st_Sen[0][ST_MT_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	return 0;
}

//=============================================================================
// Method		: OnDestroy
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/14 - 16:57
// Desc.		:
//=============================================================================
void CWnd_MotionTable::OnDestroy()
{
	StopThread_SensorMon();
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
void CWnd_MotionTable::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if (m_pstDevice == NULL)
		return;

	if ((0 == cx) || (0 == cy))
		return;

	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = 0;
	int iTop	 = iMargin;
	int iWidth	 = cx;
	int iHeight  = cy - iMargin;
	int iStaticW  = (iWidth - iSpacing * ST_MT_MAXNUM) / (ST_MT_MAXNUM + 3) + 1;
	
	int iStNameH = 28;
	int iEdNameH = 28;
	int iStSensorH = 0;
	int iHeightTemp = 0;
	int iTitleH = 35;

	int iAxisCnt = 0;

	// 	초기화
	for (UINT nAxis = 0; nAxis < m_nAxisMax; nAxis++)
	{
		for (UINT nIdx = 0; nIdx < ST_MT_MAXNUM; nIdx++)
			m_st_Sen[nAxis][nIdx].MoveWindow(iLeft, iTop, 0, 0);

		if (TRUE == m_pstDevice->GetAxisUseStatus(nAxis))
		{
			iAxisCnt++;
		}
	}

	m_st_Name.MoveWindow(0, iTop, cx, iTitleH);

	iTop += iTitleH + iSpacing;
	iLeft = 0;
	m_st_Axis[ST_MT_AXIS_NAME].MoveWindow(iLeft, iTop, iStaticW * 2, iStNameH);
	iLeft += iStaticW * 2 + iSpacing;

	m_st_Axis[ST_MT_POWER_NAME].MoveWindow(iLeft, iTop, iStaticW * 1, iStNameH);
	iLeft += iStaticW * 1 + iSpacing;

	m_st_Axis[ST_MT_POS_NAME].MoveWindow(iLeft, iTop, iStaticW * 3, iStNameH);
	iLeft += iStaticW * 3 + iSpacing;

	for (UINT nIdx = ST_MT_ORI_NAME; nIdx < ST_MT_MAXNUM; nIdx++)
	{
		m_st_Axis[nIdx].MoveWindow(iLeft, iTop, iStaticW, iStNameH);
		iLeft += iStaticW + iSpacing;
	}

	iTop += iStNameH + iSpacing;

	if (iAxisCnt == 0)
		return;

	iHeightTemp = iHeight - iTop;
	iStSensorH = (int)((iHeightTemp * HEIGHT_OFFSET) - (iSpacing * (iAxisCnt - 1))) / iAxisCnt;

	for (UINT nAxis = 0; nAxis < (UINT)iAxisCnt; nAxis++)
	{
 		if (m_pstDevice->GetAxisUseStatus(nAxis) == TRUE)
		{
			iLeft = 0;

			m_st_Sen[nAxis][ST_MT_AXIS_NAME].MoveWindow(iLeft, iTop, iStaticW * 2, iStSensorH);
			iLeft += iStaticW * 2 + iSpacing;

			m_st_Sen[nAxis][ST_MT_POWER_NAME].MoveWindow(iLeft, iTop, iStaticW * 1, iStSensorH);
			iLeft += iStaticW * 1 + iSpacing;

			m_st_Sen[nAxis][ST_MT_POS_NAME].MoveWindow(iLeft, iTop, iStaticW * 3, iStSensorH);
			iLeft += iStaticW * 3 + iSpacing;

			for (UINT nIdx = ST_MT_ORI_NAME; nIdx < ST_MT_MAXNUM; nIdx++)
			{
				m_st_Sen[nAxis][nIdx].MoveWindow(iLeft, iTop, iStaticW, iStSensorH);
				iLeft += iStaticW + iSpacing;
			}

			iTop += iStSensorH + iSpacing;
		}
	}
}

//=============================================================================
// Method		: OnSensorComds
// Access		: public  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/3/29 - 15:09
// Desc.		:
//=============================================================================
void CWnd_MotionTable::OnAxisSelect(UINT nID)
{
	if (m_pstDevice == NULL)
		return;

	m_nAxisNum = nID - IDC_ST_AXIS_NAME;

	GetOwner()->SendMessage(WM_SELECT_AXIS, (WPARAM)m_nAxisNum, 0);

	for (UINT nIdx = 0; nIdx < m_nAxisMax; nIdx++)
		m_st_Sen[nIdx][ST_MT_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_Default);

	m_st_Sen[m_nAxisNum][ST_MT_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
}

//=============================================================================
// Method		: OnAxisAmpCtr
// Access		: public  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/3/29 - 15:42
// Desc.		:
//=============================================================================
void CWnd_MotionTable::OnAxisAmpCtr(UINT nID)
{
	if (m_pstDevice == NULL)
		return;

	UINT nAxis = nID - IDC_ST_SEN_AMP;

	if (m_pstDevice->GetAmpStatus(nAxis))
		m_pstDevice->SetAmpCtr(nAxis, OFF);
	else
		m_pstDevice->SetAmpCtr(nAxis, ON);
}

//=============================================================================
// Method		: OnAxisAlramCtr
// Access		: public  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/3/29 - 15:42
// Desc.		:
//=============================================================================
void CWnd_MotionTable::OnAxisAlramCtr(UINT nID)
{
	if (m_pstDevice == NULL)
		return;

	UINT nAxis = nID - IDC_ST_SEN_ALRAM;

	m_pstDevice->SetAlarmClear(nAxis);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/5 - 18:30
// Desc.		:
//=============================================================================
BOOL CWnd_MotionTable::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: Thread_SensorMon
// Access		: protected static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2018/3/11 - 20:14
// Desc.		:
//=============================================================================
UINT WINAPI CWnd_MotionTable::Thread_SensorMon(__in LPVOID lParam)
{
	CWnd_MotionTable* pThis = (CWnd_MotionTable*)lParam;

	HANDLE hEvent[2] = { NULL, NULL };
	hEvent[0] = pThis->m_hExitEvent;

	pThis->m_bLoop_SensorMon = TRUE;
	DWORD dwEvent = 0;

	__try
	{
		while (pThis->m_bLoop_SensorMon)
		{
			dwEvent = WaitForSingleObject(pThis->m_hExitEvent, 150);

			switch (dwEvent)
			{
			case WAIT_OBJECT_0:	// Exit Program
				TRACE(_T(" -- 프로그램 종료 m_hExternalExitEvent 이벤트 감지 \n"));
				pThis->m_bLoop_SensorMon = FALSE;
				break;

			case WAIT_TIMEOUT:
				pThis->OnMonitorSensorCheck();
				break;

			case WAIT_FAILED:
				break;
			}
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_MotionTable::Thread_SensorMon()\n"));
	}

	return TRUE;
}

//=============================================================================
// Method		: TimerRoutine_SensorCheck
// Access		: protected static  
// Returns		: VOID CALLBACK
// Parameter	: __in PVOID lpParam
// Parameter	: __in BOOLEAN TimerOrWaitFired
// Qualifier	:
// Last Update	: 2017/3/29 - 9:01
// Desc.		:
//=============================================================================
VOID CALLBACK CWnd_MotionTable::TimerRoutine_SensorCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired)
{
	CWnd_MotionTable* pThis = (CWnd_MotionTable*)lpParam;

#ifndef NO_CHECK_MOTION_SENSOR
	pThis->OnMonitorSensorCheck();
#endif
}

//=============================================================================
// Method		: CreateTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:01
// Desc.		:
//=============================================================================
void CWnd_MotionTable::CreateTimerQueue_Mon()
{
	__try
	{
		// Create the timer queue.
		m_hTimerQueue = CreateTimerQueue();
		if (NULL == m_hTimerQueue)
		{
			TRACE(_T("CreateTimerQueue failed (%d)\n"), GetLastError());
			return;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		//	AddLog(_T("*** Exception Error : CreateTimerQueue_Mon ()"));
	}
}

//=============================================================================
// Method		: DeleteTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:02
// Desc.		:
//=============================================================================
void CWnd_MotionTable::DeleteTimerQueue_Mon()
{
	// 타이머가 종료될때까지 대기
	__try
	{
		if (!DeleteTimerQueue(m_hTimerQueue))
			TRACE(_T("DeleteTimerQueue failed (%d)\n"), GetLastError());
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_MotionTable::DeleteTimerQueue_Mon()\n"));
	}

	TRACE(_T("타이머 종료 : CWnd_MotionTable::DeleteTimerQueue_Mon()\n"));
}

//=============================================================================
// Method		: CreateTimerSensorCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:02
// Desc.		:
//=============================================================================
void CWnd_MotionTable::CreateTimerSensorCheck()
{
	__try
	{
		// Time Check Timer
		if (NULL == m_hTimer_SensorCheck)
		if (!CreateTimerQueueTimer(&m_hTimer_SensorCheck, m_hTimerQueue, (WAITORTIMERCALLBACK)TimerRoutine_SensorCheck, (PVOID)this, 2000, 150, WT_EXECUTEDEFAULT))
		{
			TRACE(_T("CreateTimerQueueTimer failed (%d)\n"), GetLastError());
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_IOTable::CreateTimer_InputCheck()\n"));
	}
}

//=============================================================================
// Method		: DeleteTimerSensorCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:02
// Desc.		:
//=============================================================================
void CWnd_MotionTable::DeleteTimerSensorCheck()
{
	__try
	{
		if (DeleteTimerQueueTimer(m_hTimerQueue, m_hTimer_SensorCheck, NULL))
			m_hTimer_SensorCheck = NULL;
		else
			TRACE(_T("DeleteTimerQueueTimer : m_hTimer_InputCheck failed (%d)\n"), GetLastError());
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_IOTable::DeleteTimer_InputCheck()\n"));
	}
}

//=============================================================================
// Method		: OnMonitorSensorCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:02
// Desc.		:
//=============================================================================
void CWnd_MotionTable::OnMonitorSensorCheck()
{
	// 모터 Amp 센서
	GetMotorAmpStatus();
	Sleep(10);

	// 모터 원점 센서
	GetMotorOriginStatus();
	Sleep(10);

	// 모터 동작 상태
	GetMotorMotionStatus();
	Sleep(10);

	// 모터 펄스 값
	GetMotorCurrentPosStatus();
	Sleep(10);

	// 모터 +/- 리미트 센서
	GetMotorLimitStatus();
	Sleep(10);

	// 모터 홈 센서
	GetMotorHomeStatus();
	Sleep(10);

	// 모터 알람 센서
	GetMotorAlarmStatus();
}

//=============================================================================
// Method		: GetMotorAmpStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:14
// Desc.		:
//=============================================================================
void CWnd_MotionTable::GetMotorAmpStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_Sen == NULL)
		return;

	for (UINT nAxis = 0; nAxis < m_nAxisMax; nAxis++)
	{
		if(m_pstDevice->GetAxisUseStatus(nAxis))
		{
			if (m_pstDevice->GetAmpStatus(nAxis))
				m_st_Sen[nAxis][ST_MT_POWER_NAME].SetColorStyle(CVGStatic::ColorStyle_Green);
			else
				m_st_Sen[nAxis][ST_MT_POWER_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
		}
	}  
}

//=============================================================================
// Method		: GetMotorCurrentPosStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:18
// Desc.		:
//=============================================================================
void CWnd_MotionTable::GetMotorCurrentPosStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_Sen == NULL)
		return;

	CString strPos;

	//!SH _190130: 펄스 값 뒤에 환산 값 출력 되도록, 확인 필요
	for (UINT nAxis = 0; nAxis < m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			if (nAxis < AX_StageDistance)
			{
				strPos.Format(_T("%.0f / %6.2fmm"), m_pstDevice->GetCurrentPos(nAxis), m_pstDevice->GetCurrentPos(nAxis) / 690 );
				m_st_Sen[nAxis][ST_MT_POS_NAME].SetText(strPos);
			} 
			else if (nAxis == AX_StageDistance)
			{
				strPos.Format(_T("%.0f / %6.2fmm"), m_pstDevice->GetCurrentPos(nAxis), (106 + (m_pstDevice->GetCurrentPos(nAxis) / 1000)));
				m_st_Sen[nAxis][ST_MT_POS_NAME].SetText(strPos);
			}
		}
	}
}

//=============================================================================
// Method		: GetMotorMotionStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 8:04
// Desc.		:
//=============================================================================
void CWnd_MotionTable::GetMotorMotionStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_Sen == NULL)
		return;

	for (UINT nAxis = 0; nAxis < m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			if (m_pstDevice->GetMotionStatus(nAxis))
				m_st_Sen[nAxis][ST_MT_MOTION_NAME].SetColorStyle(CVGStatic::ColorStyle_Green);
			else
				m_st_Sen[nAxis][ST_MT_MOTION_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
		}
	}
}

//=============================================================================
// Method		: GetMotorOriginStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:25
// Desc.		:
//=============================================================================
void CWnd_MotionTable::GetMotorOriginStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_Sen == NULL)
		return;

	for (UINT nAxis = 0; nAxis < m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			if (m_pstDevice->GetOriginStatus(nAxis))
				m_st_Sen[nAxis][ST_MT_ORI_NAME].SetColorStyle(CVGStatic::ColorStyle_Green);
			else
				m_st_Sen[nAxis][ST_MT_ORI_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
		}
	}
}

//=============================================================================
// Method		: GetMotorLimitStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:28
// Desc.		:
//=============================================================================
void CWnd_MotionTable::GetMotorLimitStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_Sen == NULL)
		return;
	
	for (UINT nAxis = 0; nAxis < m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			// + 센서 UNUSE 일 경우
			if (m_pstDevice->GetPosSensorLevel(nAxis) == 2)
			{
				m_st_Sen[nAxis][ST_MT_HLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Black2007);
			}
			else
			{
				if (m_pstDevice->GetPosSensorStatus(nAxis))
					m_st_Sen[nAxis][ST_MT_HLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Red);
				else
					m_st_Sen[nAxis][ST_MT_HLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
			}
		}
		
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			// - 센서 UNUSE 일 경우
			if (m_pstDevice->GetNegSensorLevel(nAxis) == 2)
			{
				m_st_Sen[nAxis][ST_MT_LLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Black2007);
			}
			else
			{
				if (m_pstDevice->GetNegSensorStatus(nAxis))
					m_st_Sen[nAxis][ST_MT_LLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Red);
				else
					m_st_Sen[nAxis][ST_MT_LLIT_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
			}
		}
	}
}

//=============================================================================
// Method		: GetMotorHomeStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:41
// Desc.		:
//=============================================================================
void CWnd_MotionTable::GetMotorHomeStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_Sen == NULL)
		return;
	
	for (UINT nAxis = 0; nAxis < m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			// HOME 센서 UNUSE 일 경우
			if (m_pstDevice->GetHomeSensorLevel(nAxis) == 2)
			{
				m_st_Sen[nAxis][ST_MT_HOME_NAME].SetColorStyle(CVGStatic::ColorStyle_Black2007);
			}
			else
			{
				if (m_pstDevice->GetHomeSensorStatus(nAxis))
					m_st_Sen[nAxis][ST_MT_HOME_NAME].SetColorStyle(CVGStatic::ColorStyle_Green);
				else
					m_st_Sen[nAxis][ST_MT_HOME_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
			}
		}
	}
}

//=============================================================================
// Method		: GetMotorAlarmStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 9:41
// Desc.		:
//=============================================================================
void CWnd_MotionTable::GetMotorAlarmStatus()
{
	if (m_pstDevice == NULL)
		return;

	if (m_st_Sen == NULL)
		return;
	
	for (UINT nAxis = 0; nAxis < m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			// ALRAM 센서 UNUSE 일 경우
			if (m_pstDevice->GetAlramSensorLevel(nAxis) == 2)
			{
				m_st_Sen[nAxis][ST_MT_ALRAM_NAME].SetColorStyle(CVGStatic::ColorStyle_Black2007);
			}
			else
			{
				if (m_pstDevice->GetAlarmSenorStatus(nAxis))
					m_st_Sen[nAxis][ST_MT_ALRAM_NAME].SetColorStyle(CVGStatic::ColorStyle_Red);
				else
					m_st_Sen[nAxis][ST_MT_ALRAM_NAME].SetColorStyle(CVGStatic::ColorStyle_Black);
			}
		}
	}
}

//=============================================================================
// Method		: SetUpdateAxisCnt
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/30 - 14:51
// Desc.		:
//=============================================================================
void CWnd_MotionTable::SetUpdateAxisCnt()
{
// 	switch (m_nSysType)
// 	{
// 	case Sys_Focusing:
// 		break;
// 
// 	case Sys_2D_Cal:
// 		break;
// 
// 	case Sys_Image_Test:
// 		break;
// 
// 	case Sys_3D_Cal:
// 		break;
// 
// 	default:
// 		break;
// 	}
}

//=============================================================================
// Method		: SetUpdataAxisName
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/4/4 - 13:24
// Desc.		:
//=============================================================================
void CWnd_MotionTable::SetUpdataAxisName()
{
	CString strName;

	for (UINT nAxis = 0; nAxis < m_nAxisMax; nAxis++)
	{
		if (m_pstDevice->m_AllMotorData.pMotionParam == NULL)
			return;

		strName = m_pstDevice->m_AllMotorData.pMotionParam[nAxis].szAxisName;
		m_st_Sen[nAxis][ST_MT_AXIS_NAME].SetWindowText(strName);
	}

	if (GetSafeHwnd())
	{
		CRect rc;
		GetClientRect(rc);
		OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	}
}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/4/4 - 17:18
// Desc.		:
//=============================================================================
void CWnd_MotionTable::SetUpdateDataReset(UINT nAxis)
{
	for (UINT nIdx = 0; nIdx < m_nAxisMax; nIdx++)
		m_st_Sen[nIdx][ST_MT_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_Default);

	m_st_Sen[nAxis][ST_MT_AXIS_NAME].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	// AXIS NAME
	SetUpdataAxisName();
}

//=============================================================================
// Method		: SetDeleteTimer
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/28 - 12:33
// Desc.		:
//=============================================================================
void CWnd_MotionTable::SetDeleteTimer()
{
	DeleteTimerSensorCheck();
}

//=============================================================================
// Method		: StartThread_SensorMon
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/3/11 - 20:03
// Desc.		:
//=============================================================================
BOOL CWnd_MotionTable::StartThread_SensorMon()
{
	if (NULL != m_hThr_SensorMon)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_SensorMon, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return FALSE;
		}
	}

	if (NULL != m_hThr_SensorMon)
	{
		CloseHandle(m_hThr_SensorMon);
		m_hThr_SensorMon = NULL;
	}

	m_hThr_SensorMon = HANDLE(_beginthreadex(NULL, 0, Thread_SensorMon, this, 0, NULL));

	return TRUE;
}

//=============================================================================
// Method		: StopThread_SensorMon
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/3/14 - 16:56
// Desc.		:
//=============================================================================
BOOL CWnd_MotionTable::StopThread_SensorMon()
{
	SetEvent(m_hExitEvent);
	m_bLoop_SensorMon = FALSE;

	Sleep(150);

	if (NULL != m_hThr_SensorMon)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_SensorMon, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TRACE(_T("TerminateThread : m_hThr_SensorMon  \n"));
			TerminateThread(m_hThr_SensorMon, dwExitCode);
			WaitForSingleObject(m_hThr_SensorMon, WAIT_ABANDONED);
		}

		CloseHandle(m_hThr_SensorMon);
		m_hThr_SensorMon = NULL;
	}

	if (NULL != m_hExitEvent)
		CloseHandle(m_hExitEvent);

	return TRUE;
}
