﻿#ifndef List_AngleResult_h__
#define List_AngleResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_AngleResult
{
	AngResult_Object = 0,
	AngResult_Value,
	AngResult_MinSpec,
	AngResult_MaxSpec,
	AngResult_Result,
	AngResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_AngleResult[] =
{
	_T(""),
	_T("Value"),
	_T("Min Spec"),
	_T("Max Spec"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_AngleResult
{
	AngResult_ItemNum = 4,
};

static LPCTSTR	g_lpszItem_AngleResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_AngleResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_AngleResult[] =
{
	80,
	80,
	80,
	80,
	80,
};
// List_AngleInfo

class CList_FOVResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_FOVResult)

public:
	CList_FOVResult();
	virtual ~CList_FOVResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Angle(ST_LT_TI_Angle* pstAngle)
	{
		if (pstAngle == NULL)
			return;

		m_pstAngle = pstAngle;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_Angle*  m_pstAngle;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_AngleInfo_h__
