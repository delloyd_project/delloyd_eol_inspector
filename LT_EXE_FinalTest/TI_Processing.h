﻿#ifndef TI_Processing_h__
#define TI_Processing_h__

#pragma once

#include "CatWrapper.h"
#include "Def_DataStruct.h"
#include "TI_PicControl.h"
#include "Wnd_SiteInfo.h"
#include "LT_Option.h"
#include "CommonFunction.h"

// CTI_Processing

#define CAM_VIEW_NUM	0

class CTI_Processing
{
public:
	CTI_Processing();
	virtual ~CTI_Processing();

protected:

	ST_Device*				m_pstDevice;
	ST_ModelInfo*			m_pstModelInfo;

	ST_LT_TI_Current*	 	m_pstCurrent;
	ST_LT_TI_SFR*		 	m_pstSFR;
	ST_LT_TI_CenterPoint*	m_pstCenterPoint;
	ST_LT_TI_Rotate*	 	m_pstRotate;
	ST_LT_TI_BlackSpot*	 	m_pstParticle;
	
	CTI_PicControl			m_TIPicControl;
	
	DWORD					m_dwWidth;
	DWORD					m_dwHeight;
	LPBYTE					m_pImageBuf;
	DWORD					m_dwImageBufSize;

	LPWORD					m_pImageSourceBuf;
	WORD					m_wImageSourceBufSize;

	HWND					m_hImageWnd;		// 이미지 저장용 핸들
	HWND					m_hOwnerWnd;		// 이 클래스를 호출한 윈도우 핸들	
	void					DeleteMemory	();

public:

	// 이미지 저장용
	void SetImagehwnd(HWND hImageWnd)
	{
		m_hImageWnd = hImageWnd;
	}

	// Log Mssg
	CWLog			m_Log;
	void SetLogMsgID(HWND hOwnerWnd, UINT nWM_ID, UINT nLogType = 0)
	{
		m_hOwnerWnd = hOwnerWnd;
		m_Log.SetOwner(m_hOwnerWnd, nWM_ID);
		m_Log.SetLogType(nLogType);
	}

	stLT_Option*		m_pstOption;

	void SetLTOption(stLT_Option* pstOption)
	{
		if (pstOption == NULL)
			return;

		m_pstOption = pstOption;
	};


	void	SetPtr_Device				(ST_Device*			pstDevice);
	void	SetPtr_ModelInfo			(ST_ModelInfo*		pstModelInfo);

	void	OnDisplayPicSetting			(UINT nTestItemID);

	// 영상 확인
	BOOL	CameraConnect				();

	// 영상 가져오기
	BOOL	GetTestImageBuffer			(UINT nCh = 0);
	BOOL	GetTestSourceImageBuffer	(UINT nCh = 0);

	// 비전 영상 가져오기
	BOOL	GetVisionTestImageBuffer	(UINT nCh = 0);

	// 카메라 전원
	UINT	CameraPowerOnOff			(BOOL bMode, UINT nCh = 0);

	// 카메라 전류
	UINT	CameraCurrent				();

	// EIAJ
	UINT	EIAJRun						(BOOL bMode);

	// SFR
	UINT	SFRRun						();

	// 보임량 광축
	UINT	CenterPointRun				(BOOL bPicMode = TRUE);

	// 보임량 광축 조정
	UINT	CenterPointTuningRun		();

	// 로테이트
	UINT	RotateRun					();

	// 로테이트 조정
	UINT	RotateTuningRun				();

	// 이물
	UINT	ParticleRun					();

	// 다 측정
	UINT	ActiveAlignRun				();

	UINT	ActiveAlignTuningRun		();

	//  메뉴얼 버튼 제어
	UINT	ManualBtnCtrl(enManual_Commend enCmd);

	// 이미지 저장
	BOOL	ImageSaveOriginal			();
	BOOL	ImageSaveGray				();
	BOOL	ImageSavePic				();

	BOOL	GetSaveImageFilePath		(__out CString& szOutPath);

	// Interlock
	//UINT	InterlockRun				();
	//UINT	InterlockFixedRun			(__in BOOL bOnOff);

	// Unloading
// 	UINT	UnloadingRun				();
// 
// 	// 콜렛 비전 측정
// 	UINT	ColletVisionRun				(__in double dDisplace, __out double& dResultDeg, __out double& dInitPosR);
// 
// 	// 변위 측정
// 	UINT	DisplaceDetectRun			(__out double& dResultDisplace);
// 
// 	// AF 위치 이동
// 	UINT	AFPositionRun				(__in double dDisplace, __out double& dInitPosY);
// 
// 	// Auto 포커싱
// 	UINT	AutoFocusingRun				(__in double dDisplace);
// 
// 	// 메뉴얼 포커싱
// 	UINT	ManualFocusingRun			(__in double dDegree);
// 
// 	// 포커싱 초기위치
// 	UINT	InitFocusingRun				(__in double dInitY, __in double dInitR);

};

#endif // TI_Processing_h__
