﻿//*****************************************************************************
// Filename	: Wnd_WorklistIRFilterManual.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistIRFilterManual_h__
#define Wnd_WorklistIRFilterManual_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_IRFilterManual.h"

//=============================================================================
// Wnd_WorklistIRFilterManual
//=============================================================================
class CWnd_WorklistIRFilterManual : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistIRFilterManual)

public:
	CWnd_WorklistIRFilterManual();
	virtual ~CWnd_WorklistIRFilterManual();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

//	CList_Work_IRFilterManual				m_list_IRFilterManual[TICnt_IRFilterManual];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistIRFilterManual_h__


