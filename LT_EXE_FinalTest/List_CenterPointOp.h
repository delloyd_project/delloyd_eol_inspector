﻿#ifndef List_CenterPointOp_h__
#define List_CenterPointOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNumCenterPointOp
{
	CpOp_Object = 0,
	CpOp_PosX,
	CpOp_PosY,
	CpOp_Width,
	CpOp_Height,
	CpOp_Mark,
	CpOp_MaxCol,

};

static LPCTSTR	g_lpszHeader_CenterPointOp[] =
{
	_T(""),
	_T("CenterX"),
	_T("CenterY"),
	_T("Width"),
	_T("Height"),
	_T("Mark"),
	NULL
};

typedef enum enListItemNum_CenterPointOp
{
	CpOp = 0,
	CpOp_ItemNum,
};

static LPCTSTR	g_lpszItem_CenterPointOp[] =
{
	_T("Center"),
	NULL
};

const int	iListAglin_CenterPointOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_CenterPointOp[] =
{
	80,
	80,
	80,
	80,
	80,
	80,
};
// List_CenterPointInfo

class CList_CenterPointOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_CenterPointOp)

public:
	CList_CenterPointOp();
	virtual ~CList_CenterPointOp();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_CenterPoint(ST_LT_TI_CenterPoint* pstCenterPoint)
	{
		if (pstCenterPoint == NULL)
			return;

		m_pstCenterPoint = pstCenterPoint;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};

protected:

	ST_LT_TI_CenterPoint*  m_pstCenterPoint;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData			(UINT nRow, UINT nCol, int iValue);
	BOOL	UpdateCelldbData		(UINT nRow, UINT nCol, double dValue);

public:

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel	(UINT nFlags, short zDelta, CPoint pt);

	afx_msg void	OnEnKillFocusEdit		();
	afx_msg void	OnEnKillFocusCombo		();
	afx_msg void	OnEnSelectFocusCombo	();
};

#endif // List_CenterPointInfo_h__
