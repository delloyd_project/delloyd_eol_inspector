﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_DAQ.cpp
// Created	:	2016/3/14 - 10:57
// Modified	:	2016/3/14 - 10:57
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_DAQ.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_DAQ.h"
#include "resource.h"
#include "Def_WindowMessage.h"

typedef enum Model_ID
{
	IDC_CB_Cam_BoardNum = 1001,
	IDC_CB_Cam_Data_Type,
	IDC_CB_Cam_Signal_Type,
	IDC_CB_Cam_MIPI_Lane,
	IDC_CB_Cam_Sensor_Type,
	IDC_CB_Cam_Color_Array,
	IDC_CB_Cam_Hsync_Polarity,
	IDC_CB_Cam_Pclk_Polarity,
	IDC_CB_Cam_DVal_Use,
	IDC_CB_Cam_Clock_Select,
	IDC_CB_Cam_Clock_Use,
	IDC_CB_Cam_I2c_Enable,
	IDC_CB_Cam_VideoCut,
	IDC_CB_Cam_Exposure,
	IDC_ED_Cam_Clock_Hz ,
	IDC_ED_Cam_Width_Multiple,
	IDC_ED_Cam_Height_Multiple,
};

static LPCTSTR lpszBoard[] = 
{
	_T("0"),
	_T("1"),
	_T("2"),
	_T("3"),
	_T("4"),
	_T("5"),
};

static LPCTSTR lpszDataType[] =
{
	_T("8 bit"),
	_T("16 bit"),
	_T("24 bit"),
	_T("32 bit"),
	_T("16 bit YUV"),
};

static LPCTSTR lpszSignalType[] =
{
	_T("Vsync, Hsync, De"),
	_T("BT656"),
	_T("MIPI"),
};

static LPCTSTR lpszMIPILane[] =
{
	_T("1 Lane"),
	_T("2 Lane"),
	_T("Empty"),
	_T("4 Lane"),
};

static LPCTSTR lpszSensorType[] =
{
	_T("YCBCR"),
	_T("BAYER 10"),
	_T("BAYER 12"),
	_T("RGB888"),
	_T("GRAY 12"),
};

static LPCTSTR lpszColorArr[] =
{
	_T("YCbYCr / BGGR"),
	_T("YCrYCb / RGGB"),
	_T("CrYCbY / GBRG"),
	_T("CbYCrY / GRBG"),
};

static LPCTSTR lpszHsyncPol[] =
{
	_T("Normal"),
	_T("Inverse"),
};

static LPCTSTR lpszPclkPol[] =
{
	_T("Rising Edge"),
	_T("Falling Edge"),
};

static LPCTSTR lpszDValUse[] =
{
	_T("Hsync Use"),
	_T("DVAL Use"),
};

static LPCTSTR lpszClockSelect[] =
{
	_T("Fixed Clock"),
	_T("Program Clock"),
};

static LPCTSTR lpszClockOff[] =
{
	_T("Off"),
	_T("On"),
};

static LPCTSTR lpszVideoCut[] =
{
	_T("None"),
	_T("OddFrame"),
	_T("EvenFrame"),
};

// CWnd_Cfg_DAQ
IMPLEMENT_DYNAMIC(CWnd_Cfg_DAQ, CWnd_BaseView)

CWnd_Cfg_DAQ::CWnd_Cfg_DAQ()
{
	VERIFY(m_font_Data.CreateFont(
		17,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_DAQ::~CWnd_Cfg_DAQ()
{
	m_font_Data.DeleteObject();
}


BEGIN_MESSAGE_MAP(CWnd_Cfg_DAQ, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CWnd_Cfg_DAQ message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
int CWnd_Cfg_DAQ::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < STI_DAQ_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 10.0F);
		m_st_Item[nIdx].Create(g_szDAQ_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < CBO_DAQ_MAXNUM; nIdex++)
		m_cd_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_Cam_BoardNum + nIdex);

	for (UINT nIdx = 0; nIdx < EDT_DAQ_MAXNUM; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_Cam_Clock_Hz + nIdx);
		m_ed_Item[nIdx].SetFont(&m_font_Data);
	}

	InitUI();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iCnt = 0;

	int iMagrin			= 10;
	int iSpacing		= 5;
	int iCateSpacing	= 5;

	int iLeft	= iMagrin;
	int iTop	= iMagrin;
	int iWidth	= cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	int iCtrlWidth	= 0;
	int iCtrlHeight = 25;
	int iHalfWidth	= (iWidth - iCateSpacing- iCateSpacing) / 3;
	int iLeftSub  	= 0;
	int iTempWidth  = 0;
	int iTempTop	= 0;

	iTop = iMagrin;
	iCtrlWidth = iHalfWidth;

	for (UINT nIdx = 0; nIdx < STI_DAQ_MAXNUM; nIdx++)
	{
// 		if (nIdx == STI_DAQ_MAXNUM / 2)
// 		{
// 			iTop = iMagrin;
// 			iLeft = iLeftSub + iCtrlWidth + iMagrin;
// 		}

		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
		
		iLeftSub = iLeft + iCtrlWidth + iCateSpacing;

		switch (nIdx)
		{
		case STI_DAQ_Cam_Clock_Hz:
			m_ed_Item[EDT_DAQ_ClockHz].MoveWindow(iLeftSub, iTop, iCtrlWidth, iCtrlHeight);
			iCnt--;
			break;
		case STI_DAQ_Cam_Width_Multiple:
			m_ed_Item[EDT_DAQ_WidthMultiple].MoveWindow(iLeftSub, iTop, iCtrlWidth, iCtrlHeight);
			iCnt--;
			break;
		case STI_DAQ_Cam_Height_Multiple:
			m_ed_Item[EDT_DAQ_HeightMultiple].MoveWindow(iLeftSub, iTop, iCtrlWidth, iCtrlHeight);
			iCnt--;
			break;
		case STI_DAQ_Cam_Exposure:
			m_ed_Item[EDT_DAQ_Exposure].MoveWindow(iLeftSub, iTop, iCtrlWidth, iCtrlHeight);
			iCnt--;
			break;
		default:
			m_cd_Item[iCnt].MoveWindow(iLeftSub, iTop, iCtrlWidth, iCtrlHeight);
			break;
		}

		iCnt++;
		iTop += iCtrlHeight + iCateSpacing;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_DAQ::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_DAQ::PreTranslateMessage(MSG* pMsg)
{
	return CWnd_BaseView::PreTranslateMessage(pMsg);
}


//=============================================================================
// Method		: InitUI
// Access		: public  
// Returns		: void
// Parameter	: 
// Qualifier	:
// Last Update	: 2017/6/28 - 19:39
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::InitUI()
{
	m_ed_Item[EDT_DAQ_ClockHz].SetWindowTextW(_T(""));
	m_ed_Item[EDT_DAQ_WidthMultiple].SetWindowTextW(_T(""));
	m_ed_Item[EDT_DAQ_HeightMultiple].SetWindowTextW(_T(""));
	m_ed_Item[EDT_DAQ_Exposure].SetWindowTextW(_T(""));

	for (int i = 0; i < DAQBoardNumber_Cnt; i++)
		m_cd_Item[CBO_DAQ_BoardNumber].AddString(lpszBoard[i]);

	for (int i = 0; i < DAQ_Data_Cnt; i++)
		m_cd_Item[CBO_DAQ_DataType].AddString(lpszDataType[i]);

	for (int i = 0; i < DAQ_Video_Cnt; i++)
		m_cd_Item[CBO_DAQ_SignalType].AddString(lpszSignalType[i]);

	for (int i = 0; i < DAQ_HsyncPol_Cnt; i++)
		m_cd_Item[CBO_DAQ_HsyncPolarity].AddString(lpszHsyncPol[i]);

	for (int i = 0; i < DAQ_PclkPol_Cnt; i++)
		m_cd_Item[CBO_DAQ_PClkPolarity].AddString(lpszPclkPol[i]);

	for (int i = 0; i < DAQ_DeUse_Cnt; i++)
		m_cd_Item[CBO_DAQ_DValUse].AddString(lpszDValUse[i]);

	for (int i = 0; i < DAQMIPILane_Cnt; i++)
		m_cd_Item[CBO_DAQ_MIPILane].AddString(lpszMIPILane[i]);

	for (int i = 0; i < enImgSensorType_Cnt; i++)
		m_cd_Item[CBO_DAQ_SensorType].AddString(lpszSensorType[i]);

	for (int i = 0; i < DAQClockSelect_Cnt; i++)
		m_cd_Item[CBO_DAQ_ClockSelect].AddString(lpszClockSelect[i]);

	for (int i = 0; i < DAQClockUse_Cnt; i++)
		m_cd_Item[CBO_DAQ_ClockUse].AddString(lpszClockOff[i]);

	for (int i = 0; i < Conv_Max; i++)
		m_cd_Item[CBO_DAQ_ColorArray].AddString(lpszColorArr[i]);

	for (int i = 0; i < Crop_Max; i++)
		m_cd_Item[CBO_DAQ_VideoCut].AddString(lpszVideoCut[i]);
}


//=============================================================================
// Method		: GetUIData
// Access		: public  
// Returns		: void
// Parameter	: __out ST_LVDSInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/6/28 - 19:39
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::GetUIData(__out ST_LVDSInfo& stLVDSInfo)
{
	CString strData;
	UINT nData;
	
	nData = m_cd_Item[CBO_DAQ_BoardNumber].GetCurSel();
	stLVDSInfo.nBoardNo[Socket_1] = nData;

	nData = m_cd_Item[CBO_DAQ_SensorType].GetCurSel();
	stLVDSInfo.stLVDSOption.nSensorType = (enImgSensorType)nData;

	nData = m_cd_Item[CBO_DAQ_ColorArray].GetCurSel();
	stLVDSInfo.stLVDSOption.nConvFormat = (enConvFormat)nData;

	nData = m_cd_Item[CBO_DAQ_ClockSelect].GetCurSel();
	stLVDSInfo.stLVDSOption.nClockSelect = (enDAQClockSelect)nData;

	nData = m_cd_Item[CBO_DAQ_ClockUse].GetCurSel();
	stLVDSInfo.stLVDSOption.nClockUse = (enDAQClockUse)nData;

	nData = m_cd_Item[CBO_DAQ_DataType].GetCurSel();
	stLVDSInfo.stLVDSOption.nDataMode = (enDAQDataMode)nData;

	nData = m_cd_Item[CBO_DAQ_DValUse].GetCurSel();
	stLVDSInfo.stLVDSOption.nDvalUse = (enDAQDvalUse)nData;

	nData = m_cd_Item[CBO_DAQ_HsyncPolarity].GetCurSel();
	stLVDSInfo.stLVDSOption.nHsyncPolarity = (enDAQHsyncPolarity)nData;

	nData = m_cd_Item[CBO_DAQ_MIPILane].GetCurSel();
	stLVDSInfo.stLVDSOption.nMIPILane = (enDAQMIPILane)nData;

	nData = m_cd_Item[CBO_DAQ_PClkPolarity].GetCurSel();
	stLVDSInfo.stLVDSOption.nPClockPolarity = (enDAQPclkPolarity)nData;

	nData = m_cd_Item[CBO_DAQ_SignalType].GetCurSel();
	stLVDSInfo.stLVDSOption.nVideoMode = (enDAQVideoMode)nData;

	nData = m_cd_Item[CBO_DAQ_VideoCut].GetCurSel();
	stLVDSInfo.stLVDSOption.nCropFrame = (enCropFrame)nData;

	m_ed_Item[EDT_DAQ_ClockHz].GetWindowTextW(strData);
	stLVDSInfo.stLVDSOption.dwClock	= (DWORD)_ttol(strData);

	m_ed_Item[EDT_DAQ_WidthMultiple].GetWindowTextW(strData);
	stLVDSInfo.stLVDSOption.dWidthMultiple = _ttof(strData);

	m_ed_Item[EDT_DAQ_HeightMultiple].GetWindowTextW(strData);
	stLVDSInfo.stLVDSOption.dHeightMultiple = _ttof(strData);

	m_ed_Item[EDT_DAQ_Exposure].GetWindowTextW(strData);
	stLVDSInfo.stLVDSOption.dShiftExp = _ttof(strData);
}


//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_LVDSInfo * pModelInfo
// Qualifier	:
// Last Update	: 2017/6/28 - 19:39
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::SetUIData(__in const ST_LVDSInfo* pstLVDSInfo)
{
	if (pstLVDSInfo == NULL)
		return;

	//ui
	CString strData;
	UINT nData;

	nData = pstLVDSInfo->nBoardNo[Socket_1];
	m_cd_Item[CBO_DAQ_BoardNumber].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nSensorType;
	m_cd_Item[CBO_DAQ_SensorType].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nConvFormat;
	m_cd_Item[CBO_DAQ_ColorArray].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nClockSelect;
	m_cd_Item[CBO_DAQ_ClockSelect].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nClockUse;
	m_cd_Item[CBO_DAQ_ClockUse].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nDataMode;
	m_cd_Item[CBO_DAQ_DataType].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nDvalUse;
	m_cd_Item[CBO_DAQ_DValUse].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nHsyncPolarity;
	m_cd_Item[CBO_DAQ_HsyncPolarity].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nMIPILane;
	m_cd_Item[CBO_DAQ_MIPILane].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nPClockPolarity;
	m_cd_Item[CBO_DAQ_PClkPolarity].SetCurSel(nData);

	nData = pstLVDSInfo->stLVDSOption.nVideoMode;
	m_cd_Item[CBO_DAQ_SignalType].SetCurSel(nData);

	strData.Format(_T("%d"), pstLVDSInfo->stLVDSOption.dwClock);
	m_ed_Item[EDT_DAQ_ClockHz].SetWindowTextW(strData);

	strData.Format(_T("%.1f"), pstLVDSInfo->stLVDSOption.dWidthMultiple);
	m_ed_Item[EDT_DAQ_WidthMultiple].SetWindowTextW(strData);

	strData.Format(_T("%.1f"), pstLVDSInfo->stLVDSOption.dHeightMultiple);
	m_ed_Item[EDT_DAQ_HeightMultiple].SetWindowTextW(strData);

	strData.Format(_T("%.1f"), pstLVDSInfo->stLVDSOption.dShiftExp);
	m_ed_Item[EDT_DAQ_Exposure].SetWindowTextW(strData);

	nData = pstLVDSInfo->stLVDSOption.nCropFrame;
	m_cd_Item[CBO_DAQ_VideoCut].SetCurSel(nData);
}


//=============================================================================
// Method		: SetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_LVDSInfo * pModelInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::SetModelInfo(__in const ST_ModelInfo* pstModelInfo)
{
	SetUIData(&pstModelInfo->stLVDSInfo);
}

//=============================================================================
// Method		: GetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_LVDSInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/1/4 - 10:42
// Desc.		:
//=============================================================================
void CWnd_Cfg_DAQ::GetModelInfo(__out ST_ModelInfo& stModelInfo)
{
	GetUIData(stModelInfo.stLVDSInfo);

}