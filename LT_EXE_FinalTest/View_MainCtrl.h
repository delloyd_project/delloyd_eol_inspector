﻿//*****************************************************************************
// Filename	: View_MainCtrl.h
// Created	: 2010/11/26
// Modified	: 2016/06/07
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef View_MainCtrl_h__
#define View_MainCtrl_h__

#pragma once

#include "TestManager_EQP.h"

// UI
#include "Wnd_MainView.h"
#include "Wnd_RecipeView.h"
#include "Wnd_DeviceView.h"
#include "Wnd_MotorView.h"
#include "Wnd_WorklistView.h"
#include "Wnd_LogView.h"
#include "Wnd_AccessMode.h"
#include "Wnd_ModelMode.h"
#include "Wnd_MessageView.h"
#include "Wnd_Origin.h"

#include "SplashScreenEx.h"
#include "Dlg_Barcode.h"
#include "Dlg_Popup.h"
#include "Dlg_ParticleManual.h"
#include "Dlg_LEDTest.h"
#include "Dlg_CenterPtTunning.h"
#include "Dlg_FailConfirm.h"
#include "Dlg_MasterTest.h"
#include "Dlg_MasterMode.h"

typedef enum _enumSubView
{
	SUBVIEW_MAIN	= 0,	
	SUBVIEW_RECIPE,
	SUBVIEW_IO,
	SUBVIEW_MOTOR,
	SUBVIEW_WORKLIST,
	SUBVIEW_LOG,
	SUBVIEW_MAX,
}enumSubView;


//=============================================================================
// CView_MainCtrl 창
//=============================================================================
class CView_MainCtrl : public CWnd, public CTestManager_EQP
{
// 생성입니다.
public:
	CView_MainCtrl();
	virtual ~CView_MainCtrl();

protected:

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnPaint					();
	afx_msg BOOL	OnEraseBkgnd			(CDC* pDC);
	afx_msg HBRUSH	OnCtlColor				(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void	OnTimer					(UINT_PTR nIDEvent);

	// 로그 메세지
	afx_msg	LRESULT	OnLogMsg				(WPARAM wParam, LPARAM lParam);

	// 검사 제어
	afx_msg	LRESULT	OnTestStart				(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnTestStop				(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnTestInit				(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT		OnTestCompleted			(WPARAM wParam, LPARAM lParam);
	
	// MES 통신
	afx_msg LRESULT	OnCommStatus_MES		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnRecvMES				(WPARAM wParam, LPARAM lParam);

	// 그래버 보드 통신
	afx_msg LRESULT	OnCameraSelect			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnCameraChgStatus		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnCameraRecvVideo		(WPARAM wParam, LPARAM lParam);

	// 검사 제어
	afx_msg	LRESULT	OnSwitchPermissionMode	(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnChangeModel			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnChangeMode			(WPARAM wParam, LPARAM lParam);
	BOOL InitStartDeviceProgress();
	BOOL MotorOrigin();
	afx_msg LRESULT	OnDeviceCtrl			(WPARAM wParam, LPARAM lParam);	
	
	// 통신 데이터
	afx_msg LRESULT	OnRecvBarcode			(WPARAM wParam, LPARAM lParam);

	// 시리얼 통신 데이터
	afx_msg LRESULT	OnRecvMainBrd			(WPARAM wParam, LPARAM lParam);

	// Pogo 카운트
	afx_msg LRESULT	OnPogoCnt_Increase		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnPogoCnt_Update		(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT	OnModelSave				(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnChangeMotor			(WPARAM wParam, LPARAM lParam);
	//afx_msg LRESULT	OnChangeMaintenance		(WPARAM wParam, LPARAM lParam);

	// Digital I/O
	afx_msg LRESULT	OnRecvDIOMon			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnRecvDIOFirstRead		(WPARAM wParam, LPARAM lParam);

	// Manual 검사
	afx_msg LRESULT OnManualTestCmd			(WPARAM wParam, LPARAM lParam);
	// Manual 검사 이미지 저장
	
	// 모터 원점
	afx_msg LRESULT OnMotorOriginAll				(WPARAM wParam, LPARAM lParam);

	// 마스터 측정
	afx_msg LRESULT OnMasterTest						(WPARAM wParam, LPARAM lParam);

	// 마스터 모드
	afx_msg LRESULT OnMasterMode					(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnMasterSet						(WPARAM wParam, LPARAM lParam);
	
	DECLARE_MESSAGE_MAP()
	
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	// 배경색 처리용
	CBrush				m_brBkgr;
	CFile_Model			m_fileModel;

	//-----------------------------------------------------
	// 차일드 윈도우 구분용
	//-----------------------------------------------------	
	UINT				m_nWndIndex;
	CWnd*				m_pWndPtr[SUBVIEW_MAX];
	CWnd_MainView		m_wnd_MainView;

	CWnd_RecipeView		m_wnd_RecipeView;
	CWnd_MotorView		m_wnd_MotorView;
	CWnd_DeviceView		m_wnd_DeviceView;
	CWnd_WorklistView	m_wnd_WorklistView;
	CWnd_LogView		m_wnd_LogView;
	CWnd_AccessMode		m_wnd_AccessMode;
	CWnd_ModelMode		m_wnd_ModelMode;
	CWnd_Origin			m_wnd_Origin;

	// 통신 상태 표시 Pane의 포인터
	CWnd* m_pwndCommPane;

	// 바코드 입력 다이얼로그
	CDlg_Barcode* m_pdlgBarcode;

	// 마스터 팝업창 다이얼로그
	CDlg_MasterTest m_DlgMasterTest;

	// 마스터 모드 팝업창 다이얼로그
	CDlg_MasterMode m_DlgMasterMode;

	// 비디오 팝업창 다이얼로그
	CDlg_ParticleManual m_DlgVideoManual;

	// LED 검사 팝업창 다이얼로그
	CDlg_LEDTest m_DlgLEDTest;

	// 광축 조정 팝업창 다이얼로그
	CDlg_CenterPtTunning m_DlgCenterPtTunning;

	// 불량함 팝업창 다이얼로그
	CDlg_FailConfirm m_DlgFailBox;

	//-----------------------------------------------------
	// 로그/파일 처리
	//-----------------------------------------------------
// 	CLogFile		m_Log_DevLog;
	virtual void	OnInitLogFolder					();
		
	//-----------------------------------------------------
	// 초기 설정 관련
	//-----------------------------------------------------
	// 생성자에서 초기화 할 세팅
	void			InitConstructionSetting			();
	// Window 생성 후 세팅
	void			InitUISetting					();
	// 주변장치들 기본 설정
	void			InitDeviceSetting				();
	// 모델, 모터 설정 파일 체크
	void			InitEVMS_EnvFile				(__in LPCTSTR szModelFile);
	void			InitEVMS_EnvFile_All			();



	//-----------------------------------------------------
	// 통신 연결 상태
	//-----------------------------------------------------

	// 그래버 보드
	virtual void	OnSetStatus_GrabberBrd_ComArt	(__in BOOL bConnect);
	//virtual void	OnSetStatus_GrabberBrd_DAQ		(__in BOOL bConnect);

	// 바코드 리더기 통신 연결 상태
	virtual void	OnSetStatus_BCR					(__in UINT nConnect);

	// 라벨 프린터 통신 연결 상태
	virtual void	OnSetStatus_LabelPrinter		(__in UINT nConnect);

	// PCB 보드 통신 연결상태
	virtual void	OnSetStatus_PCBCamBrd			(__in UINT nConnect, __in UINT nIdxBrd);
	virtual void	OnSetStatus_PCBLightBrd			(__in UINT nConnect, __in UINT nIdxBrd);

	// 모션보드 연결 상태
	virtual void	OnSetStatus_IOBoard				(__in BOOL bConnect);
	virtual void	OnSetStatus_MotorBoard			(__in BOOL bConnect);

	// Digital Indicator 연결 상태
	virtual void	OnSetStatus_Indicator			(__in BOOL bConnect, __in UINT nChIdx = 0);

	// 컴아트 영상 신호 상태
	void			OnSetStatus_Signal_CA			(__in UINT nIndex, __in BOOL bOn);

	// MES 연결 상태
	void			OnSetStatus_MES					(__in UINT nConnect);

	void			OnSetGrabberBrd_ComArt_Reboot	(__in DWORD dwType = VideoState_NTSC);

	// 바코드 입력
	virtual void	OnSet_Barcode					(__in LPCTSTR szBarcode);
	virtual void	OnAddErrorInfo					(__in enEquipErrorCode lErrorCode);

	//-----------------------------------------------------
	// 신호 감지 및 신호별 기능 처리
	//-----------------------------------------------------
	virtual void	OnDIO_UpdateDInSignal			(__in BYTE byBitOffset, __in BOOL bOnOff);
	virtual void	OnDIO_UpdateDOutSignal			(__in BYTE byBitOffset, __in BOOL bOnOff);

	//-----------------------------------------------------
	// UI 업데이트
	//-----------------------------------------------------
	virtual void	OnUpdateElapsedTime_All			(__in DWORD dwTime);
	virtual void	OnUpdateTestResult				(__in enTestEachResult EachResult, __in UINT nStepIdx, __in CString strText);
	virtual	void	OnUpdateTestJudgment			(__in enTestEachResult EachResult);
	virtual	void	OnUpdateTResetResult			();
	virtual	void	OnUpdateAlarmMessage			(__in CString szAlarmMsg);

	virtual void	OnTestFailResultCode_Unit		(__in UINT nUnitIdx, __in enResultCode_TestItem nFailCode);
	virtual void	OnSetInputTime					();
	virtual void	OnSetResetSiteInfo				();
	virtual void	OnSetInsertBarcode				(__in LPCTSTR szBarcode);
	virtual void	OnSetTestInitialize				(__in enTestEachResult EachResult, __in CString strText);	// 검사 초기화
	virtual void	OnSetTestFinalize				(__in enTestEachResult EachResult, __in CString strText);	// 검사 마무리

	virtual void	OnBarcode_Input					();

	virtual void	OnSetLotInfo					();
	virtual void	OnSetMESInfo					();

	// Start / Stop UI Status
	virtual void	OnSetLotStartBtnStatus			(__in BOOL bStatus);
	virtual void	OnSetLotStopBtnStatus			(__in BOOL bStatus);
	virtual void	OnSetStartBtnStatus				(__in BOOL bStatus);
	virtual void	OnSetStopBtnStatus				(__in BOOL bStatus);
	virtual void	OnTestMasterStartUpEvent		(__in BOOL bStatus);

	virtual BOOL	OnGetStartBtnStatus				();
	virtual BOOL	OnGetStopBtnStatus				();

	//Indicator 데이터 
	virtual void	OnSetIndicatorData				(__in float fAxisX,	__in float fAxisY);

	// 팝업된 윈도우 숨김
	virtual void	OnHidePopupUI					();
	
	virtual void	OnSetMasterOffSetData			(__in int iOffsetX, __in int iOffsetY, __in double dbDegree);

	//-----------------------------------------------------
	// 테스트
	//-----------------------------------------------------
	virtual void	OnUpdateSiteCamInfo				();

	// 카메라 데이터 초기화
	virtual void	OnResetCamInfo					();
	
	// Worklist 처리
	virtual void	OnInsertWorklist				();
	virtual void	OnSaveWorklist					();
	virtual void	OnLoadWorklist					();

	virtual void	OnUpdateYield					();
	virtual void	OnSetStatus_PogoCount			();
	
	virtual BOOL	OnChangeLotInfo					(__in enModelStatus LotStatus);
	virtual BOOL	OnChangeModelInfo				();
	virtual void	OnResetYieldCycleTime			();

	//-------------------------------------------------------------------------
	// 모델 파일 불러오기 및 세팅
	inline BOOL		LoadModelInfo					(__in LPCTSTR szModel, __in BOOL bNotifyModelWnd = TRUE);
	void			InitLoadModelInfo				();
	
	// 모터 파일 불러오기 및 세팅
	inline BOOL		LoadMotorInfo					(__in LPCTSTR szMotor, __in BOOL bNotifyModelWnd = TRUE);
	void			InitLoadMotorInfo				();
	
	// 유지 관리 파일 불러오기 및 세팅
	virtual inline BOOL		LoadMaintenanceInfo(__in LPCTSTR szMaintenance, __in BOOL bNotifyModelWnd = TRUE);
	virtual void			InitLoadMaintenanceInfo();

	virtual	BOOL	EVMSPathSearch					(__out CString &szEVNPath);

	//-----------------------------------------------------
	// 영상 작업 처리
	//-----------------------------------------------------
	void			DisplayVideo					(__in UINT nChIdx, __in UINT nGrabType);

	//-----------------------------------------------------
	// 이물 팝업 처리
	//-----------------------------------------------------
	virtual void	OnPopupParticleManualResult		(__out UINT& nTestResult);
	virtual BOOL	OnPopupParticleManualVisible	();

	//-----------------------------------------------------
	// LED 수동 검사 팝업 처리
	//-----------------------------------------------------
	virtual void	OnPopupLEDTestResult			(__in UINT nTestIdx, __out UINT& nTestResult);
	virtual BOOL	OnPopupLEDTestVisible();

	//-----------------------------------------------------
	// 광축 조정 팝업 처리
	//-----------------------------------------------------
	virtual void	OnPopupCenterPtTunningResult();
	virtual BOOL	OnPopupCenterPtTunningVisible();

	//-----------------------------------------------------
	// 불량함 팝업 처리
	//-----------------------------------------------------
	virtual void	OnPopupFailBoxConfirm();
	virtual BOOL	OnPopupFailBoxConfirmVisible();

	virtual void	OnSetMaterSetView				(__in BOOL bEnable);
	virtual void	OnSetResult						(__in enTestEachResult Result, __in CString strText);
//=============================================================================
public: 
//=============================================================================

	afx_msg LRESULT OnManualTestImageSaveCmd(WPARAM wParam, LPARAM lParam);

	// 검사기 종류 설정
	virtual void	SetSystemType				(__in enInsptrSysType nSysType);
	// 로그 메세지 처리용 함수
	virtual void	AddLog						(__in LPCTSTR lpszLog, __in BOOL bError = FALSE, __in UINT nLogType = LOGTYPE_NORMAL, __in BOOL bOnlyLogType = FALSE);
	// 윈도우 배경색 설정용 함수
	void			SetBackgroundColor			(__in COLORREF color, __in BOOL bRepaint = TRUE);
	// 차일드 윈도우 전환 시 사용
	UINT			SwitchWindow				(__in UINT nIndex);
	// 장치 통신 상태 표시 윈도우 포인터 설정
	void			SetCommPanePtr				(__in CWnd* pwndCommPane);
	// 옵션이 변경 되었을 경우 다시 UI나 데이터를 세팅하기 위한 함수
	void			ReloadOption				();
	// 프로그램 로딩 끝난 후 자동 처리를 위한 함수
	void			InitStartProgress			();	
	BOOL			InitStartBoardProgress		();	
	// 프로그램 종료시 처리해야 할 기능들을 처리하는 함수
	void			FinalExitProgress			();
	BOOL			FinalExitBoardProgress		();
	// 수동 바코드 입력
	void			ManualBarcode				();
	// 제어 권한 설정 창 
	void			PermissionView				();
	// 제어 권한 설정 창 
	virtual BOOL	MessageView					(__in CString szText, __in BOOL bMode = FALSE);
	// 제어 권한 설정 창 
	virtual void	PermissionStatsView			();
	// 제어 권한 설정
	virtual void	SetPermissionMode			(__in enPermissionMode nAcessMode);
	// 설비 구동 모드
	virtual void	SetOperateMode				(__in enOperateMode nOperMode);
	// 이미지 저장 - Temp 폴더에 저장한다.
	virtual void	ReportImageSave				(__in enLT_TestItem_ID nTestItem, __in UINT nTestCount, __in SYSTEMTIME* pTime, __in UINT nEachResult);
	void ManualTestImageSave(__in enLT_TestItem_ID nTestItem, IplImage *TestImage);
	// 설비 초기화 (원점 수행) : 설비 오류 시 초기화 하기 위해 사용
	virtual void	EquipmentInit				(__in UINT nCondition = 0);
	virtual void	ManualControl				(WPARAM wParam, LPARAM lParam);
	BOOL			IsRecipeTest				();
	
	void			Test_Process				(__in UINT nTestNo);


//	BOOL MotorOrigin();
};

#endif // View_MainCtrl_h__


