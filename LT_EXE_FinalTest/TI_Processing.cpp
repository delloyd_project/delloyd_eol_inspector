﻿// TI_Processing.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TI_Processing.h"

CTI_Processing::CTI_Processing()
{
	m_pstDevice				= NULL;
	m_pstCurrent			= NULL;
	m_pstEIAJ				= NULL;
	m_pstCenterPoint		= NULL;
	m_pstRotate				= NULL;
	m_pstModelInfo			= NULL;

	m_pImageBuf				= NULL;
	m_dwImageBufSize		= 0;

	m_pImageSourceBuf		= NULL;
	m_wImageSourceBufSize	= 0;

}

CTI_Processing::~CTI_Processing()
{
	DeleteMemory();
}

//=============================================================================
// Method		: DeleteMemory
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/4 - 20:18
// Desc.		:
//=============================================================================
void CTI_Processing::DeleteMemory()
{
	if (NULL != m_pImageBuf)
	{
		delete[] m_pImageBuf;
		m_pImageBuf = NULL;
	}

	m_TlEIAJ.DeleteMemory(m_pstEIAJ);
}

//=============================================================================
// Method		: SetPtr_Device
// Access		: public  
// Returns		: void
// Parameter	: __in ST_Device * pDevice
// Qualifier	:
// Last Update	: 2017/2/13 - 14:51
// Desc.		:
//=============================================================================
void CTI_Processing::SetPtr_Device(__in ST_Device* pstDevice)
{
	if (pstDevice == NULL)
		return;

	m_pstDevice = pstDevice;

	m_TlCurrent.SetPtr_Device(m_pstDevice);
}

//=============================================================================
// Method		: SetPtr_ModelInfo
// Access		: public  
// Returns		: void
// Parameter	: ST_ModelInfo * pstModelInfo
// Qualifier	:
// Last Update	: 2017/2/15 - 14:29
// Desc.		:
//=============================================================================
void CTI_Processing::SetPtr_ModelInfo(ST_ModelInfo* pstModelInfo)
{
	if (pstModelInfo == NULL)
		return;

	m_pstModelInfo = pstModelInfo;

	m_pstRotate		 = &m_pstModelInfo->stRotate;
	m_pstCurrent	 = &m_pstModelInfo->stCurrent;
	m_pstEIAJ		 = &m_pstModelInfo->stEIAJ;
	m_pstSFR		 = &m_pstModelInfo->stSFR;
	m_pstCenterPoint = &m_pstModelInfo->stCenterPoint;
	m_pstParticle	 = &m_pstModelInfo->stParticle;
	m_pstCustomTeach = &m_pstModelInfo->stCustomTeach;

	m_TIPicControl.SetPtr_ModelInfo(m_pstModelInfo);
}

//=============================================================================
// Method		: CameraConnect
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/15 - 16:19
// Desc.		:
//=============================================================================
BOOL CTI_Processing::CameraConnect()
{
	UINT nGrabType = m_pstModelInfo->nGrabType;

	// 영상 보드 연결 상태 확인
	switch (nGrabType)
	{
	case GrabType_NTSC:
		if (FALSE == m_pstDevice->Cat3DCtrl.IsConnect())
			return FALSE;

		if (FALSE == m_pstDevice->Cat3DCtrl.GetStatus(CAM_VIEW_NUM))
			return FALSE;
		break;
	case GrabType_LVDS:
		if (FALSE == m_pstDevice->DAQCtrl.GetSignalStatus(CAM_VIEW_NUM))
			return FALSE;
		break;
	default:
		break;
	}

	return TRUE;
}

//=============================================================================
// Method		: GetTestImageBuffer
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nCh
// Qualifier	:
// Last Update	: 2017/7/7 - 10:46
// Desc.		:
//=============================================================================
BOOL CTI_Processing::GetTestImageBuffer(UINT nCh /*= 0*/)
{
	if (m_pstDevice == NULL)
		return FALSE;
	
	if (m_pstModelInfo == NULL)
		return FALSE;

	if (CameraConnect() == FALSE)
		return FALSE;

	LPBYTE pRGBz = NULL;

	UINT nGrabType = m_pstModelInfo->nGrabType;
	
	ST_VideoRGB1* pNRGB	= NULL;
	ST_VideoRGB*  pLRGB	= NULL;

	switch (nGrabType)
	{
	case GrabType_NTSC:
		pRGBz = (LPBYTE)(m_pstDevice->Cat3DCtrl.GetRecvRGBData(nCh))->m_pMatRGB[0];
		pNRGB = m_pstDevice->Cat3DCtrl.GetRecvRGBData(VideoView_Ch_1);

		m_dwWidth  = pNRGB->m_dwWidth;
		m_dwHeight = pNRGB->m_dwHeight;
		break;
	
	case GrabType_LVDS:
		pRGBz = (LPBYTE)(m_pstDevice->DAQCtrl.GetRecvRGBData(nCh));
		pLRGB = m_pstDevice->DAQCtrl.GetRecvVideoRGB(VideoView_Ch_1);

		m_dwWidth  = pLRGB->m_dwWidth;
		m_dwHeight = pLRGB->m_dwHeight;
		break;

	default:
		break;
	}

	DWORD dwSize = m_dwWidth * m_dwHeight * 3;

	if (m_dwImageBufSize != dwSize)
	{
		m_dwImageBufSize = dwSize;

		if (NULL != m_pImageBuf)
		{
			delete[] m_pImageBuf;
			m_pImageBuf = NULL;
		}

		m_pImageBuf = new BYTE[dwSize];
	}

	//memset(m_pImageBuf, 0, dwSize);

	memcpy(m_pImageBuf, pRGBz, dwSize);
	
	return TRUE;
}


//=============================================================================
// Method		: GetTestSourceImageBuffer
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nCh
// Qualifier	:
// Last Update	: 2017/9/8 - 16:56
// Desc.		:
//=============================================================================
BOOL CTI_Processing::GetTestSourceImageBuffer(UINT nCh /*= 0*/)
{
	if (m_pstDevice == NULL)
		return FALSE;

	if (m_pstModelInfo == NULL)
		return FALSE;

	if (CameraConnect() == FALSE)
		return FALSE;

	ST_FrameData* pRGB = m_pstDevice->DAQCtrl.GetSourceFrameData_ST(VideoView_Ch_1);
	LPWORD pRGBDATA = (LPWORD)m_pstDevice->DAQCtrl.GetSourceFrameData(VideoView_Ch_1);

	DWORD dwSize = m_dwWidth * m_dwHeight;

	if (m_wImageSourceBufSize != dwSize)
	{
		m_wImageSourceBufSize = dwSize;

		if (NULL != m_pImageSourceBuf)
		{
			delete[] m_pImageSourceBuf;
			m_pImageSourceBuf = NULL;
		}

		m_pImageSourceBuf = new WORD[dwSize];
	}

	memcpy(m_pImageSourceBuf, pRGBDATA, pRGB->dwWidth * pRGB->dwHeight * sizeof(WORD));

	return TRUE;
}


//=============================================================================
// Method		: GetVisionTestImageBuffer
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nCh
// Qualifier	:
// Last Update	: 2017/10/12 - 11:19
// Desc.		:
//=============================================================================
BOOL CTI_Processing::GetVisionTestImageBuffer(UINT nCh /*= 0*/)
{
	if (m_pstDevice == NULL)
		return FALSE;

	if (m_pstModelInfo == NULL)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: ResolutionRun
// Access		: public  
// Returns		: UINT
// Parameter	: BOOL MODE
// Qualifier	:
// Last Update	: 2017/2/13 - 10:56
// Desc.		:
//=============================================================================
UINT CTI_Processing::EIAJRun(BOOL bMode)
{
	enTestEachResult enResult;

	if (NULL == m_pstEIAJ)
		return FALSE;

	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	DoEvents(33);

	m_pstModelInfo->nPicViewMode = PIC_EIAJ;
	
	// 영상 사이즈 
	if (m_TlEIAJ.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	//if (bMode)
	//	m_pstEIAJ->stEIAJData.reset();

// 	IplImage *OriginImage = cvCreateImage(cvSize(m_dwWidth, m_dwHeight), IPL_DEPTH_8U, 3);
// 
// 	for (int y = 0; y < m_dwHeight; y++)
// 	{
// 		for (int x = 0; x < m_dwWidth; x++)
// 		{
// 			OriginImage->imageData[y*OriginImage->widthStep + 3 * x + 0] = m_pImageBuf[y*(m_dwWidth * 3) + x * 3];
// 			OriginImage->imageData[y*OriginImage->widthStep + 3 * x + 1] = m_pImageBuf[y*(m_dwWidth * 3) + x * 3 + 1];
// 			OriginImage->imageData[y*OriginImage->widthStep + 3 * x + 2] = m_pImageBuf[y*(m_dwWidth * 3) + x * 3 + 2];
// 		}
// 	}
// 
// 	cvSaveImage("C:\\Testimage.bmp", OriginImage);
// 	cvReleaseImage(&OriginImage);


	enResult = (enTestEachResult)m_TlEIAJ.EIAJ_Test(m_pstEIAJ, m_pImageBuf);


	return enResult;
}

//=============================================================================
// Method		: SFRRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/8/11 - 10:42
// Desc.		:
//=============================================================================
UINT CTI_Processing::SFRRun()
{
	enTestEachResult enResult;

	if (NULL == m_pstSFR)
		return TER_Fail;

	// UI 이미지
	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	// 원본 이미지
	if (GetTestSourceImageBuffer() == FALSE)
		return TER_NoImage;

	DoEvents(33);

	m_pstModelInfo->nPicViewMode = PIC_SFR;

	// 영상 사이즈 
	if (m_TlSFR.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return TER_Fail;

	enResult = (enTestEachResult)m_TlSFR.SFR_Test(m_pstSFR, m_pImageBuf, m_pImageSourceBuf);

	return enResult;
}

//=============================================================================
// Method		: CenterPointRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/2/13 - 10:54
// Desc.		:
//=============================================================================
UINT CTI_Processing::CenterPointRun(BOOL bPicMode /*= TRUE*/)
{
	enTestEachResult enResult;

	if (NULL == m_pstCenterPoint)
		return FALSE;

	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	DoEvents(33);

	if (bPicMode == TRUE)
	m_pstModelInfo->nPicViewMode = PIC_CenterAdjust;

	// 영상 사이즈 
	if (m_TlCenterPoint.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	// 알고리즘
	enResult = (enTestEachResult)m_TlCenterPoint.CenterPoint_Test(m_pstCenterPoint, m_pImageBuf);

	return enResult;
}

//=============================================================================
// Method		: CenterPointTuningRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/9/11 - 20:58
// Desc.		:
//=============================================================================
UINT CTI_Processing::CenterPointTuningRun()
{
	enTestEachResult enResult;

	m_pstModelInfo->nPicViewMode = PIC_CenterAdjust;

	if (NULL == m_pstCenterPoint)
		return FALSE;

	for (UINT nStep = 0; nStep < m_pstModelInfo->stCenterPoint.stCenterPointOp.nWriteCnt; nStep++)
	{
		DoEvents(33);

		if (GetTestImageBuffer() == FALSE)
			return TER_NoImage;

		// 영상 사이즈 
		if (m_TlCenterPoint.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
			return FALSE;

		// 알고리즘
		enResult = (enTestEachResult)m_TlCenterPoint.CenterPoint_Test(m_pstCenterPoint, m_pImageBuf);

		// 이동 제한 값에서 벗어난 경우
		if (abs(m_pstCenterPoint->stCenterPointData.iResult_Offset_X) >= m_pstModelInfo->stCenterPoint.stCenterPointOp.nMax_PixX && abs(m_pstCenterPoint->stCenterPointData.iResult_Offset_Y) >= m_pstModelInfo->stCenterPoint.stCenterPointOp.nMax_PixY)
			break;

		// 목표 타켓내에 들어가면 루틴 중지
		if (abs(m_pstCenterPoint->stCenterPointData.iResult_Offset_X) <= m_pstModelInfo->stCenterPoint.stCenterPointOp.nTarget_OffsetX && abs(m_pstCenterPoint->stCenterPointData.iResult_Offset_Y) <= m_pstModelInfo->stCenterPoint.stCenterPointOp.nTarget_OffsetY)
			break;

		// 광축 조정 루틴
		m_pstDevice->MotionSequence.CenterPointAdjustment(m_pstCenterPoint->stCenterPointData.iResult_Offset_X, m_pstCenterPoint->stCenterPointData.iResult_Offset_Y);
	}

	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	// 영상 사이즈 
	if (m_TlCenterPoint.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	// 알고리즘
	enResult = (enTestEachResult)m_TlCenterPoint.CenterPoint_Test(m_pstCenterPoint, m_pImageBuf);

	return enResult;
}

//=============================================================================
// Method		: RotateRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/2/13 - 10:57
// Desc.		:
//=============================================================================
UINT CTI_Processing::RotateRun()
{
	enTestEachResult enResult;
	
	if (NULL == m_pstRotate)
		return FALSE;

	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	DoEvents(33);

	m_pstModelInfo->nPicViewMode = PIC_Rotation;

	// 영상 사이즈 
	if (m_TlRotate.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	enResult = (enTestEachResult)m_TlRotate.Rotate_Test(m_pstRotate, m_pImageBuf);

	return enResult;
}

//=============================================================================
// Method		: RotateTuningRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/9/11 - 20:58
// Desc.		:
//=============================================================================
UINT CTI_Processing::RotateTuningRun()
{
	enTestEachResult enResult;

	m_pstModelInfo->nPicViewMode = PIC_Rotation;

	if (NULL == m_pstRotate)
		return FALSE;

	for (UINT nStep = 0; nStep < m_pstModelInfo->stRotate.stRotateOp.nWriteCnt; nStep++)
	{
		DoEvents(33);

		if (GetTestImageBuffer() == FALSE)
			return TER_NoImage;

		// 영상 사이즈 
		if (m_TlRotate.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
			return FALSE;

		// 알고리즘
		enResult = (enTestEachResult)m_TlRotate.Rotate_Test(m_pstRotate, m_pImageBuf);

		// 이동 제한 값에서 벗어난 경우
		if (abs(m_pstRotate->stRotateData.dbDegree) >= m_pstRotate->stRotateOp.dbMaxDegree)
			break;

		// 목표 타켓내에 들어가면 루틴 중지
		if (abs(m_pstRotate->stRotateData.dbDegree) <= m_pstRotate->stRotateOp.dbTargetDegree)
			break;

		// 로테이트 조정 루틴
		m_pstDevice->MotionSequence.RotateAdjustment(m_pstRotate->stRotateData.dbDegree);
	}

	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	// 영상 사이즈 
	if (m_TlRotate.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	// 알고리즘
	enResult = (enTestEachResult)m_TlRotate.Rotate_Test(m_pstRotate, m_pImageBuf);

	return enResult;
}

//=============================================================================
// Method		: ParticleRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/8/13 - 15:13
// Desc.		:
//=============================================================================
UINT CTI_Processing::ParticleRun()
{
	enTestEachResult enResult;

	if (NULL == m_pstRotate)
		return FALSE;

	if (GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	DoEvents(33);

	m_pstModelInfo->nPicViewMode = PIC_Particle;

	// 영상 사이즈 
	if (m_TlParticle.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	enResult = (enTestEachResult)m_TlParticle.Particle_Test(m_pstParticle, m_pImageBuf);

	return enResult;
}

//=============================================================================
// Method		: ActiveAlignRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/7/10 - 13:39
// Desc.		:
//=============================================================================
UINT CTI_Processing::ActiveAlignRun()
{
	if (NULL == m_pstCenterPoint)
		return FALSE;

	if (NULL == m_pstRotate)
		return FALSE;

	if (NULL == m_pstSFR)
		return FALSE;

	enTestEachResult enCenter;
	enTestEachResult enSFR;
	enTestEachResult enRotate;

	DoEvents(33);

	if (GetTestSourceImageBuffer() == FALSE)
		return TER_NoImage;

	if(GetTestImageBuffer() == FALSE)
		return TER_NoImage;

	// 영상 사이즈 
	if (m_TlCenterPoint.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	if (m_TlSFR.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	if (m_TlRotate.SetImageSize(m_dwWidth, m_dwHeight) == FALSE)
		return FALSE;

	m_pstModelInfo->nPicViewMode = PIC_SFR_Rotation_Center;
	
	// 알고리즘
	enCenter = (enTestEachResult)m_TlCenterPoint.CenterPoint_Test(m_pstCenterPoint, m_pImageBuf);
	enRotate = (enTestEachResult)m_TlRotate.Rotate_Test(m_pstRotate, m_pImageBuf);
	enSFR	 = (enTestEachResult)m_TlSFR.SFR_Test(m_pstSFR, m_pImageBuf, m_pImageSourceBuf);

	if (enCenter == TER_Fail || enRotate == TER_Fail || enSFR == TER_Fail)
	{
		return TER_Fail;
	}

	return TER_Pass;
}

//=============================================================================
// Method		: ActiveAlignTuningRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/9/11 - 20:49
// Desc.		:
//=============================================================================
UINT CTI_Processing::ActiveAlignTuningRun()
{
	if (NULL == m_pstCenterPoint)
		return FALSE;

	if (NULL == m_pstRotate)
		return FALSE;

	enTestEachResult enCenter;
	enTestEachResult enRotate;

	DoEvents(33);

	if (m_pstCenterPoint->stCenterPointOp.nTestMode == CP_Fine_tune_TestMode)
		enCenter = (enTestEachResult)CenterPointTuningRun();

	if (m_pstCenterPoint->stCenterPointOp.nTestMode == CP_Measurement_TestMode)
		enCenter = (enTestEachResult)CenterPointRun();

	// 이미지가 없을 경우에 Fail
	if (enCenter == TER_NoImage)
		return enCenter;

	if (m_pstRotate->stRotateOp.nTestMode == RT_Fine_tune_TestMode)
		enRotate = (enTestEachResult)RotateTuningRun();

	if (m_pstRotate->stRotateOp.nTestMode == RT_Measurement_TestMode)
		enRotate = (enTestEachResult)RotateRun();

	// 이미지가 없을 경우에 Fail
	if (enRotate == TER_NoImage)
		return enRotate;

	if (m_pstCenterPoint->stCenterPointOp.nTestMode == CP_Fine_tune_TestMode)
		enCenter = (enTestEachResult)CenterPointTuningRun();

	// 이미지가 없을 경우에 Fail
	if (enCenter == TER_NoImage)
		return enCenter;

	if (enCenter == TER_Fail || enRotate == TER_Fail)
	{
		return TER_Fail;
	}

	return TER_Pass;
}


UINT CTI_Processing::ManualBtnCtrl(enManual_Commend enCmd)
{
	UINT nResult = TER_Pass;

	if (m_pstDevice == NULL)
		return nResult = TER_Fail;

	if (m_pstModelInfo == NULL)
		return nResult = TER_Fail;

	if (!m_pstDevice->MotionSequence.m_pDigitalIOCtrl->AXTState())
		return nResult = TER_Fail;

	switch (enCmd)
	{
	case Man_Cmd_ModuleFix:
		if (!m_pstDevice->MotionSequence.ModuleFixUnFixCYLMotion(FIX))
		{
			nResult = TER_Fail;
		}
		break;

	case Man_Cmd_ModuleUnFix:
		if (!m_pstDevice->MotionSequence.ModuleFixUnFixCYLMotion(UNFIX))
		{
			nResult = TER_Fail;
		}
		break;

	case Man_Cmd_PCBFix:
		if (!m_pstDevice->MotionSequence.PCBFixUnFixCYLMotion(FIX))
		{
			nResult = TER_Fail;
		}
		break;

	case Man_Cmd_PCBUnFix:
		if (!m_pstDevice->MotionSequence.PCBFixUnFixCYLMotion(UNFIX))
		{
			nResult = TER_Fail;
		}
		break;

	case Man_Cmd_DriverIn:
		if (!m_pstDevice->MotionSequence.DriverInOutCYLMotion(FIX))
		{
			nResult = TER_Fail;
		}
		break;

	case Man_Cmd_DriverOut:
		if (!m_pstDevice->MotionSequence.DriverInOutCYLMotion(UNFIX))
		{
			nResult = TER_Fail;
		}
		break;

	default:
		break;
	}

	return nResult;
}

//=============================================================================
// Method		: ImageSaveOriginal
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/9/7 - 13:12
// Desc.		:
//=============================================================================
BOOL CTI_Processing::ImageSaveOriginal()
{
	// DAQ 영상만 저장 가능 함수, 추후 수정 할 계획

	// 영상 보드 연결 상태 확인
	if (!CameraConnect())
		return FALSE;

	CString strPath;

	if (GetSaveImageFilePath(strPath))
	{
 		strPath = strPath + _T("_Original.png");

 		ST_VideoRGB* pRGB		= m_pstDevice->DAQCtrl.GetRecvVideoRGB(VideoView_Ch_1);
 		RGBQUAD** pRGBItr		= pRGB->m_pMatRGBA;
 		LPBYTE pRGBDATA			= m_pstDevice->DAQCtrl.GetRecvRGBData(VideoView_Ch_1);
		IplImage *Copyimage = cvCreateImage(cvSize(pRGB->m_dwWidth, pRGB->m_dwHeight), IPL_DEPTH_8U, 3);

		for (int y = 0; y < pRGB->m_dwHeight; y++)
		{
			for (int x = 0; x < pRGB->m_dwWidth; x++)
			{
				Copyimage->imageData[y*(Copyimage->widthStep) + x * 3 + 0] = pRGBDATA[(y)*(pRGB->m_dwWidth * 3) + x * 3 + 0];
				Copyimage->imageData[y*(Copyimage->widthStep) + x * 3 + 1] = pRGBDATA[(y)*(pRGB->m_dwWidth * 3) + x * 3 + 1];
				Copyimage->imageData[y*(Copyimage->widthStep) + x * 3 + 2] = pRGBDATA[(y)*(pRGB->m_dwWidth * 3) + x * 3 + 2];
			}
		}

		cvSaveImage((CStringA)strPath, Copyimage);
		cvReleaseImage(&Copyimage);

		return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: ImageSaveGray
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/9/7 - 17:47
// Desc.		:
//=============================================================================
BOOL CTI_Processing::ImageSaveGray()
{
	// DAQ 영상만 저장 가능 함수, 추후 수정 할 계획

	// 영상 보드 연결 상태 확인
	if (!CameraConnect())
		return FALSE;

	CString strPath;

	if (GetSaveImageFilePath(strPath))
	{
		strPath = strPath + _T("_Gray.png");
		ST_FrameData* pRGB	= m_pstDevice->DAQCtrl.GetSourceFrameData_ST(VideoView_Ch_1);
		LPWORD pRGBDATA		= (LPWORD)m_pstDevice->DAQCtrl.GetSourceFrameData(VideoView_Ch_1);
		IplImage *Copyimage = cvCreateImage(cvSize(pRGB->dwWidth, pRGB->dwHeight), IPL_DEPTH_16U, 1);

		memcpy(Copyimage->imageData, pRGBDATA, pRGB->dwWidth * pRGB->dwHeight * 2);

		cvSaveImage((CStringA)strPath, Copyimage);
		cvReleaseImage(&Copyimage);

		return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: ImageSavePic
// Access		: public  
// Returns		: BOOL
// Parameter	: CString Path
// Qualifier	:
// Last Update	: 2017/9/7 - 13:12
// Desc.		:
//=============================================================================
BOOL CTI_Processing::ImageSavePic()
{
	if (m_hImageWnd == NULL)
		return FALSE;

	// 영상 보드 연결 상태 확인
	if (CameraConnect() == FALSE)
		return FALSE;

	CString strPath;

	// 비트맵(DIB) 데이터 추출
	GetTestImageBuffer();

	if (GetSaveImageFilePath(strPath))
	{
		strPath = strPath + _T("_Pic.bmp");

		HDC hDC = ::GetDC(m_hImageWnd);

		// 윈도우의 크기 알아내기
		RECT rc;
		::GetClientRect(m_hImageWnd, &rc);

		// 비트맵(DDB) 생성하기
		HDC hMemDC = CreateCompatibleDC(hDC);
		HBITMAP hBitmap = CreateCompatibleBitmap(hDC, rc.right, rc.bottom);
		HBITMAP hBmpOld = (HBITMAP)SelectObject(hMemDC, hBitmap);
		BitBlt(hMemDC, 0, 0, rc.right, rc.bottom, hDC, 0, 0, SRCCOPY);
		SelectObject(hMemDC, hBmpOld);
		DeleteDC(hMemDC);

		// 비트맵(DIB) 사양 설정
		BITMAPINFOHEADER bmih;
		ZeroMemory(&bmih, sizeof(BITMAPINFOHEADER));
		bmih.biSize = sizeof(BITMAPINFOHEADER);
		bmih.biWidth = rc.right;
		bmih.biHeight = rc.bottom;
		bmih.biPlanes = 1;
		bmih.biBitCount = 24;
		bmih.biCompression = BI_RGB;

		GetDIBits(hDC, hBitmap, 0, rc.bottom, NULL, (LPBITMAPINFO)&bmih, DIB_RGB_COLORS);
		GetDIBits(hDC, hBitmap, 0, rc.bottom, m_pImageBuf, (LPBITMAPINFO)&bmih, DIB_RGB_COLORS);
		::ReleaseDC(m_hImageWnd, hDC);

		DeleteObject(hBitmap);

		// 비트맵 파일 헤더 설정
		BITMAPFILEHEADER bmfh;
		bmfh.bfType = 'MB';
		bmfh.bfSize = sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+bmih.biSizeImage;
		bmfh.bfReserved1 = 0;
		bmfh.bfReserved2 = 0;
		bmfh.bfOffBits = sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER);

		// 비트맵 파일 생성 및 데이터 저장
		DWORD dwWritten = 0;
		HANDLE hFile = CreateFile(strPath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

		WriteFile(hFile, &bmfh, sizeof(BITMAPFILEHEADER), &dwWritten, NULL);
		WriteFile(hFile, &bmih, sizeof(BITMAPINFOHEADER), &dwWritten, NULL);
		WriteFile(hFile, m_pImageBuf, bmih.biSizeImage, &dwWritten, NULL);
		CloseHandle(hFile);

		return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: GetSaveImageFilePath
// Access		: public  
// Returns		: BOOL
// Parameter	: __out CString & szOutPath
// Qualifier	:
// Last Update	: 2016/7/27 - 16:31
// Desc.		:
//=============================================================================
BOOL CTI_Processing::GetSaveImageFilePath(__out CString& szOutPath)
{
	CString szPath = m_pstOption->Inspector.szPath_Image;
	CString szPathTime;
	SYSTEMTIME tmLocal;

	GetLocalTime(&tmLocal);

	szPathTime.Format(_T("%04d%02d%02d"), tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
	szPath += +_T("\\") + szPathTime + _T("\\") + m_pstModelInfo->szModelFile;

	MakeDirectory(szPath);

	szOutPath.Format(_T("%04d%02d%02d-%02d%02d%02d"), tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	szOutPath = szPath + _T("\\") + szOutPath;

	return TRUE;
}


//=============================================================================
// Method		: InterlockRun
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/10/19 - 14:21
// Desc.		:
//=============================================================================
// UINT CTI_Processing::InterlockRun()
// {
// 	if (m_pstDevice == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstModelInfo == NULL)
// 		return TER_Fail;
// 
// 	if (!m_pstDevice->MotionSequence.m_pDigitalIOCtrl->AXTState())
// 		return TER_Fail;
// 
// 	if (m_pstDevice->MotionSequence.m_pDigitalIOCtrl->GetInStatus(DI_InterlockIn) == TRUE
// 		&& m_pstDevice->MotionSequence.m_pDigitalIOCtrl->GetInStatus(DI_InterlockOut) == FALSE)
// 	{
// 		if (m_pstDevice->MotionSequence.InterlockMove(OFF))
// 		{
// 			return TER_Pass;
// 		}
// 	}
// 	else if (m_pstDevice->MotionSequence.m_pDigitalIOCtrl->GetInStatus(DI_InterlockIn) == FALSE
// 		&& m_pstDevice->MotionSequence.m_pDigitalIOCtrl->GetInStatus(DI_InterlockOut) == TRUE)
// 	{
// 		if (m_pstDevice->MotionSequence.InterlockMove(ON))
// 		{
// 			return TER_Pass;
// 		}
// 	}
// 
// 	return TER_Fail;
// }
// 
// 
// UINT CTI_Processing::InterlockFixedRun(__in BOOL bOnOff)
// {
// 	if (m_pstDevice == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstModelInfo == NULL)
// 		return TER_Fail;
// 
// 	if (!m_pstDevice->MotionSequence.m_pDigitalIOCtrl->AXTState())
// 		return TER_Fail;
// 
// 	if (bOnOff == ON)
// 	{
// 		if (m_pstDevice->MotionSequence.InterlockMove(ON))
// 		{
// 			return TER_Pass;
// 		}
// 	}
// 	else if (bOnOff == OFF)
// 	{
// 		if (m_pstDevice->MotionSequence.InterlockMove(OFF))
// 		{
// 			return TER_Pass;
// 		}
// 	}
// 
// 	return TER_Fail;
// }
// 
// //=============================================================================
// // Method		: UnloadingRun
// // Access		: public  
// // Returns		: UINT
// // Qualifier	:
// // Last Update	: 2017/10/19 - 14:21
// // Desc.		:
// //=============================================================================
// UINT CTI_Processing::UnloadingRun()
// {
// 	if (m_pstDevice == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstModelInfo == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstCustomTeach == NULL)
// 		return TER_Fail;
// 
// 	// 비전 조명 OFF
// 	if (!m_pstDevice->IF_Illumination.Send_SetOnOff(0, OFF))
// 		return TER_Fail;
// 
// 	// 시퀀스 마지막 스텝의 옵셋
// 	double dOffsetZ = 0;
// 	double dOffsetX = 0;
// 	double dOffsetY = 0;
// 
// 	int	iSeq = 0;
// 	int	iStepCount = 0;
// 
// 	dOffsetZ = m_pstCustomTeach->dUnloadingMotion_OffsetZ * 1000;
// 	dOffsetX = m_pstCustomTeach->dUnloadingMotion_OffsetX * 1000;
// 	dOffsetY = m_pstCustomTeach->dUnloadingMotion_OffsetY * 1000;
// 
// 	iSeq = m_pstCustomTeach->iUnloadingMotionSeq;
// 	ST_MotionSeqTeach stMotionSeq = m_pstDevice->MotionManager.m_AllMotorData.MotionSeq[iSeq];
// 
// 	iStepCount = stMotionSeq.nStepCount;
// 	for (int iStep = 0; iStep < iStepCount; iStep++)
// 	{
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisZ].dbPos += dOffsetZ;
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisX].dbPos += dOffsetX;
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisY].dbPos += dOffsetY;
// 	}
// 
// 	if (!m_pstDevice->MotionManager.MotionSequenceMove(&stMotionSeq) == TRUE)
// 		return TER_Fail;
// 
// 	// 콜렛 초기 위치
// 	if (!m_pstDevice->MotionSequence.ColletDegreeMove(POS_ABS_MODE, 0))
// 		return TER_Fail;
// 
// 	// 인터락 해제
// 	//if (!m_pstDevice->MotionSequence.InterlockMove(OFF))
// 	//	return TER_Fail;
// 
// 	return TER_Pass;
// }
// 
// //=============================================================================
// // Method		: ColletVisionRun
// // Access		: public  
// // Returns		: UINT
// // Parameter	: __out double & dResultDeg
// // Qualifier	:
// // Last Update	: 2017/10/12 - 11:30
// // Desc.		:
// //=============================================================================
// UINT CTI_Processing::ColletVisionRun(__in double dDisplace, __out double& dResultDeg, __out double& dInitPosR)
// {
// 	if (m_pstDevice == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstModelInfo == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstCustomTeach == NULL)
// 		return TER_Fail;
// 
// 	// 비전 조명 세팅
// 	BOOL bOnOff			= OFF;
// 	UINT nBrightness	= 0;
// 
// 	if (m_pstDevice->IF_Illumination.Send_SetOnOff(0, ON))
// 	{
// // 		if (!m_pstDevice->IF_Illumination.Send_GetBrightness(0, nBrightness))
// // 			return TER_Fail;
// // 
// // 		if (m_pstModelInfo->stVisionOpt.iBrightness != (int)nBrightness)
// 		{
// 			if (!m_pstDevice->IF_Illumination.Send_SetBrightness(0, m_pstModelInfo->stVisionOpt.iBrightness))
// 				return TER_Fail;
// 		}
// 	}
// 
// 	// 비전 이미지 버퍼
// 	LPBYTE	lpImage		= NULL;		
// 	DWORD	dwWidth		= 0;	// 비전 이미지 가로 크기
// 	DWORD	dwHeight	= 0;	// 비전 이미지 세로 크기
// 
// 	// 시퀀스 마지막 스텝의 옵셋
// 	double dOffsetZ		= 0;
// 	double dOffsetX		= 0;
// 	double dOffsetY		= 0;
// 
// //	double dMasterDev = m_pstModelInfo->stAFInfo.dSTD_Displace - dDisplace;
// 
// // 	int	iSeq = 0;
// // 	int	iStepCount = 0;
// // 
// // 	dOffsetZ = m_pstCustomTeach->dAFPosition_OffsetZ * 1000;
// // 	dOffsetX = m_pstCustomTeach->dAFPosition_OffsetX * 1000;
// // 	dOffsetY = (m_pstCustomTeach->dAFPosition_OffsetY + dMasterDev) * 1000;
// 
// 	int	iSeq			= 0;
// 	int	iStepCount		= 0;
// 
// 	dOffsetZ = m_pstCustomTeach->dVisionMotion_OffsetZ * 1000;
// 	dOffsetX = m_pstCustomTeach->dVisionMotion_OffsetX * 1000;
// 	dOffsetY = m_pstCustomTeach->dVisionMotion_OffsetY * 1000;
// 
// 	iSeq = m_pstCustomTeach->iVisionMotionSeq;
// 	ST_MotionSeqTeach stMotionSeq = m_pstDevice->MotionManager.m_AllMotorData.MotionSeq[iSeq];
// 
// 	iStepCount = stMotionSeq.nStepCount;
// 	for (int iStep = 0; iStep < iStepCount; iStep++)
// 	{
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisZ].dbPos += dOffsetZ;
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisX].dbPos += dOffsetX;
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisY].dbPos += dOffsetY;
// 	}
// 
// 	// 비전 측정 위치
// 	if (!m_pstDevice->MotionManager.MotionSequenceMove(&stMotionSeq))
// 		return TER_Fail;
// 
// 	// 콜렛 초기 위치
// 	if (!m_pstDevice->MotionSequence.ColletDegreeMove(POS_ABS_MODE, 0))
// 		return TER_Fail;
// 
// 	// 안정화 딜레이
// 	DoEvents(1500);
// 
// 	// 비전 측정
// 	if (m_pstDevice->VisionCam.GetAcquiredImage(lpImage, dwWidth, dwHeight))
// 	{
// 		if (lpImage != NULL)
// 		{
// 			ST_VisionOptInfo stVisionOpt = m_pstModelInfo->stVisionOpt;
// 
// 			if (m_TICollet.TestColletAngle(lpImage, dwWidth, dwHeight, &stVisionOpt, dResultDeg) == TRUE)
// 			{
// 				if (m_pstDevice->MotionSequence.ColletDegreeMove(POS_ABS_MODE, dResultDeg * -1) == TRUE)
// 				{
// 					dInitPosR = dResultDeg * -100;
// 					return TER_Pass;
// 				}
// 			}
// 		}
// 	}
// 	
// 	return TER_Fail;
// }
// 
// 
// //=============================================================================
// // Method		: DisplaceDetectRun
// // Access		: public  
// // Returns		: UINT
// // Parameter	: __out double & dResultDisplace
// // Qualifier	:
// // Last Update	: 2017/10/19 - 11:28
// // Desc.		:
// //=============================================================================
// UINT CTI_Processing::DisplaceDetectRun(__out double& dResultDisplace)
// {
// 	if (m_pstDevice == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstModelInfo == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstCustomTeach == NULL)
// 		return TER_Fail;
// 
// 
// 	// 시퀀스 마지막 스텝의 옵셋
// 	double dOffsetZ = 0;
// 	double dOffsetX = 0;
// 	double dOffsetY = 0;
// 	int	iSeq = 0;
// 	int	iStepCount = 0;
// 
// 	// 변위 데이터
// 	float fData[2] = { 0, };
// 
// 	dOffsetZ = m_pstCustomTeach->dDisplaceMotionA_OffsetZ * 1000;
// 	dOffsetX = m_pstCustomTeach->dDisplaceMotionA_OffsetX * 1000;
// 	dOffsetY = m_pstCustomTeach->dDisplaceMotionA_OffsetY * 1000;
// 
// 	iSeq = m_pstCustomTeach->iDisplaceMotionASeq;
// 	ST_MotionSeqTeach stMotionSeqA = m_pstDevice->MotionManager.m_AllMotorData.MotionSeq[iSeq];
// 
// 	iStepCount = stMotionSeqA.nStepCount;
// 	for (int iStep = 0; iStep < iStepCount; iStep++)
// 	{
// 		stMotionSeqA.stSeqStep[iStep].stAxisParam[MotorAxisZ].dbPos += dOffsetZ;
// 		stMotionSeqA.stSeqStep[iStep].stAxisParam[MotorAxisX].dbPos += dOffsetX;
// 		stMotionSeqA.stSeqStep[iStep].stAxisParam[MotorAxisY].dbPos += dOffsetY;
// 	}
// 
// 	// A Point 측정 위치
// 	if (!m_pstDevice->MotionManager.MotionSequenceMove(&stMotionSeqA))
// 		return TER_Fail;
// 
// 	// 안정화 딜레이
// 	DoEvents(300);
// 
// 	// A Point 변위 측정
// 	if (!m_pstDevice->LaserSensor.Read_ComparatorValue(fData[0]))
// 		return TER_Fail;
// 
// 
// 	dOffsetZ = m_pstCustomTeach->dDisplaceMotionB_OffsetZ * 1000;
// 	dOffsetX = m_pstCustomTeach->dDisplaceMotionB_OffsetX * 1000;
// 	dOffsetY = m_pstCustomTeach->dDisplaceMotionB_OffsetY * 1000;
// 
// 	iSeq = m_pstCustomTeach->iDisplaceMotionBSeq;
// 	ST_MotionSeqTeach stMotionSeqB = m_pstDevice->MotionManager.m_AllMotorData.MotionSeq[iSeq];
// 
// 	iStepCount = stMotionSeqB.nStepCount;
// 	for (int iStep = 0; iStep < iStepCount; iStep++)
// 	{
// 		stMotionSeqB.stSeqStep[iStep].stAxisParam[MotorAxisZ].dbPos += dOffsetZ;
// 		stMotionSeqB.stSeqStep[iStep].stAxisParam[MotorAxisX].dbPos += dOffsetX;
// 		stMotionSeqB.stSeqStep[iStep].stAxisParam[MotorAxisY].dbPos += dOffsetY;
// 	}
// 
// 	// B Point 측정 위치
// 	if (!m_pstDevice->MotionManager.MotionSequenceMove(&stMotionSeqB))
// 		return TER_Fail;
// 
// 	// 안정화 딜레이
// 	DoEvents(300);
// 
// 	// B Point 변위 측정
// 	if (!m_pstDevice->LaserSensor.Read_ComparatorValue(fData[1]))
// 		return TER_Fail;
// 
// 	if (m_pstModelInfo->stAFInfo.dSTD_DisplaceDev >= abs(fData[0] - fData[1]))
// 	{
// 		dResultDisplace = (fData[0] + fData[1]) / 2;
// 
// 		return TER_Pass;
// 	}
// 
// 	return TER_Fail;
// }
// 
// 
// //=============================================================================
// // Method		: AFPositionRun
// // Access		: public  
// // Returns		: UINT
// // Parameter	: __in double dDisplace
// // Qualifier	:
// // Last Update	: 2017/10/19 - 11:29
// // Desc.		:
// //=============================================================================
// UINT CTI_Processing::AFPositionRun(__in double dDisplace, __out double& dInitPosY)
// {
// 	if (m_pstDevice == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstModelInfo == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstCustomTeach == NULL)
// 		return TER_Fail;
// 
// 	// 비전 이미지 버퍼
// 	LPBYTE	lpImage = NULL;
// 	DWORD	dwWidth = 0;	// 비전 이미지 가로 크기
// 	DWORD	dwHeight = 0;	// 비전 이미지 세로 크기
// 
// 	// 시퀀스 마지막 스텝의 옵셋
// 	double dOffsetZ = 0;
// 	double dOffsetX = 0;
// 	double dOffsetY = 0;
// 	double dMasterDev = m_pstModelInfo->stAFInfo.dSTD_Displace - dDisplace;
// 
// 	int	iSeq = 0;
// 	int	iStepCount = 0;
// 
// 	dOffsetZ = m_pstCustomTeach->dAFPosition_OffsetZ * 1000;
// 	dOffsetX = m_pstCustomTeach->dAFPosition_OffsetX * 1000;
// 	dOffsetY = (m_pstCustomTeach->dAFPosition_OffsetY + dMasterDev) * 1000;
// 
// 	iSeq = m_pstCustomTeach->iAFPositionSeq;
// 	ST_MotionSeqTeach stMotionSeq = m_pstDevice->MotionManager.m_AllMotorData.MotionSeq[iSeq];
// 
// 	iStepCount = stMotionSeq.nStepCount;
// 
// 	for (int iStep = 0; iStep < iStepCount; iStep++)
// 	{
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisZ].dbPos += dOffsetZ;
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisX].dbPos += dOffsetX;
// 		stMotionSeq.stSeqStep[iStep].stAxisParam[MotorAxisY].dbPos += dOffsetY;
// 	}
// 	
// 	dInitPosY = stMotionSeq.stSeqStep[iStepCount - 1].stAxisParam[MotorAxisY].dbPos;
// 
// 	// 비전 측정 위치
// 	if (m_pstDevice->MotionManager.MotionSequenceMove(&stMotionSeq) == TRUE)
// 		return TER_Pass;
// 
// 	return TER_Fail;
// }
// 
// 
// UINT CTI_Processing::AutoFocusingRun(__in double dDisplace)
// {
// 	if (m_pstDevice == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstModelInfo == NULL)
// 		return TER_Fail;
// 
// 	if (m_pstEIAJ == NULL)
// 		return TER_Fail;
// 
// 	double dStepDeg[3] =
// 	{
// 		m_pstModelInfo->stAFInfo.dAFStepDegree1, // 1차 스텝 단위
// 		m_pstModelInfo->stAFInfo.dAFStepDegree2, // 2차 스텝 단위
// 		m_pstModelInfo->stAFInfo.dAFStepDegree3, // 3차 스텝 단위
// 	};
// 
// 	double	dSTDDisplace = m_pstModelInfo->stAFInfo.dSTD_Displace;
// 	double	dDetDisplace = dDisplace;
// 	double	dLensConchoid = m_pstModelInfo->stAFInfo.dLensConchoid;
// 	BOOL	bDirection = m_pstModelInfo->stAFInfo.bLensDirection;
// 	double	dRotateCount = m_pstModelInfo->stAFInfo.dAFRotateCnt;
// 
// 	double dConchoid[3] =
// 	{
// 		dStepDeg[0] / 360.0 * dLensConchoid,
// 		dStepDeg[1] / 360.0 * dLensConchoid,
// 		dStepDeg[2] / 360.0 * dLensConchoid,
// 	};
// 
// // 	if (m_pstEIAJ->stEIAJData.nResult == TER_Pass)
// // 		return TER_Pass;
// 
// 	int	iDegDirection = 0;
// 
// 	if (bDirection == Reverse)
// 	{
// 		iDegDirection = -1;
// 	}
// 	else if (bDirection == Foward)
// 	{
// 		iDegDirection = 1;
// 	}
// 	
// 	double	dCurrentDegPuls = 0;
// 	int		iStepCnt = 0;
// 
// 
// 	while (1)
// 	{
// 		if (m_pstEIAJ->stEIAJData.nResult == TER_Pass)
// 			return TER_Pass;
// 
// 		int iDevCount = m_pstEIAJ->stEIAJData.nTotal_cnt - m_pstEIAJ->stEIAJData.nResult_cnt;
// 
// 		if (bDirection == Reverse)
// 		{
// 			iDegDirection = -1;
// 			
// 			if (dCurrentDegPuls >= (360.0 * dRotateCount * 100))
// 			{
// 				break;
// 			}
// 		}
// 		else if (bDirection == Foward)
// 		{
// 			iDegDirection = 1;
// 
// 			// 변위 위치에 대한 리미트
// 			double dLimitDev = abs(m_pstModelInfo->stAFInfo.dSTD_Displace - dDetDisplace);
// 			double dLimitPuls = dLimitDev / dLensConchoid * 36000;
// 			
// 			if (dCurrentDegPuls >= dLimitPuls)
// 			{
// 				break;
// 			}
// 		}
// 		
// 		if (iDevCount >= 2 && iDevCount < 4)
// 			iStepCnt = 1;
// 		else if (iDevCount >= 0 && iDevCount < 2)
// 			iStepCnt = 2;
// 		else
// 			iStepCnt = 0;
// 
// 		if (!m_pstDevice->MotionSequence.FocusingDegreeMove(dStepDeg[iStepCnt] * iDegDirection, dConchoid[iStepCnt] * iDegDirection))
// 			break;
// 
// 		dCurrentDegPuls += dStepDeg[iStepCnt] * 100;
// 
// 		DoEvents(150);
// 
// 	}
// 	
// 	return TER_Fail;
// }
// 
// UINT CTI_Processing::ManualFocusingRun(__in double dDegree)
// {
// 	if (m_pstDevice == NULL)
// 		return TER_Fail;
// 
// 	double dLensConchoid	= m_pstModelInfo->stAFInfo.dLensConchoid;
// 	double dConchoid		= dDegree / 360.0 * dLensConchoid;
// 
// 	if (m_pstDevice->MotionSequence.FocusingDegreeMove(dDegree, dConchoid) == TRUE)
// 	{
// 		return TER_Pass;
// 	}
// 
// 	return TER_Fail;
// }
// 
// 
// //************************************
// // Method:    InitFocusingRun
// // FullName:  CTI_Processing::InitFocusingRun
// // Access:    public 
// // Returns:   UINT
// // Qualifier:
// // Parameter: __in double dInitY
// // Parameter: __in double dInitR
// //************************************
// UINT CTI_Processing::InitFocusingRun(__in double dInitY, __in double dInitR)
// {
// 	if (m_pstDevice->MotionSequence.FocusingInitMove(dInitY, dInitR) == TRUE)
// 	{
// 		return TER_Pass;
// 	}
// 
// 	return TER_Fail;
// }

//=============================================================================
// Method		: CameraPowerOnOff
// Access		: public  
// Returns		: UINT
// Parameter	: BOOL bMode
// Qualifier	:
// Last Update	: 2017/2/15 - 14:20
// Desc.		:
//=============================================================================
UINT CTI_Processing::CameraPowerOnOff(BOOL bMode, UINT nCh /*= 0*/)
{
	if (m_pstModelInfo == NULL)
		return FALSE;

	BOOL bResult = FALSE;
	float fVolt = 0;
	CString strText;
	enTestEachResult enResult;

	m_pstModelInfo->nPicViewMode = PIC_Standby;

	if (bMode == ON)
	{
		bResult = m_pstDevice->PCBCamBrd[0].Send_SetVolt(m_pstModelInfo->fVoltage);

		if (bResult == TRUE)
		{
			if (m_pstModelInfo->nGrabType == GrabType_NTSC)
			{
				bResult = TRUE;
			}
			else if (m_pstModelInfo->nGrabType == GrabType_LVDS)
			{
				// 채널 별 영상 시작.
				m_pstDevice->DAQCtrl.SetDAQOption(nCh, m_pstModelInfo->stLVDSInfo.stLVDSOption);
				m_pstDevice->DAQCtrl.Cam_Restart(nCh);
			}

			DoEvents(m_pstModelInfo->nCameraDelay);

			if (CameraConnect() == FALSE)
				bResult = FALSE;

			if (bResult)
				m_Log.LogMsg(_T("Camera Power ON"));
			else
				m_Log.LogMsg_Err(_T("Camera Power ON : Signal Err"));

			if (bResult)
			{
				::SendNotifyMessage(m_hOwnerWnd, WM_INCREASE_POGO_CNT, 0, 0);
			}
		}
	}
	else
	{
		bResult = m_pstDevice->PCBCamBrd[0].Send_SetVolt(0);

		DoEvents(300);
	
		if (bResult == TRUE)
		{
			if (m_pstModelInfo->nGrabType == GrabType_LVDS)
			{
				// 채널 별 영상 종료
				m_pstDevice->DAQCtrl.Capture_Stop(nCh);
			}

			if (bResult)
				m_Log.LogMsg(_T("Camera Power OFF"));
			else
				m_Log.LogMsg_Err(_T("Camera Power Off : Signal Err"));
		}
	}

	if (bResult)
		enResult = TER_Pass;
	else
		enResult = TER_Fail;

	return enResult;
}

//=============================================================================
// Method		: CameraCurrent
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/2/15 - 16:15
// Desc.		:
//=============================================================================
UINT CTI_Processing::CameraCurrent()
{
	enTestEachResult enResult = TER_Fail;

	if (NULL == m_pstCurrent)
		return TER_Fail;

	// 알고리즘
	enResult = (enTestEachResult)m_TlCurrent.Current_Test(m_pstCurrent);

	m_pstCurrent->stCurrentData.nCurrent += m_pstCurrent->stCurrentOp[CuOp_Channel1].nOffset;

	if (m_pstCurrent->stCurrentData.nCurrent >= m_pstModelInfo->stCurrent.stCurrentOp[CuOp_Channel1].nMinCurrent
		&& m_pstCurrent->stCurrentData.nCurrent <= m_pstModelInfo->stCurrent.stCurrentOp[CuOp_Channel1].nMaxCurrent)
	{
		enResult = TER_Pass;
	}

	m_pstCurrent->stCurrentData.nResult = enResult;

	return enResult;
}