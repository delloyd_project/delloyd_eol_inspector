//*****************************************************************************
// Filename	: 	Wnd_Cfg_UserConfig.h
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_UserConfig_h__
#define Wnd_Cfg_UserConfig_h__

#pragma once

#include <afxwin.h>
#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Def_Enum_Cm.h"
#include "Def_TestItem.h"
#include "List_UserConfig.h"
#include "Def_TestDevice.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_UserConfig
//-----------------------------------------------------------------------------
class CWnd_Cfg_UserConfig : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_UserConfig)

public:
	CWnd_Cfg_UserConfig();
	virtual ~CWnd_Cfg_UserConfig();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate						(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize							(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedBnUserItem			(UINT nID);
	afx_msg void	OnBnClickedBnOperatorlistCtrl	(UINT nID);
	afx_msg void	OnBnClickedSeqCheck				();
	afx_msg void	OnSelChangeCbTestItem			();

	CFont			m_font;

	enum enUserConfigItem
	{
		UCI_OperatorID,
		UCI_MaxEnum,
	};

	enum enOperatorListCtrl
	{
		UCC_Add,
		UCC_Insert,
		UCC_Remove,
		UCC_Order_Up,
		UCC_Order_Down,
		UCC_MaxEnum,
	};

 	// 스텝 목록
	CList_UserConfig	m_lc_Operatorlist;

	// 스텝 항목 추가/삭제/이동
	CMFCButton		m_bn_ListCtrl[UCC_MaxEnum];
	
	// 스텝 설정 항목
	CMFCButton		m_chk_UserConfigItem[UCI_MaxEnum];
	CMFCMaskedEdit	m_ed_UserConfigItem[UCI_MaxEnum];
	
	CVGStatic		m_st_Name;
	CVGStatic		m_st_TestName;

	// UI에 설정된 스텝 데이터 구하기
	BOOL	GetUserConfigInfoData			(__out ST_UserConfig& stUserConfig);

	// 버튼 제어 함수
	void	Item_Add();
	void	Item_Insert();
	void	Item_Remove();
	void	Item_Up();
	void	Item_Down();
	void	Item_Enable(enUserConfigItem enUserConfig);

	CString m_szUserPath;
	CString m_szOperatorID;
	CString m_szOperatorPW;
	CString m_szRegistrationTime;

public:

	void SetRegistrationTime(__in CString szRegistrationTime)
	{
		m_szRegistrationTime = szRegistrationTime;
	};

	void SetOperatorID(__in LPCTSTR szOperatorID)
	{
		m_szOperatorID = szOperatorID;
	};

	void SetOperatorPW(__in LPCTSTR m_szOperatorPW)
	{
		m_szOperatorPW = m_szOperatorPW;
	};

	// 파일이 있는 경로 설정
	void SetPath(__in LPCTSTR szUserPath)
	{
		m_szUserPath = szUserPath;
	};

	// 검사기 종류 설정
	void		SetSystemType		(__in enInsptrSysType nSysType);

	// 저장된 Step Info 데이터 불러오기
	void		Set_UserConfigInfo	(__in const ST_UserConfigInfo* pstUserConfigInfo);
	void		Get_UserConfigInfo	(__out ST_UserConfigInfo& stUserConfigInfo);

	// 초기 상태로 설정 (New)
	void		Init_DefaultSet		();

};

#endif // Wnd_Cfg_UserConfig_h__


