﻿//*****************************************************************************
// Filename	: 	Grid_ModelInfo.cpp
// Created	:	2016/1/12 - 13:56
// Modified	:	2016/1/12 - 13:56
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Grid_ModelInfo.h"


static LPCTSTR lpszRowHeader[] =
{
	_T("Model"),
	_T("Model File"),	
	_T("Cable File"),
	_T("Cable Count"),
	NULL
};

typedef enum
{
	IDX_Y_ModelName = 0,
	IDX_Y_ModelFile,	
	IDX_Y_PogoFile,
	IDX_Y_PogoCount,
	IDX_ROW_MAX,
}enumRowHeaderModelInfo;

static const COLORREF clrRowHeader[] =
{
	RGB(63, 101,  169),
	RGB(0xFF, 200, 100),
	RGB(0xFF, 200, 100),
	RGB(0xFF, 200, 100),
	RGB(100, 150, 250),
	RGB(100, 150, 250),
	RGB(135, 169, 213)
};

static LPCTSTR lpszColHeader[] =
{
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};

typedef enum
{
	IDX_X_Header = 0,
	IDX_X_Item	= IDX_X_Header,
	IDX_X_Data,
	IDX_COL_MAX,
}enumColHeaderModelInfo;


#define		RGB_BLACK		RGB(0x00, 0x00, 0x00)
#define		RGB_WHITE		RGB(0xFF, 0xFF, 0xFF)
#define		RGB_YELLOW		RGB(0xFF, 0xFF, 0x00)
// #define		RGB_ROW_HEADER	RGB(63, 101,  169)
#define		RGB_ROW_HEADER	RGB(86, 86,  86)
#define		RGB_BK_ID		RGB(135, 169, 213)
#define		RGB_COL_HEADER	RGB(0xFF, 200, 100)
#define		RGB_TITLE		RGB(150, 200, 0xFF)

#define		RGB_BIT_SET		RGB(123, 255,  75) //RGB(112, 173, 71)
#define		RGB_BIT_CLEAR	RGB(100,  10,  10) //RGB(237, 125, 49)
#define		RGB_SENSOR		RGB( 80, 200,  80)
#define		RGB_POWER		RGB( 80,  80, 230)

#define		RGB_WARNING		RGB(250, 90, 90)

//=============================================================================
//
//=============================================================================
CGrid_ModelInfo::CGrid_ModelInfo()
{
	SetRowColCount(IDX_ROW_MAX, IDX_COL_MAX);

	//setup the fonts

	VERIFY(m_font_Header.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_NORMAL,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		FIXED_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Data.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_NORMAL,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		FIXED_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

//=============================================================================
//
//=============================================================================
CGrid_ModelInfo::~CGrid_ModelInfo()
{
	m_font_Header.DeleteObject();
	m_font_Data.DeleteObject();
}

//=============================================================================
// Method		: OnSetup
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/14 - 14:30
// Desc.		:
//=============================================================================
void CGrid_ModelInfo::OnSetup()
{
	__super::OnSetup();
}

//=============================================================================
// Method		: DrawGridOutline
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_ModelInfo::DrawGridOutline()
{
	CGrid_Base::DrawGridOutline();

	SetDefFont(&m_font_Data);

	// 헤더를 설정한다.
	InitHeader();
}

//=============================================================================
// Method		: CGrid_ModelInfo::CalGridOutline
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_ModelInfo::CalGridOutline()
{
	//CGrid_Base::CalGridOutline();

	// 윈도우 면적에 따라서 열의 너비를 결정
	CRect rect;
	GetWindowRect(&rect);
	int nWidth = rect.Width();
	int nHeight = rect.Height();

	if ((nWidth <= 0) || (nHeight <= 0))
		return;

	// 기본 열 추가 ---------------------------------------	
	// 열 너비용 비율을 만듦 ()
	int		nColWidthRate[IDX_COL_MAX] = { 40, 60};

	int		iMisc = 0;
	int		iUnitWidth = 0;
	// 기본 열 추가 ---------------------------------------
	for (UINT iCol = 0; iCol < m_nMaxCols; iCol++)
	{
		iUnitWidth = (nWidth * nColWidthRate[iCol]) / 100;
		iMisc += iUnitWidth;
		SetColWidth(iCol, iUnitWidth);
	}
	// Width 계산하고 남거나 모자르는 공간 계산하여 Name 영역에 추가	
	SetColWidth(IDX_X_Header, ((nWidth * nColWidthRate[IDX_X_Header]) / 100) + nWidth - iMisc);

	// 기본 열 추가 ---------------------------------------
	/*int iUnitWidth = nWidth / m_nMaxCols;
	int iMisc = nWidth - (iUnitWidth * (m_nMaxCols - 1));
	SetColWidth(IDX_X_Header, iMisc);
	for (UINT iCol = IDX_X_Header + 1; iCol < m_nMaxCols; iCol++)
	{
		SetColWidth(iCol, iUnitWidth);
	}*/

	// 패턴 행 헤더 추가 ----------------------------------		
	UINT nUnitHeight = nHeight / m_nMaxRows;
	UINT nRemindHeight = nHeight - (nUnitHeight * m_nMaxRows);

	// Height 계산하고 남거나 모자르는 공간 계산하여 헤더 Height 추가 처리
	for (UINT iRow = 0; iRow < nRemindHeight; iRow++)
	{
		SetRowHeight(iRow, nUnitHeight + 1);
	}

	for (UINT iRow = nRemindHeight; iRow < m_nMaxRows; iRow++)
	{
		SetRowHeight(iRow, nUnitHeight);
	}
}

//=============================================================================
// Method		: CGrid_ModelInfo::InitRowHeader
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_ModelInfo::InitHeader()
{
	for (UINT iRow = IDX_X_Item; iRow < m_nMaxRows; iRow++)
	{
		QuickSetFont		(IDX_X_Item, iRow, &m_font_Header);
		QuickSetBackColor_COLORREF	(IDX_X_Item, iRow, RGB_ROW_HEADER);
		QuickSetTextColor	(IDX_X_Item, iRow, RGB_WHITE);
		QuickSetText		(IDX_X_Item, iRow, lpszRowHeader[iRow]);

		QuickSetFont		(IDX_X_Data, iRow, &m_font_Data);
		QuickSetBackColor_COLORREF	(IDX_X_Data, iRow, RGB_WHITE);
		QuickSetTextColor	(IDX_X_Data, iRow, RGB_BLACK);		
	}	
}

//=============================================================================
// Method		: OnHint
// Access		: virtual protected  
// Returns		: int
// Parameter	: int col
// Parameter	: long row
// Parameter	: int section
// Parameter	: CString * string
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
int CGrid_ModelInfo::OnHint(int col, long row, int section, CString *string)
{
	return FALSE;
}

//=============================================================================
// Method		: CGrid_ModelInfo::OnGetCell
// Access		: virtual protected 
// Returns		: void
// Parameter	: int col
// Parameter	: long row
// Parameter	: CUGCell * cell
// Qualifier	:
// Last Update	: 2015/12/10 - 23:28
// Desc.		:
//=============================================================================
void CGrid_ModelInfo::OnGetCell(int col, long row, CUGCell *cell)
{
	CGrid_Base::OnGetCell(col, row, cell);

  	switch (row)
  	{
	case IDX_Y_ModelFile:
	case IDX_Y_ModelName:
		if (IDX_X_Data == col)
		{
			cell->SetBorder(cell->GetBorder() | UG_BDR_RMEDIUM);
		}
		break;
  	}
}

//=============================================================================
// Method		: OnDrawFocusRect
// Access		: virtual protected  
// Returns		: void
// Parameter	: CDC * dc
// Parameter	: RECT * rect
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_ModelInfo::OnDrawFocusRect(CDC *dc, RECT *rect)
{

}

//=============================================================================
// Method		: SetModelInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_ModelInfo * pstModelInfo
// Qualifier	:
// Last Update	: 2016/3/29 - 17:45
// Desc.		:
//=============================================================================
void CGrid_ModelInfo::SetModelInfo(__in const ST_ModelInfo* pstModelInfo)
{
	if (NULL == pstModelInfo)
		return;

	CString strValue;
	
	QuickSetText(IDX_X_Data, IDX_Y_ModelName, pstModelInfo->szModelCode);

	strValue.Format(_T("%s.%s"), pstModelInfo->szModelFile, MODEL_FILE_EXT);
	QuickSetText(IDX_X_Data, IDX_Y_ModelFile, strValue);

// 	strValue.Format(_T("%.1f"), pstModelInfo->fVoltage);
// 	QuickSetText(IDX_X_Data, IDX_Y_Voltage, strValue);

	strValue.Format(_T("%s.%s"), pstModelInfo->szPogoName, POGO_FILE_EXT);
	QuickSetText(IDX_X_Data, IDX_Y_PogoFile, strValue);

	RedrawAll();
}

//=============================================================================
// Method		: UpdatePogoCnt
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_PogoInfo * pstPogoInfo
// Qualifier	:
// Last Update	: 2017/6/28 - 16:56
// Desc.		:
//=============================================================================
void CGrid_ModelInfo::UpdatePogoCnt(__in const ST_PogoInfo* pstPogoInfo)
{
	if (pstPogoInfo)
	{
		CString strText;

		strText.Format(_T("%d / %d"), pstPogoInfo->dwCount[0], pstPogoInfo->dwCount_Max[0]);

		// 상한치에 근접했을 경우 붉은 색으로 표시
		if ((pstPogoInfo->dwCount_Max[0] <= pstPogoInfo->dwCount[0]) 
			|| ((pstPogoInfo->dwCount_Max[0] - pstPogoInfo->dwCount[0]) < 50))
		{
			QuickSetBackColor_COLORREF(IDX_X_Data, IDX_Y_PogoCount, RGB_WARNING);
		}
		else
		{
			QuickSetBackColor_COLORREF(IDX_X_Data, IDX_Y_PogoCount, RGB_WHITE);
		}

		QuickSetText(IDX_X_Data, IDX_Y_PogoCount, strText);

		RedrawCol(IDX_X_Data);
	}
}
