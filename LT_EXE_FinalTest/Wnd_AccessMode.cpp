﻿//*****************************************************************************
// Filename	: 	Wnd_AccessMode.cpp
// Created	:	2016/3/11 - 14:59
// Modified	:	2016/3/11 - 14:59
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
// Wnd_AcessMode.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_AccessMode.h"
#include "resource.h"
#include "Def_WindowMessage_Cm.h"

#define	IDC_ST_ACESS_MODE_T		1001
#define	IDC_ED_ACESS_PASSWORD	1003

#define	IDC_RB_OPER_MODE		1201
#define	IDC_RB_MES_MODE			1202
#define	IDC_RB_MANAGER_MODE		1203

//=============================================================================
// CWnd_AccessMode
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_AccessMode, CWnd_BaseView)

CWnd_AccessMode::CWnd_AccessMode()
{
	VERIFY(m_font_Default.CreateFont(
		17,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_Font.CreateFont(
		25,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_AccessMode::~CWnd_AccessMode()
{
	m_font_Default.DeleteObject();
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_AccessMode, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDOK,					OnBnClickedOK			)
	ON_BN_CLICKED(IDCANCEL,				OnBnClickedCancel		)
	ON_BN_CLICKED(IDC_RB_OPER_MODE,		OnBnClickedRbOperMode	)
	ON_BN_CLICKED(IDC_RB_MANAGER_MODE,	OnBnClickedRbManagerMode)
	ON_BN_CLICKED(IDC_RB_MES_MODE,		OnBnClickedRbMesMode	)
END_MESSAGE_MAP()

//=============================================================================
// CWnd_AccessMode message handlers
//=============================================================================
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/11 - 15:00
// Desc.		:
//=============================================================================
int CWnd_AccessMode::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Title.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Title.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Title.SetFont_Gdip(L"맑은 고딕", 20.0F);
	m_st_Title.Create(_T("Access Mode"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_rb_OperMode.Create(_T("Operator"), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON | WS_GROUP, rectDummy, this, IDC_RB_OPER_MODE);
	m_rb_AdminMode.Create(_T("Administrator"), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON, rectDummy, this, IDC_RB_MANAGER_MODE);

	m_rb_OperMode.SetFont(&m_font_Default);
	m_rb_AdminMode.SetFont(&m_font_Default);

	m_rb_OperMode.m_nFlatStyle = CMFCButton::BUTTONSTYLE_SEMIFLAT;
	m_rb_AdminMode.m_nFlatStyle = CMFCButton::BUTTONSTYLE_SEMIFLAT;

	m_rb_OperMode.SetImage(IDB_SELECTNO_16);
	m_rb_AdminMode.SetImage(IDB_SELECTNO_16);

	m_rb_OperMode.SetCheckedImage(IDB_SELECT_16);
	m_rb_AdminMode.SetCheckedImage(IDB_SELECT_16);

	m_rb_OperMode.SizeToContent();
	m_rb_AdminMode.SizeToContent();

	m_rb_OperMode.SetCheck(TRUE);

	m_st_Password.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Password.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Password.SetFont_Gdip(L"맑은 고딕", 10.0F);

	m_st_Password.Create(_T("Password"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_ed_Password.Create(dwStyle | WS_BORDER | WS_TABSTOP | ES_CENTER | ES_PASSWORD, rectDummy, this, IDC_ED_ACESS_PASSWORD);

	m_bn_OK.Create(_T("OK"), dwStyle | WS_TABSTOP, rectDummy, this, IDOK);
	m_bn_Cancel.Create(_T("Cancel"), dwStyle | WS_TABSTOP, rectDummy, this, IDCANCEL);

	m_ed_Password.SetFont(&m_Font);
	m_ed_Password.SetFocus();
	m_ed_Password.EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/11 - 15:00
// Desc.		:
//=============================================================================
void CWnd_AccessMode::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin = 10;
	int iSpacing = 5;
	int iCateSpacing = 5;

	int iLeft = iMagrin;
	int iTop = iMagrin;
	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;
	int iCtrlWidth = (iWidth - iSpacing) / 2 - iSpacing / 2;
	int iCtrlHeight = (iHeight - iSpacing - iCateSpacing) / 4;
	int iStaticWidth = 100;
	int iTempWidth = iWidth - iStaticWidth;
	int iSubLeft = iLeft + iStaticWidth;

	m_st_Title.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);
	iTop += iCtrlHeight + iSpacing;

	m_rb_OperMode.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	iLeft += iSpacing + iCtrlWidth;
	m_rb_AdminMode.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iLeft = iMagrin;
	iTop += iCtrlHeight + iSpacing;
	m_st_Password.MoveWindow(iLeft, iTop, iStaticWidth, iCtrlHeight);
	m_ed_Password.MoveWindow(iSubLeft + 5, iTop, iTempWidth - 6, iCtrlHeight);

	iTop += iCtrlHeight + iCateSpacing;
	iCtrlWidth = (iWidth - (iSpacing * 1)) / 2;
	m_bn_OK.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	iLeft += iCtrlWidth + iSpacing;
	m_bn_Cancel.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
}

//=============================================================================
// Method		: OnGetMinMaxInfo
// Access		: protected  
// Returns		: void
// Parameter	: MINMAXINFO * lpMMI
// Qualifier	:
// Last Update	: 2017/6/22 - 10:17
// Desc.		:
//=============================================================================
void CWnd_AccessMode::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMaxTrackSize.x = 450;
	lpMMI->ptMaxTrackSize.y = 200;

	lpMMI->ptMinTrackSize.x = 450;
	lpMMI->ptMinTrackSize.y = 200;

	CWnd::OnGetMinMaxInfo(lpMMI);
}

//=============================================================================
// Method		: OnBnClickedRbOperMode
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/15 - 17:09
// Desc.		:
//=============================================================================
void CWnd_AccessMode::OnBnClickedRbOperMode()
{
	m_ed_Password.SetWindowText(_T(""));
	m_ed_Password.EnableWindow(FALSE);
}

//=============================================================================
// Method		: OnBnClickedRbManagerMode
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/15 - 17:09
// Desc.		:
//=============================================================================
void CWnd_AccessMode::OnBnClickedRbManagerMode()
{
	m_ed_Password.EnableWindow(TRUE);
	m_ed_Password.SetFocus();
}

void CWnd_AccessMode::OnBnClickedRbMesMode()
{
	m_ed_Password.SetWindowText(_T(""));
	m_ed_Password.EnableWindow(FALSE);
}

//=============================================================================
// Method		: OnBnClickedOK
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/15 - 17:25
// Desc.		:
//=============================================================================
void CWnd_AccessMode::OnBnClickedOK()
{
	if (BST_CHECKED == m_rb_AdminMode.GetCheck())
	{
		if (CheckPassword())
		{
			SaveAcessMode();

			AfxGetApp()->GetMainWnd()->SendMessage(WM_PERMISSION_MODE, (LPARAM)m_AcessMode, 0);
		}
		else
		{
			// 암호가 틀림
			CString strTitle = _T("Password Err");
			AfxGetApp()->m_pszAppName = strTitle;
			AfxMessageBox(_T("Password incorrect!!"));

			return;
		}
	}
	else
	{
		SaveAcessMode();

		AfxGetApp()->GetMainWnd()->SendMessage(WM_PERMISSION_MODE, (LPARAM)m_AcessMode, 0);
	}

	AfxGetApp()->GetMainWnd()->EnableWindow(TRUE);
	m_ed_Password.SetWindowText(_T(""));
	ShowWindow(SW_HIDE);
}

//=============================================================================
// Method		: OnBnClickedCancel
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/15 - 17:26
// Desc.		:
//=============================================================================
void CWnd_AccessMode::OnBnClickedCancel()
{
	AfxGetApp()->GetMainWnd()->EnableWindow(TRUE);
	m_ed_Password.SetWindowText(_T(""));
	ShowWindow(SW_HIDE);
}

//=============================================================================
// Method		: LoadAcessMode
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/15 - 17:09
// Desc.		:
//=============================================================================
void CWnd_AccessMode::LoadAcessMode()
{
	if (m_AcessMode == Permission_Administrator || m_AcessMode == Permission_Engineer || m_AcessMode == Permission_CNC)
	{
		m_rb_AdminMode.SetCheck(BST_CHECKED);
		m_rb_OperMode.SetCheck(BST_UNCHECKED);
	}
	else if (m_AcessMode == Permission_MES)
	{
		m_rb_AdminMode.SetCheck(BST_UNCHECKED);
		m_rb_OperMode.SetCheck(BST_UNCHECKED);
	}
	else
	{
		m_rb_AdminMode.SetCheck(BST_UNCHECKED);
		m_rb_OperMode.SetCheck(BST_CHECKED);
	}
}

//=============================================================================
// Method		: SaveAcessMode
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/15 - 17:10
// Desc.		:
//=============================================================================
void CWnd_AccessMode::SaveAcessMode()
{
	if (BST_CHECKED == m_rb_AdminMode.GetCheck())
	{
		if (m_AcessMode == Permission_Engineer || m_AcessMode == Permission_CNC)
			return;

		m_AcessMode = Permission_Administrator;
	}
	else
	{
		m_AcessMode = Permission_Operator;
	}
}

//=============================================================================
// Method		: CheckPassword
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/6/15 - 17:17
// Desc.		:
//=============================================================================
BOOL CWnd_AccessMode::CheckPassword()
{
	CString szPass;
	CString szLoadPass;
	m_ed_Password.GetWindowText(szPass);

	szLoadPass = m_regManagement.LoadPassword_Admin();

	if (szPass == ENGINEER_PASSWORD)
	{
		m_AcessMode = Permission_Engineer;
		return TRUE;
	}
	if (szPass == CNC_PASSWORD || szPass == CNC_PASSWORD_2)
	{
		m_AcessMode = Permission_CNC;
		return TRUE;
	}
	if (0 == szPass.Compare(szLoadPass))
	{
		m_AcessMode = Permission_Administrator;
		return TRUE;
	}
	else
	{
		return FALSE;
	}

	if (0 == szPass.GetLength())
	{
		m_AcessMode = Permission_Operator;
		return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: SetAcessMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode nAcessMode
// Qualifier	:
// Last Update	: 2017/6/15 - 17:18
// Desc.		:
//=============================================================================
void CWnd_AccessMode::SetAcessMode(__in enPermissionMode nAcessMode)
{
	m_AcessMode = nAcessMode;
}

//=============================================================================
// Method		: SetButtonEnable
// Access		: public  
// Returns		: void
// Parameter	: BOOL bMode
// Qualifier	:
// Last Update	: 2017/6/15 - 16:57
// Desc.		:
//=============================================================================
void CWnd_AccessMode::SetButtonEnable(BOOL bMode)
{
	m_rb_OperMode.EnableWindow(bMode);
	m_rb_AdminMode.EnableWindow(bMode);
	m_bn_OK.EnableWindow(bMode);
	m_bn_Cancel.EnableWindow(bMode);
//	m_edbn_Cancel.EnableWindow(bMode);
}
