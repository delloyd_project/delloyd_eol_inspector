﻿// Wnd_OperModeOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_OperModeOp.h"

// CWnd_OperModeOp

typedef enum OperModeOp_ID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_OperModeOp, CWnd)

CWnd_OperModeOp::CWnd_OperModeOp()
{
	m_pstModelInfo = NULL;
	m_nTestItemCnt = 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

}

CWnd_OperModeOp::~CWnd_OperModeOp()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_OperModeOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_OperModeOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 18:01
// Desc.		:
//=============================================================================
int CWnd_OperModeOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_OPER_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szOperModeStatic[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < BTN_OPER_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szOperModeButton[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_OPER_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_OPER_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
	}

	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);

	return 0;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_OperModeOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/8/13 - 14:59
// Desc.		:
//=============================================================================
void CWnd_OperModeOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	//CWnd::OnShowWindow(bShow, nStatus);

	//if (NULL == m_pstModelInfo)
	//	return;

	//if (TRUE == bShow)
	//{
	//	m_pstModelInfo->nTestMode = TIID_OperationMode;
	//	m_pstModelInfo->nTestCnt = m_nTestItemCnt;
	//	m_pstModelInfo->nPicItem = PIC_OperMode;
	//}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/1/2 - 19:15
// Desc.		:
//=============================================================================
void CWnd_OperModeOp::OnRangeBtnCtrl(UINT nID)
{
	//UINT nIdex = nID - IDC_BTN_ITEM;

	//switch (nIdex)
	//{
	//case BTN_OPER_TEST:
	//	GetOwner()->SendMessage(WM_MANUAL_TEST, m_nTestItemCnt, TIID_OperationMode);
	//	break;
	//default:
	//	break;
	//}
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_OperModeOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iIdxCnt = OperOp_ItemNum;
	int iMargin = 10;
	int iSpacing = 5;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int iHeaderH = 40;
	int iList_H = iHeaderH + iIdxCnt * 12;
	int iCtrl_W = iWidth / 5;
	int iCtrl_H = 25;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	int iSTWidth = iWidth / 5;
	int iSTHeight = 25;

	m_st_Item[STI_OPER_DELAY].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
	m_ed_Item[EDT_OPER_DELAY].MoveWindow(iLeft + iSTWidth + 1, iTop, iSTWidth * 2, iSTHeight);

	iLeft = cx - iMargin - iSTWidth;
	m_bn_Item[BTN_OPER_TEST].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft = iMargin;
	iTop += iCtrl_H + iSpacing;
	m_List.MoveWindow(iLeft, iTop, iWidth, iList_H);
}

//=============================================================================
// Method		: SetUpdateUI
// Access		: public  
// Returns		: void
// Parameter	: 
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_OperModeOp::SetUpdateData()
{
	//if (m_pstModelInfo == NULL)
	//	return;

	//m_List.SetPtr_OperMode(&m_pstModelInfo->stOperMode[m_nTestItemCnt]);
	//m_List.InsertFullData();

	//CString strValue;
	//strValue.Format(_T("%d"), m_pstModelInfo->stOperMode[m_nTestItemCnt].nResetDelay);
	//m_ed_Item[EDT_OPER_DELAY].SetWindowText(strValue);
}

//=============================================================================
// Method		: GetUpdateUI
// Access		: public  
// Returns		: void
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_OperModeOp::GetUpdateData()
{
	//m_List.GetCellData();

	//CString strValue;
	//m_ed_Item[EDT_OPER_DELAY].GetWindowText(strValue);
	//m_pstModelInfo->stOperMode[m_nTestItemCnt].nResetDelay = _ttoi(strValue);
}
