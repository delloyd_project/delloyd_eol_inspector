﻿// Wnd_LEDTestOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_LEDTestOp.h"

// CWnd_LEDTestOp

typedef enum LEDCurrentOp_ID
{
	IDC_BTN_ITEM = 1001,
	IDC_CMB_ITEM = 2001,
	IDC_EDT_ITEM = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_LEDTestOp, CWnd)

CWnd_LEDTestOp::CWnd_LEDTestOp()
{
	m_pstModelInfo = NULL;
	m_nTestItemCnt = 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

}

CWnd_LEDTestOp::~CWnd_LEDTestOp()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_LEDTestOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_LEDTestOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 18:01
// Desc.		:
//=============================================================================
int CWnd_LEDTestOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_LEDCUR_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szLEDCurrentStatic[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < BTN_LEDCUR_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szLEDCurrentButton[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_LEDCUR_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_LEDCUR_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
	}

	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);

	return 0;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_LEDTestOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/8/13 - 14:59
// Desc.		:
//=============================================================================
void CWnd_LEDTestOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	//CWnd::OnShowWindow(bShow, nStatus);

	//if (NULL == m_pstModelInfo)
	//	return;

	//if (TRUE == bShow)
	//{
	//	m_pstModelInfo->nTestMode = TIID_LEDTest;
	//	m_pstModelInfo->nTestCnt = m_nTestItemCnt;
	//	m_pstModelInfo->nPicItem = PIC_LEDTest;
	//}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/1/2 - 19:15
// Desc.		:
//=============================================================================
void CWnd_LEDTestOp::OnRangeBtnCtrl(UINT nID)
{
	//UINT nIdex = nID - IDC_BTN_ITEM;

	//switch (nIdex)
	//{
	//case BTN_LEDCUR_TEST:
	//	GetOwner()->SendMessage(WM_MANUAL_TEST, m_nTestItemCnt, TIID_LEDTest);
	//	break;
	//default:
	//	break;
	//}
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_LEDTestOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iIdxCnt = LEDCurOp_ItemNum;
	int iMargin = 10;
	int iSpacing = 5;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int iHeaderH = 40;
	int iList_H = iHeaderH + iIdxCnt * 12;
	int iCtrl_W = iWidth / 5;
	int iCtrl_H = 25;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	int iSTWidth = iWidth / 5;
	int iSTHeight = 25;

	iLeft = iMargin;
	m_st_Item[STI_LEDCUR_RESET_CNT].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft = iMargin + iSTWidth + iSpacing;
	m_ed_Item[EDT_LEDCUR_RESET_CNT].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft = cx - iMargin - iSTWidth;
	m_bn_Item[BTN_LEDCUR_TEST].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iTop += iSTHeight + iSpacing;
	iLeft = iMargin;
	m_st_Item[STI_LEDCUR_RESET_DELAY].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft = iMargin + iSTWidth + iSpacing;
	m_ed_Item[EDT_LEDCUR_RESET_DELAY].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft = iMargin;
	iTop += iCtrl_H + iSpacing;
	m_List.MoveWindow(iLeft, iTop, iWidth, iList_H);
}

//=============================================================================
// Method		: SetUpdateUI
// Access		: public  
// Returns		: void
// Parameter	: 
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_LEDTestOp::SetUpdateData()
{
	//if (m_pstModelInfo == NULL)
	//	return;

	//CString szBuf;

	//szBuf.Format(_T("%d"), m_pstModelInfo->stLED[m_nTestItemCnt].nResetCnt);
	//m_ed_Item[EDT_LEDCUR_RESET_CNT].SetWindowText(szBuf);

	//szBuf.Format(_T("%d"), m_pstModelInfo->stLED[m_nTestItemCnt].nResetDelay);
	//m_ed_Item[EDT_LEDCUR_RESET_DELAY].SetWindowText(szBuf);

	//m_List.SetPtr_LEDCurrent(&m_pstModelInfo->stLED[m_nTestItemCnt]);
	//m_List.InsertFullData();
}

//=============================================================================
// Method		: GetUpdateUI
// Access		: public  
// Returns		: void
// Parameter	: __out ST_ModelInfo & stModelInfo
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_LEDTestOp::GetUpdateData()
{
	//m_List.GetCellData();

	//CString szBuf;

	//m_ed_Item[EDT_LEDCUR_RESET_CNT].GetWindowText(szBuf);
	//m_pstModelInfo->stLED[m_nTestItemCnt].nResetCnt = _ttoi(szBuf);

	//m_ed_Item[EDT_LEDCUR_RESET_DELAY].GetWindowText(szBuf);
	//m_pstModelInfo->stLED[m_nTestItemCnt].nResetDelay = _ttoi(szBuf);
}
