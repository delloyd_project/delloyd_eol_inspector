﻿#ifndef Wnd_AngleOp_h__
#define Wnd_AngleOp_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "List_AngleOp.h"
#include "List_AngleSubOp.h"
#include "Def_DataStruct.h"

// CWnd_AngleOp

enum enAngleStatic
{
	STI_AUTO_SET = 0,
	STI_ANG_ROI,
	STI_ANG_OPT,
	STI_ANG_MAX,
};

static LPCTSTR	g_szAngleStatic[] =
{
	_T("Spec Deviation"),
	_T("ROI LIST"),
	_T("OPTION LIST"),
	NULL
};

enum enAngleButton
{
	BTN_AUTO_SET = 0,
	BTN_ANG_TEST,
	BTN_ANG_MAX,
};

static LPCTSTR	g_szAngleButton[] =
{
	_T("AUTO SET"),
	_T("TEST"),
	NULL
};

enum enAngleComobox
{
	CMB_ANG_MAX = 1,
};

static LPCTSTR	g_szAngleCommoBox[] =
{
	NULL
};

enum enAngleEdit
{
	EDT_AUTO_SET,
	EDT_ANG_MAX,
};

class CWnd_AngleOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_AngleOp)

public:
	CWnd_AngleOp();
	virtual ~CWnd_AngleOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo		*m_pstModelInfo;

	CList_AngleOp		m_List;
	CList_AngleSubOp	m_ListSub;

	CFont				m_font;

	CVGStatic			m_st_Item[STI_ANG_MAX];
	CButton				m_bn_Item[BTN_ANG_MAX];
	CComboBox			m_cb_Item[CMB_ANG_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_ANG_MAX];

	// 검사 항목이 다수 인경우
	UINT				m_nTestItemCnt;

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_AngleOp_h__
