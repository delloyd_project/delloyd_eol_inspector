﻿// List_PatternNoiseOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_PatternNoiseOp.h"

#define IRtOp_ED_CELLEDIT			5001
#define IRtOp_CB_CELLCOMBO_TYPE		5002

// CList_PatternNoiseOp

IMPLEMENT_DYNAMIC(CList_PatternNoiseOp, CListCtrl)

CList_PatternNoiseOp::CList_PatternNoiseOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol	= 0;
	m_nEditRow	= 0;

	m_nWidth	= 720;
	m_nHeight	= 480;

	m_pstPatternNoise = NULL;
}

CList_PatternNoiseOp::~CList_PatternNoiseOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_PatternNoiseOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_PatternNoiseOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_PatternNoiseOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IRtOp_ED_CELLEDIT, &CList_PatternNoiseOp::OnEnKillFocusEdit)
	//ON_CBN_KILLFOCUS(IRtOp_CB_CELLCOMBO_TYPE, &CList_PatternNoiseOp::OnEnKillFocusCombo)
	//ON_CBN_SELCHANGE(IRtOp_CB_CELLCOMBO_TYPE, &CList_PatternNoiseOp::OnEnSelectFocusCombo)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_PatternNoiseOp 메시지 처리기입니다.
int CList_PatternNoiseOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IRtOp_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);

// 	m_cb_Type.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IRtOp_CB_CELLCOMBO_TYPE);
// 	m_cb_Type.ResetContent();
// 
// 	for (UINT nIdex = 0; NULL != g_szRotateMarkColor[nIdex]; nIdex++)
// 	{
// 		m_cb_Type.InsertString(nIdex, g_szRotateMarkColor[nIdex]);
// 	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_PatternNoiseOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[PNOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = PNOp_PosX; nCol < PNOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() - iHeaderWidth_PatternNoiseOp[PNOp_Object]) / (PNOp_MaxCol - PNOp_PosX);
		SetColumnWidth(nCol, iUnitWidth);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_PatternNoiseOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_PatternNoiseOp::InitHeader()
{
	for (int nCol = 0; nCol < PNOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_PatternNoiseOp[nCol], iListAglin_PatternNoiseOp[nCol], iHeaderWidth_PatternNoiseOp[nCol]);
	}

	for (int nCol = 0; nCol < PNOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_PatternNoiseOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_PatternNoiseOp::InsertFullData()
{
	if (m_pstPatternNoise == NULL)
		return;

 	DeleteAllItems();
 
	for (UINT nIdx = 0; nIdx < PNOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_PatternNoiseOp::SetRectRow(UINT nRow)
{
	if (m_pstPatternNoise == NULL)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_szRegionPatternNoise[nRow]);
	SetItemText(nRow, PNOp_Object, strValue);

	strValue.Format(_T("%d"), m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.CenterPoint().x);
	SetItemText(nRow, PNOp_PosX, strValue);

	strValue.Format(_T("%d"), m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.CenterPoint().y);
	SetItemText(nRow, PNOp_PosY, strValue);

	strValue.Format(_T("%d"), m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.Width());
	SetItemText(nRow, PNOp_Width, strValue);

	strValue.Format(_T("%d"), m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.Height());
	SetItemText(nRow, PNOp_Height, strValue);

}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_PatternNoiseOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	m_pstPatternNoise->stPatternNoiseOpt.iSelectROI = pNMItemActivate->iItem;
	m_pstPatternNoise->stPatternNoiseResult.iSelectROI = -1;

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_PatternNoiseOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < PNOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_PatternNoiseOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_PatternNoiseOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (m_pstPatternNoise == NULL)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	CRect rtTemp = m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi;

	switch (nCol)
	{
	case PNOp_PosX:
		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
		break;
	case PNOp_PosY:
		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
		break;
	case PNOp_Width:
		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].RectPosWH(iValue, rtTemp.Height());
		break;
	case PNOp_Height:
		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].RectPosWH(rtTemp.Width(), iValue);
		break;
	default:
		break;
	}

	if (m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.left < 0)
	{
		CRect rtTemp = m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi;

		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.left = 0;
		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.right = m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.left + rtTemp.Width();
	}

	if (m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.right > m_nWidth)
	{
		CRect rtTemp = m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi;

		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.right = m_nWidth;
		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.left = m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.right - rtTemp.Width();
	}

	if (m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.top < 0)
	{
		CRect rtTemp = m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi;

		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.top = 0;
		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.bottom = rtTemp.Height() + m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.top;
	}

	if (m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.bottom > m_nHeight)
	{
		CRect rtTemp = m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi;

		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.bottom = m_nHeight;
		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.top = m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.bottom - rtTemp.Height();
	}

	if (m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.Height() <= 0)
		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].RectPosWH(m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.Width(), 1);

	if (m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.Height() >= m_nHeight)
		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].RectPosWH(m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.Width(), m_nHeight);

	if (m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.Width() <= 0)
		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].RectPosWH(1, m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.Height());

	if (m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.Width() >= m_nWidth)
		m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].RectPosWH(m_nWidth, m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.Height());

	CString strValue;

	switch (nCol)
	{
	case PNOp_PosX:
		strValue.Format(_T("%d"), m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.CenterPoint().x);
		break;
	case PNOp_PosY:
		strValue.Format(_T("%d"), m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.CenterPoint().y);
		break;
	case PNOp_Width:
		strValue.Format(_T("%d"), m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.Width());
		break;
	case PNOp_Height:
		strValue.Format(_T("%d"), m_pstPatternNoise->stPatternNoiseOpt.stRegionOp[nRow].rtRoi.Height());
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}


//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_PatternNoiseOp::GetCellData()
{
	if (m_pstPatternNoise == NULL)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_PatternNoiseOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);

		iValue = iValue + ((zDelta / 120));

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		UpdateCellData(m_nEditRow, m_nEditCol, iValue);
	}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}
