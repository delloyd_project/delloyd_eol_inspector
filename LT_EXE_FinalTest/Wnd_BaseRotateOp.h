﻿#ifndef Wnd_BaseRotateOp_h__
#define Wnd_BaseRotateOp_h__

#pragma once

#include "Wnd_RotateOp.h"

// CWnd_BaseRotateOp

class CWnd_BaseRotateOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseRotateOp)

public:
	CWnd_BaseRotateOp();
	virtual ~CWnd_BaseRotateOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
	CWnd_RotateOp m_wndRotateOp[TICnt_Rotation];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseRotateOp_h__
