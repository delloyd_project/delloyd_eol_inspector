﻿//*****************************************************************************
// Filename	: Wnd_WorklistSFR.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistSFR_h__
#define Wnd_WorklistSFR_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_SFR.h"

//=============================================================================
// Wnd_WorklistSFR
//=============================================================================
class CWnd_WorklistSFR : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistSFR)

public:
	CWnd_WorklistSFR();
	virtual ~CWnd_WorklistSFR();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

	//CList_Work_SFR				m_list_SFR[TICnt_SFR];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistSFR_h__


