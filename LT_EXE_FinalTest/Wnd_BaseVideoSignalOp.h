﻿#ifndef Wnd_BaseVideoSignalOp_h__
#define Wnd_BaseVideoSignalOp_h__

#pragma once

#include "Wnd_VideoSignalOp.h"

// CWnd_BaseVideoSignalOp

class CWnd_BaseVideoSignalOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseVideoSignalOp)

public:
	CWnd_BaseVideoSignalOp();
	virtual ~CWnd_BaseVideoSignalOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
//	CWnd_VideoSignalOp m_wndVideoSignalOp[TICnt_VideoSignal];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseVideoSignalOp_h__
