﻿#ifndef Wnd_LEDTestOp_h__
#define Wnd_LEDTestOp_h__

#pragma once

#include "VGStatic.h"
#include "List_LEDCurrentOp.h"
#include "Def_DataStruct.h"

// CWnd_LEDTestOp

enum enLEDCurrentStatic
{
	STI_LEDCUR_RESET_CNT,
	STI_LEDCUR_RESET_DELAY,
	STI_LEDCUR_MAX
};

static LPCTSTR	g_szLEDCurrentStatic[] =
{
	_T("Reset Count"),
	_T("Reset Delay"),
	NULL
};

enum enLEDCurrentButton
{
	BTN_LEDCUR_TEST,
	BTN_LEDCUR_MAX,
};

static LPCTSTR	g_szLEDCurrentButton[] =
{
	_T("TEST"),
	NULL
};

enum enLEDCurrentComobox
{
	CMB_LEDCUR_MAX = 1,
};

enum enLEDCurrentEdit
{
	EDT_LEDCUR_RESET_CNT,
	EDT_LEDCUR_RESET_DELAY,
	EDT_LEDCUR_MAX
};
class CWnd_LEDTestOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_LEDTestOp)

public:
	CWnd_LEDTestOp();
	virtual ~CWnd_LEDTestOp();

	void SetUpdateData();
	void GetUpdateData();

	void SetPtr_ModelInfo(ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = pstModelInfo;
	}

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

protected:

	ST_ModelInfo	*m_pstModelInfo;
	CList_LEDCurrentOp m_List;

	CFont			m_font;

	CVGStatic			m_st_Item[STI_LEDCUR_MAX];
	CButton				m_bn_Item[BTN_LEDCUR_MAX];
	CComboBox			m_cb_Item[CMB_LEDCUR_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_LEDCUR_MAX];

	// 검사 항목이 다수 인경우
	UINT			m_nTestItemCnt;

	DECLARE_MESSAGE_MAP()

	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int	 OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnRangeBtnCtrl(UINT nID);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
};

#endif // Wnd_LEDTestOp_h__
