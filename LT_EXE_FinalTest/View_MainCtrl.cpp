﻿//*****************************************************************************
// Filename	: View_MainCtrl.cpp
// Created	: 2010/11/26
// Modified	: 2016/07/21
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
// View_MainCtrl.cpp : CView_MainCtrl 클래스의 구현
//

#include "stdafx.h"
#include "resource.h"

#include "View_MainCtrl.h"
#include "Def_CompileOption.h"
#include "CommonFunction.h"
#include "Registry.h"
#include "Pane_CommStatus.h"
#include "AToken.h"
#include "File_Report.h"
#include "Dlg_Barcode.h"
#include "Dlg_SelectLot.h"
#include "Dlg_ParticleManual.h"
#include "Dlg_FailConfirm.h"
#include "Dlg_StartUpCheck.h"
#include "Dlg_ChkPassword.h"

#include "CommonFunction.h"

#include <strsafe.h>
#include <winsock2.h>
#include <iphlpapi.h>
#include <icmpapi.h>

#pragma comment(lib, "iphlpapi.lib")

//msec 측정 라이브러리 추가
#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//=============================================================================
// CView_MainCtrl 생성자
//=============================================================================
CView_MainCtrl::CView_MainCtrl()
{
	// 윈도우 포인터	
	m_nWndIndex				= 0;
		
	for (int iCnt = 0; iCnt < SUBVIEW_MAX; iCnt++)
 		m_pWndPtr[iCnt]		= NULL;
	
	m_pwndCommPane			= NULL;
	m_pdlgBarcode			= NULL;

	InitConstructionSetting();
}

//=============================================================================
// CView_MainCtrl 소멸자
//=============================================================================
CView_MainCtrl::~CView_MainCtrl()
{
	TRACE(_T("<<< Start ~CView_MainCtrl >>> \n"));

	if (NULL != m_pdlgBarcode)
	{
		m_pdlgBarcode->DestroyWindow();
		delete m_pdlgBarcode;
		m_pdlgBarcode = NULL;
	}

	DeleteSplashScreen();

	TRACE(_T("<<< End ~CView_MainCtrl >>> \n"));
}

BEGIN_MESSAGE_MAP(CView_MainCtrl, CWnd)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_MESSAGE	(WM_LOGMSG,				OnLogMsg)
	ON_MESSAGE	(WM_TEST_START,			OnTestStart)
	ON_MESSAGE	(WM_TEST_STOP,			OnTestStop)
	ON_MESSAGE	(WM_TEST_INIT,			OnTestInit)
	ON_MESSAGE	(WM_TEST_COMPLETED,		OnTestCompleted)
	ON_MESSAGE	(WM_PERMISSION_MODE,	OnSwitchPermissionMode)
	ON_MESSAGE	(WM_CHANGED_MODEL,		OnChangeModel)
	ON_MESSAGE	(WM_CHANGE_MODE,		OnChangeMode)
	ON_MESSAGE	(WM_MANUAL_DEV_CTRL,	OnDeviceCtrl)
	ON_MESSAGE	(WM_RECV_BARCODE,		OnRecvBarcode)
	ON_MESSAGE	(WM_INCREASE_POGO_CNT,	OnPogoCnt_Increase)
	ON_MESSAGE	(WM_UPDATE_POGO_CNT,	OnPogoCnt_Update)
	ON_MESSAGE	(WM_CAMERA_SELECT,		OnCameraSelect)
	ON_MESSAGE	(WM_CAMERA_CHG_STATUS,	OnCameraChgStatus)
	ON_MESSAGE	(WM_CAMERA_RECV_VIDEO,	OnCameraRecvVideo)
	ON_MESSAGE	(WM_RECV_DIO_BIT,		OnRecvDIOMon)
	ON_MESSAGE	(WM_RECV_DIO_FST_READ,	OnRecvDIOFirstRead)
	ON_MESSAGE	(WM_MODEL_SAVE,			OnModelSave)
	ON_MESSAGE	(WM_RECV_MAIN_BRD_ACK,	OnRecvMainBrd)
	ON_MESSAGE	(WM_COMM_STATUS_MES,	OnCommStatus_MES)
	ON_MESSAGE	(WM_RECV_MES,			OnRecvMES)
	ON_MESSAGE	(WM_CHANGED_MOTOR,		OnChangeMotor)
	ON_MESSAGE	(WM_MASTER_SET,			OnMasterSet)
//	ON_MESSAGE	(WM_MANUAL_CONTROL,		OnManualCtrlCmd)
	ON_MESSAGE  (WM_MOTOR_ORIGIN,		OnMotorOriginAll)
	ON_MESSAGE	(WM_MANUAL_TEST,		OnManualTestCmd)
	ON_MESSAGE	(WM_MASTER_TEST,		OnMasterTest)
	ON_MESSAGE(WM_MASTER_MODE,			OnMasterMode)
	

END_MESSAGE_MAP()


//=============================================================================
// CView_MainCtrl 메시지 처리기
//=============================================================================
//=============================================================================
// Method		: CView_MainCtrl::PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2010/11/26 - 13:59
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

//=============================================================================
// Method		: CView_MainCtrl::OnPaint
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/11/26 - 14:00
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnPaint() 
{
	CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트
	
	// 그리기 메시지에 대해서는 CWnd::OnPaint()를 호출하지 마십시오.
}

//=============================================================================
// Method		: CView_MainCtrl::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2010/11/26 - 14:06
// Desc.		:
//=============================================================================
int CView_MainCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	if (!m_wnd_MainView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + 1, NULL))
	{
		TRACE0("m_wndMainView 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}

	if (!m_wnd_RecipeView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + 2, NULL))
	{
		TRACE0("m_wndRecipeView 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}	

	if (!m_wnd_DeviceView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + 3, NULL))
	{
		TRACE0("m_wndDeviceView 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}		

	
	if (!m_wnd_WorklistView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + 4, NULL))
	{
		TRACE0("m_wndWorklistView 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}

	if (!m_wnd_MotorView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + 5, NULL))
	{
		TRACE0("m_wndMotorView 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}
	
	if (!m_wnd_LogView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + 6, NULL))
	{
		TRACE0("m_wndLogView 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}	

	if (!m_wnd_AccessMode.CreateEx(NULL, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(COLOR_WINDOW + 1)), _T("Access Mode"), WS_POPUPWINDOW | WS_SIZEBOX | WS_EX_TOPMOST, CRect(0, 0, 0, 0), this, NULL))
	{
		TRACE0("m_wndAccessMode 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}

	if (!m_wnd_ModelMode.CreateEx(NULL, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(COLOR_WINDOW + 2)), _T("Model Mode"), WS_POPUPWINDOW | WS_SIZEBOX | WS_EX_TOPMOST, CRect(0, 0, 0, 0), this, NULL))
	{
		TRACE0("m_wndModelMode 뷰 창을 만들지 못했습니다.\n");
		return -1;
	}

	m_wnd_Origin.SetPtr_Device(&m_Device);
	if (!m_wnd_Origin.CreateEx(NULL, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(COLOR_WINDOW + 1)), _T("Oirigin"), WS_POPUPWINDOW | WS_EX_TOPMOST, CRect(0, 0, 0, 0), this, NULL))
	{
		TRACE0("m_wndOrigin창을 만들지 못했습니다.\n");
		return -1;
	}

	m_wnd_Origin.SetOwner(GetOwner());
	m_wnd_Origin.ShowWindow(SW_HIDE);

	m_wnd_ModelMode.SetOwner(this);
	m_wnd_MainView.ShowWindow(SW_SHOW);

	m_pdlgBarcode = new CDlg_Barcode;
	m_pdlgBarcode->SetBarcodeCount(USE_CHANNEL_CNT);
	m_pdlgBarcode->Create(CDlg_Barcode::IDD, GetDesktopWindow());

	m_pWndPtr[0]	= (CWnd*)&m_wnd_MainView;	// Main 화면
	m_pWndPtr[1]	= (CWnd*)&m_wnd_RecipeView;	// 모듈 설정
	m_pWndPtr[2]	= (CWnd*)&m_wnd_DeviceView;	// 장비 제어
	m_pWndPtr[3]	= (CWnd*)&m_wnd_MotorView;	// 모터 제어
	m_pWndPtr[4]	= (CWnd*)&m_wnd_WorklistView;// Worklist
	m_pWndPtr[5]	= (CWnd*)&m_wnd_LogView;		// 로그

	for (UINT nCh = 0; nCh < 1; nCh++)
	{
		DisplayVideo_NoSignal(nCh);
	}

	// 초기 세팅
	CreateSplashScreen (this, IDB_BITMAP_Luritech);

	InitUISetting ();
	InitDeviceSetting();



	return 0;
}

//=============================================================================
// Method		: CView_MainCtrl::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2010/11/26 - 14:06
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	m_wnd_MainView.MoveWindow(0, 0, cx, cy);
	m_wnd_RecipeView.MoveWindow(0, 0, cx, cy);
	m_wnd_DeviceView.MoveWindow(0, 0, cx, cy);
	m_wnd_MotorView.MoveWindow(0, 0, cx, cy);
	m_wnd_WorklistView.MoveWindow(0, 0, cx, cy);
	m_wnd_LogView.MoveWindow(0, 0, cx, cy);
}

//=============================================================================
// Method		: CView_MainCtrl::OnEraseBkgnd
// Access		: protected 
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2010/10/13 - 10:40
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::OnEraseBkgnd(CDC* pDC)
{
	if (m_brBkgr.GetSafeHandle() == NULL)
	{
		return CWnd::OnEraseBkgnd(pDC);
	}

	ASSERT_VALID(pDC);

	CRect rectClient;
	GetClientRect(rectClient);

	if (m_brBkgr.GetSafeHandle() != NULL)
	{
		pDC->FillRect(rectClient, &m_brBkgr);
	}
	else
	{
		CWnd::OnEraseBkgnd(pDC);
	}

	return TRUE;
}

//=============================================================================
// Method		: CView_MainCtrl::OnCtlColor
// Access		: protected 
// Returns		: HBRUSH
// Parameter	: CDC * pDC
// Parameter	: CWnd * pWnd
// Parameter	: UINT nCtlColor
// Qualifier	:
// Last Update	: 2010/10/12 - 17:35
// Desc.		:
//=============================================================================
HBRUSH CView_MainCtrl::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	if (m_brBkgr.GetSafeHandle() != NULL)
	{
#define AFX_MAX_CLASS_NAME 255
#define AFX_STATIC_CLASS _T("Static")
#define AFX_BUTTON_CLASS _T("Button")

		if (nCtlColor == CTLCOLOR_STATIC)
		{
			TCHAR lpszClassName [AFX_MAX_CLASS_NAME + 1];

			::GetClassName(pWnd->GetSafeHwnd(), lpszClassName, AFX_MAX_CLASS_NAME);
			CString strClass = lpszClassName;

			if (strClass == AFX_STATIC_CLASS)
			{
				pDC->SetBkMode(TRANSPARENT);
				return(HBRUSH) ::GetStockObject(HOLLOW_BRUSH);
			}

			if (strClass == AFX_BUTTON_CLASS)
			{
				if ((pWnd->GetStyle() & BS_GROUPBOX) == 0)
				{
					pDC->SetBkMode(TRANSPARENT);
				}

				return(HBRUSH) ::GetStockObject(HOLLOW_BRUSH);
			}
		}
	}

	return CWnd::OnCtlColor(pDC, pWnd, nCtlColor);
}

//=============================================================================
// Method		: CView_MainCtrl::OnTimer
// Access		: protected 
// Returns		: void
// Parameter	: UINT_PTR nIDEvent
// Qualifier	:
// Last Update	: 2010/12/9 - 15:57
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnTimer(UINT_PTR nIDEvent)
{
	// 타이머 처리
	

	CWnd::OnTimer(nIDEvent);
}

//=============================================================================
// Method		: CView_MainCtrl::OnLogMsg
// Access		: protected 
// Returns		: LRESULT
// Parameter	: WPARAM wParam	-> 메세지 문자열
// Parameter	: LPARAM lParam	
//					-> HIWORD : 오류 메세지 인가?
//					-> LOWORD : 로그 종류 (기본, PLC, 관리PC 등)
// Qualifier	:
// Last Update	: 2010/10/14 - 17:38
// Desc.		: 로그 처리용
//	LOG_TAB_PLC		= 0,
//	LOG_TAB_MANPC,
//	LOG_TAB_IRDA,
//	LOG_TAB_BCR,
//=============================================================================
LRESULT CView_MainCtrl::OnLogMsg( WPARAM wParam, LPARAM lParam )
{
	BOOL	bError = (BOOL)HIWORD(lParam);
	UINT	nType  = LOWORD(lParam);

	if (NULL == (LPCTSTR)wParam)
	{
		return FALSE;
	}
	
	AddLog((LPCTSTR)wParam, bError, nType);

	return TRUE;
}

//=============================================================================
// Method		: OnTestStart
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/5/29 - 0:12
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnTestStart(WPARAM wParam, LPARAM lParam)
{
	if (m_stInspInfo.PermissionMode == Permission_Administrator
		|| m_stInspInfo.PermissionMode == Permission_Engineer || m_stInspInfo.PermissionMode == Permission_CNC)
	{
		if (OnGetStartBtnStatus() == TRUE)
		{
			if(m_bTeststop == FALSE)
				StartOperation_AutoAll();
		}
	}
	else if (m_stInspInfo.PermissionMode == Permission_Operator)
	{
		//if (m_stInspInfo.LotInfo.bLotStatus == TRUE)
		{
			if (OnGetStartBtnStatus() == TRUE)
			{
				if (m_bTeststop == FALSE)
					StartOperation_AutoAll();
			}
		}
	}
	else if (m_stInspInfo.PermissionMode == Permission_MES)
	{
		//if (m_stInspInfo.LotInfo.bLotStatus == TRUE)
		{
			if (OnGetStartBtnStatus() == TRUE)
			{
				if (m_bTeststop == FALSE)
					StartOperation_AutoAll();
			}
		}
	}

	//SendMessage(WM_POPUPTEST_PASS, 0, 0);

	return TRUE;
}

//=============================================================================
// Method		: OnTestStop
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/5/29 - 0:16
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnTestStop(WPARAM wParam, LPARAM lParam)
{
	// Manual, EMG

	// 진행 중인 모든 작업 중지

	if (IsTesting_All())
	{
		m_bTeststop = TRUE;

		m_stInspInfo.CamInfo.nJudgment = TR_UserStop;

		StopProcess_All();
		OnAddErrorInfo(Err_UserStop);

		int a = 0;
		// Stop signal flag
		m_bTeststop = FALSE;
	}

	//SendMessage(WM_POPUPTEST_FAIL, 0, 0);

	return 1;
}

//=============================================================================
// Method		: OnTestInit
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/11/11 - 1:58
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnTestInit(WPARAM wParam, LPARAM lParam)
{
	if (IsTesting())
	{
		MessageView(_T("Inspection is Running.\r\nRetry after the work is finished."));
		return FALSE;
	}

// 	if (IDYES == AfxMessageBox(_T("데이터를 초기화 하시겠습니까?"), MB_YESNO))
// 	{
// 		// 알람 초기화
// 		OnInitDigitalIOSignal();
// 
// 		// 데이터 초기화
// 		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
// 		{	
// 			m_stInspInfo.DualCamInfo[nIdx].Reset();
// 		}
// 		OnResetCamInfo();
// 		//OnUpdateCamInfo_All();
// 		OnUpdateSiteCamInfo();
// 	}

	return TRUE;
}

//=============================================================================
// Method		: OnTestCompleted
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/5/30 - 13:43
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnTestCompleted(WPARAM wParam, LPARAM lParam)
{
	// 최종 검사 판정 업데이트?
	OnJugdement_And_Report();

	return TRUE;
}

//=============================================================================
// Method		: OnSwitchPermissionMode
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/29 - 16:46
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnSwitchPermissionMode(WPARAM wParam, LPARAM lParam)
{
 	if (IsTesting())
 	{
 		//m_wndMainView.SetInspectionMode_Cancle();
		MessageView(_T("Inspection is Running.\r\nRetry after the work is finished."));
 		return FALSE;
 	}

	enPermissionMode InspMode = (enPermissionMode)wParam;

	m_stInspInfo.PermissionMode = InspMode;

	m_wnd_MainView.SetInspectionMode(InspMode);

	// MainFrm으로 권한 변경 통보
	GetParent()->SendMessage(WM_PERMISSION_MODE, (WPARAM)InspMode, 0);

	return TRUE;
}

//=============================================================================
// Method		: OnChangeModel
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/25 - 18:31
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnChangeModel(WPARAM wParam, LPARAM lParam)
{
 	if (IsTesting())
 	{
		MessageView(_T("Inspection is Running.\r\nRetry after the work is finished."));
 		return FALSE;
 	}

	// 모델 파일에서 모델 정보 불러오기
	CString strModel = (LPCTSTR)wParam;
	BOOL bNotifyModelView = (BOOL)lParam;
	
	LoadModelInfo(strModel, bNotifyModelView);

	//if (bNotifyModelView == TRUE)
	//{
	//	m_stInspInfo.bStartUpCheckProc = TRUE;
	//	m_stInspInfo.bStartUpCheckPass = FALSE;
	//	m_stInspInfo.bStartUpCheckFail = FALSE;

	//	for (int i = 0; i < TIID_MaxEnum; i++)
	//	{
	//		m_stInspInfo.nNGTestItemMaster[i] = 0;
	//		m_stInspInfo.nNGTestItemType[i] = 0;
	//	}


	//	CDlg_StartUpCheck StartUpCheck;
	//	StartUpCheck.SetPtrInspectionInfo(&m_stInspInfo);
	//	if (StartUpCheck.DoModal() == IDOK)
	//	{
	//		OnTestMasterStartUpEvent(TRUE);
	//	}
	//	else
	//	{
	//		m_stInspInfo.bStartUpCheckPass = TRUE;
	//		m_stInspInfo.bStartUpCheckFail = TRUE;

	//		if (m_stInspInfo.PermissionMode == Permission_Operator)
	//		{
	//			OnSetLotStartBtnStatus(TRUE);
	//			OnSetLotStopBtnStatus(TRUE);
	//			OnSetStartBtnStatus(FALSE);
	//			OnSetStopBtnStatus(FALSE);
	//		}
	//		else if (m_stInspInfo.PermissionMode == Permission_MES)
	//		{
	//			OnSetLotStartBtnStatus(FALSE);
	//			OnSetLotStopBtnStatus(FALSE);
	//			OnSetStartBtnStatus(TRUE);
	//			OnSetStopBtnStatus(TRUE);
	//		}

	//	}

	//}
	

	return TRUE;
}

//=============================================================================
// Method		: OnChangeLot
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/10/28 - 16:03
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnChangeMode(WPARAM wParam, LPARAM lParam)
{
	enModelStatus LotStatus = (enModelStatus)wParam;

	if (IsTesting())
	{
		MessageView(_T("Inspection is Running.\r\nRetry after the work is finished."));
		return FALSE;
	}

	switch (LotStatus)
	{
	case MESMode:
		OnChangeLotInfo(MESMode);
		break;
	case LOT_Start:
		OnChangeLotInfo(LOT_Start);
		break;
	case LOT_End:
		OnChangeLotInfo(LOT_End);
		break;
	case Model_Change:
		OnChangeModelInfo();
		break;
	case ManualMode:
		ManualAutoMode(TRUE);
		break;
	case AutoMode:
		ManualAutoMode(FALSE);
		break;
	case MASTER_Start:
		OnSetMaterSetView(TRUE);
		break;
	case MASTER_End:

		//마스터 모드 끝낼시
		OnShowSplashScreen(TRUE, _T("Master Setting Finish"));

		OnSetMaterSetView(FALSE);

		Sleep(1000);

		OnShowSplashScreen(FALSE);
		break;
	default:
		break;
	}

	return TRUE;
}

BOOL CView_MainCtrl::InitStartDeviceProgress()
{
	m_Device.DigitalIOCtrl.Start_Monitoring();
	//OnDOut_TowerLamp(enLampColor::Lamp_All, FALSE);

	Sleep(200);

	//if (FALSE == m_Device.DigitalIOCtrl.Get_DI_Status(DI_))
	//{
	//	AfxMessageBox(_T("[ERR] MAIN POWER OFF"));
	//	return FALSE;
	//}DI_SensorParticleOut

	if (FALSE == m_Device.DigitalIOCtrl.Get_DI_Status(DI_EMO))
	{
		AfxMessageBox(_T("[ERR] EMO STATE Check"));
		return FALSE;
	}

	//if (AXT_RT_SUCCESS != m_Device.DigitalIOCtrl.Set_DO_Status(DO_ImgT_09_Light, IO_SignalT_SetOff))
	//{
	//	AfxMessageBox(_T("[ERR] Light OFF Check"));
	//	return FALSE;
	//}

	OnShowSplashScreen(FALSE);
//#ifndef MOTION_NOT_USE	
	// 모터 원점
	if (m_Device.MotionManager.m_AllMotorData.pMotionParam != NULL)
	{
		if (FALSE == MotorOrigin())
		{
			//OnDOut_StartLamp(FALSE);
			//OnDOut_StopLamp(FALSE);
			//OnDOut_TowerLamp(enLampColor::Lamp_Red, TRUE);
			return FALSE;
		}
	}
//#endif

	//OnDOut_StartLamp(TRUE);
	//OnDOut_StopLamp(FALSE);

	//OnDOut_TowerLamp(enLampColor::Lamp_Green, TRUE);

	//StartThread_Monitoring();

	return TRUE;
}
// 
// BOOL CView_MainCtrl::MotorOrigin()
// {
// 	BOOL  bReslut = FALSE;
// //#ifndef MOTION_NOT_USE
// 	CWnd_Origin*	pWnd_Origin;
// 	pWnd_Origin = new CWnd_Origin;
// 
// 	AfxGetApp()->GetMainWnd()->EnableWindow(FALSE);
// 
// 	pWnd_Origin->SetInspectorType((enInsptrSysType)SET_INSPECTOR);
// 	pWnd_Origin->SetOwner(this);
// 	pWnd_Origin->SetPtr_Device(&m_Device);
// 	pWnd_Origin->CreateEx(NULL, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(COLOR_WINDOW + 10)), _T("Message Mode"), WS_POPUPWINDOW | WS_SIZEBOX | WS_EX_TOPMOST, CRect(0, 0, 0, 0), this, NULL);
// 	pWnd_Origin->EnableWindow(TRUE);
// 	pWnd_Origin->CenterWindow();
// 
// 	//OnShowSplashScreen(FALSE);
// 	ShowSplashScreen(FALSE);
// 
// 	if (pWnd_Origin->DoModal() == TRUE)
// 		bReslut = TRUE;
// 	else
// 		bReslut = FALSE;
// 
// 	delete pWnd_Origin;
// 
// 	AfxGetApp()->GetMainWnd()->EnableWindow(TRUE);
// 	
// 	if (FALSE == bReslut)
// 		return FALSE;
// 
// //#endif
// 
// 	return bReslut;
// }

void CView_MainCtrl::EquipmentInit(__in UINT nCondition /*= 0*/)
{
	if (IsTesting())
	{
		TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
		OnLog_Err(_T("Inspection is in progress."));
		AfxMessageBox(_T("Inspection is in progress. \r\n\r\nPlease wait until the Inspection is finished."), MB_SYSTEMMODAL);
		return; //RC_AlreadyTesting;
	}

	// 확인
	if (IDYES == AfxMessageBox(_T("Are you sure you want to Initialize?"), MB_YESNO))
	{
		OnShowSplashScreen(TRUE, _T("Equipment Initialize"));
		

		// 그래버 영상 캡쳐 Off
		// 보드 전원 Off
		//for (UINT nParaIdx = 0; nParaIdx < g_InspectorTable[m_InspectionType].Grabber_Cnt; nParaIdx++)
		//{
		//	// * Capture Off
		//	OnDAQ_CaptureStop(nParaIdx);

		//	// * Camera Power Off
		//	OnCameraBrd_PowerOnOff(enPowerOnOff::Power_Off, nParaIdx);
		//}

		//OnLightBrd_Volt_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].fVolt);

		if (TRUE == InitStartDeviceProgress())
		{
			m_bFlag_ReadyTest = TRUE;
			m_stInspInfo.bForcedStop = FALSE;

			// 리셋 데이터
			//OnResetInfo_Loading();

			// 바코드 버퍼 리셋
			m_stInspInfo.ResetBarcodeBuffer();
		}
		else
		{
			m_bFlag_ReadyTest = FALSE;
		}
	}
}

//=============================================================================
// Method		: OnCommStatus_MES
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 16:22
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnCommStatus_MES(WPARAM wParam, LPARAM lParam)
{
	UINT	nDevice = (UINT)wParam;
	UINT	nStatus = (UINT)lParam;

	OnSetStatus_MES(nStatus);

	return 0;
}

//=============================================================================
// Method		: OnRecvMES
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 16:22
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnRecvMES(WPARAM wParam, LPARAM lParam)
{
	ST_LG_MES_Protocol	m_stRecvProtocol;
	m_stRecvProtocol.SetRecvProtocol((const char*)wParam, (DWORD)lParam);

	USES_CONVERSION;
	m_stInspInfo.szRecvLotID.Format(_T("%s"), A2T(m_stRecvProtocol.szLotID));
	m_stInspInfo.nRecvLotTryCnt = atoi(m_stRecvProtocol.szLotTryCount.GetBuffer());
	m_stRecvProtocol.szLotTryCount.ReleaseBuffer();
	m_stRecvProtocol.szProtocol.ReleaseBuffer();

	m_stInspInfo.ModelInfo.szLotID		= m_stInspInfo.szRecvLotID;
	m_stInspInfo.szBarcodeBuf			= m_stInspInfo.szRecvLotID;
	m_stInspInfo.ModelInfo.nLotTryCnt	= m_stInspInfo.nRecvLotTryCnt;

	ZeroMemory(&m_stInspInfo.LotInfo.StartTime, sizeof(SYSTEMTIME));
	GetLocalTime(&m_stInspInfo.LotInfo.StartTime);

	// UI에 표시
	TRACE(_T("Barcode : %s\n"), m_stInspInfo.szRecvLotID);
	OnSetResetSiteInfo();

	OnSet_Barcode(m_stInspInfo.szRecvLotID);
	return 0;
}

//=============================================================================
// Method		: OnCameraSelect
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 16:22
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnCameraSelect(WPARAM wParam, LPARAM lParam)
{
	UINT nZoneIdx = (UINT)wParam;

	//m_Cat3DCtrl.SelectCamera(nZoneIdx);

	return 0;
}

//=============================================================================
// Method		: OnCameraChgStatus
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 16:22
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnCameraChgStatus(WPARAM wParam, LPARAM lParam)
{
	UINT nGrabType = m_stInspInfo.ModelInfo.nGrabType;

 	if (nGrabType == GrabType_NTSC)
	{
		DWORD* pdwStatus = m_Device.Cat3DCtrl.GetStatus();

		// 1 -> 0으로 바뀌면 화면에 표시
		for (UINT nChIdx = 0; nChIdx < g_InspectorTable[SET_INSPECTOR].nPCBCamCnt; nChIdx++)
		{
			if (m_stInspInfo.bVideoSignal[nChIdx] != pdwStatus[nChIdx])
			{
				m_stInspInfo.bVideoSignal[nChIdx] = (BOOL)pdwStatus[nChIdx];
				OnSetStatus_Signal_CA(nChIdx, m_stInspInfo.bVideoSignal[nChIdx]);
			}
		}
	}
// 	else if (nGrabType == GrabType_LVDS)
// 	{
// 		// 1 -> 0으로 바뀌면 화면에 표시
// 		for (UINT nChIdx = 0; nChIdx < USE_CHANNEL_CNT; nChIdx++)
// 		{
// 			m_stInspInfo.bVideoSignal[nChIdx] = m_Device.DAQCtrl.GetSignalStatus(nChIdx);
// 			OnSetStatus_Signal_CA(m_stInspInfo.bVideoSignal[nChIdx], nChIdx);
// 		}
// 	}
		

	return TRUE;
}

//=============================================================================
// Method		: OnCameraRecvVideo
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 16:22
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnCameraRecvVideo(WPARAM wParam, LPARAM lParam)
{
	DWORD dwChIdx = (DWORD)wParam;
	UINT nGrabType = m_stInspInfo.ModelInfo.nGrabType;

	DisplayVideo((UINT)dwChIdx, nGrabType);

	return TRUE;
}

//=============================================================================
// Method		: OnDeviceCtrl
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/8/10 - 10:42
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnDeviceCtrl(WPARAM wParam, LPARAM lParam)
{
	UINT nCtrlIdx = (UINT)wParam;
	return TRUE;
}

//=============================================================================
// Method		: OnRecvBarcode
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/10/21 - 17:33
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnRecvBarcode(WPARAM wParam, LPARAM lParam)
{
	// ::SendNotifyMessage(m_hOwnerWnd, m_WM_Ack, (WPARAM)m_szACKBuf, (LPARAM)m_dwACKBufSize);
	CStringA szTemp = (char*)wParam;
	DWORD dwLength = (DWORD)lParam;

	szTemp.Remove('\r');
	szTemp.Remove('\n');

	USES_CONVERSION;
	CString szBarcode = A2T(szTemp.GetBuffer());
	szTemp.ReleaseBuffer();
		
	TRACE(_T("Barcode : %s (Length : %d)\n"), szBarcode, dwLength);
	
	int iref = szBarcode.Find(m_stInspInfo.szBarcodeRef);

	if (-1 < iref)
	{
		if (m_wnd_MainView.IsWindowVisible() == TRUE || m_wnd_RecipeView.IsWindowVisible() == TRUE)
		{
			if (!IsTesting_All() && m_stInspInfo.LotInfo.bLotStatus == FALSE)
			{
				// Model Change...
				CString szModelName;

				ST_BarcodeRefInfo* const pBarcodeInfo = &m_stInspInfo.stBarcodeInfo;
				INT_PTR iCnt = pBarcodeInfo->GetCount();

				for (INT nIdx = 0; nIdx < iCnt; nIdx++)
				{
					if (pBarcodeInfo->BarcodeList[nIdx].szrefBarcode == szBarcode)
					{
						szModelName = pBarcodeInfo->BarcodeList[nIdx].szModelName;
						LoadModelInfo(szModelName, FALSE);

						break;
					}
				}
			}
		}
	}
	else
	{
		if (m_stInspInfo.bUseBarcode == TRUE)
		{
			if (m_wnd_MainView.IsWindowVisible() == TRUE)
			{
				if (!IsTesting_All() && m_stInspInfo.LotInfo.bLotStatus == TRUE)
				{
					OnSet_Barcode(szBarcode);
				}
			}
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: OnRecvMainBrd
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/6/21 - 11:05
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnRecvMainBrd(WPARAM wParam, LPARAM lParam)
{
	// Start, Stop Button Recieve..
	LPCTSTR szData = (LPCTSTR)wParam;

	CStringA strData;
	strData.Format("%s", szData);
	
// 	if (strData.Left(4) == "!B10")
// 	{
// 		if (m_DlgVideoManual.IsWindowVisible() == TRUE)
// 		{
// 			m_DlgVideoManual.m_bStartBtn = TRUE;
// 		} 
// 		else
// 		{
// 		SendMessage(WM_TEST_START, 0, 0);
// 		}
// 	}
// 
// 	if (strData.Left(4) == "!B01")
// 	{
// 		if (m_DlgVideoManual.IsWindowVisible() == TRUE)
// 		{
// 			m_DlgVideoManual.m_bStopBtn = TRUE;
// 		}
// 		else
// 		{
// 		SendMessage(WM_TEST_STOP, 0, 0);
// 		}
// 	}

	return 0;
}

//=============================================================================
// Method		: OnPogoCnt_Increase
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 18:04
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnPogoCnt_Increase(WPARAM wParam, LPARAM lParam)
{
	OnSetStatus_PogoCount();
	return TRUE;
}

//=============================================================================
// Method		: OnPogoCnt_Update
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/12/28 - 18:04
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnPogoCnt_Update(WPARAM wParam, LPARAM lParam)
{
 	m_wnd_MainView.UpdatePogoCnt();
	return TRUE;
}

//=============================================================================
// Method		: OnModelSave
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/2/28 - 16:07
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnModelSave(WPARAM wParam, LPARAM lParam)
{
	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), MODEL_FILE_EXT, MODEL_FILE_EXT);

	if (m_stInspInfo.ModelInfo.szModelFile.IsEmpty())
	{
		CFileDialog fileDlg(FALSE, MODEL_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
		fileDlg.m_ofn.lpstrInitialDir = m_stInspInfo.Path.szModel;

		if (fileDlg.DoModal() == IDOK)
		{
			strFullPath = fileDlg.GetPathName();
			strFileTitle = fileDlg.GetFileTitle();

			// 저장	 		
			if (m_fileModel.SaveModelFile(strFullPath, &m_stInspInfo.ModelInfo))
			{
				// 리스트 모델 갱신
			}

			LoadModelInfo(m_stInspInfo.Path.szModel, 0);

			m_wnd_RecipeView.SetModel(m_stInspInfo.ModelInfo.szModelFile);
			m_stInspInfo.ModelInfo.szModelFile.ReleaseBuffer();
		}
	}
	else
	{
		strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szModel, m_stInspInfo.ModelInfo.szModelFile, MODEL_FILE_EXT);

		// ---------------------------------------------현재 광축 저장.
// 		LPBYTE pFrameImage = NULL;
// 		DWORD dwWidth = 0;
// 		DWORD dwHeight = 0;
// 		UINT nChannel = 0;
// 
// 		for (int i = 0; i < 30; i++)
// 		{
// 			pFrameImage = GetImageBuffer(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, dwWidth, dwHeight, nChannel);
// 
// 			Sleep(10);
// 			if (NULL != pFrameImage)
// 			{
// 				int iCheckImage = 0;
// 
// 				for (int y = 0; y < dwHeight; y++)
// 				{
// 					for (int x = 0; x < dwWidth; x++)
// 					{
// 						BYTE byCheckR = pFrameImage[y * dwWidth * nChannel + x * nChannel + 0];
// 						BYTE byCheckG = pFrameImage[y * dwWidth * nChannel + x * nChannel + 1];
// 						BYTE byCheckB = pFrameImage[y * dwWidth * nChannel + x * nChannel + 2];
// 
// 						if (byCheckR == 0x00 && byCheckG == 0x00 && byCheckB == 0x00)
// 						{
// 							iCheckImage++;
// 						}
// 					}
// 				}
// 
// 				if (80 <= ((double)iCheckImage / (double)(dwWidth * dwHeight)) * 100.0)
// 				{
// 				}
// 				else
// 				{
// 					break;
// 				}
// 			}
// 		}
// 
// 		IplImage* pImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);
// 
// 		for (int y = 0; y < dwHeight; y++)
// 		{
// 			for (int x = 0; x < dwWidth; x++)
// 			{
// 				pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 0];
// 				pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 1];
// 				pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 2];
// 			}
// 
// 		}
// 
// 		for (int i = 0; i < TICnt_SFR; i++)
// 			m_ImageTest.OnTestProcessSFRROI_Auto_Center(FALSE, pImage, &m_stInspInfo.ModelInfo.stSFR[i].stSFROpt);
// 
// 		cvReleaseImage(&pImage);

		// ---------------------------------------------현재 광축 저장.


		// 저장	 	
		m_fileModel.SaveModelFile(strFullPath, &m_stInspInfo.ModelInfo);

		// 모델 데이터 불러오기
		LoadModelInfo(m_stInspInfo.ModelInfo.szModelFile, 0);

		m_wnd_RecipeView.SetModel(m_stInspInfo.ModelInfo.szModelFile);
		m_stInspInfo.ModelInfo.szModelFile.ReleaseBuffer();
	}

	return 0;
}

//=============================================================================
// Method		: OnChangeMotor
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/8/12 - 18:27
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnChangeMotor(WPARAM wParam, LPARAM lParam)
{
	CString strMotor = (LPCTSTR)wParam;

	ShowSplashScreen();
	m_wndSplash.SetText(_T("Changing Motor..."));

	m_stInspInfo.ModelInfo.szMotorFile = strMotor;

	m_Device.MotionManager.SetPrtMotorPath(&m_stInspInfo.Path.szMotor, strMotor);
	if (m_Device.MotionManager.LoadMotionInfo())
	{
		m_regInspInfo.SaveSelectedMotor(m_stInspInfo.ModelInfo.szMotorFile);
		m_wnd_MotorView.SetPath(m_stInspInfo.Path.szMotor);
	}

	m_wnd_MotorView.UpdateMotorInfo(m_stInspInfo.ModelInfo.szMotorFile);

	ShowSplashScreen(FALSE);

	return 0;
}

//LRESULT CView_MainCtrl::OnChangeMaintenance(WPARAM wParam, LPARAM lParam)
//{
//	if (IsTesting())
//	{
//		AfxMessageBox(_T("Inspection is in progress.\r\nTry it after the Test is finished."), MB_SYSTEMMODAL);
//		return FALSE;
//	}
//
//	//DEBUG_ONLY();
//
//	// 모델 파일에서 모델 정보 불러오기
//	CString strMaintenance = (LPCTSTR)wParam;
//	BOOL bNotifyModelView = (BOOL)lParam;
//
//	LoadMaintenanceInfo(strMaintenance, bNotifyModelView);
//
//	return 0;
//}


LRESULT CView_MainCtrl::OnRecvDIOMon(WPARAM wParam, LPARAM lParam)
{
	UINT	nReadType = (UINT)wParam;
	DWORD64	dwReadData = (DWORD)lParam;

	if (CDigitalIOCtrl::ReadIdx_DI == nReadType)
	{
		if (dwReadData != m_stInspInfo.dwDI)
		{
			m_stInspInfo.dwDI = dwReadData;
			BOOL bOnOff = FALSE;
			for (UINT nOffset = 0; nOffset < MAX_DIGITAL_IO; nOffset++)
			{
				bOnOff = dwReadData >> nOffset & 0x0000000000000001;

				if (bOnOff != m_stInspInfo.byDIO_DI[nOffset])
				{
					m_stInspInfo.byDIO_DI[nOffset] = bOnOff;

					OnDetectDigitalInSignal(nOffset, bOnOff);
				}
			}

			// UI 갱신
			m_wnd_DeviceView.Set_IO_DI_Data(m_stInspInfo.byDIO_DI, MAX_DIGITAL_IO); 

			m_wnd_RecipeView.m_wnd_ManualMotion.SetParticle_Cylinder_IN(m_stInspInfo.byDIO_DI[DI_SensorParticleIn]);
			m_wnd_RecipeView.m_wnd_ManualMotion.SetParticle_Cylinder_OUT(m_stInspInfo.byDIO_DI[DI_SensorParticleOut]);

			m_wnd_MotorView.SetParticle_Cylinder_IN(m_stInspInfo.byDIO_DI[DI_SensorParticleIn]);
			m_wnd_MotorView.SetParticle_Cylinder_OUT(m_stInspInfo.byDIO_DI[DI_SensorParticleOut]);


		}
	}
	else if (CDigitalIOCtrl::ReadIdx_DO == nReadType)
	{
		if (dwReadData != m_stInspInfo.dwDO)
		{
			m_stInspInfo.dwDO = dwReadData;
			BOOL bOnOff = FALSE;
			for (UINT nOffset = 0; nOffset < MAX_DIGITAL_IO; nOffset++)
			{
				bOnOff = dwReadData >> nOffset & 0x0000000000000001;

				if (bOnOff != m_stInspInfo.byDIO_DO[nOffset])
				{
					m_stInspInfo.byDIO_DO[nOffset] = bOnOff;
				}
			}

			// UI 갱신
			m_wnd_DeviceView.Set_IO_DO_Data(m_stInspInfo.byDIO_DO, MAX_DIGITAL_IO);
		}
	}

	return 1;
}

LRESULT CView_MainCtrl::OnRecvDIOFirstRead(WPARAM wParam, LPARAM lParam)
{
	UINT	nReadType = (UINT)wParam;
	DWORD	dwReadData = (DWORD)lParam;

	if (CDigitalIOCtrl::ReadIdx_DI == nReadType)
	{
		m_stInspInfo.dwDI = dwReadData;

		for (UINT nOffset = 0; nOffset < MAX_DIGITAL_IO; nOffset++)
		{
			m_stInspInfo.byDIO_DI[nOffset] = dwReadData >> nOffset & 0x00000001;
		}

		// UI 갱신
		m_wnd_DeviceView.Set_IO_DI_Data((LPBYTE)m_stInspInfo.byDIO_DI, MAX_DIGITAL_IO);
		m_wnd_RecipeView.m_wnd_ManualMotion.SetParticle_Cylinder_IN(m_stInspInfo.byDIO_DI[DI_SensorParticleIn]);
		m_wnd_RecipeView.m_wnd_ManualMotion.SetParticle_Cylinder_OUT(m_stInspInfo.byDIO_DI[DI_SensorParticleOut]);

		m_wnd_MotorView.SetParticle_Cylinder_IN(m_stInspInfo.byDIO_DI[DI_SensorParticleIn]);
		m_wnd_MotorView.SetParticle_Cylinder_OUT(m_stInspInfo.byDIO_DI[DI_SensorParticleOut]);
	}
	else if (CDigitalIOCtrl::ReadIdx_DO == nReadType)
	{
		m_stInspInfo.dwDO = dwReadData;

		for (UINT nOffset = 0; nOffset < MAX_DIGITAL_IO; nOffset++)
		{
			m_stInspInfo.byDIO_DO[nOffset] = dwReadData >> nOffset & 0x00000001;
		}

		// UI 갱신
		m_wnd_DeviceView.Set_IO_DO_Data((LPBYTE)m_stInspInfo.byDIO_DO, MAX_DIGITAL_IO);
	}

	return 1;
}

//=============================================================================
// Method		: OnManualTestCmd
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2018/1/14 - 16:30
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnManualTestCmd(WPARAM wParam, LPARAM lParam)
{
	LRESULT	lReturn = RCA_OK;

	enLT_TestItem_ID enTestID = (enLT_TestItem_ID)lParam;
	UINT nTestCount = (UINT)wParam;

	return lReturn;
}

LRESULT CView_MainCtrl::OnManualTestImageSaveCmd(WPARAM wParam, LPARAM lParam)
{
	enLT_TestItem_ID enTestID = (enLT_TestItem_ID)lParam;
	IplImage * TestImage = (IplImage *)wParam;

	ManualTestImageSave(enTestID, TestImage);
	return TRUE;
}

LRESULT CView_MainCtrl::OnMotorOriginAll(WPARAM wParam, LPARAM lParam)
{
	LRESULT	lReturn = RCA_OK;
	
	//if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_SensorParticleIn) == TRUE && m_Device.MotionManager.GetCurrentPos(AX_StageDistance) > m_Device.MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End])
	//{
	//	//이물이 In 상태이고 이물 검사가 끝난 상태 일때
	//	lReturn = m_Device.MotionSequence.OnAction_Par_Load();
	//	lReturn = m_Device.MotionSequence.OnAction_Par_Cylinder_Out();
	//}
	//else if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_SensorParticleIn) == TRUE && m_Device.MotionManager.GetCurrentPos(AX_StageDistance) < m_Device.MotionSequence.m_pstTeachInfo->dbTeachData[TC_ImageTest_Crash_End])
	//{
	//	//이물이 In 상태이지만, 차트 검사 위치에 있을 떄
	//}

	lReturn = m_Device.MotionSequence.OnAction_Par_Cylinder_Out(TRUE);
	if (lReturn != RCA_OK)
	{
		return lReturn;
	}

	lReturn = SetMotorOrigin_All();
	if (lReturn != RCA_OK)
	{
		return lReturn;
	}

	lReturn = m_Device.MotionSequence.OnAction_Load();
	if (lReturn != RCA_OK)
	{
		return lReturn;
	}

	return lReturn;

}

LRESULT CView_MainCtrl::OnMasterTest(WPARAM wParam, LPARAM lParam)
{
	LRESULT	lReturn = RCA_OK;

	m_DlgMasterTest.SetOwner(this);
	m_DlgMasterTest.SetPtr_Device(&m_Device);
	m_DlgMasterTest.SetPtr_Modelinfo(&m_stInspInfo.ModelInfo);
	m_DlgMasterTest.SetVideoChannel(VideoView_Ch_1);

	if (m_stInspInfo.PermissionMode == Permission_Administrator || m_stInspInfo.PermissionMode == Permission_Engineer || m_stInspInfo.PermissionMode == Permission_CNC)
	{
		m_DlgMasterTest.SetMode(0);
		m_DlgMasterTest.SetPermisionMode(m_stInspInfo.PermissionMode);

		m_DlgMasterTest.DoModal();
		m_stInspInfo.m_bMasterCheck = m_DlgMasterTest.GetResult();
	}
	else
	{
		AfxMessageBox(_T("Start Good Master Inspection."));

		// 양품
		BOOL bOK_Master = FALSE;
		
		m_DlgMasterTest.SetMode(0);
		m_DlgMasterTest.SetPermisionMode(m_stInspInfo.PermissionMode);

		m_DlgMasterTest.DoModal();
		bOK_Master = m_DlgMasterTest.GetResult();

		AfxMessageBox(_T("Start Fail Master Inspection."));

		// 불량
		BOOL bNG_Master = FALSE;
		m_DlgMasterTest.SetMode(1);

		m_DlgMasterTest.SetPermisionMode(m_stInspInfo.PermissionMode);

		m_DlgMasterTest.DoModal();
		bNG_Master = m_DlgMasterTest.GetResult();


		m_stInspInfo.m_bMasterCheck = bOK_Master & bNG_Master;
	}

	return lReturn;
}
//=============================================================================
// Method		: OnMasterSet
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/2/27 - 19:40
// Desc.		:
//=============================================================================
LRESULT CView_MainCtrl::OnMasterSet(WPARAM wParam, LPARAM lParam)
{
	SetAutoMasterMode();
	return 0;
}

LRESULT CView_MainCtrl::OnMasterMode(WPARAM wParam, LPARAM lParam)
{
	LRESULT	lReturn = RCA_OK;

	m_DlgMasterMode.SetOwner(this);
	m_DlgMasterMode.SetPtr_Device(&m_Device);
	m_DlgMasterMode.SetPtr_Modelinfo(&m_stInspInfo.ModelInfo);
	m_DlgMasterMode.SetVideoChannel(VideoView_Ch_1);

	if (m_stInspInfo.PermissionMode == Permission_Administrator || m_stInspInfo.PermissionMode == Permission_Engineer || m_stInspInfo.PermissionMode == Permission_CNC)
	{
		m_DlgMasterMode.SetMode(0);
		m_DlgMasterMode.SetPermisionMode(m_stInspInfo.PermissionMode);

		m_DlgMasterMode.DoModal();
		m_stInspInfo.m_bMasterCheck = m_DlgMasterMode.GetResult();
	}
	else
	{
		AfxMessageBox(_T("Start Good Master Inspection."));

		// 양품
		BOOL bOK_Master = FALSE;

		m_DlgMasterMode.SetMode(0);
		m_DlgMasterMode.SetPermisionMode(m_stInspInfo.PermissionMode);

		m_DlgMasterMode.DoModal();
		bOK_Master = m_DlgMasterMode.GetResult();

		AfxMessageBox(_T("Start Fail Master Inspection."));

		// 불량
		BOOL bNG_Master = FALSE;
		m_DlgMasterMode.SetMode(1);

		m_DlgMasterMode.SetPermisionMode(m_stInspInfo.PermissionMode);

		m_DlgMasterMode.DoModal();
		bNG_Master = m_DlgMasterMode.GetResult();


		m_stInspInfo.m_bMasterCheck = bOK_Master & bNG_Master;
	}

	return lReturn;
}
//=============================================================================
// Method		: OnInitLogFolder
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/19 - 15:04
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnInitLogFolder()
{
#ifdef USE_EVMS_MODE

	m_logFile.SetPath(EVMS_PATH_LOG_OP, _T("Inspector"));
	m_Log_ErrLog.SetPath(EVMS_PATH_LOG_OP, _T("Error"));
	m_Log_ErrLog.SetLogFileName_Prefix(_T("Err"));

#else

	// 로그 처리
	if (!m_stInspInfo.Path.szLog.IsEmpty())
		m_logFile.SetPath(m_stInspInfo.Path.szLog, _T("Inspector"));

	if (!m_stInspInfo.Path.szLog.IsEmpty())
		m_Log_ErrLog.SetPath(m_stInspInfo.Path.szLog, _T("Error"));

	m_Log_ErrLog.SetLogFileName_Prefix(_T("Err"));

#endif
}

//=============================================================================
// Method		: CView_MainCtrl::InitConstructionSetting
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/12/13 - 15:13
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitConstructionSetting()
{
	// 프로그램 폴더 구하기
	TCHAR szExePath[MAX_PATH] = {0};	
	GetModuleFileName(NULL, szExePath, MAX_PATH);

	TCHAR drive[_MAX_DRIVE];
	TCHAR dir[_MAX_DIR];	
	TCHAR file[_MAX_FNAME];
	TCHAR ext[_MAX_EXT];
	_tsplitpath_s (szExePath, drive, _MAX_DRIVE, dir, _MAX_DIR, file, _MAX_FNAME, ext, _MAX_EXT);	
	
	m_stInspInfo.Path.szProgram.		Format(_T("%s%s"), drive, dir);
	m_stInspInfo.Path.szMes.Format(_T("%s%sMES\\Receive\\"), drive, dir);
	m_stInspInfo.Path.szMes2.Format(_T("%s%sMES\\Send\\"), drive, dir);
	m_stInspInfo.Path.szLog	.			Format(_T("%s%sLog\\"), drive, dir);
	m_stInspInfo.Path.szReport.			Format(_T("%s%sReport\\"), drive, dir);
	m_stInspInfo.Path.szModel.			Format(_T("%s%sModel\\"), drive, dir);
	m_stInspInfo.Path.szImage.			Format(_T("%s%sImage\\"), drive, dir);
	m_stInspInfo.Path.szPogo.			Format(_T("%s%sPogo\\"), drive, dir);
 	m_stInspInfo.Path.szI2C.			Format(_T("%s%sI2C\\"), drive, dir);
 	//m_stInspInfo.Path.szWav.			Format(_T("%s%sWav\\"), drive, dir);
	m_stInspInfo.Path.szMotor.			Format(_T("%s%sMotor\\"), drive, dir);
	//m_stInspInfo.Path.szMes.			Format(_T("%s%sMes\\"), drive, dir);
//	m_stInspInfo.Path.szBarcodeInfo.	Format(_T("%s%sBarcode\\"), drive, dir);
	m_stInspInfo.Path.szOperatorInfo.	Format(_T("%s%sOperator\\"), drive, dir);
	m_stInspInfo.Path.szPrnFile.		Format(_T("%s%sPrinter\\"), drive, dir);
	//m_stInspInfo.Path.szMaintenance.	Format(_T("%s%sMaintenance\\"), drive, dir);

	OnLoadOption();
	OnInitLogFolder();

	//MakeSubDirectory(m_stInspInfo.Path.szMes);
	MakeSubDirectory(m_stInspInfo.Path.szReport);
	MakeSubDirectory(m_stInspInfo.Path.szModel);
	MakeSubDirectory(m_stInspInfo.Path.szImage);
	MakeSubDirectory(m_stInspInfo.Path.szPogo);
	MakeSubDirectory(m_stInspInfo.Path.szI2C);
	//MakeSubDirectory(m_stInspInfo.Path.szWav);
	MakeSubDirectory(m_stInspInfo.Path.szMotor);
	MakeSubDirectory(m_stInspInfo.Path.szMes);
	MakeSubDirectory(m_stInspInfo.Path.szMes2);
// 	MakeSubDirectory(m_stInspInfo.Path.szBarcodeInfo);
	MakeSubDirectory(m_stInspInfo.Path.szOperatorInfo);
	MakeSubDirectory(m_stInspInfo.Path.szPrnFile);
	//MakeSubDirectory(m_stInspInfo.Path.szMaintenance);

	LoadRefBarcode();
	LoadOperatorInfo();


	// IO Board Setting
	if (g_InspectorTable[SET_INSPECTOR].bPCIIOControl == TRUE)
	{
		m_Device.DigitalIOCtrl.AXTInit();
		m_wnd_DeviceView.SetPtr_Device(&m_Device);
		m_Device.MotionSequence.SetPtrDigitalIOCtrl(&m_Device.DigitalIOCtrl);
	}

 	// Motor Board Setting
	if (g_InspectorTable[SET_INSPECTOR].bPCIMotion == TRUE)
	{
		// Motor Board Setting
		m_Device.MotionManager.SetPrtMotorPath(&m_stInspInfo.Path.szMotor);
		m_Device.MotionManager.SetAllMotorOpen();

		m_wnd_MotorView.SetPtr_Device(&m_Device.MotionManager);
		m_wnd_MotorView.SetPath(m_stInspInfo.Path.szMotor);

		m_Device.MotionSequence.SetPtrMotionManger(&m_Device.MotionManager);
	}

	m_Device.MotionSequence.SetPtrLight(&m_Device.PCBLightBrd[0], &m_Device.PCBLightBrd[1]);
	m_Device.MotionSequence.SetModelInfoData(&m_stInspInfo.ModelInfo);
	
	// Motion & IO 
	m_Device.MotionSequence.SetLTOption(&m_stOption);
	m_wnd_MotorView.SetPtr_MaintenanceInfo(&m_stInspInfo.MaintenanceInfo);
	m_Device.MotionSequence.SetPtr_TeachInfo(&m_stInspInfo.MaintenanceInfo.stTeachInfo);
	m_wnd_MotorView.m_wnd_VideoView.SetModelInfo(&m_stInspInfo.ModelInfo);


	m_wnd_RecipeView.SetPtr_ImageMode(&m_stImageMode);
	m_wnd_RecipeView.SetPtr_Device(&m_Device);
	m_wnd_RecipeView.SetLTOption(&m_stOption);
	m_wnd_RecipeView.SetPtr_PogoCnt(&m_stInspInfo.PogoInfo);
	m_wnd_RecipeView.SetPtr_ImageSaveType(&m_stOption.Inspector.nImageSaveType);

	m_wnd_RecipeView.SetPath(
		m_stInspInfo.Path.szModel, 
		m_stInspInfo.Path.szImage, 
		m_stInspInfo.Path.szPogo, 
		m_stInspInfo.Path.szI2C,
		m_stInspInfo.Path.szWav,
		m_stInspInfo.Path.szBarcodeInfo,
		m_stInspInfo.Path.szOperatorInfo,
		m_stInspInfo.Path.szPrnFile
		);

	m_wnd_RecipeView.SetPtr_BarcodeInfo(&m_stInspInfo.stBarcodeInfo, m_stInspInfo.szBarcodeRef);
	m_wnd_RecipeView.SetPtr_UserConfigInfo(&m_stInspInfo.stUserConfigInfo);

	// Master 버튼 활성 상태
	m_wnd_MainView.SetPtrInspectionInfo(&m_stInspInfo);
}

//=============================================================================
// Method		: CView_MainCtrl::InitUISetting
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2013/1/2 - 16:23
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitUISetting()
{
	CReg_InspInfo regInfo;
	DWORD dwValue = 0;

	m_wnd_WorklistView.GetPtr_Worklist(m_WorklistPtr);
//	m_wnd_MainView.OnSetBtnMasterTestEnable(m_stInspInfo.bUseMasterCheck);
	m_wnd_MainView.OnSetBtnMasterModeEnable(m_stInspInfo.bUseMasterCheck);

}

//=============================================================================
// Method		: CView_MainCtrl::InitDeviceSetting
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2012/12/17 - 17:53
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitDeviceSetting()
{
	InitDevicez(GetSafeHwnd());
}

//=============================================================================
// Method		: InitEVMS_EnvFile
// Access		: protected  
// Returns		: void
// Parameter	: __in LPCTSTR szModelFile
// Qualifier	:
// Last Update	: 2016/12/15 - 12:28
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitEVMS_EnvFile(__in LPCTSTR szModelFile)
{
#ifdef USE_EVMS_MODE
	// 기본 모델파일, 기본 모터 파일   
	// 복사

	//szModelFile;
	CString szEnvFullPath;
	CString szDefFullPath;
	// OP/ENV/
	szEnvFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szModel, szModelFile, MODEL_FILE_EXT);
	// OP/EXE/../ENV/
	szDefFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szDefaultEnv, szModelFile, MODEL_FILE_EXT);

	// 모델 파일이 없으면
	if (!PathFileExists(szEnvFullPath))
	{
		// 복사
		if (PathFileExists(szDefFullPath))
		{
			::CopyFile(szDefFullPath, szEnvFullPath, TRUE);
		}
		else
		{
			// Default.luri 사용??
			// 오류 처리 ??
		}
	}
	else // 파일이 있을 경우
	{
		// 최신 날짜 파일 사용?
		if (PathFileExists(szDefFullPath))
		{
			FILETIME tmEnv, tmDef;
			GetLastWriteTime(szEnvFullPath, tmEnv);
			GetLastWriteTime(szDefFullPath, tmDef);

			if (0 < CompareFileTime(&tmEnv, &tmDef))
			{
				::CopyFile(szDefFullPath, szEnvFullPath, TRUE);
			}
		}
	}

#endif
}

//=============================================================================
// Method		: InitEVMS_EnvFile_All
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/15 - 10:19
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitEVMS_EnvFile_All()
{
	CFileFind finder;
	CString szModelFile;
	CString strWildcard;

	// OP/EXE/../ENV/
	strWildcard.Format(_T("%s*.%s"), m_stInspInfo.Path.szDefaultEnv, MODEL_FILE_EXT);

	BOOL bWorking = finder.FindFile(strWildcard);

	while (bWorking)
	{
		bWorking = finder.FindNextFile();

		if (finder.IsDots())
			continue;

		if (finder.IsDirectory())
			continue;

		szModelFile = finder.GetFileTitle();

		InitEVMS_EnvFile(szModelFile);
	}

	finder.Close();

}

//=============================================================================
// Method		: OnSetStatus_GrabberBrd_ComArt
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/12/28 - 16:02
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_GrabberBrd_ComArt(__in BOOL bConnect)
{
	__super::OnSetStatus_GrabberBrd_ComArt(bConnect);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_GrabberBrd_ComArt((UINT)bConnect);
}

//=============================================================================
// Method		: OnSetStatus_BCR
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nConnect
// Qualifier	:
// Last Update	: 2017/8/12 - 16:52
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_BCR(__in UINT nConnect)
{
	__super::OnSetStatus_BCR(nConnect);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_BCR(nConnect);
}

void CView_MainCtrl::OnSetStatus_LabelPrinter(__in UINT nConnect)
{
	__super::OnSetStatus_LabelPrinter(nConnect);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_LabelPrinter(nConnect);
}

//=============================================================================
// Method		: OnSetStatus_PCBCamBrd
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nConnect
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2017/8/12 - 16:52
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_PCBCamBrd(__in UINT nConnect, __in UINT nIdxBrd)
{
	__super::OnSetStatus_PCBCamBrd(nConnect, nIdxBrd);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_CameraBoard(nIdxBrd, nConnect);
}

//=============================================================================
// Method		: OnSetStatus_PCBLightBrd
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nConnect
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2017/8/12 - 16:52
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_PCBLightBrd(__in UINT nConnect, __in UINT nIdxBrd)
{
	__super::OnSetStatus_PCBLightBrd(nConnect, nIdxBrd);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_LightBoard(nIdxBrd, nConnect);
}

//=============================================================================
// Method		: OnSetStatus_IOBoard
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2017/8/12 - 16:53
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_IOBoard(__in BOOL bConnect)
{
	__super::OnSetStatus_IOBoard(bConnect);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_IO(bConnect);
}

//=============================================================================
// Method		: OnSetStatus_MotorBoard
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2017/8/12 - 16:53
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_MotorBoard(__in BOOL bConnect)
{
	__super::OnSetStatus_MotorBoard(bConnect);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_Motor(bConnect);
}

//=============================================================================
// Method		: OnSetStatus_Motor
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bConnect
// Parameter	: __in UINT nChIdx
// Qualifier	:
// Last Update	: 2017/1/2 - 16:58
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_Indicator(__in BOOL bConnect, __in UINT nChIdx /*= 0*/)
{
	__super::OnSetStatus_Indicator(bConnect, nChIdx);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_Indicator(bConnect, nChIdx);
}

//=============================================================================
// Method		: OnSetStatus_Signal_CA
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nIndex
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2016/12/29 - 15:43
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_Signal_CA(__in UINT nIndex, __in BOOL bOn)
{
	if (FALSE == bOn)
	{
		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.SetEmptyImageBuffer();
		m_wnd_RecipeView.m_wnd_VideoView.SetEmptyImageBuffer();
		m_wnd_MotorView.m_wnd_VideoView.SetEmptyImageBuffer();
	}
}

//=============================================================================
// Method		: OnSetStatus_MES
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nConnect
// Qualifier	:
// Last Update	: 2017/8/12 - 16:55
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_MES(__in UINT nConnect)
{
	__super::OnSetStatus_MES(nConnect);

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_MES(nConnect);
}

//=============================================================================
// Method		: OnSetGrabberBrd_ComArt_Reboot
// Access		: protected  
// Returns		: void
// Parameter	: __in DWORD dwType
// Qualifier	:
// Last Update	: 2018/1/14 - 10:32
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetGrabberBrd_ComArt_Reboot(__in DWORD dwType /*= VideoState_NTSC*/)
{
	OnSetStatus_GrabberBrd_ComArt(FALSE);

	// 카메라 영상 중지
	m_Device.Cat3DCtrl.Stop_CaptureAll();

	m_Device.Cat3DCtrl.Final_End();
	m_Device.Cat3DCtrl.Close_Board(); // NTSC

	if (m_Device.Cat3DCtrl.Search_Cat3d())
	{
		if (m_Device.Cat3DCtrl.Open_Board(dwType)) // NTSC
		{
			m_Device.Cat3DCtrl.Init_Begin();

			OnSetStatus_GrabberBrd_ComArt(TRUE);

			// 밝기, 채도 설정
			m_Device.Cat3DCtrl.SetBrightnessContrast(110, 110);

			// 기본 프레임 30으로 설정 (XCap400E 모델)
			m_Device.Cat3DCtrl.SetFPS();

			// 영상 시작
			m_Device.Cat3DCtrl.Start_CaptureAll();
		}
		else
		{
			OnLog_Err(_T("Analog Grabber Board : Open Fail"));
		}
	}
	else
	{
		OnLog_Err(_T("Analog Grabber Board : PCI Board Recognition Fail in Device"));
	}
}

//=============================================================================
// Method		: OnSet_Barcode
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2016/10/20 - 21:20
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSet_Barcode(__in LPCTSTR szBarcode)
{
	__super::OnSet_Barcode(szBarcode);

	 	CString szNotUse = _T("NotUse");
	 
	 	if (NULL != m_pdlgBarcode)
	 	{
	 		m_pdlgBarcode->ShowWindow(SW_SHOW);
	 
	 		if (m_pdlgBarcode->InsertBarcode(szBarcode))
	 		{
	 			m_pdlgBarcode->GetBarcode(m_stInspInfo.szBarcodeBuf);
	 		}
	 	}
		OnSetInsertBarcode(m_stInspInfo.szBarcodeBuf);
		OnUpdateTResetResult();
		OnUpdateTestJudgment((enTestEachResult)TR_Init);
		((CPane_CommStatus*)m_pwndCommPane)->Set_Barcode(szBarcode);
}

//=============================================================================
// Method		: OnAddErrorInfo
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enErrorCode lErrorCode
// Qualifier	:
// Last Update	: 2016/10/31 - 1:04
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnAddErrorInfo(__in enEquipErrorCode lErrorCode)
{
	ST_ErrorInfo stErrInfo;

	stErrInfo.lCode = lErrorCode;
	stErrInfo.nType = 0;
	GetLocalTime(&stErrInfo.tmTime);
	stErrInfo.szDesc = g_szEquipErrorCode[lErrorCode];

	//m_wndErrorView.InsertErrorInfo(&stErrInfo);

	__super::OnAddErrorInfo(lErrorCode);
}

void CView_MainCtrl::OnDIO_UpdateDInSignal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	m_wnd_DeviceView.Set_IO_DI_OffsetData(byBitOffset, bOnOff);
}

void CView_MainCtrl::OnDIO_UpdateDOutSignal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	m_wnd_DeviceView.Set_IO_DO_OffsetData(byBitOffset, bOnOff);
}

//=============================================================================
// Method		: OnUpdateElapsedTime_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in DWORD dwTime
// Qualifier	:
// Last Update	: 2018/1/14 - 14:24
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnUpdateElapsedTime_All(__in DWORD dwTime)
{
	m_wnd_MainView.UpdateElapsedTime(dwTime);
}

//=============================================================================
// Method		: OnUpdateTestResult
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in UINT nStepIdx
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2018/1/14 - 14:44
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnUpdateTestResult(__in enTestEachResult EachResult, __in UINT nStepIdx, __in CString strText)
{
	//for (int i = 0; i < 10; i++)
	{
		m_wnd_MainView.UpdateTestResult(EachResult, nStepIdx, strText);
		//Sleep(10);
	}
}

//=============================================================================
// Method		: OnUpdateTestJudgment
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Qualifier	:
// Last Update	: 2018/1/14 - 15:27
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnUpdateTestJudgment(__in enTestEachResult EachResult)
{
	m_wnd_MainView.UpdateTestJudgment(EachResult);
	OnUpdateAlarmMessage(g_szAlarmMessage_All[EachResult]);
}

//=============================================================================
// Method		: OnUpdateTResetResult
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/14 - 15:49
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnUpdateTResetResult()
{
	m_wnd_MainView.UpdateResetResult();
}

void CView_MainCtrl::OnUpdateAlarmMessage(__in CString szAlarmMsg)
{
	m_wnd_MainView.UpdateAlarmMessage(szAlarmMsg);
}

//=============================================================================
// Method		: OnTestFailResultCode_Unit
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in enResultCode_TestItem nFailCode
// Qualifier	:
// Last Update	: 2017/7/7 - 9:57
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnTestFailResultCode_Unit(__in UINT nUnitIdx, __in enResultCode_TestItem nFailCode)
{
	__super::OnTestFailResultCode_Unit(nUnitIdx, nFailCode);
}

//=============================================================================
// Method		: OnSetInputTime
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/7 - 9:57
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetInputTime()
{
	__super::OnSetInputTime();
}

//=============================================================================
// Method		: OnSetResetSiteInfo
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/18 - 15:29
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetResetSiteInfo()
{
}

//=============================================================================
// Method		: OnSetInsertBarcode
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2017/2/21 - 16:58
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetInsertBarcode(__in LPCTSTR szBarcode)
{
	m_wnd_MainView.InsertBarcode(szBarcode);
}

//=============================================================================
// Method		: OnSetTestInitialize
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/7/7 - 9:57
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetTestInitialize(__in enTestEachResult EachResult, __in CString strText)
{
	__super::OnSetTestInitialize(EachResult, strText);
	//m_wnd_MainView.UpdateTestInitialize(EachResult, strText);
}

//=============================================================================
// Method		: OnSetTestFinalize
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/7/7 - 9:57
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetTestFinalize(__in enTestEachResult EachResult, __in CString strText)
{
	__super::OnSetTestFinalize(EachResult, strText);
}

//=============================================================================
// Method		: OnBarcode_Input
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/25 - 14:01
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnBarcode_Input()
{	
	//m_Site[Site_A].OnSetBarcode(m_stInspInfo.CamObject[Site_A]->szBarcode);
}


//=============================================================================
// Method		: OnSetLotInfo
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/2 - 18:55
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetLotInfo()
{
	m_wnd_MainView.UpdateLotInfo();
}

void CView_MainCtrl::OnSetMESInfo()
{
	m_wnd_MainView.UpdateMESInfo();
}

void CView_MainCtrl::OnSetLotStartBtnStatus(__in BOOL bStatus)
{
	m_wnd_MainView.OnSetBtnLotStartEnable(bStatus);
}

void CView_MainCtrl::OnSetLotStopBtnStatus(__in BOOL bStatus)
{
	m_wnd_MainView.OnSetBtnLotStopEnable(bStatus);
}

void CView_MainCtrl::OnSetStartBtnStatus(__in BOOL bStatus)
{
	m_wnd_MainView.OnSetBtnStartEnable(bStatus);
}

void CView_MainCtrl::OnSetStopBtnStatus(__in BOOL bStatus)
{
	m_wnd_MainView.OnSetBtnStopEnable(bStatus);
}

void CView_MainCtrl::OnTestMasterStartUpEvent(__in BOOL bStatus)
{
	//if (bStatus == TRUE) // 양품 마스터
	//{
	//	if (AfxMessageBox(_T("양품 마스터 체크를 시작합니다. 양품 마스터를 안착해주세요."), MB_YESNO) == IDNO)
	//	{
	//		CDlg_ChkPassword dlgPassword(this);
	//		dlgPassword.DoModal();

	//		m_stInspInfo.bStartUpCheckPass = TRUE;
	//		m_stInspInfo.bStartUpCheckFail = TRUE;

	//		OnSetLotStartBtnStatus(TRUE);
	//		OnSetLotStopBtnStatus(TRUE);
	//		OnSetStartBtnStatus(FALSE);
	//		OnSetStopBtnStatus(FALSE);

	//		return;
	//	}
	//} 
	//else // 불량 마스터
	//{
	//	if (AfxMessageBox(_T("불량 마스터 체크를 시작합니다. 불량 마스터를 안착해주세요."), MB_YESNO) == IDNO)
	//	{
	//		CDlg_ChkPassword dlgPassword(this);
	//		dlgPassword.DoModal();

	//		m_stInspInfo.bStartUpCheckFail = TRUE;
	//	}
	//}

	//OnSetLotStartBtnStatus(FALSE);
	//OnSetLotStopBtnStatus(FALSE);
	//OnSetStartBtnStatus(TRUE);
	//OnSetStopBtnStatus(TRUE);
	
}

BOOL CView_MainCtrl::OnGetStartBtnStatus()
{
	if (m_wnd_MainView.IsWindowVisible() == TRUE)
	{
		return m_wnd_MainView.OnGetBtnStartEnable();
	}
	else
	{
		return FALSE;
	}
}

BOOL CView_MainCtrl::OnGetStopBtnStatus()
{
	if (m_wnd_MainView.IsWindowVisible() == TRUE)
	{
		return m_wnd_MainView.OnGetBtnStopEnable();
	}
	else
	{
		return FALSE;
	}
}

//=============================================================================
// Method		: OnSetIndicatorData
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in float fAxisX
// Parameter	: __in float fAxisY
// Qualifier	:
// Last Update	: 2017/1/12 - 13:52
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetIndicatorData(__in float fAxisX, __in float fAxisY)
{
	((CPane_CommStatus*)m_pwndCommPane)->SetIndicatorDisplay(fAxisX, fAxisY);
}

//=============================================================================
// Method		: OnHidePopupUI
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/22 - 23:03
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnHidePopupUI()
{
	__super::OnHidePopupUI();
}

//=============================================================================
// Method		: OnUpdateSiteCamInfo
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/24 - 19:40
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnUpdateSiteCamInfo()
{
	//__super::OnUpdateSiteCamInfo();
}

//=============================================================================
// Method		: OnResetCamInfo
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/7/15 - 14:34
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnResetCamInfo()
{
	__super::OnResetCamInfo();
	//m_wndMainView.ResetCamInfo();
}

//=============================================================================
// Method		: OnInsertWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/7 - 17:13
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnInsertWorklist()
{
	
}

//=============================================================================
// Method		: OnSaveWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/7 - 17:14
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSaveWorklist()
{
	

	// 파일 저장
	//m_FileReport.Save_Worklist(m_stInspInfo.Path.szLOT, &m_stInspInfo.Worklist_Array);
	
	// 수율 업데이트

	// UI 표시	
	
	//m_wndDeviceView.InsertWorklist(&m_stInspInfo.Worklist_Array);
}

//=============================================================================
// Method		: OnLoadWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/11 - 15:57
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnLoadWorklist()
{
	SYSTEMTIME tmLocal;
	GetLocalTime(&tmLocal);

	// 패스 (Report/2016/06/11)
// 	CString szPath;
// 	szPath.Format(_T("%s%04d\\%02d\\%02d\\"), m_stInspInfo.Path.szReport, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
// 	MakeDirectory(szPath);

 	//if (NULL != m_WorklistPtr.pList_Total)
 	//{
 	//	m_WorklistPtr.pList_Total->DeleteAllItems();
 	//	m_FileReport.Save_Worklist(LoadFile_List_TotalTest(szPath, *m_WorklistPtr.pList_Total);
 	//}
}

//=============================================================================
// Method		: OnUpdateYield
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/28 - 10:25
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnUpdateYield()
{
	m_wnd_MainView.UpdateYield();
}

//=============================================================================
// Method		: OnSetStatus_PogoCount
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/20 - 16:32
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetStatus_PogoCount()
{
	m_stInspInfo.PogoInfo.IncreasePogoCount(0);
	SavePogoCount();
	m_wnd_MainView.UpdatePogoCnt();
}

//=============================================================================
// Method		: OnChangeLotInfo
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in enModelStatus bLotStatus
// Qualifier	:
// Last Update	: 2017/1/4 - 13:27
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::OnChangeLotInfo(__in enModelStatus LotStatus)
{
	if (MESMode == LotStatus)
	{
		// MES & LOT Start
		m_stInspInfo.SetLotStatus(TRUE);

		if (m_stInspInfo.ModelInfo.szModelCode.IsEmpty())
		{
			m_stInspInfo.szModelName = m_stInspInfo.ModelInfo.szModelFile;
		}
		else
		{
			m_stInspInfo.szModelName = m_stInspInfo.ModelInfo.szModelCode;
		}

		if (0 != m_stInspInfo.szLotName.Compare(m_stInspInfo.MESInfo.stMESResult.szLotNo))
		{
			// LOT 변경시 수율, Cycle Time 초기화 및 UI 업데이트
			OnResetYieldCycleTime();
		}

		if ((0 != m_stInspInfo.szLotName.Compare(m_stInspInfo.MESInfo.stMESResult.szLotNo)) 
			|| (0 != m_stInspInfo.szOperatorName.Compare(m_stInspInfo.MESInfo.stMESResult.szOperator)))
		{
			m_stInspInfo.SetLotInfo(m_stInspInfo.MESInfo.stMESResult.szLotNo, m_stInspInfo.MESInfo.stMESResult.szOperator);
			OnSetLotInfo();
		}
		else
		{
			m_stInspInfo.SetLotInfo(m_stInspInfo.MESInfo.stMESResult.szLotNo, m_stInspInfo.MESInfo.stMESResult.szOperator);
		}

		SYSTEMTIME Time;
		GetLocalTime(&Time);

		CString path = m_Worklist.Get_Report_Yield_FullPath(m_stInspInfo.Path.szReport, m_stInspInfo.szModelName, m_stInspInfo.MESInfo.stMESResult.szLotNo, &Time);
		m_FileReport.LoadYield_LOT(path, m_stInspInfo.YieldInfo);
		m_wnd_MainView.UpdateYield();

		m_stInspInfo.LotInfo.nLotCount = m_stInspInfo.YieldInfo.dwTotal;
		OnSetLotInfo();
		OnSetMESInfo();


	}
	else if (LOT_Start == LotStatus)
	{
		// 등록된 작업자 정보가 없으면, 리턴...
		/*
		CString szOperatorPath;
		szOperatorPath.Format(_T("%s\\OperatorConfig.ini"), m_stInspInfo.Path.szOperatorInfo);

		if (!PathFileExists(szOperatorPath))
		{
			AfxMessageBox(_T("Registered Operator information does not exist."));
			return FALSE;
		}

		ST_UserConfigInfo* const pUserConfigInfo = &m_stInspInfo.stUserConfigInfo;

		int iOperatorCount = pUserConfigInfo->Operatorlist.GetCount();
		if (iOperatorCount < 1)
		{
			AfxMessageBox(_T("Registered Operator information does not exist."));
			return FALSE;
		}

		CStringArray szArrOperator;
		for (int i = 0; i < iOperatorCount; i++)
			szArrOperator.Add(pUserConfigInfo->Operatorlist[i].szOperatorID);
		*/

		CStringArray szArrOperator;
		CDlg_SelectLot dlgLot;
		dlgLot.SetInfo(m_stInspInfo.Path.szReport, m_stInspInfo.ModelInfo.szModelCode);
		dlgLot.SetOperatorArray(&szArrOperator);
		dlgLot.SetLotName(m_stInspInfo.szLotName);
		dlgLot.SetOperator(m_stInspInfo.szOperatorName);

		if (m_stInspInfo.ModelInfo.szModelCode.IsEmpty())
		{
			m_stInspInfo.szModelName = m_stInspInfo.ModelInfo.szModelFile;
		}
		else
		{
			m_stInspInfo.szModelName = m_stInspInfo.ModelInfo.szModelCode;
		}

		if (IDOK == dlgLot.DoModal())
		{
			// LOT Start
			m_stInspInfo.SetLotStatus(TRUE);

			if (0 != m_stInspInfo.szLotName.Compare(dlgLot.GetLotName()))
			{
				// LOT 변경시 수율, Cycle Time 초기화 및 UI 업데이트
				OnResetYieldCycleTime();
			}

			if ((0 != m_stInspInfo.szLotName.Compare(dlgLot.GetLotName())) || (0 != m_stInspInfo.szOperatorName.Compare(dlgLot.GetOperatorName())))
			{
				m_stInspInfo.SetLotInfo(dlgLot.GetLotName(), dlgLot.GetOperatorName());			
				OnSetLotInfo();
			}
			else
			{
				m_stInspInfo.SetLotInfo(m_stInspInfo.szLotName, m_stInspInfo.szOperatorName);
			}

			SYSTEMTIME Time;
			GetLocalTime(&Time);

			CString path = m_Worklist.Get_Report_Yield_FullPath(m_stInspInfo.Path.szReport, m_stInspInfo.szModelName, m_stInspInfo.LotInfo.szLotName, &Time);
			m_FileReport.LoadYield_LOT(path, m_stInspInfo.YieldInfo);
			m_wnd_MainView.UpdateYield();

			m_stInspInfo.LotInfo.nLotCount = m_stInspInfo.YieldInfo.dwTotal;
			OnSetLotInfo();
		}
		else
		{
			return FALSE;
		}
	}
	else if (LOT_End == LotStatus)
	{
		// LOT End
		m_stInspInfo.SetLotStatus(FALSE);
		OnSetLotInfo();
	}

	return TRUE;
}

//=============================================================================
// Method		: OnChangeModelInfo
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/6/22 - 10:38
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::OnChangeModelInfo()
{
	AfxGetApp()->GetMainWnd()->EnableWindow(FALSE);
	m_wnd_ModelMode.MoveWindow(0, 0, 420, 200);
	m_wnd_ModelMode.CenterWindow();
// 
	m_wnd_ModelMode.SetModleInfo(m_stInspInfo.Path.szModel, MODEL_FILE_EXT);
	m_wnd_ModelMode.SetCurrentModel(m_stInspInfo.ModelInfo.szModelFile);

	m_wnd_ModelMode.ShowWindow(SW_SHOW);
	m_wnd_ModelMode.EnableWindow(TRUE);

	return TRUE;
}

//=============================================================================
// Method		: OnResetYieldCycleTime
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/21 - 11:07
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnResetYieldCycleTime()
{
	m_stInspInfo.YieldInfo.Reset();
	m_stInspInfo.CycleTime.Reset();

	m_wnd_MainView.UpdateYield();
}

//=============================================================================
// Method		: LoadModelInfo
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szModel
// Parameter	: BOOL bNotifyModelWnd
// Qualifier	:
// Last Update	: 2016/5/28 - 14:50
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::LoadModelInfo(__in LPCTSTR szModel, BOOL bNotifyModelWnd /*= TRUE*/)
{
	// 모델 파일에서 모델 정보 불러오기
	CString strFullPath;
	CString strRomFullPath;
	CString strLog;

	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szModel, szModel, MODEL_FILE_EXT);

	if (!bNotifyModelWnd)
	{
		ShowSplashScreen();
		m_wndSplash.SetText(_T("Changing Model..."));
	}
	
	// 변경하고자 하는 모델이 달라지면, 카메라 전원을 종료한다.
	if (m_stInspInfo.ModelInfo.szModelFile != szModel)
	{
		float fVoltageOff1[2] = { 0, 0 };

		SetLuriCamBrd_Volt(fVoltageOff1);
		CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, 1, 1, OFF);
	}

	// 모델 변경
	m_stInspInfo.ModelInfo.szModelFile = szModel;
	
	// 파일 불러오기
	CFile_Model		m_fileModel;
 	if (m_fileModel.LoadModelFile(strFullPath, m_stInspInfo.ModelInfo))
 	{
		// 선택한 모델 레지스트리에 저장
		m_regInspInfo.SaveSelectedModel(m_stInspInfo.ModelInfo.szModelFile, m_stInspInfo.ModelInfo.szModelCode);

		// UI 갱신
		m_wnd_MainView.UpdateModelInfo();

		OnSetGrabberBrd_ComArt_Reboot(m_stInspInfo.ModelInfo.nVideoType);

		// 데이터 초기화
		OnResetCamInfo();

		//if (bNotifyModelWnd)
		{
			m_wnd_RecipeView.SetModel(m_stInspInfo.ModelInfo.szModelFile);
		}	

		// 모델 정보 불러오기 완료
		strLog.Format(_T("File load completed. [File: %s]"), m_stInspInfo.ModelInfo.szModelFile);
		AddLog(strLog);
 	}
 	else
 	{
		ShowSplashScreen(FALSE);

 		strLog.Format(_T("Cannot load the Model File. [File: %s.luri]"), szModel);
 		AddLog(strLog);
		strLog.Format(_T("Cannot load the Model File.\r\nFile: %s"), strFullPath);
		MessageView(strLog);

		// UI 갱신
		m_wnd_MainView.UpdateModelInfo();

		OnSetGrabberBrd_ComArt_Reboot(m_stInspInfo.ModelInfo.nVideoType);

		// 데이터 초기화
		OnResetCamInfo();

		//if (bNotifyModelWnd)
		{
			m_wnd_RecipeView.SetModel(m_stInspInfo.ModelInfo.szModelFile);
		}
 	}

	ShowSplashScreen(FALSE);
	
	// 포고 카운트 설정
	LoadPogoCount();

	//CString strText;
	//strText = m_stInspInfo.Path.szI2C + m_stInspInfo.ModelInfo.szI2CFile + _T(".") + I2C_FILE_EXT;
	
	// 파일이 존재하는가?
	//if (PathFileExists(strText))
	//{
		//m_Device.DAQCtrl.SetI2CFilePath_All(strText);
	//}

	return TRUE;
}

//=============================================================================
// Method		: InitLoadModelInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/8/11 - 15:09
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitLoadModelInfo()
{
	CString strModelFile;
	CString strModelCode;
	CString strMotorFile;

	if (m_regInspInfo.LoadSelectedModel(strModelFile, strModelCode))
	{
		m_stInspInfo.ModelInfo.szModelFile = strModelFile;
		LoadModelInfo(strModelFile);
	}
	else
	{
		MessageView(_T("Set the model data."));

		// 포고 카운트 설정
		LoadPogoCount();

		// UI 갱신
		m_wnd_MainView.UpdateModelInfo();

		OnSetGrabberBrd_ComArt_Reboot(m_stInspInfo.ModelInfo.nVideoType);

		// 데이터 초기화
		OnResetCamInfo();

		//if (bNotifyModelWnd)
		{
			m_wnd_RecipeView.SetModel(m_stInspInfo.ModelInfo.szModelFile);
		}
	}
}

//=============================================================================
// Method		: LoadMotorInfo
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szMotor
// Parameter	: __in BOOL bNotifyModelWnd
// Qualifier	:
// Last Update	: 2018/1/3 - 14:21
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::LoadMotorInfo(__in LPCTSTR szMotor, __in BOOL bNotifyModelWnd /*= TRUE*/)
{
	// 모델 파일에서 모델 정보 불러오기
	CString strFullPath;
	CString strRomFullPath;
	CString strLog;

	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szMotor, szMotor, MOTOR_FILE_EXT);

	if (!bNotifyModelWnd)
	{
		ShowSplashScreen();
		m_wndSplash.SetText(_T("Changing Motor..."));
	}

	// 모터 변경
	m_stInspInfo.ModelInfo.szMotorFile = szMotor;

	m_Device.MotionManager.SetPrtMotorPath(&m_stInspInfo.Path.szMotor, m_stInspInfo.ModelInfo.szMotorFile);

	// 파일 불러오기
	if (m_Device.MotionManager.LoadMotionInfo())
	{
		// 선택한 모델 레지스트리에 저장
		m_regInspInfo.SaveSelectedMotor(m_stInspInfo.ModelInfo.szMotorFile);

		// UI 갱신
		m_wnd_MotorView.UpdateMotorInfo(szMotor);

		// 모델 정보 불러오기 완료
		strLog.Format(_T("Motor File load completed. [File: %s]"), m_stInspInfo.ModelInfo.szMotorFile);
		AddLog(strLog);
	}
	else
	{
		if (!bNotifyModelWnd)
		{
			ShowSplashScreen(FALSE);
		}

		strLog.Format(_T("Cannot load the Motor File. [File: %s.luri]"), szMotor);
		AddLog(strLog);
		strLog.Format(_T("Cannot load the Motor File.\r\nFile: %s"), strFullPath);
		AfxMessageBox(strLog, MB_SYSTEMMODAL);
		return FALSE;
	}

	if (!bNotifyModelWnd)
	{
		ShowSplashScreen(FALSE);
	}
#ifndef MOTION_NOT_USE	
	// 모터 원점
	if (m_Device.MotionManager.m_AllMotorData.pMotionParam != NULL)
	{

// 		if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_SensorParticleIn) == TRUE)
// 		{
// 			OnAddErrorInfo(Err_NoOrigin);
// 		}
// 		else{
			if (FALSE == MotorOrigin())
			{
				// 			OnDOut_StartLamp(FALSE);
				// 			OnDOut_StopLamp(FALSE);
				// 			OnDOut_TowerLamp(enLampColor::Lamp_Red, TRUE);
				return FALSE;
			}
		//}
		
	}
#endif
	return TRUE;
}

//=============================================================================
// Method		: InitLoadMotorInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/3 - 14:21
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitLoadMotorInfo()
{
	CString strMotorFile;

	if (m_regInspInfo.LoadSelectedMotor(strMotorFile))
	{
		m_stInspInfo.ModelInfo.szMotorFile = strMotorFile;
		LoadMotorInfo(strMotorFile);
	}
	else
	{
		AfxMessageBox(_T("Please set the motor data."), MB_SYSTEMMODAL);
	}
}

inline BOOL CView_MainCtrl::LoadMaintenanceInfo(LPCTSTR szMaintenance, BOOL bNotifyModelWnd)
{

	// 모델 파일에서 모델 정보 불러오기
	CString strFullPath;
	CString strRomFullPath;
	CString strLog;

	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szMaintenance, szMaintenance, MAINTENANCE_FILE_EXT);

	if (!bNotifyModelWnd)
	{
		ShowSplashScreen();
		m_wndSplash.SetText(_T("Changing Maintenance..."));
	}

	// 유지 변경
	m_stInspInfo.MaintenanceInfo.szMaintenanceFile = szMaintenance;
	m_stInspInfo.MaintenanceInfo.szMaintenanceFullPath = strFullPath;

	// 파일 불러오기
	CFile_Maintenance m_fileMaintenance;
	if (m_fileMaintenance.LoadTeachFile(strFullPath, m_stInspInfo.MaintenanceInfo.stTeachInfo))
	{
		//OnLightPSU_PowerOnOff(ON);

		// 선택한 유지 레지스트리에 저장
		m_regInspInfo.SaveSelectMaintenance(m_stInspInfo.MaintenanceInfo.szMaintenanceFile);

		// UI 갱신 //!SH _190102: UI 갱신?
		//m_wnd_MaintenanceView.UpdateMaintenanceInfo(szMaintenance);

		// 유지 정보 불러오기 완료
		strLog.Format(_T("Maintenance File load completed. [File: %s]"), m_stInspInfo.MaintenanceInfo.szMaintenanceFile);
		AddLog(strLog);
	}
	else
	{
		if (!bNotifyModelWnd)
		{
			ShowSplashScreen(FALSE);
		}

		strLog.Format(_T("Cannot load the Maintenance File. [File: %s.luri]"), szMaintenance);
		AddLog(strLog);
		strLog.Format(_T("Cannot load the Maintenance File.\r\nFile: %s"), strFullPath);
		AfxMessageBox(strLog, MB_SYSTEMMODAL);
		return FALSE;
	}

	if (!bNotifyModelWnd)
	{
		ShowSplashScreen(FALSE);
	}

	return TRUE;

}

void CView_MainCtrl::InitLoadMaintenanceInfo()
{
	CString szMaintenanceFile;

	if (m_regInspInfo.LoadSelectMaintenance(szMaintenanceFile))
	{
		m_stInspInfo.MaintenanceInfo.szMaintenanceFile = szMaintenanceFile;
		LoadMaintenanceInfo(szMaintenanceFile);
	}
	else
	{
		AfxMessageBox(_T("Please set the Maintenance data."), MB_SYSTEMMODAL);
	}
}

//=============================================================================
// Method		: EVMSPathSearch
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __out CString & szEVNPath
// Qualifier	:
// Last Update	: 2017/8/12 - 17:18
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::EVMSPathSearch(__out CString &szEVNPath)
{
	// 프로그램 폴더 구하기
	TCHAR szExePath[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, szExePath, MAX_PATH);

	CString	szStr;

	TCHAR drive[_MAX_DRIVE];
	TCHAR dir[_MAX_DIR];
	TCHAR file[_MAX_FNAME];
	TCHAR ext[_MAX_EXT];
	TCHAR backPath[2048];

	_tsplitpath_s(szExePath, drive, _MAX_DRIVE, dir, _MAX_DIR, file, _MAX_FNAME, ext, _MAX_EXT);

	szEVNPath.Format(_T("%s%s"), drive, dir);

	szStr = szEVNPath + _T("..");
	PathCanonicalize(backPath, szStr);//패스 정리
	szEVNPath.Format(_T("%s"), backPath);

	szEVNPath += _T("\\ENV\\MODEL");

	MakeSubDirectory(szEVNPath);

	return TRUE;
}

//=============================================================================
// Method		: DisplayVideo
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Qualifier	:
// Last Update	: 2016/12/28 - 16:18
// Desc.		:영상 뿌려지는 부분/ Pic
//=============================================================================
void CView_MainCtrl::DisplayVideo(__in UINT nChIdx, __in UINT nGrabType)
{
	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	LPBYTE pFrameImage = GetImageBuffer(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, dwWidth, dwHeight, nChannel);

	if (pFrameImage != NULL)
	{
		IplImage *pImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);
		
// 		if (m_stInspInfo.ModelInfo.nGrabType == GrabType_LVDS)
// 		{
// 			for (int y = 0; y < dwHeight; y++)
// 			{
//  				if (y < dwHeight - 1)	// 그래버 데드라인 4 : 제거
//  				{
//  					for (int x = 0; x < dwWidth; x++)
// 					{
// 						pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 0];
// 						pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 1];
// 						pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 2];
// 					}
//  				} 
//  				else
// 				{
// 					for (int x = 0; x < dwWidth; x++)
// 					{
// 						pImage->imageData[y * pImage->widthStep + x * 3 + 0] = 0;
// 						pImage->imageData[y * pImage->widthStep + x * 3 + 1] = 0;
// 						pImage->imageData[y * pImage->widthStep + x * 3 + 2] = 0;
// 					}
// 				}
// 				
// 			}
// 		}
		if (m_stInspInfo.ModelInfo.nGrabType == GrabType_NTSC)
		{
			int widthStep4 = dwWidth * nChannel;
			for (int y = 0; y < dwHeight; y++)
			{
				for (int x = 0; x < dwWidth; x++)
				{
					pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pFrameImage[y * widthStep4 + (x * nChannel) + 0];
					pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pFrameImage[y * widthStep4 + (x * nChannel) + 1];
					pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pFrameImage[y * widthStep4 + (x * nChannel) + 2];
				}
			}
		}

		if (pImage !=  NULL)
		{
			//!SH _190123: Master Dlg ??
// 			if (m_DlgMasterTest.IsWindowVisible() == TRUE)
// 			{
// 			} 
// 			else
// 			{
				if (TRUE == m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.IsWindowVisible())
				{
					m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.SetFrameImageBuffer(pImage);
				}

				if (TRUE == m_wnd_RecipeView.m_wnd_VideoView.IsWindowVisible())
				{
					m_wnd_RecipeView.m_wnd_VideoView.SetFrameImageBuffer(pImage);
				}
				if (TRUE == m_wnd_MotorView.m_wnd_VideoView.IsWindowVisible())
				{
					m_wnd_MotorView.m_wnd_VideoView.SetFrameImageBuffer(pImage);
				}
		//	}

			

			cvReleaseImage(&pImage);
		}
	}
}


void CView_MainCtrl::OnPopupParticleManualResult(__out UINT& nTestResult)
{
	//CDlg_ParticleManual DlgParticle;

	


	m_DlgVideoManual.SetOwner(this);
	m_DlgVideoManual.SetPtr_Device(&m_Device);
	m_DlgVideoManual.SetPtr_Modelinfo(&m_stInspInfo.ModelInfo);
	m_DlgVideoManual.SetVideoChannel(VideoView_Ch_1);

	if (m_DlgVideoManual.DoModal() == IDOK)
		nTestResult = TER_Pass;
	else
		nTestResult = TER_Fail;

	m_DlgVideoManual.DestroyWindow();
}

BOOL CView_MainCtrl::OnPopupParticleManualVisible()
{
	return m_DlgVideoManual.IsWindowVisible();
}

void CView_MainCtrl::OnPopupLEDTestResult(__in UINT nTestIdx, __out UINT& nTestResult)
{
	//m_DlgLEDTest.SetOwner(this);
	//m_DlgLEDTest.SetPtr_Device(&m_Device);
	//m_DlgLEDTest.SetPtr_Modelinfo(&m_stInspInfo.ModelInfo);
	//m_DlgLEDTest.SetVideoChannel(VideoView_Ch_1);

	//if (m_DlgLEDTest.DoModal() == IDOK)
	//{
	//	if (m_stInspInfo.ModelInfo.stLED[nTestIdx].stLEDCurrResult.nEachResult[0] == TER_Pass)
	//	{
	//		nTestResult = TER_Pass;
	//	}
	//	else
	//	{
	//		nTestResult = TER_Fail;
	//	}
	//}
	//else
	//{
	//	nTestResult = TER_Fail;
	//}
}

BOOL CView_MainCtrl::OnPopupLEDTestVisible()
{
	return m_DlgLEDTest.IsWindowVisible();

}

void CView_MainCtrl::OnPopupCenterPtTunningResult()
{
	m_DlgCenterPtTunning.SetOwner(this);
	m_DlgCenterPtTunning.SetPtr_Device(&m_Device);
	m_DlgCenterPtTunning.SetPtr_Modelinfo(&m_stInspInfo.ModelInfo);
	m_DlgCenterPtTunning.SetVideoChannel(VideoView_Ch_1);
	m_DlgCenterPtTunning.DoModal();
}

BOOL CView_MainCtrl::OnPopupCenterPtTunningVisible()
{
	return m_DlgCenterPtTunning.IsWindowVisible();

}

void CView_MainCtrl::OnPopupFailBoxConfirm()
{
	m_DlgFailBox.SetOwner(this);
	m_DlgFailBox.SetPtr_Device(&m_Device);
	m_DlgFailBox.DoModal();
}

BOOL CView_MainCtrl::OnPopupFailBoxConfirmVisible()
{
	return m_DlgFailBox.IsWindowVisible();
}

void CView_MainCtrl::SetSystemType(enInsptrSysType nSysType)
{
	//__super::SetSystemType(nSysType);

	//m_wnd_MainView.SetSystemType(nSysType);
	//!SH _190102: 중간에 넘겨주어야 하는 클래스 건너 뜀
	//			 : 프로그램 시작시 Teach Update 하는 용도로만 씀
	m_wnd_MotorView.SetSystemType(nSysType);
}

//=============================================================================
// Method		: CView_MainCtrl::AddLog
// Access		: public 
// Returns		: void
// Parameter	: LPCTSTR lpszLog
// Parameter	: BOOL bError
// Parameter	: UINT nLogType
// Parameter	: BOOL bOnlyLogType
// Qualifier	:
// Last Update	: 2013/1/16 - 15:39
// Desc.		:
//=============================================================================
void CView_MainCtrl::AddLog(LPCTSTR lpszLog, BOOL bError /*= FALSE*/, UINT nLogType /*= LOGTYPE_NORMAL*/, BOOL bOnlyLogType /*= FALSE*/)
{
	if (!GetSafeHwnd())
		return;

	if (NULL == lpszLog)
		return;

	__try
	{
		TCHAR		strTime[255] = { 0 };
		UINT_PTR	nLogSize = _tcslen(lpszLog) + 255;
		LPTSTR		lpszOutLog = new TCHAR[nLogSize];
		SYSTEMTIME	LocalTime;

		// **** 시간 추가 ****
		GetLocalTime(&LocalTime);
		StringCbPrintf(strTime, sizeof(strTime), _T("[%02d:%02d:%02d.%03d] "), LocalTime.wHour, LocalTime.wMinute, LocalTime.wSecond, LocalTime.wMilliseconds);

		// 파일 처리 ------------------------------------------------
		StringCbPrintf(lpszOutLog, nLogSize, _T("%s%s \r\n"), strTime, lpszLog);

		//switch (nLogType)
		//{
// 		case LOGTYPE_GUI:
// 			StringCbPrintf(lpszOutLog, nLogSize, _T("%s[GUI] %s \r\n"), strTime, lpszLog);
// 			break;
// 
// 		case LOGTYPE_COM:
// 			StringCbPrintf(lpszOutLog, nLogSize, _T("%s[COM] %s \r\n"), strTime, lpszLog);
// 			break;
// 
// 		case LOGTYPE_OPR:
// 			StringCbPrintf(lpszOutLog, nLogSize, _T("%s[OPR] %s \r\n"), strTime, lpszLog);
// 			break;
// 
// 		case LOGTYPE_NORMAL:
		//default:
		//	StringCbPrintf(lpszOutLog, nLogSize, _T("%s%s \r\n"), strTime, lpszLog);
		//	break;
		//}

		if (bError)
			m_Log_ErrLog.LogWrite(lpszOutLog);

		// UI 처리 --------------------------------------------------
		m_wnd_LogView.AddLog(lpszOutLog, bError, nLogType, RGB(0, 0, 0));
		m_logFile.LogWrite(lpszOutLog);

		delete[] lpszOutLog;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CView_MainCtrl::AddLog () \n"));
	}
}

//=============================================================================
// Method		: CView_MainCtrl::SetBackgroundColor
// Access		: protected 
// Returns		: void
// Parameter	: COLORREF color
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2010/10/13 - 10:40
// Desc.		:
//=============================================================================
void CView_MainCtrl::SetBackgroundColor(COLORREF color, BOOL bRepaint /*= TRUE*/)
{
	if (m_brBkgr.GetSafeHandle() != NULL)
	{
		m_brBkgr.DeleteObject();
	}

	if (color != (COLORREF)-1)
	{
		m_brBkgr.CreateSolidBrush(color);
	}

	if (bRepaint && GetSafeHwnd() != NULL)
	{
		Invalidate();
		UpdateWindow();
	}
}

//=============================================================================
// Method		: CView_MainCtrl::SwitchWindow
// Access		: public 
// Returns		: UINT
// Parameter	: UINT nIndex
// Qualifier	:
// Last Update	: 2010/11/26 - 14:06
// Desc.		: 자식 윈도우 전환하는 함수
// MainView에서 선택된 검사기 번호를 다른 윈도우로 넘긴다.
//=============================================================================
UINT CView_MainCtrl::SwitchWindow(UINT nIndex)
{
	if (m_nWndIndex == nIndex)
		return m_nWndIndex;

	UINT nOldView = m_nWndIndex;
	m_nWndIndex = nIndex;

	if (nOldView != -1)
	{
		if (m_pWndPtr[nOldView]->GetSafeHwnd())
		{
			m_pWndPtr[nOldView]->ShowWindow(SW_HIDE);
		}
	}

	if (m_pWndPtr[m_nWndIndex]->GetSafeHwnd())
	{
		m_pWndPtr[m_nWndIndex]->ShowWindow(SW_SHOW);
	}

	// 모델뷰 -> 메인뷰로 전환시 모델 데이터 갱신
	if ((SUBVIEW_RECIPE == nOldView) && (SUBVIEW_MAIN == m_nWndIndex))
	{
		m_wnd_RecipeView.InitOptionView();
	}

	if ((SUBVIEW_RECIPE == m_nWndIndex))
	{
		m_wnd_RecipeView.InitOptionView();
		m_wnd_RecipeView.SetModel(m_stInspInfo.ModelInfo.szModelFile);
	}

	if ((SUBVIEW_MAIN == m_nWndIndex))
	{
		m_bFlag_ReadyTest = TRUE;
	}
	else
	{
		m_bFlag_ReadyTest = FALSE;
	}

	return m_nWndIndex;
}

//=============================================================================
// Method		: CView_MainCtrl::SetCommPanePtr
// Access		: public 
// Returns		: void
// Parameter	: CWnd * pwndCommPane
// Qualifier	:
// Last Update	: 2013/7/16 - 16:51
// Desc.		:
//=============================================================================
void CView_MainCtrl::SetCommPanePtr(CWnd* pwndCommPane)
{
	m_pwndCommPane = pwndCommPane;
}

//=============================================================================
// Method		: ReloadOption
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2014/8/3 - 22:27
// Desc.		:
//=============================================================================
void CView_MainCtrl::ReloadOption()
{
	stLT_Option tempOpt = m_stOption;

	OnLoadOption();

	m_wnd_RecipeView.SetPath(
		m_stInspInfo.Path.szModel,
		m_stInspInfo.Path.szImage,
		m_stInspInfo.Path.szPogo,
		m_stInspInfo.Path.szI2C,
		m_stInspInfo.Path.szWav,
		m_stInspInfo.Path.szBarcodeInfo,
		m_stInspInfo.Path.szOperatorInfo,
		m_stInspInfo.Path.szPrnFile
		);

	m_wnd_RecipeView.SetPtr_BarcodeInfo(&m_stInspInfo.stBarcodeInfo, m_stInspInfo.szBarcodeRef);
	m_wnd_RecipeView.SetPtr_UserConfigInfo(&m_stInspInfo.stUserConfigInfo);

	// Master 버튼 활성 상태
//	m_wnd_MainView.OnSetBtnMasterTestEnable(m_stInspInfo.bUseMasterCheck);
	m_wnd_MainView.OnSetBtnMasterModeEnable(m_stInspInfo.bUseMasterCheck);

	BOOL bChanged = FALSE;
}

//=============================================================================
// Method		: CView_MainCtrl::InitStartProgress
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2014/7/5 - 10:49
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitStartProgress()
{
	ShowSplashScreen();

	m_wndSplash.SetText(_T("Loading Model File..."));

	OnLoadWorklist();

	// 모델 & 모터 정보 로드
	InitLoadModelInfo();
	InitLoadMaintenanceInfo();

	m_wndSplash.SetText(_T("Start Device Connection...."));

	// 주변 장치 연결
	__try
	{
		ConnectDevicez();
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : InitStartProgress ()"));
	}

	m_wndSplash.SetText(_T("Start Device init...."));

	// 검사 가능 상태로 변경
	if (InitStartBoardProgress() == FALSE)
	{
		m_bFlag_ReadyTest = FALSE;
	}
	else
	{
		m_bFlag_ReadyTest = TRUE;
	}
	ShowSplashScreen(FALSE);

	//모터 원점
	InitLoadMotorInfo();

	// 버튼 비활성화
	OnSetLotStartBtnStatus(TRUE);
	OnSetLotStopBtnStatus(TRUE);
	OnSetStartBtnStatus(FALSE);
	OnSetStopBtnStatus(FALSE);


	
	
}

//=============================================================================
// Method		: InitStartBoardProgress
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/7/7 - 10:26
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::InitStartBoardProgress()
{
	
#ifdef USE_LIGHTBRD_MODE
	// 광원 ON
	for (UINT nitem = 0; nitem < 4; nitem++)
	{
		if (m_Device.MotionSequence.LightVoltControl(enLightCtrl(LightChat_Center + nitem), m_stInspInfo.ModelInfo.stLightInfo.fVolt[nitem]) == FALSE)
		{
			ShowSplashScreen(FALSE);
			MessageView(_T("Light Board Volt Init Err"));
			return FALSE;
		}
		DoEvents(300);
	}

	for (UINT nitem = 0; nitem < 4; nitem++)
	{
		if (m_Device.MotionSequence.LightCurrentControl(enLightCtrl(LightChat_Center + nitem), m_stInspInfo.ModelInfo.stLightInfo.wCurrent[nitem]) == FALSE)
		{
			ShowSplashScreen(FALSE);
			MessageView(_T("Light Board Current Init Err"));
			return FALSE;
		}
		DoEvents(300);
	}

#endif

// 	// 카메라 OFF
// 	for (UINT nIdx = 0; nIdx < g_InspectorTable[SET_INSPECTOR].nPCBCamCnt; nIdx++)
// 	{
// 		// VOLT
// 		if (m_Device.PCBCamBrd[nIdx].Send_SetVolt(0) == FALSE)
// 		{
// 			ShowSplashScreen(FALSE);
// 			MessageView(_T("Interface Board Init Err"));
// 			//return FALSE;
// 		}
// 	}


	// 모터 원점
// 	if (m_Device.MotionSequence.AllMotorOrigin() == FALSE)
// 	{
// 		ShowSplashScreen(FALSE);
// 		MessageView(_T("Motor Origin Init Err"));
// 		//return FALSE;
// 	}


	// 인터락 해제
// 	if (m_Device.DigitalIOCtrl.AXTState() == TRUE)
// 	{
// 		if (m_Device.MotionSequence.InterlockMove(OFF) == FALSE)
// 		{
// 			ShowSplashScreen(FALSE);
// 			MessageView(_T("Interlock Release Err"));
// 			//return FALSE;
// 		}
// 	}

	m_Device.DigitalIOCtrl.Start_Monitoring();

	// 준비 상태
	if (m_Device.DigitalIOCtrl.AXTState() == TRUE)
	{
		if (m_Device.MotionSequence.TowerLamp(TowerLampYellow, ON) == FALSE)
		{
			ShowSplashScreen(FALSE);
			MessageView(_T("Tower Lamp Err"));
			//return FALSE;
		}
	}

	// 비전 조명
// 	if (m_Device.IF_Illumination.Send_SetOnOff(0, OFF))
// 	{
// 		//ShowSplashScreen(FALSE);
// 		//MessageView(_T("Tower Lamp Err"));
// 	}

	float fVoltageOff[2] = { 0, 0 };
	SetLuriCamBrd_Volt(fVoltageOff);
	m_Device.MotionSequence.OnAction_LED_OFF();
	m_Device.MotionSequence.OnAction_IR_LED_OFF();

	// 버튼 사용 가능
	for (UINT nIdx = 0; nIdx < DI_NotUseBit_Max; nIdx++)
		m_bFlag_Butten[nIdx] = FALSE;

	return TRUE;
}

//=============================================================================
// Method		: CView_MainCtrl::FinalExitProgress
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2016/06/13
// Desc.		: 프로그램 종료시 처리해야 할 코드들..
//=============================================================================
void CView_MainCtrl::FinalExitProgress()
{
	// 검사 불가 상태로 변경
	m_bFlag_ReadyTest = FALSE;
	FinalExitBoardProgress();

	TRACE(_T("Set Exit Program External Event\n"));
	m_bExitFlag = TRUE;

	if (FALSE == SetEvent(m_hEvent_ProgramExit))
	{
		TRACE(_T("Set Exit Program External Event 실패!!\n"));
	}

	OnShowSplashScreen(TRUE, _T("Program Exit..."));

	float fVoltageOff[2] = { 0, 0 };
	SetLuriCamBrd_Volt(fVoltageOff);
	m_Device.MotionSequence.OnAction_LED_OFF();
	m_Device.MotionSequence.OnAction_IR_LED_OFF();
	CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, 1, 1, OFF);

	// 주변 장치 연결 해제

	RunLedChange(DO_LampFail,FALSE);
	m_Device.MotionSequence.TowerLamp(TowerLampRed, OFF);
	m_Device.MotionSequence.TowerLamp(TowerLampYellow, OFF);
	m_Device.MotionSequence.TowerLamp(TowerLampGreen, OFF);
	DisconnectDevicez();

	//m_wndSplash.SetText(_T("-- 종료 --"));
	OnShowSplashScreen(TRUE, _T("Program Exit..."));
	Sleep(300);
	ShowSplashScreen(FALSE);
	TRACE(_T("- End ExitProgramCtrl -\n"));
}

//=============================================================================
// Method		: FinalExitBoardProgress
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/7/7 - 10:26
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::FinalExitBoardProgress()
{
	// 카메라 OFF
	//m_TIProcessing.CameraPowerOnOff(OFF);

	// 비전 조명 OFF
	//m_Device.IF_Illumination.Send_SetOnOff(0, OFF);

	// 경광등 
	m_Device.MotionSequence.TowerLamp(TowerLampYellow, OFF);

#ifdef USE_LIGHTBRD_MODE
	// 광원 OFF
	for (UINT nitem = 0; nitem < 4; nitem++)
		m_Device.MotionSequence.LightVoltControl(enLightCtrl(LightChat_Center + nitem), 0);
			
	for (UINT nitem = 0; nitem < 4; nitem++)
		m_Device.MotionSequence.LightCurrentControl(enLightCtrl(LightChat_Center + nitem), 0);
#endif

	return TRUE;
}

//=============================================================================
// Method		: ManualBarcode
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/10 - 13:28
// Desc.		:
//=============================================================================
void CView_MainCtrl::ManualBarcode()
{
	CDlg_Barcode	dlgBarcode;

	dlgBarcode.SetBarcodeCount(USE_CHANNEL_CNT);

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		dlgBarcode.SetUsableSite(nIdx, m_stInspInfo.bTestEnable[nIdx]);
	}
	
	if (IDOK == dlgBarcode.DoModal())
	{
		dlgBarcode.GetBarcode(m_stInspInfo.szBarcodeBuf);
		OnSetInsertBarcode(m_stInspInfo.szBarcodeBuf);
	}
}

//=============================================================================
// Method		: PermissionView
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/15 - 16:17
// Desc.		:
//=============================================================================
void CView_MainCtrl::PermissionView()
{
	AfxGetApp()->GetMainWnd()->EnableWindow(FALSE);
	m_wnd_AccessMode.MoveWindow(0, 0, 450, 200);
	m_wnd_AccessMode.CenterWindow();
	m_wnd_AccessMode.ShowWindow(SW_SHOW);
	m_wnd_AccessMode.EnableWindow(TRUE);
}

//=============================================================================
// Method		: WarringView
// Access		: public  
// Returns		: BOOL
// Parameter	: __in CString szText
// Qualifier	:
// Last Update	: 2017/7/12 - 10:56
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::MessageView(__in CString szText, __in BOOL bMode)
{
	CWnd_MessageView*	m_pwndMessageView;
	m_pwndMessageView = new CWnd_MessageView;

	BOOL bResult = FALSE;

	AfxGetApp()->GetMainWnd()->EnableWindow(FALSE);

	m_pwndMessageView->CreateEx(NULL, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(COLOR_WINDOW + 10)), _T("Message Mode"), WS_POPUPWINDOW | WS_SIZEBOX | WS_EX_TOPMOST, CRect(0, 0, 0, 0), this, NULL);
	m_pwndMessageView->EnableWindow(TRUE);
	m_pwndMessageView->CenterWindow();
	m_pwndMessageView->SetWarringMessage(szText);
	
	if (m_pwndMessageView->DoModal(bMode) == TRUE)
		bResult = TRUE;
	else
		bResult = FALSE;

	AfxGetApp()->GetMainWnd()->EnableWindow(TRUE);

	delete m_pwndMessageView;

	return bResult;
}

//=============================================================================
// Method		: PermissionStatsView
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/15 - 17:22
// Desc.		:
//=============================================================================
void CView_MainCtrl::PermissionStatsView()
{
	__super::PermissionStatsView();

	enPermissionMode	AcessMode = m_wnd_AccessMode.GetAcessMode();

	((CPane_CommStatus*)m_pwndCommPane)->SetStatus_PermissionMode(AcessMode);
	
	m_wnd_RecipeView.SetStatusEngineerMode(AcessMode);
}

//=============================================================================
// Method		: SetPermissionMode
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enPermissionMode nAcessMode
// Qualifier	:
// Last Update	: 2016/12/16 - 9:54
// Desc.		:
//=============================================================================
void CView_MainCtrl::SetPermissionMode(__in enPermissionMode nAcessMode)
{
	__super::SetPermissionMode(nAcessMode);

	m_wnd_MainView.SetInspectionMode(nAcessMode);
	m_wnd_MotorView.SetPermissionMode(nAcessMode);
	m_wnd_RecipeView.SetStatusEngineerMode(nAcessMode);
}
//=============================================================================
// Method		: SetOperateMode
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enOperateMode nOperMode
// Qualifier	:
// Last Update	: 2018/3/12 - 20:05
// Desc.		:
//=============================================================================
void CView_MainCtrl::SetOperateMode(__in enOperateMode nOperMode)
{
	if (FALSE == IsTesting())
	{
		CTestManager_EQP::SetOperateMode(nOperMode);

		((CPane_CommStatus*)m_pwndCommPane)->SetStatus_OperateMode(nOperMode);

		m_wnd_MainView.SetOperateMode(nOperMode);
	}
	else
	{
		TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
		OnLog_Err(_T("Set Operate Mode Error : Inspection is in progress."));
		AfxMessageBox(_T("Inspection is in progress. \r\n\r\nPlease wait until the Inspection is finished."), MB_SYSTEMMODAL);
	}
}

void CView_MainCtrl::ReportImageSave(__in enLT_TestItem_ID nTestItem, __in UINT nTestCount, __in SYSTEMTIME* pTime, __in UINT nEachResult)
{
	CString szTestitemFolder;

	IplImage* pBufImage = NULL;
	pBufImage = m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.GetFrameImageBuffer();

	if (pBufImage == NULL)
		return;

	IplImage* pPicImage = cvCreateImage(cvSize(pBufImage->width, pBufImage->height), IPL_DEPTH_8U, 3);
	cvCopy(pBufImage, pPicImage);

	BOOL bSaveFlag = FALSE;


	// ...\Default_A\2016-11-17\ 
	CString szDatePath;
	szDatePath.Format(_T("%s%04d-%02d-%02d\\%s"),//%s\\%s\\"), //!SH _190210: lot이랑 not lot 구분
		m_stInspInfo.Path.szImage, pTime->wYear, pTime->wMonth, pTime->wDay, 
		m_stInspInfo.szModelName );//, szTotalResult, pstWorklist->Time);
	CString szPath = szDatePath;

	if (m_stInspInfo.LotInfo.bLotStatus == TRUE)
	{
		szPath += _T("\\LotMode");

		if (!m_stInspInfo.LotInfo.szLotName.IsEmpty())
			szPath += _T("\\") + m_stInspInfo.LotInfo.szLotName;
	}
	else
	{
		szPath += _T("\\Not_LotMode");
	}

	MakeDirectory(szPath);
	CString szBarcode = m_stInspInfo.szBarcodeBuf;
	if (m_stInspInfo.szBarcodeBuf.IsEmpty() || m_stInspInfo.szBarcodeBuf == _T("No Barcode"))
	{
		szBarcode = _T("_NoBarcode");
	}
	else{
		szBarcode = m_stInspInfo.CamInfo.szBarcode;
	}
	
	
	


	szTestitemFolder.Format(_T("%04d%02d%02d_%02d%02d%02d_%s_"), pTime->wYear, pTime->wMonth, pTime->wDay, pTime->wHour, pTime->wMinute, pTime->wSecond, szBarcode);




	switch (nTestItem)
	{
	case TIID_TestInitialize:
	case TIID_TestFinalize:
	case TIID_Current:
		//szTestitemFolder.Format(_T("%s"), g_szLT_TestItem_Name[nTestItem]);
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		bSaveFlag = FALSE;
		break;

	case TIID_CenterPoint:
		//szTestitemFolder.Format(_T("%s"), g_szLT_TestItem_Name[nTestItem]);
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicCenterPoint(pPicImage);
		bSaveFlag = TRUE;
		break;

	case TIID_Rotation:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicRotate(pPicImage);
		bSaveFlag = TRUE;
		break;

	case TIID_EIAJ:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicEIAJ(pPicImage);
		bSaveFlag = TRUE;
		break;

	case TIID_FOV:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicAngle(pPicImage);
		bSaveFlag = TRUE;
		break;

	case TIID_Color:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicColor(pPicImage);
		bSaveFlag = TRUE;
		break;

	case TIID_Reverse:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicReverse(pPicImage);
		bSaveFlag = TRUE;
		break;

	case TIID_BlackSpot:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicParticle(pPicImage);
		bSaveFlag = TRUE;
		break;

	case TIID_ParticleManual:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicParticle(pPicImage);
		bSaveFlag = TRUE;
		break;

	case TIID_Brightness:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicBrightness(pPicImage);
		bSaveFlag = TRUE;
		break;

	case TIID_IRFilter:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.PicIRFilter(pPicImage);
		bSaveFlag = TRUE;
		break;

	default:
		break;
	}

	//원본은 m_TestImage 이거 저장

	if (bSaveFlag == TRUE)
	{
		CString strFile;
		strFile.Format(_T("%s\\%s_Pic.bmp"), szPath, szTestitemFolder);
		cvSaveImage(CT2A(strFile), pPicImage);

		strFile.Format(_T("%s\\%s_Origin.bmp"), szPath, szTestitemFolder);
		cvSaveImage(CT2A(strFile), m_TestImage);
		
		bSaveFlag = FALSE;
	}

	/*CString szTempFolder = _T("D:\\TempSaveImage\\") + szTestitemFolder;
	MakeDirectory(szTempFolder);

	CString szImageFilePath;
	szImageFilePath.Format(_T("%s\\%04d_%02d_%02d_%02d_%02d_%02d"),
		szTempFolder,
		pTime->wYear,
		pTime->wMonth,
		pTime->wDay,
		pTime->wHour,
		pTime->wMinute,
		pTime->wSecond
		);

	CString szImagePicFile = szImageFilePath + _T(".bmp");

	if (bSaveFlag == TRUE)
		cvSaveImage((CStringA)szImagePicFile, pPicImage);*/

	cvReleaseImage(&pPicImage);
}

void CView_MainCtrl::ManualTestImageSave(__in enLT_TestItem_ID nTestItem, IplImage *TestImage)
{
	CString szTestitemFolder;

	
	if (TestImage == NULL)
		return;

	IplImage* pPicImage = cvCreateImage(cvSize(TestImage->width, TestImage->height), IPL_DEPTH_8U, 3);
	cvCopy(TestImage, pPicImage);

	BOOL bSaveFlag = FALSE;

	SYSTEMTIME pTime;
	GetLocalTime(&pTime);




	// ...\Default_A\2016-11-17\ 
	CString szDatePath;
	szDatePath.Format(_T("%s%04d-%02d-%02d\\%s"),//%s\\%s\\"), //!SH _190210: lot이랑 not lot 구분
		m_stInspInfo.Path.szImage, pTime.wYear, pTime.wMonth, pTime.wDay,
		m_stInspInfo.ModelInfo.szModelCode);//, szTotalResult, pstWorklist->Time);
	CString szPath = szDatePath;
	szPath += _T("\\ManualTest");

	

	MakeDirectory(szPath);



	szTestitemFolder.Format(_T("%04d%02d%02d_%02d%02d%02d_"), pTime.wYear, pTime.wMonth, pTime.wDay, pTime.wHour, pTime.wMinute, pTime.wSecond);




	switch (nTestItem)
	{
	case TIID_TestInitialize:
	case TIID_TestFinalize:
	case TIID_Current:
		//szTestitemFolder.Format(_T("%s"), g_szLT_TestItem_Name[nTestItem]);
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		bSaveFlag = FALSE;
		break;

	case TIID_CenterPoint:
		//szTestitemFolder.Format(_T("%s"), g_szLT_TestItem_Name[nTestItem]);
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		if (m_wnd_RecipeView.m_bLive == TRUE)
		{
			m_wnd_RecipeView.m_wnd_VideoView.PicCenterPoint(pPicImage);
		}
		else{
			m_wnd_RecipeView.m_wnd_ImageView.PicCenterPoint(pPicImage);

		}
		bSaveFlag = TRUE;
		break;

	case TIID_Rotation:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];

		if (m_wnd_RecipeView.m_bLive == TRUE)
		{
			m_wnd_RecipeView.m_wnd_VideoView.PicRotate(pPicImage);
		}
		else{
			m_wnd_RecipeView.m_wnd_ImageView.PicRotate(pPicImage);

		}
		bSaveFlag = TRUE;
		break;

	case TIID_EIAJ:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		if (m_wnd_RecipeView.m_bLive == TRUE)
		{
			m_wnd_RecipeView.m_wnd_VideoView.PicEIAJ(pPicImage);
		}
		else{
			m_wnd_RecipeView.m_wnd_ImageView.PicEIAJ(pPicImage);

		}
		bSaveFlag = TRUE;
		break;

	case TIID_FOV:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		if (m_wnd_RecipeView.m_bLive == TRUE)
		{
			m_wnd_RecipeView.m_wnd_VideoView.PicAngle(pPicImage);
		}
		else{
			m_wnd_RecipeView.m_wnd_ImageView.PicAngle(pPicImage);

		}
		bSaveFlag = TRUE;
		break;

	case TIID_Color:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		if (m_wnd_RecipeView.m_bLive == TRUE)
		{
			m_wnd_RecipeView.m_wnd_VideoView.PicColor(pPicImage);
		}
		else{
			m_wnd_RecipeView.m_wnd_ImageView.PicColor(pPicImage);

		}
		bSaveFlag = TRUE;
		break;

	case TIID_Reverse:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		if (m_wnd_RecipeView.m_bLive == TRUE)
		{
			m_wnd_RecipeView.m_wnd_VideoView.PicReverse(pPicImage);
		}
		else{
			m_wnd_RecipeView.m_wnd_ImageView.PicReverse(pPicImage);

		}
		bSaveFlag = TRUE;
		break;

	case TIID_BlackSpot:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		if (m_wnd_RecipeView.m_bLive == TRUE)
		{
			m_wnd_RecipeView.m_wnd_VideoView.PicParticle(pPicImage);
		}
		else{
			m_wnd_RecipeView.m_wnd_ImageView.PicParticle(pPicImage);

		}
		bSaveFlag = TRUE;
		break;

	case TIID_ParticleManual:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
// 		if (m_wnd_RecipeView.m_bLive == TRUE)
// 		{
// 			m_wnd_RecipeView.m_wnd_VideoView.PicParticle(pPicImage);
// 		}
// 		else{
// 			m_wnd_RecipeView.m_wnd_ImageView.PicParticle(pPicImage);
// 
// 		}
		bSaveFlag = TRUE;
		break;

	case TIID_Brightness:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		if (m_wnd_RecipeView.m_bLive == TRUE)
		{
			m_wnd_RecipeView.m_wnd_VideoView.PicBrightness(pPicImage);
		}
		else{
			m_wnd_RecipeView.m_wnd_ImageView.PicBrightness(pPicImage);

		}
		bSaveFlag = TRUE;
		break;

	case TIID_IRFilter:
		szTestitemFolder += g_szLT_TestItem_Name[nTestItem];
		if (m_wnd_RecipeView.m_bLive == TRUE)
		{
			m_wnd_RecipeView.m_wnd_VideoView.PicIRFilter(pPicImage);
		}
		else{
			m_wnd_RecipeView.m_wnd_ImageView.PicIRFilter(pPicImage);

		}
		bSaveFlag = TRUE;
		break;

	default:
		break;
	}

	//원본은 m_TestImage 이거 저장

	if (bSaveFlag == TRUE)
	{
		CString strFile;
		strFile.Format(_T("%s\\%s_Pic.bmp"), szPath, szTestitemFolder);
		cvSaveImage(CT2A(strFile), pPicImage);

		strFile.Format(_T("%s\\%s_Origin.bmp"), szPath, szTestitemFolder);
		cvSaveImage(CT2A(strFile), TestImage);

		bSaveFlag = FALSE;
	}

	/*CString szTempFolder = _T("D:\\TempSaveImage\\") + szTestitemFolder;
	MakeDirectory(szTempFolder);

	CString szImageFilePath;
	szImageFilePath.Format(_T("%s\\%04d_%02d_%02d_%02d_%02d_%02d"),
	szTempFolder,
	pTime->wYear,
	pTime->wMonth,
	pTime->wDay,
	pTime->wHour,
	pTime->wMinute,
	pTime->wSecond
	);

	CString szImagePicFile = szImageFilePath + _T(".bmp");

	if (bSaveFlag == TRUE)
	cvSaveImage((CStringA)szImagePicFile, pPicImage);*/

	cvReleaseImage(&pPicImage);
}

//=============================================================================
// Method		: IsRecipeTest
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/7/13 - 17:36
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::IsRecipeTest()
{
	return m_wnd_RecipeView.IsRecipeTest();
}

//=============================================================================
// Method		: CView_MainCtrl::Test_Process
// Access		: public 
// Returns		: void
// Parameter	: UINT nTestNo
// Qualifier	:
// Last Update	: 2014/7/10 - 9:54
// Desc.		:
//=============================================================================
void CView_MainCtrl::Test_Process( UINT nTestNo )
{
	switch (nTestNo)
	{
	case 0:
 	//	m_wnd_MainView.m_wnd_SiteInfo.m_wnd_VideoView.SetEmptyImageBuffer();
 	//	break;
	{
			  BYTE nBoard = 0;
			  float data[1] = { 12.0, };
			  m_Device.PCBCamBrd[0].Send_Volt(data);
	}
		break;
	case 1:
	{
			  float data[1] = { 0, };
			  m_Device.PCBCamBrd[0].Send_Volt(data);
	}
		break;
	case 2:
	{
		//SaveEIAJMesSystem(0);
		ReportImageSave((enLT_TestItem_ID)3, 3, &m_stInspInfo.CamInfo.tmInputTime, 3);
	}
	break;

	default:
		m_bFlag_loopTest = FALSE;
		break;
	}
}
//=============================================================================
// Method		: MotorOrigin
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/11/7 - 21:55
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::MotorOrigin()
{
	BOOL  bReslut = FALSE;

#ifndef MOTION_NOT_USE
	CWnd_Origin*	pWnd_Origin;
	pWnd_Origin = new CWnd_Origin;

	AfxGetApp()->GetMainWnd()->EnableWindow(FALSE);


	pWnd_Origin->SetOwner(this);
	pWnd_Origin->SetPtr_Device(&m_Device);
	pWnd_Origin->CreateEx(NULL, AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(COLOR_WINDOW + 10)), _T("Message Mode"), WS_POPUPWINDOW | WS_SIZEBOX | WS_EX_TOPMOST, CRect(0, 0, 0, 0), this, NULL);
	pWnd_Origin->EnableWindow(TRUE);
	pWnd_Origin->CenterWindow();

	if (pWnd_Origin->DoModal() == TRUE)
		bReslut = TRUE;
	else
		bReslut = FALSE;

	ShowSplashScreen(FALSE);

	delete pWnd_Origin;
#endif

	return bReslut;
}

void CView_MainCtrl::ManualControl(WPARAM wParam, LPARAM lParam)
{
	enManual_Commend enCommend = (enManual_Commend)wParam;
	enManual_Motion_Com enMotion = (enManual_Motion_Com)lParam;

	if (enCommend != 0 && enMotion == 0)
	{
		float fVoltageOn[2] = { m_stInspInfo.ModelInfo.fVoltage[0], 0 };
		float fVoltageOff[2] = { 0, 0 };

		switch (enCommend - 1)
		{
		case MUL_VoltOn:
			SetLuriCamBrd_Volt(fVoltageOn);
			CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, 1, 1, ON);
			break;

		case MUL_VoltOff:
			CameraOnOff(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, 0, 1, 1, 1, OFF);
			SetLuriCamBrd_Volt(fVoltageOff);
			break;

		case MUL_LED_On:
			m_Device.MotionSequence.OnAction_LED_ON();
			break;

		case MUL_LED_Off:
			m_Device.MotionSequence.OnAction_LED_OFF();
			break;

		case MUL_IR_On:
			m_Device.MotionSequence.OnAction_IR_LED_ON();
			break;

		case MUL_IR_Off:
			m_Device.MotionSequence.OnAction_IR_LED_OFF();
			break;

		case MUL_OriginalImage:
		{
			DWORD dwWidth = 0;
			DWORD dwHeight = 0;
			UINT nChannel = 0;

			LPBYTE pFrameImage = GetImageBuffer(VideoView_Ch_1, m_stInspInfo.ModelInfo.nGrabType, dwWidth, dwHeight, nChannel);

			if (pFrameImage != NULL)
			{
				IplImage *pImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

				if (m_stInspInfo.ModelInfo.nGrabType == GrabType_NTSC)
				{
					for (int y = 0; y < dwHeight; y++)
					{
						for (int x = 0; x < dwWidth; x++)
						{
							pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 0];
							pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 1];
							pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 2];
						}
					}
				}

				if (pImage != NULL)  // rdfh
				{
					SYSTEMTIME SaveFileTime;
					SYSTEMTIME pTime;
					GetLocalTime(&pTime);




					// ...\Default_A\2016-11-17\ 
					CString szDatePath;
					szDatePath.Format(_T("%s%04d-%02d-%02d\\%s"),//%s\\%s\\"), //!SH _190210: lot이랑 not lot 구분
						m_stInspInfo.Path.szImage, pTime.wYear, pTime.wMonth, pTime.wDay,
						m_stInspInfo.ModelInfo.szModelCode);//, szTotalResult, pstWorklist->Time);
					CString szPath = szDatePath;
					szPath += _T("\\ManualTest");



					MakeDirectory(szPath);
					CString szTestitemFolder;


					szTestitemFolder.Format(_T("%04d%02d%02d_%02d%02d%02d_ManualSave"), pTime.wYear, pTime.wMonth, pTime.wDay, pTime.wHour, pTime.wMinute, pTime.wSecond);


						CString strFile;
						
						strFile.Format(_T("%s\\%s_Origin.bmp"), szPath, szTestitemFolder);
						cvSaveImage(CT2A(strFile), pImage);

					cvReleaseImage(&pImage);
				}
			}

		}
		break;

		default:
			break;
		}
	}
	else if (enCommend == 0 && enMotion != 0)
	{
		//!SH _190130: 여기에도 처리 있어야??
		switch (enMotion - 1)
		{
		case MUL_MOT_Load:
			m_Device.MotionSequence.OnAction_Load();
			break;

		case MUL_MOT_Insp:
			m_Device.MotionSequence.OnAction_Inspect(m_stInspInfo.ModelInfo.dbTestAxisX, m_stInspInfo.ModelInfo.dbTestAxisY, m_stInspInfo.ModelInfo.dbTestDistance);
			break;

		case MUL_MOT_Par_Load:
			m_Device.MotionSequence.OnAction_Par_Load();
			break;

		case MUL_MOT_Par_Insp:
			m_Device.MotionSequence.OnAction_Par_Inspect();
			break;

		case MUL_MOT_Cyl_In:
			m_Device.MotionSequence.OnAction_Par_Cylinder_In();
			break;

		case MUL_MOT_Cyl_Out:
			m_Device.MotionSequence.OnAction_Par_Cylinder_Out();
			break;

		default:
			break;
		}
	}
}


//=============================================================================
// Method		: OnSetMaterSetView
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bEnable
// Qualifier	:
// Last Update	: 2017/9/18 - 10:19
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetMaterSetView(__in BOOL bEnable)
{
	if (bEnable == TRUE)
	{
		OnSetResult(TER_MasterMode, _T("Master Setting Mode"));
	}
	else
	{
		OnSetResult(TER_Init, _T("Stand By"));
	}

	m_wnd_MainView.SetMasterMode(bEnable);
}

//=============================================================================
// Method		: OnSetMasterOffSetData
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in int iOffsetX
// Parameter	: __in int iOffsetY
// Parameter	: __in double dbDegree
// Qualifier	:
// Last Update	: 2018/4/9 - 18:23
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetMasterOffSetData(__in int iOffsetX, __in int iOffsetY, __in double dbDegree)
{
	m_wnd_MainView.UpdateMasterInfo(iOffsetX, iOffsetY, dbDegree);
}

//=============================================================================
// Method		: OnSetResult
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestResult Result
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2018/4/10 - 17:16
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetResult(__in enTestEachResult Result, __in CString strText)
{
	__super::OnSetResult(Result, strText);
	m_wnd_MainView.UpdateSetResult(Result, strText);
}