﻿// List_ColorOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_ColorOp.h"

#define IRtOp_ED_CELLEDIT			5001

// CList_ColorOp

IMPLEMENT_DYNAMIC(CList_ColorOp, CListCtrl)

CList_ColorOp::CList_ColorOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol	= 0;
	m_nEditRow	= 0;

	m_nWidth	= 640;
	m_nHeight	= 480;

	m_pstColor = NULL;
}

CList_ColorOp::~CList_ColorOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_ColorOp, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_ColorOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_ColorOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IRtOp_ED_CELLEDIT, &CList_ColorOp::OnEnKillFocusEdit)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_ColorOp 메시지 처리기입니다.
int CList_ColorOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IRtOp_ED_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ColorOp::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[CoOp_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = CoOp_PosX; nCol < CoOp_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() - iHeaderWidth_ColorOp[CoOp_Object]) / (CoOp_MaxCol - CoOp_PosX);
		SetColumnWidth(nCol, iUnitWidth);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ColorOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ColorOp::InitHeader()
{
	for (int nCol = 0; nCol < CoOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_ColorOp[nCol], iListAglin_ColorOp[nCol], iHeaderWidth_ColorOp[nCol]);
	}

	for (int nCol = 0; nCol < CoOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_ColorOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_ColorOp::InsertFullData()
{
	if (m_pstColor == NULL)
		return;

 	DeleteAllItems();
 
	for (UINT nIdx = 0; nIdx < CoOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_ColorOp::SetRectRow(UINT nRow)
{
	if (m_pstColor == NULL)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_szRoiColor[nRow]);
	SetItemText(nRow, CoOp_Object, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.CenterPoint().x);
	SetItemText(nRow, CoOp_PosX, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.CenterPoint().y);
	SetItemText(nRow, CoOp_PosY, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.Width());
	SetItemText(nRow, CoOp_Width, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.Height());
	SetItemText(nRow, CoOp_Height, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMinSpcR);
	SetItemText(nRow, CoOp_MinR, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMaxSpcR);
	SetItemText(nRow, CoOp_MaxR, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMinSpcG);
	SetItemText(nRow, CoOp_MinG, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMaxSpcG);
	SetItemText(nRow, CoOp_MaxG, strValue);
	
	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMinSpcB);
	SetItemText(nRow, CoOp_MinB, strValue);

	strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMaxSpcB);
	SetItemText(nRow, CoOp_MaxB, strValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_ColorOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	m_pstColor->bTestMode = FALSE;

	m_pstColor->stColorOpt.iSelectROI = pNMItemActivate->iItem;
	m_pstColor->stColorResult.iSelectROI = -1;

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_ColorOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < CoOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_pstColor->bTestMode = FALSE;
			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			ModifyStyle(WS_HSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_ColorOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_ColorOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (m_pstColor == NULL)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	CRect rtTemp = m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi;

	switch (nCol)
	{
	case CoOp_PosX:
		m_pstColor->stColorOpt.stRegionOp[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
		break;
	case CoOp_PosY:
		m_pstColor->stColorOpt.stRegionOp[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
		break;
	case CoOp_Width:
		m_pstColor->stColorOpt.stRegionOp[nRow].RectPosWH(iValue, rtTemp.Height());
		break;
	case CoOp_Height:
		m_pstColor->stColorOpt.stRegionOp[nRow].RectPosWH(rtTemp.Width(), iValue);
		break;
	case CoOp_MinR:
		m_pstColor->stColorOpt.stRegionOp[nRow].nMinSpcR = iValue;
		break;
	case CoOp_MaxR:
		m_pstColor->stColorOpt.stRegionOp[nRow].nMaxSpcR = iValue;
		break;
	case CoOp_MinG:
		m_pstColor->stColorOpt.stRegionOp[nRow].nMinSpcG = iValue;
		break;
	case CoOp_MaxG:
		m_pstColor->stColorOpt.stRegionOp[nRow].nMaxSpcG = iValue;
		break;
	case CoOp_MinB:
		m_pstColor->stColorOpt.stRegionOp[nRow].nMinSpcB = iValue;
		break;
	case CoOp_MaxB:
		m_pstColor->stColorOpt.stRegionOp[nRow].nMaxSpcB = iValue;
		break;
	default:
		break;
	}

	if (m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.left < 0)
	{
		CRect rtTemp = m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi;

		m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.left = 0;
		m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.right = m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.left + rtTemp.Width();
	}

	if (m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.right > m_nWidth)
	{
		CRect rtTemp = m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi;

		m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.right = m_nWidth;
		m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.left = m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.right - rtTemp.Width();
	}

	if (m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.top < 0)
	{
		CRect rtTemp = m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi;

		m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.top = 0;
		m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.bottom = rtTemp.Height() + m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.top;
	}

	if (m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.bottom > m_nHeight)
	{
		CRect rtTemp = m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi;

		m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.bottom = m_nHeight;
		m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.top = m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.bottom - rtTemp.Height();
	}

	if (m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.Height() <= 0)
		m_pstColor->stColorOpt.stRegionOp[nRow].RectPosWH(m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.Width(), 1);

	if (m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.Height() >= m_nHeight)
		m_pstColor->stColorOpt.stRegionOp[nRow].RectPosWH(m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.Width(), m_nHeight);

	if (m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.Width() <= 0)
		m_pstColor->stColorOpt.stRegionOp[nRow].RectPosWH(1, m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.Height());

	if (m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.Width() >= m_nWidth)
		m_pstColor->stColorOpt.stRegionOp[nRow].RectPosWH(m_nWidth, m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.Height());

	CString strValue;

	switch (nCol)
	{
	case CoOp_PosX:
		strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.CenterPoint().x);
		break;
	case CoOp_PosY:
		strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.CenterPoint().y);
		break;
	case CoOp_Width:
		strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.Width());
		break;
	case CoOp_Height:
		strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].rtRoi.Height());
		break;
	case CoOp_MinR:
		strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMinSpcR);
		break;
	case CoOp_MaxR:
		strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMaxSpcR);
		break;
	case CoOp_MinG:
		strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMinSpcG);
		break;
	case CoOp_MaxG:
		strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMaxSpcG);
		break;
	case CoOp_MinB:
		strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMinSpcB);
		break;
	case CoOp_MaxB:
		strValue.Format(_T("%d"), m_pstColor->stColorOpt.stRegionOp[nRow].nMaxSpcB);
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ColorOp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	CString str;
	str.Format(_T("%.1f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ColorOp::GetCellData()
{
	if (m_pstColor == NULL)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ColorOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue  = _ttof(strText);

		iValue = iValue + ((zDelta / 120));
		dbValue = dbValue + ((zDelta / 120)*0.1);

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		if (dbValue > 2.0)
			dbValue = 2.0;
		
		UpdateCellData(m_nEditRow, m_nEditCol, iValue);
	}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}
