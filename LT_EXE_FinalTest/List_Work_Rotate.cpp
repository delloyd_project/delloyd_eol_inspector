﻿// List_Work_Rotate.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_Work_Rotate.h"

// CList_Work_Rotate
IMPLEMENT_DYNAMIC(CList_Work_Rotate, CListCtrl)

CList_Work_Rotate::CList_Work_Rotate()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Work_Rotate::~CList_Work_Rotate()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_Work_Rotate, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CList_Work_Rotate 메시지 처리기입니다.
int CList_Work_Rotate::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	InitHeader();
	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/2/22 - 12:44
// Desc.		:
//=============================================================================
void CList_Work_Rotate::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
BOOL CList_Work_Rotate::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_Rotate::InitHeader()
{
	for (int nCol = 0; nCol < Rota_W_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_Rota_Worklist[nCol], iListAglin_Rota_Worklist[nCol], iHeaderWidth_Rota_Worklist[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in const  ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_Rotate::InsertFullData(__in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	int iNewCount = GetItemCount();

	InsertItem(iNewCount, _T(""));
	SetRectRow(iNewCount, pstCamInfo);
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: __in const  ST_CamInfo* pstCamInfo
// Qualifier	:
// Last Update	: 2017/2/22 - 12:45
// Desc.		:
//=============================================================================
void CList_Work_Rotate::SetRectRow(UINT nRow, __in const ST_CamInfo* pstCamInfo)
{
	if (NULL == pstCamInfo)
		return;

	CString strText;

	strText.Format(_T("%s"), pstCamInfo->szIndex);
	SetItemText(nRow, Rota_W_Recode, strText);

	strText.Format(_T("%s"), pstCamInfo->szTime);
	SetItemText(nRow, Rota_W_Time, strText);

	strText.Format(_T("%s"), pstCamInfo->szEquipment);
	SetItemText(nRow, Rota_W_Equipment, strText);

	strText.Format(_T("%s"), pstCamInfo->szModelName);
	SetItemText(nRow, Rota_W_Model, strText);

	strText.Format(_T("%s"), pstCamInfo->szSWVersion);
	SetItemText(nRow, Rota_W_SWVersion, strText);

	strText.Format(_T("%s"), pstCamInfo->szLotID);
	SetItemText(nRow, Rota_W_LOTNum, strText);

	strText.Format(_T("%s"), pstCamInfo->szBarcode);
	SetItemText(nRow, Rota_W_Barcode, strText);

	//strText.Format(_T("%s"), pstCamInfo->szOperatorName);
	//SetItemText(nRow, Rota_W_Operator, strText);
 
// 	strText.Format(_T("%s"), pstCamInfo->szCamType);
// 	SetItemText(nRow, Rota_W_CameraType, strText);

	strText.Format(_T("%s"), g_TestEachResult[pstCamInfo->stRotate[m_nTestIndex].stRotateResult.nResult].szText);
	SetItemText(nRow, Rota_W_Result, strText);

	if (pstCamInfo->stRotate[m_nTestIndex].stRotateResult.nResult == TER_Init)
	{
		strText.Format(_T("X"));
		SetItemText(nRow, Rota_W_Degree, strText);
	}
	else
	{
		strText.Format(_T("%0.2f"), pstCamInfo->stRotate[m_nTestIndex].stRotateResult.dbValue);
		SetItemText(nRow, Rota_W_Degree, strText);
	}
}

//=============================================================================
// Method		: GetData
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: UINT & DataNum
// Parameter	: CString * Data
// Qualifier	:
// Last Update	: 2017/2/22 - 12:47
// Desc.		:
//=============================================================================
void CList_Work_Rotate::GetData(UINT nRow, UINT &DataNum, CString *Data)
{
	DataNum = Rota_W_MaxCol;
	CString temp[Rota_W_MaxCol];
	for (int t = 0; t < Rota_W_MaxCol; t++)
	{
		temp[t] = GetItemText(nRow, t);
		Data[t] = GetItemText(nRow, t);
	}
}
