﻿//*****************************************************************************
// Filename	: 	Wnd_EachTestResult.h
// Created	:	2017/1/4 - 13:33
// Modified	:	2017/1/4 - 13:33
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_EachTestResult_h__
#define Wnd_EachTestResult_h__

#pragma once

#include "Def_TestDevice.h"

#include "List_AngleResult.h"
#include "List_BrightnessResult.h"
#include "List_CenterPointResult.h"
#include "List_ColorResult.h"
#include "List_CurrentResult.h"
#include "List_FFTResult.h"
#include "List_ParticleResult.h"
#include "List_RotateResult.h"
#include "List_SFRResult.h"
#include "List_DefectPixelResult.h"

#include "List_EIAJResult.h"
#include "List_ReverseResult.h"
#include "List_ParticleMAResult.h"
#include "List_PatternNoiseResult.h"
#include "List_IRFilterResult.h"
#include "List_IRFilterManualResult.h"
#include "List_LEDResult.h"
#include "List_VideoSignalResult.h"
#include "List_ResetResult.h"
#include "List_OperModeResult.h"


// CWnd_EachTestResult

class CWnd_EachTestResult : public CWnd
{
	DECLARE_DYNAMIC(CWnd_EachTestResult)

public:
	CWnd_EachTestResult();
	virtual ~CWnd_EachTestResult();

protected:
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()

	ST_ModelInfo*	m_pModelinfo;
	CFont			m_font;

	CMFCTabCtrl		m_tc_Option;
	
	CList_FOVResult			m_listResultFOV;
	CList_BrightnessResult		m_listResultBrightness;
	CList_CenterPointResult		m_listResultCenterPoint;
	CList_ColorResult			m_listResultColor;
	CList_CurrentResult			m_listResultCurrent;
//	CList_FFTResult				m_listResultFFT;
	CList_ParticleResult		m_listResultParticle;
	CList_RotateResult			m_listResultRotate;
//	CList_SFRResult				m_listResultSFR;
	CList_IRFilterResult		m_listResultIRFilter;

	CList_EIAJResult			m_listResultEIAJ;
	CList_ReverseResult			m_listResultReverse;
	CList_ParticleMAResult		m_listResultParticleMA;
	CList_DefectPixelResult		m_listResultDefectPixel;
//	CList_PatternNoiseResult	m_listResultPatternNoise;
//	CList_IRFilterManualResult	m_listResultIRFilterMA;
//	CList_LEDResult				m_listResultLED;
//	CList_VideoSignalResult		m_listResultVideoSignal;
//	CList_ResetResult			m_listResultReset;
//	CList_OperModeResult		m_listResultOperMode;

	UINT m_nTestItemID;
	UINT m_nTestCount;

public:

	void SetPtr_Modelinfo(__in ST_ModelInfo* pModelinfo)
	{
		m_pModelinfo = pModelinfo;
	};

	void SetTestItemTab(__in UINT nTestItemID)
	{
		m_tc_Option.SetActiveTab(m_nTestItemID - TIID_Current);
	}

	void SetTestItemResult(__in UINT nTestItemID, __in UINT nTestCount)
	{
		m_nTestItemID	= nTestItemID;
		m_nTestCount	= nTestCount;

		m_tc_Option.SetActiveTab(m_nTestItemID - TIID_Current);

		switch (m_nTestItemID)
		{
		case TIID_Current:
			m_listResultCurrent.SetPtr_Current(&m_pModelinfo->stCurrent[nTestCount]);
			m_listResultCurrent.InsertFullData();
			break;

		//case TIID_OperationMode:
		//	m_listResultOperMode.SetPtr_OperMode(&m_pModelinfo->stOperMode[nTestCount]);
		//	m_listResultOperMode.InsertFullData();
		//	break;

		case TIID_CenterPoint:
			m_listResultCenterPoint.SetPtr_CenterPoint(&m_pModelinfo->stCenterPoint[nTestCount]);
			m_listResultCenterPoint.InsertFullData();
			break;

		case TIID_Rotation:
			m_listResultRotate.SetPtr_Rotate(&m_pModelinfo->stRotate[nTestCount]);
			m_listResultRotate.InsertFullData();
			break;

 		case TIID_EIAJ:
 			m_listResultEIAJ.SetPtr_EIAJ(&m_pModelinfo->stEIAJ[nTestCount]);
 			m_listResultEIAJ.InsertFullData();
 			break;

		case TIID_FOV:
			m_listResultFOV.SetPtr_Angle(&m_pModelinfo->stAngle[nTestCount]);
			m_listResultFOV.InsertFullData();
			break;

		case TIID_Color:
			m_listResultColor.SetPtr_Color(&m_pModelinfo->stColor[nTestCount]);
			m_listResultColor.InsertFullData();
			break;

		case TIID_Reverse:
			m_listResultReverse.SetPtr_Reverse(&m_pModelinfo->stReverse[nTestCount]);
			m_listResultReverse.InsertFullData();
			break;

		//case TIID_LEDTest:
		//	m_listResultLED.SetPtr_LED(&m_pModelinfo->stLED[nTestCount]);
		//	m_listResultLED.InsertFullData();
		//	break;

		case TIID_ParticleManual:
			m_listResultParticleMA.SetPtr_ParticleMA(&m_pModelinfo->stParticleMA[nTestCount]);
			m_listResultParticleMA.InsertFullData();
			break;

		case TIID_BlackSpot:
			m_listResultParticle.SetPtr_Particle(&m_pModelinfo->stBlackSpot[nTestCount]);
			m_listResultParticle.InsertFullData();
			break;

// 		case TIID_VideoSignal:
// 			m_listResultVideoSignal.SetPtr_VideoSignal(&m_pModelinfo->stVideoSignal[nTestCount]);
// 			m_listResultVideoSignal.InsertFullData();
// 			break;
// 
// 		case TIID_Reset:
// 			m_listResultReset.SetPtr_Reset(&m_pModelinfo->stReset[nTestCount]);
// 			m_listResultReset.InsertFullData();
// 			break;

		//case TIID_PatternNoise:
		//	m_listResultPatternNoise.SetPtr_PatternNoise(&m_pModelinfo->stPatternNoise[nTestCount]);
		//	m_listResultPatternNoise.InsertFullData();
		//	break;

		case TIID_IRFilter:
			m_listResultIRFilter.SetPtr_IRFilter(&m_pModelinfo->stIRFilter[nTestCount]);
			m_listResultIRFilter.InsertFullData();
			break;

		case TIID_Brightness:
			m_listResultBrightness.SetPtr_Brightness(&m_pModelinfo->stBrightness[nTestCount]);
			m_listResultBrightness.InsertFullData();
			break;

		case TIID_DefectPixel:
			m_listResultDefectPixel.SetPtr_DefectFixel(&m_pModelinfo->stDefectPixel[nTestCount]);
			m_listResultDefectPixel.InsertFullData();
			break;
// 		case TIID_IRFilterManual:
// 			m_listResultIRFilterMA.SetPtr_IRFilterManual(&m_pModelinfo->stIRFilterManual[nTestCount]);
// 			m_listResultIRFilterMA.InsertFullData();
// 			break;
//
// 		case TIID_FFT:
// 			m_listResultFFT.SetPtr_FFT(&m_pModelinfo->stFFT[nTestCount]);
// 			m_listResultFFT.InsertFullData();
// 			break;
// 
// 		case TIID_Particle:
// 			m_listResultParticle.SetPtr_Particle(&m_pModelinfo->stBlackSpot[nTestCount]);
// 			m_listResultParticle.InsertFullData();
// 			break;

		//case TIID_SFR:
		//	m_listResultSFR.SetPtr_SFR(&m_pModelinfo->stSFR[nTestCount]);
		//	m_listResultSFR.InsertFullData();
		//	break;

		default:
			break;
		}
	};

};
#endif // Wnd_EachTestResult_h__


