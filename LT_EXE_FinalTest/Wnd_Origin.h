﻿//*****************************************************************************
// Filename	: 	Wnd_Origin.h
// Created	:	2017/03/28 - 13:47
// Modified	:	2017/03/28 - 13:47
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************

#pragma once
#include "VGStatic.h"
#include "resource.h"
#include "Def_Motion.h"
#include "Def_TestDevice.h"
#include "VGGroupWnd.h"

#define  MAX_OR_AIXS		 100

#define	 MT_OR_WIDTH_OFFSET	 1.0
#define	 MT_OR_HEIGHT_OFFSET 1.0

typedef enum MotionOrigin_ID
{
	IDC_ST_OR_AXIS_NAME		= 1000,
	IDC_ST_OR_SEN_AMP		= 1100,
	IDC_ST_OR_SEN_POS		= 1200,
	IDC_ST_OR_SEN_ORI		= 1300,
	IDC_ST_OR_SEN_MOTION	= 1400,
	IDC_ST_OR_SEN_LLIT		= 1500,
	IDC_ST_OR_SEN_HOME		= 1600,
	IDC_ST_OR_SEN_HLIT		= 1700,
	IDC_ST_OR_SEN_ALRAM		= 1800,
	IDC_BN_OR_STOP			= 1900,
	IDC_ST_OR_SEN_MAXNUM,
};

enum enMotionOriginStatic
{
	ST_MT_OR_AXIS_NAME = 0,
	ST_MT_OR_POWER_NAME,
	ST_MT_OR_POS_NAME,
	ST_MT_OR_ORI_NAME,
	ST_MT_OR_MOTION_NAME,
	ST_MT_OR_HLIT_NAME,
	ST_MT_OR_HOME_NAME,
	ST_MT_OR_LLIT_NAME,
	ST_MT_OR_ALRAM_NAME,
	ST_MT_OR_MAXNUM,
};

enum enOriginButton
{
	BN_ORI_FIX_L = 0,
	BN_ORI_FIX_R,
	BN_ORI_START,
	BN_ORI_STOP,
	BN_ORI_EXIT,
	BN_ORI_MAXNUM,
};

static LPCTSTR g_szOriginButton[] =
{
	_T("CYL UNFIX [L]"),
	_T("CYL UNFIX [R]"),
	_T("START"),
	_T("STOP"),
	_T("EXIT"),
	NULL
};

static LPCTSTR g_szMotionOriginStatusName[] =
{
	_T("AXIS NAME"),
	_T("POWER"),
	_T("POSITION"),
	_T("ORIGIN"),
	_T("IN MOTION"),
	_T("+ LIMIT"),
	_T("HOME"),
	_T("- LIMIT"),
	_T("ALRAM"),
	NULL
};

// CWnd_Origin
class CWnd_Origin : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Origin)

public:
	CWnd_Origin();
	virtual ~CWnd_Origin();
	ST_Device*	m_pstDevice;

	void SetPtr_Device(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
	};

	void	SetInspectorType(__in enInsptrSysType nInsptrType)
	{
		m_InsptrType = nInsptrType;
	}

	BOOL	DoModal();

protected:

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void	OnAxisSelect(UINT nID);
	afx_msg void	OnAxisAmpCtr(UINT nID);
	afx_msg void	OnAxisAlramCtr(UINT nID);
	afx_msg void	OnBnClickedBnStart();
	afx_msg void	OnBnClickedBnStop();
	afx_msg void	OnBnClickedBnExit();
	afx_msg void	OnBnClickedBnLeftUnFix();
	afx_msg void	OnBnClickedBnRightUnFix();
	afx_msg void	OnShowWindow(BOOL bShow, UINT nStatus);

	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

	void	SetUpdataAxisName();

	void	SetUpdataDataReset(UINT nAxis);

	DECLARE_MESSAGE_MAP()

	CFont			m_font;
	UINT			m_nAxisNum;
	UINT			m_nAxisMax;
	BOOL			m_bResult;
	BOOL			m_bFlag_Timer;
	enInsptrSysType m_InsptrType;

	//	타이머
	HANDLE			m_hTimerQueue;
	HANDLE			m_hTimer_SensorCheck;

	CVGGroupWnd		m_Group_Nmae;

	CVGStatic		m_st_AxisName[MAX_OR_AIXS];
	CVGStatic		m_st_AxisSen[MAX_OR_AIXS][ST_MT_OR_MAXNUM];

	CMFCButton		m_bn_Item[BN_ORI_MAXNUM];

	static VOID CALLBACK TimerRoutine_SensorCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired);

	void	OnMonitorSensorCheck();
	void	CreateTimerQueue_Mon();
	void	DeleteTimerQueue_Mon();
	void	CreateTimerSensorCheck();
	void	DeleteTimerSensorCheck();

	void	GetMotorAmpStatus();
	void	GetMotorOriginStatus();
	void	GetMotorCurrentPosStatus();
	void	GetMotorMotionStatus();
	void	GetMotorLimitStatus();
	void	GetMotorHomeStatus();
	void	GetMotorAlarmStatus();

	void	IsOrigin();
};