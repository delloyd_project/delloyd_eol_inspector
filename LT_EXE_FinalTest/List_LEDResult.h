﻿#ifndef List_LEDResult_h__
#define List_LEDResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_LEDResult
{
	LEDResult_Object = 0,
	LEDResult_Value,
	LEDResult_OnOffCount,
	LEDResult_MinSpec,
	LEDResult_MaxSpec,
	LEDResult_Result,
	LEDResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_LEDResult[] =
{
	_T(""),
	_T("Current"),
	_T("On Off Count"),
	_T("Min Spec"),
	_T("Max Spec"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_LEDResult
{
	LEDResult_ItemNum = 1,
};

static LPCTSTR	g_lpszItem_LEDResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_LEDResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_LEDResult[] =
{
	80,
	80,
	80,
	80,
	80,
	80,
};
// List_LEDInfo

class CList_LEDResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_LEDResult)

public:
	CList_LEDResult();
	virtual ~CList_LEDResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_LED(ST_LT_TI_LED* pstLED)
	{
		if (pstLED == NULL)
			return;

		m_pstLED = pstLED;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_LED*  m_pstLED;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_LEDInfo_h__
