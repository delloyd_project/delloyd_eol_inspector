﻿//*****************************************************************************
// Filename	: Wnd_WorklistReset.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistReset_h__
#define Wnd_WorklistReset_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_Reset.h"

//=============================================================================
// Wnd_WorklistReset
//=============================================================================
class CWnd_WorklistReset : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistReset)

public:
	CWnd_WorklistReset();
	virtual ~CWnd_WorklistReset();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

//	CList_Work_Reset				m_list_Reset[TICnt_Reset];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistReset_h__


