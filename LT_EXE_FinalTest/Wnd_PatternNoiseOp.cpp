﻿// Wnd_PatternNoiseOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_PatternNoiseOp.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_PatternNoiseOp
typedef enum RotateOptID
{
	IDC_BTN_ITEM  = 1001,
	IDC_EDT_ITEM = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_PatternNoiseOp, CWnd)

CWnd_PatternNoiseOp::CWnd_PatternNoiseOp()
{
	m_pstModelInfo = NULL;
	m_nTestItemCnt = 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_PatternNoiseOp::~CWnd_PatternNoiseOp()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_PatternNoiseOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_PatternNoiseOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_PatternNoiseOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_PN_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szPNStatic[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < EDT_PN_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < BTN_PN_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szPatternNoiseButton[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}
		
	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_PatternNoiseOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iIdxCnt  = PNOp_ItemNum;
	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	int iHeaderH = 40;
	int iList_H  = iHeaderH + iIdxCnt * 15;

	int iCtrl_W = iWidth / 5;
	int iCtrl_H = 25;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	int iSTWidth = iWidth / 4;
	int iSTHeight = 25;

	iTop = iMargin;

	iLeft = iMargin;
	m_st_Item[STI_PN_dBThr].MoveWindow(iLeft, iTop, iSTWidth, iCtrl_H);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_PN_dBThr].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iLeft = cx - iMargin - iCtrl_W;
	m_bn_Item[BTN_PN_TEST].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);


	iTop += iCtrl_H + iSpacing;

	iLeft = iMargin;
	m_st_Item[STI_PN_EnvThr].MoveWindow(iLeft, iTop, iSTWidth, iCtrl_H);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_PN_EnvThr].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iLeft = cx - iMargin - iCtrl_W;
	m_bn_Item[BTN_PN_RESET].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iLeft = iMargin;
	iTop += iCtrl_H + iSpacing;
	m_List.MoveWindow(iLeft, iTop, iWidth, iList_H);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_PatternNoiseOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_PatternNoiseOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	//CWnd::OnShowWindow(bShow, nStatus);

	//if (NULL == m_pstModelInfo)
	//	return;

	//if (TRUE == bShow)
	//{
	//	m_pstModelInfo->nTestMode = TIID_PatternNoise;
	//	m_pstModelInfo->nTestCnt = m_nTestItemCnt;
	//	m_pstModelInfo->nPicItem = PIC_PatternNoise;
	//}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_PatternNoiseOp::OnRangeBtnCtrl(UINT nID)
{
	//UINT nIdex = nID - IDC_BTN_ITEM;

	//switch (nIdex)
	//{
	//case BTN_PN_TEST:
	//	GetOwner()->SendMessage(WM_MANUAL_TEST, m_nTestItemCnt, TIID_PatternNoise);
	//	break;
	//case BTN_PN_RESET:
	//	GetOwner()->SendMessage(WM_OPTDATA_RESET, 0, 0);
	//	break;
	//default:
	//	break;
	//}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_PatternNoiseOp::SetUpdateData()
{
	//if (m_pstModelInfo == NULL)
	//	return;

	//m_List.SetPtr_Brightenss(&m_pstModelInfo->stPatternNoise[m_nTestItemCnt]);
	//m_List.SetImageSize(m_pstModelInfo->dwWidth, m_pstModelInfo->dwHeight);
	//m_List.InsertFullData();

	//CString strValue;
	//strValue.Format(_T("%.2f"), m_pstModelInfo->stPatternNoise[m_nTestItemCnt].stPatternNoiseOpt.dThr_dB);
	//m_ed_Item[EDT_PN_dBThr].SetWindowText(strValue);

	//strValue.Format(_T("%.2f"), m_pstModelInfo->stPatternNoise[m_nTestItemCnt].stPatternNoiseOpt.dThr_Env);
	//m_ed_Item[EDT_PN_EnvThr].SetWindowText(strValue);
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/14 - 18:05
// Desc.		:
//=============================================================================
void CWnd_PatternNoiseOp::GetUpdateData()
{
	//if (m_pstModelInfo == NULL)
	//	return;

	//m_List.GetCellData();

	//CString strValue;
	//m_ed_Item[EDT_PN_dBThr].GetWindowText(strValue);
	//m_pstModelInfo->stPatternNoise[m_nTestItemCnt].stPatternNoiseOpt.dThr_dB = _ttof(strValue);

	//m_ed_Item[EDT_PN_EnvThr].GetWindowText(strValue);
	//m_pstModelInfo->stPatternNoise[m_nTestItemCnt].stPatternNoiseOpt.dThr_Env = _ttof(strValue);
}