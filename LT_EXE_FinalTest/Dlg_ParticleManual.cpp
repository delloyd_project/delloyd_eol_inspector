// Dlg_ParticleManual.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Dlg_ParticleManual.h"
#include "afxdialogex.h"


CWinThread* pThread_Particle = NULL;

static volatile bool isThreadRunning_Particle;

UINT MyThread_Particle(LPVOID ipParam)
{
	CDlg_ParticleManual* pClass = (CDlg_ParticleManual*)ipParam;
	int iReturn = pClass->ThreadFunction();
	return 0L;
}

#define		IDC_WND_PARTICLE_MANUAL	1000
#define		IDC_BN_OK				2000
#define		IDC_BN_NG				3000

// CDlg_ParticleManual 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlg_ParticleManual, CDialogEx)

CDlg_ParticleManual::CDlg_ParticleManual(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_ParticleManual::IDD, pParent)
{
	m_nResult = 0;
	m_pDevice = NULL;

	VERIFY(m_font.CreateFont(
		30,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	for (UINT nIdx = 0; nIdx < DI_NotUseBit_Max; nIdx++)
		m_bFlag_Butten[nIdx] = FALSE;
}

CDlg_ParticleManual::~CDlg_ParticleManual()
{
	isThreadRunning_Particle = false;
	DestroyThread();
	KillTimer(99);
}

void CDlg_ParticleManual::CreateThread(UINT _method)
{
	if (pThread_Particle != NULL)
	{
		return;
	}

	pThread_Particle = AfxBeginThread(MyThread_Particle, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);

	if (pThread_Particle == NULL)
	{
		//cout<<"Fail to create camera thread!!";
	}

	pThread_Particle->m_bAutoDelete = FALSE;
	pThread_Particle->ResumeThread();
}

bool CDlg_ParticleManual::DestroyThread()
{
	if (NULL != pThread_Particle)
	{
		DWORD dwResult = ::WaitForSingleObject(pThread_Particle->m_hThread, INFINITE);

		if (dwResult == WAIT_TIMEOUT)
		{
			//cout<<"time out!"<<endl;
		}
		else if (dwResult == WAIT_OBJECT_0)
		{
			//cout<<"Thread END"<<endl;
		}

		delete pThread_Particle;

		pThread_Particle = NULL;
	}
	return true;
}

int CDlg_ParticleManual::ThreadFunction()
{
	while (isThreadRunning_Particle)
	{
		if (!isThreadRunning_Particle)
			break;

		Sleep(33);

		if (IsCameraConnect(m_nViewChannel, m_pModelinfo->nGrabType))
		{
			LPBYTE pFrameImage = NULL;
			IplImage *pImage = NULL;

			DWORD dwWidth = 0;
			DWORD dwHeight = 0;
			UINT nChannel = 0;

			pFrameImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);

			if (pFrameImage != NULL)
			{
				pImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

				if (pImage != NULL)
				{
// 					if (m_pModelinfo->nGrabType == GrabType_LVDS)
// 					{
// 						for (int y = 0; y < dwHeight; y++)
// 						{
// 							if (y < dwHeight - 1)	// 그래버 데드라인 4 : 제거
// 							{
// 								for (int x = 0; x < dwWidth; x++)
// 								{
// 									pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 0];
// 									pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 1];
// 									pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 2];
// 								}
// 							}
// 							else
// 							{
// 								for (int x = 0; x < dwWidth; x++)
// 								{
// 									pImage->imageData[y * pImage->widthStep + x * 3 + 0] = 0;
// 									pImage->imageData[y * pImage->widthStep + x * 3 + 1] = 0;
// 									pImage->imageData[y * pImage->widthStep + x * 3 + 2] = 0;
// 								}
// 							}
// 
// 						}
// 					}
					if (m_pModelinfo->nGrabType == GrabType_NTSC)
					{
						for (int y = 0; y < dwHeight; y++)
						{
							for (int x = 0; x < dwWidth; x++)
							{
								pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 0];
								pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 1];
								pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 2];
							}
						}
					}

					m_wndVideoView.SetFrameImageBuffer(pImage);
					cvReleaseImage(&pImage);
				}
			}
		}
	}

	return 0;
}

void CDlg_ParticleManual::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}



BEGIN_MESSAGE_MAP(CDlg_ParticleManual, CDialogEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BN_OK, OnBnClickedBnOK)
	ON_BN_CLICKED(IDC_BN_NG, OnBnClickedBnNG)
	ON_WM_TIMER()

	ON_MESSAGE(WM_RECV_MAIN_BRD_ACK, OnRecvMainBrd)

	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CDlg_ParticleManual 메시지 처리기입니다.


int CDlg_ParticleManual::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_wndVideoView.SetOwner(GetParent());
	m_wndVideoView.SetModelInfo(m_pModelinfo);
	m_wndVideoView.Create(NULL, _T(""), dwStyle, rectDummy, this, IDC_WND_PARTICLE_MANUAL);

	m_bn_OK.Create(_T("Pass"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_OK);
	m_bn_NG.Create(_T("Fail"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_NG);

	m_bn_OK.SetMouseCursorHand();
	m_bn_NG.SetMouseCursorHand();

	m_bn_OK.SetFont(&m_font);
	m_bn_NG.SetFont(&m_font);

	SetTimer(99, 100, NULL);
	isThreadRunning_Particle = true;
	CreateThread(isThreadRunning_Particle);

	return 0;
}


void CDlg_ParticleManual::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing = 5;
	int iCateSpacing = 10;
	int iMagin = 10;
	int iLeft = iMagin;
	int iTop = iMagin;
	int iWidth = cx - (iMagin * 2);
	int iHeight = cy - (iMagin * 2);

	m_wndVideoView.MoveWindow(iLeft, iTop, iWidth, iHeight * 5 / 6);

	iTop += iHeight * 5 / 6 + iSpacing;
	m_bn_OK.MoveWindow(iLeft, iTop, iWidth / 2, iHeight / 6);
	iLeft += iWidth / 2;
	m_bn_NG.MoveWindow(iLeft, iTop, iWidth / 2, iHeight / 6);

}


void CDlg_ParticleManual::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	lpMMI->ptMaxTrackSize.x = 1000;
	lpMMI->ptMaxTrackSize.y = 800;

	lpMMI->ptMinTrackSize.x = 1000;
	lpMMI->ptMinTrackSize.y = 800;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}


BOOL CDlg_ParticleManual::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreCreateWindow(cs);
}


BOOL CDlg_ParticleManual::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_RETURN)
	{
		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN
		&& pMsg->wParam == VK_ESCAPE)
	{
		// 여기에 ESC키 기능 작성       
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CDlg_ParticleManual::OnBnClickedBnOK()
{
	KillTimer(99);

	isThreadRunning_Particle = false;
	DestroyThread();
	Sleep(500);

	OnOK();
}


void CDlg_ParticleManual::OnBnClickedBnNG()
{
	KillTimer(99);

	isThreadRunning_Particle = false;
	DestroyThread();
	Sleep(500);

	OnCancel();
}

LRESULT CDlg_ParticleManual::OnRecvMainBrd(WPARAM wParam, LPARAM lParam)
{
	// Start, Stop Button Recieve..
	LPCTSTR szData = (LPCTSTR)wParam;

	CStringA strData;
	strData.Format("%s", szData);

	if (strData.Left(4) == "!B10")
		OnBnClickedBnOK();

	if (strData.Left(4) == "!B01")
		OnBnClickedBnNG();

	return 0;
}

void CDlg_ParticleManual::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	LPBYTE pFrameImage = NULL;
	IplImage *pImage = NULL;

	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	UINT nChannel = 0;

	switch (nIDEvent)
	{
	case 99:

		if (m_pDevice->DigitalIOCtrl.AXTState() == TRUE)
		{
			if (TRUE == m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_StartBtn))
			{
				if (m_bFlag_Butten[DI_StartBtn] == FALSE)
				{
					m_bFlag_Butten[DI_StartBtn] = TRUE;

					OnBnClickedBnOK();

					m_bFlag_Butten[DI_StartBtn] = FALSE;
				}
			}

			if (TRUE == m_pDevice->DigitalIOCtrl.Get_DI_Status(DI_StopBtn))
			{
				if (m_bFlag_Butten[DI_StopBtn] == FALSE)
				{
					m_bFlag_Butten[DI_StopBtn] = TRUE;

					OnBnClickedBnNG();

					m_bFlag_Butten[DI_StopBtn] = FALSE;
				}
			}
		}
		break;

	case 100:

		if (IsCameraConnect(m_nViewChannel, m_pModelinfo->nGrabType))
		{
			pFrameImage = GetImageBuffer(m_nViewChannel, m_pModelinfo->nGrabType, dwWidth, dwHeight, nChannel);

			if (pFrameImage != NULL)
			{
				pImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

			 if (m_pModelinfo->nGrabType == GrabType_NTSC)
				{
					for (int y = 0; y < dwHeight; y++)
					{
						for (int x = 0; x < dwWidth; x++)
						{
							pImage->imageData[y * pImage->widthStep + x * 3 + 0] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 0];
							pImage->imageData[y * pImage->widthStep + x * 3 + 1] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 1];
							pImage->imageData[y * pImage->widthStep + x * 3 + 2] = pFrameImage[y * dwWidth * nChannel + x * nChannel + 2];
						}
					}
				}

				m_wndVideoView.SetFrameImageBuffer(pImage);

				cvReleaseImage(&pImage);
			}
		}
		
		break;
	
	default:
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDlg_ParticleManual::IsCameraConnect(__in UINT nCh, __in UINT nGrabType)
{
	switch (nGrabType)
	{
	case GrabType_NTSC:

		if (m_pDevice->Cat3DCtrl.IsConnect() == FALSE)
			return FALSE;

		if (m_pDevice->Cat3DCtrl.GetStatus(nCh) == FALSE)
			return FALSE;

		break;

	default:
		break;
	}

	return TRUE;
}


LPBYTE CDlg_ParticleManual::GetImageBuffer(__in UINT nViewCh, __in UINT nGrabType, __out DWORD& dwWidth, __out DWORD& dwHeight, __out UINT& nChannel)
{
	// 카메라 연결 상태
	if (IsCameraConnect(nViewCh, nGrabType) == FALSE)
		return NULL;

	// Result Buffer
	LPBYTE pRGBDATA = NULL;

	// NTSC Buffer
	ST_VideoRGB_NTSC* pNTSCRGB = NULL;

	if (nGrabType == GrabType_NTSC)
	{
		pNTSCRGB = m_pDevice->Cat3DCtrl.GetRecvRGBData(nViewCh);
		pRGBDATA = (LPBYTE)((m_pDevice->Cat3DCtrl.GetRecvRGBData(nViewCh))->m_pMatRGB[0]);

		dwWidth = pNTSCRGB->m_dwWidth;
		dwHeight = pNTSCRGB->m_dwHeight;
		nChannel = 4;
	}
	
	return pRGBDATA;
}


void CDlg_ParticleManual::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	isThreadRunning_Particle = false;
	DestroyThread();

	KillTimer(99);
	CDialogEx::OnClose();
}
