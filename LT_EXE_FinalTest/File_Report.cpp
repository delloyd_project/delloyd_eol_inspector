﻿//*****************************************************************************
// Filename	: 	File_Report.cpp
// Created	:	2016/8/5 - 17:18
// Modified	:	2016/8/5 - 17:18
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "File_Report.h"
#include "CommonFunction.h"

CFile_Report::CFile_Report()
{
}


CFile_Report::~CFile_Report()
{
}

//=============================================================================
// Method		: Make_Worklist
// Access		: protected  
// Returns		: CString
// Parameter	: __in const ST_Worklist* pstWorklist
// Qualifier	:
// Last Update	: 2016/8/8 - 17:27
// Desc.		:
//=============================================================================
CString CFile_Report::Make_Worklist(__in const ST_Worklist* pstWorklist)
{
	CString szLine;
	CString szItem;
	CString szHeader;

	// Header
	szHeader = _T(",\r\n");
	szLine += szHeader;

	// Lot Name
// 	szItem = pstWorklist->szLotName;
// 	szLine += szItem + _T(",");

	szLine += _T("\r\n");

	return szLine;
}

//=============================================================================
// Method		: Save_Worklist
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_Worklist* pstWorklist
// Qualifier	:
// Last Update	: 2016/8/8 - 17:58
// Desc.		:
//=============================================================================
BOOL CFile_Report::Save_Worklist(__in LPCTSTR szPath, __in const ST_Worklist* pstWorklist)
{
	BOOL bReturn = TRUE;
	CString szFullPath;
	CString szBuff = Make_Worklist(pstWorklist);

	USES_CONVERSION;
	CStringA szMBBuff = CT2A(szBuff.GetBuffer(0));

	// 파일 패스
	// Report 경로// Lot_시간_.csv
// 	szFullPath.Format(_T("%s%s_%04d%02d%02d%02d%02d%02d.csv"),	szPath, 
// 																pstWorklist->szLotName, 
// 																pstWorklist->tmStartTime.wYear,
// 																pstWorklist->tmStartTime.wMonth,
// 																pstWorklist->tmStartTime.wDay,
// 																pstWorklist->tmStartTime.wHour,
// 																pstWorklist->tmStartTime.wMinute,
// 																pstWorklist->tmStartTime.wSecond);

	CFile File;
	CFileException e;

	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

// 		if (bReturn)
// 		{
// 			WORD mode = 0xFEFF;
// 			File.Write(&mode, sizeof(WORD));
// 		}
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}
	}

	File.SeekToEnd();	
	File.Write(szMBBuff.GetBuffer(), szMBBuff.GetLength());
	File.Flush();
	szBuff.ReleaseBuffer();

	File.Close();

	return bReturn;
}


//=============================================================================
// Method		: Save_LotInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in ST_LOTInfo * pstLotInfo
// Qualifier	:
// Last Update	: 2016/8/11 - 14:33
// Desc.		:
//=============================================================================
BOOL CFile_Report::Save_LotInfo(__in LPCTSTR szPath, __in ST_LOTInfo* pstLotInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstLotInfo)
		return FALSE;

	::DeleteFile(szPath);

	CString strFullPath;
	CString	strAppName;
	CString	strValue;

	strFullPath.Format(_T("%sLotInfo.ini"), szPath);
	
	WritePrivateProfileString(_T("Info"), _T("LotName"), pstLotInfo->szLotName, strFullPath);

	WritePrivateProfileString(_T("Info"), _T("Operator"), pstLotInfo->szOperator, strFullPath);

	strValue = SystemTimeToFormatString(pstLotInfo->StartTime);
	WritePrivateProfileString(_T("Info"), _T("StartTime"), strValue, strFullPath);


	return TRUE;
}

//=============================================================================
// Method		: Load_LotInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_LOTInfo & stLotInfo
// Qualifier	:
// Last Update	: 2016/8/11 - 14:33
// Desc.		:
//=============================================================================
BOOL CFile_Report::Load_LotInfo(__in LPCTSTR szPath, __out ST_LOTInfo& stLotInfo)
{
	CString strFullPath;
	CString	strAppName;

	strFullPath.Format(_T("%sLotInfo.ini"), szPath);

	TCHAR   inBuff[1024] = { 0, };

	GetPrivateProfileString(_T("Info"), _T("LotName"), _T(""), inBuff, 1024, strFullPath);
	stLotInfo.szLotName = inBuff;

	GetPrivateProfileString(_T("Info"), _T("Operator"), _T(""), inBuff, 1024, strFullPath);
	stLotInfo.szOperator = inBuff;

	GetPrivateProfileString(_T("Info"), _T("StartTime"), _T(""), inBuff, 1024, strFullPath);
	FormatStringToSystemTime(inBuff, stLotInfo.StartTime);
	

	return TRUE;
}

//=============================================================================
// Method		: SaveYield_LOT
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFullPath
// Parameter	: __in const ST_Yield * pYieldInfo
// Qualifier	:
// Last Update	: 2016/8/11 - 14:21
// Desc.		:
//=============================================================================
BOOL CFile_Report::SaveYield_LOT(__in LPCTSTR szFullPath, __in const ST_Yield* pYieldInfo)
{
	if (NULL == szFullPath)
		return FALSE;

	if (NULL == pYieldInfo)
		return FALSE;

	CString	strAppName;
	CString	strValue;

	strValue.Format(_T("%d"), pYieldInfo->dwTotal);
	WritePrivateProfileString(_T("Yield"), _T("TOTAL"), strValue, szFullPath);

	strValue.Format(_T("%d"), pYieldInfo->dwPass);
	WritePrivateProfileString(_T("Yield"), _T("PASS"), strValue, szFullPath);

	strValue.Format(_T("%d"), pYieldInfo->dwFail);
	WritePrivateProfileString(_T("Yield"), _T("FAIL"), strValue, szFullPath);

	strValue.Format(_T("%6.2f"), pYieldInfo->fYield);
	WritePrivateProfileString(_T("Yield"), _T("Yield"), strValue, szFullPath);
	return TRUE;
}

//=============================================================================
// Method		: LoadYield_LOT
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFullPath
// Parameter	: __out ST_Yield & stYieldInfo
// Qualifier	:
// Last Update	: 2016/8/11 - 14:21
// Desc.		:
//=============================================================================
BOOL CFile_Report::LoadYield_LOT(__in LPCTSTR szFullPath, __out ST_Yield& stYieldInfo)
{
	// Path + Yield.ini
	CString strFullPath;
	CString	strAppName;

	stYieldInfo.dwTotal = GetPrivateProfileInt(_T("Yield"), _T("TOTAL"), 0, szFullPath);

	stYieldInfo.dwPass = GetPrivateProfileInt(_T("Yield"), _T("PASS"), 0, szFullPath);

	stYieldInfo.dwFail = GetPrivateProfileInt(_T("Yield"), _T("FAIL"), 0, szFullPath);

	stYieldInfo.ComputeYield();

	return TRUE;
}

BOOL CFile_Report::SaveMES_Report(__in LPCTSTR szFullPath, __in const ST_CamInfo* pstCamInfo)
{
	if (szFullPath == NULL)
	{
		return FALSE;
	}

	CStdioFile		File;
	CFileFind		FileFind;
	CFileException	e;

	CString str, strOut;
	CString strMESFilePath, strMESFolderPath;

	strMESFolderPath.Format(_T("%s\\Quality_Test"), szFullPath);
	MakeDirectory(strMESFolderPath);

	strMESFilePath.Format(_T("%s\\EDC_%s.idx"), strMESFolderPath, pstCamInfo->szDay);

	if (!FileFind.FindFile(strMESFilePath))
	{
		if (!File.Open((LPCTSTR)strMESFilePath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		str = _T("file_path,");
		strOut += str;

		str = _T("recode_no,");
		strOut += str;

		str = _T("update_time,\n");
		strOut += str;
	}
	else
	{
		if (!File.Open((LPCTSTR)strMESFilePath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}
	}

	File.SeekToEnd();

	strOut += pstCamInfo->szReportFilePath;
	strOut += _T(",");

	strOut += pstCamInfo->szIndex;
	strOut += _T(",");

	str.Format(_T("%s %s"), pstCamInfo->szDay, pstCamInfo->szTime);
	strOut += str;
	strOut += _T(",");
	strOut += _T("\n");

	File.WriteString((LPCTSTR)strOut);
	File.Close();
	
	return TRUE;
}
