﻿#ifndef List_Work_FFT_h__
#define List_Work_FFT_h__

#pragma once

#include "Def_Test.h"

typedef enum enListNum_FFT_Worklist
{
	FFT_W_Recode,
	FFT_W_Time,
	FFT_W_Equipment,
	FFT_W_Model,
	FFT_W_SWVersion,
	FFT_W_CameraType,
	FFT_W_LOTNum,
	FFT_W_Barcode,
	FFT_W_Operator,
	FFT_W_Result,
	FFT_W_Amp,
	FFT_W_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_FFT_Worklist[] =
{
	_T("No"),
	_T("Time"),
	_T("Equipment"),
	_T("Model"),
	_T("SW Version"),
	_T("Camera Type"),
	_T("LOTNum"),
	_T("Barcode"),
	_T("Operator"),
	_T("Result"),
	_T("Amplitude"),
	NULL
};

const int	iListAglin_FFT_Worklist[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_FFT_Worklist[] =
{
	40,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
	80,
};

// CList_Work_FFT

class CList_Work_FFT : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Work_FFT)

public:
	CList_Work_FFT();
	virtual ~CList_Work_FFT();

	void InitHeader		();
	void InsertFullData	(__in const ST_CamInfo* pstCamInfo);

	void SetRectRow		(UINT nRow, __in const ST_CamInfo* pstCamInfo);
	void GetData		(UINT nRow, UINT &DataNum, CString *Data);

	UINT m_nTestIndex;
	void SetTestIndex(__in UINT nTestIndex)
	{
		m_nTestIndex = nTestIndex;
	}

protected:

	CFont	m_Font;

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // List_Work_FFT_h__
