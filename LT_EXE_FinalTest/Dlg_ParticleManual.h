#pragma once
#include "resource.h"

#include "Def_TestDevice.h"
#include "Wnd_VideoView.h"

// CDlg_ParticleManual 대화 상자입니다.

class CDlg_ParticleManual : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_ParticleManual)

public:
	CDlg_ParticleManual(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_ParticleManual();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_PARTICLE_MANUAL };

	void CreateThread(UINT _method);
	bool DestroyThread();
	int	 ThreadFunction();

	UINT m_nResult;
	UINT m_nViewChannel;
	BOOL m_bFlag_Butten[DI_NotUseBit_Max];

	ST_Device* m_pDevice;
	ST_ModelInfo* m_pModelinfo;

	UINT GetResult()
	{
		return m_nResult;
	};

	void SetPtr_Device(__in ST_Device* pDevice)
	{
		if (pDevice == NULL)
			return;

		m_pDevice = pDevice;
	};

	void SetPtr_Modelinfo(__in ST_ModelInfo* pModelinfo)
	{
		if (pModelinfo == NULL)
			return;

		m_pModelinfo = pModelinfo;
	};

	void SetVideoChannel(__in UINT nCH)
	{
		m_nViewChannel = nCH;
	};

	CWnd_VideoView m_wndVideoView;

protected:
	CFont m_font;

	CMFCButton m_bn_OK;
	CMFCButton m_bn_NG;

	BOOL IsCameraConnect(__in UINT nCh, __in UINT nGrabType);
	LPBYTE GetImageBuffer(__in UINT nViewCh, __in UINT nGrabType, __out DWORD& dwWidth, __out DWORD& dwHeight, __out UINT& nChannel);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg void OnBnClickedBnOK();
	afx_msg void OnBnClickedBnNG();

	// 시리얼 통신 데이터
	afx_msg LRESULT	OnRecvMainBrd(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnClose();
};
