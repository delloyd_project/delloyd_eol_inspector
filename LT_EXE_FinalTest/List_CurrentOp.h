﻿#ifndef List_CurrentOp_h__
#define List_CurrentOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_CurrentOp
{
	CurOp_Channel,
	CurOp_Min,
	CurOp_Max,
	CurOp_Offset,
	CurOp_MaxCol,
};

// 헤더
static const LPCTSTR g_lpszHeader_CurrOp[] =
{
	_T("CH"),
	_T("Min [ mA ]"),
	_T("Max [ mA ]"),
	_T("Offset [ mA ]"),
	NULL
};

typedef enum enListItemNum_Current
{
	CurOp_Site1,
	CurOp_ItemNum,
};

static const LPCTSTR g_lpszItem_CurrOp[] =
{
	_T("1"),
	NULL
};

const int	iListAglin_CurOp[] =
{
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_CurrOp[] =
{
	100,
	130,
	130,
	130,
};
// List_CurrentInfo

class CList_CurrentOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_CurrentOp)

public:
	CList_CurrentOp();
	virtual ~CList_CurrentOp();

	void InitHeader();
	void InsertFullData();
	void SetRectRow(UINT nRow);
	void GetCellData();

	void SetPtr_Current(ST_LT_TI_Current *pstCurrent)
	{
		if (pstCurrent == NULL)
			return;

		m_pstCurrent = pstCurrent;
	};

protected:

	ST_LT_TI_Current*	m_pstCurrent;

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	UINT		m_nEditCol;
	UINT		m_nEditRow;

	BOOL		UpdateCellData			(UINT nRow, UINT nCol, int  iValue);
	BOOL		UpdateCellData_double	(UINT nRow, UINT nCol, double dValue);

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick			(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk			(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnEnKillFocusEdit	();

	afx_msg BOOL	OnMouseWheel		(UINT nFlags, short zDelta, CPoint pt);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()
};

#endif // List_CurrentInfo_h__
