﻿#ifndef Wnd_BrightnessOp_h__
#define Wnd_BrightnessOp_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "List_BrightnessOp.h"
#include "Def_DataStruct_Cm.h"

// CWnd_BrightnessOp

enum enBrightnessStatic
{
	STI_BR_OPT = 0,
	STI_BR_MAX,
};

static LPCTSTR	g_szBrightnessStatic[] =
{
	_T("OPTION LIST"),
	NULL
};

enum enBrightnessButton
{
	BTN_BR_TEST = 0,
	BTN_BR_MAX,
};

static LPCTSTR	g_szBrightnessButton[] =
{
	_T("TEST"),
	NULL
};

class CWnd_BrightnessOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BrightnessOp)

public:
	CWnd_BrightnessOp();
	virtual ~CWnd_BrightnessOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo			*m_pstModelInfo;
	CList_BrightnessOp		m_List;

	CVGStatic			m_st_Item[STI_BR_MAX];
	CFont				m_font;
	CButton				m_bn_Item[BTN_BR_MAX];

	// 검사 항목이 다수 인경우
	UINT				m_nTestItemCnt;

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BrightnessOp_h__
