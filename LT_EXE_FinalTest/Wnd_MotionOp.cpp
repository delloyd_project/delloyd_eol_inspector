﻿// Wnd_MotionOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_MotionOp.h"

// CWnd_MotionOp

IMPLEMENT_DYNAMIC(CWnd_MotionOp, CWnd)

typedef enum MotionOp_ID
{
	IDC_MO_CB_PULSE_DATA = 1000,
	IDC_MO_CB_SERVO_DATA,
	IDC_MO_CB_POSITION_DATA,
	IDC_MO_CB_ENC_DATA,
	IDC_MO_CB_ALARM_DATA,
	IDC_MO_CB_HOME_DATA,
	IDC_MO_CB_HLIMIT_DATA,
	IDC_MO_CB_LLIMIT_DATA,
	IDC_MO_CB_USE_DATA,
	IDC_MO_BN_MO_SETTING,
	IDC_MO_BN_MO_SAVE,
	IDC_MO_ED_MO_NUMBER,
	IDC_MO_ED_MO_NAME,
	IDC_MO_ED_MO_VAL,
	IDC_MO_ED_MO_ACC,
	IDC_MO_MO_MAXNUM,
};

CWnd_MotionOp::CWnd_MotionOp()
{
	m_pstDevice				= NULL;
	m_nAxis					= 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_MotionOp::~CWnd_MotionOp()
{
	m_font.DeleteObject		();
}

BEGIN_MESSAGE_MAP(CWnd_MotionOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_MO_BN_MO_SAVE,	OnBnClickedBnSave	)
	ON_BN_CLICKED(IDC_MO_BN_MO_SETTING, OnBnClickedBnSetting)
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/28 - 17:02
// Desc.		:
//=============================================================================
int CWnd_MotionOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwEtStyle = WS_VISIBLE | WS_BORDER | ES_CENTER;
	DWORD dwStyle	= WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Name.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Name.SetBackColor_COLORREF(RGB(33, 73, 125));
	m_st_Name.SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
	m_st_Name.SetFont_Gdip(L"Arial", 12.0F);
	m_st_Name.Create(_T("MOTOR PARAMETER SETTING"), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = ST_MO_PULSE; nIdx < ST_MO_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 10.0F);
		m_st_Item[nIdx].Create(g_szMotionOpName[nIdx], dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < CB_MO_MAXNUM; nIdx++)
	{
		m_cb_Item[nIdx].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_MO_CB_PULSE_DATA + nIdx);
		m_cb_Item[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; NULL != g_szMotorOpPuls[nIdx]; nIdx++)
		m_cb_Item[CB_MO_PULSE].AddString(g_szMotorOpPuls[nIdx]);

	for (UINT nIdx = 0; NULL != g_szMotorOpEnc[nIdx]; nIdx++)
		m_cb_Item[CB_MO_ENC].AddString(g_szMotorOpEnc[nIdx]);

	for (UINT nIdx = 0; NULL != g_szMotorOpServo[nIdx]; nIdx++)
		m_cb_Item[CB_MO_SERVO].AddString(g_szMotorOpServo[nIdx]);

	for (UINT nIdx = 0; NULL != g_szMotorOpUse[nIdx]; nIdx++)
		m_cb_Item[CB_MO_USED].AddString(g_szMotorOpUse[nIdx]);

	for (UINT nIdx = 0; NULL != g_szMotorOpLevel[nIdx]; nIdx++)
	{
		m_cb_Item[CB_MO_HOME].AddString(g_szMotorOpLevel[nIdx]);
		m_cb_Item[CB_MO_ALRAM].AddString(g_szMotorOpLevel[nIdx]);
		m_cb_Item[CB_MO_HLIMIT].AddString(g_szMotorOpLevel[nIdx]);
		m_cb_Item[CB_MO_LLIMIT].AddString(g_szMotorOpLevel[nIdx]);
		m_cb_Item[CB_MO_POSITION].AddString(g_szMotorOpLevel[nIdx]);
		m_cb_Item[CB_MO_EMO].AddString(g_szMotorOpLevel[nIdx]);
	}

	for (UINT nIdx = 0; nIdx < BN_MO_MAXNUM; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szMotionOpButton[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_MO_BN_MO_SETTING + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; nIdx < ED_MO_MAXNUM; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwEtStyle, rectDummy, this, IDC_MO_ED_MO_NUMBER + nIdx);
		m_ed_Item[nIdx].SetWindowText(_T("0"));
	}

	m_ed_Item[ED_MO_VAL].SetValidChars(_T("0123456789.-"));
	m_ed_Item[ED_MO_ACC].SetValidChars(_T("0123456789.-"));
	m_ed_Item[ED_MO_NUMBER].SetValidChars(_T("0123456789.-"));
	m_ed_Item[ED_MO_UINT_PER].SetValidChars(_T("0123456789."));
	m_ed_Item[ED_MO_PULSE_PER].SetValidChars(_T("0123456789."));

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
void CWnd_MotionOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMargin  = 5;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth	 = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	int iEdWidth = (iWidth - iSpacing * 6) / 6;
	int iBnWidth = (iWidth - iSpacing * 3) / 3;

	int iEditHeight	= 23;

	int iTitleH = 35;
	m_st_Name.MoveWindow(0, iTop, cx, iTitleH);

	iTop += iTitleH + iSpacing;
	iLeft = iMargin;

	for (UINT nIdx = 0; nIdx < ST_MO_MAXNUM; nIdx++)
	{
		if (0 == nIdx % 3 && 0 != nIdx)
		{
			iLeft = iMargin;
			iTop += iEditHeight + iSpacing;
		}

		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
		iLeft += iEdWidth + iSpacing;

		switch (nIdx)
		{
		case ST_MO_PULSE:
			m_cb_Item[CB_MO_PULSE].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_SERVO:
			m_cb_Item[CB_MO_SERVO].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_POSITION:
			m_cb_Item[CB_MO_POSITION].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_ENC:
			m_cb_Item[CB_MO_ENC].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_ALARAM:
			m_cb_Item[CB_MO_ALRAM].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_HOME:
			m_cb_Item[CB_MO_HOME].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_HLIMIT:
			m_cb_Item[CB_MO_HLIMIT].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_LLIMIT:
			m_cb_Item[CB_MO_LLIMIT].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_EMO:
			m_cb_Item[CB_MO_EMO].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_UNIT_PER:
			m_ed_Item[ED_MO_UINT_PER].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_PULSE_PER:
			m_ed_Item[ED_MO_PULSE_PER].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_USE:
			m_cb_Item[CB_MO_USED].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_NUMBER:
			m_ed_Item[ED_MO_NUMBER].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_NAME:
			m_ed_Item[ED_MO_NAME].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_VAL:
			m_ed_Item[ED_MO_VAL].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		case ST_MO_ACC:
			m_ed_Item[ED_MO_ACC].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
			break;
		}

		iLeft += iEdWidth + iSpacing;
	}

	//iLeft = cx - iEdWidth - iSpacing;
	iLeft += iEdWidth + iSpacing;
	iLeft += iEdWidth + iSpacing;
	m_bn_Item[BN_MO_SAVE].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);
	iLeft += iEdWidth + iSpacing;
	m_bn_Item[BN_MO_SETTING].MoveWindow(iLeft, iTop, iEdWidth, iEditHeight);

	//iLeft -= (iEdWidth + iSpacing);
	

}

//=============================================================================
// Method		: OnBnClickedBnSave
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 9:56
// Desc.		:
//=============================================================================
void CWnd_MotionOp::OnBnClickedBnSave()
{
	// UI 데이터 수집
	GetUpdateData();

	// 수집된 데이터 셋팅
	m_pstDevice->SetMotionSetting(m_nAxis);

	// 수집된 데이터 저장
	m_pstDevice->SaveMotionInfo();

	SetUpdateData();
	GetOwner()->SendNotifyMessage(WM_UPDATA_AXIS, 0, 0);
}

//=============================================================================
// Method		: OnBnClickedBnSetting
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/4/6 - 10:20
// Desc.		:
//=============================================================================
void CWnd_MotionOp::OnBnClickedBnSetting()
{
	// UI 데이터 수집
	GetUpdateData();

	// 수집된 데이터 셋팅
	m_pstDevice->SetMotionSetting(m_nAxis);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
BOOL CWnd_MotionOp::PreTranslateMessage(MSG* pMsg)
{
	CString str;
	long lAxis = 0;

	m_ed_Item[ED_MO_NUMBER].GetWindowText(str);
	lAxis = _ttol(str);

	if (lAxis > m_pstDevice->m_AllMotorData.MotorInfo.lMaxAxisCnt - 1)
	{
		lAxis = m_pstDevice->m_AllMotorData.MotorInfo.lMaxAxisCnt - 1;

		str.Format(_T("%d"), lAxis);
		m_ed_Item[ED_MO_NUMBER].SetWindowText(str);
	}

	switch (pMsg->message)
	{
	case WM_KEYDOWN:
		if ((::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 86)) // Ctrl + V
		{
			CWnd* pWnd = CWnd::FromHandle(pMsg->hwnd);
			pWnd->SetWindowText(_T(""));
			pMsg->message = WM_PASTE;
		}

		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 67)  // Ctrl + C
			pMsg->message = WM_COPY;

// 		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 90)  // Ctrl + Z
// 			pMsg->message = WM_UNDO;

		break;
	}

	return CWnd::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/5 - 18:30
// Desc.		:
//=============================================================================
BOOL CWnd_MotionOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetSelectAxis
// Access		: public  
// Returns		: void
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/4/3 - 11:49
// Desc.		:
//=============================================================================
void CWnd_MotionOp::SetSelectAxis(UINT nAxis)
{
	m_nAxis = nAxis;
	SetUpdateData();
}

//=============================================================================
// Method		: SetUpdateData
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 10:29
// Desc.		:
//=============================================================================
void CWnd_MotionOp::SetUpdateData()
{
	if (m_pstDevice == NULL)
		return;

	if (m_pstDevice->m_AllMotorData.pMotionParam == NULL)
		return;

	CString strData;

	ST_MotionParam stMotorParam = m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis];
	
	// Puls Output
	m_cb_Item[CB_MO_PULSE].SetCurSel(stMotorParam.iAxisOutputMethod);

	// Servo Level
	m_cb_Item[CB_MO_SERVO].SetCurSel(stMotorParam.iAxisServoLevel);

	// In Position Level
	m_cb_Item[CB_MO_POSITION].SetCurSel(stMotorParam.iAxisInpositionLevel);

	// Enc Input Level
	m_cb_Item[CB_MO_ENC].SetCurSel(stMotorParam.iAxisEncInputMethod);

	// Alram Level
	m_cb_Item[CB_MO_ALRAM].SetCurSel(stMotorParam.iAxisAlarmLevel);

	// Home Level
	m_cb_Item[CB_MO_HOME].SetCurSel(stMotorParam.iAxisHomeSenLevel);

	// PosLimit Level
	m_cb_Item[CB_MO_HLIMIT].SetCurSel(stMotorParam.iAxisPosLimitSenLevel);

	// NegLimit Level
	m_cb_Item[CB_MO_LLIMIT].SetCurSel(stMotorParam.iAxisNegLimitSenLevel);

	// EMO Level
	m_cb_Item[CB_MO_EMO].SetCurSel(stMotorParam.iAxisEmergencyLevel);

	// USE AXIS
	m_cb_Item[CB_MO_USED].SetCurSel(stMotorParam.bAxisUse);

	// USE Number
	strData.Format(_T("%d"), stMotorParam.iAxisNum);
	m_ed_Item[ED_MO_NUMBER].SetWindowText(strData);

	// AXIS NAME
	m_ed_Item[ED_MO_NAME].SetWindowText(stMotorParam.szAxisName);

	// AXIS VAL
	strData.Format(_T("%.1f"), stMotorParam.dbVel);
	m_ed_Item[ED_MO_VAL].SetWindowText(strData);

	// AXIS ACC
	strData.Format(_T("%.1f"), stMotorParam.dbAcc);
	m_ed_Item[ED_MO_ACC].SetWindowText(strData);

	strData.Format(_T("%.1f"), stMotorParam.dbUnit);
	m_ed_Item[ED_MO_UINT_PER].SetWindowText(strData);

	strData.Format(_T("%.1f"), stMotorParam.dbPulse);
	m_ed_Item[ED_MO_PULSE_PER].SetWindowText(strData);
}

//=============================================================================
// Method		: GetUpdateData
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 10:41
// Desc.		:
//=============================================================================
void CWnd_MotionOp::GetUpdateData()
{
	if (m_pstDevice == NULL)
		return;

	CString strData;

	// Puls Output
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisOutputMethod = m_cb_Item[CB_MO_PULSE].GetCurSel();

	// Servo Level
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisServoLevel = m_cb_Item[CB_MO_SERVO].GetCurSel();

	// In Position Level
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisInpositionLevel = m_cb_Item[CB_MO_POSITION].GetCurSel();

	// Enc Input Level
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisEncInputMethod = m_cb_Item[CB_MO_ENC].GetCurSel();

	// Alram Level
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisAlarmLevel = m_cb_Item[CB_MO_ALRAM].GetCurSel();

	// Home Level
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisHomeSenLevel = m_cb_Item[CB_MO_HOME].GetCurSel();

	// PosLimit Level
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisPosLimitSenLevel = m_cb_Item[CB_MO_HLIMIT].GetCurSel();

	// NegLimit Level
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisNegLimitSenLevel = m_cb_Item[CB_MO_LLIMIT].GetCurSel();

	// EMO Level
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisEmergencyLevel = m_cb_Item[CB_MO_EMO].GetCurSel();

	// USE AXIS
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].bAxisUse = m_cb_Item[CB_MO_USED].GetCurSel();

	// USE Number
	m_ed_Item[ED_MO_NUMBER].GetWindowText(strData);
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].iAxisNum = _ttoi(strData);

	// Axis Name
	m_ed_Item[ED_MO_NAME].GetWindowText(strData);
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].szAxisName = strData;

	// AXIS VAL
	m_ed_Item[ED_MO_VAL].GetWindowText(strData);
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].dbVel = _ttoi(strData);

	// AXIS ACC
	m_ed_Item[ED_MO_ACC].GetWindowText(strData);
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].dbAcc = _ttoi(strData);

	m_ed_Item[ED_MO_UINT_PER].GetWindowText(strData);
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].dbUnit = _ttoi(strData);

	m_ed_Item[ED_MO_PULSE_PER].GetWindowText(strData);
	m_pstDevice->m_AllMotorData.pMotionParam[m_nAxis].dbPulse = _ttoi(strData);
}
