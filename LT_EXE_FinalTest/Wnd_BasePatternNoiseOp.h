﻿#ifndef Wnd_BasePatternNoiseOp_h__
#define Wnd_BasePatternNoiseOp_h__

#pragma once

#include "Wnd_PatternNoiseOp.h"

// CWnd_BasePatternNoiseOp

class CWnd_BasePatternNoiseOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BasePatternNoiseOp)

public:
	CWnd_BasePatternNoiseOp();
	virtual ~CWnd_BasePatternNoiseOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
//	CWnd_PatternNoiseOp m_wndPatternNoiseOp[TICnt_PatternNoise];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BasePatternNoiseOp_h__
