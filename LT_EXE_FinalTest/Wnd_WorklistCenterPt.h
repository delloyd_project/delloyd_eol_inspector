﻿//*****************************************************************************
// Filename	: Wnd_WorklistCenterPt.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistCenterPt_h__
#define Wnd_WorklistCenterPt_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_CenterPoint.h"

//=============================================================================
// Wnd_WorklistCenterPt
//=============================================================================
class CWnd_WorklistCenterPt : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistCenterPt)

public:
	CWnd_WorklistCenterPt();
	virtual ~CWnd_WorklistCenterPt();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

	CList_Work_CenterPoint			m_list_CenterPt[TICnt_CenterPoint];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistCenterPt_h__


