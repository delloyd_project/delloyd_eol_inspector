﻿#ifndef Wnd_ColorOp_h__
#define Wnd_ColorOp_h__

#pragma once

#include "VGStatic.h"
#include "List_ColorOp.h"
#include "Def_DataStruct.h"

// CWnd_ColorOp

enum enColorStatic
{
	STI_COR_AUTOSET,
	STI_COR_OPT,
	STI_COR_MAX,
};

static LPCTSTR	g_szColorStatic[] =
{
	_T("Spec Deviation"),
	_T("OPTION LIST"),
	NULL
};

enum enColorButton
{
	BTN_COR_AUTOSET,
	BTN_COR_TEST,
	BTN_COR_MAX,
};

static LPCTSTR	g_szColorButton[] =
{
	_T("AUTO SET"),
	_T("TEST"),
	NULL
};

enum enColorComobox
{
	CMB_COR_MAX = 1,
};

enum enColorEdit
{
	EDT_COR_AUTOSET,
	EDT_COR_MAX,
};
class CWnd_ColorOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_ColorOp)

public:
	CWnd_ColorOp();
	virtual ~CWnd_ColorOp();

	void SetUpdateData	();
	void GetUpdateData	();

	void	SetPtr_ModelInfo(ST_ModelInfo* pstModelInfo)
	{
		if (pstModelInfo == NULL)
			return;

		m_pstModelInfo = pstModelInfo;
	}

	void SetTestItemCount(UINT nTestItemCnt)
	{
		m_nTestItemCnt = nTestItemCnt;
	};

protected:

	ST_ModelInfo	*m_pstModelInfo;
	CList_ColorOp	 m_List;

	CFont			m_font;

	CVGStatic			m_st_Item[STI_COR_MAX];
	CButton				m_bn_Item[BTN_COR_MAX];
	CComboBox			m_cb_Item[CMB_COR_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_COR_MAX];
	
	// 검사 항목이 다수 인경우
	UINT			m_nTestItemCnt;

	DECLARE_MESSAGE_MAP()

	afx_msg void OnSize				(UINT nType, int cx, int cy);
	afx_msg int	 OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnShowWindow		(BOOL bShow, UINT nStatus);
	afx_msg void OnRangeBtnCtrl		(UINT nID);
	virtual BOOL PreCreateWindow	(CREATESTRUCT& cs);
};

#endif // Wnd_ColorOp_h__
