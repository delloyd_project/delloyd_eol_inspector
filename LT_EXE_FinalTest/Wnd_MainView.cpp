﻿//*****************************************************************************
// Filename	: Wnd_MainView.cpp
// Created	: 2016/03/11
// Modified	: 2016/03/11
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
// Wnd_MainView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_MainView.h"
#include "resource.h"
#include "Dlg_ChkPassword.h"

//=============================================================================
// CWnd_MainView
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_MainView, CWnd_BaseView)

CWnd_MainView::CWnd_MainView()
{
	m_InspMode		= Permission_Operator;
	m_pstInspInfo	= NULL;
	m_pDevice		= NULL;
}

CWnd_MainView::~CWnd_MainView()
{
	TRACE(_T("<<< Start ~CWnd_MainView >>> \n"));
}

BEGIN_MESSAGE_MAP(CWnd_MainView, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE	()
END_MESSAGE_MAP()


//=============================================================================
// CWnd_MainView 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_MainView::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2010/11/26 - 14:25
// Desc.		:
//=============================================================================
int CWnd_MainView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	CStringW strText;

	m_wnd_SiteInfo.SetOwner(GetParent());
	m_wnd_SiteInfo.Create( _T(""), dwStyle, rectDummy, this, 100);

	m_wnd_ModelInfo.SetOwner(GetParent());
	m_wnd_ModelInfo.Create(NULL, _T(""), dwStyle, rectDummy, this, 101);

	m_wnd_LotInfo.SetOwner(GetParent());
	m_wnd_LotInfo.Create(NULL, _T(""), dwStyle, rectDummy, this, 102);

	m_wnd_TestInfo.SetOwner(GetParent());
	m_wnd_TestInfo.Create(NULL, _T(""), dwStyle, rectDummy, this, 103);

	m_wnd_MasterSet.SetOwner(GetParent());
	m_wnd_MasterSet.Create(NULL, _T(""), dwStyle, rectDummy, this, 104);

	m_stAlramMsg.SetTextColor(Color::White, Color::White);
	m_stAlramMsg.SetBorderColor_COLORREF(RGB(88, 88, 88));
	m_stAlramMsg.SetBackColor_COLORREF(RGB(88, 88, 88));
	m_stAlramMsg.SetTextAlignment(StringAlignmentNear);
	m_stAlramMsg.SetFont_Gdip(L"Arial", 9.0F);
	m_stAlramMsg.Create(_T("Alarm Message"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	
	return 0;
}

//=============================================================================
// Method		: CWnd_MainView::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2010/11/26 - 14:25
// Desc.		:
//=============================================================================
void CWnd_MainView::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin			= 5;
	int iSpacing		= 5;
	int iCateSpacing	= 10;
	int iErrHeight		= 20;

	int iLeft		= iMagrin;
	int iTop		= iMagrin;
	int iWidth		= cx - iMagrin - iMagrin;
	int iHeight		= cy - iMagrin - iMagrin;
	int iHalfWidth	= (iWidth - iCateSpacing) * 75 / 100;
	int iHalfHeight = (iHeight - iCateSpacing) * 4 / 5; //5 / 7;
	int iTempHeight = 0;
	int iTempWidth	= 0;

	m_wnd_SiteInfo.MoveWindow(iLeft, iTop, iWidth, iHalfHeight);

	iTop  = iMagrin + iHalfHeight + 5;
	iTempHeight = cy - iTop - iErrHeight;
 	
  	iWidth = (iWidth - iMagrin - iMagrin) / 3;
  	m_wnd_TestInfo.MoveWindow(iLeft, iTop, iWidth, iTempHeight);
	m_wnd_MasterSet.MoveWindow(iLeft, iTop, iWidth, iTempHeight);

	iLeft += iWidth;
	m_wnd_ModelInfo.MoveWindow(iLeft, iTop, iWidth + iSpacing, iTempHeight);

	iLeft += iWidth + iSpacing + iSpacing + 2;
 	m_wnd_LotInfo.MoveWindow(iLeft, iTop, iWidth - 4, iTempHeight);
	
	iTop = cy - iErrHeight;
	m_stAlramMsg.MoveWindow(0, iTop, cx, iErrHeight);

}
//=============================================================================
// Method		: MoveWindow_Status
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2016/5/17 - 11:32
// Desc.		:
//=============================================================================
void CWnd_MainView::MoveWindow_Status(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	int iSpacing = 5;

	int iLeft = x;
	int iTop = y;
	int iTempHeight = (nHeight - iSpacing) * 3 / 4;

	m_st_Judgment.MoveWindow(iLeft, iTop, nWidth, iTempHeight);
//	m_st_OperateMode.MoveWindow(iLeft, iTop, nWidth, iTempHeight);

// 	iTop += iTempHeight + iSpacing;
// 	iTempHeight = nHeight - iSpacing - iTempHeight;
// 	m_wnd_Progress.MoveWindow(iLeft, iTop, nWidth, iTempHeight);
	//m_st_ProgressInfo.MoveWindow(iLeft, iTop, nWidth, iTempHeight);

	//m_st_frameSatus.MoveWindow(iLeft, iTop, nWidth, nHeight);	
}
//=============================================================================
// Method		: SetInspectionMode
// Access		: public  
// Returns		: void
// Parameter	: enum_Inspection_Mode InspMode
// Qualifier	:
// Last Update	: 2016/1/13 - 14:18
// Desc.		:
//=============================================================================
void CWnd_MainView::SetInspectionMode(enPermissionMode InspMode)
{
	switch (InspMode)
	{
	case Permission_Operator:
		m_InspMode = Permission_Operator;
		break;

	case Permission_MES:
		m_InspMode = Permission_MES;
		break;

	case Permission_Manager:
	case Permission_Administrator:
	case Permission_Engineer:
	case Permission_CNC:
		m_InspMode = Permission_Administrator;

		m_pstInspInfo->LotInfo.Reset();
		UpdateLotInfo();

		m_pstInspInfo->YieldInfo.Reset();
		m_pstInspInfo->CycleTime.Reset();
		UpdateYield();

		break;

	default:
		break;
	}

	m_wnd_TestInfo.PermissionMode(InspMode);
	m_wnd_MasterSet.PermissionMode(InspMode);
}
//=============================================================================
// Method		: SetOperateMode
// Access		: public  
// Returns		: void
// Parameter	: __in enOperateMode nOperMode
// Qualifier	:
// Last Update	: 2018/3/12 - 19:52
// Desc.		:
//=============================================================================
void CWnd_MainView::SetOperateMode(__in enOperateMode nOperMode)
{
// 	switch (nOperMode)
// 	{
// 	case OpMode_Production:
// 	{
// 				  m_st_Judgment.ShowWindow(SW_SHOW);
// 				  m_st_OperateMode.ShowWindow(SW_HIDE);
// 	}
// 		break;
// 
// 	case OpMode_Master:
// 	case OpMode_StartUp_Check:
// 	//case OpMode_DryRun:
// 	{
// 				  m_st_OperateMode.SetText(g_szOperateMode[nOperMode]);
// 				  m_st_OperateMode.ShowWindow(SW_SHOW);
// 				  m_st_Judgment.ShowWindow(SW_HIDE);
// 	}
// 		break;
// 
// 	default:
// 		break;
// 	}
}
//=============================================================================
// Method		: SetMasterMode
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bMode
// Qualifier	:
// Last Update	: 2017/7/11 - 10:19
// Desc.		:
//=============================================================================
void CWnd_MainView::SetMasterMode(__in BOOL bMode)
{
	if (bMode == TRUE)
	{
		m_wnd_TestInfo.ShowWindow(SW_HIDE);
		m_wnd_LotInfo.ShowWindow(SW_SHOW);
		m_wnd_ModelInfo.ShowWindow(SW_SHOW);
		m_wnd_MasterSet.ShowWindow(SW_SHOW);
	}
	else
	{
		m_wnd_TestInfo.ShowWindow(SW_SHOW);
		m_wnd_LotInfo.ShowWindow(SW_SHOW);
		m_wnd_ModelInfo.ShowWindow(SW_SHOW);
		m_wnd_MasterSet.ShowWindow(SW_HIDE);
	}
}

//=============================================================================
// Method		: UpdateModelInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/7/12 - 16:03
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateModelInfo()
{
	// 검사항목 선택 처리
	m_wnd_SiteInfo.SetModelInfo(&m_pstInspInfo->ModelInfo);
	m_wnd_ModelInfo.SetModelInfo(&m_pstInspInfo->ModelInfo);
	m_wnd_ModelInfo.UpdatePogoCnt(&m_pstInspInfo->PogoInfo);
	m_wnd_MasterSet.SetModelInfo(&m_pstInspInfo->ModelInfo);
}

//=============================================================================
// Method		: UpdateMasterInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/7/11 - 17:19
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateMasterInfo(__in int iOffsetX, __in int iOffsetY, __in double dbDegree)
{
	m_wnd_MasterSet.SetMasterData(iOffsetX, iOffsetY, dbDegree);
}

//=============================================================================
// Method		: UpdateLotInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/4 - 10:54
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateLotInfo()
{	
	if (m_pstInspInfo->LotInfo.bLotStatus == TRUE)
		m_wnd_TestInfo.ButtonEnable(FALSE);
	else
		m_wnd_TestInfo.ButtonEnable(TRUE);

	m_wnd_LotInfo.SetLotInifo(&m_pstInspInfo->LotInfo);
}

void CWnd_MainView::UpdateMESInfo()
{
	m_wnd_LotInfo.SetMESInifo(&m_pstInspInfo->MESInfo.stMESResult);
}

//=============================================================================
// Method		: UpdateYield
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/4 - 10:54
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateYield()
{
 	m_wnd_LotInfo.SetYield(&m_pstInspInfo->YieldInfo);
}

//=============================================================================
// Method		: UpdateTestResult
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Parameter	: __in UINT nStepIdx
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2018/1/14 - 14:36
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateTestResult(__in enTestEachResult EachResult, __in UINT nStepIdx, __in CString strText)
{
	m_wnd_SiteInfo.SetTestResult(EachResult, nStepIdx, strText);
}

//=============================================================================
// Method		: UpdateTestJudgment
// Access		: public  
// Returns		: void
// Parameter	: __in enTestEachResult EachResult
// Qualifier	:
// Last Update	: 2018/1/14 - 15:27
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateTestJudgment(__in enTestEachResult EachResult)
{
	m_wnd_SiteInfo.SetTestJudgment(EachResult);
}

//=============================================================================
// Method		: UpdateResetResult
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/14 - 15:48
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateResetResult()
{
	m_wnd_SiteInfo.SetResetResult();
}

void CWnd_MainView::UpdateAlarmMessage(CString szText)
{
	m_stAlramMsg.SetText(szText);
}

void CWnd_MainView::OnSetBtnLotStartEnable(__in BOOL bEnable)
{
	m_wnd_TestInfo.LotStartBtnEnable(bEnable);
}

void CWnd_MainView::OnSetBtnLotStopEnable(__in BOOL bEnable)
{
	m_wnd_TestInfo.LotStopBtnEnable(bEnable);
}

void CWnd_MainView::OnSetBtnStartEnable(__in BOOL bEnable)
{
	m_wnd_TestInfo.StartBtnEnable(bEnable);
}

void CWnd_MainView::OnSetBtnStopEnable(__in BOOL bEnable)
{
	m_wnd_TestInfo.StopBtnEnable(bEnable);
}

// void CWnd_MainView::OnSetBtnMasterTestEnable(__in BOOL bEnable)
// {
// 	m_wnd_TestInfo.MasterTestBtnEnable(bEnable);
// }

void CWnd_MainView::OnSetBtnMasterModeEnable(__in BOOL bEnable)
{
	m_wnd_TestInfo.MasterModeBtnEnable(bEnable);
}
//=============================================================================
// Method		: UpdateElapsedTime_All
// Access		: public  
// Returns		: void
// Parameter	: __in DWORD dwTime
// Qualifier	:
// Last Update	: 2018/1/14 - 14:22
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateElapsedTime(__in DWORD dwTime)
{
	m_wnd_SiteInfo.SetElapsedTime(dwTime);
}

//=============================================================================
// Method		: UpdatePogoCnt
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/10 - 16:24
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdatePogoCnt()
{
	m_wnd_ModelInfo.UpdatePogoCnt(&m_pstInspInfo->PogoInfo);
}


BOOL CWnd_MainView::OnGetBtnStartEnable()
{
	return m_wnd_TestInfo.GetStartBtnEnable();
}


BOOL CWnd_MainView::OnGetBtnStopEnable()
{
	return m_wnd_TestInfo.GetStopBtnEnable();
}


//=============================================================================
// Method		: InsertBarcode
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2017/1/4 - 10:56
// Desc.		:
//=============================================================================
void CWnd_MainView::InsertBarcode(__in LPCTSTR szBarcode)
{
	m_wnd_SiteInfo.SetBarcode(szBarcode);
}


//=============================================================================
// Method		: UpdatSetResult
// Access		: public  
// Returns		: void
// Parameter	: __in enTestResult Result
// Parameter	: __in CString strText
// Qualifier	:
// Last Update	: 2017/7/11 - 9:58
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateSetResult(__in enTestEachResult Result, __in CString strText)
{
	m_wnd_SiteInfo.SetTestJudgment(Result, strText);
}
