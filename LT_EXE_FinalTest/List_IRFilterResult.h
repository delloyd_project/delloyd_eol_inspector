﻿#ifndef List_IRFilterResult_h__
#define List_IRFilterResult_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_IRFilterResult
{
	IRFilterResult_Object = 0,
	IRFilterResult_Value,
	IRFilterResult_TestThr,
	IRFilterResult_Result,
	IRFilterResult_MaxCol,
};

static LPCTSTR	g_lpszHeader_IRFilterResult[] =
{
	_T(""),
	_T("Value"),
	_T("Test Threshold"),
	_T("Result"),
	NULL
};

typedef enum enListItemNum_IRFilterResult
{
	IRFilterResult_ItemNum = 1,
};

static LPCTSTR	g_lpszItem_IRFilterResult[] =
{
	_T("FAIL"),
	_T("PASS"),
	NULL
};

const int	iListAglin_IRFilterResult[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_IRFilterResult[] =
{
	80,
	80,
	80,
	80,
};
// List_IRFilterInfo

class CList_IRFilterResult : public CListCtrl
{
	DECLARE_DYNAMIC(CList_IRFilterResult)

public:
	CList_IRFilterResult();
	virtual ~CList_IRFilterResult();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_IRFilter(ST_LT_TI_IRFilter* pstIRFilter)
	{
		if (pstIRFilter == NULL)
			return;

		m_pstIRFilter = pstIRFilter;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_IRFilter*  m_pstIRFilter;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
		
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif // List_IRFilterInfo_h__
