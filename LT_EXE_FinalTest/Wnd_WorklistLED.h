﻿//*****************************************************************************
// Filename	: Wnd_WorklistLED.h
// Created	: 2016/05/29
// Modified	: 2016/05/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_WorklistLED_h__
#define Wnd_WorklistLED_h__

#pragma once

#include "VGStatic.h"

#include "List_Worklist.h"
#include "List_Work_LED.h"

//=============================================================================
// Wnd_WorklistLED
//=============================================================================
class CWnd_WorklistLED : public CWnd
{
	DECLARE_DYNAMIC(CWnd_WorklistLED)

public:
	CWnd_WorklistLED();
	virtual ~CWnd_WorklistLED();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnNMClickListArray	( NMHDR * pNMHDR, LRESULT * result );

	CMFCTabCtrl					m_tc_Worklist;
	CList_Worklist				m_list_Array;

//	CList_Work_LED			m_list_LED[TICnt_LEDTest];

public:
	
	void		InsertWorklist		(__in const ST_Worklist* pstWorklist);
	void		GetPtr_Worklist		(ST_Worklist& pWorklist);

};

#endif // Wnd_WorklistLED_h__


