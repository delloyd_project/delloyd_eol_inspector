﻿// Wnd_SFROp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Wnd_SFROp.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_SFROp
typedef enum SFROptID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_SFROp, CWnd)

CWnd_SFROp::CWnd_SFROp()
{
	m_pstModelInfo	= NULL;
	m_nTestItemCnt	= 0;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font2.CreateFont(
		18,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_SFROp::~CWnd_SFROp()
{
	m_font.DeleteObject();
	m_font2.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_SFROp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_SFROp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_SFROp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_SFR_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szSFRStatic[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdex = 0; nIdex < BTN_SFR_MAX; nIdex++)
	{
		if (nIdex == BTN_SFR_FIELD || nIdex == BTN_SFR_EDGE || nIdex == BTN_SFR_DISTORTION || BTN_SFR_IIC_1)
			m_bn_Item[nIdex].Create(g_szSFRButton[nIdex], dwStyle | BS_PUSHLIKE | BS_AUTOCHECKBOX, rectDummy, this, IDC_BTN_ITEM + nIdex);
		else
			m_bn_Item[nIdex].Create(g_szSFRButton[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);

		m_bn_Item[nIdex].SetFont(&m_font);
	}

	m_bn_Item[BTN_SFR_FIELD].SetImage(IDB_UNCHECKED_16);
	m_bn_Item[BTN_SFR_FIELD].SetCheckedImage(IDB_CHECKED_16);
	m_bn_Item[BTN_SFR_FIELD].SizeToContent();
	m_bn_Item[BTN_SFR_FIELD].SetCheck(BST_UNCHECKED);

	m_bn_Item[BTN_SFR_EDGE].SetImage(IDB_UNCHECKED_16);
	m_bn_Item[BTN_SFR_EDGE].SetCheckedImage(IDB_CHECKED_16);
	m_bn_Item[BTN_SFR_EDGE].SizeToContent();
	m_bn_Item[BTN_SFR_EDGE].SetCheck(BST_UNCHECKED);

	m_bn_Item[BTN_SFR_DISTORTION].SetImage(IDB_UNCHECKED_16);
	m_bn_Item[BTN_SFR_DISTORTION].SetCheckedImage(IDB_CHECKED_16);
	m_bn_Item[BTN_SFR_DISTORTION].SizeToContent();
	m_bn_Item[BTN_SFR_DISTORTION].SetCheck(BST_UNCHECKED);

	m_bn_Item[BTN_SFR_IIC_1].SetImage(IDB_UNCHECKED_16);
	m_bn_Item[BTN_SFR_IIC_1].SetCheckedImage(IDB_CHECKED_16);
	m_bn_Item[BTN_SFR_IIC_1].SizeToContent();
	m_bn_Item[BTN_SFR_IIC_1].SetCheck(BST_UNCHECKED);

	for (UINT nIdex = 0; nIdex < EDT_SFR_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_SFR_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
		m_cb_Item[nIdex].SetFont(&m_font2);
	}

	for (int i = 0; i < enSFRDataType_Max; i++)
	{
		m_cb_Item[CMB_SFR_ResultType].AddString(g_szSFRDataType[i]);
	}
	m_cb_Item[CMB_SFR_ResultType].SetCurSel(0);

		
	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);


	if (!m_szI2CPath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_szI2CPath, I2C_FILE_EXT);
		m_IniWatch.RefreshList();

		RefreshFileList(CMB_SFR_IIC_1, m_IniWatch.GetFileList());
		m_cb_Item[CMB_SFR_IIC_1].SetCurSel(0);
	}

// 	if (!m_szI2CPath.IsEmpty())
// 	{
// 		m_IniWatch.SetWatchOption(m_szI2CPath, I2C_FILE_EXT);
// 		m_IniWatch.RefreshList();
// 
// 		RefreshFileList(CMB_SFR_IIC_2, m_IniWatch.GetFileList());
// 		m_cb_Item[CMB_SFR_IIC_2].SetCurSel(0);
// 	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_SFROp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iIdxCnt  = SfOp_ItemNum;
	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	int iHeaderH = 65;
	int iList_H = iHeaderH + iIdxCnt * 16;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	int iCtrl_W = iWidth / 5;
	int iCtrl_H = 25;

	iLeft = iMargin;
	m_st_Item[STI_SFR_PIXELSIZE].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iLeft += iCtrl_W + 1;
	m_ed_Item[EDT_SFR_PIXELSIZE].MoveWindow(iLeft, iTop, iCtrl_W * 1.5, iCtrl_H);

	iLeft = cx - iMargin - iCtrl_W;
	m_bn_Item[BTN_SFR_ROIRESET].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);
	
	iTop += iCtrl_H + iSpacing;
	iLeft = iMargin;
	m_st_Item[STI_SFR_RESULTTYPE].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iLeft += iCtrl_W + 1;
	m_cb_Item[CMB_SFR_ResultType].MoveWindow(iLeft, iTop, iCtrl_W * 1.5, 100);

	iLeft = cx - iMargin - iCtrl_W;
	m_bn_Item[BTN_SFR_TEST].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iTop += iCtrl_H + iSpacing;
	iLeft = iMargin;
	m_st_Item[STI_SFR_IIC_1].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iLeft += iCtrl_W + 1;
	m_bn_Item[BTN_SFR_IIC_1].MoveWindow(iLeft, iTop - 1, 35, iCtrl_H + 2);
	m_cb_Item[CMB_SFR_IIC_1].MoveWindow(iLeft + 35, iTop, iCtrl_W * 1.5 - 35, 100);
	
	iLeft = cx - iMargin - iCtrl_W;
	m_bn_Item[BTN_SFR_FIELD].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iTop += iCtrl_H + iSpacing;
// 	iLeft = iMargin;
// 	m_st_Item[STI_SFR_IIC_2].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);
// 
// 	iLeft += iCtrl_W + 1;
// 	m_bn_Item[BTN_SFR_IIC_2].MoveWindow(iLeft, iTop, 35, 100);
// 	m_cb_Item[CMB_SFR_IIC_2].MoveWindow(iLeft + 35, iTop, iCtrl_W * 1.5, 100);

	iLeft = cx - iMargin - iCtrl_W;
	m_bn_Item[BTN_SFR_EDGE].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iTop += iCtrl_H + iSpacing;
	m_bn_Item[BTN_SFR_DISTORTION].MoveWindow(iLeft, iTop, iCtrl_W, iCtrl_H);

	iLeft = iMargin;
	iTop += iCtrl_H + iSpacing;
	m_List.MoveWindow(iLeft, iTop, iWidth, cy - iTop - iMargin);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_SFROp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_SFROp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	//CWnd::OnShowWindow(bShow, nStatus);

	//if (NULL == m_pstModelInfo)
	//	return;

	//if (TRUE == bShow)
	//{
	//	m_pstModelInfo->nTestMode = TIID_SFR;
	//	m_pstModelInfo->nTestCnt = m_nTestItemCnt;
	//	m_pstModelInfo->nPicItem = PIC_SFR;
	//}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_SFROp::OnRangeBtnCtrl(UINT nID)
{
	//UINT nIdex = nID - IDC_BTN_ITEM;

	//switch (nIdex)
	//{
	//case BTN_SFR_TEST:
	//	GetOwner()->SendMessage(WM_MANUAL_TEST, m_nTestItemCnt, TIID_SFR);
	//	break;

	//case BTN_SFR_ROIRESET:
	//	m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt = m_pstModelInfo->stSFR[m_nTestItemCnt].stInitSFROpt;
	//	GetOwner()->SendMessage(WM_OPTDATA_RESET, 0, 0);
	//	break;
	//case BTN_SFR_EDGE:
	//	m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.bEdge = m_bn_Item[BTN_SFR_EDGE].GetCheck();
	//	GetOwner()->SendMessage(WM_EDGE_ONOFF, m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.bEdge, 0);
	//	break;
	//case BTN_SFR_DISTORTION:
	//	m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.bDistortion = m_bn_Item[BTN_SFR_DISTORTION].GetCheck();
	//	GetOwner()->SendMessage(WM_DISTORTION_CORR, m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.bDistortion, 0);
	//	break;
	//default:
	//	break;
	//}
}


void CWnd_SFROp::RefreshFileList(__in UINT nComboID, __in const CStringList* pFileList)
{
	m_cb_Item[nComboID].ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();

	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_cb_Item[nComboID].AddString(pFileList->GetNext(pos));
	}

	// 초기화
	if (0 < pFileList->GetCount())
	{
		m_cb_Item[nComboID].SetCurSel(0);
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_SFROp::SetUpdateData()
{
//	if (m_pstModelInfo == NULL)
//		return;
//
//	m_List.SetPtr_SFR(&m_pstModelInfo->stSFR[m_nTestItemCnt]);
//	m_List.SetImageSize(m_pstModelInfo->dwWidth, m_pstModelInfo->dwHeight);
//	m_List.InsertFullData();
//
//	CString strValue;
//	strValue.Format(_T("%.2f"), m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.dPixelSz);
//	m_ed_Item[EDT_SFR_PIXELSIZE].SetWindowText(strValue);
//
//	m_bn_Item[BTN_SFR_FIELD].SetCheck(m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.bField);
//	m_bn_Item[BTN_SFR_EDGE].SetCheck(m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.bEdge);
//	m_bn_Item[BTN_SFR_DISTORTION].SetCheck(m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.bDistortion);
//
//	m_cb_Item[CMB_SFR_ResultType].SetCurSel(m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.nResultType);
//
//	if (!m_szI2CPath.IsEmpty())
//	{
//		m_IniWatch.SetWatchOption(m_szI2CPath, I2C_FILE_EXT);
//		m_IniWatch.RefreshList();
//
//		RefreshFileList(CMB_SFR_IIC_1, m_IniWatch.GetFileList());
//	}
//
//	if (!m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.szIICFile_1.IsEmpty())
//	{
//		int iSel = m_cb_Item[CMB_SFR_IIC_1].FindStringExact(0, m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.szIICFile_1);
//
//		if (0 <= iSel)
//			m_cb_Item[CMB_SFR_IIC_1].SetCurSel(iSel);
//	}
//
//// 	if (!m_szI2CPath.IsEmpty())
//// 	{
//// 		m_IniWatch.SetWatchOption(m_szI2CPath, I2C_FILE_EXT);
//// 		m_IniWatch.RefreshList();
//// 
//// 		RefreshFileList(CMB_SFR_IIC_2, m_IniWatch.GetFileList());
//// 	}
//// 
//// 	if (!m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.szIICFile_1.IsEmpty())
//// 	{
//// 		int iSel = m_cb_Item[CMB_SFR_IIC_2].FindStringExact(0, m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.szIICFile_1);
//// 
//// 		if (0 <= iSel)
//// 			m_cb_Item[CMB_SFR_IIC_2].SetCurSel(iSel);
//// 	}
//
//	m_bn_Item[BTN_SFR_IIC_1].SetCheck(m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.bIICFile_1);
////	m_bn_Item[BTN_SFR_IIC_2].SetCheck(m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.bIICFile_2);
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/14 - 18:04
// Desc.		:
//=============================================================================
void CWnd_SFROp::GetUpdateData()
{
//	if (m_pstModelInfo == NULL)
//		return;
//
//	CString strValue;
//	m_ed_Item[EDT_SFR_PIXELSIZE].GetWindowText(strValue);
//	m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.dPixelSz = _ttof(strValue);
//	
//	m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.bField = m_bn_Item[BTN_SFR_FIELD].GetCheck();
//	m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.bEdge = m_bn_Item[BTN_SFR_EDGE].GetCheck();
//	m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.bDistortion = m_bn_Item[BTN_SFR_DISTORTION].GetCheck();
//
//	m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.nResultType = m_cb_Item[CMB_SFR_ResultType].GetCurSel();
//
//	m_cb_Item[CMB_SFR_IIC_1].GetWindowText(strValue);
//	m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.szIICFile_1 = strValue;
//
////	m_cb_Item[CMB_SFR_IIC_2].GetWindowText(strValue);
////	m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.szIICFile_2 = strValue;
//
//	m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.bIICFile_1 = m_bn_Item[BTN_SFR_IIC_1].GetCheck();
////	m_pstModelInfo->stSFR[m_nTestItemCnt].stSFROpt.bIICFile_2 = m_bn_Item[BTN_SFR_IIC_2].GetCheck();
}
