﻿#ifndef List_AngleOp_h__
#define List_AngleOp_h__

#pragma once

#include "Def_DataStruct.h"

typedef enum enListNum_AngleOp
{
	AngOp_Object = 0,
	AngOp_PosX,
	AngOp_PosY,
	AngOp_Width,
	AngOp_Height,
	AngOp_Mark,
	AngOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_AngleOp[] =
{
	_T(""),
	_T("CenterX"),
	_T("CenterY"),
	_T("Width"),
	_T("Height"),
	_T("Mark"),
	NULL
};

typedef enum enListItemNum_AngleOp
{
	AngOp_ItemNum = ROI_AL_Max,
};

static LPCTSTR	g_lpszItem_AngleOp[] =
{
	NULL
};

const int	iListAglin_AngleOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_AngleOp[] =
{
	80,
	80,
	80,
	80,
	80,
	80,
};
// List_AngleInfo

class CList_AngleOp : public CListCtrl
{
	DECLARE_DYNAMIC(CList_AngleOp)

public:
	CList_AngleOp();
	virtual ~CList_AngleOp();

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

	void SetPtr_Angle(ST_LT_TI_Angle* pstAngle)
	{
		if (pstAngle == NULL)
			return;

		m_pstAngle = pstAngle;
	};

	void SetImageSize(__out const UINT nWidth, __out const UINT nHeight)
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;
	};


protected:
	
	ST_LT_TI_Angle*  m_pstAngle;

	DECLARE_MESSAGE_MAP()

	CFont		m_Font;
	CEdit		m_ed_CellEdit;
	CComboBox	m_cb_Type;

	UINT	m_nEditCol;
	UINT	m_nEditRow;

	UINT	m_nWidth;
	UINT	m_nHeight;

	BOOL	UpdateCellData		(UINT nRow, UINT nCol, int  iValue);
	BOOL	UpdateCelldbData	(UINT nRow, UINT nCol, double dBValue);

public:
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel	(UINT nFlags, short zDelta, CPoint pt);
		
	afx_msg void	OnEnKillFocusEdit		();
	afx_msg void	OnEnKillFocusCombo		();
	afx_msg void	OnEnSelectFocusCombo	();
};

#endif // List_AngleInfo_h__
