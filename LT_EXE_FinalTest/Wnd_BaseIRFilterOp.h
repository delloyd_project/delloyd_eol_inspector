﻿#ifndef Wnd_BaseIRFilterOp_h__
#define Wnd_BaseIRFilterOp_h__

#pragma once

#include "Wnd_IRFilterOp.h"

// CWnd_BaseIRFilterOp

class CWnd_BaseIRFilterOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_BaseIRFilterOp)

public:
	CWnd_BaseIRFilterOp();
	virtual ~CWnd_BaseIRFilterOp();

protected:
	DECLARE_MESSAGE_MAP()

	ST_ModelInfo *m_pstModelInfo;

	CMFCTabCtrl m_tcTestItem;
	CWnd_IRFilterOp m_wndIRFilterOp[TICnt_IRFilter];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_ModelInfo(ST_ModelInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstModelInfo = pstRecipeInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};
#endif // Wnd_BaseIRFilterOp_h__
